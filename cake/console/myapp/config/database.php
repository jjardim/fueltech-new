<?php
class DATABASE_CONFIG {

	var $cakephp = array(
		'driver' => 'mysql',
		'persistent' => true,
		'host' => 'localhost',
		'login' => 'root',
		'password' => '',
		'database' => 'cakephp',
		'encoding' => 'utf8'
	);
}
?>