<?php

class SejaUmRevendedorController extends AppController {

    public $uses = array("SejaUmRevendedor");
    public $components = array("Session", "Email", "Filter");
    public $helpers = array("Image", "Flash", "Html", "Estados", "Javascript", "Calendario");

    public function admin_index() {
        $filtros = array();
        if (isset($this->data["Filter"]["filtro"])) {
            $filtros['filtro'] = "SejaUmRevendedor.nome_do_responsavel LIKE '%{%value%}%' OR SejaUmRevendedor.cnpj LIKE '%{%value%}%' OR SejaUmRevendedor.nome_fantasia LIKE '%{%value%}%'";
        }
        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();
        $data = $this->paginate('SejaUmRevendedor', $conditions);
        $this->set('seja_um_revendedor', $data);
    }
    
    public function admin_view($id = null) {
        $data = $this->SejaUmRevendedor->read(null, $id);
        $this->set('data', $data);
    }
    
    public function admin_delete($id = null) {
        $this->SejaUmRevendedor->delete($id);
        $this->redirect(array('controller' => 'seja_um_revendedor', 'action' => 'admin_index'));
    }

}

?>