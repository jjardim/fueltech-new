<?php

class ParceirosController extends AppController {

    var $name = 'Parceiros';
    var $components = array('Session', 'Filter');
    var $helpers = array('Calendario', 'String', 'Image', 'Flash', 'Javascript', 'Marcas', 'Estados');

    function admin_index() {
        //filters
        $filtros = array();
        if (isset($this->data["Filter"]["nome"])) {
            $filtros['nome'] = "Parceiro.nome LIKE '%{%value%}%'";
        }
        if (isset($this->data["Filter"]["filtro"])) {
            $filtros['filtro'] = "Parceiro.cidade LIKE '%{%value%}%' OR Parceiro.estado LIKE '%{%value%}%'";
        }

        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();

        if (isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar") {
            $this->admin_exportar($conditions);
        }

        $this->Parceiro->recursive = 0;
        $this->set('parceiros', $this->paginate($conditions));
    }

    public function admin_exportar($conditions) {

        App::import('Helper', 'Calendario');
        $this->Calendario = new CalendarioHelper();

        $rows = $this->Parceiro->find('all', array('conditions' => $conditions));

        $table = "<table>";
        $table .= "
				<tr bgcolor=\"#CECECE\">
					<td><strong>Id</strong></td>
					<td><strong>Nome</strong></td>
					<td><strong>Descricao Resumida</strong></td>
					<td><strong>Descricao Completa</strong></td>
					<td><strong>Cidade</strong></td>
                                        <td><strong>Estado</strong></td>
					<td><strong>Status</strong></td>
					<td><strong>Criado</strong></td>
					<td><strong>Modificado</strong></td>
				</tr>";
        foreach ($rows as $row) {
            $status = ( $row['Parceiro']['status'] ) ? "Ativo" : "Inativo";
            $table .= "
				<tr>
					<td>" . $row['Parceiro']['id'] . "</td>
					<td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Parceiro']['nome']) . "</td>
					<td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Parceiro']['descricao_resumida']) . "</td>
					<td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Parceiro']['descricao_completa']) . "</td>
					<td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Parceiro']['cidade']) . "</td>
                                        <td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Parceiro']['estado']) . "</td>
					<td>" . $status . "</td>
					<td>" . $this->Calendario->DataFormatada("d-m-Y", $row['Parceiro']['created']) . "</td>
					<td>" . $this->Calendario->DataFormatada("d-m-Y", $row['Parceiro']['modified']) . "</td>
				</tr>";
        }
        $table .= "</table>";

        App::import("helper", "String");
        $this->String = new StringHelper();
        $this->layout = false;
        $this->render(false);
        set_time_limit(0);
        header('Content-type: application/x-msexcel');
        $filename = "Parceiros_" . date("d_m_Y_H_i_s");
        header('Content-Disposition: attachment; filename=' . $filename . '.xls');
        header('Pragma: no-cache');
        header('Expires: 0');

        die($table);
    }

    function admin_add() {
        if (!empty($this->data)) {

            $this->Parceiro->create();

            if ($this->Parceiro->save($this->data)) {
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Parâmetro inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            $this->data['Parceiro']['id'] = $id;
            $this->Parceiro->id = $id;

            if (isset($this->data['Parceiro']['thumb_remove']) && $this->data['Parceiro']['thumb_remove'] == 1) {
                $this->Parceiro->remover_thumb($this->data['Parceiro']['id']);
            }

            if ($this->Parceiro->save($this->data)) {
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Parceiro->read(null, $id);
        }
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Parceiro->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
    }
    
   
    function get_cidades($uf){
        $this->render(false);
        $this->layout = false;
        
        $cidades = $this->Parceiro->find('all', 
                                                array(
                                                        'recursive'     => -1, 
                                                        'conditions'    => array('Parceiro.status' => true, 'Parceiro.localidade_label' => $uf), 
                                                        'fields'        => array('Parceiro.cidade'),
                                                        'group'         => array('Parceiro.cidade'),
                                                    )
                                            );
        
       die(json_encode(Set::combine($cidades, '{n}/Parceiro/cidade', '{n}/Parceiro/cidade')));
    }

    function lista_parceiros($local) {

        $conditions = array('Parceiro.status' => true);

        if ($local != null) {
            $conditions = array_merge($conditions, array("AND" => array("Parceiro.localidade_label LIKE" => "%$local%")));
        }

        if ($cidade != null) {
            $conditions = array_merge($conditions, array("AND" => array("Parceiro.cidade LIKE" => "%$cidade%")));
        }
        
        $this->layout = 'ajax';
        $this->paginate = array(
            'limit' => 9,
            'conditions' => $conditions
        );

        $this->set('parceiros', $this->paginate('Parceiro'));
        $this->render('lista_parceiros');
    }

    function busca_parceiros($estado = "", $cidade = "") {

        $conditions = array('Parceiro.status' => true);

        if ($estado != null) {
            $conditions = array_merge($conditions, array("AND" => array("Parceiro.localidade_label LIKE" => "%$estado%")));
        }

        if ($cidade != null) {
            $conditions = array_merge($conditions, array("AND" => array("Parceiro.cidade LIKE" => "%$cidade%")));
        }

        $this->layout = 'ajax';
        $this->paginate = array(
            'limit' => 9,
            'conditions' => $conditions
        );

        $this->set('parceiros', $this->paginate('Parceiro'));
        $this->render('lista_parceiros');
    }

}

?>