<?php
class CarrinhoAbandonadoController extends AppController {

	var $name = 'CarrinhoAbandonado';
	var $components = array('Session');
	var $helpers = array('Calendario','String','Image','Flash','Javascript');
	
	function admin_index() {
		$this->CarrinhoAbandonado->recursive = 0;
		$this->set('carrinhos_abandonados', $this->paginate());
	}	
}
?>