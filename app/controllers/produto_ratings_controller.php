<?php

class ProdutoRatingsController extends AppController {

	var $name = 'ProdutoRatings';
	var $components = array('Session','Filter');
	var $helpers = array('Image','Calendario', 'String', 'Javascript');

	function admin_index() {	
		//filters
		$filtros = array();
        if (isset($this->data["Filter"]["filtro"])) {
            $filtros['filtro'] = "Produto.nome LIKE '%{%value%}%' OR Usuario.nome LIKE '%{%value%}%' OR ProdutoRating.comentario LIKE '%{%value%}%'";
        }
		
        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();
		
		if(isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar"){
			$this->admin_exportar($conditions);
		}
		
		$this->ProdutoRating->recursive = 1;
		$this->set('classificacoes', $this->paginate($conditions));
	}

	function admin_mudastatus($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash('Parâmetro inválidos','flash/error');
			$this->redirect(array('action' => 'index'));
		}
		
		$oesquema = $this->ProdutoRating->find('first',array('conditions'=>array('ProdutoRating.id'=>$id)));
		
		if( $oesquema['ProdutoRating']['status'] == 1 )
			$oesquema['ProdutoRating']['status'] = 0;
		else
			$oesquema['ProdutoRating']['status'] = 1;
			
		if ($this->ProdutoRating->save($oesquema)) {
			$this->Session->setFlash('Alteração de status realizado com sucesso.', 'flash/success');
			$this->redirect(array('action' => 'index'));
		} else {
			$this->Session->setFlash('Erro.', 'flash/error');
		}
	}
	
	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash('Parametros inválidos', 'flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->ProdutoRating->delete($id)) {
			$this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
		$this->redirect(array('action' => 'index'));
	}
	public function admin_exportar($conditions){
	
		App::import('Helper', 'Calendario');
		$this->Calendario = new CalendarioHelper();
		
		$rows = $this->ProdutoRating->find('all',array('conditions' => $conditions));
		
		$table = "<table>";
		$table .= "
				<tr bgcolor=\"#CECECE\">
					<td><strong>Id</strong></td>
					<td><strong>Produto</strong></td>
					<td><strong>".iconv("UTF-8", "ISO-8859-1//IGNORE","Usuário")."</strong></td>
					<td><strong>".iconv("UTF-8", "ISO-8859-1//IGNORE","Título")."</strong></td>
					<td><strong>Nota</strong></td>
					<td><strong>".iconv("UTF-8", "ISO-8859-1//IGNORE","Avaliação Geral")."</strong></td>
					<td><strong>".iconv("UTF-8", "ISO-8859-1//IGNORE","Localização")."</strong></td>
					<td><strong>".iconv("UTF-8", "ISO-8859-1//IGNORE","Comnetário")."</strong></td>
					<td><strong>Status</strong></td>
					<td><strong>Criado</strong></td>
					<td><strong>Editado</strong></td>
				</tr>";
		foreach ($rows as $row) {
			$status = ( $row['ProdutoRating']['status'] ) ? "Ativo" : "Inativo";
			$table .= "
				<tr>
					<td>".$row['ProdutoRating']['id']."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Produto']['nome'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Usuario']['nome'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['ProdutoRating']['titulo'])."</td>
					<td>".$row['ProdutoRating']['nota']."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['ProdutoRating']['avaliacao_geral'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['ProdutoRating']['localizacao'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['ProdutoRating']['comentario'])."</td>
					<td>".$status."</td>
					<td>".$this->Calendario->DataFormatada("d-m-Y",  $row['ProdutoRating']['created'])."</td>
					<td>".$this->Calendario->DataFormatada("d-m-Y",  $row['ProdutoRating']['modified'])."</td>
				</tr>";
		}
		$table .= "</table>";
		
		App::import("helper", "String");
		$this->String = new StringHelper();
		$this->layout = false;
		$this->render(false);
		set_time_limit(0);		
		header('Content-type: application/x-msexcel');
		$filename = "classificacoes_" . date("d_m_Y_H_i_s");
		header('Content-Disposition: attachment; filename='.$filename.'.xls');
		header('Pragma: no-cache');
		header('Expires: 0');
		
		die($table);
	}
    
}

?>