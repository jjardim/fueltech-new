<?php

class DownloadsController extends AppController {

    var $name = 'Downloads';
    var $components = array('Session', 'Filter');
    var $helpers = array('Calendario', 'String', 'Image', 'Flash', 'Javascript');

    function admin_index() {
        //filters
        $filtros = array();
        if (isset($this->data["Filter"]["filtro"])) {
            $filtros['filtro'] = "Download.nome LIKE '%{%value%}%' OR Download.descricao LIKE '%{%value%}%'";
        }
        if (isset($this->data["Filter"]["download_tipo_id"])) {
            $filtros['download_tipo_id'] = "Download.download_tipo_id = '{%value%}'";
        }

        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();

        if (isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar") {
            $this->admin_exportar($conditions);
        }

        $this->Download->recursive = 0;
        $this->set('downloads', $this->paginate($conditions));

        //video tipo
        App::import('Model', 'DownloadTipo');
        $this->DownloadTipo = new DownloadTipo();
        $download_tipos = $this->DownloadTipo->find('list', array('recursive' => -1, 'fields' => array('DownloadTipo.id', 'DownloadTipo.nome')));
        $this->set('download_tipos', $download_tipos);
    }

    public function admin_exportar($conditions) {

        App::import('Helper', 'Calendario');
        $this->Calendario = new CalendarioHelper();

        App::import('Helper', 'Html');
        $this->Html = new HtmlHelper();

        $rows = $this->Download->find('all', array('conditions' => $conditions));

        $table = "<table>";
        $table .= "
				<tr bgcolor=\"#CECECE\">
					<td><strong>Id</strong></td>
					<td><strong>Language</strong></td>
					<td><strong>Tipo</strong></td>
					<td><strong>Nome</strong></td>
					<td><strong>Descricao</strong></td>
					<td><strong>Versao</strong></td>
					<td><strong>URL</strong></td>
					<td><strong>Status</strong></td>
					<td><strong>Criado</strong></td>
					<td><strong>Modificado</strong></td>
				</tr>";
        foreach ($rows as $row) {
            $status = ( $row['Download']['status'] ) ? "Ativo" : "Inativo";
            $table .= "
				<tr>
					<td>" . $row['Download']['id'] . "</td>
					<td>" . $row['Download']['language'] . "</td>
					<td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['DownloadTipo']['nome']) . "</td>
					<td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Download']['nome']) . "</td>
					<td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Download']['descricao']) . "</td>
					<td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Download']['versao']) . "</td>
					<td>" . $this->Html->Url("/downloads/download/", true) . $row['Download']['id'] . "/" . low(Inflector::slug($row['Download']['nome'], '-')) . "/" . "</td>
					<td>" . $status . "</td>
					<td>" . $this->Calendario->DataFormatada("d-m-Y", $row['Download']['created']) . "</td>
					<td>" . $this->Calendario->DataFormatada("d-m-Y", $row['Download']['modified']) . "</td>
				</tr>";
        }
        $table .= "</table>";

        App::import("helper", "String");
        $this->String = new StringHelper();
        $this->layout = false;
        $this->render(false);
        set_time_limit(0);
        header('Content-type: application/x-msexcel');
        $filename = "downloads_" . date("d_m_Y_H_i_s");
        header('Content-Disposition: attachment; filename=' . $filename . '.xls');
        header('Pragma: no-cache');
        header('Expires: 0');

        die($table);
    }

    function admin_add() {

        if (!empty($this->data)) {           

            if(isset($this->data['Download']['nome_arquivo']) && $this->data['Download']['nome_arquivo'] != ""){                
                App::import('Helper','String');
                $this->String = new StringHelper();
                $this->Download->Behaviors->detach('MeioUpload');
                $this->data['Download']['filename'] = $this->data['Download']['nome_arquivo'];
                $this->data['Download']['filesize']     = $this->data['Download']['tamanho_arquivo']*1000000;
                $this->data['Download']['dir']      = 'uploads/download/filename';
                $this->data['Download']['mimetype'] = $this->String->type_file($this->data['Download']['nome_arquivo']);

               if($this->data['Download']['thumb_filename']){
                     $icone = $this->data['Download']['thumb_filename'];

                     $this->data['Download']['thumb_filename'] = $icone['name'];
                     $this->data['Download']['thumb_dir'] = 'uploads/download/thumb';
                     $this->data['Download']['thumb_mimetype'] = $icone['type'];
                     $this->data['Download']['thumb_filesize'] = $icone['size'];                       
                }
            }

            $this->Download->create();

            if ($this->Download->save($this->data)) {
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }

        //linguagens
        App::import("Model", 'Linguagem');
        $this->Linguagem = new Linguagem();
        $idiomas = $this->Linguagem->find("list", array("fields" => array("Linguagem.codigo", "Linguagem.nome"), "conditions" => array("Linguagem.status" => true)));
        $this->set("idiomas", array('' => 'Selecione...') + $idiomas);

        //downloads tipos
        App::import("Model", 'DownloadTipo');
        $this->DownloadTipo = new DownloadTipo();
        $download_tipos = $this->DownloadTipo->find("list", array("fields" => array("DownloadTipo.id", "DownloadTipo.nome"), "conditions" => array("DownloadTipo.status" => true)));
        $this->set("download_tipos", array('' => 'Selecione...') + $download_tipos);
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Parâmetro inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }

        if (!empty($this->data)) {
            $this->data['Download']['id'] = $id;
            $this->Download->id = $id;

            if (isset($this->data['Download']['thumb_remove']) && $this->data['Download']['thumb_remove'] == 1) {
                $this->Download->remover_thumb($this->data['Download']['id']);
            }
            
            if(isset($this->data['Download']['name']) && $this->data['Download']['name'] != ""){                
                App::import('Helper','String');
                $this->String = new StringHelper();
                $this->Download->Behaviors->detach('MeioUpload');
                $this->data['Download']['filename'] = $this->data['Download']['name'];
                $this->data['Download']['filesize']     = $this->data['Download']['size']*1000000;
                $this->data['Download']['dir']      = 'uploads/download/filename';
                $this->data['Download']['mimetype'] = $this->String->type_file($this->data['Download']['name']);
            }

            if ($this->Download->save($this->data)) {
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Download->read(null, $id);
            $this->data['Download']['nome_arquivo'] = $this->data['Download']['filename'];
            $this->data['Download']['tamanho_arquivo'] = $this->data['Download']['filesize'];
        }

        if ($this->data['Download']['thumb_filename'] != "") {
            $this->data['Download']['thumb'] = 1;
        } else {
            $this->data['Download']['thumb'] = 0;
        }



        //linguagens
        App::import("Model", 'Linguagem');
        $this->Linguagem = new Linguagem();
        $idiomas = $this->Linguagem->find("list", array("fields" => array("Linguagem.codigo", "Linguagem.nome"), "conditions" => array("Linguagem.status" => true)));
        $this->set("idiomas", array('' => 'Selecione...') + $idiomas);

        //downloads tipos
        App::import("Model", 'DownloadTipo');
        $this->DownloadTipo = new DownloadTipo();
        $download_tipos = $this->DownloadTipo->find("list", array("fields" => array("DownloadTipo.id", "DownloadTipo.nome"), "conditions" => array("DownloadTipo.status" => true)));
        $this->set("download_tipos", array('' => 'Selecione...') + $download_tipos);
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Download->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
    }

    function download($id) {

        $arquivo = $this->Download->find('first', array('conditions' => array('Download.id' => $id)));

        if (!$arquivo) {
            $this->redirect($this->referer());
        }

        $dir = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'uploads' . DS . 'download' . DS . 'filename' . DS;
        $filename = $dir . $arquivo['Download']['filename'];

        if (file_exists($filename)) {
            //Parse file get extension and size
            $fsize = filesize($filename);
            $path_parts = pathinfo($filename);
            $ext = strtolower($path_parts["extension"]);
            // Determine Content Type
            switch ($ext) {
                case "pdf":
                    $ctype = "application/pdf";
                    break;

                case "exe":
                    $ctype = "application/octet-stream";
                    break;

                case "zip":
                case "rar":
                    $ctype = "application/zip";
                    break;

                case "doc":
                case "docx":
                    $ctype = "application/msword";
                    break;

                case "xls":
                case "xlsx":
                    $ctype = "application/vnd.ms-excel";
                    break;

                case "ppt":
                case "pptx":
                    $ctype = "application/vnd.ms-powerpoint";
                    break;

                case "gif":
                    $ctype = "image/gif";
                    break;

                case "png":
                    $ctype = "image/png";
                    break;

                case "jpeg":
                case "jpg":
                    $ctype = "image/jpg";
                    break;

                default: $ctype = "application/force-download";
            }

            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private", false);
            header("Content-Type: $ctype");
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=" . basename($filename) . ";");
            header("Content-Transfer-Encoding: binary");
            header("Content-Length: " . $fsize);
            readfile($filename) or die(‘Errors’);
            exit(0);
        }
    }

    public function admin_get_ajax_downloads() {
        $this->layout = 'ajax';
        $variacoes = $this->Download->find("all", array(
            'recursive' => -1,
            'fields' => array('Download.id', 'Download.nome'),
            'conditions' => array('Download.download_tipo_id' => $this->params['form']['download_id'])
                )
        );
        $final = array();
        if ($variacoes) {
            foreach ($variacoes as $variacoes) {
                $final[$variacoes['Download']['id']] = $variacoes['Download']['nome'];
            }
        }
        die(json_encode($final));
    }
}

?>