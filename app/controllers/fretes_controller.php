<?php
//App::import('Controller', 'Adm');
class FretesController extends AppController {

	var $name = 'Fretes';
	var $components = array('Filter','Session');
	var $helpers = array('Form','Session','String','Javascript'	,'Calendario');
	var $uses = array('Frete');
	
	function admin_index() {
		 
        //options status do pedido
        //filtros
        $filtros['frete_tipo_id'] = "Frete.frete_tipo_id = '{%value%}'";        

        if ($this->data["Filter"]["cep_inicial"]) {
            $filtros['cep_inicial'] = "Frete.cep_inicial >= '{%value%}' ";
        }
		if ($this->data["Filter"]["cep_final"]) {
            $filtros['cep_final'] = "Frete.cep_final <= '{%value%}' ";
        }

        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();


        $this->paginate = array('limit' => 24);
        $fretes = $this->paginate('Frete', $conditions);
        $this->set('fretes', $fretes);
         //options status do pedido
        $frete_tipos = array('' => 'Selecione') +$this->Frete->FreteTipo->find('list', array('conditions'=>array('status'=>true),'fields' => array('id', 'nome')));
        $this->set(compact('frete_tipos'));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->Frete->create();
			if ($this->Frete->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				 $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash('Parametros inválidos', 'flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {                        
			if ($this->Frete->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				 $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Frete->read(null, $id);
		}
	}
	function admin_delete($id = null) {
		if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Frete->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}
	
}
?>