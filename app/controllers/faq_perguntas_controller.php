<?php

class FaqPerguntasController extends AppController {

    var $name = 'FaqPerguntas';
    var $components = array('Session', 'Filter');
    var $helpers = array('Calendario', 'String', 'Image', 'Flash', 'Javascript');

    function admin_index() {
	//filters
	$filtros = array();
	if (isset($this->data["Filter"]["filtro"])) {
	    $filtros['filtro'] = "FaqPergunta.pergunta LIKE '%{%value%}%'";
	}

	$this->Filter->setConditions($filtros);
	$this->Filter->check();
	$conditions = $this->Filter->getFilters();
	$conditions = array_merge(array('Faq.status' => true), $conditions);
	$this->Filter->setDataToView();

	if (isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar") {
	    $this->admin_exportar($conditions);
	}

	$this->FaqPergunta->recursive = 1;
	$faq_perguntas = $this->paginate($conditions);
	$this->set('faq_perguntas', $faq_perguntas);
    }

    public function admin_exportar($conditions) {

	App::import('Helper', 'Calendario');
	$this->Calendario = new CalendarioHelper();

	$rows = $this->DuvidaFrequente->find('all', array('conditions' => $conditions));

	$table = "<table>";
	$table .= "
				<tr bgcolor=\"#CECECE\">
					<td><strong>Id</strong></td>
					<td><strong>Pergunta</strong></td>
					<td><strong>Resposta</strong></td>
					<td><strong>Status</strong></td>
					<td><strong>Criado</strong></td>
					<td><strong>Modificado</strong></td>
				</tr>";
	foreach ($rows as $row) {
	    $status = ( $row['DuvidaFrequente']['status'] ) ? "Ativo" : "Inativo";
	    $table .= "
				<tr>
					<td>" . $row['DuvidaFrequente']['id'] . "</td>
					<td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['DuvidaFrequente']['pergunta']) . "</td>
					<td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['DuvidaFrequente']['resposta']) . "</td>
					<td>" . $status . "</td>
					<td>" . $this->Calendario->DataFormatada("d-m-Y", $row['DuvidaFrequente']['created']) . "</td>
					<td>" . $this->Calendario->DataFormatada("d-m-Y", $row['DuvidaFrequente']['modified']) . "</td>
				</tr>";
	}
	$table .= "</table>";

	App::import("helper", "String");
	$this->String = new StringHelper();
	$this->layout = false;
	$this->render(false);
	set_time_limit(0);
	header('Content-type: application/x-msexcel');
	$filename = "duvidas_frequentes_" . date("d_m_Y_H_i_s");
	header('Content-Disposition: attachment; filename=' . $filename . '.xls');
	header('Pragma: no-cache');
	header('Expires: 0');

	die($table);
    }

    function admin_add() {
	if (!empty($this->data)) {
	    if (isset($this->data['FaqPerguntaDescricao'][0]['pergunta'])) {
		$this->data['FaqPergunta']['pergunta'] = $this->data['FaqPerguntaDescricao'][0]['pergunta'];
	    }
	    $this->FaqPergunta->create();

	    if ($this->FaqPergunta->saveAll($this->data)) {
		$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
		$this->redirect(array('action' => 'index'));
	    } else {
		$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
	    }
	}

	//set linguas
	App::import('Model', 'Linguagem');
	$this->Linguagem = new Linguagem();
	$idiomas = $this->Linguagem->find('all', array('fields' => array('Linguagem.id', 'Linguagem.codigo', 'Linguagem.nome', 'Linguagem.thumb_filename', 'Linguagem.thumb_dir'), 'conditions' => array('Linguagem.status' => true)));
	$this->set('idiomas', $idiomas);

	//set faqs
	App::import('Model', 'Faq');
	$this->Faq = new Faq();
	$faqs = $this->Faq->find('list', array('fields' => array('Faq.id', 'Faq.nome'), 'conditions' => array('Faq.status' => true)));
	$this->set('faqs', array('' => 'Selecione...') + $faqs);
    }

    function admin_edit($id = null) {
	//set faqs
	App::import('Model', 'FaqPerguntaDescricao');
	$this->FaqPerguntaDescricao = new FaqPerguntaDescricao();

	if (!$id && empty($this->data)) {
	    $this->Session->setFlash('Parâmetro inválidos', 'flash/error');
	    $this->redirect(array('action' => 'index'));
	}
	if (!empty($this->data)) {
	    $this->data['FaqPergunta']['id'] = $id;
	    $this->FaqPergunta->id = $id;

	    if (isset($this->data['FaqPerguntaDescricao'][0]['pergunta'])) {
		$this->data['FaqPergunta']['pergunta'] = $this->data['FaqPerguntaDescricao'][0]['pergunta'];
	    }

	    if ($this->FaqPergunta->save($this->data)) {
		foreach ($this->data['FaqPerguntaDescricao'] as $FaqPerguntaDescricao) {
		    $FaqPerguntaDescricao['faq_pergunta_id'] = $id;
		    $this->FaqPerguntaDescricao->save($FaqPerguntaDescricao);
		}
		$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
		$this->redirect(array('action' => 'index'));
	    } else {
		$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
	    }
	}
	if (empty($this->data)) {
	    $this->data = $this->FaqPergunta->read(null, $id);
	}

	//set linguas
	App::import('Model', 'Linguagem');
	$this->Linguagem = new Linguagem();
	$idiomas = $this->Linguagem->find('all', array('fields' => array('Linguagem.id', 'Linguagem.codigo', 'Linguagem.nome', 'Linguagem.thumb_filename', 'Linguagem.thumb_dir'), 'conditions' => array('Linguagem.status' => true)));
	$this->set('idiomas', $idiomas);

	//set faqs
	App::import('Model', 'Faq');
	$this->Faq = new Faq();
	$faqs = $this->Faq->find('list', array('fields' => array('Faq.id', 'Faq.nome'), 'conditions' => array('Faq.status' => true)));
	$this->set('faqs', array('' => 'Selecione...') + $faqs);
    }

    function admin_delete($id = null) {
	if (!$id) {
	    $this->Session->setFlash('Parametros inválidos', 'flash/error');
	    $this->redirect(array('action' => 'index'));
	}
	if ($this->FaqPergunta->delete($id)) {
	    $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
	    $this->redirect(array('action' => 'index'));
	}
	$this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
	$this->redirect(array('action' => 'index'));
    }

}

?>