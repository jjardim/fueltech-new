<?php

class PedidoStatusController extends AppController {

    var $name = 'PedidoStatus';
	var $uses = array('PedidoStatus');
    var $components = array('Session');
    var $helpers = array('Image','Calendario', 'String', 'Javascript');

    function admin_index() {
        $this->PedidoStatus->recursive = 0;
        $this->set('pedido_status', $this->paginate());
    }

    function admin_add() {
        if (!empty($this->data)) {
            if ($this->PedidoStatus->saveAll($this->data)) {
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Parâmetro inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
			if($this->data['PedidoStatus']['restaurar_template']){
				$template = $this->PedidoStatus->read(null, $id);
				$this->data['PedidoStatus']['template_email'] = $template['PedidoStatus']['template_default'];
			}
             if ($this->PedidoStatus->save($this->data)) {
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->PedidoStatus->read(null, $id);
            if (!$this->data) {
                $this->redirect(array('action' => 'index'));
            }
        }
    }
	function admin_delete($id = null) {
		if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->PedidoStatus->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}
    
}

?>