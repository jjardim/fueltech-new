<?php
class GaleriasController extends AppController {

	var $name = 'Galerias';
	var $components = array('Session','Filter');
	var $helpers = array('Calendario','String','Image','Flash','Javascript','Marcas', 'Uploadify');
	
	function admin_index() {
		//filters
		$filtros = array();
        if (isset($this->data["Filter"]["nome"])) {
            $filtros['nome'] = "Galeria.nome LIKE '%{%value%}%'";
        }
		
        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();
		
		if(isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar"){
			$this->admin_exportar($conditions);
		}
		
		$this->Galeria->recursive = 0;
		$this->set('galerias', $this->paginate($conditions));
	}
	public function admin_exportar($conditions){
		
		App::import('Helper', 'Calendario');
		$this->Calendario = new CalendarioHelper();
		
		$rows = $this->Assistencia->find('all',array('conditions' => $conditions));
		
		$table = "<table>";
		$table .= "
				<tr bgcolor=\"#CECECE\">
					<td><strong>Id</strong></td>
					<td><strong>Marca</strong></td>
					<td><strong>URL</strong></td>
					<td><strong>Status</strong></td>
					<td><strong>Criado</strong></td>
					<td><strong>Modificado</strong></td>
				</tr>";
		foreach ($rows as $row) {
			$status = ( $row['Assistencia']['status'] ) ? "Ativo" : "Inativo";
			$table .= "
				<tr>
					<td>".$row['Assistencia']['id']."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Assistencia']['marca'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Assistencia']['url'])."</td>
					<td>".$status."</td>
					<td>".$this->Calendario->DataFormatada("d-m-Y",  $row['Assistencia']['created'])."</td>
					<td>".$this->Calendario->DataFormatada("d-m-Y",  $row['Assistencia']['modified'])."</td>
				</tr>";
		}
		$table .= "</table>";
		
		App::import("helper", "String");
		$this->String = new StringHelper();
		$this->layout = false;
		$this->render(false);
		set_time_limit(0);		
		header('Content-type: application/x-msexcel');
		$filename = "assistencias_" . date("d_m_Y_H_i_s");
		header('Content-Disposition: attachment; filename='.$filename.'.xls');
		header('Pragma: no-cache');
		header('Expires: 0');
		
		die($table);
	}
	function admin_add() {
		if (!empty($this->data)) {
		
			$this->Galeria->create();
            
			if ($this->Galeria->save($this->data)) {
				
				App::import("Model", "GaleriaFoto");
				$this->GaleriaFoto = new GaleriaFoto();
                $this->Galeria->bindModel(array('hasMany' => array('GaleriaFoto')));
                $this->GaleriaFoto->create();
                foreach ($this->Session->read('TMP.GaleriaFoto') as $img):
                    $this->data['GaleriaFoto']['id'] = '';
                    $this->data['GaleriaFoto']['dir'] = '';
                    $this->data['GaleriaFoto']['mimetype'] = '';
                    $this->data['GaleriaFoto']['filesize'] = '';
                    $this->data['GaleriaFoto']['ordem'] = $img['ordem'];
					if(isset($img['destaque'])){
						$this->data['GaleriaFoto']['destaque'] = $img['destaque'];
                    }
					$this->data['GaleriaFoto']['filename']['type'] = 'image/jpeg';
                    $this->data['GaleriaFoto']['filename']['name'] = $img['tmp_file'];
                    $this->data['GaleriaFoto']['filename']['tmp_name'] = WWW_ROOT . 'uploads' . DS . 'tmp' . DS . $img['tmp_file'];
                    $this->data['GaleriaFoto']['filename']['error'] = $img['error'];
                    $this->data['GaleriaFoto']['filename']['size'] = $img['size'];
                    $this->data['GaleriaFoto']['filename']['created'] = time();
                    $this->data['GaleriaFoto']['filename']['modified'] = time();
					$this->data['GaleriaFoto']['galeria_id'] = $this->Galeria->id;
                    $this->GaleriaFoto->save($this->data);

                    unlink(WWW_ROOT . 'uploads' . DS . 'tmp' . DS . $img['tmp_file']);
                endForeach;
                $this->Session->delete('TMP.GaleriaFoto');
			
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
		
		$imgs = (array) $this->Session->read('TMP.GaleriaFoto');
        $this->set('imgs', $this->FlashImages($imgs, 'GaleriaFoto', true));
		
		//galeria tipos
		App::import('Model','GaleriaTipo');
		$this->GaleriaTipo = new GaleriaTipo();
		$galeria_tipos = $this->GaleriaTipo->find('list', array('fields' => array('GaleriaTipo.id','GaleriaTipo.nome'), 'conditions' => array('GaleriaTipo.status' => true)));
		$this->set('galeria_tipos', array(''=> 'Selecione...')+$galeria_tipos);
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash('Parâmetro inválidos','flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			$this->data['Galeria']['id'] = $id;
			$this->Galeria->id = $id;
                 
			if ($this->Galeria->save($this->data)) {
				
				
				App::import("Model", "GaleriaFoto");
				$this->GaleriaFoto = new GaleriaFoto();
				
				 //GaleriaFoto
                $this->Galeria->bindModel(array('hasMany' => array('GaleriaFoto')));
                $this->GaleriaFoto->create();
                $imgs_tmp = $this->Session->read('TMP.GaleriaFoto');
                if ($imgs_tmp):
                    foreach ($imgs_tmp as $img):
                        $this->data['GaleriaFoto']['galeria_id'] = $this->Galeria->id;
                        $this->data['GaleriaFoto']['id'] = '';
                        $this->data['GaleriaFoto']['dir'] = '';
                        $this->data['GaleriaFoto']['mimetype'] = '';
                        $this->data['GaleriaFoto']['filesize'] = '';
                        $this->data['GaleriaFoto']['filename']['type'] = 'image/jpeg';
                        $this->data['GaleriaFoto']['ordem'] = $img['ordem'];
						
						if(isset($img['destaque'])){
							$this->data['GaleriaFoto']['destaque'] = $img['destaque'];
                        }
						
						$this->data['GaleriaFoto']['filename']['name'] = $img['tmp_file'];
                        $this->data['GaleriaFoto']['filename']['tmp_name'] = WWW_ROOT . 'uploads' . DS . 'tmp' . DS . $img['tmp_file'];
                        $this->data['GaleriaFoto']['filename']['error'] = $img['error'];
                        $this->data['GaleriaFoto']['filename']['size'] = $img['size'];
                        $this->data['GaleriaFoto']['filename']['created'] = time();
                        $this->data['GaleriaFoto']['filename']['modified'] = time();
                        $this->GaleriaFoto->save($this->data);
                        unlink($img['tmp_file']);
                    endForeach;
                endIf;
                if (isset($this->data['GaleriaFoto'])):
                    foreach ($this->data['GaleriaFoto'] as $id):
                        $this->GaleriaFoto->delete($id);
                    endforeach;
                endIf;
                $this->Session->delete('TMP.GaleriaFoto');
				
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Galeria->read(null, $id);
		}
		
		/*         * *
         * lista as imagens já cadastradas.
         * */
        $imgs = $imgs_old = array();
        foreach ($this->data['GaleriaFoto'] as $img) {
            $imgs_old[] = $img;
        }
        $this->set('imgs_old', $imgs_old);
		
		//galeria tipos
		App::import('Model','GaleriaTipo');
		$this->GaleriaTipo = new GaleriaTipo();
		$galeria_tipos = $this->GaleriaTipo->find('list', array('fields' => array('GaleriaTipo.id','GaleriaTipo.nome'), 'conditions' => array('GaleriaTipo.status' => true)));
		$this->set('galeria_tipos', array(''=> 'Selecione...')+$galeria_tipos);
	}
	
	function admin_delete($id = null) {
		if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Galeria->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}
	
	
	function formatbytes($file, $type = "MB") {
        switch ($type) {
            case "KB":
                $filesize = filesize($file) * .0009765625; // bytes to KB
                break;
            case "MB":
                $filesize = (filesize($file) * .0009765625) * .0009765625; // bytes to MB
                break;
            case "GB":
                $filesize = ((filesize($file) * .0009765625) * .0009765625) * .0009765625; // bytes to GB
                break;
        }
        if ($filesize <= 0) {
            return $filesize = 'unknown file size';
        } else {
            return round($filesize, 2) . ' ' . $type;
        }
    }

    public function FlashImages($imgs, $model = "ProdutoImagem", $destaque = false) {

        App::import("helper", "Html");
        $this->Html = new HtmlHelper();
        App::import("helper", "Form");
        $this->Form = new FormHelper();
        App::import("helper", "Image");
        $this->Image = new ImageHelper();

        $string = '';
        foreach ($imgs as $img):

            if (isset($img['tmp_file'])) {
                $imagem = '../../' . $this->Image->resize('uploads/tmp/' . $img['tmp_file'], 40, 40, array(), null, true);
            } else {
                $imagem = '../../../' . $this->Image->resize($img['dir'] . DS . $img['filename'], 40, 40, array(), null, true);
            }

            if (isset($img['tmp_file'])) {
                $id = $img['tmp_file'];
            } else {
                $id = $img['id'];
            }
            if (isset($img['tmp_file'])) {
                $name = $img['tmp_file'];
            } else {
                $name = $img['filename'];
            }

            $string .= '
			<div class="uploadifyQueue" id="file_uploadQueue"><div class="uploadifyQueueItem">								
				<div class="cancel">
					' . (is_numeric($id) ? '<a href="javascript:;" rel="' . $id . '" class="rm rm-img-old">remover</a>' : '<a href="javascript:;" rel="' . $id . '" class="rm rm-img">remover</a>') . '
				</div>										
			<span class="fileName">
			<label>Ordem <input type="text" class="ordem" value="' . $img['ordem'] . '" rel="' . $id . '" name="data['.$model.'][ordem]" /></label>';
			if($destaque == true){
				$checked = "";
				if($img['destaque'] == 1){
					$checked = "checked='checked'";
				}
				$string .= '<label>Destaque <input type="checkbox" class="destaque" value="'.$img['destaque'].'" rel="'.$id.'" name="data['.$model.'][destaque]" '.$checked.' /></label>';
			}
			$string .= '<img src="' . $imagem . '" alt="Foto" /> ' . $name . '</span>
			<span class="percentage"> - 100%</span>											
			</div>
			</div>';
        endForeach;

        return $string;
    }

    public function admin_ajax_imagens() {

        App::import("helper", "Html");
        $this->Html = new HtmlHelper();
        App::import("helper", "Form");
        $this->Form = new FormHelper();
        App::import("helper", "Image");
        $this->Image = new ImageHelper();

        $string = '';
        foreach ($imgs as $img):

            if (isset($img['tmp_file'])) {
                $imagem = '../../' . $this->Image->resize('uploads/tmp/' . $img['tmp_file'], 40, 40, array(), null, true);
            } else {
                $imagem = '../../../' . $this->Image->resize($img['dir'] . DS . $img['filename'], 40, 40, array(), null, true);
            }

            if (isset($img['tmp_file'])) {
                $id = $img['tmp_file'];
            } else {
                $id = $img['id'];
            }
            if (isset($img['tmp_file'])) {
                $name = $img['tmp_file'];
            } else {
                $name = $img['filename'];
            }

            $string .= '
			<div class="uploadifyQueue" id="file_uploadQueue"><div class="uploadifyQueueItem">								
				<div class="cancel">
					' . (is_numeric($id) ? '<a href="javascript:;" rel="' . $id . '" class="rm rm-img-old">remover</a>' : '<a href="javascript:;" rel="' . $id . '" class="rm rm-img">remover</a>') . '
				</div>										
			<span class="fileName">
			<label>Ordem <input type="text" class="ordem" value="' . $img['ordem'] . '" rel="' . $id . '" name="data[ProdutoImagem][ordem]" /></label>
			<img src="' . $imagem . '" alt="Foto" /> ' . $name . '</span>
			<span class="percentage"> - 100%</span>											
			</div>
			</div>';
        endForeach;

        return $string;
    }
	
	function admin_img_add() {
		
		App::import("helper", "Uploadify");
        $this->Uploadify = new UploadifyHelper();
		
		$path = "uploads/tmp/";
        if (!is_dir($path)) {
            mkdir($path);
        }
		//$this->log(var_export($_FILES, true),'mariafiles');
        $valid_formats = array("jpg", "png", "gif", "bmp");
        if (!empty($_FILES)) {
            $name = $_FILES['Filedata']['name'];
			
            if (strlen($name)) {
                if (preg_match('/(.*)\.([a-z]{3,4})$/i', $name, $flag)) {
                    $ext = $flag[2];
                    if (in_array(low($ext), $valid_formats)) {
                        $size = $this->formatbytes($_FILES['Filedata']['tmp_name']);
                        if ($size < 100) {
                            $actual_image_name = md5(time() . $name) . "." . $ext;
                            $tmp = $_FILES['Filedata']['tmp_name'];

                            if (move_uploaded_file($tmp, $path . '/' . $actual_image_name)) {
                                $_FILES['Filedata']['tmp_file'] = $actual_image_name;
                                $_FILES['Filedata']['ordem'] = '';
								$_FILES['Filedata']['destaque'] = '';
                                //pega as imagens no tmp
                                $imgs = $this->Session->read('TMP.GaleriaFoto');
                                if ($imgs) {
                                    $imgs = am($imgs, array($_FILES['Filedata']));
                                } else {
                                    $imgs = array($_FILES['Filedata']);
                                }
                                $this->Session->write('TMP.GaleriaFoto', $imgs);
								
								echo $this->Uploadify->build($imgs, 'GaleriaFoto', true);
                                exit;
                            } else {
                                echo "Falha ao mover arquivo";
                                exit;
                            }
                        } else {
                            echo "Arquivo muito grande";
                            exit;
                        }
                    } else {
                        echo "Formato de arquivo inválido.";
                        exit;
                    }
                } else {
                    echo "Formato de arquivo inválido.";
                    exit;
                }
            } else {
                echo "por favor selecione uma imagem.!";
                exit;
            }
        } else {
            echo "Falha ao enviar arquivo tente novamente.!";
            exit;
        }
    }

    function admin_img_edit($galeria_id) {
        App::import("helper", "Uploadify");
        $this->Uploadify = new UploadifyHelper();
        $this->loadModel('GaleriaFoto');
        $path = "uploads/tmp/";
        if (!is_dir($path)) {
            mkdir($path);
        }

        $valid_formats = array("jpg", "png", "gif");
        if (!empty($_FILES)) {
            $name = $_FILES['Filedata']['name'];
            if (strlen($name)) {
                if (preg_match('/(.*)\.([a-z]{3,4})$/i', $name, $flag)) {
                    $ext = $flag[2];
                    if (in_array(low($ext), $valid_formats)) {
                        $size = $this->formatbytes($_FILES['Filedata']['tmp_name']);
                        if ($size < 100) {
                            $actual_image_name = md5(time() . $name) . "." . $ext;
                            $tmp = $_FILES['Filedata']['tmp_name'];

                            if (move_uploaded_file($tmp, $path . '/' . $actual_image_name)) {
                                $_FILES['Filedata']['tmp_file'] = $actual_image_name;
                                $_FILES['Filedata']['ordem'] = '';
								$_FILES['Filedata']['destaque'] = '';

                                $this->GaleriaFoto->create();
                                $this->data['GaleriaFoto']['id'] = '';
								$this->data['GaleriaFoto']['galeria_id'] = $galeria_id;
                                $this->data['GaleriaFoto']['dir'] = '';
                                $this->data['GaleriaFoto']['mimetype'] = '';
                                $this->data['GaleriaFoto']['filesize'] = '';
                                $this->data['GaleriaFoto']['ordem'] = 0;
								$this->data['GaleriaFoto']['destaque'] = 0;
                                $this->data['GaleriaFoto']['filename']['type'] = 'image/jpeg';
                                $this->data['GaleriaFoto']['filename']['name'] = $_FILES['Filedata']['tmp_file'];
                                $this->data['GaleriaFoto']['filename']['tmp_name'] = WWW_ROOT . 'uploads' . DS . 'tmp' . DS . $_FILES['Filedata']['tmp_file'];
                                $this->data['GaleriaFoto']['filename']['error'] = $_FILES['Filedata']['error'];
                                $this->data['GaleriaFoto']['filename']['size'] = $_FILES['Filedata']['size'];
                                $this->data['GaleriaFoto']['filename']['created'] = time();
                                $this->data['GaleriaFoto']['filename']['modified'] = time();
                                $this->GaleriaFoto->save($this->data);
                                unlink(WWW_ROOT . 'uploads' . DS . 'tmp' . DS . $_FILES['Filedata']['tmp_file']);
                             
                                $imgs = array();
								$galeria = $this->Galeria->find('first', array('conditions' => array('Galeria.id' => $galeria_id)));
                                foreach ($galeria['GaleriaFoto'] as $img) {
                                    $imgs[] = $img;
                                }
                                echo $this->Uploadify->build($imgs, 'GaleriaFoto', true);
                                exit;
                            } else {
                                echo "Falha ao mover arquivo";
                                exit;
                            }
                        } else {
                            echo "Arquivo muito grande";
                            exit;
                        }
                    } else {
                        echo "Formato de arquivo inválido.";
                        exit;
                    }
                } else {
                    echo "Formato de arquivo inválido.";
                    exit;
                }
            } else {
                echo "por favor selecione uma imagem.!";
                exit;
            }
        } else {
            echo "Falha ao enviar arquivo tente novamente.!";
            exit;
        }
    }

    function admin_rm_img_tmp($remove) {

        $this->layout = false;
        $this->render(false);
        $path = "uploads/tmp/";
        //pega as imagens no tmp
        $olds = $this->Session->read('TMP.GaleriaFoto');
        //se a imagem passada não está no array add a img no tmp
        foreach ($olds as $key => $img) {
            if ($img['tmp_file'] === $remove) {
                $this->Session->delete("TMP.GaleriaFoto.{$key}");
                unlink($path . $img['tmp_file']);
            }
        }
        die(json_encode(true));
    }

    function admin_rm_img_old($id) {
		App::import('Model','GaleriaFoto');
		$this->GaleriaFoto = new GaleriaFoto();
        if (!$id) {
            die(json_encode(false));
        }
        if ($this->Galeria->GaleriaFoto->delete($id)) {
            die(json_encode(true));
        } else {
            die(json_encode(false));
        }
    }

    function admin_img_ordem_old($id, $ordem) {
		App::import('Model','GaleriaFoto');
		$this->GaleriaFoto = new GaleriaFoto();
        if ($id && $ordem) {
            $this->GaleriaFoto->id = $id;
            if ($this->GaleriaFoto->saveField('ordem', $ordem)) {
                die(json_encode(true));
            }
        }
        die(json_encode(false));
    }

    function admin_img_ordem_tmp($id, $ordem) {

        if ($id && $ordem) {
            $tmp = $this->Session->read('TMP.GaleriaFoto');
            //se a imagem passada não está no array add a img no tmp
            foreach ($tmp as $key => $img) {
                if ($img['tmp_file'] === $id) {
                    $this->Session->write("TMP.GaleriaFoto.{$key}.ordem", $ordem);
                }
            }
        }
        die(json_encode(true));
    }
	
	function admin_img_destaque_old($id, $destaque) {
		App::import('Model','GaleriaFoto');
		$this->GaleriaFoto = new GaleriaFoto();
        if ($id && $destaque != "") {
            $this->GaleriaFoto->id = $id;
            if ($this->GaleriaFoto->saveField('destaque', $destaque)) {
                die(json_encode(true));
            }
        }
        die(json_encode(false));
    }

    function admin_img_destaque_tmp($id, $destaque) {

        if ($id && $destaque != "") {
            $tmp = $this->Session->read('TMP.GaleriaFoto');
            //se a imagem passada não está no array add a img no tmp
            foreach ($tmp as $key => $img) {
                if ($img['tmp_file'] === $id) {
                    $this->Session->write("TMP.GaleriaFoto.{$key}.destaque", $destaque);
                }
            }
        }
        die(json_encode(true));
    }
	
}
?>