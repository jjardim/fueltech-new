<?php

class VeiculosController extends AppController {

    var $name = 'Veiculos';
    var $components = array('Session', 'Filter');
    var $helpers = array('Calendario', 'String', 'Image', 'Flash', 'Javascript', 'Youtube', 'Uploadify');

    function detalhe($id = false) {
        App::import('Helper', 'Youtube');

        //id
        if (!$id) {
            $id = $this->params['id'];
        }
        
        //find
        $veiculo = $this->Veiculo->find("first", array(
            "conditions" => array("Veiculo.id" => $id),
            "contain" => array("Galeria" => array("GaleriaFoto"))
                )
        );

        //redirect
        if (!$veiculo) {
            $this->redirect("/");
        }

        //set
        $this->set('veiculo', $veiculo);

        //breadcrumb
        $breadcrumb[] = array('url' => 'javascript:void(0);', 'nome' => __("Suporte", true));
        $this->set('breadcrumb', $breadcrumb);

        //noticias
        $this->set('pagina_atual_tipo', "Mundo FuelTech");
        $this->set('pagina_atual_url', "veiculos-com-fueltech");
    }

    function admin_index() {
        //filters
        $filtros = array();
        if (isset($this->data["Filter"]["filtro"])) {
            $filtros['filtro'] = "Veiculo.nome LIKE '%{%value%}%'";
        }

        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();

        if (isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar") {
            $this->admin_exportar($conditions);
        }

        $this->Veiculo->recursive = 0;
        $this->set('veiculos', $this->paginate($conditions));
    }

    public function admin_exportar($conditions) {

        App::import('Helper', 'Calendario');
        $this->Calendario = new CalendarioHelper();

        $rows = $this->Veiculo->find('all', array('conditions' => $conditions));

        $table = "<table>";
        $table .= "
				<tr bgcolor=\"#CECECE\">
					<td><strong>Id</strong></td>
					<td><strong>Nome</strong></td>
					<td><strong>Descricao</strong></td>
					<td><strong>Status</strong></td>
					<td><strong>Criado</strong></td>
					<td><strong>Modificado</strong></td>
				</tr>";
        foreach ($rows as $row) {
            $status = ( $row['Veiculo']['status'] ) ? "Ativo" : "Inativo";
            $table .= "
				<tr>
					<td>" . $row['Veiculo']['id'] . "</td>
					<td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Veiculo']['nome']) . "</td>
					<td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Veiculo']['descricao']) . "</td>
					<td>" . $status . "</td>
					<td>" . $this->Calendario->DataFormatada("d-m-Y", $row['Veiculo']['created']) . "</td>
					<td>" . $this->Calendario->DataFormatada("d-m-Y", $row['Veiculo']['modified']) . "</td>
				</tr>";
        }
        $table .= "</table>";

        App::import("helper", "String");
        $this->String = new StringHelper();
        $this->layout = false;
        $this->render(false);
        set_time_limit(0);
        header('Content-type: application/x-msexcel');
        $filename = "veiculos_" . date("d_m_Y_H_i_s");
        header('Content-Disposition: attachment; filename=' . $filename . '.xls');
        header('Pragma: no-cache');
        header('Expires: 0');

        die($table);
    }

    // function add() {
    // if (!empty($this->data)) {
    // $this->Veiculo->create();
    // if ($this->Veiculo->save($this->data)) {
    // $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
    // $this->redirect(array('action' => 'index'));
    // } else {
    // $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
    // }
    // }
    // }

    function admin_add() {
        if (!empty($this->data)) {

            $this->Veiculo->create();
	    
	    App::import('Model', 'Galeria');
	    $this->Galeria = new Galeria();
	    $this->Galeria->save(array(
		'galeria_tipo_id' => 1,
		'nome' => $this->data['Veiculo']['nome'],
	    ), false);
	    $this->data['Veiculo']['galeria_id'] = $this->Galeria->id;

            if ($this->Veiculo->save($this->data)) {
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Parâmetro inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            $this->data['Veiculo']['id'] = $id;
            $this->Veiculo->id = $id;

            if ($this->Veiculo->save($this->data)) {

                if (isset($this->data['GaleriaFoto'])) {
                    App::import('Model', 'GaleriaFoto');
                    $this->GaleriaFoto = new GaleriaFoto();
                    foreach ($this->data['GaleriaFoto'] as $k => $fg) {
                        if (isset($fg['delete']) && $fg['delete'] == 1) {
                            $this->GaleriaFoto->delete($fg['id']);
                        }
                    }
                }

                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Veiculo->read(null, $id);
        }

        //galeria fotos
        App::import('Model', 'GaleriaFoto');
        $this->GaleriaFoto = new GaleriaFoto();
	$imgs_old = array();
        $galeria_fotos = $this->GaleriaFoto->find("all", array('recursive' => -1, 'conditions' => array('GaleriaFoto.galeria_id' => $this->data['Veiculo']['galeria_id'])));
        foreach ($galeria_fotos as $img) {
            $imgs_olds[] = $img['GaleriaFoto'];
        }
	$this->set('galeria_id', $this->data['Veiculo']['galeria_id']);
        $this->set('galeria_fotos', $galeria_fotos);
	$this->set('imgs_old', $imgs_olds);
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Veiculo->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
    }

    function admin_rm_img_tmp($remove) {

        $this->layout = false;
        $this->render(false);
        $path = "uploads/tmp/";
        //pega as imagens no tmp
        $olds = $this->Session->read('TMP.GaleriaFoto');
        //se a imagem passada não está no array add a img no tmp
        foreach ($olds as $key => $img) {
            if ($img['tmp_file'] === $remove) {
                $this->Session->delete("TMP.GaleriaFoto.{$key}");
                unlink($path . $img['tmp_file']);
            }
        }
        die(json_encode(true));
    }

    function admin_rm_img_old($id) {
        if (!$id) {
            die(json_encode(false));
        }else{
            $this->GaleriaFoto = new GaleriaFoto();
            $status = false;
            if ($this->GaleriaFoto->delete($id)) {
                $status = true;
            }

            if ($status == true) {
                die(json_encode(true));
            } else {
                die(json_encode(false));
            }
        }
    }

}

?>