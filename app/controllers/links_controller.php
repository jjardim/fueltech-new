<?php
/**
 * Links Controller
 *
 * PHP version 5
 *
 * @category Controller
 * @package  Croogo
 * @version  1.0
 * @author   Fahad Ibnay Heylaal <contact@fahad19.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class LinksController extends AppController {
/**
 * Controller name
 *
 * @var string
 * @access public
 */
    var $name = 'Links';
    var $helpers = array('Html', 'Javascript');

/**
 * Models used by the Controller
 *
 * @var array
 * @access public
 */
    var $uses = array(
        'Link',
        'Grupo',
    );
/**
 * Menu ID
 *
 * holds the current menu ID (if any)
 *
 * @var string
 * @access public
 */
    var $menuId = '';

    function beforeFilter() {
        parent::beforeFilter();

        if (isset($this->params['named']['menu']) && $this->params['named']['menu'] != null) {
            $menu = $this->params['named']['menu'];
            $this->menuId = $menu;
            $this->Link->Behaviors->attach('Tree', array('scope' => array('Link.menu_id' => $this->menuId)));
        } else {
            $menu = '';
            $this->menuId = $menu;
        }
        $this->set(compact('menu'));
    }

    function admin_index() {
        $this->set('title_for_layout', __('Links', true));

        $conditions = array();
        if ($this->menuId != null) {
            $menu = $this->Link->Menu->findById($this->menuId);
            $this->set('title_for_layout', sprintf(__('Links: %s', true), $menu['Menu']['title']));
            $conditions['Link.menu_id'] = $this->menuId;
        }

        $this->Link->recursive = 0;
        $linksTree = $this->Link->generatetreelist($conditions);
        $linksStatus = $this->Link->find('list', array(
            'conditions' => $conditions,
            'fields' => array(
                'Link.id',
                'Link.status',
            ),
        ));
        $this->set(compact('linksTree', 'linksStatus'));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Link->create();
            $this->data['Link']['visibility_grupos'] = $this->Link->encodeData($this->data['Grupo']['Grupo']);
            if ($this->Link->save($this->data)) {
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action'=>'index', 'menu' => $this->menuId));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        $menus = $this->Link->Menu->find('list');
        $grupos = $this->Grupo->find('list',array('fields'=>array('nome')));
        $parentConditions = array();
        if ($this->menuId != null) {
            $parentConditions['Link.menu_id'] = $this->menuId;
        }
        $parentLinks = $this->Link->generatetreelist($parentConditions);
        $this->set(compact('menus', 'grupos', 'parentLinks'));
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Parâmentros inválidos', 'flash/error');
            $this->redirect(array('action'=>'index', 'menu' => $this->menuId));
        }
        if (!empty($this->data)) {
            $this->data['Link']['visibility_grupos'] = $this->Link->encodeData($this->data['Grupo']['Grupo']);
            if ($this->Link->save($this->data)) {
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action'=>'index', 'menu' => $this->menuId));
            } else {
               $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        if (empty($this->data)) {
            $data = $this->Link->read(null, $id);
            $data['Grupo']['Grupo'] = $this->Link->decodeData($data['Link']['visibility_grupos']);
            $this->data = $data;
        }
        $menus = $this->Link->Menu->find('list');
       $grupos = $this->Grupo->find('list',array('fields'=>array('nome')));
        $parentConditions = array();
        if ($this->menuId != null) {
            $parentConditions['Link.menu_id'] = $this->menuId;
        }
        $parentLinks = $this->Link->generatetreelist($parentConditions);
        $this->set(compact('menus', 'grupos', 'parentLinks'));
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash('Parâmentros inválidos', 'flash/error');
            $this->redirect(array('action'=>'index', 'menu' => $this->menuId));
        }
       
        if ($this->Link->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action'=>'index', 'menu' => $this->menuId));
        }
    }

    function admin_moveup($id, $step = 1) {
        if( $this->Link->moveup($id, $step) ) {
            $this->Session->setFlash('Movido para cima com sucesso','flash/success');
        } else {
            $this->Session->setFlash('O registro não pode ser movido, tente novamente', 'flash/error');
        }

        $this->redirect(array('admin' => true, 'action' => 'index', 'menu' => $this->menuId));
    }

    function admin_movedown($id, $step = 1) {
        if( $this->Link->movedown($id, $step) ) {
            $this->Session->setFlash('Movido para baixo com sucesso','flash/success');
        } else {
            $this->Session->setFlash('O registro não pode ser movido, tente novamente', 'flash/error');
        }

        $this->redirect(array('admin' => true, 'action' => 'index', 'menu' => $this->menuId));
    }
}
?>