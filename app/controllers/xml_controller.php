<?php
class XmlController extends AppController {

	var $name = 'Xml';
	var $components = array('Session');
	var $helpers = array('Calendario','String','Flash','Javascript');
	
	function admin_index() {
		$this->Xml->recursive = 0;
		$this->paginate = array('limit' => 1000);
		$this->set('xmls', $this->paginate());
	}

	function admin_add() {
		if (!empty($this->data)) {
		
			$this->Xml->create();
			
			//$this->data['Xml']['produtos_ids'] = json_encode($this->data['Xml']['produtos_ids']);
            
			if ($this->Xml->save($this->data)) {
				$this->admin_gera_xml($this->Xml->id);
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				App::import('model','Produto');
				$this->Produto = new Produto();
				$this->Produto->virtualFields = array("sku_nome"=>"CONCAT(Produto.sku, ' - ' ,Produto.nome)");
				
				if (!empty($this->data['Xml']['produtos_ids'])) {
					$produtos_xml = $this->Produto->find('all',array('fields'=>array('id','sku','nome'),'limit'=>100,'conditions'=>array('Produto.id'=>$this->data['Xml']['produtos_ids'])));
				}
				foreach ($produtos_xml as $allprodutos) {
					$final[$allprodutos['Produto']['id']] = $allprodutos['Produto']['sku'] . ' - ' . $allprodutos['Produto']['nome'];
				}
				$this->data['Xml']['produtos_ids'] = $final;
		
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
		//set XmlTipo
		App::import('Model', 'XmlTipo');
		$this->XmlTipo = new XmlTipo();
        $xml_tipos = array('' => 'Selecione') +$this->XmlTipo->find('list', array('conditions'=>array('XmlTipo.status'=>true),'fields' => array('id', 'nome')));
		$this->set(compact('xml_tipos'));
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash('Parâmetro inválidos','flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			$this->data['Xml']['id'] = $id;
			$this->data['Xml']['produtos_ids'] = json_encode($this->data['Xml']['produtos_ids']);
			$this->Xml->id = $id;
                 
			if ($this->Xml->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Xml->read(null, $id);
		}
		//set XmlTipo
		App::import('Model', 'XmlTipo');
		$this->XmlTipo = new XmlTipo();
        $xml_tipos = array('' => 'Selecione') +$this->XmlTipo->find('list', array('conditions'=>array('XmlTipo.status'=>true),'fields' => array('id', 'nome')));
		$this->set(compact('xml_tipos'));
	}
	function admin_delete($id = null) {
		if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }else
		{
			$xml_url_arquivo = $this->Xml->find('first',array('conditions' => array('Xml.id' => $id )));
		}
        if ($this->Xml->delete($id) && unlink(WWW_ROOT .  'xmls' . DS .$xml_url_arquivo['Xml']['url'] .'.xml') ) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}
	function admin_gera_xml($id = null) {
	
		//Configure::write ('debug', 0);		
		//import helper
		App::import("helper", "Html");
		$this->Html = new HtmlHelper();	
		App::import("helper", "String");
		$this->String = new StringHelper();	
		
		//import Produto
		App::import('Model', 'Produto');
		$this->Produto = new Produto();
		
		//import PagamentoCondicao
		App::import('Model', 'PagamentoCondicao');
		$this->PagamentoCondicao = new PagamentoCondicao();
		
		//import Categoria
		App::import('Model', 'Categoria');
		$this->Categoria = new Categoria();
		
		//Import core file
		App::import('Core','File');
		
		
		$xml = $this->Xml->find('first',array('conditions' => array('Xml.id' => $id)));
		
		$xml_content = "";
		$xml_content .= $xml['Xml']['template_cabecalho'];
		
		//replaces cabecalhos
		$xml_content = str_replace('{{cabecalho_title}}',Configure::read('Loja.nome'),$xml_content);
		$xml_content = str_replace('{{cabecalho_link}}',$this->Html->Url('/',true),$xml_content);
		$xml_content = str_replace('{{cabecalho_descricao}}',Configure::read('Loja.titulo'),$xml_content);
		
		//produtos
        $produtos = $this->Produto->find('all', array('conditions'=>array('Produto.id'=>json_decode($xml['Xml']['produtos_ids']))));
		
		//contador
		$cont = 1;
		
		//coeficiente do juros
		$coef = array(1=>1.00000,2=>0.51875,3=>	0.35007,4=>	0.26575,5=>	0.21518,6=>	0.18148,7=>	0.15743,8=>	0.13941,9=>	0.12540,10=> 0.11420,11=> 0.10505,12=> 0.09743,13=> 0.09099,14=>0.08548,15=>0.08071,16=>0.07654,17=>0.07287,18=>0.06961);
		
		//percorre produtos
		foreach ($produtos as $produto):
		
			//produto marca
			$produto_marca = $produto['Produto']['marca'];
			
			//produto imagem
			if(!empty($produto['ProdutoImagem'][0]['filename'])){
				$produto_imagem='/uploads/produto_imagem/filename/'.$produto['ProdutoImagem'][0]['filename'];
			}else{
				$produto_imagem='/img/site/logo.png';
			}
			
			//produto categoria
			$produto_categoria = "";	
			$seo_url = "";
			foreach ($produto['Categoria'] as $categoria) {
				$categoria_nome = $this->Produto->Categoria->find('first', array('recursive' => -1, 'fields' => 'Categoria.nome', 'conditions' => array('Categoria.id' => $categoria['parent_id'])));

				if ($categoria_nome['Categoria']['nome'] != 'marcas') {
					$seo_url = $categoria['seo_url'];
					break;
				}
			}

			$quebra = explode("/", $seo_url);
			$url_atual = "";
			foreach ($quebra as $key => $cat_url) {
				$url_atual .= $cat_url;

				$categoria_nome = $this->Produto->Categoria->find('first', array('recursive' => -1, 'fields' => 'Categoria.nome', 'conditions' => array('Categoria.seo_url' => $url_atual)));

				$categorias[] = array('url' => 'categorias/' . $url_atual, 'nome' => ucfirst(strtolower($categoria_nome['Categoria']['nome'])));

				$url_atual = $url_atual . "/";
			}
			$first = true;
			foreach($categorias as $categoria){
				if($first){
					$first = false;
				}else{
					$produto_categoria .= " - ";
				}
				$produto_categoria .= $this->String->title_case($categoria['nome']);
			}
			
			//produto preco
			$produto_preco = $produto['Produto']['preco'];
			if($produto['Produto']['preco_promocao']>0){
				$produto_preco = $produto['Produto']['preco_promocao'];
			}
			
			//parcelamento
			$parcelas = $this->PagamentoCondicao->find('first',array('recursive' => -1, 'conditions' => array('PagamentoCondicao.status' => true)));
			if(is_string($produto_preco)){
				$valor=(str_replace(',','.',$produto_preco));
			}
			$juros_apartir = $parcelas['PagamentoCondicao']['juros_apartir'];
			$juros = false;			
			for ($i = 1; $i <= (int)$parcelas['PagamentoCondicao']['parcelas']; $i++):
				$parcela = $valor / $i;			
				if($i>$juros_apartir){
					$adicional = $valor*$coef[$i];
					$parcela =+ $adicional;		
					$juros = true;
				}	
				if ($i == 1 || $parcela > $this->String->moedaToBco(Configure::read('Loja.valor_minimo_parcelamento'))):
					$retorno['valor'] = $parcela;
					$retorno['parcela'] = $i;
					$retorno['juros'] = $juros;
				endIf;
				
			endFor;
			
			$parcelas = $retorno['parcela'];
			$parcelado = $retorno['valor'];
			if($retorno['juros'])
				$parcelamento_juros = " com juros";
			else
				$parcelamento_juros = " sem juros";
			
			$valor_parcelado_br = number_format($parcelado, 2, ",", " ");
			$valor_parcelado_us = number_format($parcelado, 2, ".", " ");
			
			$produto_preco_br = number_format($produto_preco, 2, ",", " ");
			$produto_preco_us = number_format($produto_preco, 2, ".", " ");
			
			$produto_preco_promocao_br = number_format($produto['Produto']['preco_promocao'], 2, ",", " ");
			$produto_preco_promocao_us = number_format($produto['Produto']['preco_promocao'], 2, ".", " ");
			
			//produto nome
			$produto_nome = strip_tags(html_entity_decode($produto['Produto']['nome']));
			
			//produto descricao
			$produto_descricao = strip_tags($this->CoverteTexto($produto['Produto']['descricao']));
			
			//produto descricao resumida
			$produto_descricao_resumida = strip_tags($this->CoverteTexto($produto['Produto']['descricao_resumida']));
			
			//valida disponibidade
			if(!empty($produto['Produto']['quantidade']) && $produto['Produto']['quantidade']>0){
				$availability  = "in stock";
			}else{
				$availability  ='out of stock';
			}
			
			//replaces
			$xml_bloco = $xml['Xml']['template_centro'];
			
			//google shooppin
			$xml_bloco = str_replace('{{content_g:image_link}}',$this->Html->Url((($produto_imagem)),true),$xml_bloco);
			$xml_bloco = str_replace('{{content_g:price_[00,00]}}',$produto_preco_br,$xml_bloco);
			$xml_bloco = str_replace('{{content_g:price_[00.00]}}',$produto_preco_us,$xml_bloco);
			$xml_bloco = str_replace('{{content_g:condition}}',"new",$xml_bloco);
			$xml_bloco = str_replace('{{content_g:id}}',$produto['Produto']['id'],$xml_bloco);
			$xml_bloco = str_replace('{{content_g:brand}}',$produto_categoria,$xml_bloco);
			$xml_bloco = str_replace('{{content_g:product_type}}',$produto_categoria,$xml_bloco);
			$xml_bloco = str_replace('{{content_g:google_product_category}}',Configure::read('Loja.seo_meta_keywords'),$xml_bloco);
			$xml_bloco = str_replace('{{content_g:availability}}',$availability,$xml_bloco);
			
			//demais
			$xml_bloco = str_replace('{{content_contador}}',$cont,$xml_bloco);
			$xml_bloco = str_replace('{{produto_nome}}',$produto_nome,$xml_bloco);
			$xml_bloco = str_replace('{{produto_link}}',$this->Html->Url("/" . low(Inflector::slug($produto_nome, '-')) . '-prod-' . $produto['Produto']['id'],true).'.html',$xml_bloco);
			$xml_bloco = str_replace('{{produto_descricao}}',$produto_descricao,$xml_bloco);
			$xml_bloco = str_replace('{{produto_descricao_resumida}}',$produto_descricao_resumida,$xml_bloco);
			$xml_bloco = str_replace('{{produto_id}}',$produto['Produto']['id'],$xml_bloco);
			$xml_bloco = str_replace('{{produto_sku}}',$produto['Produto']['sku'],$xml_bloco);
			$xml_bloco = str_replace('{{produto_preco_[00,00]}}',$produto_preco_br,$xml_bloco);
			$xml_bloco = str_replace('{{produto_preco_[00.00]}}',$produto_preco_us,$xml_bloco);
			$xml_bloco = str_replace('{{produto_preco_promocao_[00,00]}}',$produto_preco_promocao_br,$xml_bloco);
			$xml_bloco = str_replace('{{produto_preco_promocao_[00.00]}}',$produto_preco_promocao_us,$xml_bloco);
			$xml_bloco = str_replace('{{produto_parcelamento_parcelas}}',$parcelas,$xml_bloco);
			$xml_bloco = str_replace('{{produto_parcelamento_juros}}',$parcelamento_juros,$xml_bloco);
			$xml_bloco = str_replace('{{produto_parcelamento_valor_[00.00]}}',$valor_parcelado_us,$xml_bloco);
			$xml_bloco = str_replace('{{produto_parcelamento_valor_[00,00]}}',$valor_parcelado_br,$xml_bloco);
			$xml_bloco = str_replace('{{produto_parcelamento_parcela}}',$parcelas,$xml_bloco);
			$xml_bloco = str_replace('{{produto_disponibilidade}}',($produto['Produto']['quantidade_disponivel']> 0 )? 'Entrega imediata':'Indisponível',$xml_bloco);
			$xml_bloco = str_replace('{{produto_imagem}}',$this->Html->Url((($produto_imagem)),true),$xml_bloco);
			$xml_bloco = str_replace('{{produto_categoria}}',$produto_categoria,$xml_bloco);			
			$xml_bloco = str_replace('{{produto_marca}}',$produto_marca,$xml_bloco);
			
			$xml_content .= $xml_bloco;
			
			$cont++;
			
		endforeach;
		
		$xml_content .= $xml['Xml']['template_rodape'];
		
		$xmlFile = new File("xmls/".$xml['Xml']['url'].".xml");
		$xmlFile->write($xml_content, 'w');
		$xmlFile->close();

	}
	
	private function CoverteTexto($str){
	
		setlocale(LC_ALL, 'pt_BR.UTF-8');

		$codigo_acentos = array(
		'/&Agrave;/','/&Aacute;/','/&Acirc;/','/&Atilde;/','/&Auml;/','/&Aring;/',
		'/&agrave;/','/&aacute;/','/&acirc;/','/&atilde;/','/&auml;/','/&aring;/',
		'/&Ccedil;/',
		'/&ccedil;/',
		'/&Egrave;/','/&Eacute;/','/&Ecirc;/','/&Euml;/',
		'/&egrave;/','/&eacute;/','/&ecirc;/','/&euml;/',
		'/&Igrave;/','/&Iacute;/','/&Icirc;/','/&Iuml;/',
		'/&igrave;/','/&iacute;/','/&icirc;/','/&iuml;/',
		'/&Ntilde;/',
		'/&ntilde;/',
		'/&Ograve;/','/&Oacute;/','/&Ocirc;/','/&Otilde;/','/&Ouml;/',
		'/&ograve;/','/&oacute;/','/&ocirc;/','/&otilde;/','/&ouml;/',
		'/&Ugrave;/','/&Uacute;/','/&Ucirc;/','/&Uuml;/',
		'/&ugrave;/','/&uacute;/','/&ucirc;/','/&uuml;/',
		'/&Yacute;/',
		'/&yacute;/','/&yuml;/',
		'/&ordf;/',
		'/&ordm;/',
		'/&quot;/');

		$acentos = array(
		'À','Á','Â','Ã','Ä','Å',
		'à','á','â','ã','ä','å',
		'Ç',
		'ç',
		'È','É','Ê','Ë',
		'è','é','ê','ë',
		'Ì','Í','Î','Ï',
		'ì','í','î','ï',
		'Ñ',
		'ñ',
		'Ò','Ó','Ô','Õ','Ö',
		'ò','ó','ô','õ','ö',
		'Ù','Ú','Û','Ü',
		'ù','ú','û','ü',
		'Ý',
		'ý','ÿ',
		'ª',
		'º',
		"'");
		
		return utf8_encode(utf8_decode(preg_replace($codigo_acentos, $acentos, $str)));
	}
}
?>