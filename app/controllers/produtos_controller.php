<?php

class ProdutosController extends AppController {

    var $components = array('RequestHandler', 'Filter', 'Email', 'Session');
    var $helpers = array('Parcelamento', 'String', 'Image', 'Javascript', 'Calendario', 'Uploadify');
    var $uses = array('Produto', 'Avise', 'ProdutoRating');

    function index2() {
		$joins = array(
			array(
			'table' => 'produtos_categorias',
			'alias' => 'Categorias',
			'type' => 'left',
			'conditions' =>
			array(
				'Categorias.produto_id = Produto.id'
			)
			), array(
			'table' => 'categorias',
			'alias' => 'Categoria',
			'type' => 'left',
			'conditions' =>
			array(
				'Categorias.categoria_id = Categoria.id'
			)
			), array(
			'table' => 'produto_descricoes',
			'alias' => 'ProdutoDescricao',
			'type' => 'left',
			'conditions' => array(
				'ProdutoDescricao.produto_id = Produto.id'
			)
			)
		);

		$conditions = array('Produto.status >' => 0, 'ProdutoDescricao.language' => Configure::read('Config.language'));
		$this->paginate = array(
			'limit' => 12,
			
			'order' => array('rating' => 'DESC', 'quantidade_disponivel' => 'DESC', 'quantidade_acessos' => 'DESC'),
			'joins' => $joins,
			'contain' => array('ProdutoDescricao'),
			'fields' => array('(SELECT IF(FreteGratis.label != "",FreteGratis.label,"") FROM frete_gratis FreteGratis WHERE (FreteGratis.categoria_id LIKE concat("%\"",Categorias.categoria_id,"\"%") OR FreteGratis.produto_id LIKE concat("%\"",Categorias.produto_id,"\"%") ) AND ( FreteGratis.status = true AND IF(Produto.preco_promocao > 0,Produto.preco_promocao,Produto.preco) >= FreteGratis.apartir_de AND FreteGratis.data_inicio <= now() AND FreteGratis.data_fim >= now() ) LIMIT 1) as frete_gratis,Produto.id,ProdutoDescricao.nome,Produto.preco,Produto.preco_promocao,Produto.quantidade_disponivel,Produto.status,Produto.preco_promocao_inicio,Produto.preco_promocao_fim,Produto.preco_promocao_novo,Produto.preco_promocao_velho, Produto.status,Categoria.nome,Categoria.seo_url ,(SELECT concat("uploads/produto_imagem/filename/",ProdutoImagem.filename) FROM produto_imagens as ProdutoImagem WHERE ProdutoImagem.produto_id = Produto.id LIMIT 1) as imagem,(MATCH (Produto.nome,Produto.tag) AGAINST ("' . addslashes($this->params['url']['busca']) . '" IN BOOLEAN MODE)) AS rating')
		);
		
		$this->set('produtos', $this->paginate('Produto', $conditions));

		//breadcrumb
		$breadcrumb[] = array('url' => 'javascript:void(0);', 'nome' => __("Produtos", true));
		$this->set('breadcrumb', $breadcrumb);
    }

    function detalhe($id = false) {

		App::import('Helper', 'String');
		$this->String = new StringHelper();

		//Salva registro de avise-me estoque produto
		if (!empty($this->data) && isset($this->data['Avise']['nome'])) {
			$this->Avise->create();
			if ($this->Avise->save($this->data)) {
			$this->emailAviseMe($this->data);
			$this->data = array();
			$this->Session->setFlash('Em breve entraremos em contato, para notificar a disponibilidade do produto.', 'flash/success');
			} else {
			$this->Session->setFlash('Verifique os campos em destaque  e tente novamente.', 'flash/error');
			}
		} elseif (!empty($this->data) && isset($this->data['ProdutoRating']['produto_id'])) {
			if (!empty($this->data)) {
			if ($this->ProdutoRating->save($this->data)) {
				$this->data = array();
				$this->Session->setFlash('Classificação enviada e aguardando a aprovação.', 'flash/success');
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
			}
		}

		if (!$id) {
			$id = $this->params['id'];
		}
		
		$prd_kit_validate = $this->Produto->find('first', array('fields' => array('Produto.kit'), 'conditions' => array('Produto.id' => $id)));
		if($prd_kit_validate && $prd_kit_validate['Produto']['kit'] == true){
			$this->loadModel('ProdutoKit');
			$this->ProdutoKit->atualiza_soma_valores($id, null, false);
			$this->ProdutoKit->valida_estoque($id);
		}

		$produto = $this->Produto->find(
			'first', array(
			'fields' => array('Produto.*', 'ProdutoDescricao.*'),
			'contain' => array(
			'VariacaoProduto' => array('Variacao' => array('VariacaoTipo')),
			'Categoria' => array('CategoriaDescricao'), 'ProdutoImagem', 'ProdutoPreco', 'ProdutoDownload' => array('Download')),
			'joins' => array(
			array(
				'table' => 'produto_descricoes',
				'alias' => 'ProdutoDescricao',
				'type' => 'LEFT',
				'conditions' => 'ProdutoDescricao.produto_id = Produto.id'
			)
			),
			'conditions' => array(
			'AND' => array('Produto.preco >' => 0, 'Produto.status >' => 0, 'Produto.id' => $id, 'ProdutoDescricao.language' => Configure::read('Config.language'))
			)
			)
		);



//        var_dump($produto);die;

		/* ordenando imagens */
		if (isset($produto['ProdutoImagem']) && count($produto['ProdutoImagem']) > 0) {
			foreach($produto['ProdutoImagem'] as $key => $row) {
				$imagem[$key] = $row['ordem'];
			}
			array_multisort($imagem, SORT_NUMERIC, $produto['ProdutoImagem']);
		}

		if (!isset($produto)) {
			$this->redirect("/");
		}

		$agrupador = $produto['Produto']['agrupador'];
		if ($agrupador) {

			$this->set('produto_agrupador', $agrupador);

			$agrupados = $this->Produto->VariacaoProduto->find(
				'all', array(			
			'order' => array('LENGTH(Variacao.valor) DESC, Variacao.valor DESC'),
			'fields' => array('Produto.status, Produto.nome,Produto.quantidade_disponivel,Produto.id,VariacaoProduto.*,Variacao.*, VariacaoDescricao.*'),
			'contain' => array('Variacao' => array('VariacaoTipo')),
			'joins' => array(
				array(
				'table' => 'produtos',
				'alias' => 'Produto',
				'type' => 'LEFT',
				'conditions' => array('VariacaoProduto.produto_id = Produto.id')
				),
				array(
				'table' => 'variacao_descricoes',
				'alias' => 'VariacaoDescricao',
				'type' => 'LEFT',
				'conditions' => array('VariacaoDescricao.variacao_id = VariacaoProduto.variacao_id')
				)
			),
			'conditions' => array('Produto.status >' => 0, 'VariacaoProduto.agrupador' => $agrupador)
				)
			);
			$this->log(var_export($agrupados,true),'texte');
			///$agrupados = Set::sort($agrupados, '{n}.Variacao.VariacaoTipo.ordem', 'asc');

			$variacoes = array();
			$correcao = array();

			foreach ($agrupados as $valor):
				$variacoes[$valor['Variacao']['VariacaoTipo']['nome']][$valor['VariacaoDescricao']['valor']] = $valor['VariacaoDescricao']['valor'];
			endforeach;

			foreach ($agrupados as $valor):
				$correcao[$valor['Produto']['id']]['Produto'] = $valor['Produto'];
				$correcao[$valor['Produto']['id']]['VariacaoDescricao'] = $valor['VariacaoDescricao'];
				//$valor['Variacao']['valor'] = $valor['VariacaoDescricao']['valor'];
				//$valor['Variacao']['label'] = $valor['VariacaoDescricao']['label'];
				$correcao[$valor['Produto']['id']]['Variacao'][$valor['Variacao']['VariacaoTipo']['nome']]['Variacao'] = $valor['Variacao'];
				$correcao[$valor['Produto']['id']]['Variacao'][$valor['Variacao']['VariacaoTipo']['nome']]['VariacaoDescricao'] = $valor['VariacaoDescricao'];
			endforeach;
			
			$grade = array();
			$full = null;
			$anterior_tipo = null;
			$anterior_tam = null;
			foreach ($variacoes as $tipo => $valor) {
			if ($tipo != $anterior_tipo && $anterior_tipo != null) {
				foreach ($grade as $grade_c => &$grade_v) {
					foreach ($grade_v as $grade_t => $grade_cor) {
						foreach ($correcao as $prod) {
							foreach ($prod['Variacao'] as $b => $variacao) {
								if ($variacao['Variacao']['valor'] == $grade_t) {
									$grade[$grade_c][$grade_t]['tamanhos'][$prod['Produto']['id']] = $prod['Variacao'][$tipo];
									$grade[$grade_c][$grade_t]['tamanhos'][$prod['Produto']['id']]['Produto'] = $prod['Produto'];
									$grade[$grade_c][$grade_t]['Data']['tipo_variacao'] = $tipo;
								}
							}
						}
					}
				}
			} else {
				foreach ($valor as $cor) {
					foreach ($correcao as $prod) {
						if ($prod['Variacao'][$tipo]['Variacao']['valor'] == $cor || $prod['Variacao'][$tipo]['VariacaoDescricao']['valor'] == $cor) {
							//$grade[$tipo][$cor]['Produtos'][] = $prod['Produto']['id'];
							$grade[$tipo][$cor]['Produtos'][] = $prod['Produto'];
							$grade[$tipo][$cor]['Data'] = $prod['Variacao'][$tipo]['Variacao'];
						}
					}
				}
			}
			$anterior_tipo = $tipo;
			}

			//$this->arraySortRecursive($grade);
			$this->set('variacoes', $grade);
		} else {
			$this->set('variacoes', array());
		}

		$this->set('produto', $produto);

		//begin historico de navegação
		//salvo a visita do cliente, e salvo na sessao
		$historico = $this->Session->read("Historico.navegacao");
		$sessao_atual = array('id' => $produto['Produto']['id'], 'nome' => $produto['Produto']['nome']);
		if (isset($historico['Produtos'])) {
			foreach ($historico['Produtos'] as $hp) {
			if ($hp['id'] != $produto['Produto']['id']) {
				$historico_tmp[] = $hp;
			}
			}

			$historico_tmp[] = $sessao_atual;
		} else {
			$historico_tmp = array($sessao_atual);
		}
		$this->Session->write("Historico.navegacao.Produtos", $historico_tmp);
		//end historico de navegação
		//start breadcrumb
		$breadcrumb = array();
		$seo_url = "";

		foreach ($produto['Categoria'] as $categoria) {
			$categoria_nome = $this->Produto->Categoria->find('first', array('recursive' => -1, 'fields' => 'Categoria.nome', 'conditions' => array('Categoria.id' => $categoria['parent_id'])));

			if ($categoria_nome['Categoria']['nome'] != 'marcas') {
			$seo_url = $categoria['seo_url'];
			break;
			}
		}
		$quebra = explode("/", $seo_url);

		$url_atual = "";
		foreach ($quebra as $key => $cat_url) {
			$url_atual .= $cat_url;

			$categoria_nome = $this->Produto->Categoria->find('first', array(
			'recursive' => -1,
			'fields' => 'CategoriaDescricao.nome',
			'joins' => array(
				array(
				'table' => 'categoria_descricoes',
				'alias' => 'CategoriaDescricao',
				'type' => 'LEFT',
				'conditions' => 'CategoriaDescricao.categoria_id = Categoria.id'
				)
			),
			'conditions' => array('Categoria.seo_url' => $url_atual, 'CategoriaDescricao.language' => Configure::read('Config.language'))
				)
			);

			$breadcrumb[] = array('url' => 'categorias/' . $url_atual, 'nome' => $this->String->title_case($categoria_nome['CategoriaDescricao']['nome']));

			$url_atual = $url_atual . "/";
		}
		$breadcrumb[] = array('url' => 'javascript:void(0);', 'nome' => $produto['ProdutoDescricao']['nome']);
		$this->set('breadcrumb', $breadcrumb);
		//end breadcrumb
		//begin quantidade de acessos
		//salvo o acesso do produto.
		$this->Produto->id = $id;
		$this->Produto->saveField('quantidade_acessos', $produto['Produto']['quantidade_acessos'] + 1);
		//end quantidade de acessos
		//begin produtos_relacionados
		if ($ids = json_decode($produto['Produto']['relacionados'])) {
			$produtos_relacionados = $this->Produto->find('all', array(
			'fields' => array('Produto.nome, Produto.status, Produto.quantidade_disponivel, Produto.preco, Produto.preco_promocao, Produto.preco_promocao_inicio, 
																			Produto.preco_promocao_inicio, Produto.preco_promocao_fim, Produto.preco_promocao_novo, Produto.preco_promocao_velho,Produto.marca,
																			(SELECT concat("uploads/produto_imagem/filename/",ProdutoImagem.filename) FROM produto_imagens ProdutoImagem WHERE ProdutoImagem.produto_id = Produto.id LIMIT 1) as imagem,
																			(SELECT IF(FreteGratis.label != "",FreteGratis.label,"") FROM frete_gratis FreteGratis WHERE (FreteGratis.categoria_id LIKE concat("%\"",ProdutoCategoria.categoria_id,"\"%") OR FreteGratis.produto_id LIKE concat("%\"",ProdutoCategoria.produto_id,"\"%") ) AND ( FreteGratis.status = true AND IF(Produto.preco_promocao > 0,Produto.preco_promocao,Produto.preco) >= FreteGratis.apartir_de ) LIMIT 1) as frete_gratis
																			'),
			'joins' => array(
				array('table' => 'produtos_categorias', 'alias' => 'ProdutoCategoria', 'type' => 'LEFT',
				'conditions' => array('ProdutoCategoria.produto_id = Produto.id'))),
			'conditions' => array('Produto.status >' => 0, 'Produto.id' => $ids, 'Produto.parent_id' => 0),
			'limit' => 5,
			'group' => 'Produto.id')
			);
		} else {
			$produtos_relacionados = $this->Produto->find('all', array(
			'fields' => array('Produto.nome, Produto.status, Produto.quantidade_disponivel, Produto.preco, Produto.preco_promocao, Produto.preco_promocao_inicio, 
																			Produto.preco_promocao_inicio, Produto.preco_promocao_fim, Produto.preco_promocao_novo, Produto.preco_promocao_velho,Produto.marca,
																			(SELECT concat("uploads/produto_imagem/filename/",ProdutoImagem.filename) FROM produto_imagens ProdutoImagem WHERE ProdutoImagem.produto_id = Produto.id LIMIT 1) as imagem,
																			(SELECT IF(FreteGratis.label != "",FreteGratis.label,"") FROM frete_gratis FreteGratis WHERE (FreteGratis.categoria_id LIKE concat("%\"",ProdutoCategoria.categoria_id,"\"%") OR FreteGratis.produto_id LIKE concat("%\"",ProdutoCategoria.produto_id,"\"%") ) AND ( FreteGratis.status = true AND IF(Produto.preco_promocao > 0,Produto.preco_promocao,Produto.preco) >= FreteGratis.apartir_de ) LIMIT 1) as frete_gratis
																			'),
			'joins' => array(
				array('table' => 'produtos_categorias', 'alias' => 'ProdutoCategoria', 'type' => 'LEFT',
				'conditions' => array('ProdutoCategoria.produto_id = Produto.id'))),
			'conditions' => array('Produto.status >' => 0, 'Produto.parent_id' => 0, 'Produto.id !=' => $id, 'ProdutoCategoria.categoria_id' => set::extract('{n}.id', $produto['Categoria'])),
			'limit' => 5)
			);
		}
		$this->set('produtos_relacionados', $produtos_relacionados);
		//end produtos_relacionados
		//begin produtos_mais_vendidos
		$mais_vendidos = $this->Produto->find('all', array(
			'fields' => array('Produto.id,Produto.status, Produto.nome,Produto.preco,Produto.preco_promocao,Produto.quantidade_disponivel,Produto.preco_promocao_inicio,Produto.preco_promocao_fim,Produto.preco_promocao_novo,Produto.preco_promocao_velho,
									(SELECT concat("uploads/produto_imagem/filename/",ProdutoImagem.filename) FROM produto_imagens ProdutoImagem WHERE ProdutoImagem.produto_id = Produto.id LIMIT 1) as imagem'),
			'joins' => array(
			array('table' => 'produtos_categorias', 'alias' => 'ProdutoCategoria', 'type' => 'LEFT',
				'conditions' => array('ProdutoCategoria.produto_id = Produto.id'))),
			'conditions' => array('Produto.parent_id' => 0, 'Produto.status >' => 0, 'Produto.id !=' => $id, 'ProdutoCategoria.categoria_id' => set::extract('{n}.id', $produto['Categoria'])),
			'recursive' => -1,
			'limit' => 4,
			'order' => 'Produto.quantidade_vendidos DESC'
			)
		);
		$this->set('mais_vendidos', $mais_vendidos);
		//end produtos_mais_vendidos
		//begin produto_ratings
		$this->loadModel('ProdutoRating');
		$this->ProdutoRating = new ProdutoRating();
		$produto_ratings = $this->ProdutoRating->find('all', array('conditions' => array('ProdutoRating.produto_id' => $id, 'ProdutoRating.status' => '1'), 'recursive' - 1));
		//consulto a media de notas do produto
		$produto_media_ratings = $this->ProdutoRating->returnaRatingsProduto($id);
		//ser variaveis		
		$this->set(compact('produto_ratings', 'produto_media_ratings'));
		//end produto_ratings
		//begin Tags SEO
		if ($produto['ProdutoDescricao']['seo_title']) {
			$this->set('seo_title', $produto['ProdutoDescricao']['seo_title']);
		} else {
			$this->set('title_for_layout_produto', $produto['Produto']['nome'] . ' - ' . Configure::read('Loja.titulo'));
		}
		if ($produto['ProdutoDescricao']['seo_meta_description']) {
			$this->set('seo_meta_description',  $this->String->cropSentence($produto['ProdutoDescricao']['seo_meta_description'],156,''));
			///$this->set('seo_meta_description', $produto['ProdutoDescricao']['seo_meta_description']);
		} elseif ($produto['Categoria'][0]['seo_meta_description']) {
			$this->set('seo_meta_description',  $this->String->cropSentence($produto['Categoria'][0]['seo_meta_description'],156,''));
			///$this->set('seo_meta_description', $produto['Categoria'][0]['seo_meta_description']);
		} else {
			$this->set('seo_meta_description',  $this->String->cropSentence(Configure::read('Loja.seo_meta_description'),156,''));
			////$this->set('seo_meta_description', Configure::read('Loja.seo_meta_description'));
		}        
		
		if ($produto['ProdutoDescricao']['seo_meta_keywords']) {
			$this->set('seo_meta_keywords', $produto['ProdutoDescricao']['seo_meta_keywords']);
		} elseif ($produto['Categoria'][0]['seo_meta_keywords']) {
			$this->set('seo_meta_keywords', $produto['Categoria'][0]['seo_meta_keywords']);
		} else {
			$this->set('seo_meta_keywords', Configure::read('Loja.seo_meta_keywords'));
		}
		if ($produto['ProdutoDescricao']['seo_institucional']) {
			$this->set('seo_institucional', $produto['ProdutoDescricao']['seo_institucional']);
		} elseif ($produto['Categoria'][0]['seo_institucional']) {
			$this->set('seo_institucional', $produto['Categoria'][0]['seo_institucional']);
		} else {
			$this->set('seo_institucional', Configure::read('Loja.seo_institucional'));
		}
		//end Tags SEO
		//Salva registro de produto acessado
		//$this->ProdutosAcessados->add($id);
		//Set produtos visualizados
		//$this->set('produtos_visualizados', $this->Produto->find('all', array('contain'=>array('Categoria'),'fields'=>array('*,(SELECT concat("uploads/produto_imagem/filename/",ProdutoImagem.filename) FROM produto_imagens ProdutoImagem WHERE ProdutoImagem.produto_id = Produto.id LIMIT 1) as imagem'),'limit' => 3,'conditions' => array('Produto.status >' => 0, 'Produto.id' =>$this->ProdutosAcessados->getAll()))));
    }

    function admin_index() {

		$filtros = array();
		// if (isset($this->data["Filter"]["categorias_id"])) {
		// $filtros['categoria_id'] = "Categorias.categoria_id = '{%value%})'";
		// }

		if ($this->data["Filter"]["filtro"]) {
			$filtros['filtro'] = "Produto.nome LIKE '%{%value%}%' OR Produto.sku LIKE '%{%value%}%' ";
		}

		if ($this->data["Filter"]["status"] != "") {
			$filtros['status'] = "Produto.status = '{%value%}'";
		}

		$this->Filter->setConditions($filtros);
		$this->Filter->check();
		$conditions = $this->Filter->getFilters();
		$this->Filter->setDataToView();

		if (isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar") {
			$this->admin_exportar($conditions);
		}

		$this->paginate = array(
			'contain' => array('ProdutoImagem', 'Categoria'),
			'recursive' => 1,
			'limit' => 24
		);

		$produtos = $this->paginate('Produto', $conditions);
		//debug($conditions);die;
		//set produtos
		$this->set('produtos', $produtos);

		//set categorias
		App::import('Model', 'Categoria');
		$this->Categoria = new Categoria();
		//$categorias = array('' => 'Selecione') +$this->Produto->Categoria->find('list', array('conditions'=>array('status'=>true),'fields' => array('id', 'descricao')));
		$categorias = array('' => 'Selecione') + $this->Categoria->generatetreelist(null, '{n}.Categoria.id', '{n}.Categoria.descricao', '--');
		$this->set(compact('categorias'));
		}

	function admin_add() {

		App::import('Model','ProdutoKit');
		$this->ProdutoKit = new ProdutoKit();

		if (!empty($this->data)) {
		
			$this->Produto->create();
			$this->Produto->unbindModel(array('hasMany' => array('ProdutoImagem')));
			
			//quantidade disponnivel
			if (isset($this->data['Produto']['quantidade']) && $this->data['Produto']['quantidade'] != "") {
				$this->data['Produto']['quantidade_disponivel'] = $this->data['Produto']['quantidade'];
			}
			if (isset($this->data['Produto']['parent_id']) && $this->data['Produto']['parent_id'] > 0) {
				$produto_pai = $this->Produto->find('first', array('recursive' => -1, 'conditions' => array('Produto.id' => $this->data['Produto']['parent_id']), 'fields' => array('Produto.agrupador')));
				$this->data['Produto']['agrupador'] = $produto_pai['Produto']['agrupador'];
			} else {
				$this->data['Produto']['parent_id'] = 0;
				$this->data['Produto']['agrupador'] = $this->data['Produto']['sku'];
			}
			if (isset($this->data['ProdutoDescricao'][0]['nome'])) {
				$this->data['Produto']['nome'] = $this->data['ProdutoDescricao'][0]['nome'];
			}


			$this->Produto->unbindModel(array('hasMany' => array('ProdutoKit')));
			//trata os valores do kit do produto
			if($this->data['Produto']['kit'] == true){
				$this->ProdutoKit->soma_valores($this->data);
			}
			
			//begin gamba produto kit (unbind não funcionou)
			if(isset($this->data['ProdutoKit'])){
				$data_produto_kit = $this->data['ProdutoKit'];
				unset($this->data['ProdutoKit']);
			}
			//end gamba produto kit  (unbind não funcionou)
            $this->data['Produto']['preco'] = number_format(($this->data['Produto']['preco'] / 100), 2, ',', '.');


			if ($this->Produto->saveAll($this->data)) {

				//begin gamba produto kit (unbind não funcionou)
				if(isset($data_produto_kit)){
					$this->data['ProdutoKit'] = $data_produto_kit;
				}
				//end gamba produto kit (unbind não funcionou)
			
				//salva kit do produto
				if($this->data['Produto']['kit'] == true){
					$this->ProdutoKit->salvar($this->data, $this->Produto->id);
					$this->ProdutoKit->valida_estoque($this->Produto->id);
				}else{
					//não tem estoque? desativa KIT se houver, senão ativa
					//dois metodos, para evitar finds desnecessários em ambas ações
					if($this->data['Produto']['status'] != 1 ||  $this->data['Produto']['quantidade'] <= 0 ){
						$this->ProdutoKit->desativa_kit_sem_estoque($this->Produto->id);
					}else{
						$this->ProdutoKit->ativa_kit_com_estoque($this->Produto->id);
					}
				}


				App::import("Model", "ProdutoImagem");
				$this->ProdutoImagem = new ProdutoImagem();
				$this->Produto->bindModel(array('hasMany' => array('ProdutoImagem')));
				$this->ProdutoImagem->create();
				foreach ($this->Session->read('TMP.ProdutoImagem') as $img):
					$this->data['ProdutoImagem']['produto_id'] = $this->Produto->id;
					$this->data['ProdutoImagem']['id'] = '';
					$this->data['ProdutoImagem']['dir'] = '';
					$this->data['ProdutoImagem']['mimetype'] = '';
					$this->data['ProdutoImagem']['filesize'] = '';
					$this->data['ProdutoImagem']['ordem'] = $img['ordem'];
					$this->data['ProdutoImagem']['filename']['type'] = 'image/jpeg';
					$this->data['ProdutoImagem']['filename']['name'] = $img['tmp_file'];
					$this->data['ProdutoImagem']['filename']['tmp_name'] = WWW_ROOT . 'uploads' . DS . 'tmp' . DS . $img['tmp_file'];
					$this->data['ProdutoImagem']['filename']['error'] = $img['error'];
					$this->data['ProdutoImagem']['filename']['size'] = $img['size'];
					$this->data['ProdutoImagem']['filename']['created'] = time();
					$this->data['ProdutoImagem']['filename']['modified'] = time();
					$this->ProdutoImagem->save($this->data);

					unlink(WWW_ROOT . 'uploads' . DS . 'tmp' . DS . $img['tmp_file']);
				endForeach;
				$this->Session->delete('TMP.ProdutoImagem');

				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
			
			if (!empty($this->data['Produto']['Relacionados'])) {
				$this->data['Produto']['Relacionados'] = $this->Produto->find('list', array('fields' => array('id', 'nome'), 'limit' => 100, 'conditions' => array('Produto.id' => $this->data['Produto']['Relacionados'])));
			}

		}
		
		//begin kit
		$kit_sel = array();
		if (count($this->data['ProdutoKit']) > 0):
			foreach ($this->data['ProdutoKit'] as $c => $v) {
				
				$prd =  $this->Produto->find('first', array(
															'recursive' 	=> -1, 
															'conditions' 	=> array('Produto.id' => $v['produto_id']), 
															'fields' 		=> array('Produto.id', 'Produto.sku', 'Produto.nome', 'Produto.preco_promocao_inicio', 'Produto.preco_promocao_fim', 'Produto.preco_promocao_novo')
														)
											);
			
				//$this->data['ProdutoKit'][$c]['produto_id'] = array($v['produto_id'] => $prd['Produto']['sku'].' - '.$prd['Produto']['nome']);
				$kit_sel[$c] = array($v['produto_id'] => $prd['Produto']['sku'].' - '.$prd['Produto']['nome']);
			}
		endIf;
		//seto o sel de atributos
		$this->set('kit_sel', $kit_sel);
		//end kit

		$otemp = $this->Produto->find('all', array('recursive' => -1));
		$parents = array('' => 'Selecione');
		foreach ($otemp as $prods) {
			$parents[$prods['Produto']['id']] = $prods['Produto']['codigo'] . ' - ' . $prods['Produto']['nome'];
		}

		$this->set(compact('parents'));
		$imgs = (array) $this->Session->read('TMP.ProdutoImagem');
		$this->set('imgs', $this->FlashImages($imgs));

		//set categorias
		App::import('Model', 'Categoria');
		$this->Categoria = new Categoria();
		//set categorias
		$categorias = $this->Categoria->generatetreelist(null, '{n}.Categoria.id', '{n}.Categoria.nome', '--');

		$this->set('categorias', $categorias);

		//set linguas
		App::import('Model', 'Linguagem');
		$this->Linguagem = new Linguagem();
		$idiomas = $this->Linguagem->find('all', array('fields' => array('Linguagem.id', 'Linguagem.codigo', 'Linguagem.nome', 'Linguagem.thumb_filename', 'Linguagem.thumb_dir'), 'conditions' => array('Linguagem.status' => true, 'Linguagem.externo' => 0)));
		$this->set('idiomas', $idiomas);

		if (!isset($this->data['Produto']['sku']) || $this->data['Produto']['sku'] == "") {
			$this->data['Produto']['sku'] = substr(md5(date('Y-m-d H:i:s')) . rand(0, 1000), -11);
		}
    }

    function admin_edit($id = null) {


//        echo '<pre>';print_r($this->data);die;

		//import models
		App::import("helper", "Uploadify");
		$this->Uploadify = new UploadifyHelper();

		App::import("Model", "ProdutoImagem");
		$this->ProdutoImagem = new ProdutoImagem();

		App::import('Model', 'VariacaoTipo');
		$this->VariacaoTipo = new VariacaoTipo();

		App::import('Model', 'Variacao');
		$this->Variacao = new Variacao();

		App::import('Model', 'AtributoTipo');
		$this->AtributoTipo = new AtributoTipo();

		App::import('Model', 'Atributo');
		$this->Atributo = new Atributo();

		App::import('Model', 'Categoria');
		$this->Categoria = new Categoria();

		App::import('Model', 'Download');
		$this->Download = new Download();

		App::import('Model', 'DownloadTipo');
		$this->DownloadTipo = new DownloadTipo();

		App::import('Model', 'ProdutoDownload');
		$this->ProdutoDownload = new ProdutoDownload();

		App::import('Model','ProdutoKit');
		$this->ProdutoKit = new ProdutoKit();

		App::import('Helper', 'String');
		$this->String = new StringHelper();

		//valida se tem id
		if (!$id && empty($this->data)) {
			$this->Session->setFlash('Paramentros inválidos.', 'flash/error');
			$this->redirect(array('action' => 'index'));
		}

		//BEGIN POST FORM
		if (!empty($this->data)) {


			//seto a quantidade disponivel
			$produto_atual = $this->Produto->find('first', array('recursive' => -1, 'fields' => array('Produto.quantidade_alocada', 'Produto.quantidade_disponivel', 'Produto.agrupador'), 'conditions' => array('Produto.id' => $id)));
			$this->data['Produto']['quantidade_disponivel'] = $this->data['Produto']['quantidade'] - $produto_atual['Produto']['quantidade_alocada'];
			$this->data['Produto']['quantidade'] = $this->data['Produto']['quantidade_disponivel'];
			//unbindModel
			$this->Produto->unbindModel(array('hasMany' => array('ProdutoImagem', 'ProdutoPreco', 'VariacaoProduto', 'AtributoProduto', 'ProdutoDownload', 'ProdutoKit')));

			//atualizado o agrupador
			if ($this->data['Produto']['parent_id'] > 0) {
				$produto_pai = $this->Produto->find('first', array('recursive' => -1, 'conditions' => array('Produto.id' => $this->data['Produto']['parent_id']), 'fields' => array('Produto.agrupador')));
				$this->data['Produto']['agrupador'] = $produto_pai['Produto']['agrupador'];
			} else {
				$this->data['Produto']['agrupador'] = $produto_atual['Produto']['agrupador'];
			}

			if (isset($this->data['ProdutoDescricao'][0]['nome'])) {
				$this->data['Produto']['nome'] = $this->data['ProdutoDescricao'][0]['nome'];
			}

			// Verifica a integridade dos dados no POST de imagens
			if (empty($this->data['ProdutoImagem'][0]['id'])) {
				unset($this->data['ProdutoImagem']);
			}

			//trata os valores do kit do produto
			$this->Produto->id = $id;
			if($this->Produto->field('kit') == true){

				$this->ProdutoKit->soma_valores($this->data);

				$this->ProdutoKit->remove_selecionados($this->data);
			}

            $this->data['Produto']['preco'] = number_format(($this->data['Produto']['preco'] / 100), 2, ',', '.');


//            if ($this->data['Produto']['kit'] == 1) {
//
//                $this->data['Produto']['preco'] = number_format(($this->data['Produto']['preco'] / 100), 0, '', '');
//                foreach ($this->data['ProdutoKit'] as $key => $pk) {
//                    $preco = str_replace('.', '', $pk['preco']);
//                    $preco_real = str_replace('.', '', $pk['preco_real']);
//
//                    $this->data['ProdutoKit'][$key]['preco'] = number_format(($preco / 100), 0, '', '');
//                    $this->data['ProdutoKit'][$key]['preco_real'] = number_format(($preco_real / 100), 0, '', '');
//                }

//            var_dump($this->data);die;
			//begin gamba produto kit (unbind não funcionou)
			if(isset($this->data['ProdutoKit'])){
				$data_produto_kit = $this->data['ProdutoKit'];
				unset($this->data['ProdutoKit']);
			}
			//end gamba produto kit  (unbind não funcionou)

			if ($this->Produto->saveAll($this->data)) {

				//begin gamba produto kit (unbind não funcionou)
				if(isset($data_produto_kit)){
					$this->data['ProdutoKit'] = $data_produto_kit;
				}
				//end gamba produto kit (unbind não funcionou)

				$this->Produto->ProdutoPreco->salvar($this->data);
				$atributos = $this->Produto->AtributoProduto->salvar($this->data);
				$variacoes = $this->Produto->VariacaoProduto->salvar($this->data);
				$download = $this->Produto->ProdutoDownload->salvar($this->data);

				//salva kit do produto
				if($this->Produto->field('kit') == true){

					$this->ProdutoKit->salvar($this->data, $this->Produto->id);
					$this->ProdutoKit->valida_estoque($this->Produto->id);
				}else{
					//não tem estoque? desativa KIT se houver, senão ativa
					//dois metodos, para evitar finds desnecessários em ambas ações
					if($this->data['Produto']['status'] != 1 ||  $this->data['Produto']['quantidade'] <= 0 || $this->data['Produto']['quantidade_disponivel'] <= 0){
						$this->ProdutoKit->desativa_kit_sem_estoque($this->Produto->id);
					}else{
						$this->ProdutoKit->ativa_kit_com_estoque($this->Produto->id);
					}

					//atualiza os valores nos kits que o produto faz parte
					$this->ProdutoKit->atualiza_soma_valores($this->Produto->id, $this->data);
				}

				//CategoriaAtributo
				if ($atributos != "") {
					App::import("Model", "CategoriaAtributo");
					$this->CategoriaAtributo = new CategoriaAtributo();
					$this->CategoriaAtributo->salvar($this->data, $atributos);
				}

				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');

				//ProdutoImagem
				$this->Produto->bindModel(array('hasMany' => array('ProdutoImagem')));
				$this->ProdutoImagem->create();
				$imgs_tmp = $this->Session->read('TMP.ProdutoImagem');
				if ($imgs_tmp):
					foreach ($imgs_tmp as $img):
					$this->data['ProdutoImagem']['produto_id'] = $this->Produto->id;
					$this->data['ProdutoImagem']['id'] = '';
					$this->data['ProdutoImagem']['dir'] = '';
					$this->data['ProdutoImagem']['mimetype'] = '';
					$this->data['ProdutoImagem']['filesize'] = '';
					$this->data['ProdutoImagem']['filename']['type'] = 'image/jpeg';
					$this->data['ProdutoImagem']['ordem'] = $img['ordem'];
					$this->data['ProdutoImagem']['filename']['name'] = $img['tmp_file'];
					$this->data['ProdutoImagem']['filename']['tmp_name'] = WWW_ROOT . 'uploads' . DS . 'tmp' . DS . $img['tmp_file'];
					$this->data['ProdutoImagem']['filename']['error'] = $img['error'];
					$this->data['ProdutoImagem']['filename']['size'] = $img['size'];
					$this->data['ProdutoImagem']['filename']['created'] = time();
					$this->data['ProdutoImagem']['filename']['modified'] = time();
					$this->ProdutoImagem->save($this->data);
					unlink($img['tmp_file']);
					endForeach;
				endIf;
				if (isset($this->data['ProdutoImagem'])):
					foreach ($this->data['ProdutoImagem'] as $id):
					$this->ProdutoImagem->delete($id);
					endforeach;
				endIf;
				$this->Session->delete('TMP.ProdutoImagem');

				//update na familia
				if (isset($this->data['Produto']['children_parent_id']) && is_numeric($this->data['Produto']['children_parent_id'])) {
					$old_product_id = $this->Produto->id;
					$old_chieldrens = $this->Produto->children($old_product_id);
					$new_parent_id = $this->data['Produto']['children_parent_id'];
					$new_chieldrens = set::extract("/Produto[id!={$new_parent_id}]", $old_chieldrens);
					//altera os filhos do produto atual para filho do filho passado
					foreach ($new_chieldrens as $c) {
						$this->Produto->updateAll(array('Produto.parent_id' => "'" . $new_parent_id . "'"), array('Produto.id' => $c['Produto']['id']));
					}

					$tree1 = $this->Produto->find('all', array('recursive' => -1, 'fields' => array('Produto.lft', 'Produto.rght'), 'conditions' => array('Produto.id' => $old_product_id)));
					$tree2 = $this->Produto->find('all', array('recursive' => -1, 'fields' => array('Produto.lft', 'Produto.rght'), 'conditions' => array('Produto.id' => $new_parent_id)));
					//altera o produto atual para filho do filho passado
					$this->Produto->updateAll(array('Produto.parent_id' => "'" . $new_parent_id . "'", "Produto.lft" => "'" . $tree2[0]['Produto']['lft'] . "'", "Produto.rght" => "'" . $tree2[0]['Produto']['rght'] . "'"), array('Produto.id' => $old_product_id));
					//altera o filho passado para pai de todos
					$this->Produto->updateAll(array('Produto.parent_id' => "'0'", "Produto.lft" => "'" . $tree1[0]['Produto']['lft'] . "'", "Produto.rght" => "'" . $tree1[0]['Produto']['rght'] . "'"), array('Produto.id' => $new_parent_id));
				}

                                //FT-338
                                //atualiza os produtoskits relacionados a esse produto
                                if ($this->data['Produto']['kit'] == 0) {
                                    $produto_id = $this->data['Produto']['id'];
                                    $preco = str_replace(',','.',$this->data['Produto']['preco']);
                                    $query = "UPDATE produtos_kits SET preco=".$preco." WHERE produto_id = ".$produto_id;
                                    $this->Produto->query($query);
                                }

                                //atualiza os produtos relacionados a um produtokit
                                if ($this->data['Produto']['kit'] == 1) {
                                    //echo "OK1";

                                    foreach ($this->data['ProdutoKit'] as $produto_kit) {
                                        $produto_id = $produto_kit['produto_id'];
                                        $preco = str_replace(',','.',$produto_kit['preco']);
                                        $query = "UPDATE produtos SET preco=".$preco." WHERE id = ".$produto_id;
                                        $this->Produto->query($query);
                                    }

                                }
                                        
				$this->redirect(array('action' => 'index'));
			} else {
				//debug($this->Produto->invalidfields());
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
			
		}
		//END POST FORM

		if (!empty($this->data['Produto']['Relacionados'])) {
			$this->data['Produto']['Relacionados'] = $this->Produto->find('list', array('fields' => array('id', 'nome'), 'limit' => 100, 'conditions' => array('Produto.id' => $this->data['Produto']['Relacionados'])));
		}
				
				
		//carrega as informacoes do produto
		if (empty($this->data)) {
			$this->data = $this->Produto->find('first', array('recursive' => 1, 'conditions' => array('Produto.id' => $id)));
			if (!$this->data) {
				$this->redirect(array('action' => 'index'));
			}
		} else {
			$produto = $this->Produto->find('first', array('recursive' => 1, 'conditions' => array('Produto.id' => $id)));
			$this->data['ProdutoImagem'] = $produto['ProdutoImagem'];
		}
		
		$otemp = $this->Produto->find('all', array('conditions' => array('Produto.id' => set::extract('/Produto/id', $this->Produto->getpath($id))), 'fields' => array('Produto.id', 'Produto.nome', 'Produto.sku'), 'recursive' => -1));
		$parents = array('' => 'Selecione');
		foreach ($otemp as $prods) {
			$parents[$prods['Produto']['id']] = $prods['Produto']['sku'] . ' - ' . $prods['Produto']['nome'];
		}
		$criancas = ($this->Produto->children($id));
		$this->set('criancas', $criancas);
		//$this->set(compact('categorias', 'fabricantes','atributos','cores','tamanhos','parents'));
		$this->set(compact('parents'));

		/*	 * *
		 * lista as imagens já cadastradas.
		 * */
		$imgs = $imgs_old = array();
		foreach ($this->data['ProdutoImagem'] as $img) {
			$imgs_old[] = $img;
		}
		$this->set('imgs_old', $imgs_old);

		//carrego as variacoes para os selects
		// $variacoes = array('' => 'Selecione') + $this->Variacao->find('list', array('fields' => array('Variacao.id', 'Variacao.valor'), 'order' => array('Variacao.valor')));
		// $this->set('variacoes', $variacoes);
		//carrego os atributos para os selects
		$variacoes_tipos = $this->VariacaoTipo->find('list', array('fields' => array('VariacaoTipo.id', 'VariacaoTipo.nome'), 'order' => array('VariacaoTipo.nome')));
		$this->set('variacoes_tipos', array('' => 'Selecione') + $variacoes_tipos);

		//carrego os atributos para os selects
		$atributos_tipos = $this->AtributoTipo->find('list', array('fields' => array('AtributoTipo.id', 'AtributoTipo.nome'), 'order' => array('AtributoTipo.nome')));
		$this->set('atributos_tipos', array('' => 'Selecione') + $atributos_tipos);

		//carrego os atributos para os selects
		$download_tipos = $this->DownloadTipo->find('list', array('fields' => array('DownloadTipo.id', 'DownloadTipo.nome'), 'order' => array('DownloadTipo.nome')));
		$this->set('download_tipos', array('' => 'Selecione') + $download_tipos);

		//begin atributos
		$atributos_sel = array();
		if (count($this->data['AtributoProduto']) > 0):
			foreach ($this->data['AtributoProduto'] as $c => $v) {
				$this->data['AtributoProduto'][$c]['atributo_id'] = $v['atributo_id'];

				$atributo_id = $this->Atributo->find('first', array('recursive' => -1, 'fields' => array('Atributo.atributo_tipo_id'), 'conditions' => array('Atributo.id' => $v['atributo_id'])));
			
				$this->data['AtributoProduto'][$c]['atributo_tipo_id'] = $atributo_id['Atributo']['atributo_tipo_id'];

				$atributos_sel[$c] = $this->Atributo->find('list', array('fields' => array('Atributo.id', 'Atributo.valor'), 'order' => array('Atributo.valor'), 'conditions' => array('Atributo.atributo_tipo_id' => $atributo_id['Atributo']['atributo_tipo_id'])));
			}
		endIf;
		//seto o sel de atributos
		$this->set('atributos_sel', array('' => 'Selecione') + $atributos_sel);
		//end atributos
		//begin variacoes
		$variacoes_sel = array();
			if (isset($this->data['VariacaoProduto']) && count($this->data['VariacaoProduto']) > 0):
			foreach ($this->data['VariacaoProduto'] as $c => $v) {
				$this->data['VariacaoProduto'][$c]['variacao_id'] = $v['variacao_id'];

				$variacao_id = $this->Variacao->find('first', array('fields' => array('Variacao.variacao_tipo_id'), 'conditions' => array('Variacao.id' => $v['variacao_id'])));
				$this->data['VariacaoProduto'][$c]['variacao_tipo_id'] = $variacao_id['Variacao']['variacao_tipo_id'];

				$variacoes_sel[$c] = $this->Variacao->find('list', array('fields' => array('Variacao.id', 'Variacao.valor'), 'order' => array('Variacao.valor'), 'conditions' => array('Variacao.variacao_tipo_id' => $variacao_id['Variacao']['variacao_tipo_id'])));
			}
		endIf;
		//seto o sel de atributos
		$this->set('variacoes_sel', array('' => 'Selecione') + $variacoes_sel);
		//end variacoes
		//begin downloads
		$downloads_sel = array();
		if (isset($this->data['ProdutoDownload']) && is_array($this->data['ProdutoDownload'])) {
			if (count($this->data['ProdutoDownload']) > 0):
				foreach ($this->data['ProdutoDownload'] as $c => $v) {
					$this->data['ProdutoDownload'][$c]['download_id'] = $v['download_id'];

					$download_id = $this->Download->find('first', array('fields' => array('Download.download_tipo_id'), 'conditions' => array('Download.id' => $v['download_id'])));
					$this->data['ProdutoDownload'][$c]['download_tipo_id'] = $download_id['Download']['download_tipo_id'];

					$downloads_sel[$c] = $this->Download->find('list', array('fields' => array('Download.id', 'Download.nome'), 'order' => array('Download.nome'), 'conditions' => array('Download.download_tipo_id' => $download_id['Download']['download_tipo_id'])));
				}
			endIf;
			//seto o sel de atributos
			$this->set('downloads_sel', array('' => 'Selecione') + $downloads_sel);
		}
		//end downloads

        //begin kit
        $kit_sel = array();
        if (count($this->data['ProdutoKit']) > 0):
            foreach ($this->data['ProdutoKit'] as $c => $v) {

                $prd =  $this->Produto->find('first', array(
                        'recursive' 	=> -1,
                        'conditions' 	=> array('Produto.id' => $v['produto_id']),
                        'fields' 		=> array('Produto.id', 'Produto.sku', 'Produto.nome', 'Produto.preco_promocao_inicio', 'Produto.preco_promocao_fim', 'Produto.preco_promocao_novo')
                    )
                );

                //$this->data['ProdutoKit'][$c]['produto_id'] = array($v['produto_id'] => $prd['Produto']['sku'].' - '.$prd['Produto']['nome']);
                $kit_sel[$c] = array($v['produto_id'] => $prd['Produto']['sku'].' - '.$prd['Produto']['nome']);
            }
        endIf;
		
		/*begin kit_old
		$kit_sel = array();
		if (count($this->data['ProdutoKit']) > 0):
			foreach ($this->data['ProdutoKit'] as $c => $v) {
				$this->data['ProdutoKit'][$c]['preco'] = $this->String->moedaToBco($v['preco']);
				$this->data['ProdutoKit'][$c]['quantidade'] = $v['quantidade'];
				$this->data['ProdutoKit'][$c]['preco_real'] = $v['preco_real'];
				$prd =  $this->Produto->find('first', array(
															'recursive' 	=> -1, 
															'conditions' 	=> array('Produto.id' => $v['produto_id']), 
															'fields' 		=> array('Produto.id', 'Produto.sku', 'Produto.nome', 'Produto.preco_promocao_inicio', 'Produto.preco_promocao_fim', 'Produto.preco_promocao_novo')
														)
											);

				$kit_sel[$c] = array($v['produto_id'] => $prd['Produto']['sku'].' - '.$prd['Produto']['nome']);
			}
		endIf;*/
		//seto o sel de atributos
		$this->set('kit_sel', $kit_sel);
		//end kit
		
		//set categorias
		$categorias = $this->Categoria->generatetreelist(null, '{n}.Categoria.id', '{n}.Categoria.nome', '--');

		$this->set('categorias', $categorias);

		//set linguas
		App::import('Model', 'Linguagem');
		$this->Linguagem = new Linguagem();
		$idiomas = $this->Linguagem->find('all', array('fields' => array('Linguagem.id', 'Linguagem.codigo', 'Linguagem.nome', 'Linguagem.thumb_filename', 'Linguagem.thumb_dir'), 'conditions' => array('Linguagem.status' => true, 'Linguagem.externo' => 0)));
		$this->set('idiomas', $idiomas);
		}

		function admin_delete_download($id = null) {
		$this->render(false);
		$this->layout = false;
		if (!$id) {
			$this->Session->setFlash('Paramêtros inválidos', 'flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->ProdutoDownload->delete($id)) {
			$this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('Registro não foi deletado', 'flash/error');
		$this->redirect(array('action' => 'index'));
    }

    function admin_promocao() {

		if (!empty($this->data)) {
			App::import("helper", "String");
			$this->String = new StringHelper();

			App::import("helper", "Calendario");
			$this->Calendario = new CalendarioHelper();

			if ($this->data['Produto']['preco_promocao_inicio'] == "" && $this->data['Produto']['preco_promocao_fim'] == "" && $this->data['Produto']['preco_promocao_pct'] == "") {
				$this->Produto->invalidate('preco_promocao_inicio', 'campo obrigatório');
				$this->Produto->invalidate('preco_promocao_fim', 'campo obrigatório');
				$this->Produto->invalidate('preco_promocao_pct', 'campo obrigatório');
			} else {
				$this->Produto->set($this->data);
				if ($this->Produto->validates($this->data)) {
					$this->data['Produto']['preco_promocao_inicio'] = $this->Calendario->dataFormatada('Y-m-d', $this->data['Produto']['preco_promocao_inicio']) . ' 00:00:00';
					$this->data['Produto']['preco_promocao_fim'] = $this->Calendario->dataFormatada('Y-m-d', $this->data['Produto']['preco_promocao_fim']) . ' 23:59:59';
					$this->data['Produto']['preco_promocao_pct'] = $this->String->moedaToBco($this->data['Produto']['preco_promocao_pct']);

					if ($this->Produto->updateAll(array('Produto.preco_promocao_novo' => "Produto.preco-(Produto.preco*" . $this->data['Produto']['preco_promocao_pct'] . "/100)", 'Produto.preco_promocao_inicio' => "'" . $this->data['Produto']['preco_promocao_inicio'] . "'", 'Produto.preco_promocao_fim' => "'" . $this->data['Produto']['preco_promocao_fim'] . "'"), array('Produto.categoria_id' => (int) $this->data['Produto']['categoria_id']))) {
						$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
						$this->data = array();
						$this->redirect(array('action' => 'promocao'));
					} else {
						$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
					}
				}
			}
		}

		$fabricantes = array('' => 'Selecione');
		$fabricantes += $this->Produto->Fabricante->find('list', array('fields' => array('id', 'nome')));

		$cores = array('' => 'Selecione');
		$cores += $this->Produto->Cor->find('list', array('fields' => array('id', 'nome')));

		$tamanhos = array('' => 'Selecione');
		$tamanhos += $this->Produto->Tamanho->find('list', array('fields' => array('id', 'nome')));

		$categorias = $this->Categoria->find('treepath');

		$this->set(compact('categorias', 'fabricantes', 'atributos', 'cores', 'tamanhos'));
		
	}

	public function admin_ajax_atributos($categoria_id) {
		$this->layout = 'ajax';
		if (!empty($categoria_id) && is_numeric($categoria_id)) {
			/*	     * *
			 * faz o bind pra pegar os filhos dos atributos setados no categoria_atributos
			 * */
			$this->Produto->Categoria->CategoriaAtributo->primaryKey = 'atributo_id';
			$this->Produto->Categoria->CategoriaAtributo->bindModel(array(
			'hasMany' => array(
				'Filho' => array(
				'className' => 'Atributo',
				'foreignKey' => 'parent_id'
			))));
			//pega os filhos dos atributos filhos dos atributos pais que pertencem a categoria.
			$atributos_da_categoria = set::extract('/Filho/id', $this->Produto->Categoria->CategoriaAtributo->find('all', array('conditions' => array('CategoriaAtributo.categoria_id' => $categoria_id))));
			$this->Produto->Categoria->CategoriaAtributo->unbindModel(array('hasMany' => array('Filho')));
			$this->Produto->Categoria->CategoriaAtributo->primaryKey = 'categoria_id';

			$this->loadModel('Atributo');

			$this->Atributo->bindModel(array(
			'belongsTo' => array(
				'ParentCategory' => array(
				'className' => 'Atributo',
				'foreignKey' => 'parent_id'
			))));

			$atributos = $this->Atributo->find('list', array(
			'fields' => array('Atributo.id', 'Atributo.nome', 'ParentCategory.nome'),
			'recursive' => 0,
			'conditions' => array('Atributo.id' => $atributos_da_categoria, 'ParentCategory.parent_id' => 0),
			'order' => 'ParentCategory.lft ASC, Atributo.lft ASC',
			));
		} else {
			$atributos = array();
		}
		$this->set(compact('atributos'));
    }

    function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash('Parametros inválidos', 'flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->Produto->delete($id)) {
			$this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
		$this->redirect(array('action' => 'index'));
    }

    /*     * *
     * Rating do produto
     */

    public function rating_media($p_id) {
		App::import('Model', 'ProdutoRating');
		$ProdutoRating = new ProdutoRating();
		return $ProdutoRating->query("SELECT ROUND(SUM(nota)/COUNT(nota),2) media FROM produto_ratings WHERE produto_id = '" . addslashes($p_id) . "'");
    }

    public function ajax_rating($p_id, $nota) {

		App::import('Model', 'ProdutoRating');
		$ProdutoRating = new ProdutoRating();

		if ($this->Auth->user()) {
			$data = date('Y-m-d');
			$ip = $_SERVER['REMOTE_ADDR'];
			$ratAnterior = $ProdutoRating->find('first', array(
			'conditions' => array(
				'ip' => $ip,
				'data_votacao' => $data,
				'produto_id' => $p_id
			),
			'fields' => array(
				'id'
			)
			));
			$id = ($ratAnterior) ? $ratAnterior['ProdutoRating']['id'] : false;

			$ProdutoRating->set(array(
			'produto_id' => $p_id,
			'data_votacao' => $data,
			'nota' => (int) $nota > 10 ? 10 : $nota,
			'ip' => $ip,
			'id' => (($id) ? $id : null)
			));
			if ($ProdutoRating->save()) {
			die(json_encode(true));
			} else {
			die(json_encode(false));
			}
		} else {
			die(json_encode(false));
		}
    }

    public function indique($produto_id) {

		$this->layout = 'modal';

		App::import('Model', 'Indique');
		$Indique = new Indique();

		App::import('Model', 'VwEstoque');
		$VwEstoque = new VwEstoque();

		if (!empty($this->data)) {
			$Indique->set($this->data);
			if ($Indique->Validates()) {
				$produto = $VwEstoque->read(null, $produto_id);
				$dados = array_merge($produto, $this->data);
				if ($this->emailIndique($dados)) {
					$this->Session->setFlash("Seus dados foram enviados com sucesso!", "flash/success");
					$this->data = array();
				} else {
					$this->Session->setFlash("Ocorreu um erro ao tentar enviar o email, tente novamente mais tarde", "flash/error");
				}
			} else {
			$this->Session->setFlash("Verifique os campos em destaque!", "flash/error");
			}
		} else {
			$this->data = array();
		}
    }

    /**
     * Email da notificação da compra do cliente, enviado na tela de finalização;
     * @return Boolean
     */
    public function emailIndique($data) {

		App::import("helper", "Html");
		$this->Html = new HtmlHelper();

		App::import("helper", "Calendario");
		$this->Calendario = new CalendarioHelper();
		//seta o debug como zero para evitar aparecer bugs no email do cliente.
		Configure::write('debug', 0);
		if (Configure::read('Loja.smtp_host') != 'localhost') {
			$this->Email->smtpOptions = array(
			'port' => (int) Configure::read('Loja.smtp_port'),
			'host' => Configure::read('Loja.smtp_host'),
			'username' => Configure::read('Loja.smtp_user'),
			'password' => Configure::read('Loja.smtp_password')
			);
			$this->Email->delivery = 'smtp';
		}
		$this->Email->lineLength = 120;
		$this->Email->sendAs = 'html';
		$this->Email->from = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_compra') . ">";
		$this->Email->to = "{$data['Indique']['amigo_nome']} <{$data['Indique']['amigo_email']}>";
		$this->Email->subject = Configure::read('Loja.nome') . " - Produto indicado Pelo seu amigo " . $data['Indique']['seu_nome'];
		if (Configure::read('Reweb.nome_bcc')) {
			$bccs = array();
			$assunto = Configure::read('Reweb.nome_bcc');
			$emails = Configure::read('Reweb.email_bcc');
			foreach (explode(';', $emails) as $bcc) {
			$bccs[] = "{$assunto} <{$bcc}>";
			}
			$this->Email->bcc = $bccs;
		}
		$produto_imagem = str_replace('uploads/produto_imagem/filename/', '', $data['VwEstoque']['imagem']);
		$produto_imagem = $this->Html->image($this->Html->Url("/resizer/view/420/290/false/true/" . $produto_imagem, true), array("alt" => $data['VwEstoque']['nome']));
		$produto_url = Router::url("/" . low(Inflector::slug($data['VwEstoque']['nome'], '-')) . '-' . 'prod-' . $data['VwEstoque']['produto_id'], true) . ".html";
		$email = str_replace(array('{SEU_NOME}', '{SEU_EMAIL}', '{PRODUTO_NOME}', '{PRODUTO_IMAGEM}', '{PRODUTO_URL}', '{SEO_INSTITUCIONAL}'), array(
			$data['Indique']['seu_nome'],
			$data['Indique']['seu_email'],
			$data['VwEstoque']['nome'],
			$produto_imagem,
			$produto_url,
			Configure::read('Loja.seo_institucional')
			), Configure::read('LojaTemplate.indique')
		);
		//setando os dados para o email
		if ($this->Email->send($email)) {
			return true;
		} else {
			return false;
		}
    }

    function admin_ajax_produtos() {
		$query = $this->params['form']['query'];

		//produtos já selecionados
		$selecteds = array();
		if(isset($this->params['form']['selecteds']) && $this->params['form']['selecteds'] != ""){
			$selecteds = $this->params['form']['selecteds'];
		}
		

		if ($query) {
			$allp = $this->Produto->find('all', array('fields' => array('Produto.id', 'Produto.sku', 'Produto.nome'), 'recursive' => -1, 'limit' => '10000', 'conditions' => array('NOT' => array('Produto.id' => $selecteds), 'OR' => array('Produto.tag LIKE' => '%' . $query . '%', 'Produto.sku LIKE' => '%' . $query . '%', 'Produto.descricao LIKE' => '%' . $query . '%', 'Produto.nome LIKE' => '%' . $query . '%'))));
		} else {
			$allp = null;
		}


		$final = array();
		if ($allp) {
			foreach ($allp as $allprodutos) {
				$final[$allprodutos['Produto']['id']] = $allprodutos['Produto']['sku'] . ' - ' . $allprodutos['Produto']['nome'];
			}
		}

		die(json_encode($final));
    }
	
	function ajax_produtos() {
		$query = $this->params['form']['query'];

		//produtos já selecionados
		$selecteds = array();
		if(isset($this->params['form']['selecteds']) && $this->params['form']['selecteds'] != ""){
			$selecteds = $this->params['form']['selecteds'];
		}

		if ($query) {
			$allp = $this->Produto->find('all', array('fields' => array('Produto.id', 'Produto.sku', 'Produto.nome'), 'recursive' => -1, 'limit' => '10000', 'conditions' => array('NOT' => array('Produto.id' => $selecteds), 'OR' => array('Produto.tag LIKE' => '%' . $query . '%', 'Produto.sku LIKE' => '%' . $query . '%', 'Produto.descricao LIKE' => '%' . $query . '%', 'Produto.nome LIKE' => '%' . $query . '%'))));
		} else {
			$allp = null;
		}


		$final = array();
		if ($allp) {
			foreach ($allp as $allprodutos) {
				$final[$allprodutos['Produto']['id']] = $allprodutos['Produto']['nome'];
			}
		}

		die(json_encode($final));
    }

    function admin_ajax_pai() {
		$query = $this->params['form']['query'];

		if ($query) {
			$allp = (($this->Produto->find('all', array('recursive' => -1, 'fields' => array('Produto.id', 'Produto.sku', 'Produto.nome'), 'limit' => '100', 'conditions' => array('parent_id' => 0, 'OR' => array('Produto.tag LIKE' => '%' . $query . '%', 'Produto.sku LIKE' => '%' . $query . '%', 'Produto.descricao LIKE' => '%' . $query . '%', 'Produto.nome LIKE' => '%' . $query . '%'))))));
		} else {
			$allp = (($this->Produto->find('all', array('recursive' => -1, 'fields' => array('Produto.id', 'Produto.sku', 'Produto.nome'), 'limit' => '100', 'conditions' => array('parent_id' => 0)))));
		}

		$final = array();
		foreach ($allp as $allprodutos) {
			$final[$allprodutos['Produto']['id']] = $allprodutos['Produto']['sku'] . ' - ' . $allprodutos['Produto']['nome'];
		}

		die(json_encode($final));
    }
	
	function admin_get_preco() {
		$produto_id = $this->params['form']['produto_id'];
		App::import('Helper', 'String');
		$this->String = new StringHelper();
		if($produto_id) {
			$produto = $this->Produto->find('first', array('recursive' => -1, 'fields' => array('Produto.preco','Produto.preco_promocao', 'Produto.preco_promocao_inicio', 'Produto.preco_promocao_fim'), 'conditions' => array('id' => $produto_id) ));
			if($produto){
				$preco = ($produto['Produto']['preco_promocao'] > 0) ? $produto['Produto']['preco_promocao'] : $produto['Produto']['preco'];
				die(json_encode($this->String->bcoToMoeda($preco)));
			}
		}else{
			die(json_encode(false));
		}		
    }

    function formatbytes($file, $type = "MB") {
		switch ($type) {
			case "KB":
			$filesize = filesize($file) * .0009765625; // bytes to KB
			break;
			case "MB":
			$filesize = (filesize($file) * .0009765625) * .0009765625; // bytes to MB
			break;
			case "GB":
			$filesize = ((filesize($file) * .0009765625) * .0009765625) * .0009765625; // bytes to GB
			break;
		}
		if ($filesize <= 0) {
			return $filesize = 'unknown file size';
		} else {
			return round($filesize, 2) . ' ' . $type;
		}
    }

    public function FlashImages($imgs) {

		App::import("helper", "Html");
		$this->Html = new HtmlHelper();
		App::import("helper", "Form");
		$this->Form = new FormHelper();
		App::import("helper", "Image");
		$this->Image = new ImageHelper();

		$string = '';
		foreach ($imgs as $img):

			if (isset($img['tmp_file'])) {
				$imagem = '../../' . $this->Image->resize('uploads/tmp/' . $img['tmp_file'], 40, 40, array(), null, true);
			} else {
				$imagem = '../../../' . $this->Image->resize($img['dir'] . DS . $img['filename'], 40, 40, array(), null, true);
			}

			if (isset($img['tmp_file'])) {
				$id = $img['tmp_file'];
			} else {
				$id = $img['id'];
			}
			if (isset($img['tmp_file'])) {
				$name = $img['tmp_file'];
			} else {
				$name = $img['filename'];
			}

			$string .= '
				<div class="uploadifyQueue" id="file_uploadQueue"><div class="uploadifyQueueItem">								
					<div class="cancel">
						' . (is_numeric($id) ? '<a href="javascript:;" rel="' . $id . '" class="rm rm-img-old">remover</a>' : '<a href="javascript:;" rel="' . $id . '" class="rm rm-img">remover</a>') . '
					</div>										
				<span class="fileName">
				<label>Ordem <input type="text" class="ordem" value="' . $img['ordem'] . '" rel="' . $id . '" name="data[ProdutoImagem][ordem]" /></label>
				<img src="' . $imagem . '" alt="Foto" /> ' . $name . '</span>
				<span class="percentage"> - 100%</span>											
				</div>
				</div>';
		endForeach;

		return $string;
    }

    public function admin_ajax_imagens() {

		App::import("helper", "Html");
		$this->Html = new HtmlHelper();
		App::import("helper", "Form");
		$this->Form = new FormHelper();
		App::import("helper", "Image");
		$this->Image = new ImageHelper();

		$string = '';
		foreach ($imgs as $img):

			if (isset($img['tmp_file'])) {
				$imagem = '../../' . $this->Image->resize('uploads/tmp/' . $img['tmp_file'], 40, 40, array(), null, true);
			} else {
				$imagem = '../../../' . $this->Image->resize($img['dir'] . DS . $img['filename'], 40, 40, array(), null, true);
			}

			if (isset($img['tmp_file'])) {
				$id = $img['tmp_file'];
			} else {
				$id = $img['id'];
			}
			if (isset($img['tmp_file'])) {
				$name = $img['tmp_file'];
			} else {
				$name = $img['filename'];
			}

			$string .= '
				<div class="uploadifyQueue" id="file_uploadQueue"><div class="uploadifyQueueItem">								
					<div class="cancel">
						' . (is_numeric($id) ? '<a href="javascript:;" rel="' . $id . '" class="rm rm-img-old">remover</a>' : '<a href="javascript:;" rel="' . $id . '" class="rm rm-img">remover</a>') . '
					</div>										
				<span class="fileName">
				<label>Ordem <input type="text" class="ordem" value="' . $img['ordem'] . '" rel="' . $id . '" name="data[ProdutoImagem][ordem]" /></label>
				<img src="' . $imagem . '" alt="Foto" /> ' . $name . '</span>
				<span class="percentage"> - 100%</span>											
				</div>
				</div>';
		endForeach;

		return $string;
    }

    function admin_img_add() {
		App::import("helper", "Uploadify");
		$this->Uploadify = new UploadifyHelper();

		$path = "uploads/tmp/";
		if (!is_dir($path)) {
			mkdir($path);
		}

		$valid_formats = array("jpg", "png", "gif");
		if (!empty($_FILES)) {
			$name = $_FILES['Filedata']['name'];
			if (strlen($name)) {
				if (preg_match('/(.*)\.([a-z]{3,4})$/i', $name, $flag)) {
					$ext = $flag[2];
					if (in_array(low($ext), $valid_formats)) {
						$size = $this->formatbytes($_FILES['Filedata']['tmp_name']);
					if ($size < 100) {
						$actual_image_name = md5(time() . $name) . "." . $ext;
						$tmp = $_FILES['Filedata']['tmp_name'];

						if (move_uploaded_file($tmp, $path . '/' . $actual_image_name)) {
							$_FILES['Filedata']['tmp_file'] = $actual_image_name;
							$_FILES['Filedata']['ordem'] = '';
							//pega as imagens no tmp
							$imgs = $this->Session->read('TMP.ProdutoImagem');
							if ($imgs) {
								$imgs = am($imgs, array($_FILES['Filedata']));
							} else {
								$imgs = array($_FILES['Filedata']);
							}
							$this->Session->write('TMP.ProdutoImagem', $imgs);
							echo $this->Uploadify->build($imgs);
							exit;
						} else {
							echo "Falha ao mover arquivo";
						exit;
						}
					} else {
						echo "Arquivo muito grande";
						exit;
					}
					} else {
						echo "Formato de arquivo inválido.";
						exit;
					}
				} else {
					echo "Formato de arquivo inválido.";
					exit;
				}
			} else {
			echo "por favor selecione uma imagem.!";
			exit;
			}
		} else {
			echo "Falha ao enviar arquivo tente novamente.!";
			exit;
		}
    }

    function admin_img_edit($produto_id) {

		App::import("helper", "Uploadify");
		$this->Uploadify = new UploadifyHelper();
		$this->loadModel('ProdutoImagem');
		$path = "uploads/tmp/";
		if (!is_dir($path)) {
			mkdir($path);
		}

		$valid_formats = array("jpg", "png", "gif", "bmp");
		if (!empty($_FILES)) {
			$name = $_FILES['Filedata']['name'];
			if (strlen($name)) {
			if (preg_match('/(.*)\.([a-z]{3,4})$/i', $name, $flag)) {
				$ext = $flag[2];
				if (in_array(low($ext), $valid_formats)) {
				$size = $this->formatbytes($_FILES['Filedata']['tmp_name']);
				if ($size < 100) {
					$actual_image_name = md5(time() . $name) . "." . $ext;
					$tmp = $_FILES['Filedata']['tmp_name'];

					if (move_uploaded_file($tmp, $path . '/' . $actual_image_name)) {
					$_FILES['Filedata']['tmp_file'] = $actual_image_name;
					$_FILES['Filedata']['ordem'] = '';

					$this->ProdutoImagem->create();
					$this->data['ProdutoImagem']['produto_id'] = $produto_id;
					$this->data['ProdutoImagem']['id'] = '';
					$this->data['ProdutoImagem']['dir'] = '';
					$this->data['ProdutoImagem']['mimetype'] = '';
					$this->data['ProdutoImagem']['filesize'] = '';
					$this->data['ProdutoImagem']['ordem'] = 0;
					$this->data['ProdutoImagem']['filename']['type'] = 'image/jpeg';
					$this->data['ProdutoImagem']['filename']['name'] = $_FILES['Filedata']['tmp_file'];
					$this->data['ProdutoImagem']['filename']['tmp_name'] = WWW_ROOT . 'uploads' . DS . 'tmp' . DS . $_FILES['Filedata']['tmp_file'];
					$this->data['ProdutoImagem']['filename']['error'] = $_FILES['Filedata']['error'];
					$this->data['ProdutoImagem']['filename']['size'] = $_FILES['Filedata']['size'];
					$this->data['ProdutoImagem']['filename']['created'] = time();
					$this->data['ProdutoImagem']['filename']['modified'] = time();
					$this->ProdutoImagem->save($this->data);
					unlink(WWW_ROOT . 'uploads' . DS . 'tmp' . DS . $_FILES['Filedata']['tmp_file']);
					$produto = $this->Produto->find('first', array('conditions' => array('Produto.id' => $produto_id)));
					$imgs = array();
					foreach ($produto['ProdutoImagem'] as $img) {
						$imgs[] = $img;
					}
					echo $this->Uploadify->build($imgs);
					exit;
					} else {
					echo "Falha ao mover arquivo";
					exit;
					}
				} else {
					echo "Arquivo muito grande";
					exit;
				}
				} else {
				echo "Formato de arquivo inválido.";
				exit;
				}
			} else {
				echo "Formato de arquivo inválido.";
				exit;
			}
			} else {
			echo "por favor selecione uma imagem.!";
			exit;
			}
		} else {
			echo "Falha ao enviar arquivo tente novamente.!";
			exit;
		}
    }

    function admin_rm_img_tmp($remove) {

		$this->layout = false;
		$this->render(false);
		$path = "uploads/tmp/";
		//pega as imagens no tmp
		$olds = $this->Session->read('TMP.ProdutoImagem');
		//se a imagem passada não está no array add a img no tmp
		foreach ($olds as $key => $img) {
			if ($img['tmp_file'] === $remove) {
				$this->Session->delete("TMP.ProdutoImagem.{$key}");
				unlink($path . $img['tmp_file']);
			}
		}
		die(json_encode(true));
    }

    function admin_rm_img_old($id) {

		if (!$id) {
			die(json_encode(false));
		}
		if ($id != "") {

			//produto imagem dados
			$produto_imagem = $this->Produto->ProdutoImagem->find('first', array('conditions' => array('ProdutoImagem.id' => $id)));

			//produto dados
			$produto = $this->Produto->find("first", array('recursive' => -1, 'conditions' => array('Produto.id' => $produto_imagem['ProdutoImagem']['produto_id'])));

			//verifico se é pai ou não
			if ($produto['Produto']['parent_id'] == 0) {
				//pai
				$filhos = $this->Produto->children($produto['Produto']['id']);
			} else {
				//filho
				$produto = $this->Produto->find("first", array('recursive' => -1, 'conditions' => array('Produto.id' => $produto['Produto']['parent_id'])));
				$filhos = $this->Produto->children($produto['Produto']['id']);
			}

			$produtos_ids = set::extract("/Produto/id", $filhos);
			$produtos_ids[] = $produto['Produto']['id'];

			//produtos imagens
			$produtos_imagens = $this->Produto->ProdutoImagem->find('all', array('conditions' => array('ProdutoImagem.filename' => $produto_imagem['ProdutoImagem']['filename'], 'ProdutoImagem.produto_id' => $produtos_ids)));

			$status = true;
			foreach ($produtos_imagens as $produto_img) {
				if (!$this->Produto->ProdutoImagem->delete($produto_img['ProdutoImagem']['id'])) {
					$status = false;
				}
			}

			if ($status == true) {
				die(json_encode(true));
			}
		} else {
			die(json_encode(false));
		}
    }

    function admin_img_ordem_old($id, $ordem) {
		$this->loadModel('ProdutoImagem');
		if ($id && $ordem) {
			$this->ProdutoImagem->id = $id;
			if ($this->ProdutoImagem->saveField('ordem', $ordem)) {
				die(json_encode(true));
			}
		}
		die(json_encode(false));
    }

    function admin_img_ordem_tmp($id, $ordem) {
		$this->loadModel('ProdutoImagem');
		if ($id && $ordem) {
			$tmp = $this->Session->read('TMP.ProdutoImagem');
			//se a imagem passada não está no array add a img no tmp
			foreach ($tmp as $key => $img) {
				if ($img['tmp_file'] === $id) {
					$this->Session->write("TMP.ProdutoImagem.{$key}.ordem", $ordem);
				}
			}
		}
		die(json_encode(true));
    }

    function download($produto_id = null) {
		if (!$produto_id) {
			$this->Session->setFlash(__('Parametros inválidos', 'flash/error', array(), 'error'));
			$this->redirect(array('action' => 'index'));
		}
		$this->loadModel('ProdutoDownload');
		$download = $this->ProdutoDownload->find('first', array('conditions' => array('ProdutoDownload.produto_id' => $produto_id)));
		$arquivo = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . $download['ProdutoDownload']['dir'] . DS . $download['ProdutoDownload']['filename'];
		$this->downloadFile($arquivo);
    }

    function downloadFile($fullPath) {

		// Must be fresh start
		if (headers_sent())
			die('Headers Sent');

		// Required for some browsers
		if (ini_get('zlib.output_compression'))
			ini_set('zlib.output_compression', 'Off');

		// File Exists?
		if (file_exists($fullPath)) {

			// Parse Info / Get Extension
			$fsize = filesize($fullPath);
			$path_parts = pathinfo($fullPath);
			$ext = strtolower($path_parts["extension"]);

			// Determine Content Type
			switch ($ext) {
			case "pdf": $ctype = "application/pdf";
				break;
			case "exe": $ctype = "application/octet-stream";
				break;
			case "zip": $ctype = "application/zip";
				break;
			case "doc": $ctype = "application/msword";
				break;
			case "xls": $ctype = "application/vnd.ms-excel";
				break;
			case "ppt": $ctype = "application/vnd.ms-powerpoint";
				break;
			case "gif": $ctype = "image/gif";
				break;
			case "png": $ctype = "image/png";
				break;
			case "jpeg":
			case "jpg": $ctype = "image/jpg";
				break;
			default: $ctype = "application/force-download";
			}

			header("Pragma: public"); // required
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private", false); // required for certain browsers
			header("Content-Type: $ctype");
			header("Content-Disposition: attachment; filename=\"" . basename($fullPath) . "\";");
			header("Content-Transfer-Encoding: binary");
			header("Content-Length: " . $fsize);
			ob_clean();
			flush();
			readfile($fullPath);
		} else {
			die('Arquivo não encontrado.');
		}
    }

    public function admin_exportar($conditions) {

		//$filtros_usados = $this->Filter->getStoredData();
		App::import('Helper', 'Calendario');
		$this->Calendario = new CalendarioHelper();

		$rows = $this->Produto->find('all', array(
			'recursive' => -1,
			'conditions' => $conditions,
			'joins' => array(
			array(
				'table' => 'produtos_categorias',
				'alias' => 'Categorias',
				'type' => 'left',
				'conditions' =>
				array(
				'Categorias.produto_id = Produto.id'
				)
			), array(
				'table' => 'categorias',
				'alias' => 'Categoria',
				'type' => 'left',
				'conditions' =>
				array(
				'Categorias.categoria_id = Categoria.id'
				)
			)
			),
			'group' => 'Produto.id'
			)
		);
		$table = "<table>";
		$table .= "
					<tr bgcolor=\"#CECECE\">
						<td><strong>Id</strong></td>
						<td><strong>SKU</strong></td>
						<td><strong>Referencia</strong></td>
						<td><strong>Nome</strong></td>
						<td><strong>" . iconv("UTF-8", "ISO-8859-1//IGNORE", "Descrição") . "</strong></td>
						<td><strong>" . iconv("UTF-8", "ISO-8859-1//IGNORE", "Descrição Resumida") . "</strong></td>
						<td><strong>Custo</strong></td>
						<td><strong>" . iconv("UTF-8", "ISO-8859-1//IGNORE", "Preço") . "</strong></td>
						<td><strong>" . iconv("UTF-8", "ISO-8859-1//IGNORE", "Preço Promoção") . "</strong></td>
						<td><strong>Quantidade</strong></td>
						<td><strong>" . iconv("UTF-8", "ISO-8859-1//IGNORE", "Quantidade Disponível") . "</strong></td>
						<td><strong>Quantidade Alocada</strong></td>
						<td><strong>Peso</strong></td>
						<td><strong>Peso Ramos</strong></td>
						<td><strong>Status</strong></td>
						<td><strong>Largura</strong></td>
						<td><strong>Altura</strong></td>
						<td><strong>Profundidade</strong></td>
						<td><strong>Unidade Venda</strong></td>
						<td><strong>Quantidade Acessos</strong></td>
						<td><strong>Criado</strong></td>
						<td><strong>Editado</strong></td>
					</tr>";
		foreach ($rows as $row) {
			if ($row['Produto']['status'] == 1)
			$status = "Ativo";
			elseif ($row['Produto']['status'] == 2)
			$status = "Indispónivel";
			else
			$status = "Inativo";
			//$status = ( $row['Produto']['status'] == ) ? "Ativo" : "Inativo";
			$table .= "
					<tr>
						<td>" . $row['Produto']['id'] . "</td>
						<td>" . $row['Produto']['sku'] . "</td>
						<td>" . $row['Produto']['referencia'] . "</td>
						<td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Produto']['nome']) . "</td>
						<td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Produto']['descricao']) . "</td>
						<td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Produto']['descricao_resumida']) . "</td>
						<td>" . $row['Produto']['custo'] . "</td>
						<td>" . $row['Produto']['preco'] . "</td>
						<td>" . $row['Produto']['preco_promocao'] . "</td>
						<td>" . $row['Produto']['quantidade'] . "</td>
						<td>" . $row['Produto']['quantidade_disponivel'] . "</td>
						<td>" . $row['Produto']['quantidade_alocada'] . "</td>
						<td>" . $row['Produto']['peso'] . "</td>
						<td>" . $row['Produto']['peso_ramos'] . "</td>
						<td>" . $status . "</td>
						<td>" . $row['Produto']['largura'] . "</td>
						<td>" . $row['Produto']['altura'] . "</td>
						<td>" . $row['Produto']['profundidade'] . "</td>
						<td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Produto']['unidade_venda']) . "</td>
						<td>" . $row['Produto']['quantidade_acessos'] . "</td>
						<td>" . $this->Calendario->DataFormatada("d-m-Y", $row['Produto']['created']) . "</td>
						<td>" . $this->Calendario->DataFormatada("d-m-Y", $row['Produto']['modified']) . "</td>
					</tr>";
		}
		$table .= "</table>";

		App::import("helper", "String");
		$this->String = new StringHelper();
		$this->layout = false;
		$this->render(false);
		set_time_limit(0);
		header('Content-type: application/x-msexcel');
		$filename = "produtos_" . date("d_m_Y_H_i_s");
		header('Content-Disposition: attachment; filename=' . $filename . '.xls');
		header('Pragma: no-cache');
		header('Expires: 0');

		die($table);
    }

    /**
     * Email da notificação do formulario de aviseme estoque do produto;
     * @return Boolean
     */
    public function emailAviseMe($data) {

		App::import("helper", "Html");
		$this->Html = new HtmlHelper();

		App::import("helper", "Calendario");
		$this->Calendario = new CalendarioHelper();
		//seta o debug como zero para evitar aparecer bugs no email do cliente.
		//Configure::write('debug', 0);
		
		if (Configure::read('Loja.smtp_host') != 'localhost') {
			$this->Email->smtpOptions = array(
				'port' => (int) Configure::read('Loja.smtp_port'),
				'host' => Configure::read('Loja.smtp_host'),
				'username' => Configure::read('Loja.smtp_user'),
				'password' => Configure::read('Loja.smtp_password')
			);
			$this->Email->delivery = 'smtp';
		}
		
		$this->Email->lineLength = 120;
		$this->Email->sendAs = 'html';
		$this->Email->from = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_compra') . ">";
		$this->Email->to = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_aviseme') . ">";
		$this->Email->subject = Configure::read('Loja.nome') . " - Avise-me quando chegar";

		$produtos = $this->Produto->find('first', array('recursive' => -1, 'conditions' => array('Produto.id' => $data['Avise']['produto_id'])));

		$produto_url = Router::url("/" . low(Inflector::slug($produtos['Produto']['nome'], '-')) . '-' . 'prod-' . $produtos['Produto']['id'], true) . ".html";
		$email = str_replace(
			array('{AVISEME_PRODUTO_URL}', '{AVISEME_PRODUTO_NOME}', '{AVISEME_USUARIO_NOME}', '{AVISEME_USUARIO_EMAIL}', '{AVISEME_DATA}', '{AVISEME_IP}'), array(
			$produto_url,
			$produtos['Produto']['nome'],
			$data['Avise']['nome'],
			$data['Avise']['email'],
			date("d/m/Y H:i:s"),
			$_SERVER['REMOTE_ADDR']
			), Configure::read('LojaTemplate.produto_aviseme')
		);
		//setando os dados para o email
		if ($this->Email->send($email)) {
			return true;
		} else {
			return false;
		}
    }

}

?>