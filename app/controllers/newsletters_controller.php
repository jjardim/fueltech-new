<?php

class NewslettersController extends AppController {

    var $name = 'Newsletters';
    var $helpers = array('Calendario', 'String', 'Javascript');
	 var $components = array('Filter','Session');

    function admin_index() {
	
		$filtros = array();
		if (isset($this->data["Filter"]["nome"])) {
			$filtros['nome'] = "Newsletter.nome LIKE '%{%value%})%'";
		}

		if (isset($this->data["Filter"]["email"])) {
			$filtros['email'] = "Newsletter.email LIKE '%{%value%}%'";
		}
		
		$this->Filter->setConditions($filtros);
		$this->Filter->check();
		$conditions = $this->Filter->getFilters();
		$this->Filter->setDataToView();
		
		if(isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar"){
			$this->admin_export($conditions);
		}
		
        $this->Newsletter->recursive = 0;
        $this->set('newsletters', $this->paginate($conditions));
    }
    function admin_export($conditions) {
        $this->layout = false;
        $this->render(false);
        set_time_limit(0);
        $this->recursive = -1;
        $rows = $this->Newsletter->find('all',array('conditions' => $conditions));
        header("Content-Type: application/octet-stream");
        $file = "newsletter_" . date("y-m-d-H-i-s") . ".csv";
        header("Content-disposition:attachment; filename=$file");
        echo "Nome;Email"."\n";
        foreach ($rows as $row) {
           echo "{$row['Newsletter']['nome']}".";";
           echo "{$row['Newsletter']['email']}"."\n";
        }
        exit;
    }
	function admin_delete($id = null) {
		if (!$id) {
            $this->Session->setFlash('Parametros inv�lidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Newsletter->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro n�o pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}
}

?>