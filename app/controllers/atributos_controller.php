<?php

/**
 * BakeSale shopping cart
 * Copyright (c)	2006-2009, Matti Putkonen
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright		Copyright (c) 2006-2009, Matti Putkonen
 * @link			http://bakesalepro.com/
 * @package			BakeSale
 * @license			http://www.opensource.org/licenses/mit-license.php The MIT License
 */

/**
 * Manages product categories
 *
 */
class AtributosController extends AppController {

    public $uses = array('Atributo');
    var $helpers = array('Javascript','Calendario','Image');
    var $components = array('Filter', 'Session');
	
	public function admin_index() {
		//filters
		$filtros = array();
        if (isset($this->data["Filter"]["atributo_tipo_id"])) {
            $filtros['atributo_tipo_id'] = "Atributo.atributo_tipo_id = '{%value%}'";
        }		
		if (isset($this->data["Filter"]["valor"])) {
            $filtros['valor'] = "Atributo.valor LIKE '%{%value%}%'";
        }		
		if (isset($this->data["Filter"]["status"])) {
            $filtros['status'] = "Atributo.status = '{%value%}'";
        }
		
        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();
		
		if(isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar"){
			$this->admin_exportar($conditions);
		}
		
		$this->Atributo->recursive = 0;
		$this->set('atributos', $this->paginate($conditions));
		
		App::import('Model','AtributoTipo');
		$this->AtributoTipo = new AtributoTipo();		
        $atributo_tipos = array('' => 'Selecione') + $this->AtributoTipo->find('list', array('fields' => array('AtributoTipo.id','AtributoTipo.nome'), 'order' => array('AtributoTipo.nome')));
		$this->set('atributo_tipos',$atributo_tipos);
    }
	
	/**
     * add new category
     */
    public function admin_add() {
        if (!empty($this->data)) {

            if ($this->Atributo->saveAll($this->data)) {
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
		
        App::import('Model','AtributoTipo');
		$this->AtributoTipo = new AtributoTipo();		
        $atributo_tipos = array('' => 'Selecione') + $this->AtributoTipo->find('list', array('fields' => array('AtributoTipo.id','AtributoTipo.nome')));
		$this->set('atributo_tipos',$atributo_tipos);
    }

    /**
     * Edits information on a category.
     *
     * @param id int
     * The ID field of the category to edit.
     */
    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            $this->data['Atributo']['id'] = $id;
			
			if( $this->data['Atributo']['thumb_remove'] == 1 ){
				$this->Atributo->remover_thumb($this->data['Atributo']['id']);
			}
			
            if ($this->Atributo->saveAll($this->data)) {

                $filhos = $this->Atributo->children($this->params['pass'][0]);
                $ids = set::extract('/Atributo/id', $filhos);
                foreach ($ids as $id) {
                    $this->Atributo->id = $id;
                    $this->Atributo->saveField('status', $this->data['Atributo']['status']);
                }
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
			    $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
		
		if (empty($this->data)) {
			$this->Atributo->recursive = 0;
            $this->data = $this->Atributo->read(null, $id);
        }
		
		App::import('Model','AtributoTipo');
		$this->AtributoTipo = new AtributoTipo();		
        $atributo_tipos = array('' => 'Selecione') + $this->AtributoTipo->find('list', array('fields' => array('AtributoTipo.id','AtributoTipo.nome')));
		$this->set('atributo_tipos',$atributo_tipos);
    }

	function admin_delete($id = null) {
		if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Atributo->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}
    
	public function admin_menu() {
		debug($this->Atributo->_menu('admin'));
        return $this->Atributo->_menu('admin');
    }
	
	public function admin_exportar($conditions){
	
		App::import('Helper', 'Calendario');
		$this->Calendario = new CalendarioHelper();
		
		$rows = $this->Atributo->find('all',array('recursive' => 0, 'conditions' => $conditions));
		
		$table = "<table>";
		$table .= "
				<tr bgcolor=\"#CECECE\">
					<td><strong>Id</strong></td>
					<td><strong>Tipo de Atributo</strong></td>
					<td><strong>Valor</strong></td>
					<td><strong>Status</strong></td>
					<td><strong>Criado</strong></td>
					<td><strong>Editado</strong></td>
				</tr>";
		foreach ($rows as $row) {
			$status = ( $row['Atributo']['status'] ) ? "Ativo" : "Inativo";
			$table .= "
				<tr>
					<td>".$row['Atributo']['id']."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['AtributoTipo']['nome'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Atributo']['valor'])."</td>
					<td>".$status."</td>
					<td>".$this->Calendario->DataFormatada("d-m-Y",  $row['Atributo']['created'])."</td>
					<td>".$this->Calendario->DataFormatada("d-m-Y",  $row['Atributo']['updated'])."</td>
				</tr>";
		}
		$table .= "</table>";
		
		App::import("helper", "String");
		$this->String = new StringHelper();
		$this->layout = false;
		$this->render(false);
		set_time_limit(0);		
		header('Content-type: application/x-msexcel');
		$filename = "atributos_" . date("d_m_Y_H_i_s");
		header('Content-Disposition: attachment; filename='.$filename.'.xls');
		header('Pragma: no-cache');
		header('Expires: 0');
		
		die($table);
	}

	public function admin_get_ajax_atributos() {
		$this->layout = 'ajax';
		$atributos = $this->Atributo->find("all", array(
										'recursive'=>-1,
										'fields' => array('Atributo.id','Atributo.valor'),
										'conditions' => array('Atributo.atributo_tipo_id' => $this->params['form']['atributo_id'])
										)
							);
		$final = array();
		if($atributos){
			foreach($atributos as $atributos){
				$final[$atributos['Atributo']['id']] = $atributos['Atributo']['valor'];
			}
		}
		die(json_encode($final));
    }
	
	public function ajax_atributos_categoria($categoria_id){
		
		$this->layout = 'ajax';
		
		$produtos_categoria = $this->Session->read("ProdutosCategoria");
		$atributos_menu = array();
		
		if(is_array($produtos_categoria) && count($produtos_categoria) > 0){
			$query = "
						SELECT 
							Atributo.valor,
							AtributoTipo.nome,
							AtributoTipo.codigo,
							AtributoTipo.id,
							Atributo.id
						FROM 
							atributo_tipos AS AtributoTipo 
								LEFT JOIN atributos AS Atributo ON (Atributo.atributo_tipo_id = AtributoTipo.id) 
								LEFT JOIN atributos_produtos AS ProdutoAtributo ON (Atributo.id = ProdutoAtributo.atributo_id)
								LEFT JOIN produtos AS Produto ON (Produto.id = ProdutoAtributo.produto_id)
						WHERE 
							Atributo.filtro = TRUE 
							AND Produto.status > 0
							AND 
							(Produto.parent_id in (".implode(",",$produtos_categoria).") OR Produto.id in (".implode(",",$produtos_categoria)."))
						GROUP BY
							Atributo.id
						ORDER BY
							AtributoTipo.nome,
							Atributo.valor
						";
			//print($query);die;
			App::import('Model','Categoria');
			$this->Categoria = new Categoria();
			$atributos = $this->Categoria->Query($query);
			$atributos_menu = array();
			foreach ($atributos as $linha) {
				$atributos_menu[$linha['AtributoTipo']['id']]['nome_filtro']	= $linha['AtributoTipo']['nome'];
				$atributos_menu[$linha['AtributoTipo']['id']]['codigo_filtro']	= $linha['AtributoTipo']['codigo'];
				$atributos_menu[$linha['AtributoTipo']['id']]['valor_filtro'][] = $linha['Atributo'];
			}
			
			//filtros selecionados
			$filtros = array();
			//debug($this->params);
			$atributos_selecionados = $this->Session->read('atributos_selecionados');
			if($atributos_selecionados != ""):
				foreach( json_decode($atributos_selecionados) as $v )
				{
					foreach($atributos as $key => $atributo)
					{
						$k = array_search($v, $atributo['Atributo']);
						if( $k == 'id' )
						{
							$filtros[$key] = am($filtros, $atributos[$key]);
							unset($atributos[$key]);
						}
					}
				}
				
				//monto o menu de filtros selecionados
				$filtros_menu = array();
				foreach ($filtros as $linha) {
					
					$filtros_menu[$linha['AtributoTipo']['id']]['nome_filtro']	= $linha['AtributoTipo']['nome'];
					$filtros_menu[$linha['AtributoTipo']['id']]['codigo_filtro']	= $linha['AtributoTipo']['codigo'];
					$filtros_menu[$linha['AtributoTipo']['id']]['id_filtro']	= $linha['Atributo']['id'];
					$filtros_menu[$linha['AtributoTipo']['id']]['valor_filtro'] = $linha['Atributo']['valor'];
				}
				//set
				$this->set('filtros_selecionados', $filtros_menu);
			endIf;
		}
		//debug($atributos);die.
		//die(json_encode($atributos_menu));
		$this->set('menu_filtro', $atributos_menu);
		//$this->render('ajax_atributos_categoria');
	}
	
}

?>
