<?php
class GeraxmlController extends AppController{
	var $name = 'Geraxml';
	var $uses = array('Produto');
	var $helpers = array('Time');
    function googleshopping(){
	
        Configure::write ('debug', 0);		
		App::import("helper", "Html");
        $this->Html = new HtmlHelper();		
		App::import("helper", "Time");
        $time = new TimeHelper();		
		header("Content-Type: text/xml; charset=UTF-8");
		$this->Produto->recursive = -1;
		
		
        $produtos = $this->Produto->find('all', array('contain'=>array('Categoria','ProdutoImagem','Cor','Tamanho'),'conditions' => array('Produto.xml_googleshopping'=>true,'Produto.status'=>true)));
		$loja_nome  = Configure::read('Loja.nome');
		$loja_titulo  = Configure::read('Loja.titulo');
		$loja_url  = $this->Html->Url('/',true);
		
		$xml ="<?xml version=\"1.0\"?>
					<rss version=\"2.0\" xmlns:g=\"http://base.google.com/ns/1.0\">
						<channel>
							<title>{$loja_nome}</title>
							<link>{$loja_url}</link>
							<description>{$loja_titulo}</description>\r\n";
			foreach ($produtos as $produto):
				if(!empty($produto['ProdutoImagem'][0]['filename'])){
					$imagem = $loja_url . '/uploads/produto_imagem/filename/'.$produto['ProdutoImagem'][0]['filename'];
				}else{
					$imagem = $loja_url . '/img/site/logo.png';
				}
					$produto['Produto']['nome'] = str_replace('&','',$produto['Produto']['nome'] . ' '. $produto['Cor']['nome']. ' '.$produto['Tamanho']['nome']);
				
				// EXTRAS
				$extras = '';
				
				$extras .='<g:brand>'.$produto['Categoria']['nome'].'</g:brand>';
				
				$extras .='<g:product_type>'.$produto['Categoria']['nome'].'</g:product_type>';
				
				$extras.='<g:google_product_category>'.Configure::read('Loja.seo_meta_keywords').'</g:google_product_category>';					
					
				if(!empty($produto['Produto']['quantidade']) && $produto['Produto']['quantidade']>0){
					$extras .='<g:availability>in stock</g:availability>';
				}else{
					$extras .='<g:availability>out of stock</g:availability>';
				}
				
				if(!empty($produto['Produto']['codigo_externo'])){
					$extras .='<g:mpn>'.$produto['Produto']['codigo_externo'].'</g:mpn>';
				}
				
				$extras .='<g:shipping><g:country>BR</g:country><g:price>0,00</g:price></g:shipping>';

				
				$xml  .= '<item><title>'.$produto['Produto']['nome'].'</title>
				<link>'.$this->Html->Url("/" . low(Inflector::slug($produto['Produto']['nome'], '-')) . '-prod-' . $produto['Produto']['id'],true).'.html</link>
				<description>'.strip_tags($produto['Produto']['nome']).'</description>
				<g:image_link>'.((($imagem))).'</g:image_link>
				<g:price>'.(($produto['Produto']['preco_promocao']>0)?$produto['Produto']['preco_promocao']:$produto['Produto']['preco']).'</g:price>
				<g:condition>new</g:condition>
				<g:id>'.$produto['Produto']['id'].'</g:id>'.$extras.'</item>';
			endforeach;
		$xml .='</channel></rss>';
		die($xml);
    }
	
	function buscape (){
		$xml_topo = '<?xml version="1.0" encoding="ISO-8859-1"?>
						<'.strtoupper(preg_replace("/[^a-z]/i", "",env('HTTP_HOST'))).'>';
		$xml_template = '<produto>
							<id_produto>[[id_produto]]</id_produto>
							<link_produto>[[link_produto]]</link_produto>
							<titulo>[[nome]]</titulo>
							<preco>[[preco]]</preco>
							<parcelamento>[[parcelamento]]</parcelamento>
							<disponibilidade>[[disponibilidade]]</disponibilidade>
							<imagem>[[imagem]]</imagem>
							<categoria>[[categoria]]</categoria>
						</produto>';
		$xml_rodape = '</'.strtoupper(preg_replace("/[^a-z]/i", "",env('HTTP_HOST'))).'>';
		$this->modelo($xml_topo,$xml_template,$xml_rodape,'BUSCAPE');
	}
	function jacotei (){
		$xml_topo = '<?xml version="1.0" encoding="ISO-8859-1"?>
						<loja nome="'.strtoupper(preg_replace("/[^a-z]/i", "",env('HTTP_HOST'))).'">';
		$xml_template = '<produto>
							<id_produto>[[codigo]]</id_produto>
							<descritor nome=\'Tamanho\'>[[tamanho]]</descritor>
							<descritor nome=\'Cor\'>[[cor]]</descritor>
							<link_produto>[[link_produto]]</link_produto>
							<descricao>[[nome]]</descricao>
							<preco>[[preco]]</preco>
							<preco_normal>[[preco]]</preco_normal>
							<imagem>[[imagem]]</imagem>
							<categoria>[[categoria]]</categoria>
						</produto>';
		
		$xml_rodape = '<regs>[[count]]</regs></loja>';
		$this->modelo($xml_topo,$xml_template,$xml_rodape,'JACOTEI');
	}
	
	function ibiubi (){
		$xml_topo = '<?xml version="1.0" encoding="utf-8"?><ibiubi>';
		$xml_template = '<anuncio_produtos>
							<cod_anuncio><![CDATA[[[id_produto]]]]></cod_anuncio>
							<url_anuncio><![CDATA[[[link_produto]]]]></url_anuncio>
							<categoria><![CDATA[[[categoria]]]]></categoria>
							<titulo><![CDATA[[[nome]]]]></titulo>
							<descricao><![CDATA[[[descricao]]]]></descricao>
							<fotos><url><![CDATA[[[imagem]]]]></url></fotos>
							<valor><![CDATA[[[preco]]]]></valor>
						</anuncio_produtos>';
		$xml_rodape = '</ibiubi>';
		
		$this->modelo($xml_topo,$xml_template,$xml_rodape,'IBIUBI');
	}
	
	function shopmania (){
		$xml_topo = "<?xml version='1.0' encoding='ISO-8859-1'?><lista>";
		$xml_template = '<produto>
							<titulo><![CDATA[[[nome]]]]></titulo>
							<descricao><![CDATA[[[descricao]]]]></descricao>
							<url_detalhes>[[link_produto]]</url_detalhes>
							<imagem>[[imagem]]</imagem>
							<preco>[[preco]]</preco>
							<preco_promocao>[[preco]]</preco_promocao>
							<parcelamento>[[parcelamento]]</parcelamento>
							<categoria><![CDATA[[[categoria]]]]></categoria>
						</produto>';
		$xml_rodape = '</lista>';
		
		$this->modelo($xml_topo,$xml_template,$xml_rodape,'SHOPMANIA');
	}
	
	private function modelo ($xml_topo,$xml_template,$xml_rodape,$tipo){
	
		Configure::write ('debug', 0);		
		App::import("helper", "Html");
		$this->Html = new HtmlHelper();		
		App::import("helper", "Time");
		$time = new TimeHelper();		
		header("Content-Type: text/xml; charset=UTF-8");
		
		$conditions['Produto.status'] = true;
		
		if($tipo=='BUSCAPE'){
			$conditions['Produto.xml_buscape'] = true;
		}elseif($tipo=='IBIUBI'){
			$conditions['Produto.xml_ibiubi'] = true;
		}elseif($tipo=='SHOPMANIA'){
			$conditions['Produto.xml_shopmania'] = true;
		}elseif($tipo=='JACOTEI'){
			$conditions['Produto.xml_jacotei'] = true;
		}
		
		
		$cache_name = 'query_xmls';
		$cache_data = Cache::read($cache_name,'sql');
		if (empty($cache_data))
		{
			$dataModel = $this->Produto->find('all', array('fields'=>"*,(`Produto`.`quantidade` - ifnull((select sum(`pedido_itens`.`quantidade`) AS `sum(``pedido_itens``.``quantidade``)` from (`pedido_itens` join `pedidos`) where ((`Produto`.`id` = `pedido_itens`.`produto_id`) and (`pedido_itens`.`pedido_id` = `pedidos`.`id`) and (`pedidos`.`finalizado` = 0))),0)) as disponivel",'contain'=>array('ProdutoImagem','Categoria','Cor','Tamanho'),'conditions' => $conditions));
			Cache::write($cache_name, $dataModel,'sql');
		}
		else
		{
			$dataModel = $cache_data;
		}
		
		$produtos = $dataModel;
		
		
		$xml = $xml_topo;
		$modelo = $xml_template;
		$count = 1;
		foreach ($produtos as $produto):
		
			if(!empty($produto['ProdutoImagem'][0]['filename'])){
				$imagem='/uploads/produto_imagem/filename/'.$produto['ProdutoImagem'][0]['filename'];
			}else{
				$imagem='/img/site/logo.png';
			}
			
			if(!empty($produto['Categoria']['nome'])){
				$categoria = str_replace('&','',$produto['Categoria']['nome']);
			}
			
			$precocerto=$produto['Produto']['preco'];
			if($produto['Produto']['preco_promocao']>0){
				$precocerto=$produto['Produto']['preco_promocao'];
			}
			$parcelas = floor($precocerto/Configure::read('Loja.valor_minimo_parcelamento'));
			
			if($parcelas<=0){
				$parcelas =1;
			}else if($parcelas>10){
				$parcelas=10;
			}
			$parcelado = $precocerto / $parcelas;
			
			$parcelado = number_format($parcelado, 2, ",", ".");
			$precocerto = number_format($precocerto, 2, ",", ".");
			
			$nomecerto = str_replace('&','',$produto['Produto']['nome'] . ' '. $produto['Cor']['nome']. ' '.$produto['Tamanho']['nome']);
			$nomecerto = str_replace(';','',$nomecerto);
			
			$bloco = $modelo;
			$bloco = str_replace('[[cor]]',$produto['Cor']['nome'],$bloco);
			$bloco = str_replace('[[tamanho]]',$produto['Tamanho']['nome'],$bloco);
			$bloco = str_replace('[[id_produto]]',$produto['Produto']['id'],$bloco);
			$bloco = str_replace('[[link_produto]]',$this->Html->Url("/" . low(Inflector::slug($nomecerto, '-')) . '-prod-' . $produto['Produto']['id'],true).'.html',$bloco);
			$bloco = str_replace('[[nome]]',$nomecerto,$bloco);
			$bloco = str_replace('[[nome]]',$nomecerto,$bloco);
			$bloco = str_replace('[[disponibilidade]]',($produto[0]['disponivel']> 0 )? 'Entrega imediata':'Indisponível',$bloco);
			$bloco = str_replace('[[imagem]]',$this->Html->Url((($imagem)),true),$bloco);
			$bloco = str_replace('[[preco]]','R$ '.$precocerto,$bloco);
			$bloco = str_replace('[[parcelamento]]','ou '.$parcelas.'x de R$ '.$parcelado.' sem juros',$bloco);
			$bloco = str_replace('[[categoria]]',$categoria,$bloco);
			$bloco = str_replace('[[descricao]]',$produto['Produto']['descricao'],$bloco);
			$bloco = str_replace('[[codigo]]',$produto['Produto']['codigo'],$bloco);
			$xml  .= $bloco;
			$count++;
		endforeach;
		$xml_rodape = str_replace('[[count]]',$count,$xml_rodape);
		$xml .= $xml_rodape;
		die($xml);
	}
	
    function admin_index($tipo) {
		if(!$tipo){
			$this->Session->setFlash('Parâmetros inválidos.', 'flash/error');
			$this->redirect(array('controller'=>'home','action' => 'index'));
		}
        if (!empty($this->data)) {
			$this->Produto->recursive = -1;
			$this->Produto->unbindModel(array('belongsTo'=>array('Sexo','Fragrancia','Cor','Fabricante','Categoria')));
			if($tipo=='buscape'){
				$this->Produto->updateAll( array('Produto.xml_buscape' => '0'),array('Produto.xml_buscape' => '1'));
				$this->Produto->updateAll( array('Produto.xml_buscape' => true),array('Produto.id' => $this->data['Geraxml']['Produtos']));
			}else if($tipo=='googleshopping'){
				$this->Produto->updateAll( array('Produto.xml_googleshopping' => '0'),array('Produto.xml_googleshopping' => '1'));
				$this->Produto->updateAll( array('Produto.xml_googleshopping' => true),array('Produto.id' => $this->data['Geraxml']['Produtos']));
			}else if($tipo=='shopmania'){
				$this->Produto->updateAll( array('Produto.xml_shopmania' => '0'),array('Produto.xml_shopmania' => '1'));
				$this->Produto->updateAll( array('Produto.xml_shopmania' => true),array('Produto.id' => $this->data['Geraxml']['Produtos']));
			}else if($tipo=='ubiubi'){
				$this->Produto->updateAll( array('Produto.xml_ubiubi' => '0'),array('Produto.xml_ubiubi' => '1'));
				$this->Produto->updateAll( array('Produto.xml_ubiubi' => true),array('Produto.id' => $this->data['Geraxml']['Produtos']));
			}else if($tipo=='jacotei'){
				$this->Produto->updateAll( array('Produto.xml_jacotei' => '0'),array('Produto.xml_jacotei' => '1'));
				$this->Produto->updateAll( array('Produto.xml_jacotei' => true),array('Produto.id' => $this->data['Geraxml']['Produtos']));
			}
			$this->Session->setFlash('Dados salvos.', 'flash/success');
			$this->redirect(array('action' => 'index/'.$tipo));
        }
		
		//estoque
		//tamanho
		//preco
		if($tipo=='buscape'){
			$titulo = 'Busca Pé';
			$produtos = $this->Produto->find('all', array('contain'=>array('Tamanho'),'fields' => "*,(`Produto`.`quantidade` - ifnull((select sum(`pedido_itens`.`quantidade`) AS `sum(``pedido_itens``.``quantidade``)` from (`pedido_itens` join `pedidos`) where ((`Produto`.`id` = `pedido_itens`.`produto_id`) and (`pedido_itens`.`pedido_id` = `pedidos`.`id`) and (`pedidos`.`finalizado` = 0))),0)) as disponivel",'conditions' => array('xml_buscape ='=>true)));
		}else if($tipo=='googleshopping'){
			$titulo = 'Google Shopping';
			$produtos = $this->Produto->find('all', array('contain'=>array('Tamanho'),'fields' => "*,(`Produto`.`quantidade` - ifnull((select sum(`pedido_itens`.`quantidade`) AS `sum(``pedido_itens``.``quantidade``)` from (`pedido_itens` join `pedidos`) where ((`Produto`.`id` = `pedido_itens`.`produto_id`) and (`pedido_itens`.`pedido_id` = `pedidos`.`id`) and (`pedidos`.`finalizado` = 0))),0)) as disponivel",'conditions' => array('Produto.xml_googleshopping'=>true)));
		}else if($tipo=='shopmania'){
			$titulo = 'Shopping Mania';
			$produtos = $this->Produto->find('all', array('contain'=>array('Tamanho'),'fields' => "*,(`Produto`.`quantidade` - ifnull((select sum(`pedido_itens`.`quantidade`) AS `sum(``pedido_itens``.``quantidade``)` from (`pedido_itens` join `pedidos`) where ((`Produto`.`id` = `pedido_itens`.`produto_id`) and (`pedido_itens`.`pedido_id` = `pedidos`.`id`) and (`pedidos`.`finalizado` = 0))),0)) as disponivel",'conditions' => array('xml_shopmania ='=>true)));
		}else if($tipo=='ubiubi'){
			$titulo = 'Ubiubi';
			$produtos = $this->Produto->find('all', array('contain'=>array('Tamanho'),'fields' => "*,(`Produto`.`quantidade` - ifnull((select sum(`pedido_itens`.`quantidade`) AS `sum(``pedido_itens``.``quantidade``)` from (`pedido_itens` join `pedidos`) where ((`Produto`.`id` = `pedido_itens`.`produto_id`) and (`pedido_itens`.`pedido_id` = `pedidos`.`id`) and (`pedidos`.`finalizado` = 0))),0)) as disponivel",'conditions' => array('xml_ubiubi ='=>true)));		
		}else if($tipo=='jacotei'){
			$titulo = 'Já Cotei';
			$produtos = $this->Produto->find('all', array('contain'=>array('Tamanho'),'fields' => "*,(`Produto`.`quantidade` - ifnull((select sum(`pedido_itens`.`quantidade`) AS `sum(``pedido_itens``.``quantidade``)` from (`pedido_itens` join `pedidos`) where ((`Produto`.`id` = `pedido_itens`.`produto_id`) and (`pedido_itens`.`pedido_id` = `pedidos`.`id`) and (`pedidos`.`finalizado` = 0))),0)) as disponivel",'conditions' => array('xml_jacotei ='=>true)));		
		}
		
		
		foreach($produtos as $produto){
			if($produto[0]['disponivel'] > 0){
				$preco = $produto['Produto']['preco_promocao']>0?$produto['Produto']['preco_promocao']:$produto['Produto']['preco'];
				$produtos_arr[$produto['Produto']['id']] = $produto['Produto']['codigo'].' - '.$produto['Produto']['nome'].' - '.$produto['Tamanho']['nome'].' - Preço '.$preco;
			}
		}
		
		$this->set('produtossel', $produtos_arr);
		$this->set('titulo', $titulo);
    }
	
    function admin_ajax_produtos() {
		$query = $this->params['form']['query'];
        
        $selecteds = $this->params['form']['selecteds'];
        if ($query) {
            $produtos = $this->Produto->find('all', array('fields'=>"*,(`Produto`.`quantidade` - ifnull((select sum(`pedido_itens`.`quantidade`) AS `sum(``pedido_itens``.``quantidade``)` from (`pedido_itens` join `pedidos`) where ((`Produto`.`id` = `pedido_itens`.`produto_id`) and (`pedido_itens`.`pedido_id` = `pedidos`.`id`) and (`pedidos`.`finalizado` = 0))),0)) as disponivel", 'contain'=>array('Tamanho'),'limit' => '100', 'conditions' => array('NOT' => array('Produto.id' => $selecteds), 'OR' => array('Produto.tag LIKE' => '%' . $query . '%', 'Produto.codigo LIKE' => '%' . $query . '%', 'Produto.descricao LIKE' => '%' . $query . '%', 'Produto.nome LIKE' => '%' . $query . '%'))));
			$produtos_arr = array();
			foreach($produtos as $produto){
				if($produto[0]['disponivel'] > 0){
					$preco = $produto['Produto']['preco_promocao']>0?$produto['Produto']['preco_promocao']:$produto['Produto']['preco'];
					$produtos_arr[$produto['Produto']['id']] = $produto['Produto']['codigo'].' - '.$produto['Produto']['nome'].' - '.$produto['Tamanho']['nome'].' - Preço '.$preco;
				}
			}
			die(json_encode($produtos_arr));
        } else {
            die(json_encode(false));
        }
    }
	
}