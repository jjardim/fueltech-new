<?php

class AviseController extends AppController {

    var $name = 'Avise';
    var $uses = array('Avise');
    var $components = array('Email','Filter');
	var $helpers = array('Calendario');
    function admin_index() {
		
		$filtros = array();
        if ($this->data["Filter"]["filtro"]) {
            $filtros['filtro'] = "Avise.nome LIKE '%{%value%}%' OR Avise.email = '{%value%}' ";
        }
		
        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();
		
        $this->paginate = array('recursive' => 1, 'limit' => 24, 'conditions' => array('Avise.status' => true));
        $avise = $this->paginate('Avise', $conditions);
        $this->set('avises', $avise);
    }
	function admin_enviar() {
        //$this->layout = false;
        //$this->render(false);
		$avise = $this->Avise->find('all',array('conditions'=>array('Avise.status'=>true)));
		$count_sim = false;
        
		App::import('Model','Produto');
		$this->Produto = new Produto();
		
		foreach($avise as $valor){
            $this->Avise->id = $valor['Avise']['id'];
			$produto = $this->Produto->find('first', array('recursive' => 1, 'contain' => 'ProdutoImagem', 'conditions' => array('AND' => array('Produto.quantidade_disponivel >' => 0, 'Produto.id' => $valor['Avise']['produto_id'], 'Produto.status' => 1)) ));
			
			if($produto){
				$this->Avise->id = $valor['Avise']['id'];
				$count_sim = true;
				$this->Avise->saveField('status', 0);
				$this->disponivel($valor,$produto);				
            }
        }
		if($count_sim){
			$this->Session->setFlash('Rotina rodada com sucesso. Os clientes serão notificados por email.', 'flash/success');
		}else{
			$this->Session->setFlash('Você não possui produtos para enviar notificações.', 'flash/error');
		}
		$this->redirect(array('action' => 'index'));
    }
	public function disponivel($usuario,$data) {
		
		App::import("helper", "Html");
		$this->Html = new HtmlHelper();
		
		App::import("helper", "String");
		$this->String = new StringHelper();
		
		Configure::write('debug', 0);
		if (Configure::read('Loja.smtp_host') != 'localhost') {
			$this->Email->smtpOptions = array(
				'port' => (int)Configure::read('Loja.smtp_port'),
				'host' => Configure::read('Loja.smtp_host'),
				'username' => Configure::read('Loja.smtp_user'),
				'password' => Configure::read('Loja.smtp_password')
			);		
		}

		$this->Email->delivery = 'smtp';
		$this->Email->lineLength = 120;
		$this->Email->sendAs = 'html';
		$this->Email->from          = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_compra') . ">";
		$this->Email->to            = "{$usuario['Avise']['nome']} <{$usuario['Avise']['email']}>";
		$this->Email->subject       = Configure::read('Loja.nome') ." Produto disponível";
		
		if(Configure::read('Reweb.nome_bcc')){
			$bccs = array();
			$assunto = Configure::read('Reweb.nome_bcc');
			$emails = Configure::read('Reweb.email_bcc');
			foreach(explode(';',$emails) as $bcc){
				$bccs[] = "{$assunto} <{$bcc}>";
			}
			$this->Email->bcc = $bccs;
		}	
		
		$produto_preco = $data['Produto']['preco'];
		if($data['Produto']['preco_promocao']>0){
			$produto_preco = $data['Produto']['preco_promocao'];
		}
			
		$produto_imagem = str_replace('uploads/produto_imagem/filename/','',$data['ProdutoImagem'][0]['filename']);
		$produto_imagem = $this->Html->image($this->Html->Url("/resizer/view/110/290/false/true/" .$produto_imagem,true), array("alt" => $data['Produto']['nome']));
		$produto_url = 	Router::url("/" . low(Inflector::slug($data['Produto']['nome'], '-')) . '-' . 'prod-' . $data['Produto']['id'],true).".html";
       $email = str_replace(array('{USUARIO_NOME}','{PRODUTO_NOME}','{PRODUTO_IMAGEM}','{PRODUTO_URL}','{PRODUTO_PRECO}','{LOJA_NOME}'),
	   array(
		$usuario['Avise']['nome'],
		$data['Produto']['nome'],
		$produto_imagem,
		$produto_url,
		'R$ '.$this->String->bcoToMoeda($produto_preco),
		Configure::read('Loja.smtp_assinatura_email')
		),Configure::read('LojaTemplate.aviseme')
		);		
		//setando os dados para o email
		if ($this->Email->send($email)) {
			return true;
		} else {
			return false;
		}
	}
}