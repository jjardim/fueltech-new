<?php

class FabricantesController extends AppController {

    var $name = 'Fabricantes';
    var $components = array('Session');
    var $helpers = array('Image','Calendario', 'String', 'Javascript');

    function admin_index() {
        $this->Fabricante->recursive = 0;
        $this->set('fabricantes', $this->paginate());
    }

    function admin_add() {
        if (!empty($this->data)) {
            if ($this->Fabricante->saveAll($this->data, array('validate' => 'only'))) {
                //salva condicao
                $this->Fabricante->create();
                $this->Fabricante->save($this->data);
                //salva imagem
                App::import("model", "FabricanteImagem");
                $this->FabricanteImagem = new FabricanteImagem();
                $this->data['FabricanteImagem']['fabricante_id'] = $this->Fabricante->id;
                $this->FabricanteImagem->save($this->data);
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Parâmetro inválidos','flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            App::import("model", "FabricanteImagem");
            $this->FabricanteImagem = new FabricanteImagem();
            if ($this->Fabricante->save($this->data, array('validate' => 'only')) && $this->FabricanteImagem->save($this->data, array('validate' => 'only'))) {
                //salva condicao
                $this->Fabricante->save($this->data);
                //salva imagem
                    if (isset($this->data['FabricanteImagem']['id'])) {
                        $this->FabricanteImagem->delete($this->data['FabricanteImagem']['id']);
                    }
                $this->FabricanteImagem->create();
                $this->FabricanteImagem->save($this->data);

                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Fabricante->read(null, $id);
            if (!$this->data) {
                $this->redirect(array('action' => 'index'));
            }
        }
    }
	function admin_delete($id = null) {
		if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Fabricante->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}
    
}

?>