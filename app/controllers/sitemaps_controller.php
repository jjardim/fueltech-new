<?php
class SitemapsController extends AppController{
    var $name = 'Sitemaps';
    var $uses = array('Pagina', 'Produto','Categoria');
    var $helpers = array('Time');	
    function index (){    
		$this->render(false);
		$this->layout = false;
        Configure::write ('debug', 0);		
		App::import("helper", "Html");
        $this->Html = new HtmlHelper();		
		App::import("helper", "Time");
        $time = new TimeHelper();		
		header("Content-Type: text/xml; charset=UTF-8");		
        $categorias = $this->Categoria->find('all', array('conditions' => array('Categoria.status'=>true)));
        $produtos = $this->Produto->find('all', array('fields'=>array('Produto.nome','Produto.id','Produto.modified','Produto.created'),'recursive'=>-1,'conditions' => array('status'=>true)));
        $paginas = $this->Pagina->find('all', array('recursive'=>-1,'conditions' => array('status'=>true)));		
		$xml ='<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"><url><loc>'.Router::url('/',true).'</loc><changefreq>daily</changefreq><priority>1.0</priority></url>';
			foreach ($produtos as $produto):
				$xml  .= '<url><loc>'.$this->Html->Url("/" . low(Inflector::slug($produto['Produto']['nome'], '-')) . '-prod-' . $produto['Produto']['id'],true).'.html</loc><lastmod>'.$time->toAtom($produto['Produto']['modified']).'</lastmod><changefreq>daily</changefreq><priority>1.0</priority></url>';
			endforeach; 
			foreach ($paginas as $pagina):
				$pag = $this->Html->Url("/" . low(Inflector::slug($pagina['Pagina']['nome'], '-')),true);
				$pag  .= (!empty($pagina['Pagina']['url'])?$pagina['Pagina']['url']:$pag);
				$xml .= '<url><loc>'.$pag.'</loc><lastmod>'.$time->toAtom($pagina['Pagina']['modified']).'</lastmod><changefreq>weekly</changefreq><priority>1.0</priority></url>';
			endforeach; 	
			foreach ($categorias as $categoria):
				if($categoria['Categoria']['parent_id']==0){
					$categoria_url = $this->Html->Url("/" . low(Inflector::slug($categoria['Categoria']['nome'], '-')."-depto-".$categoria['Categoria']['id']),true);
				}else{
					$categoria_url = $this->Html->Url("/" . low(Inflector::slug($categoria['Categoria']['nome'], '-')."-cat-".$categoria['Categoria']['id']),true);
				}				
				$xml .='<url><loc>'.$categoria_url.'</loc><lastmod>'.$time->toAtom($categoria['Categoria']['modified']).'</lastmod><changefreq>daily</changefreq><priority>1.0</priority></url>';
			endforeach;				
	$xml .='</urlset>';
	die($xml);	
    }
} 