<?php
class VariacoesController extends AppController {

	var $name = 'Variacoes';
	var $components = array('Session','Filter');
	var $helpers = array('Calendario','String','Image','Flash','Javascript');
	
	function admin_index() {
	
		//filters
		$filtros = array();
		if (isset($this->data["Filter"]["variacao_tipo_id"])) {
            $filtros['variacao_tipo_id'] = "Variacao.variacao_tipo_id = '{%value%}'";
        }
        if (isset($this->data["Filter"]["valor"])) {
            $filtros['valor'] = "Variacao.valor LIKE '%{%value%}%'";
        }
		if (isset($this->data["Filter"]["status"])) {
            $filtros['status'] = "Variacao.status = '{%value%}'";
        }
		
        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();
		
		if(isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar"){
			$this->admin_exportar($conditions);
		}
		$this->Variacao->recursive = 1;
		
		//$this->paginate = array( 'contain' => array('Produto','Variacao') );
		$this->set('variacoes', $this->paginate($conditions));
		//debug($this->paginate($conditions));
		
		//carrego as VariacaoTipos para os selects
		App::import('Model','VariacaoTipo');
		$this->VariacaoTipo = new VariacaoTipo();		
        $VariacaoTipos = array('' => 'Selecione') + $this->VariacaoTipo->find('list', array('fields' => array('VariacaoTipo.id','VariacaoTipo.nome'), 'order' => array('VariacaoTipo.nome')));
		$this->set('variacao_tipos',$VariacaoTipos);
		
		//carrego os produtos para os selects
		// App::import('Model','Produto');
		// $this->Produto = new Produto();	
		// $produtos = array('' => 'Selecione') + $this->Produto->find('list', array('fields' => array('Produto.id','Produto.nome'), 'order' => array('Produto.nome')));
		// $this->set('produtos',$produtos);
	}

	function admin_add() {
	
		//carrego os produtos para os selects
		// App::import('Model','Produto');
		// $this->Produto = new Produto();	
		
		if (!empty($this->data)) {
		
			$this->Variacao->create();
            
			if ($this->Variacao->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
		
		//carrego as VariacaoTipos para os selects
		App::import('Model','VariacaoTipo');
		$this->VariacaoTipo = new VariacaoTipo();		
        $VariacaoTipos = array('' => 'Selecione') + $this->VariacaoTipo->find('list', array('fields' => array('VariacaoTipo.id','VariacaoTipo.nome'), 'order' => array('VariacaoTipo.nome')));
		$this->set('variacao_tipos',$VariacaoTipos);
		
		//set linguas
		App::import('Model','Linguagem');
		$this->Linguagem = new Linguagem();
		$idiomas = $this->Linguagem->find('all', array('fields' => array('Linguagem.id','Linguagem.codigo','Linguagem.nome','Linguagem.thumb_filename', 'Linguagem.thumb_dir'), 'conditions' => array('Linguagem.status' => true, 'Linguagem.externo' => 0)));
		$this->set('idiomas', $idiomas);
		
        // $produtos = array('' => 'Selecione') + $this->Produto->find('list', array('fields' => array('Produto.id','Produto.nome'), 'order' => array('Produto.nome')));
		// $this->set('produtos',$produtos);
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash('Parâmetro inválidos','flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			$this->data['Variacao']['id'] = $id;
			$this->Variacao->id = $id;
			
			if( $this->data['Variacao']['thumb_remove'] == 1 ){
				$this->Variacao->remover_thumb($this->data['Variacao']['id']);
			}
                 
			if ($this->Variacao->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Variacao->read(null, $id);
		}
		
		//carrego as VariacaoTipos para os selects
		App::import('Model','VariacaoTipo');
		$this->VariacaoTipo = new VariacaoTipo();		
        $VariacaoTipos = array('' => 'Selecione') + $this->VariacaoTipo->find('list', array('fields' => array('VariacaoTipo.id','VariacaoTipo.nome'), 'order' => array('VariacaoTipo.nome')));
		$this->set('variacao_tipos',$VariacaoTipos);
		
		//set linguas
		App::import('Model','Linguagem');
		$this->Linguagem = new Linguagem();
		$idiomas = $this->Linguagem->find('all', array('fields' => array('Linguagem.id','Linguagem.codigo','Linguagem.nome','Linguagem.thumb_filename', 'Linguagem.thumb_dir'), 'conditions' => array('Linguagem.status' => true, 'Linguagem.externo' => 0)));
		$this->set('idiomas', $idiomas);
		
		//carrego os produtos para os selects
		// App::import('Model','Produto');
		// $this->Produto = new Produto();		
        // $produtos = array('' => 'Selecione') + $this->Produto->find('list', array('fields' => array('Produto.id','Produto.nome'), 'order' => array('Produto.nome')));
		// $this->set('produtos',$produtos);
	}
	function admin_delete($id = null) {
		if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Variacao->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}
	
	public function admin_exportar($conditions){
	
		App::import('Helper', 'Calendario');
		$this->Calendario = new CalendarioHelper();
		
		$rows = $this->Variacao->find('all',array('conditions' => $conditions));
		
		$table = "<table>";
		$table .= "
				<tr bgcolor=\"#CECECE\">
					<td><strong>Id</strong></td>
					<td><strong>".iconv("UTF-8", "ISO-8859-1//IGNORE","Variação")."</strong></td>
					<td><strong>".iconv("UTF-8", "ISO-8859-1//IGNORE","Valor")."</strong></td>
					<td><strong>Agrupador</strong></td>
					<td><strong>Status</strong></td>
					<td><strong>Criado</strong></td>
					<td><strong>Editado</strong></td>
				</tr>";
		foreach ($rows as $row) {
			$status = ( $row['Variacao']['status'] ) ? "Ativo" : "Inativo";
			$table .= "
				<tr>
					<td>".$row['Variacao']['id']."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['VariacaoTipo']['nome'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Variacao']['valor'])."</td>
					<td>".$row['Variacao']['agrupador']."</td>
					<td>".$status."</td>
					<td>".$this->Calendario->DataFormatada("d-m-Y",  $row['Variacao']['created'])."</td>
					<td>".$this->Calendario->DataFormatada("d-m-Y",  $row['Variacao']['modified'])."</td>
				</tr>";
		}
		$table .= "</table>";
		
		App::import("helper", "String");
		$this->String = new StringHelper();
		$this->layout = false;
		$this->render(false);
		set_time_limit(0);		
		header('Content-type: application/x-msexcel');
		$filename = "Variacao_produtos_" . date("d_m_Y_H_i_s");
		header('Content-Disposition: attachment; filename='.$filename.'.xls');
		header('Pragma: no-cache');
		header('Expires: 0');
		
		die($table);
	}

	public function admin_get_ajax_variacoes() {
		$this->layout = 'ajax';
		$variacoes = $this->Variacao->find("all", array(
										'recursive'=>-1,
										'fields' => array('Variacao.id','Variacao.valor'),
										'conditions' => array('Variacao.variacao_tipo_id' => $this->params['form']['variacao_id'])
										)
							);
		$final = array();
		if($variacoes){
			foreach($variacoes as $variacoes){
				$final[$variacoes['Variacao']['id']] = $variacoes['Variacao']['valor'];
			}
		}
		die(json_encode($final));
    }
	
	public function ajax_get_cor_fabricante($fabricante_id, $ajax = true){
		
		App::import("Model","VariacaoProduto");
		$this->VariacaoProduto = new VariacaoProduto();
		
		App::import("Model","Variacao");
		$this->Variacao = new Variacao();
		
		App::import("Model","Produto");
		$this->Produto = new Produto();
		
		App::import('Component', 'Session');
		$this->Session = new SessionComponent();
		//$this->load('Component','Session');
		
		$conditions = array(
							"Produto.id" => $this->Session->read("ProdutosCategoria"), 
							"Produto.fabricante_id" => $fabricante_id,
							//"Produto.status >" => 0
							);
		$produtos_ids = $this->Produto->find("list", array("recursive" => -1, "fields" => "Produto.id", "conditions" => $conditions));
		
		$variacoes_cores = $this->Variacao->find("list", array(
													'recursive'	=>	1,
													'contain'	=>  array('VariacaoTipo'),
													'joins' 	 => array(
																		array(
																			'table' 	 => 'variacoes_produtos',
																			'alias' 	 => 'VariacaoProduto',
																			'type'  	 => 'LEFT',
																			'conditions' => array('VariacaoProduto.variacao_id = Variacao.id')
																		)
																	),
													'fields' => array('Variacao.id','Variacao.valor'),
													'conditions' => array('VariacaoProduto.produto_id' => $produtos_ids, 'VariacaoTipo.codigo' => 'cor')
													)
							);
		if($ajax){
			$this->layout = 'ajax';
			die(json_encode($variacoes_cores));
		}else{
			return $variacoes_cores;
		}
	}
	
	public function ajax_get_tamanho_cor($fabricante_id, $cor_id, $ajax = true){
		$this->layout = 'ajax';
		
		App::import("Model","VariacaoProduto");
		$this->VariacaoProduto = new VariacaoProduto();
		
		App::import("Model","Produto");
		$this->Produto = new Produto();
		
		$conditions = array(
							"Produto.id" => $this->Session->read("ProdutosCategoria"), 
							"Produto.status >" => 0,
							"Produto.fabricante_id" => $fabricante_id,
							"VariacaoProduto.variacao_id" => $cor_id
							);
		$produtos_ids = $this->Produto->find("list", array(
													"recursive" => 1, 
													'joins' 	 => array(
																		array(
																			'table' 	 => 'variacoes_produtos',
																			'alias' 	 => 'VariacaoProduto',
																			'type'  	 => 'LEFT',
																			'conditions' => array('VariacaoProduto.produto_id = Produto.id')
																		)
																	),
													"fields" => "Produto.id", 
													"conditions" => $conditions)
											);
		
		//$filhos = $this->Produto->children($produtos_ids);
		$conditions2 = array(
							"Produto.parent_id" => $produtos_ids,
							"Produto.status >" => 0
							);
		$filhos_ids = $this->Produto->find("list", array(
													"recursive" => 1, 
													"fields" => "Produto.id", 
													"conditions" => $conditions2)
											);
		//debug($filhos_ids);
		$variacoes_tamanho = $this->Variacao->find("list", array(
													'recursive'	=>	1,
													'contain'	=>  array('VariacaoTipo'),
													'joins' 	 => array(
																		array(
																			'table' 	 => 'variacoes_produtos',
																			'alias' 	 => 'VariacaoProduto',
																			'type'  	 => 'LEFT',
																			'conditions' => array('VariacaoProduto.variacao_id = Variacao.id')
																		)
																	),
													'fields' => array('Variacao.id','Variacao.valor'),
													'conditions' => array('VariacaoProduto.produto_id' => am($produtos_ids, $filhos_ids), 'VariacaoTipo.codigo' => 'tamanho'),
													'group' => 'Variacao.valor'
												)
							);
		
		if($ajax){
			$this->layout = 'ajax';
			die(json_encode($variacoes_tamanho));
		}else{
			return $variacoes_tamanho;
		}
	}
}
?>