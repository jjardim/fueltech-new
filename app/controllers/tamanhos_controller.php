<?php

class TamanhosController extends AppController {

    var $name = 'Tamanhos';
    var $components = array('Session');
    var $helpers = array('Image','Calendario', 'String', 'Javascript');

    function admin_index() {
        $this->Tamanho->recursive = 0;
        $this->set('tamanhos', $this->paginate());
    }

    function admin_add() {
        if (!empty($this->data)) {
            if ($this->Tamanho->saveAll($this->data)) {              
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Parâmetro inválidos','flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->Tamanho->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Tamanho->read(null, $id);
            if (!$this->data) {
                $this->redirect(array('action' => 'index'));
            }
        }
    }
	function admin_delete($id = null) {
		if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
       if(!$this->Tamanho->Produto->find('first',array('conditions'=>array('Produto.tamanho_id'=>$id)))){
			if ($this->Tamanho->delete($id)) {
				$this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
				$this->redirect(array('action' => 'index'));
			}
		}else{
			$this->Session->setFlash('O Registro não pode ser deletado, tente novamente., pois possui registros vinculados', 'flash/error');
			$this->redirect(array('action' => 'index'));
		}
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}
    
}

?>