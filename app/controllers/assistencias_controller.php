<?php
class AssistenciasController extends AppController {

	var $name = 'Assistencias';
	var $components = array('Session','Filter');
	var $helpers = array('Calendario','String','Image','Flash','Javascript','Marcas');
	
	function admin_index() {
		//filters
		$filtros = array();
        if (isset($this->data["Filter"]["marca"])) {
            $filtros['marca'] = "Assistencia.marca LIKE '%{%value%}%'";
        }
		
        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();
		
		if(isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar"){
			$this->admin_exportar($conditions);
		}
		
		$this->Assistencia->recursive = 0;
		$this->set('assistencias', $this->paginate($conditions));
	}
	public function admin_exportar($conditions){
		
		App::import('Helper', 'Calendario');
		$this->Calendario = new CalendarioHelper();
		
		$rows = $this->Assistencia->find('all',array('conditions' => $conditions));
		
		$table = "<table>";
		$table .= "
				<tr bgcolor=\"#CECECE\">
					<td><strong>Id</strong></td>
					<td><strong>Marca</strong></td>
					<td><strong>URL</strong></td>
					<td><strong>Status</strong></td>
					<td><strong>Criado</strong></td>
					<td><strong>Modificado</strong></td>
				</tr>";
		foreach ($rows as $row) {
			$status = ( $row['Assistencia']['status'] ) ? "Ativo" : "Inativo";
			$table .= "
				<tr>
					<td>".$row['Assistencia']['id']."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Assistencia']['marca'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Assistencia']['url'])."</td>
					<td>".$status."</td>
					<td>".$this->Calendario->DataFormatada("d-m-Y",  $row['Assistencia']['created'])."</td>
					<td>".$this->Calendario->DataFormatada("d-m-Y",  $row['Assistencia']['modified'])."</td>
				</tr>";
		}
		$table .= "</table>";
		
		App::import("helper", "String");
		$this->String = new StringHelper();
		$this->layout = false;
		$this->render(false);
		set_time_limit(0);		
		header('Content-type: application/x-msexcel');
		$filename = "assistencias_" . date("d_m_Y_H_i_s");
		header('Content-Disposition: attachment; filename='.$filename.'.xls');
		header('Pragma: no-cache');
		header('Expires: 0');
		
		die($table);
	}
	function admin_add() {
		if (!empty($this->data)) {
		
			$this->Assistencia->create();
            
			if ($this->Assistencia->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash('Parâmetro inválidos','flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			$this->data['Assistencia']['id'] = $id;
			$this->Assistencia->id = $id;
                 
			if ($this->Assistencia->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Assistencia->read(null, $id);
		}
	}
	function admin_delete($id = null) {
		if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Assistencia->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}
	
	public function get_estado_por_marca($marca) {
		$this->render(false);
        $this->layout = false;
		$assistencias = $this->Assistencia->find('all',array('fields' =>  array('Assistencia.estado') ,'recursive'=>-1,'order' => array('Assistencia.estado'), 'conditions'  =>  array('Assistencia.status'=>true, 'Assistencia.marca' => $marca)));
		die(json_encode(Set::combine($assistencias, '{n}/Assistencia/estado', '{n}/Assistencia/estado')));
    }
	
	public function get_assistencia() {
		$this->render(false);
        $this->layout = false;
		$assistencias = $this->Assistencia->find('all',array('fields' =>  array('Assistencia.empresa', 'Assistencia.endereco', 'Assistencia.telefone','Assistencia.email','Assistencia.contato','Assistencia.url') ,'recursive'=>-1,'conditions'  =>  array('Assistencia.status'=>true, 'Assistencia.marca' => $this->params['form']['var_marca'], 'Assistencia.estado' => $this->params['form']['var_uf'])));
		die(json_encode($assistencias));
    }
	
}
?>