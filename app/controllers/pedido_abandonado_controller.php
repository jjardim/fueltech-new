<?php
class PedidoAbandonadoController extends AppController {

	var $name = 'PedidoAbandonado';
	var $components = array('Session');
	var $helpers = array('Calendario','String','Image','Flash','Javascript');
	
	function admin_index() {
		$this->PedidoAbandonado->recursive = 0;
		$this->set('pedidos_abandonados', $this->paginate());
	}	
}
?>