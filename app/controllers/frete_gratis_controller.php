<?php

class FreteGratisController extends AppController {

	var $name = 'FreteGratis';
	var $components = array('Filter','Session');
	var $helpers = array('Form','Session','String','Javascript'	,'Calendario');
	var $uses = array('FreteGratis');
	
	function admin_index() {
		
        $filtros['codigo'] = "FreteGratis.codigo = '{%value%}'";        

        if ($this->data["Filter"]["cep_inicial"]) {
            $filtros['cep_inicial'] = "FreteGratis.cep_inicial >= '{%value%}' ";
        }
		if ($this->data["Filter"]["cep_final"]) {
            $filtros['cep_final'] = "FreteGratis.cep_final <= '{%value%}' ";
        }

        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();
        $this->paginate = array('limit' => 24);
        $fretes = $this->paginate('FreteGratis', $conditions);
        $this->set('fretes', $fretes);
	}

	function admin_add() {
		$produtos_produtos = array();	
		$categorias_categorias = array();
		$this->loadModel('Categoria');
		if (!empty($this->data)) {
			$this->FreteGratis->create();
			if($this->data['FreteGratis']['codigo']=='PRODUTO'){
				$this->data['FreteGratis']['apartir_de'] = 0 ;
			}
			if ($this->FreteGratis->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				 $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		
			$this->loadModel('Produto');
			if($this->data['FreteGratis']['produto_id']){
				$produtos = $this->Produto->find('all',array('recursive'=>-1,'fields'=>array('id','nome','sku'),'conditions'=>array('Produto.id'=>$this->data['FreteGratis']['produto_id'])));
			
				
				foreach($produtos as &$produto){
					$produtos_produtos[$produto['Produto']['id']] = $produto['Produto']['sku'].' - '.$produto['Produto']['nome'];				
				}
				$this->data['FreteGratis']['produto_id'] = $produtos_produtos;
			}
			if($this->data['FreteGratis']['categoria_id']){
				$categorias = $this->Categoria->find('all',array('recursive'=>-1,'fields'=>array('id','seo_url'),'conditions'=>array('Categoria.id'=>$this->data['FreteGratis']['categoria_id'])));
			
				foreach($categorias as $categoria){
					$categorias_categorias[$categoria['Categoria']['id']] = $categoria['Categoria']['seo_url'];				
				}
				$this->data['FreteGratis']['categoria_id'] = $categorias_categorias;
			}		
		}
	
		
		$this->Categoria->displayField = 'nome';
		
		$produtos_categorias = $this->Categoria->find('list',array('recursive'=>-1,'conditions'=>array('parent_id > '=>0),'fields'=>array('id','seo_url')));
		
		
        $this->set(compact('produtos_categorias','produtos_produtos','categorias_categorias'));
	}
	function admin_edit($id = null) {
		$this->loadModel('Categoria');
		$this->loadModel('Produto');
		if (!$id && empty($this->data)) {
			$this->Session->setFlash('Parametros inválidos', 'flash/error');
			$this->redirect(array('action' => 'index'));
		}
		$produtos_produtos = array();	
		$categorias_categorias = array();	
		if (!empty($this->data)) {
			if($this->data['FreteGratis']['codigo']=='PRODUTO'){
				$this->data['FreteGratis']['apartir_de'] = 0 ;
			}
			if ($this->FreteGratis->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				 $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		
			
			if($this->data['FreteGratis']['produto_id']){
				$produtos = $this->Produto->find('all',array('recursive'=>-1,'fields'=>array('id','nome','sku'),'conditions'=>array('Produto.id'=>$this->data['FreteGratis']['produto_id'])));
			
				
				foreach($produtos as &$produto){
					$produtos_produtos[$produto['Produto']['id']] = $produto['Produto']['sku'].' - '.$produto['Produto']['nome'];				
				}
				$this->data['FreteGratis']['produto_id'] = $produtos_produtos;
			}
			if($this->data['FreteGratis']['categoria_id']){
				$categorias = $this->Categoria->find('all',array('recursive'=>-1,'fields'=>array('id','seo_url'),'conditions'=>array('Categoria.id'=>$this->data['FreteGratis']['categoria_id'])));
			
				foreach($categorias as $categoria){
					$categorias_categorias[$categoria['Categoria']['id']] = $categoria['Categoria']['seo_url'];				
				}
				$this->data['FreteGratis']['categoria_id'] = $categorias_categorias;
			}		
		
		
		}
		if (empty($this->data)) {
			$this->data = $this->FreteGratis->read(null, $id);
	
			
			$produtos_ids = json_decode($this->data['FreteGratis']['produto_id']);
			
			
			$produtos = $this->Produto->find('all',array('recursive'=>-1,'fields'=>array('id','nome','sku'),'conditions'=>array('Produto.id'=>$produtos_ids)));
		
			
			foreach($produtos as $produto){
				$produtos_produtos[$produto['Produto']['id']] = $produto['Produto']['sku'].' - '.$produto['Produto']['nome'];				
			}
			$this->data['FreteGratis']['produto_id'] = $produtos_produtos;
			
			$categorias_ids_tmp = json_decode($this->data['FreteGratis']['categoria_id']);
			$categorias_ids = array();
			if($categorias_ids_tmp){
				foreach($categorias_ids_tmp as $c){
					foreach($c as $v){
						$categorias_ids[$v] = $v;
					}
				}
			}
			
			$categorias = $this->Categoria->find('all',array('recursive'=>-1,'fields'=>array('id','seo_url'),'conditions'=>array('Categoria.id'=>$categorias_ids)));
		
			
			foreach($categorias as $categoria){
				$categorias_categorias[$categoria['Categoria']['id']] = $categoria['Categoria']['seo_url'];				
			}
			$this->data['FreteGratis']['categoria_id'] = $categorias_categorias;
		}
		$this->Categoria->displayField = 'nome';
		
		$produtos_categorias = $this->Categoria->find('list',array('recursive'=>-1,'conditions'=>array('parent_id > '=>0),'fields'=>array('id','seo_url')));
		
        $this->set(compact('produtos_categorias','produtos_produtos','categorias_categorias'));
	}
	
	function admin_delete($id = null) {
		if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->FreteGratis->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}
	
}
?>