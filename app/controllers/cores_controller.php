<?php

class CoresController extends AppController {

    var $name = 'Cores';
    var $components = array('Session');
    var $helpers = array('Image','Calendario', 'String', 'Javascript');

    function admin_index() {
        $this->Cor->recursive = 0;
        $this->set('cores', $this->paginate());
    }

    function admin_add() {
        if (!empty($this->data)) {
            if ($this->Cor->saveAll($this->data)) {                
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Parâmetro inválidos','flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->Cor->save($this->data)) {
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Cor->read(null, $id);
            if (!$this->data) {
                $this->redirect(array('action' => 'index'));
            }
        }
    }
	function admin_delete($id = null) {
		if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
		if(!$this->Cor->Produto->find('first',array('conditions'=>array('Produto.cor_id'=>$id)))){
			if ($this->Cor->delete($id)) {
				$this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
				$this->redirect(array('action' => 'index'));
			}
		}else{
			$this->Session->setFlash('O Registro não pode ser deletado, pois possui registros vinculados', 'flash/error');
			$this->redirect(array('action' => 'index'));
		}
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}
    
}

?>