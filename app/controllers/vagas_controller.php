<?php
class VagasController extends AppController {

	var $name = 'Vagas';
	var $components = array('Session','Filter');
	var $helpers = array('Calendario','String','Image','Flash','Javascript');
	
	function admin_index() {
		//filters
		$filtros = array();
        if (isset($this->data["Filter"]["tipo"])) {
            $filtros['tipo'] = "Vaga.tipo LIKE '%{%value%}%'";
        }
		if (isset($this->data["Filter"]["cargo"])) {
            $filtros['cargo'] = "Vaga.cargo LIKE '%{%value%}%'";
        }
		if (isset($this->data["Filter"]["localidade"])) {
            $filtros['localidade'] = "Vaga.localidade LIKE '%{%value%}%'";
        }
		
        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();
		
		if(isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar"){
			$this->admin_exportar($conditions);
		}
	
		$this->Vaga->recursive = 0;
		$this->set('vagas', $this->paginate($conditions));
	}
	public function admin_exportar($conditions){
		
		App::import('Helper', 'Calendario');
		$this->Calendario = new CalendarioHelper();
		
		$rows = $this->Vaga->find('all',array('conditions' => $conditions));
		
		$table = "<table>";
		$table .= "
				<tr bgcolor=\"#CECECE\">
					<td><strong>Id</strong></td>
					<td><strong>Tipo</strong></td>
					<td><strong>Cargo</strong></td>
					<td><strong>Localidade</strong></td>
					<td><strong>Resumo</strong></td>
					<td><strong>Requisitos</strong></td>
					<td><strong>Status</strong></td>
					<td><strong>Criado</strong></td>
					<td><strong>Modificado</strong></td>
				</tr>";
		foreach ($rows as $row) {
			$status = ( $row['Vaga']['status'] ) ? "Ativo" : "Inativo";
			$table .= "
				<tr>
					<td>".$row['Vaga']['id']."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Vaga']['tipo'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Vaga']['cargo'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Vaga']['localidade'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Vaga']['resumo'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Vaga']['requisitos'])."</td>
					<td>".$status."</td>
					<td>".$this->Calendario->DataFormatada("d-m-Y",  $row['Vaga']['created'])."</td>
					<td>".$this->Calendario->DataFormatada("d-m-Y",  $row['Vaga']['modified'])."</td>
				</tr>";
		}
		$table .= "</table>";
		
		App::import("helper", "String");
		$this->String = new StringHelper();
		$this->layout = false;
		$this->render(false);
		set_time_limit(0);		
		header('Content-type: application/x-msexcel');
		$filename = "vagas_" . date("d_m_Y_H_i_s");
		header('Content-Disposition: attachment; filename='.$filename.'.xls');
		header('Pragma: no-cache');
		header('Expires: 0');
		
		die($table);
	}
	function admin_add() {
		if (!empty($this->data)) {
		
			$this->Vaga->create();
            
			if ($this->Vaga->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash('Parâmetro inválidos','flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			$this->data['Vaga']['id'] = $id;
			$this->Vaga->id = $id;
                 
			if ($this->Vaga->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Vaga->read(null, $id);
		}
	}
	function admin_delete($id = null) {
		if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Vaga->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}
	
}
?>