<?php
class NoticiaTiposController extends AppController {

	var $name = 'NoticiaTipos';
	var $components = array('Session','Filter');
	var $helpers = array('Calendario','String','Image','Flash','Javascript');
	
	function admin_index() {
		//filters
		$filtros = array();
        if (isset($this->data["Filter"]["titulo"])) {
            $filtros['marca'] = "NoticiaTipo.nome LIKE '%{%value%}%'";
        }
		
        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();
		
		if(isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar"){
			$this->admin_exportar($conditions);
		}
		
		$this->NoticiaTipo->recursive = 0;
		$this->set('noticia_tipos', $this->paginate($conditions));
	}
	
	public function admin_exportar($conditions){
		
		App::import('Helper', 'Calendario');
		$this->Calendario = new CalendarioHelper();
		
		$rows = $this->Noticia->find('all',array('conditions' => $conditions));
		
		$table = "<table>";
		$table .= "
				<tr bgcolor=\"#CECECE\">
					<td><strong>Id</strong></td>
					<td><strong>Titulo</strong></td>
					<td><strong>Resumo</strong></td>
					<td><strong>Status</strong></td>
					<td><strong>Criado</strong></td>
					<td><strong>Modificado</strong></td>
				</tr>";
		foreach ($rows as $row) {
			$status = ( $row['Noticia']['status'] ) ? "Ativo" : "Inativo";
			$table .= "
				<tr>
					<td>".$row['Noticia']['id']."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Noticia']['titulo'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Noticia']['resumo'])."</td>
					<td>".$status."</td>
					<td>".$this->Calendario->DataFormatada("d-m-Y",  $row['Noticia']['created'])."</td>
					<td>".$this->Calendario->DataFormatada("d-m-Y",  $row['Noticia']['modified'])."</td>
				</tr>";
		}
		$table .= "</table>";
		
		App::import("helper", "String");
		$this->String = new StringHelper();
		$this->layout = false;
		$this->render(false);
		set_time_limit(0);		
		header('Content-type: application/x-msexcel');
		$filename = "noticias_" . date("d_m_Y_H_i_s");
		header('Content-Disposition: attachment; filename='.$filename.'.xls');
		header('Pragma: no-cache');
		header('Expires: 0');
		
		die($table);
	}
	
	function admin_add() {
		if (!empty($this->data)) {
		
			$this->NoticiaTipo->create();
            if($this->data['NoticiaTipo']['nome'] != ""){
				$this->data['NoticiaTipo']['codigo'] = low(Inflector::slug($this->data['NoticiaTipo']['nome'], '-'));
			}
			if ($this->NoticiaTipo->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash('Parâmetro inválidos','flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			$this->data['NoticiaTipo']['id'] = $id;
			$this->NoticiaTipo->id = $id;
            if($this->data['NoticiaTipo']['nome'] != ""){
				$this->data['NoticiaTipo']['codigo'] = low(Inflector::slug($this->data['NoticiaTipo']['nome'], '-'));
			}
			if ($this->NoticiaTipo->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
		if (empty($this->data)) {
			$this->data = $this->NoticiaTipo->read(null, $id);
		}
	}
	
	function admin_delete($id = null) {
		if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->NoticiaTipo->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}
	
}
?>