<?php

class DesejosController extends AppController {

    var $components = array('Session');
    var $helpers = array('Calendario', 'String', 'Javascript', 'Image', 'Parcelamento');
    var $name = 'Desejos';
    var $uses = array('PagamentoCondicao','DesejoProduto', 'Desejo', 'Produto');

    function index() {
        $usuario = $this->Auth->User();
        $this->Desejo->recursive = 1;
        $this->set('desejos', $this->paginate('Desejo', array('usuario_id' => $usuario['Usuario']['id'])));
        $this->set('breadcrumbs', array(array('nome' => 'Desejos', 'link' => '/lista_desejos')));
    }

    function produtos($lista_id = null) {
        $usuario = $this->Auth->User();
        $desejos = $this->Desejo->find('first', array('recursive'=>-1,'conditions' => array('Desejo.id' => $lista_id, 'Desejo.usuario_id' => $usuario['Usuario']['id'])));
        if ($desejos) {
            $this->set('parcelamento', current($this->PagamentoCondicao->find('list', array('fields' => array('parcelas'), 'recursive' => -1, 'order' => 'parcelas DESC'))));
            $produtos = $this->DesejoProduto->find('all', array('recursive'=>-1,'conditions' => array('desejo_id' => $lista_id)));
            $ids = set::extract('/DesejoProduto/produto_id', $produtos);
            $this->DesejoProduto->recursive = 2;
            $this->set('produtos', $this->paginate($this->DesejoProduto, array('produto_id' => $ids)));
            $this->set('desejo', $desejos);
        } else {
            $this->redirect(array('action' => 'index'));
        }
        $this->set('breadcrumbs', array(array('nome' => 'Desejos', 'link' => '/lista_desejos')));
    }

    function produto_delete($id=null, $lista_id=null) {
        $usuario = $this->Auth->User();
        $desejos = $this->Desejo->find('first', array('conditions' => array('Desejo.id' => $lista_id, 'Desejo.usuario_id' => $usuario['Usuario']['id'])));

        if (!empty($desejos) && !empty($lista_id) && !empty($id)) {
            //var_dump($this->DesejoProduto->delete($id),$id);die;
            if ($this->DesejoProduto->delete($id)) {
                $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            } else {
                $this->Session->setFlash('Registro não foi deletado', 'flash/error');
            }
        } else {
            $this->Session->setFlash('Registro não foi deletado', 'flash/error');
        }
        $this->redirect(array('action' => 'produtos', $lista_id));
    }

    function add($produto_id = null) {

        $parcelamento = current($this->PagamentoCondicao->find('list', array('recursive'=>-1,'fields' => array('parcelas'), 'recursive' => -1, 'order' => 'parcelas DESC')));
        $this->set('parcelamento', $parcelamento);

        $usuario = $this->Auth->User();

        if ($produto_id) {
            $produto = $this->Produto->find('first', array('conditions' => array('Produto.id' => $produto_id)));
        }

        if ($produto) {
            $this->set('produto', $produto);
            $desejos = $this->Desejo->find('all', array('conditions' => array('usuario_id' => $usuario['Usuario']['id'], 'status' => true)));
            $this->set('listas', $desejos);
        } else {
            $this->Session->setFlash('Os parametros passados estão inválidos.', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }

        if (!empty($this->data)) {
            if (!empty($this->data['Desejo']['nome'])) {
                $this->data['Desejo']['usuario_id'] = $usuario['Usuario']['id'];
                if ($this->Desejo->save($this->data)) {
                    $this->data['DesejoProduto']['produto_id'] = $this->data['Produto']['id'];
                    $this->data['DesejoProduto']['desejo_id'] = $this->Desejo->id;
                    $this->DesejoProduto->save($this->data);
                } else {
                    $this->Session->setFlash('Os dados não foram salvos, tente novamente.', 'flash/error');
                }
            } else {
                $empty = true;
            }

            if (!empty($this->data['DesejoProduto']['desejo_id'])&&is_array($this->data['DesejoProduto']['desejo_id'])) {
                foreach ($this->data['DesejoProduto']['desejo_id'] as $chave => $valor):
                    if ($valor == 1) {
                        $this->DesejoProduto->id = null;
                        $this->data['DesejoProduto']['produto_id'] = $this->data['Produto']['id'];
                        $this->data['DesejoProduto']['desejo_id'] = $chave;
                        $this->DesejoProduto->save($this->data);
                        $empty = false;
                    }
                endForeach;
            }

            if (isset($empty) && $empty) {
				$this->Session->setFlash('Os dados não foram salvos, tente novamente.', 'flash/error');
                $this->Desejo->invalidate('nome', 'Campo de preenchimento obrigatório, Você deve selecionar uma Lista de desejos ou criar uma nova!');
            } else {
                $this->Session->setFlash('Dados salvos com sucesso!.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            }
        }
		
        $this->set('breadcrumbs', array(array('nome' => 'Desejos', 'link' => '/lista_desejos')));
    }

    function edit($lista_id = null) {

        $usuario = $this->Auth->User();

        if ($lista_id) {
            $desejos = $this->Desejo->find('first', array('conditions' => array('Desejo.id' => $lista_id, 'usuario_id' => $usuario['Usuario']['id'], 'status' => true)));
        } else {
            $this->Session->setFlash('Os parametros passados estão inválidos.', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if (!empty($this->data['Desejo']['nome'])) {
                $this->data['Desejo']['usuario_id'] = $usuario['Usuario']['id'];
                $this->data['Desejo']['id'] = $lista_id;
                if ($this->Desejo->save($this->data)) {
                     $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
                }
            }else{
                 $empty = true;
            }
            if ($empty) {
                $this->Desejo->invalidate('nome', 'Campo de preenchimento obrigatório');
            } else {
                $this->redirect(array('action' => 'index'));
            }
        }

        if (empty($this->data)) {
            $this->data = $this->Desejo->read(null, $lista_id);
            if (!$this->data) {
                $this->redirect(array('action' => 'index'));
            }
        }
        $this->set('breadcrumbs', array(array('nome' => 'Desejos', 'link' => '/lista_desejos')));
    }

    function delete($id) {
        $usuario = $this->Auth->User();
        $desejos = $this->Desejo->find('first', array('conditions' => array('Desejo.id' => $id, 'Desejo.usuario_id' => $usuario['Usuario']['id'])));

        if (!empty($desejos) && !empty($id)) {
            if ($this->Desejo->delete($id)) {
                $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            } else {
                $this->Session->setFlash('Registro não foi deletado, você deve possuir produtos vinculados a esta lista', 'flash/error');
            }
        } else {
            $this->Session->setFlash('Registro não foi deletado', 'flash/error');
        }
        $this->redirect(array('action' => 'produtos', $lista_id));
    }

}

?>