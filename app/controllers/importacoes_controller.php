<?php

class ImportacoesController extends AppController {

    public $uses = array('Setting');

    function admin_index($tipo) {
        App::import('Component', 'Importacao');
        $this->Importacao = new ImportacaoComponent();
        switch ($tipo) {

            case'xml_produto_imagem':
                $this->Importacao->xml_produto_imagem();
                break;
            case'xml_atributo_tipo':
                $this->Importacao->xml_atributo_tipo();
                break;
            case'xml_categorias':
                $this->Importacao->xml_categorias();
                break;
            case'xml_produtos':
                $this->Importacao->xml_produtos();
                break;
            case'xml_categorias_produtos':
                $this->Importacao->xml_categorias_produtos();
                break;
            case'xml_produtos_variacoes':
                $this->Importacao->xml_produtos_variacoes();
                break;
            case'xml_atributos_produtos':
                $this->Importacao->xml_atributos_produtos();
                break;
            case'xml_produto_estoque':
                $this->Importacao->xml_produto_estoque();
                break;
            case'xml_produto_preco':
                $this->Importacao->xml_produto_preco();
                break;
        }
        $this->layout = '';
        $this->render(false);
        die();
    }

}