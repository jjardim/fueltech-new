<?php
class UrlsController extends AppController {

	var $name = 'Urls';
	var $components = array('Session');
	var $helpers = array('Calendario','String','Javascript');
	
	function admin_index() {
		$this->Url->recursive = 0;
		$this->set('urls', $this->paginate());
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->Url->create();
			if ($this->Url->save($this->data)) {
				 $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				 $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash('Parametros inválidos', 'flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {                        
			if ($this->Url->save($this->data)) {
				 $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				 $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Url->read(null, $id);
		}
	}
	function admin_delete($id = null) {
		if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Url->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}
	
}
?>