<?php
class XmlTiposController extends AppController {

	var $name = 'XmlTipos';
	var $components = array('Session');
	var $helpers = array('Calendario','String','Flash','Javascript');
	
	function admin_index() {
		$this->XmlTipo->recursive = 0;
		$this->set('xml_tipos', $this->paginate());
	}

	function admin_add() {
		if (!empty($this->data)) {
		
			$this->XmlTipo->create();
            
			if ($this->XmlTipo->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash('Parâmetro inválidos','flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			$this->data['XmlTipo']['id'] = $id;
			$this->XmlTipo->id = $id;
                 
			if ($this->XmlTipo->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
		if (empty($this->data)) {
			$this->data = $this->XmlTipo->read(null, $id);
		}
	}
	function admin_delete($id = null) {
		if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->XmlTipo->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}	
	function admin_ajax_get_dados_xml(){
		$this->layout = "ajax";
		$xml_tipo_id = $this->params['form']['xml_tipo_id'];
		$dados = $this->XmlTipo->find("first", array(
										'fields' => array('XmlTipo.template_cabecalho','XmlTipo.template_centro','XmlTipo.template_rodape'),
										'conditions' => array('XmlTipo.id' => $xml_tipo_id)
										)
							);
		die(json_encode($dados));
	}
}
?>