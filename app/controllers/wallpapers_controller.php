<?php
class WallpapersController extends AppController {

	var $name = 'Wallpapers';
	var $components = array('Session','Filter');
	var $helpers = array('Calendario','String','Image','Flash','Javascript','Marcas');
	
	function admin_index() {
		//filters
		$filtros = array();
        if (isset($this->data["Filter"]["filtro"])) {
            $filtros['filtro'] = "Wallpaper.nome LIKE '%{%value%}%'";
        }
		
        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();
		
		if(isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar"){
			$this->admin_exportar($conditions);
		}
		
		$this->paginate = array(
            'contain' => array('WallpaperGrade'),
            'recursive' => 1,
            'limit' => 24
        );
		$this->Wallpaper->recursive = 0;
		$this->set('wallpapers', $this->paginate($conditions));
	}
	public function admin_exportar($conditions){
		
		App::import('Helper', 'Calendario');
		$this->Calendario = new CalendarioHelper();
		
		$rows = $this->Wallpaper->find('all',array('conditions' => $conditions));
		
		$table = "<table>";
		$table .= "
				<tr bgcolor=\"#CECECE\">
					<td><strong>Id</strong></td>
					<td><strong>Nome</strong></td>
					<td><strong>Status</strong></td>
					<td><strong>Criado</strong></td>
					<td><strong>Modificado</strong></td>
				</tr>";
		foreach ($rows as $row) {
			$status = ( $row['Wallpaper']['status'] ) ? "Ativo" : "Inativo";
			$table .= "
				<tr>
					<td>".$row['Wallpaper']['id']."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Wallpaper']['nome'])."</td>
					<td>".$status."</td>
					<td>".$this->Calendario->DataFormatada("d-m-Y",  $row['Wallpaper']['created'])."</td>
					<td>".$this->Calendario->DataFormatada("d-m-Y",  $row['Wallpaper']['modified'])."</td>
				</tr>";
		}
		$table .= "</table>";
		
		App::import("helper", "String");
		$this->String = new StringHelper();
		$this->layout = false;
		$this->render(false);
		set_time_limit(0);		
		header('Content-type: application/x-msexcel');
		$filename = "wallpapers_" . date("d_m_Y_H_i_s");
		header('Content-Disposition: attachment; filename='.$filename.'.xls');
		header('Pragma: no-cache');
		header('Expires: 0');
		
		die($table);
	}
	function admin_add() {
		if (!empty($this->data)) {
		
			$this->Wallpaper->create();
            
			if ($this->Wallpaper->save($this->data)) {
				
				if(isset($this->data['WallpaperGrade'])){
					
					App::import('Model','WallpaperGrade');
					$this->WallpaperGrade = new WallpaperGrade();
					
					foreach($this->data['WallpaperGrade'] as $k => $fg){
						$this->data['WallpaperGrade'][$k]['id'] = '';
						$this->data['WallpaperGrade'][$k]['wallpaper_id'] = $this->Wallpaper->id;
						$this->WallpaperGrade->save($this->data['WallpaperGrade'][$k], false);
					}
				}
			
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
		
		//WallpaperTamanho
		App::import('Model','WallpaperTamanho');
		$this->WallpaperTamanho = new WallpaperTamanho();
		$wallpapers_tamanhos = $this->WallpaperTamanho->find('list', array('fields' => array('WallpaperTamanho.id', 'WallpaperTamanho.valor'), 'recursive' => -1, 'conditions' => array('WallpaperTamanho.status' => true)));
		$this->set('wallpaper_tamanhos', array(''=>'Selecione...')+$wallpapers_tamanhos);
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash('Parâmetro inválidos','flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			$this->data['Wallpaper']['id'] = $id;
			$this->Wallpaper->id = $id;
                 
			if ($this->Wallpaper->save($this->data)) {
				
				if(isset($this->data['WallpaperGrade'])){
					
					App::import('Model','WallpaperGrade');
					$this->WallpaperGrade = new WallpaperGrade();
					
					foreach($this->data['WallpaperGrade'] as $k => $fg){
						if(isset($fg['filename']) && !empty($fg['filename']['name'])){
							$this->data['WallpaperGrade'][$k]['id'] = '';
							$this->data['WallpaperGrade'][$k]['wallpaper_id'] = $this->Wallpaper->id;
							$this->WallpaperGrade->save($this->data['WallpaperGrade'][$k], false);
						}elseif(!isset($fg['filename']) && $fg['delete'] == 1){
							$this->WallpaperGrade->deletar($fg['id']);
						}
					} 
				}
				
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Wallpaper->find('first', array(
																'conditions' 	=> array('Wallpaper.id' => $id),
																'contain'		=> array('WallpaperGrade' => array('WallpaperTamanho'))
															)
												);
		}
		//WallpaperTamanho
		App::import('Model','WallpaperTamanho');
		$this->WallpaperTamanho = new WallpaperTamanho();
		$wallpapers_tamanhos = $this->WallpaperTamanho->find('list', array('fields' => array('WallpaperTamanho.id', 'WallpaperTamanho.valor'), 'recursive' => -1, 'conditions' => array('WallpaperTamanho.status' => true)));
		$this->set('wallpaper_tamanhos', array(''=>'Selecione...')+$wallpapers_tamanhos);
	}
	function admin_delete($id = null) {
		if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Wallpaper->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}
	
	function download($id){
		
		App::import('Model','WallpaperGrade');
		$this->WallpaperGrade = new WallpaperGrade();
		
		$arquivo = $this->WallpaperGrade->find('first',array('conditions' => array('WallpaperGrade.id' => $id)));
		
		if(!$arquivo){
			$this->redirect($this->referer());
		}
		
		$dir = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'uploads' . DS . 'wallpaper' . DS . 'filename' . DS;
		$filename = $dir.$arquivo['WallpaperGrade']['filename'];
		
		if (file_exists($filename)) {
			//Parse file get extension and size
			$fsize = filesize($filename);
			$path_parts = pathinfo($filename);
			$ext = strtolower($path_parts["extension"]);
			// Determine Content Type
			switch ($ext) {
				case "pdf":
				$ctype = "application/pdf";
				break;
				
				case "exe":
				$ctype = "application/octet-stream";
				break;
			
				case "zip":
				case "rar":
				$ctype = "application/zip";
				break;
			
				case "doc":
				case "docx":
				$ctype = "application/msword";
				break;
				
				case "xls":
				case "xlsx":
				$ctype = "application/vnd.ms-excel";
				break;
			
				case "ppt":
				case "pptx":
				$ctype = "application/vnd.ms-powerpoint";
				break;
				
				case "gif":
				$ctype = "image/gif";
				break;
				
				case "png":
				$ctype = "image/png";
				break;
				
				case "jpeg":
				case "jpg":
				$ctype = "image/jpg";
				break;
				
				default: $ctype = "application/force-download";
			}
			
			header("Pragma: public");
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private", false);
			header("Content-Type: $ctype");
			header("Content-Description: File Transfer");
			header("Content-Disposition: attachment; filename=" . basename($filename) . ";");
			header("Content-Transfer-Encoding: binary");
			header("Content-Length: " . $fsize);
			readfile($filename) or die(‘Errors’);
			exit(0);
		}
	}
	
}
?>