<?php
/**
 * Tipo de Pagamento

    Código Descrição
    1 Cartão de Crédito
    2 Bloqueto Bancário
    3 Débito Bancário
    4 Débito Bancário – Dinheiro
    5 Débito Bancário – Cheque
    6 Transferência Bancária
    7 Sedex a Cobrar
    8 Cheque
    9 Dinheiro
    10 Financiamento
    11 Fatura
    12 Cupom
    13 Multicheque
    14 Outros


    Bandeira Cartão

    Código Descrição
    1 Diners
    2 MasterCard
    3 Visa
    4 Outros
    5 American Express
    6 HiperCard
    7 Aura

 *
 */
class ClearsaleComponent extends Object {
    private $url = "https://www.clearsale.com.br/integracaov2/freeclearsale/frame.aspx";
    private $campos = array(
        "pedido" => array(
            "CodigoIntegracao"      => "431e5d11-decc-4059-8eef-fca823bcb8e2",
            "PedidoID"      => "",
            "Data"          => "", //(dd-mm-yyyy hh:mm:ss)
            //"IP"            => "", //obrigatorio = false
            "Total"         => "", //obrigatorio = true
            "TipoPagamento" => "1", //obrigatorio = true
            //"TipoCartao"    => "", //obrigatorio = false
            "Parcelas"      => "", //obrigatorio = true
        ),
        "endereco_cobranca" => array(
            "Cobranca_Nome"                     => "", //nome
            "Cobranca_Email"                    => "", //email
            "Cobranca_Documento"                => "", //CPF ou CNPJ
            "Cobranca_Logradouro"               => "", //endereco
            "Cobranca_Logradouro_Numero"        => "", //endereco numero
            "Cobranca_Logradouro_Complemento"   => "", //endereco complemento
            "Cobranca_Bairro"                   => "", //endereco complemento
            "Cobranca_Cidade"                   => "", //endereco complemento
            "Cobranca_Estado"                   => "", //endereco complemento
            "Cobranca_CEP"                      => "", //endereco complemento
            "Cobranca_Pais"                     => "", //endereco complemento
            "Cobranca_DDD_Telefone"             => "", //endereco complemento
            "Cobranca_Telefone"                 => "", //endereco complemento
            //"Cobranca_DDD_Celular"              => "", //endereco complemento
            //"Cobranca_Celular"                  => "", //endereco complemento
        ),
        "endereco_entrega" => array(
            "Entrega_Nome"                      => "", //nome
            "Entrega_Email"                     => "", //email
            "Entrega_Documento"                 => "", //CPF ou CNPJ
            "Entrega_Logradouro"                => "", //endereco
            "Entrega_Logradouro_Numero"         => "", //endereco numero
            "Entrega_Logradouro_Complemento"    => "", //endereco complemento
            "Entrega_Bairro"                    => "", //endereco complemento
            "Entrega_Cidade"                    => "", //endereco complemento
            "Entrega_Estado"                    => "", //endereco complemento
            "Entrega_CEP"                       => "", //endereco complemento
            "Entrega_Pais"                      => "", //endereco complemento
            "Entrega_DDD_Telefone"              => "", //endereco complemento
            "Entrega_Telefone"                  => "", //endereco complemento
           // "Entrega_DDD_Celular"               => "", //endereco complemento
           // "Entrega_Celular"                   => "", //endereco complemento
        ),
        "pedido_itens"=>array()
        
    );

    public function run($pedido) {

        App::import("helper", "Calendario");
        $this->Calendario = new CalendarioHelper();

        App::import("helper", "String");
        $this->String = new StringHelper();

        $this->campos['pedido']['PedidoID']         = $pedido['Pedido']['id'];
        $this->campos['pedido']['Data']             = $this->Calendario->DataFormatada('d/m/Y H:i:s',$pedido['Pedido']['created']);
        $this->campos['pedido']['Total']            = preg_replace("/[^0-9]/", "", $this->String->bcoToMoeda($this->String->moedaToBco($pedido['Pedido']['valor_pedido']) + $this->String->moedaToBco($pedido['Pedido']['valor_frete'])));
        $this->campos['pedido']['TipoPagamento']    = 1;
        $this->campos['pedido']['Parcelas']         = $pedido['Pedido']['parcelas'];

        $this->campos['endereco_cobranca']['Cobranca_Nome']                     = $pedido['Usuario']['nome'];
        $this->campos['endereco_cobranca']['Cobranca_Email']                    = $pedido['Usuario']['email'];
        $this->campos['endereco_cobranca']['Cobranca_Documento']                = $pedido['Usuario']['tipo_pessoa']=="F"?$pedido['Usuario']['cpf']:$pedido['Usuario']['cnpj'];
        $this->campos['endereco_cobranca']['Cobranca_Logradouro']               = $pedido['Pedido']['endereco_rua'];
        $this->campos['endereco_cobranca']['Cobranca_Logradouro_Numero']        = $pedido['Pedido']['endereco_numero'];
        $this->campos['endereco_cobranca']['Cobranca_Logradouro_Complemento']   = $pedido['Pedido']['endereco_complemento'];
        $this->campos['endereco_cobranca']['Cobranca_Bairro']                   = $pedido['Pedido']['endereco_bairro'];
        $this->campos['endereco_cobranca']['Cobranca_Cidade']                   = $pedido['Pedido']['endereco_cidade'];
        $this->campos['endereco_cobranca']['Cobranca_Estado']                   = $pedido['Pedido']['endereco_estado'];
        $this->campos['endereco_cobranca']['Cobranca_CEP']                      = $pedido['Pedido']['endereco_cep'];
        $this->campos['endereco_cobranca']['Cobranca_Pais']                     = 'Bra';
        $this->campos['endereco_cobranca']['Cobranca_DDD_Telefone']             = substr($pedido['Usuario']['telefone'],0,2);
        $this->campos['endereco_cobranca']['Cobranca_Telefone']                 = substr($pedido['Usuario']['telefone'],2);

        $this->campos['endereco_entrega']['Entrega_Nome']                       = $pedido['Usuario']['nome'];
        $this->campos['endereco_entrega']['Entrega_Email']                      = $pedido['Usuario']['email'];
        $this->campos['endereco_entrega']['Entrega_Documento']                  = $pedido['Usuario']['tipo_pessoa']=="F"?$pedido['Usuario']['cpf']:$pedido['Usuario']['cnpj'];
        $this->campos['endereco_entrega']['Entrega_Logradouro']                 = $pedido['Pedido']['endereco_rua'];
        $this->campos['endereco_entrega']['Entrega_Logradouro_Numero']          = $pedido['Pedido']['endereco_numero'];
        $this->campos['endereco_entrega']['Entrega_Logradouro_Complemento']     = $pedido['Pedido']['endereco_complemento'];
        $this->campos['endereco_entrega']['Entrega_Bairro']                     = $pedido['Pedido']['endereco_bairro'];
        $this->campos['endereco_entrega']['Entrega_Cidade']                     = $pedido['Pedido']['endereco_cidade'];
        $this->campos['endereco_entrega']['Entrega_Estado']                     = $pedido['Pedido']['endereco_estado'];
        $this->campos['endereco_entrega']['Entrega_CEP']                        = $pedido['Pedido']['endereco_cep'];
        $this->campos['endereco_entrega']['Entrega_Pais']                       = 'Bra';
        $this->campos['endereco_entrega']['Entrega_DDD_Telefone']               = substr($pedido['Usuario']['telefone'],0,2);
        $this->campos['endereco_entrega']['Entrega_Telefone']                   = substr($pedido['Usuario']['telefone'],2);


         foreach($pedido['PedidoItem'] as $item){
            $this->campos['pedido_itens']["Item_ID_{$item['id']}"]          = $item['id'];
            $this->campos['pedido_itens']["Item_Nome_{$item['id']}"]        = $item['nome'];
            $this->campos['pedido_itens']["Item_Qtd_{$item['id']}"]         = $item['quantidade'];
            $this->campos['pedido_itens']["Item_Valor_{$item['id']}"]       = $item['preco_promocao']>0?$item['preco_promocao']:$item['preco'];
         }

         $args[] = $this->campos['pedido'];
         $args[] = $this->campos['endereco_cobranca'];
         $args[] = $this->campos['endereco_entrega'];
         $args[] = $this->campos['pedido_itens'];
         $retorno = $this->enviarFormulario($args);
        return $retorno;
    }
     /**
     * Metodo responsavel somente por submeter os dados
     */
    private function enviarFormulario($args) {
        //Form
        $form = '
            <iframe src="" border="0" frameborder="0" scrolling="0" noresize="noresize" name="Clearsale" width="320" height="80" style="overflow:hidden"></iframe>
            <form action="' . $this->url . '" target="Clearsale" name="formPagamento" method="post" id="formPagamento">';
                        foreach ($args as $arg) {
                            foreach ($arg as $c => $v) {
                                $form .= '<input type="hidden" name="' . $c . '" value="' . $v . '" />';
                            }
                        }
                        $form .= '
            </form>
            <script language="JavaScript">
                document.getElementById(\'formPagamento\').submit();
            </script>
            ';
        return $form;
    }
}