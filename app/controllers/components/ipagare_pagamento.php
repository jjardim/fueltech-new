<?php

class IpagarePagamentoComponent extends Object {
	
	/*
	 * Dados de teste do IPAGARE para Reweb
	 */
	private $codigoEstabelecimento = 101635;
	private $codigoSeguranca = 'h5p7q4u6';
	private $url = 'https://ww2.ipagare.com.br/service/process.do';
	public $params = array();
	
	public function pagar($pedido) {
		switch ($pedido['PagamentoCondicao']['sigla']) {
			case 'VISA':
				$pedido['Pedido']['codigo_pagamento'] = 34;
				return $this->pagamentoNaoFinalizado($pedido);                
				break;
			case 'MASTERCARD':
				$pedido['Pedido']['codigo_pagamento'] = 2;
				return $this->pagamentoNaoFinalizado($pedido);
				break;
			case 'DINERS':
				$pedido['Pedido']['codigo_pagamento'] = 1;
				return $this->pagamentoNaoFinalizado($pedido);
				break;
			case 'BOLETO':
				$pedido['Pedido']['codigo_pagamento'] = 6;
				return $this->pagamentoNaoFinalizado($pedido);
				break;
			case 'TRANSFERENCIA':
				$pedido['Pedido']['codigo_pagamento'] = 4;
				return $this->pagamentoNaoFinalizado($pedido);
				break;
			
			case 'BANRISUL_PGBC':
				$pedido['Pedido']['codigo_pagamento'] = 20;
				return $this->pagamentoNaoFinalizado($pedido);
				break;
			case 'BANRISUL_PGTA':
				$pedido['Pedido']['codigo_pagamento'] = 21;
				return $this->pagamentoNaoFinalizado($pedido);
				break;
			case 'BANRISUL_PGTX':
				$pedido['Pedido']['codigo_pagamento'] = 23;
				return $this->pagamentoNaoFinalizado($pedido);
				break;
			case 'BANRISUL_PGTP':
				$pedido['Pedido']['codigo_pagamento'] = 22;
				return $this->pagamentoNaoFinalizado($pedido);
				break;
		}
	}

	public function pagamentoNaoFinalizado($pedido) {

		App::import("helper", "String");
		$this->String = new StringHelper();

		$total = $this->String->moedaToBco($this->String->bcoToMoeda($this->String->moedaToBco($pedido['Pedido']['valor_pedido']) + $this->String->moedaToBco($pedido['Pedido']['valor_frete'])));

		$codigo_pagamento = $pedido['Pedido']['codigo_pagamento'];
		$parcelas = $pedido['Pedido']['parcelas'] < 10 ? "A0" . $pedido['Pedido']['parcelas'] : "A" . $pedido['Pedido']['parcelas'];
		$acao = '1';
		$valorTotal = $this->formataParaIpagare($total);
        $codigoEstabelecimento = $this->codigoEstabelecimento;
        $codigoSeguranca = $this->codigoSeguranca;
		$codigoSeguranca = md5($codigoSeguranca);
		$chave = md5($codigoEstabelecimento . $codigoSeguranca . $acao . $valorTotal);
		$this->params = array();
		$this->params['estabelecimento'] = $codigoEstabelecimento;
		$this->params['acao'] = $acao;
		$this->params['valor_total'] = $valorTotal;
		$this->params['chave'] = $chave;
		$this->params['teste'] = '1';
		// Parametros obrigatorios - pagamento do pedido
		$this->params['codigo_pagamento'] = $codigo_pagamento;
		$this->params['forma_pagamento'] = $parcelas;

		//Parametros opcionais
		$this->params['codigo_pedido'] = $pedido['Pedido']['id']; //Codigo ou chave unica do pedido no Site
		$this->params['idioma'] = "pt";

		//Dados cliente
		$this->params['tipo_cliente'] = "1";
		$this->params['codigo_cliente'] = $pedido['Usuario']['id'];
		$this->params['nome_cliente'] = $pedido['Usuario']['nome'];
		$this->params['cpf_cnpj_cliente'] = $pedido['Usuario']['tipo_pessoa'] == 'F' ? $pedido['Usuario']['cpf'] : $pedido['Usuario']['cnpj'];
		$this->params['email_cliente'] = $pedido['Usuario']['email'];
		//Dados do endereco de cobrança.
		$this->params['logradouro_cobranca'] = $pedido['Pedido']['endereco_rua'];
		$this->params['numero_cobranca'] = $pedido['Pedido']['endereco_numero'];
		$this->params['complemento_cobranca'] = $pedido['Pedido']['endereco_complemento'];
		$this->params['bairro_cobranca'] = $pedido['Pedido']['endereco_bairro'];
		$this->params['cep_cobranca'] = $pedido['Pedido']['endereco_cep'];
		$this->params['cidade_cobranca'] = $pedido['Pedido']['endereco_cidade'];
		$this->params['uf_cobranca'] = $pedido['Pedido']['endereco_estado'];
		$this->params['pais_cobranca'] = "Brasil";

		//Dados do endereco de entrega.
		$this->params['logradouro_entrega'] = $pedido['Pedido']['endereco_rua'];
		$this->params['numero_entrega'] = $pedido['Pedido']['endereco_numero'];
		$this->params['complemento_entrega'] = $pedido['Pedido']['endereco_complemento'];
		$this->params['bairro_entrega'] = $pedido['Pedido']['endereco_bairro'];
		$this->params['cep_entrega'] = $pedido['Pedido']['endereco_cep'];
		$this->params['cidade_entrega'] = $pedido['Pedido']['endereco_cidade'];
		$this->params['uf_entrega'] = $pedido['Pedido']['endereco_estado'];
		$this->params['pais_entrega'] = "Brasil";

		//Dados de telefone
		if ($pedido['Usuario']['telefone']) {
			$this->params['ddd_telefone_1'] = substr($pedido['Usuario']['telefone'], -2);
			$this->params['numero_telefone_1'] = substr($pedido['Usuario']['telefone'], 2);
		}
		//Dados de celular
		if ($pedido['Usuario']['celular']) {
			$this->params['ddd_telefone_2'] = substr($pedido['Usuario']['celular'], -2);
			$this->params['numero_telefone_2'] = substr($pedido['Usuario']['celular'], 2);
		}
		$this->params['numero_itens'] = count($pedido['PedidoItem']);
		$i = 1;

		foreach ($pedido['PedidoItem'] as $item) {
			$this->params['codigo_item_' . $i] = $item['id'];
			$this->params['descricao_item_' . $i] = $item['nome'];
			$this->params['quantidade_item_' . $i] = $item['quantidade'] . "00";
			$preco = $item['preco_promocao'] > 0 ? $item['preco_promocao'] : $item['preco'];
			$this->params['valor_item_' . $i] = $this->formataParaIpagare($preco);
			$i++;
		}
		//print_r($this->enviarFormulario($this->url, $this->params, $this->params['codigo_pedido']));
        print_r($this->enviarFormulario('https://ww2.ipagare.com.br/service/process.do', $this->params, $this->params['codigo_pedido']));
	}

	public function pagamentoCartao($pedido) {
		App::import("helper", "String");
		$this->String = new StringHelper();

		$total = $this->String->moedaToBco($this->String->bcoToMoeda($this->String->moedaToBco($pedido['Pedido']['valor_pedido']) + $this->String->moedaToBco($pedido['Pedido']['valor_frete'])));

		$codigo_pagamento = $pedido['Pedido']['codigo_pagamento'];
		$parcelas = $pedido['Pedido']['parcelas'] < 10 ? "A0" . $pedido['Pedido']['parcelas'] : "A" . $pedido['Pedido']['parcelas'];
		$acao = '2';
		$valorTotal = $this->formataParaIpagare($total);
		$codigoSeguranca = md5($this->codigoSeguranca);
		$chave = md5($this->codigoEstabelecimento . $codigoSeguranca . $acao . $valorTotal);
		$this->params = array();
		$this->params['estabelecimento'] = $this->codigoEstabelecimento;
		$this->params['acao'] = $acao;
		$this->params['valor_total'] = $valorTotal;
		$this->params['chave'] = $chave;
		$this->params['teste'] = '1';
		// Parametros obrigatorios - pagamento do pedido
		$this->params['codigo_pagamento'] = $codigo_pagamento;
		$this->params['forma_pagamento'] = $parcelas;
		/* Dados cartão */
		$this->params['numero_cartao'] = $pedido['Pedido']['numero_cartao'];
		list($mes, $ano) = explode('/', $pedido['Pedido']['validade_cartao']);
		$this->params['mes_validade_cartao'] = $mes;
		$this->params['ano_validade_cartao'] = $ano;
		$this->params['codigo_seguranca_cartao'] = $pedido['Pedido']['codigo_seguranca_cartao'];

		//Parametros opcionais
		$this->params['codigo_pedido'] = $pedido['Pedido']['id']; //Codigo ou chave unica do pedido no Site
		$this->params['idioma'] = "pt";

		//Dados cliente
		$this->params['tipo_cliente'] = "1";
		$this->params['codigo_cliente'] = $pedido['Usuario']['id'];
		$this->params['nome_cliente'] = $pedido['Usuario']['nome'];
		$this->params['cpf_cnpj_cliente'] = $pedido['Usuario']['tipo_pessoa'] == 'F' ? $pedido['Usuario']['cpf'] : $pedido['Usuario']['cnpj'];
		$this->params['email_cliente'] = $pedido['Usuario']['email'];
		//Dados do endereco de cobrança.
		$this->params['logradouro_cobranca'] = $pedido['Pedido']['endereco_rua'];
		$this->params['numero_cobranca'] = $pedido['Pedido']['endereco_numero'];
		$this->params['complemento_cobranca'] = $pedido['Pedido']['endereco_complemento'];
		$this->params['bairro_cobranca'] = $pedido['Pedido']['endereco_bairro'];
		$this->params['cep_cobranca'] = $pedido['Pedido']['endereco_cep'];
		$this->params['cidade_cobranca'] = $pedido['Pedido']['endereco_cidade'];
		$this->params['uf_cobranca'] = $pedido['Pedido']['endereco_estado'];
		$this->params['pais_cobranca'] = "Brasil";

		//Dados do endereco de entrega.
		$this->params['logradouro_entrega'] = $pedido['Pedido']['endereco_rua'];
		$this->params['numero_entrega'] = $pedido['Pedido']['endereco_numero'];
		$this->params['complemento_entrega'] = $pedido['Pedido']['endereco_complemento'];
		$this->params['bairro_entrega'] = $pedido['Pedido']['endereco_bairro'];
		$this->params['cep_entrega'] = $pedido['Pedido']['endereco_cep'];
		$this->params['cidade_entrega'] = $pedido['Pedido']['endereco_cidade'];
		$this->params['uf_entrega'] = $pedido['Pedido']['endereco_estado'];
		$this->params['pais_entrega'] = "Brasil";

		//Dados de telefone
		if ($pedido['Usuario']['telefone']) {
			$this->params['ddd_telefone_1'] = substr($pedido['Usuario']['telefone'], -2);
			$this->params['numero_telefone_1'] = substr($pedido['Usuario']['telefone'], 2);
		}
		//Dados de celular
		if ($pedido['Usuario']['celular']) {
			$this->params['ddd_telefone_2'] = substr($pedido['Usuario']['celular'], -2);
			$this->params['numero_telefone_2'] = substr($pedido['Usuario']['celular'], 2);
		}
		$this->params['numero_itens'] = count($pedido['PedidoItem']);
		$i = 1;

		foreach ($pedido['PedidoItem'] as $item) {
			$this->params['codigo_item_' . $i] = $item['id'];
			$this->params['descricao_item_' . $i] = $item['nome'];
			$this->params['quantidade_item_' . $i] = $item['quantidade'] . "00";
			$preco = $item['preco_promocao'] > 0 ? $item['preco_promocao'] : $item['preco'];
			$this->params['valor_item_' . $i] = $this->formataParaIpagare($preco);
			$i++;
		}

		App::import("Xml");
		return Set::reverse(new Xml($this->postHttp($this->url, $this->params)));
	}

	/**
	 * Formata um valor para ter no maximo dois digitos apos a virgula.
	 */
	public function formatAmount($amount) {
		return round($amount, 2);
	}

	/*
	 * Formata o valor do pedido no formato exigido pelo I-PAGARE.
	 * Por exemplo, um pedido de
	 * - valor total R$ 499,99 deve ser enviado como 49999
	 * - valor total R$ 499,00 deve ser enviado como 49900
	 * - quantidade igual a 1 deve ser enviado como 100
	 * - quantidade igual a 1,5 deve ser enviado como 150
	 */

	public function formataParaIpagare($amount) {
		$amountInt = (int) $amount;
		if ($amountInt == $amount) {
			//se o numero for inteiro, entao tira os zeros do final.
			$amount = (int) $amount;
			//transforma em string
			$amountStr = $amount;
			$amountStr = str_ireplace(",", "", $amountStr);
			$amountStr = str_ireplace(".", "", $amountStr);
			//adiciona os zeros dos centavos.
			$amountStr = $amountStr . '00';
		} else {
			$amount = self::formatAmount($amount);
			$amountStr = $amount;

			$pos = strpos($amountStr, '.');
			$decimais = substr($amountStr, $pos + 1);
			$tamDecimais = strlen($decimais);

			while ($tamDecimais < 2) {
				$amountStr = $amountStr . '0';

				$pos = strpos($amountStr, '.');
				$decimais = substr($amountStr, $pos + 1);
				$tamDecimais = strlen($decimais);
			}

			$amountStr = str_ireplace(",", "", $amountStr);
			$amountStr = str_ireplace(".", "", $amountStr);
		}

		return $amountStr;
	}

	public function isEmpty($str) {
		if ($str == null) {
			return true;
		}
		if (trim($str) == '') {
			return true;
		}
		return false;
	}

	public function convertStringToDate($dataStatus, $horaStatus) {
		$hora = "00";
		$minuto = "00";
		$segundo = "00";

		if ($horaStatus != null && $horaStatus) {
			$hora = substr($horaStatus, 0, 2);
			$minuto = substr($horaStatus, 2, 2);
			$segundo = substr($horaStatus, 4, 2);
		}

		$dia = substr($dataStatus, 0, 2);
		$mes = substr($dataStatus, 2, 2);
		$ano = substr($dataStatus, 4);

		$data = strftime("%d/%m/%Y %X", mktime($hora, $minuto, $segundo, $mes, $dia, $ano));

		return $data;
	}

	public function convertStringToDouble($valorStr) {
		$valorStr = str_ireplace(".", "", $valorStr);
		$valor = $valorStr / 100;
		return $valor;
	}

	public function postHttp($url, $data) {
		$params = '';
		foreach ($data as $name => $value) {
			$params = $params . $name . '=' . $value . '&';
		}

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_POST, count($data));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($ch);

		return $response;
	}
	public function enviarFormulario($url, $args, $id_form) {

		$form = '   <iframe src="" border="0" frameborder="0" scrolling="0" noresize="noresize" name="ipagare' . $id_form . '" height=475px" width="100%" style="overflow:hidden"></iframe>
					<form action="' . $url . '" target="ipagare' . $id_form . '" name="formPagamento" method="post" id="formPagamento' . $id_form . '">';
					foreach ($args as $c => $v) {
						$form .= '<input type="hidden" name="' . $c . '" value="' . $v . '" />';
					}
					$form .= '</form>
					<script type="text/javascript">
						document.getElementById(\'formPagamento' . $id_form . '\').submit();
					</script>';
		return $form;
	}
}