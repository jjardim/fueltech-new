<?php

/**
 * Classe que gerência os produtos visualizados
 * @Author Luan Garcia <luan.garcia@gmail.com>
 * */
Class ProdutosAcessadosComponent extends Object {
    public $Cookie;
    function __construct() {
        App::import("component", "Cookie");
        $this->Cookie = new CookieComponent();
        $this->Cookie->name = 'visualizados';
        $this->Cookie->time = '365 Days';
        $this->Cookie->key = 'qSI232qs*&sXOw!';
    }
    function add($id) {
        $visualizados_tmp = $this->Cookie->read('visualizados');
        $visualizados_tmp[] = $id;
        $visualizados = array_unique($visualizados_tmp);
		if(count($visualizados)>10)array_shift($visualizados);
        $this->Cookie->write('visualizados',$visualizados);
    }
    function getAll(){
        return $this->Cookie->read('visualizados');
    }
}

