<?php

/**
 * @Author Luan Garcia <luan.garcia@gmail.com>
 * 
 */
Class ImportacaoComponent extends Object {
    /* TODO DIFF dos atributos */

    function __construct() {
	error_reporting(E_ALL);

	date_default_timezone_set('America/Sao_Paulo');

	ini_set('max_execution_time', 0);
	ini_set('default_socket_timeout', 3600);
	ini_set('memory_limit', '3096M');
	set_time_limit(0);
	App::import("model", "ImportacaoLog");
	$this->ImportacaoLog = new ImportacaoLog();

	$this->importacao_dir = WWW_ROOT . 'integracao' . DS;
    }

    public function start($method) {
	$sql = "SELECT * FROM importacoes_log WHERE method = '{}' AND status = '1' ";
	if ($this->ImportacaoLog->find('first', array('conditions' => array('method' => $method, 'status' => true))))
			die('processo em execuçao');
	$this->ImportacaoLog->save(array('method' => $method, 'status' => true));
	return $this->ImportacaoLog->id;
    }

    public function end($id, $log) {
	return $this->ImportacaoLog->save(array('id' => $id, 'log' => $log, 'status' => false));
    }

    function xml_produto_preco() {

	$this->importacao_produtos_preco_dir = WWW_ROOT . 'integracao' . DS . 'AtuPreco' . DS;

	App::import("helper", "String");
	$this->String = new StringHelper();

	$import_id = $this->start('produto_preco');
	echo "Comecei a rodar ás " . date('d/m/Y H:i:s');

	App::import('Model', 'Produto');
	$this->Produto = new Produto();
	App::import('Model', 'AtributoTipo');
	$this->AtributoTipo = new AtributoTipo();

	foreach (glob($this->importacao_produtos_preco_dir . "produto_atualiza*") as $file):
	    $xml_file = $file;
	    $execute = TRUE;
	    break;
	endforeach;
	$ff = str_replace('&', '&amp;', file_get_contents($xml_file));
	$ff = str_replace(' > ', ' &gt; ', $ff);
	$ff = str_replace(' < ', ' &lt; ', $ff);

	$xml = simplexml_load_string($ff);


	foreach ($xml as $produto) {

	    if (isset($produto->preco) && !empty($produto->preco)) {
		$xmlCollection[(string) $produto->sku] = array(
		    'sku' => (string) $produto->sku,
		    'data' => array(
			'price' => (string) $produto->preco,
			'special_price' => (string) $produto->preco_promocional,
			'special_from_date' => (string) $produto->data_entrada_promocao,
			'special_to_date' => (string) $produto->data_saida_promocao
		    )
		);
	    } else {
		$xmlCollection[(string) $produto->sku] = array(
		    'sku' => (string) $produto->sku,
		    'data' => array(
			'special_price' => (string) $produto->preco_promocional,
			'special_from_date' => (string) $produto->data_entrada_promocao,
			'special_to_date' => (string) $produto->data_saida_promocao
		    )
		);
	    }
	}

	$log = 'sku não encontrado ';
	foreach ($xmlCollection as $produto):
	    $db = $this->Produto->find('first', array('fields' => array('Produto.id'), 'recursive' => -1, 'limit' => -1, 'conditions' => array('Produto.sku' => $produto['sku'])));
	    if ($db) {
		$preco_promocao_novo = $this->String->bcoToMoeda((double) str_replace(',', '', $produto['data']['special_price']));
		$preco_promocao_velho = $this->String->bcoToMoeda((double) str_replace(',', '', $produto['data']['price']));

		if ($preco_promocao_velho == $preco_promocao_novo) {
		    $preco_promocao_novo = '0,00';
		}

		if ($produto['data']['special_from_date'] == '' && $produto['data']['special_to_date'] == '') {
		    $this->Produto->save(array(
			'id' => $db['Produto']['id'],
			'preco' => $preco_promocao_velho,
			'preco_promocao' => $preco_promocao_novo,
			'preco_promocao_novo' => '',
			'preco_promocao_velho' => '',
			'preco_promocao_fim' => '',
			'preco_promocao_inicio' => ''
			    ), false);
		} else {
		    $this->Produto->save(array(
			'id' => $db['Produto']['id'],
			'preco_promocao_inicio' => $produto['data']['special_from_date'],
			'preco_promocao_fim' => $produto['data']['special_to_date'],
			'preco_promocao_novo' => $preco_promocao_novo,
			'preco_promocao_velho' => $preco_promocao_velho), false);
		}
	    } else {
		$log .= $produto['sku'] . '#';
	    }

	endforeach;

	copy($xml_file, str_replace('AtuPreco/', 'AtuPreco/_bkp/', $xml_file));
	unlink($xml_file);
	echo "<br>Terminei de rodar as " . date('d/m/Y H:i:s');
	$this->end($import_id, $log);
    }

    function xml_produto_estoque() {
        
        //die('estoque');
        
    	$this->importacao_produtos_estoque_dir = WWW_ROOT . 'integracao' . DS . 'AtuEstoque' . DS;
		App::import("helper", "String");
		$this->String = new StringHelper();
	
		$import_id = $this->start('produto_estoque');
		echo "Comecei a rodar ás " . date('d/m/Y H:i:s');
	
		App::import('Model', 'Produto');
		$this->Produto = new Produto();
		App::import('Model', 'AtributoTipo');
		$this->AtributoTipo = new AtributoTipo();
	
		foreach (glob($this->importacao_produtos_estoque_dir . "produto_atualiza*") as $file):
		    $xml_file = $file;
		    $execute = TRUE;
		    break;
		endforeach;
	
		$ff = str_replace('&', '&amp;', file_get_contents($xml_file));
		$ff = str_replace(' > ', ' &gt; ', $ff);
		$ff = str_replace(' < ', ' &lt; ', $ff);
	
		$xml = simplexml_load_string($ff);
	
		foreach ($xml as $produto) {
		    if ($produto->ativo == 'Ativo') {
				$isInStock = 1;
		    } else {
				$isInStock = 0;
		    }
		    $xmlCollection[(string) $produto->sku] = array(
			'sku' => (string) $produto->sku,
			'data' => array(
			    'stock_data' => array(
				'qty' => (string) $produto->estoque,
				'is_in_stock' => $isInStock
			    )
			)
		    );
		}
		foreach ($xmlCollection as $produto):
		    $db = $this->Produto->find('first', array('fields' => array('Produto.id', 'Produto.quantidade_alocada'), 'recursive' => -1, 'conditions' => array('Produto.sku' => $produto['sku'])));
		    if ($db) {
				$save = array(
				    'id' => $db['Produto']['id'],
				    'status' => $produto['data']['stock_data']['is_in_stock'],
				    'quantidade' => $produto['data']['stock_data']['qty'],
				    'quantidade_disponivel' => ($produto['data']['stock_data']['qty'] - $db['Produto']['quantidade_alocada']),
				);
				$this->Produto->save($save, false);
		    }
		endforeach;

		copy($xml_file, str_replace('AtuEstoque/', 'AtuEstoque/_bkp/', $xml_file));
		unlink($xml_file);
		echo "<br>Terminei de rodar as " . date('d/m/Y H:i:s');
		$this->end($import_id);
	}
	
	function xml_atributo_tipo() {
		$import_id = $this->start('atributo_tipo');
		echo "Comecei a rodar ás " . date('d/m/Y H:i:s');
		App::import('Model', 'AtributoTipo');
		$this->AtributoTipo = new AtributoTipo();
	
		App::import("Xml");
		$xml = Set::reverse(new Xml($this->importacao_dir . 'atributo.xml'));
		$log = 'SUCESSO';
		foreach ($xml['Root']['Atributo'] as $atributo):
		    $data['id'] = '';
		    $data['nome'] = $atributo;
		    $this->AtributoTipo->set($data);
		    if ($this->AtributoTipo->validates($data)) {
			if ($this->AtributoTipo->save($data)) {
			    //$log['SUCESSO'][] = $log;
			} else {
			    //$log['ERRO'][] = $data;				
			}
		    }
		endforeach;
		echo "<br>Terminei de rodar as " . date('d/m/Y H:i:s');
		$this->end($import_id, $log);
	    }
	
	    function getImagensNome($xml) {
		foreach ($xml->children() as $imagem) {
		    $subCollection[(string) $imagem->ordem]['nome'] = (string) $imagem->nome;
		    $subCollection[(string) $imagem->ordem]['ordem'] = (string) $imagem->ordem;
		}
		return $subCollection;
	    }
	
	    function getImagensDiff($xml) {
		//IMPORTANTE AQUI ADICIONAR O CAMINHO DAS IMAGENS DA FG
		//http://www.fg.com.br/imagens_produtos/Fotos/
	
		$dir = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'integracao' . DS . 'imagens' . DS;
	
		foreach ($xml->children() as $imagem) {
		    $str = $dir . $imagem->nome;
		    if (file_exists($str)) {
			$imgbinary = fread(fopen($str, "r"), filesize($str));
			$subCollection[(string) $imagem->ordem] = base64_encode($imgbinary);
		    } else {
			$subCollection[(string) $imagem->ordem] = 'Arquivo não encontrado';
		    }
		}
		return $subCollection;
    }

    /*
      TODO DIFF dos produtos imagens
      n√£o deixar inserir se existir o filename para o produto_id
     */

    function xml_produto_imagem() {
	// $import_id = $this->start('produto_imagem');
	echo "Comecei a rodar ás " . date('d/m/Y H:i:s');
	$log = '';
	App::import('Model', 'ProdutoImagem');
	App::import('Model', 'Produto');

	$this->ProdutoImagem = new ProdutoImagem();
	$this->Produto = new Produto();
	//desligo o behavior de upload
	$this->ProdutoImagem->Behaviors->detach('MeioUpload');
	$dir = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'uploads' . DS . 'produto_imagem' . DS . 'filename' . DS;

	$ff = str_replace('&', '&amp;', file_get_contents($this->importacao_dir . 'produto_imagens.xml'));
	$ff = str_replace(' > ', ' &gt; ', $ff);
	$ff = str_replace(' < ', ' &lt; ', $ff);

	$xml = simplexml_load_string($ff);
	try {
	    foreach ($xml as $produto) {
		$xmlCollection[(string) $produto->sku] = array(
		    'sku' => (string) $produto->sku,
		);
		if (isset($produto->imagens)) {
		    $xmlCollection[(string) $produto->sku]['imagens'] = $this->getImagensNome($produto->imagens);
		    $xmlCollection[(string) $produto->sku]['imagens_diff'] = $this->getImagensDiff($produto->imagens);
		}
	    }

	    //http://www.fg.com.br/imagens_produtos/Fotos/1506.jpg
	    foreach ($xmlCollection as $produtos):
		$produto = $this->Produto->find('first', array('contain' => array('ProdutoImagem'), 'fields' => array('id', 'sku'), 'limit' => 1, 'conditions' => array('Produto.sku' => $produtos['sku'])));
		if (!$produto) {
		    echo 'produto não encontrado ' . $produtos['sku'];
		    continue;
		}
		$dbImages = array();
		$dbImagesName = array();
		$xmlImages = array();
		$xmlImagesName = array();
		//se tiver a imagem na pasta n√£o faz o find de produto
		foreach ($produtos['imagens'] as $imagem):
		    foreach ($produto['ProdutoImagem'] as $image) {
			$dir = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'uploads' . DS . 'produto_imagem' . DS . 'filename' . DS;
			$str = $dir . $image['filename'];
			$dbImages[$image['ordem']] = base64_encode(fread(fopen($str, "r"), filesize($str)));
			$dbImagesName[$image['ordem']] = $image['filename'];
		    }

		    $xmlImages = $produtos['imagens_diff'];
		    $xmlImages_name = $produtos['imagens'];

		    $remover = array_diff_assoc($dbImages, $xmlImages);

		    foreach ($remover as $position => $imagem) {
			try {
			    if (!$this->ProdutoImagem->deleteAll(array('ProdutoImagem.produto_id' => $produto['Produto']['id'], 'ProdutoImagem.filename LIKE' => $dbImagesName[$position]), false)) {
				$log .= "Falha ao remover a imagem " . $xmlImagesName[$position] . "\n";
			    }
			} catch (Exception $e) {
			    $log .= $e->getMessage() . ': ' . $xmlImagesName[$position] . "\n";
			}
		    }
		    $adicionar = array_diff_assoc($xmlImages, $dbImages);

		    foreach ($adicionar as $position => $imagem) {

			try {
			    $data['id'] = '';
			    $data['filename'] = $xmlImages_name[$position]['nome'];
			    $data['dir'] = 'uploads/produto_imagem/filename';
			    $data['mimetype'] = 'image/jpeg';
			    $data['filesize'] = '13722';
			    $data['ordem'] = $xmlImages_name[$position]['ordem'];
			    $data['produto_id'] = $produto['Produto']['id'];
			    $origem = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'integracao' . DS . 'imagens' . DS . $xmlImages_name[$position]['nome'];
			    $dir = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'uploads' . DS . 'produto_imagem' . DS . 'filename' . DS;
			    $destino = $dir . $xmlImages_name[$position]['nome'];
			    //se não existir imagem na pasta ele copia ela e add no banco
			    if ($this->ProdutoImagem->save($data)) {
				copy($origem, $destino);
			    }
			} catch (Exception $e) {
			    
			}
		    }

		endforeach;

	    endforeach;
	} catch (Exception $e) {
	    $log .= $e->getMessage() . "\n";
	}

	echo "<br>Terminei de rodar as " . date('d/m/Y H:i:s');
	$this->end($import_id, print_r($log, true));
    }

    /* TODO DIFF dos produtos */

    function xml_produtos() {
        
        //die('produtos');
        
		$import_id = $this->start('protudos');
		echo "Comecei a rodar ás " . date('d/m/Y H:i:s');
		App::import('Model', 'Produto');
		App::import("helper", "String");
		$this->String = new StringHelper();
		$this->Produto = new Produto();

		$ff = str_replace('&', '&amp;', file_get_contents($this->importacao_dir . 'produto_cadastra.xml'));
		$ff = str_replace('<descricao>', '<descricao><![CDATA[', $ff);
		$ff = str_replace('</descricao>', ']]></descricao>', $ff);
		$ff = str_replace(' > ', ' &gt; ', $ff);
		$ff = str_replace(' < ', ' &lt; ', $ff);

		$xml = simplexml_load_string($ff);

		foreach ($xml as $pai) {
		    $xmlCollection[(string) $pai->sku] = array(
			'nome' => (string) $pai->nome,
			'tipo' => (string) $pai->tipo,
			'sku' => (string) $pai->sku,
			'referencia' => (string) $pai->referencia,
			'descricao' => (string) $pai->descricao,
			'descricao_resumida' => (string) $pai->descricao_resumida,
			'unidade_venda' => (string) $pai->unidade_venda,
			'preco' => (string) $pai->preco,
			'preco_promocional' => (string) $pai->preco_promocional,
			'custo' => (string) $pai->custo,
			'data_entrada_promocao' => (string) $pai->data_entrada_promocao,
			'data_saida_promocao' => (string) $pai->data_saida_promocao,
			'peso' => (string) $pai->peso,
			'peso_ramos' => (string) $pai->peso_ramos,
			'estoque' => (string) $pai->estoque,
			'ativo' => (string) $pai->ativo,
			'observacao' => (string) $pai->observacao,
			'altura' => (string) $pai->altura,
			'comprimento' => (string) $pai->comprimento,
			'largura' => (string) $pai->largura,
			'atributo' => (string) $pai->atributo,
			'marca' => (string) $pai->marca,
		    );
		}
	
		$log = null;
		//$this->Produto->Behaviors->detach('Tree');
		foreach ($xmlCollection as $produto):
		    if ($p = $this->Produto->find('first', array('conditions' => array('Produto.sku' => $produto['sku'])))) {
			$data['id'] = $p['Produto']['id'];
			$data['quantidade_alocada'] = $p['Produto']['quantidade_alocada'];
		    } else {
			$data['id'] = '';
			$data['quantidade_alocada'] = 0;
		    }
	
		    $data['fabricante_id'] = 1;
		    $data['parent_id'] = 0;
		    $data['nome'] = $produto['nome'];
		    $data['tipo'] = $produto['tipo'];
		    $data['sku'] = $produto['sku'];
		    $data['referencia'] = $produto['referencia'];
		    $data['descricao'] = $produto['descricao'];
		    $data['descricao_resumida'] = $produto['descricao_resumida'];
		    $data['unidade_venda'] = $produto['unidade_venda'];
	
		    $produto['preco'] = $this->String->bcoToMoeda((double) str_replace(',', '', $produto['preco']));
		    $promocional = (double) str_replace(',', '', $produto['preco_promocional']) > 0 && (double) str_replace(',', '', $produto['preco_promocional']) < (double) str_replace(',', '', $produto['preco']) ? (double) str_replace(',', '', $produto['preco_promocional']) : 0;
		    $produto['preco_promocional'] = $this->String->bcoToMoeda($promocional);
		    $produto['custo'] = $this->String->bcoToMoeda((double) str_replace(',', '', $produto['custo']));
	
		    $data['preco'] = $produto['preco'];
		    $data['preco_promocao'] = $produto['preco_promocional'];
		    $data['custo'] = $produto['custo'];
		    $data['peso'] = $produto['peso'] > 0 ? $produto['peso'] : 0;
		    $data['peso_ramos'] = $produto['peso_ramos'];
		    $data['quantidade'] = $produto['estoque'];
		    $data['quantidade_disponivel'] = $produto['estoque'] - $data['quantidade_alocada'];
		    
		    $data['status'] = $produto['ativo'] == 'Ativo' ? true : false;
		    
		    $data['preco_promocao_inicio'] = null;
		    $data['preco_promocao_fim'] = null;
		    $data['preco_promocao_novo'] = null;
		    $data['observacao'] = $produto['observacao'];
		    $data['altura'] = $produto['altura'];
		    $data['largura'] = $produto['largura'];
		    $data['profundidade'] = $produto['comprimento'];
		    $data['atributo'] = $produto['atributo'];
		    $data['marca'] = $produto['marca'];
	
		    if ($this->Produto->save($data, false)) {
				$log['SUCESSO'][] = $data;
		    } else {
				$log['ERRO'][] = $data;
		    }
		endforeach;

		echo "<br>Terminei de rodar as " . date('d/m/Y H:i:s');

		$this->end($import_id, var_export($log, true));
    }

    function getFilhasCP($xml) {

	foreach ($xml->children() as $filha) {
	    $cate = (string) $filha->nome;
	    $cate = $this->strtolower_br($cate);
	    $subCollection[$cate] = array(
		'nome' => $cate,
	    );
	    if (isset($filha->produto)) {
		$subCollection[$cate]['produtos'] = $this->getProdutosCP($filha->produto);
	    }
	    if (isset($filha->filhas)) {
		$subCollection[$cate]['filhas'] = $this->getFilhasCP($filha->filhas);
	    }
	}
	return $subCollection;
    }

    function getProdutosCP($xml) {
	foreach ($xml->children() as $produto) {
	    $subCollection[] = array('sku' => (string) $produto);
	}
	return $subCollection;
    }

    function getImagem($xml) {
	foreach ($xml->imagens() as $imagem) {
	    $subCollection[] = array('nome' => (string) $imagem->nome, 'ordem' => (string) $imagem->ordem,);
	}
	return $subCollection;
    }

    function getFilhasC($xml) {
	foreach ($xml->children() as $filha) {
	    $cate = (string) $filha->nome;
	    $cate = $this->strtolower_br($cate);
	    $subCollection[$cate] = array(
		'nome' => $cate,
		'status' => (string) $filha->status,
		'descricao' => (string) $filha->descricao
	    );
	    if (isset($filha->filhas)) {
		$subCollection[$cate]['filhas'] = $this->getFilhasC($filha->filhas);
	    }
	}
	return $subCollection;
    }

    function xml_categorias_produtos() {
	$import_id = $this->start('categorias_produtos');
	echo "Comecei a rodar ás " . date('d/m/Y H:i:s');
	App::import('Model', 'Produto');
	App::import('Model', 'Categoria');
	App::import('Model', 'ProdutoCategoria');

	$this->Produto = new Produto();
	$this->Categoria = new Categoria();
	$this->ProdutoCategoria = new ProdutoCategoria();
	$this->ProdutoCategoria->primaryKey = '';

	$ff = str_replace('&', '&amp;', file_get_contents($this->importacao_dir . 'produto_categoria.xml'));
	$ff = str_replace('<descricao>', '<descricao><![CDATA[', $ff);
	$ff = str_replace('</descricao>', ']]></descricao>', $ff);
	$ff = str_replace(' > ', ' &gt; ', $ff);
	$ff = str_replace(' < ', ' &lt; ', $ff);

	$xml = simplexml_load_string($ff);

	foreach ($xml as $pai) {
	    $cate = (string) $pai->nome;
	    $cate = $this->strtolower_br($cate);
	    $xmlCollection[$cate] = array('nome' => $cate);
	    if (isset($pai->produto)) {
		$xmlCollection[$cate]['produtos'] = $this->getProdutosCP($pai->produto);
	    }
	    if (isset($pai->filhas)) {
		$xmlCollection[$cate]['filhas'] = $this->getFilhasCP($pai->filhas);
	    }
	}

	$produtos_categorias = array();
	foreach ($xmlCollection as $categoria) {

	    foreach ($categoria['filhas'] as $cate) {
		if (!isset($cate['produtos'])) {
		    foreach ($cate['filhas'] as $t) {
			$produtos_categorias[$categoria['nome'] . '#' . $cate['nome'] . '#' . $t['nome']] = $t['produtos'];
		    }
		} else {
		    $produtos_categorias[$categoria['nome'] . '#' . $cate['nome']] = $cate['produtos'];
		}
	    }
	}

	$erros = '';
	$count = 0;

	foreach ($produtos_categorias as $cx => $produto) {


	    $dbProdutos = array();
	    $xmlProdutos = array();

	    $departamento = $categoria = $subcategoria = '';
	    @list($departamento, $categoria, $subcategoria) = explode('#', $cx);


	    $d = $this->Categoria->find('first', array('recursive' => -1, 'conditions' => array('Categoria.parent_id' => 0, 'Categoria.nome LIKE' => $departamento)));

	    if ($categoria != "") {
		$d = $this->Categoria->find('first', array('recursive' => -1, 'conditions' => array('Categoria.nome LIKE' => $categoria, 'Categoria.parent_id' => $d['Categoria']['id'])));
	    }

	    if ($subcategoria != "") {
		$d = $this->Categoria->find('first', array('recursive' => -1, 'conditions' => array('Categoria.nome LIKE' => $subcategoria, 'Categoria.parent_id' => $d['Categoria']['id'])));
	    }

	    $this->Produto->ProdutoCategoria->unbindModel(array('belongsTo' => array('Produto', 'Categoria')));
	    $p_bco = $this->Produto->ProdutoCategoria->find('all', array('fields' => array('*,(SELECT sku FROM produtos Produto WHERE Produto.id = ProdutoCategoria.produto_id LIMIT 1) AS sku'), 'conditions' => array('ProdutoCategoria.categoria_id' => $d['Categoria']['id'])));

	    foreach ($p_bco as $_product) {
		$dbProdutos[$_product[0]['sku']] = $_product[0]['sku'];
	    }

	    foreach ($produto as $produ) {
		$xmlProdutos[$produ['sku']] = $produ['sku'];
	    }

	    $adicionar = array_diff($xmlProdutos, $dbProdutos);
	    $remover = array_diff($dbProdutos, $xmlProdutos);

	    foreach ($adicionar as $sku) {
		try {
		    $produto = $this->Produto->find('first', array('recursive' => -1, 'conditions' => array('Produto.sku' => $sku)));
		    if ($produto['Produto']['id'] != "" && $d['Categoria']['id'] != "") {
			$ProdutoCategoria['categoria_id'] = $d['Categoria']['id'];
			$ProdutoCategoria['produto_id'] = $produto['Produto']['id'];
			debug($ProdutoCategoria);
			if ($this->ProdutoCategoria->save($ProdutoCategoria, false)) {
			    $erros .= "SUCESSO categoria {$cx} sku {$sku}\n";
			} else {
			    $erros .= "ERRO categoria {$cx} sku {$sku}\n";
			}
		    } else {
			$erros .= "adicionar Produto " . $sku . " não encontrado na base de dados\n";
		    }
		} catch (Exception $e) {
		    $erros .= $e->getMessage() . "\n";
		}
	    }
	    continue;
	    foreach ($remover as $sku) {
		try {
		    $produto = $this->Produto->find('first', array('recursive' => -1, 'conditions' => array('Produto.sku' => $sku)));
		    if ($produto) {
			$sql = array('ProdutoCategoria.produto_id' => $produto['Produto']['id'], 'ProdutoCategoria.categoria_id' => $d['Categoria']['id']);

			$r = $this->ProdutoCategoria->deleteAll($sql, false);
			if (!$r) {
			    $erros .= "Falha ao remover a produto {$produto['Produto']['id']} categoria {$d['Categoria']['id']} \n";
			}
		    } else {
			$erros .= "remover Produto " . $sku . " não encontrado na base de dados\n";
		    }
		} catch (Exception $e) {
		    $erros .= $e->getMessage() . "\n";
		}
	    }
	}

	$log = var_export($erros, true);
	$this->end($import_id, $log);
	echo "<br>Terminei de rodar as " . date('d/m/Y H:i:s');
    }

    function getAtributosFiltro($xml) {

	$xmlFilterReturn = array();
	foreach ($xml->children() as $atributo) {

	    $nome = (string) $atributo->nome;
	    $nome = $this->strtolower_br($nome);

	    $xmlCollectionFilter[$nome] = (string) $atributo->filtro;
	    $xmlFilterReturn[$nome] = (string) $atributo->filtro;
	}
	return $xmlFilterReturn;
    }

    function getAtributos($xml) {
	foreach ($xml->children() as $atributo) {
	    $nome = (string) $atributo->nome;
	    $nome = $this->strtolower_br($nome);
	    $valor = (string) $atributo->valor;
	    $valor = $this->strtolower_br($valor);
	    $subCollection[$nome] = (string) $valor;
	}
	return $subCollection;
    }

    function getAtributos2($xml) {

	foreach ($xml->children() as $filha) {

	    $nome = (string) $filha->nome;
	    $nome = $this->strtolower_br($nome);
	    $valor = (string) $filha->valor;
	    $valor = $this->strtolower_br($valor);
	    $subCollection[$nome] = array(
		'nome' => $nome,
		'valor' => $valor,
		'filtro' => (string) $filha->filtro,
	    );
	    if (isset($filha->produto)) {
		$subCollection[$nome]['produtos'] = $this->getProdutosAtributo2($filha->produto);
	    }
	    if (isset($filha->atributos)) {
		$subCollection['atributos'][] = $this->getAtributos2($filha->atributos);
	    }
	}
	return $subCollection;
    }

    function getProdutosAtributo2($xml) {
	foreach ($xml->children() as $produto) {
	    $subCollection[] = array('sku' => (string) $produto);
	}
	return $subCollection;
    }

    function getVariacoes($xml) {
	foreach ($xml->children() as $variacoes) {
	    $subCollection[] = array('nome' => (string) $variacoes->nome, 'valor' => (string) $variacoes->valor, 'ativo' => (string) $variacoes->ativo, 'ordem' => (string) $variacoes->ordem);
	}
	// var_dump($subCollection);
	return $subCollection;
    }

    public function strtolower_br($texto) {
	$texto = trim($texto);
	$oque = array("/(?i)À/", "/(?i)Ã/", "/(?i)Â/", "/(?i)É/", "/(?i)Ê/", "/(?i)Í/", "/(?i)Î/", "/(?i)Ó/", "/(?i)Õ/", "/(?i)Ô/", "/(?i)Ú/", "/(?i)Û/", "/(?i)Ç/");
	$peloque = array("á", "ã", "â", "é", "ê", "í", "î", "ó", "õ", "ô", "ú", "û", "ç");
	$texto = strtolower($texto);
	return preg_replace($oque, $peloque, $texto);
    }

    function _createTag($string) {
	$tag = trim($string);
	$tag = str_replace(Array(" "), "_", $tag);
	$tag = str_replace(Array("Á", "À", "Â", "Ã", "á", "à", "â", "ã", "ª"), "a", $tag);
	$tag = str_replace(Array("É", "È", "Ê", "é", "è", "ê"), "e", $tag);
	$tag = str_replace(Array("Í", "Ì", "í", "ì"), "i", $tag);
	$tag = str_replace(Array("Ó", "Ò", "Ô", "Õ", "ó", "ò", "ô", "õ", "º"), "o", $tag);
	$tag = str_replace(Array("Ú", "Ù", "Û", "ú", "ù", "û"), "u", $tag);
	$tag = str_replace(Array("Ç", "ç"), "c", $tag);
	//$tag = str_replace(Array("(", ")", "<", ">", "[", "]", "{", "}", "/", "|", "%", "?", "!", "$", "+", "=", "*", "&", "!", "#", "`", "@", ";", ":", "¬¥", "~", "^", ",", "'", '"', ".", "-"), "", $tag);
	$tag = str_replace(Array("____", "___", "__"), "_", $tag);
	$tag = preg_replace("/\s/", ' ', $tag);
	$tag = preg_replace("/\n/", "", $tag);
	$tag = preg_replace("/\r/", "", $tag);
	return strtolower($tag);
    }

    function xml_atributos_produtos() {
	$import_id = $this->start('atributos_produtos');

	echo "Comecei a rodar ás " . date('d/m/Y H:i:s');
	ini_set('display_errors', 'On');


	$xmlCollectionFilter = array();
	$ff = str_replace('&', '&amp;', file_get_contents($this->importacao_dir . 'produto_atributo.xml'));
	$ff = str_replace('<descricao>', '<descricao><![CDATA[', $ff);
	$ff = str_replace('</descricao>', ']]></descricao>', $ff);
	$ff = str_replace(' > ', ' &gt; ', $ff);
	$ff = str_replace(' < ', ' &lt; ', $ff);

	$xml = simplexml_load_string($ff);

	foreach ($xml as $pai) {
	    $xmlCollection[(string) $pai->sku] = array('sku' => (string) $pai->sku,);
	    if (isset($pai->atributos)) {
		$xmlCollection[(string) $pai->sku]['filhas'] = $this->getAtributos2($pai->atributos);
	    }
	}
	App::import('Model', 'Atributo');
	App::import('Model', 'Produto');
	App::import('Model', 'CategoriaAtributo');
	App::import('Model', 'AtributoProduto');
	App::import('Model', 'AtributoTipo');
	$this->Produto = new Produto();
	$this->Atributo = new Atributo();
	$this->CategoriaAtributo = new CategoriaAtributo();
	$this->AtributoProduto = new AtributoProduto();
	$this->AtributoTipo = new AtributoTipo();
	$this->AtributoProduto->primaryKey = '';
	$this->Atributo->AtributoProduto->primaryKey = '';
	$log = '';

	foreach ($xmlCollection as $valor) {
	    $xmlAtributos = array();
	    $dbAtributos = array();
	    $produto = $this->Produto->find('first', array('contain' => array('AtributoProduto' => array('Atributo' => array('AtributoTipo')), 'Categoria'), 'conditions' => array('Produto.sku' => $valor['sku'])));
	    if ($produto['Produto']['id']) {
		if ($produto['AtributoProduto']) {
		    foreach ($produto['AtributoProduto'] as $atr):
			unset($atr['atributo_id']);
			unset($atr['produto_id']);
			foreach ($atr as $atrs):
			    if (isset($atrs['AtributoTipo']['nome'])) {
				$dbAtributos[$this->_createTag($atrs['AtributoTipo']['nome']) . '#' . $this->_createTag($atrs['valor'])] = array('nome' => trim($atrs['AtributoTipo']['nome']), 'valor' => trim($atrs['valor']), 'filtro' => $atrs['filtro']);
			    }
			endforeach;
		    endforeach;
		}
		//atributos do xml
		foreach ($valor['filhas'] as $atrs):
		    $xmlAtributos[$this->_createTag($atrs['nome']) . '#' . $this->_createTag($atrs['valor'])] = array('nome' => $atrs['nome'], 'valor' => $atrs['valor'], 'filtro' => $atrs['filtro']);
		endforeach;

		$remover = array_diff_assoc($dbAtributos, $xmlAtributos);

		foreach ($remover as $atr) {

		    $atributoTipo = $this->Atributo->AtributoTipo->find('first', array('recursive' => -1, 'conditions' => array('AtributoTipo.nome LIKE' => $atr['nome'])));
		    $atributoId = $this->Atributo->find('first', array('recursive' => -1, 'conditions' => array('Atributo.codigo LIKE' => $this->_createTag($atr['valor']), 'Atributo.atributo_tipo_id' => $atributoTipo['AtributoTipo']['id'])));


		    if (!$atributoTipo['AtributoTipo']['id'] || !$atributoId) {
			continue;
		    }
		    $this->Atributo->AtributoProduto->Query("DELETE FROM atributos_produtos WHERE atributo_id='{$atributoId['Atributo']['id']}' AND produto_id='{$produto['Produto']['id']}' ");
		    $this->Atributo->Query("DELETE FROM atributos a WHERE a.atributo_tipo_id='{$atributoTipo['AtributoTipo']['id']}' AND a.id = '{$atributoId['Atributo']['id']}' ");
		    foreach ($produto['Categoria'] as $categoria) {
			$this->Atributo->CategoriaAtributo->Query("DELETE FROM categorias_atributos WHERE atributo_id='{$atributoId['Atributo']['id']}' AND categoria_id='{$categoria['id']}'");
		    }
		}

		$adicionar = array_diff_assoc($xmlAtributos, $dbAtributos);


		foreach ($adicionar as $atr) {
		    if ($atr['valor'] == "") {
			$log .= 'ERROR valor do atributo vazio\n';
			continue;
		    }

		    $atributoTipo = $this->AtributoTipo->find('first', array('recursive' => -1, 'conditions' => array('AtributoTipo.nome LIKE' => $atr['nome'])));

		    if (!$atributoTipo['AtributoTipo']['id']) {
			$data['id'] = '';
			$data['nome'] = $atr['nome'];

			if (!$this->AtributoTipo->save($data)) {
			    $log .= "ERROR atributo_tipo no cadastro {$atr['nome']}\n";
			}
			$atributoTipo['AtributoTipo']['id'] = $this->AtributoTipo->id;
		    }

		    $rest = $this->Atributo->find('first', array('recursive' => -1, 'conditions' => array('Atributo.atributo_tipo_id' => $atributoTipo['AtributoTipo']['id'], 'Atributo.codigo' => $this->_createTag($atr['valor']))));

		    if ($rest) {
			$atributo_id = $rest['Atributo']['id'];
		    } else {
			$Atributo['Atributo']['id'] = '';
			$Atributo['Atributo']['atributo_tipo_id'] = $atributoTipo['AtributoTipo']['id'];
			$Atributo['Atributo']['valor'] = $atr['valor'];
			$Atributo['Atributo']['codigo'] = $this->_createTag($atr['valor']);
			$Atributo['Atributo']['sort'] = $produto['Produto']['id'];
			$Atributo['Atributo']['status'] = true;
			$Atributo['Atributo']['filtro'] = $atr['filtro'];

			if (!$this->Atributo->save($Atributo)) {
			    $log .= "ERRO atributo {$atr['nome']}->{$atr['valor']}->{$produto['Produto']['sku']}\n";
			}

			$atributo_id = $this->Atributo->id;
		    }

		    $rest = $this->AtributoProduto->find('first', array('recursive' => -1, 'conditions' => array('AtributoProduto.atributo_id' => $atributo_id, 'AtributoProduto.produto_id' => $produto['Produto']['id'])));

		    if (!$rest) {
			$this->AtributoProduto->Query("INSERT INTO atributos_produtos (atributo_id, produto_id)values('" . $atributo_id . "', '" . $produto['Produto']['id'] . "')");
		    }
		    //tabela auxiliar para busca de atributos por categoria
		    foreach ($produto['Categoria'] as $categoria) {
			$rest = $this->CategoriaAtributo->find('first', array('recursive' => -1, 'conditions' => array('CategoriaAtributo.categoria_id' => $categoria['id'], 'CategoriaAtributo.atributo_id' => $atributo_id)));
			if (!$rest) {
			    $this->CategoriaAtributo->Query("INSERT INTO categorias_atributos (atributo_id, categoria_id)values('" . $atributo_id . "', '" . $categoria['id'] . "') ");
			}
		    }
		}
	    }
	}
	if ($log == '') {
	    $log = 'OK';
	}
	$log = var_export($log, true);
	echo "<br>Terminei de rodar as " . date('d/m/Y H:i:s');
	$this->end($import_id, $log);
    }

    /* ok */

    function xml_categorias() {
	$import_id = $this->start('categorias');
	echo "Comecei a rodar ás " . date('d/m/Y H:i:s');
	App::import("Xml");
	$ff = str_replace('&', '&amp;', file_get_contents($this->importacao_dir . 'categoria.xml'));
	$ff = str_replace('<descricao>', '<descricao><![CDATA[', $ff);
	$ff = str_replace('</descricao>', ']]></descricao>', $ff);
	$ff = str_replace(' > ', ' &gt; ', $ff);
	$ff = str_replace(' < ', ' &lt; ', $ff);

	$xml = simplexml_load_string($ff);
	App::import('Model', 'Categoria');
	$this->Categoria = new Categoria();
	foreach ($xml as $pai) {
	    $nome = (string) $pai->nome;
	    $nome = $this->strtolower_br($nome);

	    $xmlCollection[$nome] = array(
		'nome' => $nome,
		'status' => (string) $pai->status,
		'descricao' => (string) $pai->descricao
	    );
	    if (isset($pai->filhas)) {
		$xmlCollection[$nome]['filhas'] = $this->getFilhasC($pai->filhas);
	    }
	}
	$log = '';
	foreach ($xmlCollection as $cat) {

	    $data_1['Categoria']['id'] = '';
	    $data_1['Categoria']['parent_id'] = 0;
	    $data_1['Categoria']['nome'] = $cat['nome'];
	    $data_1['Categoria']['seo_url'] = low(Inflector::slug($cat['nome'], '-'));
	    $data_1['Categoria']['descricao'] = $cat['descricao'];
	    $data_1['Categoria']['status'] = $cat['status'] == 'Ativo' ? true : false;
	    //se retornou da um update na categoria
	    if ($cb = $this->Categoria->find('first', array('recursive' => -1, 'limit' => 1, 'conditions' => array('parent_id' => 0, 'Categoria.nome LIKE' => $cat['nome'])))) {
		$data_1['Categoria']['id'] = $cb['Categoria']['id'];
		$log .= "SUCESSO departamento = {$cb['Categoria']['id']}\n";
	    } else {
		$log .= "ERROR departamento {$filhas['nome']} não encontrado parent_id 0";
	    }

	    $this->Categoria->recursive = -1;
	    $this->Categoria->save($data_1, false);
	    $cat_id = $this->Categoria->id;


	    foreach ($cat['filhas'] as $filhas) {
		$data_2['id'] = '';
		$data_2['parent_id'] = $cat_id;
		$data_2['nome'] = $filhas['nome'];
		$data_2['seo_url'] = low(Inflector::slug($cat['nome'], '-')) . '/' . low(Inflector::slug($filhas['nome'], '-'));
		$data_2['descricao'] = $filhas['descricao'];
		$data_2['status'] = $filhas['status'] == 'Ativo' ? true : false;
		if ($cb2 = $this->Categoria->find('first', array('recursive' => -1, 'limit' => 1, 'conditions' => array('Categoria.parent_id' => $cat_id, 'Categoria.nome LIKE' => trim($filhas['nome']))))) {
		    $data_2['id'] = $cb2['Categoria']['id'];
		    $log .= "SUCESSO categoria = {$cb2['Categoria']['id']}\n";
		} else {
		    $log .= "ERROR categoria {$filhas['nome']} não encontrada parent_id {$cat_id}";
		}

		$this->Categoria->save($data_2, false);

		$subcategoria_id = $this->Categoria->id;

		foreach ($filhas['filhas'] as $subFilhas) {
		    $data_3['id'] = '';
		    $data_3['parent_id'] = $subcategoria_id;
		    $data_3['nome'] = trim($subFilhas['nome']);
		    $data_3['seo_url'] = low(Inflector::slug(trim($cat['nome']), '-')) . '/' . low(Inflector::slug(trim($filhas['nome']), '-')) . '/' . low(Inflector::slug(trim($subFilhas['nome']), '-'));
		    $data_3['descricao'] = $subFilhas['descricao'];
		    $data_3['status'] = $subFilhas['status'] == 'Ativo' ? true : false;
		    if ($cb3 = $this->Categoria->find('first', array('recursive' => -1, 'limit' => 1, 'conditions' => array('Categoria.parent_id' => $subcategoria_id, 'Categoria.nome LIKE' => trim($subFilhas['nome']))))) {
			$data_3['id'] = $cb3['Categoria']['id'];
			$log .= "SUCESSO subcategoria = {$cb3['Categoria']['id']}\n";
		    } else {
			$log .= "ERROR subcategoria {$subFilhas['nome']} não encontrada parent_id {$subcategoria_id}\n";
		    }
		    $this->Categoria->save($data_3, false);
		}
	    }
	}

	echo "<br>Terminei de rodar as " . date('d/m/Y H:i:s');
	$this->end($import_id, $log);
    }

    function xml_produtos_variacoes() {
	$import_id = $this->start('variacoes_produtos');
	echo "Comecei a rodar ás " . date('d/m/Y H:i:s');
	App::import("Xml");
	$ff = str_replace('&', '&amp;', file_get_contents($this->importacao_dir . 'produto_variacoes.xml'));
	$ff = str_replace(' > ', ' &gt; ', $ff);
	$ff = str_replace(' < ', ' &lt; ', $ff);

	$xml = simplexml_load_string($ff);
	App::import('Model', 'Produto');
	App::import('Model', 'Variacao');
	App::import('Model', 'VariacaoProduto');
	$this->Produto = new Produto();
	$this->Variacao = new Variacao();
	$this->VariacaoProduto = new VariacaoProduto();
	foreach ($xml as $pai) {
	    foreach ($pai->produto as $produto) {
		if (isset($produto->variacoes)) {
		    $xmlCollection[(string) $pai->id][(string) $produto->sku]['variacoes'] = $this->getVariacoes($produto->variacoes);
		}
	    }
	}
	$log = '';
	$count = 0;

	foreach ($xmlCollection as $agrupador => $grupos) {

	    $array_xml = array();
	    $array_db = array();

	    foreach ($grupos as $sku => $variacoes) {
		foreach ($variacoes['variacoes'] as $variacao) {
		    $array_xml[$agrupador . '#' . $variacao['nome'] . '#' . $variacao['valor'] . '#' . $sku] = array(
			'agrupador' => $agrupador,
			'variacao' => $variacao['nome'],
			'status' => $variacao['ativo'],
			'ordem' => $variacao['ordem'],
			'variacao_produto' => $variacao['valor'],
			'sku' => $sku
		    );
		}
	    }

	    $db = $this->VariacaoProduto->find('all', array('conditions' => array('VariacaoProduto.agrupador' => $agrupador)));
	    foreach ($db as $var_db) {
		$array_db[$agrupador . '#' . $var_db['Variacao']['nome'] . '#' . $var_db['VariacaoProduto']['valor'] . '#' . $var_db['VariacaoProduto']['sku']] = array(
		    'agrupador' => $agrupador,
		    'variacao' => $var_db['Variacao']['nome'],
		    'ordem' => $var_db['Variacao']['ordem'],
		    'status' => $var_db['Variacao']['status'],
		    'variacao_produto' => $var_db['VariacaoProduto']['valor'],
		    'sku' => $var_db['VariacaoProduto']['sku']
		);
	    }
	    $adicionar = array_diff_assoc($array_xml, $array_db);

	    $remover = array_diff_assoc($array_db, $array_xml);

	    foreach ($adicionar as $variacoes) {

		$produto = $this->Produto->find('first', array('conditions' => array('Produto.sku' => $variacoes['sku'])));

		if (!$produto) {
		    continue;
		}

		if ($var = $this->Variacao->find('first', array('conditions' => array('Variacao.nome' => $variacoes['variacao'])))) {
		    $v['id'] = $var['Variacao']['id'];
		} else {
		    $v['id'] = '';
		}
		$v['nome'] = $variacoes['variacao'];
		$v['ordem'] = $variacoes['ordem'];
		$v['status'] = true;
		$this->Variacao->save($v);

		$variacao_id = $this->Variacao->id;

		if (!$variacoes['variacao_produto']) {
		    continue;
		}
		$vp['sku'] = $variacoes['sku'];
		$vp['status'] = $variacoes['status'];
		$vp['valor'] = $variacoes['variacao_produto'];
		$vp['ordem'] = $variacoes['ordem'];
		$vp['produto_id'] = $produto['Produto']['id'];
		$vp['variacao_id'] = $variacao_id;
		$vp['agrupador'] = $variacoes['agrupador'];
		$where = array('AND' => array('VariacaoProduto.sku' => $variacoes['sku'], 'VariacaoProduto.agrupador' => $variacoes['agrupador'], 'VariacaoProduto.variacao_id' => $variacao_id, 'VariacaoProduto.produto_id' => $produto['Produto']['id'], 'VariacaoProduto.valor' => $variacoes['variacao_produto']));
		if ($var = $this->VariacaoProduto->find('first', array('conditions' => $where))) {
		    $vp['id'] = $var['VariacaoProduto']['id'];
		} else {
		    $vp['id'] = '';
		}
		$this->VariacaoProduto->save($vp);
	    }

	    foreach ($remover as $del) {
		$var = $this->Variacao->find('first', array('conditions' => array('Variacao.nome' => $del['variacao'])));
		$sql = array('VariacaoProduto.agrupador' => $del['agrupador'], 'VariacaoProduto.variacao_id' => $var['Variacao']['id'], 'VariacaoProduto.valor' => $del['variacao_produto'], 'VariacaoProduto.sku' => $del['sku']);
		$r = $this->VariacaoProduto->deleteAll($sql, false);
	    }
	}
	echo "<br>Terminei de rodar as " . date('d/m/Y H:i:s');
	$this->end($import_id, $log);
    }

    //fueltech
    function importa_variacoes() {

	///import helper
	App::import("helper", "String");
	$this->String = new StringHelper();

	App::import("Model", "VariacaoTipo");
	$this->VariacaoTipo = new VariacaoTipo();

	//banco de dados
	mysql_connect("127.0.0.1", "root", "");
	mysql_select_db("fueltech_producao2");
	mysql_query('SET NAMES utf8');

	$sql = "SELECT * FROM prms_grade_tipo";
	$result = mysql_query($sql);
	while ($row = mysql_fetch_assoc($result)) {
	    $data = '';
	    $data['VariacaoTipo']['id'] = '';
	    $data['VariacaoTipo']['nome'] = $row['tpgrd_label'];
	    $data['VariacaoTipo']['ordem'] = 1;
	    $data['VariacaoTipo']['codigo'] = low(Inflector::slug($row['tpgrd_label'], '-'));
	    $data['VariacaoTipo']['codigo_externo'] = $row['tpgrd_id'];

	    $this->VariacaoTipo->save($data, false);
	}
    }

    function importa_atributos() {

	///import helper
	App::import("helper", "String");
	$this->String = new StringHelper();

	App::import("Model", "AtributoTipo");
	$this->AtributoTipo = new AtributoTipo();

	//banco de dados
	mysql_connect("127.0.0.1", "root", "");
	mysql_select_db("fueltech_producao2");
	mysql_query('SET NAMES utf8');

	$sql = "SELECT * FROM prms_grade_tipo";
	$result = mysql_query($sql);
	while ($row = mysql_fetch_assoc($result)) {
	    $data = '';
	    $data['AtributoTipo']['id'] = '';
	    $data['AtributoTipo']['nome'] = $row['tpgrd_label'];
	    $data['AtributoTipo']['sort'] = 1;
	    $data['AtributoTipo']['codigo'] = low(Inflector::slug($row['tpgrd_label'], '-'));
	    $data['AtributoTipo']['codigo_externo'] = $row['tpgrd_id'];

	    $this->AtributoTipo->save($data, false);
	}
    }

    function importa_revendedores() {

	///import helper
	App::import("helper", "String");
	$this->String = new StringHelper();

	//import model
	App::import('Model', 'Revendedor');
	$this->Revendedor = new Revendedor();
	$this->Revendedor->Behaviors->detach('MeioUpload');

	//banco de dados
	mysql_connect("127.0.0.1", "root", "");
	mysql_select_db("fueltech_producao2");
	mysql_query('SET NAMES utf8');

	//ftp		
	$ftp_server = 'ftp.fueltech.com.br'; // FTP do destino
	$ftp_user_name = 'fueltech';
	$ftp_user_pass = 'ftadmin';
	$conn_id = ftp_connect($ftp_server);
	$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

	//query
	$sql = "SELECT * FROM stgs_textos LEFT JOIN stgs_sgrptexto ON stgs_textos.`txt_sgrpcdg` = stgs_sgrptexto.`sgtxt_cdg` WHERE stgs_sgrptexto.`sgtxt_grpcdg` = 6";
	$result = mysql_query($sql);

	while ($row = mysql_fetch_assoc($result)) {

	    $data = '';

	    print('begin <hr />');
	    print($row['txt_descr'] . '<br />');

	    $data['Revendedor']['id'] = '';
	    $data['Revendedor']['nome'] = $row['txt_descr'];
	    $data['Revendedor']['descricao_resumida'] = $row['txt_txt_resumo'];
	    $data['Revendedor']['descricao_completa'] = $row['txt_txt_noticia'];

	    //pais - estado
	    $sgtxt_descr = $row['sgtxt_descr'];
	    if (strpos($sgtxt_descr, "-") !== false) {
		$tmp = explode("-", $sgtxt_descr);
		$data['Revendedor']['estado'] = trim($tmp[1]);
		$data['Revendedor']['pais'] = trim($tmp[0]);
		$data['Revendedor']['localidade_label'] = trim($data['estado']);
	    } else {
		$data['Revendedor']['pais'] = trim($sgtxt_descr);
		$data['Revendedor']['localidade_label'] = trim($data['pais']);
	    }

	    $dir = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'uploads' . DS . 'revendedor' . DS . 'thumb' . DS;

	    $remote_dir = "/www/tw5.0/contas/00009_v4/arquivos/workspaces/stgs/textos/" . $row['txt_cdg'] . "/190x142/";

	    //lista arquivos
	    $contents = ftp_nlist($conn_id, $remote_dir);
	    $imagem = end($contents);

	    $nome_imagem = end(explode("/", $imagem));

	    //seto o destino
	    $destino = $dir . $nome_imagem;

	    //baixo o arquivo se o mesmo já não existir
	    if (!file_exists($dir . $nome_imagem)) {
		if (!ftp_get($conn_id, $destino, $imagem, FTP_BINARY)) {
		    echo "Ocorreu um erro\n";
		}
	    }

	    $data['Revendedor']['thumb_filename'] = $nome_imagem;
	    $data['Revendedor']['thumb_dir'] = 'uploads/revendedor/thumb';
	    $data['Revendedor']['thumb_mimetype'] = 'image/jpeg';
	    $data['Revendedor']['thumb_filesize'] = '13722';
	    $data['Revendedor']['status'] = true;

	    debug($data);

	    if (is_array($data) && count($data) > 0) {
		if (!$this->Revendedor->save($data, false)) {
		    print('erro :' . $data['nome'] . '<br />');
		    print($this->Revendedor->invalidFields());
		}
	    }

	    print('end <hr />');
	}

	// fecha a conexão
	ftp_close($conn_id);
    }

    function importa_parceiros() {

	///import helper
	App::import("helper", "String");
	$this->String = new StringHelper();

	//import model
	App::import('Model', 'Parceiro');
	$this->Parceiro = new Parceiro();
	$this->Parceiro->Behaviors->detach('MeioUpload');

	//banco de dados
	mysql_connect("127.0.0.1", "root", "");
	mysql_select_db("fueltech_producao2");
	mysql_query('SET NAMES utf8');

	//ftp		
	$ftp_server = 'ftp.fueltech.com.br'; // FTP do destino
	$ftp_user_name = 'fueltech';
	$ftp_user_pass = 'ftadmin';
	$conn_id = ftp_connect($ftp_server);
	$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

	//query
	$sql = "SELECT * FROM stgs_textos LEFT JOIN stgs_sgrptexto ON stgs_textos.`txt_sgrpcdg` = stgs_sgrptexto.`sgtxt_cdg` WHERE stgs_sgrptexto.`sgtxt_grpcdg` = 7";
	$result = mysql_query($sql);

	while ($row = mysql_fetch_assoc($result)) {

	    $data = '';

	    print('begin <hr />');
	    print($row['txt_descr'] . '<br />');

	    $data['Parceiro']['id'] = '';
	    $data['Parceiro']['nome'] = $row['txt_descr'];
	    $data['Parceiro']['descricao_resumida'] = $row['txt_txt_resumo'];
	    $data['Parceiro']['descricao_completa'] = $row['txt_txt_noticia'];

	    //pais - estado
	    $sgtxt_descr = $row['sgtxt_descr'];
	    if (strpos($sgtxt_descr, "-") !== false) {
		$tmp = explode("-", $sgtxt_descr);
		$data['Parceiro']['estado'] = trim($tmp[1]);
		$data['Parceiro']['pais'] = trim($tmp[0]);
		$data['Parceiro']['localidade_label'] = trim($data['estado']);
	    } else {
		$data['Parceiro']['pais'] = trim($sgtxt_descr);
		$data['Parceiro']['localidade_label'] = trim($data['pais']);
	    }

	    $dir = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'uploads' . DS . 'parceiro' . DS . 'thumb' . DS;

	    $remote_dir = "/www/tw5.0/contas/00009_v4/arquivos/workspaces/stgs/textos/" . $row['txt_cdg'] . "/190x142/";

	    //lista arquivos
	    $contents = ftp_nlist($conn_id, $remote_dir);
	    $imagem = end($contents);

	    $nome_imagem = end(explode("/", $imagem));

	    //seto o destino
	    $destino = $dir . $nome_imagem;

	    //baixo o arquivo se o mesmo já não existir
	    if (!file_exists($dir . $nome_imagem)) {
		if (!ftp_get($conn_id, $destino, $imagem, FTP_BINARY)) {
		    echo "Ocorreu um erro\n";
		}
	    }

	    $data['Parceiro']['thumb_filename'] = $nome_imagem;
	    $data['Parceiro']['thumb_dir'] = 'uploads/parceiro/thumb';
	    $data['Parceiro']['thumb_mimetype'] = 'image/jpeg';
	    $data['Parceiro']['thumb_filesize'] = '13722';
	    $data['Parceiro']['status'] = true;

	    debug($data);

	    if (is_array($data) && count($data) > 0) {
		if (!$this->Parceiro->save($data, false)) {
		    print('erro :' . $data['nome'] . '<br />');
		    print($this->Parceiro->invalidFields());
		}
	    }

	    print('end <hr />');
	}

	// fecha a conexão
	ftp_close($conn_id);
    }

    function importa_categorias() {

	///import helper
	App::import("helper", "String");
	$this->String = new StringHelper();

	//import model
	App::import('Model', 'Categoria');
	$this->Categoria = new Categoria();
	$this->Categoria->Behaviors->detach('MeioUpload');

	//import model
	App::import('Model', 'Linguagem');
	$this->Linguagem = new Linguagem();
	$idiomas = $this->Linguagem->find("all", array("conditions" => array('Linguagem.status' => true)));

	//banco de dados
	$con1 = mysql_connect("127.0.0.1", "root", "");
	$db1 = mysql_select_db("fueltech_producao2", $con1);
	mysql_query('SET NAMES utf8');

	//ftp		
	$ftp_server = 'ftp.fueltech.com.br'; // FTP do destino
	$ftp_user_name = 'fueltech';
	$ftp_user_pass = 'ftadmin';
	$conn_id = ftp_connect($ftp_server);
	$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

	//query de departamentos
	$sql_dep = "SELECT 
						prms_departamento.`prddep_id` AS departamento_id, 
						prms_departamento.`prddep_descricao` AS departamento_nome 
					FROM prms_departamento";
	$result1 = mysql_query($sql_dep, $con1);

	while ($row = mysql_fetch_assoc($result1)) {

	    print('begin DEPARTAMENTO<hr />');
	    print($row['departamento_nome'] . '<br />');

	    $data['Categoria']['id'] = '';
	    $data['Categoria']['parent_id'] = 0;
	    $data['Categoria']['status'] = 1;
	    $data['Categoria']['destaque_home'] = 0;
	    $data['Categoria']['site_id'] = 1;
	    $data['Categoria']['nome'] = $row['departamento_nome'];
	    $data['Categoria']['seo_url'] = low(Inflector::slug($row['departamento_nome'], '-'));
	    //$data['Categoria']['codigo_externo'] 	= $row['departamento_id'];

	    foreach ($idiomas as $key => $idioma) {
		$data['CategoriaDescricao'][$key]['language'] = $idioma['Linguagem']['codigo'];
		$data['CategoriaDescricao'][$key]['nome'] = $row['departamento_nome'];
	    }

	    if (is_array($data) && count($data) > 0) {
		if (!$this->Categoria->saveAll($data, false)) {
		    print('erro :' . $data['CategoriaDescricao'][0]['nome'] . '<br />');
		    print($this->Parceiro->invalidFields());
		} else {

		    $parent_id = $this->Categoria->id;

		    //con2
		    $con2 = mysql_connect("127.0.0.1", "root", "");
		    $db2 = mysql_select_db("fueltech_producao2", $con2);
		    mysql_query('SET NAMES utf8');

		    //query
		    $sql2 = "
							SELECT 
								prms_departamento.`prddep_id` AS departamento_id, prms_departamento.`prddep_descricao` AS departamento_nome,
								prms_grupo.`prdgrp_id` AS categoria_id, prms_grupo.`prdgrp_descricao` AS categoria_nome,
								prms_grupo.`prdgrp_dep_id` AS categoria_departamento_id, prms_grupo.`prdgrp_imagem` AS categoria_logo,
								prms_grupo.`prdgrp_descr_resumo` AS categoria_texto_seo		
							 FROM prms_grupo LEFT JOIN prms_departamento ON prms_grupo.`prdgrp_dep_id` = prms_departamento.`prddep_id`
							 WHERE prms_grupo.`prdgrp_dep_id` = " . $row['departamento_id'];

		    //print($sql2);die;
		    $result2 = mysql_query($sql2, $con2);

		    while ($row2 = mysql_fetch_assoc($result2)) {

			$data = '';

			print('begin GRUPO <hr />');
			print($row2['categoria_nome'] . '<br />');

			$data['Categoria']['id'] = '';
			$data['Categoria']['parent_id'] = $parent_id;
			$data['Categoria']['status'] = 1;
			$data['Categoria']['destaque_home'] = 0;
			$data['Categoria']['site_id'] = 1;
			$data['Categoria']['nome'] = $row2['categoria_nome'];
			$data['Categoria']['seo_url'] = low(Inflector::slug($row['departamento_nome'], '-')) . '/' . low(Inflector::slug($row2['categoria_nome'], '-'));
			//$data['Categoria']['codigo_externo'] 	= $row2['categoria_id'];

			foreach ($idiomas as $key => $idioma) {
			    $data['CategoriaDescricao'][$key]['language'] = $idioma['Linguagem']['codigo'];
			    $data['CategoriaDescricao'][$key]['nome'] = $row2['categoria_nome'];
			    $data['CategoriaDescricao'][$key]['descricao'] = $row2['categoria_texto_seo'];
			}

			$dir = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'uploads' . DS . 'categoria' . DS . 'thumb' . DS;
			$remote_dir = "/www/tw5.0/contas/00009_v4/arquivos/workspaces/prms/grupo/" . $row2['categoria_id'] . "/img/" . $row2['categoria_logo'];

			//lista arquivos
			$contents = ftp_nlist($conn_id, $remote_dir);
			$imagem = end($contents);

			$nome_imagem = end(explode("/", $imagem));

			//seto o destino
			$destino = $dir . $nome_imagem;

			//baixo o arquivo se o mesmo já não existir
			if (!file_exists($dir . $nome_imagem)) {
			    if (!ftp_get($conn_id, $destino, $imagem, FTP_BINARY)) {
				echo "Ocorreu um erro\n";
			    }
			}

			$data['Categoria']['thumb_filename'] = $nome_imagem;
			$data['Categoria']['thumb_dir'] = 'uploads/categoria/thumb';
			$data['Categoria']['thumb_mimetype'] = 'image/jpeg';
			$data['Categoria']['thumb_filesize'] = '13722';
			$data['Categoria']['status'] = true;

			debug($data);

			if (is_array($data) && count($data) > 0) {
			    if (!$this->Categoria->saveAll($data, false)) {
				print('erro :' . $data['CategoriaDescricao'][0]['nome'] . '<br />');
				print($this->Categoria->invalidFields());
			    } else {
				$parent_id2 = $this->Categoria->id;

				//con2
				$con3 = mysql_connect("127.0.0.1", "root", "");
				$db2 = mysql_select_db("fueltech_producao2", $con3);
				mysql_query('SET NAMES utf8');

				$sql3 = "SELECT 
												prms_sgrupo.`prdsgrp_id` AS categoria_id, prms_sgrupo.`prdsgrp_descricao` AS categoria_nome 
										FROM prms_sgrupo WHERE prdsgrp_prdgrp_id = " . $row2['categoria_id'];
				$result3 = mysql_query($sql3, $con3);

				// print($sql3);
				// print_r($result3);die;

				while ($row3 = mysql_fetch_assoc($result3)) {

				    $data = '';

				    print('begin SUBGRUPO <hr />');
				    print($row3['categoria_nome'] . '<br />');

				    if (low(Inflector::slug($row3['categoria_nome'], '-')) != low(Inflector::slug($row2['categoria_nome'], '-'))) {

					$data['Categoria']['id'] = '';
					$data['Categoria']['parent_id'] = $parent_id2;
					$data['Categoria']['status'] = 1;
					$data['Categoria']['destaque_home'] = 0;
					$data['Categoria']['site_id'] = 1;
					$data['Categoria']['nome'] = $row3['categoria_nome'];
					$data['Categoria']['seo_url'] = low(Inflector::slug($row['departamento_nome'], '-')) . '/' . low(Inflector::slug($row2['categoria_nome'], '-')) . '/' . low(Inflector::slug($row3['categoria_nome'], '-'));
					$data['Categoria']['codigo_externo'] = $row3['categoria_id'];

					foreach ($idiomas as $key => $idioma) {
					    $data['CategoriaDescricao'][$key]['language'] = $idioma['Linguagem']['codigo'];
					    $data['CategoriaDescricao'][$key]['nome'] = $row3['categoria_nome'];
					}

					if (is_array($data) && count($data) > 0) {
					    if (!$this->Categoria->saveAll($data, false)) {
						print('erro :' . $data['CategoriaDescricao'][0]['nome'] . '<br />');
						print($this->Categoria->invalidFields());
					    }
					}
				    } else {
					$data['Categoria']['parent_id'] = $parent_id;
					$data['Categoria']['id'] = $parent_id2;
					$data['Categoria']['codigo_externo'] = $row3['categoria_id'];
					if (is_array($data) && count($data) > 0) {
					    if (!$this->Categoria->save($data, false)) {
						print($this->Categoria->invalidFields());
					    }
					}
				    }
				}
			    }
			}
		    }
		}
	    }
	}
	print('end <hr />');
	// fecha a conexão
	ftp_close($conn_id);
    }

    function importa_atualizada_erp_produto() {
	//banco de dados
	$con1 = mysql_connect("127.0.0.1", "root", "");
	$db1 = mysql_select_db("fueltech_producao2", $con1);
	mysql_query('SET NAMES utf8');

	$sql = "SELECT 	
                    p.prd_id AS produto_id,
                    p.prd_codigoloja AS produto_codigo_erp
                FROM 
                    prms_produto AS p";

	$result = mysql_query($sql, $con1);

	//import model
	App::import('Model', 'Produto');
	$this->Produto = new Produto();

	$cont = 1;

	while ($row = mysql_fetch_assoc($result)) {

	    $produto = $this->Produto->find('first', array('recursive' => -1, 'fields' => array('Produto.id'), 'conditions' => array('Produto.codigo_externo' => $row['produto_id'])));


	    $data['Produto']['id'] = $produto['Produto']['id'];
	    $data['Produto']['codigo_erp'] = $row['produto_codigo_erp'];

//            print($cont);
//            print('<br />');
//            debug($data);
	    if ($data['Produto']['id'] != "") {
		if ($this->Produto->save($data, false)) {
		    print('Salvou produto: ' . $cont . '<hr />');
		}
	    }
//            print('<hr />');
//            
	    $cont++;
	}
    }

    function importa_produtos() {

	///import helper
	App::import("helper", "String");
	$this->String = new StringHelper();

	//import model
	App::import('Model', 'Produto');
	$this->Produto = new Produto();

	App::import('Model', 'Categoria');
	$this->Categoria = new Categoria();

	App::import('Model', 'ProdutoCategoria');
	$this->ProdutoCategoria = new ProdutoCategoria();
	$this->ProdutoCategoria->primaryKey = '';

	App::import('Model', 'VariacaoTipo');
	$this->VariacaoTipo = new VariacaoTipo();

	App::import('Model', 'Variacao');
	$this->Variacao = new Variacao();

	App::import('Model', 'VariacaoProduto');
	$this->VariacaoProduto = new VariacaoProduto();
	$this->VariacaoProduto->primaryKey = '';

	App::import('Model', 'AtributoTipo');
	$this->AtributoTipo = new AtributoTipo();

	App::import('Model', 'Atributo');
	$this->Atributo = new Atributo();

	App::import('Model', 'AtributoProduto');
	$this->AtributoProduto = new AtributoProduto();
	$this->AtributoProduto->primaryKey = '';

	App::import('Model', 'CategoriaAtributo');
	$this->CategoriaAtributo = new CategoriaAtributo();
	$this->CategoriaAtributo->primaryKey = '';

	//import model
	App::import('Model', 'Linguagem');
	$this->Linguagem = new Linguagem();
	$idiomas = $this->Linguagem->find("all", array("conditions" => array('Linguagem.status' => true, 'Linguagem.externo' => 0)));

	//banco de dados
	$con1 = mysql_connect("127.0.0.1", "root", "");
	$db1 = mysql_select_db("fueltech_producao2", $con1);
	mysql_query('SET NAMES utf8');

	//banco de dados
	$con12 = mysql_connect("127.0.0.1", "root", "");
	$db12 = mysql_select_db("fueltech_producao2", $con12);
	mysql_query('SET NAMES utf8');

	//ftp		
	$ftp_server = 'ftp.fueltech.com.br'; // FTP do destino
	$ftp_user_name = 'fueltech';
	$ftp_user_pass = 'ftadmin';
	$conn_id = ftp_connect($ftp_server);
	$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
	ftp_pasv($conn_id, true);

	//query
	// $sql_old = "SELECT 
	// prd_id AS produto_id, prd_descricao AS nome, 
	// prd_sgrp_id AS categoria_id, prms_sgrupo.`prdsgrp_descricao` AS categoria_nome,
	// prd_grd_id AS grande_id, prms_grades.`grd_descricao` AS grade_nome, 
	// prd_grd_id_base AS grade_id_base,
	// prd_preco_b2b, prd_preco_de, prd_preco, 
	// prd_tipocobr_id, prd_destaque_tipo AS tipo_destaque_id, prms_tipodestaque.`tipodestaq_descricao` AS tipo_destaque_nome, 
	// prd_unid_id AS unidade_id, prms_unidade.`unid_descricao` AS unidade_nome, 
	// prd_estoque_disp, prd_estoque_min, prd_peso, prd_peso_cubado, prd_descr_resumo, 
	// prd_ativo, prd_ipi_aliq, prd_ipi_cobrar, stgs_textos.`txt_txt_noticia` AS produto_descricao
	// FROM prms_produto 
	// LEFT JOIN prms_sgrupo ON prms_sgrupo.prdsgrp_id = prd_sgrp_id
	// LEFT JOIN prms_grades ON prms_grades.grd_id = prd_grd_id
	// LEFT JOIN prms_tipodestaque ON prms_tipodestaque.tipodestaq_id = prd_destaque_tipo
	// LEFT JOIN prms_unidade ON prms_unidade.unid_id = prd_unid_id
	// LEFT JOIN prms_produto_ctms ON prms_produto_ctms.prdctms_prd_id = prms_produto.`prd_id`
	// LEFT JOIN stgs_textos ON stgs_textos.txt_sgrpcdg = prms_produto_ctms.prdctms_prd_id
	// GROUP BY prms_produto.`prd_id`";
	// $sql = "SELECT 
	// prd_id AS produto_id, prd_descricao AS nome, 
	// prd_sgrp_id AS categoria_id, prms_sgrupo.`prdsgrp_descricao` AS categoria_nome,
	// prd_grd_id AS grande_id, prms_grades.`grd_descricao` AS grade_nome, 
	// prd_grd_id_base AS grade_id_base,
	// prd_preco_b2b, prd_preco_de, prd_preco, 
	// prd_tipocobr_id, prd_destaque_tipo AS tipo_destaque_id, prms_tipodestaque.`tipodestaq_descricao` AS tipo_destaque_nome, 
	// prd_unid_id AS unidade_id, prms_unidade.`unid_descricao` AS unidade_nome, 
	// prd_estoque_disp, prd_estoque_min, prd_peso, prd_peso_cubado, prd_descr_resumo, 
	// prd_ativo, prd_ipi_aliq, prd_ipi_cobrar, 
	// stgs_textos.`txt_txt_noticia` AS produto_descricao,
	// stgs_textos.`txt_txt_noticia` AS produto_descricao,
	// FROM prms_produto 
	// LEFT JOIN prms_sgrupo ON prms_sgrupo.prdsgrp_id = prd_sgrp_id
	// LEFT JOIN prms_grades ON prms_grades.grd_id = prd_grd_id
	// LEFT JOIN prms_tipodestaque ON prms_tipodestaque.tipodestaq_id = prd_destaque_tipo
	// LEFT JOIN prms_unidade ON prms_unidade.unid_id = prd_unid_id
	// LEFT JOIN prms_produto_ctms ON prms_produto_ctms.prdctms_prd_id = prms_produto.`prd_id`
	// LEFT JOIN stgs_textos ON stgs_textos.txt_sgrpcdg = prms_produto_ctms.prdctms_prd_id
	// GROUP BY prms_produto.`prd_id`
	// ORDER BY grade_id_base";

	$sql = "SELECT 	p.prd_id AS produto_id,
					p.prd_descricao AS produto_descricao,
					p.`prd_descr_base` AS produto_descricao_base,
					p.`prd_descr_peq` AS produto_descricao_pequena,
					p.`prd_sgrp_id` AS produto_subgrupo_id,
					p_sgrupo.`prdsgrp_descricao` AS produto_subgrupo_nome,
					p.prd_grd_id AS produto_grade_id,
					p.`prd_grd_id_base` AS produto_grade_base_id,
					p.`prd_grd_eh_base` AS produto_grade_eh_base,
					p.`prd_preco_b2b` AS produto_preco_b2b,
					p.`prd_preco_de` AS produto_preco_de,
					p.`prd_preco` AS produto_preco,
					p.`prd_destaque_tipo` AS produto_destaque_cat,
					#p_tipo_destaque.`tipodestaq_descricao` as produto_destaque_cat_nome,
					p.`prd_estoque_disp` AS produto_quantidade,
					p.`prd_estoque_min` AS produto_quantidade_minima,
					p.`prd_unid_id` AS produto_unidade_id,
					#p_unidades.`unid_descricao` as produto_unidade_nome,
					p.`prd_peso` AS produto_peso,
					p.`prd_peso_cubado` AS produto_peso_cubado,
					p.`prd_descr_resumo` AS produto_descricao_resumo,
					p.`prd_views` AS produto_visualizacoes,
					p.`prd_ativo` AS produto_status,
					p.`prd_ipi_aliq` AS produto_aliq_ipi,
					p.`prd_ipi_cobrar` AS produto_ipi_cobrar,
					p_ctms.`prdctms_lermais_sgrp_id` AS produto_llermais_sgrupo_id,
                                        p.prd_codigoloja AS produto_codigo_erp
				FROM 
					prms_produto AS p,
					prms_sgrupo AS p_sgrupo,
					prms_produto_ctms AS p_ctms
					#prms_tipodestaque as p_tipo_destaque,
					#prms_unidade as p_unidades
				WHERE 
					p_sgrupo.`prdsgrp_id` = p.prd_sgrp_id 
					AND p_ctms.`prdctms_prd_id` = p.`prd_id`
					#AND p_tipo_destaque.`tipodestaq_id` = p.prd_destaque_tipo
					#AND p_unidades.`unid_id` = p.`prd_unid_id`
				ORDER BY produto_grade_base_id ASC, produto_grade_eh_base DESC";

	//ASC, produto_grade_eh_base DESC

	$result = mysql_query($sql, $con1);

	$cont = 1;
	//print(mysql_num_rows($result));
	//die;
	while ($row = mysql_fetch_assoc($result)) {

	    $data = '';

	    print('begin <hr />');
	    print('cont: ' . $cont . '<br />');
	    print($row['produto_id'] . '<br />');
	    // print('end <hr />');
	    // $cont++;
	    // continue;

	    $data['Produto']['id'] = '';
	    $data['Produto']['sku'] = substr(md5(date('Y-m-d H:i:s')) . rand(0, 1000), -11);
	    if ($row['produto_grade_id'] > 0) {
		$data['Produto']['agrupador'] = $row['produto_grade_id'];
	    } else {
		$data['Produto']['agrupador'] = substr(md5(date('Y-m-d H:i:s')) . rand(0, 1000), -11);
	    }
	    $data['Produto']['codigo_erp'] = $row['produto_codigo_erp'];

	    //if($row['grade_id_base'] == 0 || $row['grade_id_base'] == $row['produto_id']){
	    if ($row['produto_grade_eh_base'] == 'S') {
		$data['Produto']['parent_id'] = 0;
	    } else {
		$parent_id = $this->Produto->find('first', array('fields' => array('Produto.id', 'Produto.agrupador', 'Produto.sku'), 'conditions' => array('Produto.codigo_externo' => $row['produto_grade_base_id'])));
		if (isset($parent_id['Produto']['id']) && $parent_id['Produto']['id'] != null) {
		    $data['Produto']['parent_id'] = $parent_id['Produto']['id'];
		    $data['Produto']['agrupador'] = $parent_id['Produto']['agrupador'];
		    $data['Produto']['sku'] = $parent_id['Produto']['sku'];
		} else {
		    $data['Produto']['parent_id'] = 0;
		}
	    }

	    $data['Produto']['status'] = $row['produto_status'];
	    $data['Produto']['site_id'] = 1;

	    if ($row['produto_descricao_base'] != "") {
		$data['Produto']['nome'] = $row['produto_descricao_base'];
	    } else {
		$data['Produto']['nome'] = $row['produto_descricao'];
	    }

	    $data['Produto']['codigo_externo'] = $row['produto_id'];

	    if ($row['produto_preco_de'] > 0 && $row['produto_preco_de'] != $row['produto_preco']) {
		$data['Produto']['preco'] = $this->String->bcoToMoeda($row['produto_preco_de']);
		$data['Produto']['preco_promocao'] = $this->String->bcoToMoeda($row['produto_preco']);
	    } else {
		$data['Produto']['preco'] = $this->String->bcoToMoeda($row['produto_preco']);
		$data['Produto']['preco_promocao'] = $this->String->bcoToMoeda(0.00);
		;
	    }

	    $data['Produto']['quantidade'] = $row['produto_quantidade'];
	    $data['Produto']['quantidade_minima'] = $row['produto_quantidade_minima'];

	    $data['Produto']['largura'] = 11;
	    $data['Produto']['altura'] = 17;
	    $data['Produto']['profundidade'] = 20;
	    $data['Produto']['quantidade_disponivel'] = $row['produto_quantidade'];
	    $data['Produto']['quantidade_alocada'] = 0;

	    if ($row['produto_peso'] >= $row['produto_peso_cubado']) {
		$peso = $row['produto_peso'];
	    } else {
		$peso = $row['produto_peso_cubado'];
	    }
	    $data['Produto']['peso'] = $this->String->bcoToMoeda($peso);
	    //$data['Produto']['peso'] = $this->String->bcoToMoeda($row['produto_peso']);
	    $data['Produto']['peso_cubico'] = $row['produto_peso_cubado'];
	    $data['Produto']['preco_b2b'] = $this->String->bcoToMoeda($row['produto_preco_b2b']);
	    $data['Produto']['quantidade_acessos'] = $row['produto_visualizacoes'];

	    $sql12 = "SELECT 
						t.* FROM stgs_textos t 
					  INNER JOIN stgs_sgrptexto s 
						ON s.sgtxt_cdg = t.txt_sgrpcdg 
				      INNER JOIN stgs_grptexto g 
						ON g.gtxt_cdg = s.sgtxt_grpcdg
					  WHERE 
						t.txt_status != 2
					  AND 
						s.sgtxt_cdg = " . $row['produto_llermais_sgrupo_id'] . "
					  ORDER BY 
						txt_sgrpcdg ASC";
	    $result12 = mysql_query($sql12, $con12);

	    $produto_descricao = '';
	    $produto_ficha_tecnica = '';

	    while ($row12 = mysql_fetch_assoc($result12)) {
		if ($row12['txt_descr'] == "Informações Gerais") {
		    $produto_descricao = $row12['txt_txt_noticia'];
		} elseif ($row12['txt_descr'] == "Ficha Técnica") {
		    $produto_ficha_tecnica = $row12['txt_txt_noticia'];
		}
	    }

	    foreach ($idiomas as $key => $idioma) {
		$data['ProdutoDescricao'][$key]['language'] = $idioma['Linguagem']['codigo'];
		$data['ProdutoDescricao'][$key]['nome'] = $data['Produto']['nome'];
		$data['ProdutoDescricao'][$key]['descricao_resumida'] = $row['produto_descricao_resumo'];
		$data['ProdutoDescricao'][$key]['descricao'] = $produto_descricao;
		$data['ProdutoDescricao'][$key]['ficha_tecnica'] = $produto_ficha_tecnica;
	    }

	    //debug($data);
	    //gamba
	    //if($data['Produto']['nome'] == "") continue;
	    //begin save produto
	    if ($this->Produto->saveAll($data, false)) {

		print('<br />Produto: ok <br />');

		$lastid = $this->Produto->id;

		//begin save categoria
		$categoria_id = $this->Categoria->find('first', array('fields' => array('Categoria.id'), 'conditions' => array('Categoria.codigo_externo' => $row['produto_subgrupo_id'])));
		//debug($categoria_id);
		if ($lastid != 0) {
		    if ($this->ProdutoCategoria->save(array('produto_id' => $lastid, 'categoria_id' => $categoria_id['Categoria']['id']), false)) {
			print('<br />ProdutoCategoria: ok <br />');
		    } else {
			debug($this->ProdutoCategoria->invalidFields());
			//die();
		    }
		}
		//end save categoria
		//begin save variacoes
		//banco de dados
		$con33 = mysql_connect("127.0.0.1", "root", "");
		$db33 = mysql_select_db("fueltech_producao2", $con33);
		mysql_query('SET NAMES utf8');
		$sql33 = "SELECT * 
							FROM prms_produto_grade 
							LEFT JOIN prms_grade_conteudos ON prms_grade_conteudos.`ctgrd_id` = prms_produto_grade.`prdgrd_ctgrd_id`
							LEFT JOIN prms_grade_tipo ON prms_grade_tipo.`tpgrd_id` = prms_grade_conteudos.`ctgrd_tpgrd_id`
							WHERE prms_produto_grade.`prdgrd_prd_id` = " . $row['produto_id'];
		$result33 = mysql_query($sql33, $con33);

		//variacoes
		$result31 = $result33;
		$result32 = $result33;

		while ($row3 = mysql_fetch_assoc($result31)) {

		    //begin variacoes
		    $variacao_tipo = $this->VariacaoTipo->find('first', array('fields' => array('VariacaoTipo.id'), 'conditions' => array('VariacaoTipo.codigo_externo' => $row3['tpgrd_id'])));
		    $vt = $this->Variacao->find('first', array('recursive' => -1, 'conditions' => array('Variacao.variacao_tipo_id' => $variacao_tipo['VariacaoTipo']['id'], 'Variacao.valor' => $row3['ctgrd_label'])));
		    if (!$vt) {
			$data3 = '';
			$data3['Variacao']['id'] = '';
			$data3['Variacao']['variacao_tipo_id'] = $variacao_tipo['VariacaoTipo']['id'];
			$data3['Variacao']['valor'] = $row3['ctgrd_label'];
			$data3['Variacao']['ordem'] = $row3['ctgrd_ordem'];

			foreach ($idiomas as $key => $idioma) {
			    $data3['VariacaoDescricao'][$key]['language'] = $idioma['Linguagem']['codigo'];
			    $data3['VariacaoDescricao'][$key]['valor'] = $row3['ctgrd_label'];
			    $data3['VariacaoDescricao'][$key]['label'] = $row3['ctgrd_ref'];
			}

			if ($row3['tpgrd_thumb'] == 'S') {
			    //begin save ProdutoImagem
			    $dir = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'uploads' . DS . 'variacao' . DS . 'thumb' . DS;
			    $remote_dir = "/www/tw5.0/contas/00009_v4/arquivos/workspaces/prms/gradecont/" . $row3['ctgrd_id'];

			    //lista arquivos
			    $contents = ftp_nlist($conn_id, $remote_dir);
			    foreach ($contents as $file) {
				$res = ftp_size($conn_id, $file);
				if ($res != -1) {
				    print($file . '<hr />');

				    //$imagem = $file;
				    //$imagem = end($contents);				
				    $nome_imagem = end(explode("/", $file));
				    //seto o destino
				    $destino = $dir . $nome_imagem;

				    //begin produto imagem
				    $this->Variacao->Behaviors->detach('MeioUpload');
				    $data3['Variacao']['thumb'] = 1;
				    $data3['Variacao']['thumb_filename'] = $nome_imagem;
				    $data3['Variacao']['thumb_dir'] = 'uploads/variacao/thumb';
				    $data3['Variacao']['thumb_mimetype'] = 'image/gif';
				    $data3['Variacao']['thumb_filesize'] = '13722';
				}
			    }
			}

			if ($this->Variacao->saveAll($data3, false)) {
			    $variacao_id = $this->Variacao->id;
			    if (!file_exists($dir . $nome_imagem)) {
				//baixo o arquivo se o mesmo já não existir
				if (!ftp_get($conn_id, $destino, $file, FTP_BINARY)) {
				    print('destino: ' . $destino . '<br />');
				    print('imagem: ' . $file . '<br />');
				    echo "Ocorreu um erro\n";
				}
			    }
			}
		    } else {
			$variacao_id = $vt['Variacao']['id'];
		    }

		    //begin variacao produto
		    if ($lastid != 0 && $variacao_id != 0) {
			$variacao_produto = array('variacao_id' => $variacao_id, 'produto_id' => $lastid, 'agrupador' => $data['Produto']['agrupador']);
			$this->VariacaoProduto->save($variacao_produto, false);
		    }
		    //end variacao produto	
		    //end variacoes
		    //begin atributos
		    $atributo_tipo = $this->AtributoTipo->find('first', array('fields' => array('AtributoTipo.id'), 'conditions' => array('AtributoTipo.codigo_externo' => $row3['tpgrd_id'])));
		    $at = $this->Atributo->find('first', array('recursive' => -1, 'conditions' => array('Atributo.atributo_tipo_id' => $atributo_tipo['AtributoTipo']['id'], 'Atributo.valor' => $row3['ctgrd_label'])));
		    if (!$at) {
			$data34 = '';
			$data34['Atributo']['id'] = null;
			$data34['Atributo']['atributo_tipo_id'] = $atributo_tipo['AtributoTipo']['id'];
			$data34['Atributo']['valor'] = $row3['ctgrd_label'];
			$data34['Atributo']['filtro'] = 1;
			$data34['Atributo']['status'] = 1;

			foreach ($idiomas as $key => $idioma) {
			    $data34['AtributoDescricao'][$key]['language'] = $idioma['Linguagem']['codigo'];
			    $data34['AtributoDescricao'][$key]['valor'] = $row3['ctgrd_label'];
			    $data34['AtributoDescricao'][$key]['label'] = $row3['ctgrd_ref'];
			}

			if ($this->Atributo->saveAll($data34, false)) {
			    $atributo_id = $this->Atributo->id;
			} else {
			    die(debug($this->Atributo->invalidFields()));
			}
		    } else {
			$atributo_id = $at['Atributo']['id'];
		    }

		    //begin atributo produto
		    if ($lastid != 0 && $atributo_id != 0) {
			$atributo_produto = array('atributo_id' => $atributo_id, 'produto_id' => $lastid);
			$this->AtributoProduto->save($atributo_produto, false);
		    }
		    //end atributo produto
		    //begin atributo categoria
		    if ($atributo_id != 0) {
			$atributo_categoria = array('atributo_id' => $atributo_id, 'categoria_id' => $categoria_id['Categoria']['id']);
			$this->CategoriaAtributo->save($atributo_categoria, false);
		    }
		    //end variacao categoria
		    //end atributos
		}
		//die(debug($result32));
		//atributos
		// while($row32 = mysql_fetch_assoc($result32)){
		// $atributo_tipo = $this->AtributoTipo->find('first', array('fields' => array('AtributoTipo.id'), 'conditions' => array('AtributoTipo.codigo_externo' => $row32['tpgrd_id'])));
		// $vt = $this->Atributo->find('first',array('recursive' => -1,'conditions'=>array('Atributo.atributo_tipo_id'=>$atributo_tipo['AtributoTipo']['id'],'Atributo.valor'=> $row32['ctgrd_label'] )));
		// if(!$vt){					
		// $data3 = '';
		// $data34['Atributo']['id'] 				= '';
		// $data34['Atributo']['atributo_tipo_id'] 	= $atributo_tipo['AtributoTipo']['id'];
		// $data34['Atributo']['valor']				= $row32['ctgrd_label'];
		// foreach($idiomas as $key => $idioma){
		// $data34['AtributoDescricao'][$key]['language']		= $idioma['Linguagem']['codigo'];
		// $data34['AtributoDescricao'][$key]['valor'] 		= $row32['ctgrd_label'];
		// }
		// if($this->Atributo->saveAll($data34, false)){
		// $atributo_id = $this->Atributo->id;
		// }else{
		// die(debug($this->Atributo->invalidFields()));
		// }							
		// }else{
		// $atributo_id = $vt['Atributo']['id'];   
		// }
		// begin atributo produto
		// if($lastid != 0){
		// $atributo_produto = array('atributo_id' => $atributo_id, 'produto_id' => $lastid);
		// $this->AtributoProduto->save($atributo_produto, false);
		// }
		// end atributo produto
		// begin atributo categoria
		// $atributo_categoria = array('atributo_id' => $atributo_id, 'categoria_id' => $categoria_id['Categoria']['id']);
		// $this->CategoriaAtributo->save($atributo_categoria, false);
		// end variacao categoria
		// }
		// mysql_close($con33);
		//end save variacoes
		//begin save ProdutoImagem
		$dir = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'uploads' . DS . 'produto_imagem' . DS . 'filename' . DS;
		$remote_dir = "/www/tw5.0/contas/00009_v4/arquivos/workspaces/prms/produto/" . $row['produto_id'] . "/img_g/";

		//lista arquivos
		$contents = ftp_nlist($conn_id, $remote_dir);
		foreach ($contents as $file) {
		    $res = ftp_size($conn_id, $file);
		    if ($res != -1) {
			print($file . '<hr />');

			//$imagem = $file;
			//$imagem = end($contents);				
			$nome_imagem = end(explode("/", $file));
			//seto o destino
			$destino = $dir . $nome_imagem;

			//begin produto imagem
			$this->Produto->ProdutoImagem->Behaviors->detach('MeioUpload');
			$data['ProdutoImagem']['id'] = '';
			$data['ProdutoImagem']['filename'] = $nome_imagem;
			$data['ProdutoImagem']['dir'] = 'uploads/produto_imagem/filename';
			$data['ProdutoImagem']['mimetype'] = 'image/jpeg';
			$data['ProdutoImagem']['filesize'] = '13722';
			$data['ProdutoImagem']['ordem'] = 1;
			$data['ProdutoImagem']['produto_id'] = $lastid;

			//debug($data);						
			//se não existir imagem na pasta ele copia ela e add no banco
			if ($this->Produto->ProdutoImagem->save($data)) {
			    if (!file_exists($dir . $nome_imagem)) {
				//baixo o arquivo se o mesmo já não existir
				if (!ftp_get($conn_id, $destino, $file, FTP_BINARY)) {
				    print('destino: ' . $destino . '<br />');
				    print('imagem: ' . $file . '<br />');
				    echo "Ocorreu um erro\n";
				}
			    }
			}
		    }
		}
		//end save ProdutoImagem
	    } else {
		var_dump($data);
		//die(debug($this->Produto->invalidFields()));
	    }
	    //end save produto
	    //debug($data);
	    print('end <hr />');

	    $cont++;

	    //if($cont == 20) die;
	}

	// fecha a conexão
	ftp_close($conn_id);
    }

    function importa_noticias() {

	///import helper
	App::import("helper", "String");
	$this->String = new StringHelper();

	App::import("Model", "Noticia");
	$this->Noticia = new Noticia();

	App::import("Model", "Video");
	$this->Video = new Video();

	//banco de dados
	mysql_connect("127.0.0.1", "root", "");
	mysql_select_db("fueltech_producao2");
	mysql_query('SET NAMES utf8');

	//ftp		
	$ftp_server = 'ftp.fueltech.com.br'; // FTP do destino
	$ftp_user_name = 'fueltech';
	$ftp_user_pass = 'ftadmin';
	$conn_id = ftp_connect($ftp_server);
	$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
	ftp_pasv($conn_id, true);

	$sql = "SELECT
				  `sgtxt_cdg`,
				  `sgtxt_grpcdg`,
				  `sgtxt_descr`,
				  `sgtxt_descr_label`,
				  `sgtxt_ativo`, 
				  `fueltech_producao2`.`stgs_textos`.`txt_cdg` AS codigo_externo,
				  `fueltech_producao2`.`stgs_textos`.`txt_sgrpcdg` AS categoria_id,
				  `fueltech_producao2`.`stgs_textos`.`txt_descr` AS titulo,
				  `fueltech_producao2`.`stgs_textos`.`txt_txt_resumo` AS resumo,
				  `fueltech_producao2`.`stgs_textos`.`txt_txt_noticia` AS conteudo,
				  `fueltech_producao2`.`stgs_textos`.`txt_status` AS status,
				  `fueltech_producao2`.`stgs_textos`.`txt_galfot_id` AS galeria_id,
				  `fueltech_producao2`.`stgs_textos`.`txt_vid_id` AS video_id,
				  `fueltech_producao2`.`stgs_textos`.`txt_img_resumo` AS thumb
				FROM `fueltech_producao2`.`stgs_sgrptexto` 
				LEFT JOIN `fueltech_producao2`.`stgs_textos` ON `fueltech_producao2`.`stgs_sgrptexto`.`sgtxt_cdg` = `fueltech_producao2`.`stgs_textos`.`txt_sgrpcdg`
				WHERE sgtxt_grpcdg = 2 AND sgtxt_ativo = 1
				ORDER BY txt_cdg ASC";
	$result = mysql_query($sql);
	while ($row = mysql_fetch_assoc($result)) {

	    $status = 1;
	    $destaque_home = 0;
	    if ($row['status'] == 2) {
		$status = 0;
	    }
	    if ($row['status'] == 3) {
		$destaque_home = 1;
	    }

	    if ($row['categoria_id'] == 3) {
		$categoria_id = 1;
	    } elseif ($row['categoria_id'] == 5) {
		$categoria_id = 2;
	    }

	    $data = '';
	    $data['Noticia']['id'] = '';
	    $data['Noticia']['language'] = 'pt';
	    $data['Noticia']['titulo'] = $row['titulo'];
	    $data['Noticia']['descricao'] = $row['resumo'];
	    $data['Noticia']['conteudo'] = $row['conteudo'];
	    $data['Noticia']['destaque_home'] = $destaque_home;
	    $data['Noticia']['status'] = $status;
	    $data['Noticia']['codigo_externo'] = $row['codigo_externo'];
	    $data['Noticia']['noticia_tipo_id'] = $categoria_id;

	    //begin save imagens
	    $dir = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'uploads' . DS . 'noticia' . DS . 'thumb' . DS;
	    $remote_dir = "/www/tw5.0/contas/00009_v4/arquivos/workspaces/stgs/textos/" . $row['codigo_externo'] . "/190x142/";

	    //lista arquivos
	    $contents = ftp_nlist($conn_id, $remote_dir);
	    if (is_array($contents) && $contents > 0) {
		foreach ($contents as $file) {
		    $res = ftp_size($conn_id, $file);
		    if ($res != -1) {
			print($file . '<hr />');

			$nome_imagem = end(explode("/", $file));
			//seto o destino
			$destino = $dir . $nome_imagem;

			if (!file_exists($dir . $nome_imagem)) {
			    if (!ftp_get($conn_id, $destino, $file, FTP_BINARY)) {
				echo "Ocorreu um erro\n";
			    }
			}

			//begin produto imagem
			$this->Noticia->Behaviors->detach('MeioUpload');
			$data['Noticia']['thumb_filename'] = $nome_imagem;
			$data['Noticia']['thumb_dir'] = 'uploads/noticia/thumb';
			$data['Noticia']['thumb_mimetype'] = 'image/gif';
			$data['Noticia']['thumb_filesize'] = '13722';
		    }
		}
	    }

	    //galeria
	    $galeria_id = $this->general_galerias($row['galeria_id'], 2);
	    if ($galeria !== false) {
		$data['Noticia']['galeria_id'] = $galeria_id;
	    }

	    $video = $this->Video->find('first', array('conditions' => array('Video.codigo_externo' => $row['video_id'])));
	    if ($video) {
		$data['Noticia']['video_id'] = $video['Video']['id'];
	    }

	    $this->Noticia->save($data, false);
	    // if($this->Noticia->save($data, false)){
	    // if(!file_exists($dir . $nome_imagem)){
	    //baixo o arquivo se o mesmo já não existir
	    // if (!ftp_get($conn_id, $destino, $file, FTP_BINARY)){
	    // print('destino: '.$destino.'<br />');
	    // print('imagem: '.$file.'<br />');
	    // echo "Ocorreu um erro\n";
	    // }
	    // }
	    // }
	}
    }

    function importa_galerias() {

	///import helper
	App::import("helper", "String");
	$this->String = new StringHelper();

	App::import("Model", "Galeria");
	$this->Galeria = new Galeria();

	App::import("Model", "GaleriaFoto");
	$this->GaleriaFoto = new GaleriaFoto();

	//banco de dados
	$con1 = mysql_connect("127.0.0.1", "root", "");
	$db1 = mysql_select_db("fueltech_producao2", $con1);
	mysql_query('SET NAMES utf8');

	//ftp		
	$ftp_server = 'ftp.fueltech.com.br'; // FTP do destino
	$ftp_user_name = 'fueltech';
	$ftp_user_pass = 'ftadmin';
	$conn_id = ftp_connect($ftp_server);
	$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
	ftp_pasv($conn_id, true);

	$sql = "SELECT * FROM stgs_galeria WHERE gal_sgrp = 4 OR gal_sgrp = 67 ORDER BY gal_id";
	$result = mysql_query($sql, $con1);
	while ($row = mysql_fetch_assoc($result)) {

	    $data = '';
	    $data['Galeria']['id'] = '';
	    $data['Galeria']['galeria_tipo_id'] = 1;
	    $data['Galeria']['nome'] = $row['gal_nome'];
	    $data['Galeria']['descricao'] = $row['gal_descr'];
	    $data['Galeria']['status'] = ($row['gal_ativo']) ? true : false;
	    $data['Galeria']['codigo_externo'] = $row['gal_id'];

	    if ($this->Galeria->save($data, false)) {

		$galeria_id = $this->Galeria->id;

		//banco de dados
		$con2 = mysql_connect("127.0.0.1", "root", "");
		$db2 = mysql_select_db("fueltech_producao2", $con2);
		mysql_query('SET NAMES utf8');

		$sql2 = "SELECT * FROM stgs_galeria_foto WHERE galf_gal_id = " . $row['gal_id'] . "ORDER BY galf_ordem";
		$result2 = mysql_query($sql2, $con2);
		while ($row2 = mysql_fetch_assoc($result2)) {

		    $dir = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'uploads' . DS . 'galeria' . DS . 'filename' . DS;
		    $remote_dir = "/www/tw5.0/contas/00009_v4/arquivos/workspaces/galeria/" . $row['gal_id'] . "/g/" . $row2['galf_arquivo'];

		    //lista arquivos
		    $contents = ftp_nlist($conn_id, $remote_dir);
		    $imagem = end($contents);

		    $nome_imagem = end(explode("/", $imagem));
		    $nome_imagem = $row2['galf_id'] . '_v_' . $nome_imagem;

		    //seto o destino
		    $destino = $dir . $nome_imagem;

		    $this->GaleriaFoto->Behaviors->detach('MeioUpload');
		    $data1 = '';
		    $data1['GaleriaFoto']['id'] = '';
		    $data1['GaleriaFoto']['titulo'] = $row2['galf_descr'];
		    $data1['GaleriaFoto']['filename'] = $nome_imagem;
		    $data1['GaleriaFoto']['dir'] = 'uploads/galeria/filename';
		    $data1['GaleriaFoto']['mimetype'] = 'image/jpeg';
		    $data1['GaleriaFoto']['filesize'] = '13722';
		    $data1['GaleriaFoto']['ordem'] = $row2['galf_ordem'];
		    $data1['GaleriaFoto']['status'] = $row2['galf_ativo'];
		    $data1['GaleriaFoto']['galeria_id'] = $galeria_id;

		    if ($this->GaleriaFoto->save($data1, false)) {
			//baixo o arquivo se o mesmo já não existir
			if (!file_exists($dir . $nome_imagem)) {
			    if (!ftp_get($conn_id, $destino, $imagem, FTP_BINARY)) {
				echo "Ocorreu um erro\n<br />";
			    }
			}
		    }
		}
	    }
	}
    }

    function importa_veiculos() {

	///import helper
	App::import("helper", "String");
	$this->String = new StringHelper();

	App::import("Model", "Galeria");
	$this->Galeria = new Galeria();

	App::import("Model", "GaleriaFoto");
	$this->GaleriaFoto = new GaleriaFoto();

	App::import("Model", "Veiculo");
	$this->Veiculo = new Veiculo();

	//banco de dados
	$con = mysql_connect("127.0.0.1", "root", "");
	$db = mysql_select_db("fueltech_producao2", $con);
	mysql_query('SET NAMES utf8');

	//banco de dados
	$con2 = mysql_connect("127.0.0.1", "root", "");
	$db2 = mysql_select_db("fueltech_producao2", $con2);
	mysql_query('SET NAMES utf8');

	//banco de dados
	$con3 = mysql_connect("127.0.0.1", "root", "");
	$db3 = mysql_select_db("fueltech_producao2", $con3);
	mysql_query('SET NAMES utf8');

	//ftp		
	$ftp_server = 'ftp.fueltech.com.br'; // FTP do destino
	$ftp_user_name = 'fueltech';
	$ftp_user_pass = 'ftadmin';
	$conn_id = ftp_connect($ftp_server);
	$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
	ftp_pasv($conn_id, true);

	$sql = "SELECT * FROM stgv_veiculo ORDER BY vei_id ASC";
	$result = mysql_query($sql, $con);
	while ($row = mysql_fetch_assoc($result)) {

	    $data = '';
	    $data['Veiculo']['id'] = '';
	    $data['Veiculo']['nome'] = $row['vei_descricao'];
	    $data['Veiculo']['descricao'] = $row['vei_descr_resumo'];
	    $data['Veiculo']['status'] = ($row['vei_ativo']) ? true : false;

	    //$galeria = $this->Galeria->find('first', array('recursive' => -1, 'conditions' => array('Galeria.codigo_externo' => $row['vei_gal_img'])));
	    //$sql = "SELECT * FROM stgs_galeria WHERE gal_sgrp = 4 OR gal_sgrp = 67 ORDER BY gal_id";
	    // $sql2 = "SELECT * FROM stgs_galeria WHERE gal_id = ".$row['vei_gal_img'];
	    // $result2 = mysql_query($sql2, $con2);
	    // while($row2 = mysql_fetch_assoc($result2)){
	    // $data2 = '';
	    // $data2['Galeria']['id'] 				= '';
	    // $data2['Galeria']['galeria_tipo_id'] 	= 1;
	    // $data2['Galeria']['nome'] 				= $row2['gal_nome'];
	    // $data2['Galeria']['descricao'] 			= $row2['gal_descr'];
	    // $data2['Galeria']['status']	 			= ($row2['gal_ativo']) ? true : false;
	    // $data2['Galeria']['codigo_externo'] 	= $row2['gal_id'];
	    // if($this->Galeria->save($data2, false)){
	    // $galeria_id = $this->Galeria->id;
	    // $sql3 = "SELECT * FROM stgs_galeria_foto WHERE galf_gal_id = ".$row2['gal_id']." ORDER BY galf_ordem";
	    // $result3 = mysql_query($sql3, $con3);
	    // while($row3 = mysql_fetch_assoc($result3)){
	    // $dir = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'uploads' . DS . 'galeria' . DS . 'filename' . DS;			
	    // $remote_dir = "/www/tw5.0/contas/00009_v4/arquivos/workspaces/galeria/".$row2['gal_id']."/g/".$row3['galf_arquivo'];
	    // lista arquivos
	    // $contents = ftp_nlist($conn_id, $remote_dir);
	    // $imagem = end($contents);
	    // $res = ftp_size($conn_id, $imagem); 
	    // if($res != -1) { 
	    // print('<br /><hr /><hr />');
	    // print($sql2);
	    // print('<br />');
	    // print($sql3);
	    // print('<br />');
	    // print($remote_dir);
	    // print('<br />');
	    // print('contents: <br />');
	    // debug($contents);
	    // print('imagem: <br />');
	    // print($imagem);
	    // print('<br /><hr /><hr />');
	    // $nome_imagem = end(explode("/", $imagem));
	    // $nome_imagem = $row3['galf_id'].'_v_'.$nome_imagem;
	    // seto o destino
	    // $destino = $dir.$nome_imagem;
	    // $this->GaleriaFoto->Behaviors->detach('MeioUpload');
	    // $data3 = '';
	    // $data3['GaleriaFoto']['id']	 			= '';
	    // $data3['GaleriaFoto']['titulo']	 		= $row3['galf_descr'];
	    // $data3['GaleriaFoto']['filename'] 		= $nome_imagem;
	    // $data3['GaleriaFoto']['dir'] 			= 'uploads/galeria/filename';
	    // $data3['GaleriaFoto']['mimetype'] 		= 'image/jpeg';
	    // $data3['GaleriaFoto']['filesize'] 		= '13722';
	    // $data3['GaleriaFoto']['ordem']	 		= $row3['galf_ordem'];
	    // $data3['GaleriaFoto']['status']	 		= $row3['galf_ativo'];
	    // $data3['GaleriaFoto']['galeria_id'] 	= $galeria_id;
	    // if($this->GaleriaFoto->save($data3, false)){
	    // baixo o arquivo se o mesmo já não existir
	    // if(!file_exists($dir . $nome_imagem)){
	    // if (!ftp_get($conn_id, $destino, $imagem, FTP_BINARY)) {
	    // echo "Ocorreu um erro\n<br />";
	    // }
	    // }
	    // }
	    // }
	    // }
	    //	}
	    //}

	    $galeria_id = $this->general_galerias($row['vei_gal_img'], 1);
	    if ($galeria !== false) {
		$data['Veiculo']['galeria_id'] = $galeria_id;
	    }
	    //$data['Veiculo']['galeria_id'] 				= $galeria_id;

	    if ($this->Veiculo->save($data, false)) {
		print('salvou: ' . $data['Veiculo']['nome'] . '<br />');
	    }

	    // die;
	}
    }

    function general_galerias($param_id, $tipo) {

	//banco de dados
	$con9 = mysql_connect("127.0.0.1", "root", "");
	$db9 = mysql_select_db("fueltech_producao2", $con9);
	mysql_query('SET NAMES utf8');

	//banco de dados
	$con10 = mysql_connect("127.0.0.1", "root", "");
	$db10 = mysql_select_db("fueltech_producao2", $con10);
	mysql_query('SET NAMES utf8');

	//ftp		
	$ftp_server = 'ftp.fueltech.com.br'; // FTP do destino
	$ftp_user_name = 'fueltech';
	$ftp_user_pass = 'ftadmin';
	$conn_id = ftp_connect($ftp_server);
	$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
	ftp_pasv($conn_id, true);

	//models
	App::import("Model", "Galeria");
	$this->Galeria = new Galeria();

	App::import("Model", "GaleriaFoto");
	$this->GaleriaFoto = new GaleriaFoto();

	$sql9 = "SELECT * FROM stgs_galeria WHERE gal_id = " . $param_id;
	$result9 = mysql_query($sql9, $con9);

	while ($row9 = mysql_fetch_assoc($result9)) {

	    $data9 = '';
	    $data9['Galeria']['id'] = '';
	    $data9['Galeria']['galeria_tipo_id'] = $tipo;
	    $data9['Galeria']['nome'] = $row9['gal_nome'];
	    $data9['Galeria']['descricao'] = $row9['gal_descr'];
	    $data9['Galeria']['status'] = ($row9['gal_ativo']) ? true : false;
	    $data9['Galeria']['codigo_externo'] = $row9['gal_id'];

	    if ($this->Galeria->save($data9, false)) {

		$galeria_id = $this->Galeria->id;

		$sql10 = "SELECT * FROM stgs_galeria_foto WHERE galf_gal_id = " . $row9['gal_id'] . " ORDER BY galf_ordem";
		$result10 = mysql_query($sql10, $con10);
		while ($row10 = mysql_fetch_assoc($result10)) {

		    $dir = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'uploads' . DS . 'galeria' . DS . 'filename' . DS;
		    $remote_dir = "/www/tw5.0/contas/00009_v4/arquivos/workspaces/galeria/" . $row9['gal_id'] . "/g/" . $row10['galf_arquivo'];

		    //lista arquivos
		    $contents = ftp_nlist($conn_id, $remote_dir);
		    $imagem = end($contents);

		    $res = ftp_size($conn_id, $imagem);
		    if ($res != -1) {
			// print('<br /><hr /><hr />');
			// print($sql2);
			// print('<br />');
			// print($sql3);
			// print('<br />');
			// print($remote_dir);
			// print('<br />');
			// print('contents: <br />');
			// debug($contents);
			// print('imagem: <br />');
			// print($imagem);
			// print('<br /><hr /><hr />');

			$nome_imagem = end(explode("/", $imagem));
			$nome_imagem = $row10['galf_id'] . '_v_' . $nome_imagem;

			//seto o destino
			$destino = $dir . $nome_imagem;

			$this->GaleriaFoto->Behaviors->detach('MeioUpload');
			$data10 = '';
			$data10['GaleriaFoto']['id'] = '';
			$data10['GaleriaFoto']['titulo'] = $row10['galf_descr'];
			$data10['GaleriaFoto']['filename'] = $nome_imagem;
			$data10['GaleriaFoto']['dir'] = 'uploads/galeria/filename';
			$data10['GaleriaFoto']['mimetype'] = 'image/jpeg';
			$data10['GaleriaFoto']['filesize'] = '13722';
			$data10['GaleriaFoto']['ordem'] = $row10['galf_ordem'];
			$data10['GaleriaFoto']['status'] = $row10['galf_ativo'];
			$data10['GaleriaFoto']['galeria_id'] = $galeria_id;

			if ($this->GaleriaFoto->save($data10, false)) {
			    //baixo o arquivo se o mesmo já não existir
			    if (!file_exists($dir . $nome_imagem)) {
				if (!ftp_get($conn_id, $destino, $imagem, FTP_BINARY)) {
				    echo "Ocorreu um erro\n<br />";
				}
			    }
			}
		    }
		}

		return $this->Galeria->id;
	    } else {
		return false;
	    }
	}
    }

    function importa_videos() {

	///import helper
	App::import("helper", "String");
	$this->String = new StringHelper();

	App::import("Model", "Video");
	$this->Video = new Video();

	//banco de dados
	mysql_connect("127.0.0.1", "root", "");
	mysql_select_db("fueltech_producao2");
	mysql_query('SET NAMES utf8');

	//ftp		
	$ftp_server = 'ftp.fueltech.com.br'; // FTP do destino
	$ftp_user_name = 'fueltech';
	$ftp_user_pass = 'ftadmin';
	$conn_id = ftp_connect($ftp_server);
	$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
	ftp_pasv($conn_id, true);

	$sql = "SELECT * FROM stgs_video ORDER BY vid_id ASC";
	$result = mysql_query($sql);
	while ($row = mysql_fetch_assoc($result)) {

	    $data = '';

	    if ($row['vid_cat_id'] == 1) {
		$data['Video']['video_tipo_id'] = 1;
	    } elseif ($row['vid_cat_id'] == 6) {
		$data['Video']['video_tipo_id'] = 2;
	    } elseif ($row['vid_cat_id'] == 7) {
		$data['Video']['video_tipo_id'] = 3;
	    }

	    $data['Video']['id'] = '';
	    $data['Video']['language'] = 'pt';
	    $data['Video']['nome'] = $row['vid_titulo'];
	    $data['Video']['descricao'] = $row['vid_descricao'];
	    $data['Video']['duracao'] = $row['vid_duracao'];
	    $data['Video']['url'] = $row['vid_video'];
	    $data['Video']['video_youtube_id'] = $this->get_youtube_video_id($data['Video']['url']);
	    $data['Video']['status'] = $row['vid_ativo'];
	    $data['Video']['codigo_externo'] = $row['vid_id'];

	    //begin save imagens
	    $dir = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'uploads' . DS . 'videos' . DS . 'thumb' . DS;
	    $remote_dir = "/www/tw5.0/contas/00009_v4/arquivos/workspaces/videos/" . $row['vid_miniatura'];

	    //lista arquivos
	    $contents = ftp_nlist($conn_id, $remote_dir);
	    if (is_array($contents) && $contents > 0) {
		foreach ($contents as $file) {
		    $res = ftp_size($conn_id, $file);
		    if ($res != -1) {
			print($file . '<hr />');
			$nome_imagem = end(explode("/", $file));
			//seto o destino
			$destino = $dir . $nome_imagem;

			if (!file_exists($dir . $nome_imagem)) {
			    if (!ftp_get($conn_id, $destino, $file, FTP_BINARY)) {
				echo "Ocorreu um erro\n";
			    }
			}

			//begin produto imagem
			$this->Video->Behaviors->detach('MeioUpload');
			$data['Video']['thumb_filename'] = $nome_imagem;
			$data['Video']['thumb_dir'] = 'uploads/noticia/thumb';
			$data['Video']['thumb_mimetype'] = 'image/gif';
			$data['Video']['thumb_filesize'] = '13722';
		    }
		}
	    }
	    $this->Video->save($data, false);
	}
    }

    function importa_download_produtos() {
	//banco de dados
	mysql_connect("127.0.0.1", "root", "");
	mysql_select_db("fueltech_producao2");
	mysql_query('SET NAMES utf8');

	App::import("Model", "Download");
	$this->Download = new Download();

	App::import("Model", "Produto");
	$this->Produto = new Produto();

	App::import("Model", "ProdutoDownload");
	$this->ProdutoDownload = new ProdutoDownload();
	$this->ProdutoDownload->primaryKey = '';

	$sql = "SELECT * FROM stgs_download ORDER BY down_id ASC";
	$result = mysql_query($sql);
	while ($row = mysql_fetch_assoc($result)) {

	    $down_id = $row['down_id'];
	    $down_prd_id = $row['down_prd_id'];

	    //busco o download
	    $download = $this->Download->find('first', array('recursive' => -1, 'fields' => array('Download.id'), 'conditions' => array('Download.codigo_externo' => $down_id)));

	    //busco o produto
	    $produto = $this->Produto->find('first', array('recursive' => -1, 'fields' => array('Produto.id'), 'conditions' => array('Produto.codigo_externo' => $down_prd_id)));

	    if ($produto['Produto']['id'] != 0 && $download['Download']['id'] != 0) {
		$produto_download = array('produto_id' => $produto['Produto']['id'], 'download_id' => $download['Download']['id']);
		$this->ProdutoDownload->save($produto_download, false);

		$produtos = $this->Produto->find('all', array('recursive' => -1, 'fields' => array('Produto.id'), 'conditions' => array('Produto.parent_id' => $produto['Produto']['id'])));

		foreach ($produtos as $prod) {

		    if ($prod['Produto']['id'] != 0 && $download['Download']['id'] != 0) {
			$produto_download = array('produto_id' => $prod['Produto']['id'], 'download_id' => $download['Download']['id']);
			$this->ProdutoDownload->save($produto_download, false);
		    }
		}
	    }
	}
    }

    function importa_downloads() {

	///import helper
	App::import("helper", "String");
	$this->String = new StringHelper();

	App::import("Model", "Download");
	$this->Download = new Download();

	//banco de dados
	mysql_connect("127.0.0.1", "root", "");
	mysql_select_db("fueltech_producao2");
	mysql_query('SET NAMES utf8');

	//ftp		
	$ftp_server = 'ftp.fueltech.com.br'; // FTP do destino
	$ftp_user_name = 'fueltech';
	$ftp_user_pass = 'ftadmin';
	$conn_id = ftp_connect($ftp_server);
	$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
	ftp_pasv($conn_id, true);

	$sql = "SELECT * FROM stgs_download ORDER BY down_tipo_id ASC, down_id ASC";
	$result = mysql_query($sql);
	while ($row = mysql_fetch_assoc($result)) {

	    $data = '';

	    /*
	      1 - manual em portugues
	      3 - manual em espanhol
	      4 - material de divulgação
	      5 - software
	      7 - manuel em ingles
	     */
	    if ($row['down_tipo_id'] == 1) {
		$data['Download']['download_tipo_id'] = 2;
	    } elseif ($row['down_tipo_id'] == 3) {
		$data['Download']['download_tipo_id'] = 6;
	    } elseif ($row['down_tipo_id'] == 4) {
		$data['Download']['download_tipo_id'] = 3;
	    } elseif ($row['down_tipo_id'] == 5) {
		$data['Download']['download_tipo_id'] = 1;
	    } elseif ($row['down_tipo_id'] == 7) {
		$data['Download']['download_tipo_id'] = 5;
	    }

	    $data['Download']['id'] = '';
	    $data['Download']['language'] = 'pt';
	    $data['Download']['nome'] = $row['down_titulo'];
	    $data['Download']['descricao'] = $row['down_descr'];
	    $data['Download']['versao'] = $row['down_versao'];
	    $data['Download']['ordem'] = $row['down_ordem'];
	    $data['Download']['destaque'] = $row['down_destaque'];
	    $data['Download']['status'] = $row['down_ativo'];
	    $data['Download']['codigo_externo'] = $row['down_id'];

	    //begin save imagens
	    $dir = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'uploads' . DS . 'download' . DS . 'filename' . DS;
	    $remote_dir = "/www/tw5.0/contas/00009_v4/arquivos/workspaces/download/1/" . $row['down_arquivo'];

	    //lista arquivos
	    $contents = ftp_nlist($conn_id, $remote_dir);
	    if (is_array($contents) && $contents > 0) {
		foreach ($contents as $file) {
		    $res = ftp_size($conn_id, $file);
		    if ($res != -1) {
			print($file . '<hr />');
			$nome_imagem = end(explode("/", $file));
			//seto o destino
			$destino = $dir . $nome_imagem;

			if (!file_exists($dir . $nome_imagem)) {
			    if (!ftp_get($conn_id, $destino, $file, FTP_BINARY)) {
				echo "Ocorreu um erro\n";
			    }
			}

			$this->Download->Behaviors->detach('MeioUpload');
			$data['Download']['filename'] = $nome_imagem;
			$data['Download']['dir'] = 'uploads/download/filename';
			$data['Download']['mimetype'] = 'image/pdf';
			$data['Download']['filesize'] = $res;

			//begin produto imagem
			// $this->Video->Behaviors->detach('MeioUpload');
			// $data['Download']['thumb_filename'] 		= $nome_imagem;
			// $data['Download']['thumb_dir']	 			= 'uploads/noticia/thumb';
			// $data['Download']['thumb_mimetype'] 		= 'image/gif';
			// $data['Download']['thumb_filesize'] 		= '13722';
		    }
		}
	    }
	    $this->Download->save($data, false);
	}
    }

    function get_youtube_video_id($url) {
	$video_id = false;
	$url = parse_url($url);
	if (strcasecmp($url['host'], 'youtu.be') === 0) {
	    #### (dontcare)://youtu.be/<video id>
	    $video_id = substr($url['path'], 1);
	} elseif (strcasecmp($url['host'], 'www.youtube.com') === 0) {
	    if (isset($url['query'])) {
		parse_str($url['query'], $url['query']);
		if (isset($url['query']['v'])) {
		    #### (dontcare)://www.youtube.com/(dontcare)?v=<video id>
		    $video_id = $url['query']['v'];
		}
	    }
	    if ($video_id == false) {
		$url['path'] = explode('/', substr($url['path'], 1));
		if (in_array($url['path'][0], array('e', 'embed', 'v'))) {
		    #### (dontcare)://www.youtube.com/(whitelist)/<video id>
		    $video_id = $url['path'][1];
		}
	    }
	}
	return $video_id;
    }
	
	function estoque_alocado() {
		
		Configure::write('debug', 0);
		
		App::import('Model', 'Produto');
		$this->Produto = new Produto();
		
		App::import('Model', 'ProdutoKit');
		$this->ProdutoKit = new ProdutoKit();
			
		$arrAlocado = $this->Produto->find('all',array('conditions'=>array('Produto.quantidade_alocada > 0')));
		
		//debug($arrAlocado);die;
		
		if($arrAlocado){
			foreach($arrAlocado as $ddAlocado){
				//echo $ddAlocado['Produto']['quantidade_alocada'].'<br/>';
				
				$query = "SELECT 
					SUM(pi.quantidade) alocado

					FROM pedido_itens pi

					LEFT JOIN pedidos p
					ON p.id = pi.pedido_id

					LEFT JOIN pedido_status ps
					ON ps.id = p.pedido_status_id

					WHERE sku LIKE '".$ddAlocado['Produto']['sku']."' 
					AND p.pedido_status_id IN (1,2)
					";
				$retorno = $this->Produto->query($query);
				$alocado = $retorno[0][0];
				
				echo 'Produto '.$ddAlocado['Produto']['id'].': alocado: '.$alocado['alocado'].'<br/>';
				
				$this->Produto->saveField('quantidade_alocada',$alocado['alocado']);
				
				$save = array(
						'id' => $ddAlocado['Produto']['id'],
						'quantidade' => $ddAlocado['Produto']['quantidade'],
						'quantidade_alocada' => $alocado['alocado'],
						'quantidade_disponivel' => ($ddAlocado['Produto']['quantidade'] - $alocado['alocado']),
				);
				
				if ($ddAlocado['Produto']['status'] != 0 && $ddAlocado['Produto']['quantidade'] > 0) {
					$this->Produto->saveField('status',1);
				}
				
				if($this->Produto->save($save, array('validate' => false, 'callbacks' => false))){
					echo '<b style="color:green">Atualizado com sucesso.</b></p><br/>';
				}else{
					echo '<b style="color:red">Erro ao atualizar.</b></p><br/>';
				}
				
				if($ddAlocado['Produto']['quantidade'] > 0){
						$this->ProdutoKit->ativa_kit_com_estoque($ddAlocado['Produto']['id']);
				}else{
						$this->ProdutoKit->desativa_kit_sem_estoque($ddAlocado['Produto']['id']);
				}
				
			}
		}
		
	}

    function estoque() {
        
        
            //$import_id = $this->start('estoque');
            echo "<h4>Atualizando estoque: " . date('d/m/Y H:i:s') . "</h4>";

            App::import('Model', 'Produto');
            $this->Produto = new Produto();
            $this->Produto->recursive = -1;

            App::import('Model', 'ProdutoKit');
            $this->ProdutoKit = new ProdutoKit();

            $handle = fopen($this->importacao_dir .  'estoque' . DS . "produtos.csv", "r");

            //echo 'chegou 1';
            
            
            if ($handle) {
                
                    //echo 'chegou 2';
                
                    //$html = '<table border="1>';
                    
                    //echo '<tr><th>Código</th><th>Estoque</th><th>Status</th></tr>';
                
                    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                        
                        
                            //echo 'chegou 3';
                        
                            $codigo_externo = (int) $data[0];
                            $quantidade = (int) $data[1];
                            
                            echo '<p style="float:left; width:100%; height: 20px; padding: 0; margin: 0;">Produto: #'.$codigo_externo.' - Qtde'.$quantidade.' '; 
                            //echo '<th></th></tr>';
                            
                            if (!empty($codigo_externo)) {

                                    $db = $this->Produto->find('first', array('fields' => array('Produto.id', 'Produto.quantidade', 'Produto.quantidade_disponivel', 'Produto.quantidade_alocada', 'Produto.status'), 'recursive' => -1, 'conditions' => array('Produto.codigo_erp' => $codigo_externo)));

                                    if (isset($db) || null != $db || "" != $db) {
									
											$query = "SELECT 
                                                        SUM(pi.quantidade) alocado

                                                        FROM pedido_itens pi

                                                        LEFT JOIN pedidos p
                                                        ON p.id = pi.pedido_id

                                                        LEFT JOIN pedido_status ps
                                                        ON ps.id = p.pedido_status_id

                                                        WHERE sku LIKE '".$db['Produto']['sku']."' 
                                                        AND p.pedido_status_id IN (1,2)
                                                        ";
                                            $retorno = $this->Produto->query($query);
                                            $alocado = $retorno[0][0];
                                            
											$this->Produto->saveField('quantidade_alocada',$alocado['alocado']);

                                            $save = array(
                                                    'id' => $db['Produto']['id'],
                                                    'quantidade' => $quantidade,
                                                    'quantidade_disponivel' => ($quantidade - $alocado['alocado']),
                                            );
											
											if ($db['Produto']['status'] != 0 && $quantidade > 0) {
												$this->Produto->saveField('status',1);
											}
                                            
                                            if(is_numeric($db['Produto']['id'])){
                                                
                                                echo '<span style="color:green;">Localizou id: #'.$db['Produto']['id'].' alocado: '.$alocado['alocado'].'</span> ';
                                                
                                                if ($db['Produto']['status'] != 0 && $quantidade > 0) {
                                                    $save['status'] = 1;
                                                }
                                                
                                                if($this->Produto->save($save, array('validate' => false, 'callbacks' => false))){
                                                    echo '<b>Atualizado com sucesso.</b></p><br/>';
                                                }else{
                                                    echo '<b style="color:red">Erro ao atualizar.</b></p><br/>';
                                                }

                                                if($quantidade > 0){
                                                        $this->ProdutoKit->ativa_kit_com_estoque($db['Produto']['id']);
                                                }else{
                                                        $this->ProdutoKit->desativa_kit_sem_estoque($db['Produto']['id']);
                                                }
                                                
                                            }else{
                                                echo '<span style="color:orange;">Id inválido: #'.$db['Produto']['id'].'</span> </p><br/>';
                                            }

                                            // [FT-151] Se o produto possuir quantidade maior que 0, ativa o produto
                                            // FT-333 [MELHORIA] Atualização do status do produto
                                            

                                            
                                    }else{
                                        echo '<span style="color:orange;">Não cadastrado</span> </p><br/>';
                                    }
                            }
                            
                    }
                    
                    //$html .= '</table>';
                    
            }else{
                
                echo "<h6>Arquivo não encontrado: " . date('d/m/Y H:i:s') . "</h6>";
                
            }

            //echo $html;
            
            $this->end($import_id, "fim");
            echo "<h4>Estoque atualizado: " . date('d/m/Y H:i:s') . "</h4>";
            //die;

            if (copy($this->importacao_dir  . 'estoque' . DS . 'produtos.csv', $this->importacao_dir . DS . 'estoque' . DS . 'importados' . DS . 'produtos-' . date('Ymd_His') . '.csv')) {
        unlink($this->importacao_dir  . 'estoque' . DS . 'produtos.csv');
            }
            die('terminou');
    }

    function atualiza_preco() {
        $import_id = $this->start('preco');
        echo "Comecei a rodar às " . date('d/m/Y H:i:s');
    
        App::import('Model', 'Produto');
        $this->Produto = new Produto();
        $this->Produto->recursive = -1;
		
		App::import('Model', 'ProdutoKit');
        $this->ProdutoKit = new ProdutoKit();
        //$this->ProdutoKit->recursive = -1;
		
        
		$handle = fopen($this->importacao_dir. 'preco' . DS . "precos_produtos.csv", "r");  
        $linhas_lidas=0;
        $atualizados=0;
        if ($handle) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
				$codigo_externo = (int) $data[0];
                $preco = $data[1];
                $preco = str_replace(',','.',str_replace('.','',$preco));
		
                if (!empty($codigo_externo)) {
                    $db = $this->Produto->find('first', array('fields' => array('Produto.id', 'Produto.preco'), 'recursive' => -1, 'conditions' => array('Produto.codigo_erp' => $codigo_externo)));                
					
                    if ($db) {
                        $this->Produto->Query("UPDATE produtos SET preco = {$preco} WHERE id={$db['Produto']['id']} ");
                        $atualizados++;             
                    }
					
					//FT-338
					//atualiza o precço dos itens relacinados na tabela produtos_kits
					$this->ProdutoKit->atualiza_preco_by_id($db['Produto']['id'], $preco);
					
                }
            $linhas_lidas++;            
            }
        }

        $this->end($import_id, "fim");
        echo "<br>Terminou de rodar às " . date('d/m/Y H:i:s') . " e foram atualizados ".$atualizados . " de ". $linhas_lidas ." produtos listados.";

        if (copy($this->importacao_dir. 'preco' . DS . 'precos_produtos.csv', $this->importacao_dir . 'preco' . DS . 'importados' . DS . 'precos_produtos-' . date('Ymd_His') . '.csv')) {
            unlink($this->importacao_dir. 'preco' . DS . 'precos_produtos.csv');
        }
    }      
	
	function atualiza_peso_produtos(){
		App::import("helper", "String");
        $this->String = new StringHelper();
      
        App::import('Model', 'Produto');
        $this->Produto = new Produto();
		
		$caminho_root = WWW_ROOT . 'importacao' . DS;
		$arquivo = $caminho_root.'PesoProdutosFT.csv';
		
		$handle = fopen($arquivo, "r");
		
		$first = true;
		$cont = 0;
		while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
		
			$codigo = $data[0];
			$peso 	= $data[3];
		
			if($first == true){
				$first = false;
				continue;
			}
			
			$codigo = (int) substr($codigo, -4);
			
			$peso = str_replace(',','.',str_replace(',000','',$peso));
			
			if($peso >= 0.10){
				$peso = (int)($peso*1000);
				$peso = str_pad(substr($peso,0,-3),1,0).'.'.str_pad(substr($peso,-3),3,0).'0';
			}else{
				$peso = number_format($peso, 4, '.', '');
			}
			
			if($peso != 0){
				$peso_ramos = $peso+0.2000;
				$peso_ramos = number_format($peso_ramos, 4, '.', '');
			
				//$sql = "UPDATE produtos SET peso = '".$peso."', peso_ramos = '".$peso_ramos."' WHERE codigo_erp = ".$codigo.';';
				$sql = "UPDATE produtos SET peso = '".$peso."' WHERE codigo_erp = ".$codigo.';';
				print($sql.'<br />');
				$cont++;
				//$this->Produto->updateAll(array('Produto.peso' => $peso), array('Produto.cod_erp' => $codigo));
			}
		}
		print('total: '.$cont);
		
	}
}