<?php

class DinamizeIntegracaoComponent extends Object {	
	
	protected $URL 		= 'http://cache.mail2easy.com.br/integracao';
	protected $CON_ID 	= '1666.f3844c2083cb89b74a73e4eed86e74fe';
    protected $DESTINO 	= 'http://www.dinamize.com';
    protected $REFERER 	= '';
    protected $GRUPOS 	= '';
    protected $SMT_MAIL = '';
    protected $SMT_NOME = '';
    protected $SMT_ORIGEM 	= '';
    protected $SMT_RECEBER 	= '';
    protected $data 		= Array();	
	
    public function _construct()
    {
        $this->REFERER = $_SERVER['HTTP_REFERER'];
		$this->SMT_RECEBER = '1';
    }
	
    public function setGroup($grupo_id) {
        $this->GRUPOS = $grupo_id;
    }
	
	public function setReceber($receber) {
        $this->SMT_RECEBER = $receber;
    }

    public function setVar($var,$value=FALSE) {
    	if(is_array($var)) {
    		foreach($var as $k=>$v) $this->{$k} = $v;
    	} else 
        	$this->{$var} 	= $value;
    }

    public function register() {
        $this->data = array(
            'CON_ID' 			=> $this->CON_ID,
            'DESTINO' 			=> $this->DESTINO,
            'GRUPOS_CADASTRAR' 	=> $this->GRUPOS,
			'SMT_RECEBER' 		=> $this->SMT_RECEBER,
        );
        
        $_others = 'SMT_nome,SMT_sexo,SMT_email,SMT_endereco,SMT_bairro,SMT_cidade,SMT_cep,SMT_estado,SMT_telefone_residencial,SMT_telefone_celular,SMT_organizacao,SMT_cargo,SMT_seto,SMT_telefone_comercial,SMT_observacoes,SMT_ano_nascimento,SMT_vendedor,SMT_origem,SMT_ddd,SMT_ddi,SMT_descricao,SMT_nome_fantasia,SMT_pais,SMT_tipo_pf_ou_pj,SMT_time_de_futebol,SMT_cnpj,SMT_atividade,SMT_n_de_funcionarios,SMT_departament,SMT_rua,SMT_n,SMT_complemento,SMT_fax,SMT_rg,SMT_cpf,SMT_site,SMT_e_mail_mkt_semanal,SMT_e_mail_mkt_quinzenal,SMT_e_mail_mkt_mensal,SMT_tabloide_impresso,SMT_tabloide_virtual,SMT_ramal,SMT_razao_social,SMT_contato,SMT_segmento,SMT_mensagem,SMT_aniversario';
        
        $others = explode(',',$_others);
        
        foreach($others as $k):
        	if(isset($this->{$k}) && trim($this->{$k}) != '')
        		$this->data[$k] = $this->{$k};
        endforeach;
		
        $this->PostRequest();
    }

    private function PostRequest() {
        // convert variables array to string:
        $data = array();
        while (list($n, $v) = each($this->data)) {
            $data[] = "$n=$v";
        }
       $data = implode('&', $data);
	 
        // format --> test1=a&test2=b etc.
        // parse the given URL
        $http = parse_url($this->URL);

        if ($http['scheme'] != "http") {
            die('te seOnly HTTP request are supported !');
        }

        // extract host and path:
        $host = $http['host'];
        $path = $http['path'];

        // open a socket connection on port 80
        $fp = fsockopen($host, 80);

        // send the request headers:
        fputs($fp, "POST $path HTTP/1.1\r\n");
        fputs($fp, "Host: $host\r\n");
        fputs($fp, "Referer: $this->REFERER\r\n");
        fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
        fputs($fp, "Content-length: " . strlen($data) . "\r\n");
        fputs($fp, "Connection: close\r\n\r\n");
        fputs($fp, $data);

        $result = '';
        while (!feof($fp)) {
            // receive the results of the request
            $result .= fgets($fp, 128);
        }

        // close the socket connection:
        fclose($fp);

        // split the result header from the content
        $result = explode("\r\n\r\n", $result, 2);

        $header = isset($result[0]) ? $result[0] : '';
        $content = isset($result[1]) ? $result[1] : '';

        // return as array:
        return array($header, $content);
    }
}