<?php

class FilterComponent extends Object
{
    private $controller = null;
    private $filters = array();
    private $storedData = array();
    private $filterKey = "Filter";
    private $conditions = array();

    /**
     * @author Luan Garcia
     * @param object $controller
     * @constructor
     * @classDescription
     * Classe que grava o POST, na sessao.
     * Utilizada para realizar filtros em tabelas.
     */
    public function startup(&$controller)
    {
        $this->controller = &$controller;
        $this->filters = &$_SESSION['filter'][$this->controller->name][$this->controller->action];
        $this->storedData = &$_SESSION['storedData'][$this->controller->name][$this->controller->action];

        if(empty($this->filters)) $this->filters = array();
        if(empty($this->storedData)) $this->storedData = array();
    }

    /**
     * @author Luan Garcia
     * @param string $filterKey
     *
     * Utilizada para informar em qual chave
     * da variavel estao os filtros.
     * Por padrao fica na chave filter.
     */
    public function setFilterKey($filterKey)
    {
        $this->filterKey = $filterKey;
    }

    /**
     * @author Luan Garcia
     * @param array $conditions
     */
    public function setConditions($conditions)
    {
        if(!is_array($conditions)) return;

        $this->conditions = $conditions;
    }

    /**
     * @author Luan Garcia
     * Verifica se ha novas regras, ou se foram retiradas regras
     */
    public function check($escape = false)
    {
        //funcao que testa e se necessario limpa os filtros
        $this->testAndClear();

        //senao havia regras novas retorna
        if(empty($this->controller->data)) return;

        //seta as novas regras
        $this->setFilters($escape);
    }

    /**
     * @author Luan Garcia
     * @param boolean $escape se eh para utilizar addslashes, informacao vem da funcao check
     * @return
     */
    private function setFilters($escape=true)
    {
        //facilita o acesso
        $data = &$this->controller->data[$this->filterKey];

        //limpa os filtros anteriores
        $this->filters = array();

        //esvazia os dados salvos
        $this->storedData = array();

        //se nao ha regras ou nao ha dados nao faz nada
        if(empty($this->conditions) || empty($data)) return;

        foreach($this->conditions as $ruleName => $condition)
        {
            //se for array eh um OR
            if(is_array($condition))
                $condition = "(" . implode(" OR ", $condition) . ")";

            //nao foi postada nenhuma regra para este campo
            if(empty( $this->controller->data[$this->filterKey][$ruleName] ) &&  !is_numeric($this->controller->data[$this->filterKey][$ruleName])) continue;

            //grava o valor da variavel
            $this->storeData($ruleName, $data[$ruleName]);

            //verficando a necessidade de escapar
            $value = $escape === true? addslashes($data[$ruleName]) : $data[$ruleName];
            //substitui o VALUE na condicao SQL pelo conteudo do data
            $formatedCondition = str_replace("{%value%}", $value, $condition);
            $this->filters[] = $formatedCondition;
        }
    }

    /**
     * @author Luan Garcia
     * @param mixed $add [optional] Parametros a serem adicionados nas conditions sempre
     * @return os filtros gerados
     */
    public function getFilters($add = null)
    {
        if(empty($add)) return $this->filters;

        $filters = $this->filters;
        if(is_array($add))
        {
            foreach($add as $k => $condition)
            {
                if(!is_numeric($k))
                {
                    $condition = "{$k} = '{$condition}'";
                }
                array_push($filters, $condition);
            }
        }else
        {
            array_push($filters, $add);
        }

        return $filters;
    }

    /**
     * @author Luan Garcia
     * @param string $variable nome da variavel
     * @param string $value valor
     * @return
     */
    private function storeData($variable, $value)
    {
        $this->storedData[$variable] = $value;
    }

    /**
     * @author Luan Garcia
     * @return variables to view
     */
    public function setDataToView()
    {
        $this->controller->data[$this->filterKey] = array();

        foreach($this->storedData as $variable => $value)
        {
            if(empty($value) && !is_numeric($value)) continue;

            $this->controller->data[$this->filterKey][$variable] = $value;
        }
    }

    /**
     * Limpa os filtros da sessao se o parametro nomeado
     * filter:clear foi passado na url
     *
     * @author Luan Garcia
     */
    public function testAndClear( $clear = false)
    {
        if( (isset($this->controller->params['named']['filter']) && $this->controller->params['named']['filter'] == "clear") || $clear == true )
        {
            $this->filters = array();
            $this->storedData = array();
        }
    }

    /**
     * Retorna um array caso a não seja fornecido o argumento variable
     * ou o valor se fornecido
     *
     * @author Luan Garcia
     * @param string $variable
     * @return mixed
     */
    public function getStoredData($variable = null)
    {
        if(empty($key))
        {
            return $this->storedData;
        }else
        {
            return isset($this->storedData[$variable])? $this->storedData[$variable] : null;
        }
    }
}
?>