<?php

/**
 * Classe que retorna o endereço pelo CEP e o Frete Do cliente baseado na regras de frete do BCO
 * @Author Luan Garcia <luan.garcia@gmail.com>
 * */
Class FreteComponent extends Object {
	public $jundiai_desconto = 15;
    private $url = 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?';
	public $tipos = array('PAC (Correios)'=>41106,'SEDEX' => 40010,'E-Sedex (Correios)' => 81019);
	public $correios = array('altura_padrao'=>2,'largura_padrao'=>11,'comprimento_padrao'=>16,'coeficiente_volume'=>6.000,'altura_max'=>90,'altura_min'=>2,'comprimento_max'=>90,'comprimento_min'=>16,'largura_max'=>90,'largura_min'=>11,'sum_max'=>160,'check_dimensions'=>true);
	
    public $campos = array(
		"nCdEmpresa" => '', //codigo da empresa se tiver afiliacao com os correios
        "sDsSenha" => '', //senha da afiliacao,
        "nCdServico" => NULL, //servico sedex,sedex10,pac
        "sCepOrigem" => '93265150', //origem do frete
        "sCepDestin" => NULL, //destino do pedido
        "nVlPeso" => NULL, //peso do pedido 0.3 para 300 gramas max 30kg
        "nCdFormato" => 1, //formato do pacote, caixa,envelope etc,
        "nVlComprimento" => 0, //comprimento do pacote,
        "nVlAltura" => 0, //altura do pacote,
        "nVlLargura" => 0, //largura do pacote,
        "nVlDiametro" => 0, //diametro do pacote,
        "sCdMaoPropria" => "N", //entregar somente em mao propria
        "nVlValorDeclarado" => 0, //declarar valor do pedido
        "sCdAvisoRecebimento" => "N", //aviso de recebimento,
        "StrRetorno" => "XML"
    );
	
    /**
     * Retorna o valor e o prazo dos serviços dos correios (PAC,SEDEX e SEDEX10)
	* */
   public function correios($cep, $codigo, $carrinho) {
	
        App::import("Xml");
        $cep = preg_replace("/[^0-9]+/", "", $cep);
        //se exceder o peso colocar o peso max de 30 kg
		$pesoCubicoTotal = 0;
        $peso = 0;
		$altura = 0;
		$comprimento = 0;
		$largura = 0;
        $preco = 0;
			
        foreach($carrinho['itens'] as $_product){
			
			unset($_item);
			
            $itemAltura= 0;
            $itemLargura = 0;
            $itemComprimento = 0;

            if($_product['altura'] == '' || (int)$_product['altura'] == 0 || (int)$_product['altura'] < $this->correios['altura_padrao'])
                $_item[] = $this->correios['altura_padrao'];
            else
                $_item[] = $_product['altura'];

			 if($_product['largura'] == '' || (int)$_product['largura'] == 0 || (int)$_product['largura'] < $this->correios['largura_padrao'])
                $_item[] = $this->correios['largura_padrao'];
            else
                $_item[] = $_product['largura'];

			if($_product['profundidade'] == '' || (int)$_product['profundidade'] == 0 || (int)$_product['profundidade'] < $this->correios['comprimento_padrao'])
                $_item[] = $this->correios['comprimento_padrao'];
            else
                $_item[] = $_product['profundidade'];
				sort($_item);
                $itemAltura 	 	= $_item[0];
                $itemLargura 	 	= $_item[1];
                $itemComprimento 	= $_item[2];
                //pega o ultimo produto maior
                $altura 			= $itemAltura 		> $altura 		? $itemAltura : 	 $altura;
                $largura 			= $itemLargura 		> $largura 		? $itemLargura : 	 $largura;
                $comprimento 		= $itemComprimento 	> $comprimento 	? $itemComprimento : $comprimento;
				
            if($this->correios['check_dimensions']==true){
                if($itemAltura > $this->correios['altura_max']|| $itemAltura < $this->correios['altura_min']|| $itemLargura > $this->correios['largura_max']|| $itemLargura < $this->correios['largura_min']|| $itemComprimento > $this->correios['comprimento_max']|| $itemComprimento < $this->correios['comprimento_min']|| ($itemAltura+$itemLargura+$itemComprimento) > $this->correios['sum_max']){
                    return false;
                }
            }          

            $preco += $_product['preco_promocao'] > 0 ? $_product['preco_promocao'] : $_product['preco'];

            $pesoCubicoTotal += (($itemAltura*$itemLargura*$itemComprimento)*$_product['quantidade'])/$this->correios['coeficiente_volume'];
            $peso += $_product['peso']*$_product['quantidade'];
        }



		$this->campos['nVlPeso'] =  $peso;
        $this->campos['sCepDestino'] = $cep;
		$this->campos['nVlComprimento'] = ceil($comprimento);
        $this->campos['nVlAltura'] = ceil($altura);
        $this->campos['nVlLargura'] = ceil($largura);
        $this->campos['nVlValorDeclarado'] = ceil($preco);
	
		//$pesoCubicoTotal = (($altura*$largura*$comprimento)*1)/$this->correios['coeficiente_volume'];
		//$pesoCubicoTotal = number_format($pesoCubicoTotal, 2, '.', '');
		
		/*
         * Monta o valor conforme o tipo de frete
         */
		$this->campos['nCdServico'] = $codigo;
		$args = http_build_query($this->campos);
		$retorno = Set::reverse(new Xml(file_get_contents($this->url . $args)));          
		
        return $retorno;
	}
	
	
   /**
     * Retorna o valor e o prazo dos serviços dos correios (PAC,SEDEX e SEDEX10)
     * @param String $cep cep do destino
     * @param Int $peso peso da compra
     * @return Array
     * */
    public function consultarFretesDisponiveis($cep,$carrinho) {

        $cubagem_max = 0;
        $largura_max = 0;
        $altura_max = 0;
        $profundidade_max = 0;
                $peso                           = 0;
                $peso_ramos             = 0;
                $pedidoTotal            = 0;

        foreach ($carrinho['itens'] as $k => $i) {

            $cubagem_max                += $i['profundidade'];
            $cubagem_max                += $i['largura'];
            $cubagem_max                += $i['quantidade'] * $i['altura'];
                        $profundidade_max       += $i['profundidade'];
                        $altura_max             += $i['quantidade'] * $i['altura'];
                        $largura_max            += $i['largura'];
                        /**/
            $peso                               += $i['quantidade'] * $i['peso'];
            $peso_ramos                 += $i['quantidade'] * $i['peso_ramos'];
            $pedidoTotal                += $i['quantidade'] * ($i['preco_promocao']>0?$i['preco_promocao']:$i['preco']);
        }

                App::import("model", "FreteTipo");
        $this->FreteTipo = new FreteTipo();
        $cep = preg_replace("/[^0-9]+/", "", $cep);
        $conditions = array(
                'OR'=>array(
                                        'AND'=>array(
                                                'FreteTipo.status'=>true,
                                                'FreteTipo.altura_max >'=>$altura_max,
                                                'FreteTipo.largura_max >'=>$largura_max,
                                                'FreteTipo.profundidade_max >'=>$profundidade_max,
                                                'FreteTipo.cubagem_max >'=>$cubagem_max,
                                        ),array(
                                        'AND'=>array(
														'FreteTipo.status'=>true,
                                                        'FreteTipo.altura_max'=>0,
                                                        'FreteTipo.largura_max'=>0,
                                                        'FreteTipo.profundidade_max'=>0,
                                                        'FreteTipo.cubagem_max'=>0
                                        )
                                )
                        )
                );

        if (count($carrinho['itens'])<=0)
            return 0;

                $consulta = $this->FreteTipo->find('all', array('contain'=>array('Frete'=>array('limit'=>1,'order'=>'peso DESC','conditions'=>array(
                'AND'=>array('Frete.peso <='=>$peso_ramos,'Frete.frete_tipo_id !='=>3),
                'Frete.cep_inicial <='=>$cep,'Frete.cep_final >='=>$cep))),'conditions' => $conditions));
                //remove o tipo de frete que não possui um frete válido, para não exibir um meio de entrega vazio.
                foreach($consulta as $chave=>$valor){
                        if(!$valor['Frete']){
                                unset($consulta[$chave]);
                        }
                }
                $consulta = array_values($consulta);
                foreach($consulta as &$con){
                        //se for ramos ou jundiai
                        if(in_array($con['FreteTipo']['id'],array(1,2))){
                                $con = $this->calculaTransportadora($con,$peso_ramos,$pedidoTotal);
                        }
                }
                App::import("helper", "String");
                $this->String = new StringHelper();
                $a = array();
                foreach($this->tipos as $nome=>$codigo){
                        $correios = $this->correios($cep,$codigo, $carrinho);
                        $val = $this->String->moedaToBco($correios['Servicos']['CServico']['Valor']);
                                if($val>0){
                                        $consulta[] =  array(
                                                        'FreteTipo' => array(
                                                        'id' => 3,
                                                        'status' => 1,
                                                        'created' => null ,
                                                        'modified' => null ,
                                                        'nome' => $nome,
                                                        'descricao' => 'Correios',
                                                        'altura_max' => $altura_max,
                                                        'largura_max' => $largura_max,
                                                        'profundidade_max' => $profundidade_max,
                                                        'cubagem_max' => $cubagem_max
                                                ),
                                                'Frete' => array(
                                                        0 => array(
                                                                        'id' => $codigo,
                                                                        'status' => '1',
                                                                        'created' => null,
                                                                        'modified' => null,
                                                                        'cep_inicial' => $cep,
                                                                        'cep_final' => $cep,
                                                                        'peso' => $peso,
                                                                        'preco' => $this->String->moedaToBco($correios['Servicos']['CServico']['Valor']),
                                                                        'descricao' =>'Correios',
                                                                        'prazo' => $correios['Servicos']['CServico']['PrazoEntrega'],
                                                                        'frete_tipo_id' => '2',
                                                                        'apartir_de' => '0.00',
                                                                        'adicional' => '0.00',
                                                                )));
                                }
                }

                //zera o tempo de producao adicional dos produtos
                $tempo_producao = 0;
                foreach($carrinho['itens'] as $item){
                        //se um produto tiver um tempo de producao coloca o maior valor na variavel
                        if($item['tempo_producao']>0 && $item['tempo_producao'] > $tempo_producao){
                                $tempo_producao = $item['tempo_producao'];
                        }
                }
                $tempo_add = Configure::read('Loja.frete_dias_adicionais');

                if($tempo_producao>0||$tempo_add>0){
                        foreach($consulta as &$frete1):
                                $frete1['Frete'][0]['prazo'] += $tempo_producao;
                                if($tempo_add>0){
                                        $frete1['Frete'][0]['prazo'] += $tempo_add;
                                }
                        endForeach;
                }

                $menor = (isset($consulta[0]['Frete'][0]['preco']) ? $consulta[0]['Frete'][0]['preco'] : 0);
                foreach($consulta as $chave=>&$teste){
                        if ((int)$menor >= (int)$teste['Frete'][0]['preco']) {
                                $menor = $teste['Frete'][0]['preco'];
                                $oMenor = $teste;
                        }
                }

                if($this->verificaFreteGratis($cep,$carrinho)){
                        foreach($consulta as $chave=>$valor){
                                if($valor['Frete'][0]['id']==$oMenor['Frete'][0]['id']){
                                        $consulta[$chave]['Frete'][0]['preco'] = 0;
                                        $consulta[$chave]['FreteTipo']['nome'] = 'GRÁTIS';
                                }
                        }
                }

                return $consulta;
    }
	
	function calculaTransportadora($freteTipo,$peso,$pedido_valor_total){
			App::import("model", "Frete");
        	$this->Frete = new Frete();
			$valorPedido = $pedido_valor_total;

			$valorPeso = $peso;
			$pesoRamos = $peso;
			
			$linha = isset($freteTipo['Frete'][0])?$freteTipo['Frete'][0]:$freteTipo['Frete'];
			$rate['preco']    	= isset($freteTipo['Frete'][0]['preco'])?$freteTipo['Frete'][0]['preco']:$freteTipo['Frete']['preco'];
			
			$frete 		= $linha["frete"];
			$gris 		= $linha["gris"];
			$pedagio	= $linha["pedagio"];
			$tas 	  	= $linha["tas"];
			$icms 		= $linha["icms"];
			$prazo 		= $linha["prazo"];  
			$valor  	= $linha["preco"];
			$zip    	= $linha["cep_final"];
	
			$trt_minimo = $linha["trt_minimo"];
			$trt = $linha["trt"];
			
			if($pedagio < 3) {
				$transportadora = 'Ramos';
			} else {
				$transportadora = 'Jundiai';


				if($pesoRamos > 50) {
					//pega o frete mais baixo da faixa
					$frete_first = $this->Frete->find('first',array('conditions'=>array('peso'=>31,'cep_inicial <='=>$zip,'cep_final >='=>$zip)));
					$preco_50 = $frete_first['Frete']['preco'];
					$peso = $valorPeso - 50;


					$rate['preco'] = $preco_50 + ($peso * $valor);

					
				}

			}
		
			if ($valorPeso <= 200){
				$valorBase = $rate['preco'];
			}else{		
				if($transportadora=='Ramos') {
					$valorBase = $valorPeso * $rate['preco'] / 1000;
				}else{
					$valorBase = $rate['preco'];
				} 
			}
	  
			// ADD FV
			$valorBase 		+= round(($frete/100) * $valorPedido, 2);
			//ADD GRIS
			$valorBase 		+= round(($gris/100) * $valorPedido, 2);
	
			$valorParcial 	= $valorBase;
			
			if($transportadora=='Ramos'){
				$valorParcial += ceil($valorPeso/100)*$pedagio;
			}else
				//ADD PEDAGIO
				$valorParcial+= $pedagio;
			
			//ADD TAS
			$valorParcial+= $tas;
		
			if( $transportadora=='Jundiai' && $trt>0) {
				$trt = (($this->jundiai_desconto * $Price)/100);
				
				if ($trt<$trt_minimo) $trt = $trt_minimo;
				//$trt ++;
				$valorParcial += $trt;
			}
		
			//ADD ICMS
			$base_icms = 1.0 - ($icms/100);
			
			$valor_icms = round($valorParcial/$base_icms, 2);
			$valor_icms = round(($valor_icms*$icms)/100, 2);
			
			$Price = $valorParcial + $valor_icms;
			
		
			//ADD Adiciona os 10% de ajuste
			//echo $valorParcial;
			$Price =  $Price - (($this->jundiai_desconto * $Price)/100);
			
			if(isset($freteTipo['Frete'][0])){
				$freteTipo['Frete'][0]['descricao'] = $transportadora;
				$freteTipo['Frete'][0]['preco'] = $Price;
			}else{
				$freteTipo['Frete']['descricao'] = $transportadora;
				$freteTipo['Frete']['preco'] = $Price;
			}
		
			return $freteTipo;
		
	}
	/**
	 * verifica se o possui regra de frete grátis por produto,categoria ou carrinho.
	 */
	function verificaFreteGratis($cep,$carrinho){

		App::import("model", "FreteGratis");
        $this->FreteGratis = new FreteGratis();
			
		App::import("model", "ProdutoCategoria");
        $this->ProdutoCategoria = new ProdutoCategoria();
		
		$pedidoTotal = 0;
		
		foreach ($carrinho['itens'] as $k => $i) {
			$pedidoTotal 		+= $i['quantidade'] * ($i['preco_promocao']>0?$i['preco_promocao']:$i['preco']);
		}
		//verifica se tem frete gratis por carrinho
		if($this->FreteGratis->find('first',array('conditions'=>array('FreteGratis.data_inicio <= now()', 'FreteGratis.data_fim >= now()','FreteGratis.cep_inicial <='=>$cep,'FreteGratis.cep_final >='=>$cep,'FreteGratis.codigo'=>'CARRINHO','FreteGratis.status'=>true,'FreteGratis.apartir_de <='=>$pedidoTotal)))){
			return true;
		}
		
		//verifica se tem frete grátis por produto, se encontrar um produto que tenha a regra da o frete grátis no carrinho;
		foreach ($carrinho['itens'] as $k => $i) {
			if($this->FreteGratis->find('first',array('conditions'=>array('FreteGratis.data_inicio <= now()', 'FreteGratis.data_fim >= now()','FreteGratis.cep_inicial <='=>$cep,'FreteGratis.cep_final >='=>$cep,'FreteGratis.codigo'=>'PRODUTO','FreteGratis.status'=>true,'FreteGratis.produto_id LIKE concat("%\"",'.$i['id'].',"\"%") ')))){
				return true;
			}
		}
		
		//verifica se tem frete grátis por categoria, se encontrar um produto que tenha a categoria dá frete grátis no carrinho;
		$categorias = array();
		foreach ($carrinho['itens'] as $k => $i) {
			$cats = $this->ProdutoCategoria->find('all',array('recursive'=>-1,'conditions'=>array('ProdutoCategoria.produto_id'=>$i['id'])));
			foreach($cats as $cat){
				$categorias[$cat['ProdutoCategoria']['categoria_id']] = ($i['quantidade'] * ($i['preco_promocao']>0?$i['preco_promocao']:$i['preco']));
			}
		}
		
		foreach ($categorias as $k => $i) {
			if($this->FreteGratis->find('first',array('conditions'=>array('FreteGratis.data_inicio <= now()', 'FreteGratis.data_fim >= now()','FreteGratis.cep_inicial <='=>$cep,'FreteGratis.cep_final >='=>$cep,'FreteGratis.codigo'=>'CATEGORIA','FreteGratis.status'=>true,'FreteGratis.categoria_id LIKE concat("%\"",'.$k.',"\"%") ','FreteGratis.apartir_de <='=>$i)))){
				return true;
			}
		}
		return false;
		
	}
	function consultaEndereco($cep = false) {
        $return = false;
        $cep = preg_replace("/[^0-9]+/", "", $cep);
        if ($cep) {
            $url = "http://www.buscacep.correios.com.br/servicos/dnec/consultaLogradouroAction.do?Metodo=listaLogradouro&TipoConsulta=cep&StartRow=1&EndRow=10&CEP=" . $cep . "";
            preg_match_all("/\<[td].*\>(.*)\<\/[td>]+>/", file_get_contents($url), $regs);
            if (isset($regs[1][0])) {
                if (isset($regs[1][3])) {
                    $return['rua'] = utf8_encode($regs[1][0]);
                    $return['bairro'] = utf8_encode($regs[1][1]);
                    $return['cidade'] = utf8_encode($regs[1][2]);
                    $return['nome_cidade'] = utf8_encode($regs[1][2]);
                    $return['estado'] = utf8_encode(!isset($regs[1][3]) ? $regs[1][1] : $regs[1][3]);
                } else {
                    $return['rua'] = '';
                    $return['bairro'] = '';
                    $return['cidade'] = utf8_encode($regs[1][0]);
                    $return['nome_cidade'] = utf8_encode($regs[1][2]);
                    $return['estado'] = utf8_encode(!isset($regs[1][3]) ? $regs[1][1] : $regs[1][3]);
                }
            }
        }
        return $return;
    }
}
