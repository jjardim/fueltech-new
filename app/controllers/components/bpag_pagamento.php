<?php

class BpagPagamentoComponent extends Object {

    var $url = 'https://bpag.uol.com.br/bpag2/services/BPagWS?wsdl';
    #var $url = 'https://certificacao.bpag.uol.com.br/bpag2/services/BPagWS?wsdl';

    public function pagar($pedido) {

        switch ($pedido['PagamentoCondicao']['sigla']) {
            case 'VISA':
            case 'HIPERCARD':
            case 'MASTERCARD':
            case 'ELO':
            case 'AMEX':
            case 'DINERS':
            case 'BANRISUL':
            case 'BOLETO':
            case 'TRANSFERENCIA':
                return $this->pagamentoWS($pedido);
                break;
            default:
                break;
        }
    }
	public function corrigeString($nome,$tam){
		//se o nome do usuário ultrapassar 30 caracters remove o ultimo nome
		if(strlen($nome) > $tam){
			$nome = explode(' ',$nome);
			array_pop($nome);
			$nome = implode(' ',$nome);
			return $this->corrigeString($nome,$tam);
		}else{
			return $nome;
		}		
	}
    public function EncryptData($plaintext) {
        $fp = fopen(ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'ssl' . DS . "criptografia_dados_prod.crt", "r");		
        $publicKey = fread($fp, 8192);
        fclose($fp);
        $encryptedData = "";
        openssl_public_encrypt($plaintext, $encrypted, $publicKey);
        return base64_encode($encrypted);
    }

    public function campainhaWS($dados) {

        ini_set("soap.wsdl_cache_enabled", "0"); // disabling WSDL cache
        App::import("Xml");
        $client = new SoapClient($this->url);
        $xml = '<probe>
                <merch_ref>' . $dados['merch_ref'] . '</merch_ref>
                <id>' . $dados['id'] . '</id>
                <bpag_payment_id>' . $dados['bpag_payment_id'] . '</bpag_payment_id>
                </probe>';

        $retorno = $client->doService(array('version' => '1.1.0', 'action' => 'probe', 'merchant' => 'famastil', 'user' => 'famastil_ws', 'password' => '123456', 'data' => $xml));
        $log = var_export($retorno->doServiceReturn, true);
        $this->log($log, LOG_DEBUG);
        $retorno = Set::reverse(new Xml($retorno->doServiceReturn));
        return $retorno;
    }

    public function pagamentoWS($pedido) {
        App::import('Helper', 'Html');
        $this->Html = new HtmlHelper();

        App::import("helper", "String");
        $this->String = new StringHelper();

        ini_set("soap.wsdl_cache_enabled", "0");
        
        $client = new SoapClient($this->url);
      
        $frete = $this->String->moedaToBco($pedido['Pedido']['valor_frete']);
        $frete = preg_replace('/[^0-9]/', '', $pedido['Pedido']['valor_frete']);

		$totals = 0;
		$subtotal = 0;
		foreach ($pedido['PedidoItem'] as $item):
			$totals += ($item['preco_promocao'] > 0 || $item['gratis']==true) ? ($item['quantidade']*$item['preco_promocao']) : ($item['quantidade']*$item['preco']);
			$subtotal += ($item['preco_promocao'] > 0 || $item['gratis']==true) ? ($item['quantidade']*$item['preco_promocao']) : ($item['quantidade']*$item['preco']);
        endforeach;
		
        $total = $this->String->bcoToMoeda($totals + $this->String->moedaToBco($pedido['Pedido']['valor_frete']));
        $total = preg_replace('/[^0-9]/', '', $total);
		$subtotal = $this->String->bcoToMoeda($subtotal);
        $subtotal = preg_replace('/[^0-9]/', '', $subtotal);
		
		 //se o nome do usuário ultrapassar 30 caracters remove o ultimo nome
		$pedido['Usuario']['nome'] = $this->corrigeString($pedido['Usuario']['nome'],30);
		$pedido['Pedido']['endereco_complemento'] = $this->corrigeString($pedido['Pedido']['endereco_complemento'],25);
		$pedido['Pedido']['endereco_numero'] = $this->corrigeString($pedido['Pedido']['endereco_numero'],25);
      
        list($dia, $mes, $ano) = explode('-', $pedido['Usuario']['data_nascimento']);
		
		$data_nascimento = $ano . $mes . $dia;
        $documento = $pedido['Usuario']['tipo_pessoa'] == 'F' ? $pedido['Usuario']['cpf'] : $pedido['Usuario']['cnpj'];
        $documento_tipo = ($pedido['Usuario']['tipo_pessoa'] == 'F') ? '0' : '1';
        $xml = '<payOrder>
                  <order_data>
                    <merch_ref>' . $pedido['Pedido']['id']. '</merch_ref>
                    <origin>e-commerce</origin>
                    <currency>BRL</currency>
                    <tax_freight>' . $frete . '</tax_freight>
                    <order_subtotal>' . $subtotal . '</order_subtotal>
                    <order_total>' . $total . '</order_total>
                    <order_items>';
        foreach ($pedido['PedidoItem'] as $item):
            $preco = $item['preco_promocao'] > 0 ? $item['preco_promocao'] : $item['preco'];
            $preco = preg_replace('/[^0-9]/', '', $preco);
            $xml .='<order_item>
                                    <code>' . $item['codigo'] . '</code>
                                    <description>' . $item['nome'] . '</description>
                                    <units>' . $item['quantidade'] . '</units>
                                    <unit_value>' . $preco . '</unit_value>
                                </order_item>';
        endforeach;
        $xml .= '</order_items>
                  </order_data>
                  <behavior_data>
                    <language>ptbr</language>
                    <url_post_bell>' . $this->Html->Url('/pedidos/ws_campainha', true) . '</url_post_bell>
                  </behavior_data>
                  <payment_data>
                    <payment>
                      <payment_method>' . $pedido['PagamentoCondicao']['codigo'] . '</payment_method>
                      <installments>' . $pedido['Pedido']['parcelas'] . '</installments>
                      <payment_value>' . $total . '</payment_value>';
        //se for boleto não manda o campos de cartão
        if (up($pedido['PagamentoCondicao']['sigla']) != 'BOLETO' &&up($pedido['PagamentoCondicao']['sigla']) != 'TRANSFERENCIA' && up($pedido['PagamentoCondicao']['sigla']) !='BANRISUL') {
            $pedido['Pedido']['numero_cartao'] = $this->EncryptData($pedido['Pedido']['numero_cartao']);
            $pedido['Pedido']['codigo_seguranca_cartao'] = $this->EncryptData($pedido['Pedido']['codigo_seguranca_cartao']);
            $xml .= '<cc_brand>' . low($pedido['PagamentoCondicao']['sigla']) . '</cc_brand>
                                <cc_number>' . $pedido['Pedido']['numero_cartao'] . '</cc_number>
                                <cc_cvv>' . $pedido['Pedido']['codigo_seguranca_cartao'] . '</cc_cvv>
                                <cc_exp_month>' . current(explode('/', $pedido['Pedido']['validade_cartao'])) . '</cc_exp_month>
                                <cc_exp_year>' . end(explode('/', $pedido['Pedido']['validade_cartao'])) . '</cc_exp_year>
                                <cc_card_holder>' . $pedido['Pedido']['nome_portador_cartao'] . '</cc_card_holder>';
        }
        $xml .= '</payment>
                  </payment_data>
                  <customer_data>
                    <customer_id>' . $pedido['Usuario']['id'] . '</customer_id>
                    <customer_info>
                      <first_name>' . $pedido['Usuario']['nome'] . '</first_name>
                      <email>' . $pedido['Usuario']['email'] . '</email>
                      <gender>' . $pedido['Usuario']['sexo'] . '</gender>
                      <birthday>' . $data_nascimento . '</birthday>
                      <document_type>' . $documento_tipo . '</document_type>
                      <document>' . $documento . '</document>
                      <phone_home>
                        <country_code>55</country_code>
                        <area_code>' . substr($pedido['Usuario']['telefone'], -2) . '</area_code>
                        <phone_number>' . substr($pedido['Usuario']['telefone'], 2) . '</phone_number>
                      </phone_home>';
        if (!empty($pedido['Usuario']['celular'])) {
            $xml .= '<phone_mobile>
                                    <country_code>55</country_code>
                                    <area_code>' . substr($pedido['Usuario']['celular'], -2) . '</area_code>
                                    <phone_number>' . substr($pedido['Usuario']['celular'], 2) . '</phone_number>
                                  </phone_mobile>';
        }

        $xml .= '<address_street>' . $pedido['Pedido']['endereco_rua'] . '</address_street>
                      <address_street_nr>' . $pedido['Pedido']['endereco_numero'] . '</address_street_nr>
                      <address_additional_data>' . $pedido['Pedido']['endereco_complemento'] . '</address_additional_data>
                      <address_comunity>' . $pedido['Pedido']['endereco_bairro'] . '</address_comunity>
                      <address_city>' . $pedido['Pedido']['endereco_cidade'] . '</address_city>
                      <address_state>' . $pedido['Pedido']['endereco_estado'] . '</address_state>
                      <address_country>BR</address_country>
                      <address_zip>' . $pedido['Pedido']['endereco_cep'] . '</address_zip>
                    </customer_info>
                    <billing_info>
                      <first_name>' . $pedido['Usuario']['nome'] . '</first_name>
                      <email>' . $pedido['Usuario']['email'] . '</email>
                      <gender>' . $pedido['Usuario']['sexo'] . '</gender>
                      <birthday>' . $data_nascimento . '</birthday>
                      <document_type>' . $documento_tipo . '</document_type>
                      <document>' . $documento . '</document>
                      <phone_home>
                        <country_code>55</country_code>
                        <area_code>' . substr($pedido['Usuario']['telefone'], -2) . '</area_code>
                        <phone_number>' . substr($pedido['Usuario']['telefone'], 2) . '</phone_number>
                      </phone_home>';
        if (!empty($pedido['Usuario']['celular'])) {
            $xml .= '<phone_mobile>
                                    <country_code>55</country_code>
                                    <area_code>' . substr($pedido['Usuario']['celular'], -2) . '</area_code>
                                    <phone_number>' . substr($pedido['Usuario']['celular'], 2) . '</phone_number>
                                  </phone_mobile>';
        }
        $xml .= '
                      <address_street>' . $pedido['Pedido']['endereco_rua'] . '</address_street>
                      <address_street_nr>' . $pedido['Pedido']['endereco_numero'] . '</address_street_nr>
                      <address_additional_data>' . $pedido['Pedido']['endereco_complemento'] . '</address_additional_data>
                      <address_comunity>' . $pedido['Pedido']['endereco_bairro'] . '</address_comunity>
                      <address_city>' . $pedido['Pedido']['endereco_cidade'] . '</address_city>
                      <address_state>' . $pedido['Pedido']['endereco_estado'] . '</address_state>
                      <address_country>BR</address_country>
                      <address_zip>' . $pedido['Pedido']['endereco_cep'] . '</address_zip>
                    </billing_info>
                    <shipment_info>
                      <first_name>' . $pedido['Usuario']['nome'] . '</first_name>
                      <email>' . $pedido['Usuario']['email'] . '</email>
                      <gender>' . $pedido['Usuario']['sexo'] . '</gender>
                      <birthday>' . $data_nascimento . '</birthday>
                      <document_type>' . $documento_tipo . '</document_type>
                      <document>' . $documento . '</document>
                      <phone_home>
                        <country_code>55</country_code>
                        <area_code>' . substr($pedido['Usuario']['telefone'], -2) . '</area_code>
                        <phone_number>' . substr($pedido['Usuario']['telefone'], 2) . '</phone_number>
                      </phone_home>';
        if (!empty($pedido['Usuario']['celular'])) {
            $xml .= '<phone_mobile>
                                    <country_code>55</country_code>
                                    <area_code>' . substr($pedido['Usuario']['celular'], -2) . '</area_code>
                                    <phone_number>' . substr($pedido['Usuario']['celular'], 2) . '</phone_number>
                                  </phone_mobile>';
        }
        $xml .= '
                      <address_street>' . $pedido['Pedido']['endereco_rua'] . '</address_street>
                      <address_street_nr>' . $pedido['Pedido']['endereco_numero'] . '</address_street_nr>
                      <address_additional_data>' . $pedido['Pedido']['endereco_complemento'] . '</address_additional_data>
                      <address_comunity>' . $pedido['Pedido']['endereco_bairro'] . '</address_comunity>
                      <address_city>' . $pedido['Pedido']['endereco_cidade'] . '</address_city>
                      <address_state>' . $pedido['Pedido']['endereco_estado'] . '</address_state>
                      <address_country>BR</address_country>
                      <address_zip>' . $pedido['Pedido']['endereco_cep'] . '</address_zip>
                    </shipment_info>
                  </customer_data>
                </payOrder>';

        App::import("Xml");
        $retorno = $client->doService(array('version' => '1.1.0', 'action' => 'payOrder', 'merchant' => 'famastil', 'user' => 'famastil_ws', 'password' => 'mudar123', 'data' => $xml));
        $log = var_export($retorno->doServiceReturn, true);
        $this->log($log, LOG_DEBUG);
		$xml = var_export($xml, true);
        $this->log($xml, LOG_DEBUG);
        return Set::reverse(new Xml($retorno->doServiceReturn));
    }

}