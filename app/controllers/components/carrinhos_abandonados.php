<?php

/**
 * Classe que gerência os produtos carrinho_abandonado
 * @Author Luan Garcia <luan.garcia@gmail.com>
 * */
Class CarrinhosAbandonadosComponent extends Object {
    public $Cookie;
    function __construct() {
        App::import("component", "Cookie");
        $this->Cookie = new CookieComponent();
        $this->Cookie->name = 'carrinho_abandonado';
        $this->Cookie->time = '365 Days';
        $this->Cookie->key = 'qSI232qs*&sXOw!';
    }
    function add($produto) {
        $adicionado_tmp = $this->Cookie->read('carrinho_abandonado');
		$produto_tmp = unserialize($produto);		
		foreach($adicionado_tmp as $key => $temp){
			$temp2 = unserialize($temp);
			foreach($produto_tmp as $tmp){
				if($temp2['produto_id'] == $tmp['produto_id'])
					unset($adicionado_tmp[$key]);
			}
		}
		$adicionado_tmp[] = $produto;
		$this->Cookie->write('carrinho_abandonado',$adicionado_tmp, false);
    }
    function getAll(){
        return $this->Cookie->read('carrinho_abandonado');
    }
	function remove($id) {
        $adicionado_tmp = $this->Cookie->read('carrinho_abandonado');
		foreach($adicionado_tmp as $key => $temp){
			if($id == $tmp['produto_id'])
				unset($adicionado_tmp[$key]);
		}
		$this->Cookie->write('carrinho_abandonado',$adicionado_tmp, false);
    }
	function destroy(){
		$this->Cookie->write('carrinho_abandonado',array(), false);
	}
}

