<?php

class AtualizarEstoqueController extends AppController {
    public $uses = array();
    public $components = array('Importacao', 'Session');
    
    public function admin_index()
    {
	$this->layout = false;
	$this->autoRender = false;
	$this->render(false);
	$this->Importacao->estoque();
	$this->Session->setFlash('O estoque dos produtos foi atualizado.', 'flash/success');
	$this->redirect(array('controller' => 'home', 'action' => 'index'));
    }
}