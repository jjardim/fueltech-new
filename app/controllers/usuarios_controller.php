<?php

class UsuariosController extends AppController {

    var $components = array('Email', 'Session', 'Carrinho.Carrinho', 'Filter', 'CarrinhosAbandonados');
    var $helpers = array('Calendario', 'String', 'Image', 'Flash', 'Javascript', 'Estados');
    var $name = 'Usuarios';
    var $uses = array('UsuarioEndereco', 'Usuario');

    public function exportar_usuario($id) {
        $export_dir = WWW_ROOT . "integracao" . DS . "cliente" . DS;
        $this->loadModel('Usuario');
        $usuario = $this->Usuario->find('first', array('contain' => array('UsuarioEndereco'), 'conditions' => array('Usuario.id' => $id)));

        if ($usuario['Usuario']['tipo_pessoa'] == 'F') {
            $tipoCliente = 'F';
            $strTipoCliente = 'cpf';
        } else {
            $tipoCliente = 'J';
            $strTipoCliente = 'cnpj';
        }

        // Limpa telefone
        $arReplace = array('(', ')', '-', ' ', '.', ',', '/');
        $strTelefone = substr(str_replace($arReplace, "", trim($usuario['Usuario']['telefone'])), 0, 20);
        $strCelular = substr(str_replace($arReplace, "", trim($usuario['Usuario']['celular'])), 0, 20);

        // Limpa cpf/cnpj
        $cpf_cnpj = substr(str_replace($arReplace, "", trim(($usuario['Usuario']['tipo_pessoa'] == 'F') ? $usuario['Usuario']['cpf'] : $usuario['Usuario']['cnpj'])), 0, 14);

        // Razão Social
        $razao_social = ($usuario['Usuario']['tipo_pessoa'] == 'J') ? $usuario['Usuario']['razao_social'] : $usuario['Usuario']['nome'];

        //$customerData
        $xml = "<?xml version='1.0' encoding='utf-8'?>
				<cliente>
					<tipo_cliente>" . $tipoCliente . "</tipo_cliente>
					<" . $strTipoCliente . ">" . $cpf_cnpj . "</" . $strTipoCliente . ">
					<razao_social>" . $razao_social . "</razao_social>
					<fone>" . $strTelefone . "</fone>
					<celular>" . $strCelular . "</celular>
					<email>" . substr($usuario['Usuario']['email'], 0, 50) . "</email>";
        //se tem mais de 1 endereco
        if (count($usuario['UsuarioEndereco']) > 1) {
            $enderecos = array();
            foreach ($usuario['UsuarioEndereco'] as $endereco):
                if ($endereco['cobranca'] == true) {
                    $tipo_endereco = "C";
                    $enderecos[] = $tipo_endereco;
                } elseif (!in_array("E", $enderecos)) {
                    $tipo_endereco = "E";
                    $enderecos[] = $tipo_endereco;
                } else {
                    $tipo_endereco = "O";
                    $enderecos[] = $tipo_endereco;
                }
                $xml .= "<endereco>
								<tipo_endereco>" . $tipo_endereco . "</tipo_endereco>
								<cep_entrega>" . substr($endereco['cep'], 0, 5) . '-' . substr($endereco['cep'], 5) . "</cep_entrega>	
								<logradouro_entrega>" . substr($endereco['rua'], 0, 70) . "</logradouro_entrega>
								<numero_entrega>" . substr($endereco['numero'], 0, 10) . "</numero_entrega>
								<complemento_entrega>" . substr($endereco['complemento'], 0, 70) . "</complemento_entrega>
								<bairro_entrega>" . substr($endereco['bairro'], 0, 50) . "</bairro_entrega>
								<cidade_entrega>" . substr($endereco['cidade'], 0, 30) . "</cidade_entrega>
								<estado_entrega>" . substr($endereco['uf'], 0, 2) . "</estado_entrega>
								<pais_entrega>BR</pais_entrega>
							</endereco>";
            endforeach;
        } else {
            $xml .= "<endereco>
								<tipo_endereco>C</tipo_endereco>
								<cep_entrega>" . substr($usuario['UsuarioEndereco'][0]['cep'], 0, 5) . '-' . substr($usuario['UsuarioEndereco'][0]['cep'], 5) . "</cep_entrega>	
								<logradouro_entrega>" . substr($usuario['UsuarioEndereco'][0]['rua'], 0, 70) . "</logradouro_entrega>
								<numero_entrega>" . substr($usuario['UsuarioEndereco'][0]['numero'], 0, 10) . "</numero_entrega>
								<complemento_entrega>" . substr($usuario['UsuarioEndereco'][0]['complemento'], 0, 70) . "</complemento_entrega>
								<bairro_entrega>" . substr($usuario['UsuarioEndereco'][0]['bairro'], 0, 50) . "</bairro_entrega>
								<cidade_entrega>" . substr($usuario['UsuarioEndereco'][0]['cidade'], 0, 30) . "</cidade_entrega>
								<estado_entrega>" . substr($usuario['UsuarioEndereco'][0]['uf'], 0, 2) . "</estado_entrega>
								<pais_entrega>BR</pais_entrega>
							</endereco>";
            $xml .= "<endereco>
								<tipo_endereco>E</tipo_endereco>
								<cep_entrega>" . substr($usuario['UsuarioEndereco'][0]['cep'], 0, 5) . '-' . substr($usuario['UsuarioEndereco'][0]['cep'], 5) . "</cep_entrega>	
								<logradouro_entrega>" . substr($usuario['UsuarioEndereco'][0]['rua'], 0, 70) . "</logradouro_entrega>
								<numero_entrega>" . substr($usuario['UsuarioEndereco'][0]['numero'], 0, 10) . "</numero_entrega>
								<complemento_entrega>" . substr($usuario['UsuarioEndereco'][0]['complemento'], 0, 70) . "</complemento_entrega>
								<bairro_entrega>" . substr($usuario['UsuarioEndereco'][0]['bairro'], 0, 50) . "</bairro_entrega>
								<cidade_entrega>" . substr($usuario['UsuarioEndereco'][0]['cidade'], 0, 30) . "</cidade_entrega>
								<estado_entrega>" . substr($usuario['UsuarioEndereco'][0]['uf'], 0, 2) . "</estado_entrega>
								<pais_entrega>BR</pais_entrega>
							</endereco>";
        }
        $xml .= "</cliente>";
        $fp = fopen($export_dir . 'cliente' . $usuario['Usuario']['id'] . '.xml', 'w');
        fwrite($fp, $xml);
        fclose($fp);
    }

    function login() {

        //model CarrinhoAbandonado
        App::import('Model', 'CarrinhoAbandonado');
        $this->CarrinhoAbandonado = new CarrinhoAbandonado();

        if (!empty($this->data)) {
            $usuario_db = $this->Usuario->find('first', array('recursive' => -1, 'conditions' => array('Usuario.email' => $this->data['Usuario']['login_email'])));

            $hash = explode(':', $usuario_db['Usuario']['senha']);

            if (isset($hash[1])) {

                $this->data['Usuario']['login_senha'] = $hash[1] . $this->data['Usuario']['login_senha'];
                $usuario['Usuario']['senha'] = Security::hash($this->data['Usuario']['login_senha']) . ':' . $hash[1];
            } else {
                $usuario['Usuario']['senha'] = Security::hash($this->data['Usuario']['login_senha']);
            }

            $usuario['Usuario']['email'] = $this->data['Usuario']['login_email'];

            if ($this->Auth->login($usuario)) {

                //redireiciona o usuario para a tela de validacao
                if ($this->Session->read('UrlProdutoAtualId') != null) {

                    $this->redirect($this->Session->read('UrlProdutoAtualId') . '#rating-form');
                }

                //dados usuarios
                $usuario_logado = $this->Auth->User();
                $data['usuario_id'] = $usuario_logado['Usuario']['id'];

                if ($this->Carrinho->getStep() >= 1 && count($this->Carrinho->getItens()) > 0) {
                    //salvo o registro de pedido abandonado temporario, enquanto o usuario não finaliza a compra
                    if (count($this->CarrinhosAbandonados->getAll()) > 0) {
                        $data['produtos'] = $this->CarrinhosAbandonados->getAll();
                        //salvo CarrinhoAbandonado
                        $this->CarrinhoAbandonado->salvar($data);
                    }
                    $this->redirect(array('plugin' => 'carrinho', 'controller' => 'carrinho', 'action' => 'entrega'));
                } else {
                    $this->redirect(array('action' => 'edit'));
                }
            } else {
                $this->Session->setFlash('Login e/ou senha inválidos', 'flash/error');
            }
        }

        //verifica se veio do produto
        if (preg_match('/.*prod-[0-9]+\.html/', Controller::referer())) {
            $this->Session->write("UrlProdutoAtualId", Controller::referer());
        } else {
            $this->Session->write("UrlProdutoAtualId", null);
        }

        $usuario = $this->Auth->User();
        if ($usuario['Usuario']['id']) {

            //redireiciona o usuario para a tela de validacao
            if ($this->Session->read('UrlProdutoAtualId') != "") {
                $this->redirect($this->Session->read('UrlProdutoAtualId'));
            }

            //dados usuarios
            $usuario_logado = $this->Auth->User();
            $data['usuario_id'] = $usuario_logado['Usuario']['id'];

            if ($this->Carrinho->getStep() >= 1 && count($this->Carrinho->getItens()) > 0) {
                //salvo o registro de pedido abandonado temporario, enquanto o usuario não finaliza a compra
                if (count($this->CarrinhosAbandonados->getAll()) > 0) {
                    $data['produtos'] = $this->CarrinhosAbandonados->getAll();
                    //salvo CarrinhoAbandonado
                    $this->CarrinhoAbandonado->salvar($data);
                }
                $this->redirect(array('plugin' => 'carrinho', 'controller' => 'carrinho', 'action' => 'entrega'));
            } else {
                $this->redirect(array('controller' => 'home', 'action' => 'index'));
            }
        }

        $this->set('breadcrumbs', array(array('nome' => 'Usuarios', 'link' => '/usuarios/add'), array('nome' => 'Login', 'link' => '/usuarios/login')));
    }

    function logout() {
        Cache::clear();
        $this->Session->setFlash('Logout realizado com sucesso.', 'flash/success');
        $this->redirect($this->Auth->logout());
    }

    function admin_login() {
        $this->layout = 'admin_login';
        if (!empty($this->data)) {
            $usuario_db = $this->Usuario->find('first', array('recursive' => -1, 'conditions' => array('Usuario.email' => $this->data['Usuario']['login_email'])));

            $hash = explode(':', $usuario_db['Usuario']['senha']);

            if (isset($hash[1])) {
                $this->data['Usuario']['login_senha'] = $hash[1] . $this->data['Usuario']['login_senha'];
                $usuario['Usuario']['senha'] = Security::hash($this->data['Usuario']['login_senha']) . ':' . $hash[1];
            } else {
                $usuario['Usuario']['senha'] = Security::hash($this->data['Usuario']['login_senha']);
            }

            $usuario['Usuario']['email'] = $this->data['Usuario']['login_email'];


            if ($this->Auth->login($usuario)) {
                $this->redirect(array('controller' => 'home', 'action' => 'index', 'admin' => true, 'plugin' => null));
            } else {
                $this->Session->setFlash('Login e/ou senha inválidos', 'flash/error');
            }
        }
    }

    function admin_logout() {
        Cache::clear();
        $this->Session->setFlash('Logout realizado com sucesso.', 'flash/success');
        $this->redirect($this->Auth->logout());
    }

    function admin_index() {

        $filtros = array();
        if ($this->data["Filter"]["filtro"]) {
            $filtros['filtro'] = "Usuario.nome LIKE '%{%value%}%' OR Usuario.email LIKE '%{%value%}%' OR Usuario.cpf LIKE '%{%value%}%'  OR Usuario.cnpj LIKE '%{%value%}%' ";
        }

        if ($this->data["Filter"]["tipo_pessoa"]) {
            $filtros['tipo_pessoa'] = "Usuario.tipo_pessoa = '{%value%}'";
        }

        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();

        if (isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar") {
            $this->admin_exportar($conditions);
        }

        $this->Usuario->recursive = 1;
        $this->set('usuarios', $this->paginate('Usuario', array('site_id' => 1, $conditions)));
    }

    function edit($tipo = null) {

        $usuario = $this->Auth->User();

        if (!$usuario['Usuario']['id']) {
            $this->Session->setFlash('Paramentros inválidos', 'flash/error');
            $this->redirect(array('controller' => 'usuarios', 'action' => 'login'));
        }
        if (!empty($this->data)) {
            $this->data['Usuario']['id'] = $usuario['Usuario']['id'];
            $this->data['Usuario']['grupo_id'] = $usuario['Usuario']['grupo_id'];

            // if( isset($this->data['Usuario']['nomeTmp']) && !empty($this->data['Usuario']['nomeTmp']) ){
            // $this->data['Usuario']['nome'] = $this->data['Usuario']['nomeTmp'];
            // }
            //remove o endereco do cadastro se nao passar um cep
            if ($tipo != 'endereco') {
                if ($this->Usuario->save($this->data)) {
                    $this->exportar_usuario($usuario['Usuario']['id']);
                    $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                    $usuario['Usuario']['senha'] = Security::hash($this->data['Usuario']['senha_nova']);
                    $usuario['Usuario']['email'] = $this->data['Usuario']['email'];
                    if ($this->Carrinho->getStep() >= 1 && count($this->Carrinho->getItens()) > 0) {
                        $this->redirect(array('plugin' => 'carrinho', 'controller' => 'carrinho', 'action' => 'entrega'));
                    } else {
                        $this->redirect(array('controller' => 'usuarios', 'action' => 'edit'));
                    }
                } else {
                    $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
                }
            } else {
                $this->data['UsuarioEndereco']['usuario_id'] = $usuario['Usuario']['id'];
                if ($this->UsuarioEndereco->save($this->data)) {
                    if ($this->data['UsuarioEndereco']['cobranca'] == true && $this->data['UsuarioEndereco']['id'] != "") {
						$this->UsuarioEndereco->updateAll(array('cobranca' => '0'), array('UsuarioEndereco.usuario_id' => $usuario['Usuario']['id'], 'UsuarioEndereco.id !=' => $this->data['UsuarioEndereco']['id']));
                    } else {
                        $this->UsuarioEndereco->updateAll(array('cobranca' => '0'), array('UsuarioEndereco.usuario_id' => $usuario['Usuario']['id'], 'UsuarioEndereco.id !=' => $this->UsuarioEndereco->id));
                    }
					
                    $this->exportar_usuario($usuario['Usuario']['id']);
                    $this->Session->setFlash('Dados salvos com sucesso!', 'flash/success');
                    if ($this->Carrinho->getStep() >= 1 && count($this->Carrinho->getItens()) > 0) {
                        $this->redirect(array('plugin' => 'carrinho', 'controller' => 'carrinho', 'action' => 'entrega'));
                    } else {
                        $this->redirect(array('controller' => 'usuario_enderecos', 'action' => 'edit'));
                    }
                } else {
                    $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
                }
                $user = $this->Usuario->find('first', array('conditions' => array('Usuario.id' => $usuario['Usuario']['id'])));
                $this->data['Usuario'] = $user['Usuario'];
            }
        }

        if (empty($this->data)) {
            $this->data = $this->Usuario->read(null, $usuario['Usuario']['id']);
            $this->data['UsuarioEndereco'] = $this->data['UsuarioEndereco'][0];
            if (!$this->data) {
                unset($this->data['Usuario']['senha']);
                $this->redirect(array('action' => 'index'));
            }
        }

        $usuario = $this->Usuario->find('first', array('conditions' => array('Usuario.id' => $usuario['Usuario']['id'])));
        $this->set('enderecos', $usuario['UsuarioEndereco']);
        $this->set('breadcrumbs', array(array('nome' => 'Cadastro Edição', 'link' => '/usuarios/add')));

        //start breadcrumb
        $breadcrumb = "";
        $breadcrumb[] = array('url' => 'javascript:void(0);', 'nome' => "Meus dados");
        $this->set('breadcrumb', $breadcrumb);
        //end breadcrumb
    }

    function ajax_endereco_usuario($endereco_id = null) {
        $usuario = $this->Auth->User();
        $endereco = $this->UsuarioEndereco->find('first', array('recursive' => -1, 'conditions' => array('UsuarioEndereco.id' => $endereco_id, 'UsuarioEndereco.usuario_id' => $usuario['Usuario']['id'])));
        die(json_encode($endereco));
    }

    function add() {

        if ($this->Auth->User()) {
            $this->redirect(array('controller' => 'usuarios', 'action' => 'edit'));
        }
        if (!empty($this->data) && $this->referer() != '/login') {
            //$this->Usuario->create();
            //grupo administrador 1
            //grupo cliente 2
            //grupo public 3
            $this->data['Usuario']['grupo_id'] = 2;
            $this->data['Usuario']['status'] = true;
            $this->data['Usuario']['activation_key'] = md5(uniqid());
            
            if ($this->Usuario->saveAll($this->data, array('validate' => 'first'))) {
                $this->enviaEmailConfirmacao($this->data);
				
				// if ($this->Carrinho->getStep() >= 1 && count($this->Carrinho->getItens()) > 0){
					// $this->integracaoDinamize($this->data, '24');
				// }else{
					// $this->integracaoDinamize($this->data, '23');
				// }
                $mensagem = "Nome: ".$this->data['Usuario']['nome']." Data de Nascimento:".$this->data['Usuario']['data_nascimento']." Sexo:".$this->data['Usuario']['sexo']." RG:".$this->data['Usuario']['rg']." CPF:".$this->data['Usuario']['cpf']." Email:".$this->data['Usuario']['email']
                            ."</br> Telefone:".$this->data['Usuario']['telefone']." Celular:".$this->data['Usuario']['celular']." Rua :".$this->data['UsuarioEndereco'][0]['rua']." Número :".$this->data['UsuarioEndereco'][0]['numero']." Complemento:".$this->data['UsuarioEndereco'][0]['complemento'].
                            "</br> Bairro:".$this->data['UsuarioEndereco'][0]['bairro']." Cep:".$this->data['UsuarioEndereco'][0]['cep']." Cidade:".$this->data['UsuarioEndereco'][0]['cidade']." UF:".$this->data['UsuarioEndereco'][0]['uf'];
                            
                require_once('vendors/AdManagerAPI.class.php');               

                $admanager = new AdManagerAPI();       
                $admanager->registraAcesso();                  

                $meio_captacao = 'FORM_IDENTIFICACAO';
                $admanager->registraLead($meio_captacao,$this->data['Usuario']['nome'],$this->data['Usuario']['email'],$this->data['Usuario']['telefone'],$this->data['UsuarioEndereco'][0]['cidade'],$this->data['UsuarioEndereco'][0]['uf'],'Brasil',iconv("UTF-8", "ISO-8859-1//IGNORE", $mensagem));
				
				if (!$this->Carrinho->getStep() >= 1)
                    $this->Session->setFlash('Seu cadastro foi realizado com sucesso!', 'flash/success');
                $usuario['Usuario']['senha'] = Security::hash($this->data['Usuario']['senha_nova']);
                $usuario['Usuario']['email'] = $this->data['Usuario']['email'];

                if ($this->data['UsuarioEndereco'][0]['cobranca'] == true) {
                    $endereco = $this->UsuarioEndereco->find('first', array('order' => 'UsuarioEndereco.id DESC', 'recursive' => -1, 'limit' => 1, 'conditions' => array('UsuarioEndereco.usuario_id' => $this->Usuario->id)));
                    $this->UsuarioEndereco->updateAll(array('cobranca' => '0'), array('UsuarioEndereco.usuario_id' => $this->Usuario->id, 'UsuarioEndereco.id !=' => $endereco['UsuarioEndereco']['id']));
                }

                if ($this->Auth->login($usuario)) {
                    $this->exportar_usuario($this->Usuario->id);
                    $this->Carrinho->setStep(1);
                    if ($this->Carrinho->getStep() >= 1 && count($this->Carrinho->getItens()) > 0) {
                        $this->redirect(array('plugin' => 'carrinho', 'controller' => 'carrinho', 'action' => 'entrega'));
                    } else {
                    	#$this->redirect(array('action' => 'edit'));
                    	$this->Session->setFlash('Cadastro efetuado com sucesso!', 'flash/success');
                        $this->redirect(array('controller' => 'produtos'));
                    }
                }
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }

        if (isset($this->data['Usuario']['usuario_email']) && isset($this->data['Usuario']['usuario_cep'])) {
            $this->data['UsuarioEndereco'][0]['cep'] = $this->data['Usuario']['usuario_cep'];
            $this->loadModel('Frete');
            $this->data['Usuario']['email'] = $this->data['Usuario']['usuario_email'];
            App::import("Component", "Frete");
            $this->Frete = new FreteComponent();
            $cep = $this->Frete->consultaEndereco($this->data['Usuario']['usuario_cep']);

            if ($cep) {
                $this->data['UsuarioEndereco'][0]['rua'] = $cep['rua'];
                $this->data['UsuarioEndereco'][0]['cidade'] = $cep['cidade'];
                $this->data['UsuarioEndereco'][0]['uf'] = $cep['estado'];
                $this->data['UsuarioEndereco'][0]['bairro'] = $cep['bairro'];
            }
        }

        //start breadcrumb
        //$breadcrumb = "";
        //$breadcrumb[] = array('url' => 'javascript:void(0);', 'nome' => "Atendimento");
        //$breadcrumb[] = array('url' => 'javascript:void(0);', 'nome' => "Loja Virtual");
        //$this->set('breadcrumb', $breadcrumb);
        //end breadcrumb
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Usuario->create();
            $this->data['Usuario']['activation_key'] = md5(uniqid());
            if ($this->Usuario->save($this->data)) {
                $this->exportar_usuario($this->Usuario->id);
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        $grupos = array('' => 'Selecione') + $this->Usuario->Grupo->find('list', array('fields' => array('nome')));
        $this->set(compact('grupos'));
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Paramentros inválidos.', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->Usuario->save($this->data)) {
                $this->exportar_usuario($this->data['Usuario']['id']);
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Usuario->read(null, $id);
            if ($this->data) {
                
            } else {
                unset($this->data['Usuario']['senha']);
                $this->redirect(array('action' => 'index'));
            }
        }
        $grupos = array('' => 'Selecione') + $this->Usuario->Grupo->find('list', array('fields' => array('nome')));
        $this->set(compact('grupos'));
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('controller' => 'usuarios', 'action' => 'index'));
        }
        if ($this->Usuario->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('controller' => 'usuarios', 'action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('controller' => 'usuarios', 'action' => 'index'));
    }

    function activate($email = null, $key = null) {
        if ($email == null || $key == null) {
            $this->redirect(array('action' => 'login'));
        }
        if ($this->Usuario->hasAny(array(
                    'Usuario.email' => $email,
                    'Usuario.activation_key' => $key
                ))) {
            $usuario = $this->Usuario->findByEmail($email);
            $this->Usuario->id = $usuario['Usuario']['id'];
            $this->Usuario->saveField('status', 1);
            $this->Usuario->saveField('activation_key', md5(uniqid()));
            $this->Session->setFlash('Conta ativada com sucesso!.', 'flash/success');
        } else {
            $this->Session->setFlash('Ocorreu um erro ao tentar ativar sua conta.', 'flash/error');
        }

        $this->redirect(array('action' => 'login'));
    }

    function esqueci() {
        $this->layout = 'modal';
        if (!empty($this->data)) {

            $usuario = $this->Usuario->findByEmail($this->data['Usuario']['email']);
            if (isset($usuario['Usuario']['id'])) {

                $this->Usuario->id = $usuario['Usuario']['id'];
                $activationKey = md5(uniqid());
                $this->Usuario->saveField('activation_key', $activationKey);

                if (Configure::read('Loja.smtp_host') != 'localhost') {
                    $this->Email->smtpOptions = array(
                        'port' => (int) Configure::read('Loja.smtp_port'),
                        'host' => Configure::read('Loja.smtp_host'),
                        'username' => Configure::read('Loja.smtp_user'),
                        'password' => Configure::read('Loja.smtp_password')
                    );
                    $this->Email->delivery = 'smtp';
                }
                $this->Email->lineLength = 120;
                $this->Email->sendAs = 'html';
                $this->Email->from = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_compra') . ">";
                $this->Email->to = "{$usuario['Usuario']['nome']} <{$usuario['Usuario']['email']}>";
                $this->Email->subject = Configure::read('Loja.nome') . ' - Alteração de Senha';

                $email = str_replace(array(
                    '{URL}', '{USUARIO_NOME}', '{USUARIO_IP}', '{LOJA_NOME}'
                        ), array(
                    Router::url(array('controller' => 'usuarios', 'action' => 'reset', $usuario['Usuario']['email'], $activationKey,), true),
                    $usuario['Usuario']['nome'],
                    $_SERVER['REMOTE_ADDR'],
					Configure::read('Loja.smtp_assinatura_email')
                        ), Configure::read('LojaTemplate.esqueci_senha')
                );

                if (Configure::read('Reweb.nome_bcc')) {
                    $bccs = array();
                    $assunto = Configure::read('Reweb.nome_bcc');
                    $emails = Configure::read('Reweb.email_bcc');
                    foreach (explode(';', $emails) as $bcc) {
                        $bccs[] = "{$assunto} <{$bcc}>";
                    }
                    $this->Email->bcc = $bccs;
                }

                if ($this->Email->send($email)) {
                    $this->Session->setFlash('As instruções para recuperar sua senha foram enviadas para seu email.', 'flash/success');
                    $this->data = array();
                } else {
                    $this->Session->setFlash('Ocorreu um erro ao tentar resetar sua senha, tente novamente.', 'flash/error');
                }
            } elseif (empty($this->data['Usuario']['email'])) {
                $this->Usuario->invalidate('email', 'Campo de preenchimento obrigatório');
            } else {
                $this->Usuario->invalidate('email', 'O email informado não foi encontrado.');
            }
        }
    }
	function enviaEmailConfirmacao($data) {
        $this->layout = false;
		$this->render = false;
        if (!empty($data)) {
		   if (Configure::read('Loja.smtp_host') != 'localhost') {
				$this->Email->smtpOptions = array(
					'port' => (int) Configure::read('Loja.smtp_port'),
					'host' => Configure::read('Loja.smtp_host'),
					'username' => Configure::read('Loja.smtp_user'),
					'password' => Configure::read('Loja.smtp_password')
				);
				$this->Email->delivery = 'smtp';
		   }
			$this->Email->lineLength = 120;
			$this->Email->sendAs = 'html';
			$this->Email->from = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_compra') . ">";
			$this->Email->to = "{$data['Usuario']['nome']} <{$data['Usuario']['email']}>";
			$this->Email->subject = Configure::read('Loja.nome') . ' - Confirmação de Cadastro';

			$email = str_replace(array(
				'{USUARIO_NOME}', '{USUARIO_EMAIL}', '{USUARIO_SENHA}', '{LOJA_NOME}'
				), array(
				$data['Usuario']['nome'],
				$data['Usuario']['email'],
				$data['Usuario']['senha_nova'],
				Configure::read('Loja.smtp_assinatura_email')
				), Configure::read('LojaTemplate.confirmacao_cadastro')
			);

			if ($this->Email->send($email)) {
				return true;
			} else {
				return false;
			}
        }
    }

    function reset($email = null, $key = null) {

        if ($email == null || $key == null) {
            $this->Session->setFlash('Paramentros inválidos.', 'flash/error');
            $this->redirect(array('action' => 'login'));
        }

        $usuario = $this->Usuario->find('first', array(
            'conditions' => array(
                'Usuario.email' => $email,
                'Usuario.activation_key' => $key,
            )
                ));
        if (!isset($usuario['Usuario']['id'])) {
            $this->Session->setFlash('Paramentros inválidos.', 'flash/error');
            $this->redirect(array('action' => 'login'));
        }

        if (!empty($this->data)) {
            if (!empty($this->data['Usuario']['senha'])) {
                $this->Usuario->id = $usuario['Usuario']['id'];
                $usuario['Usuario']['senha'] = Security::hash($this->data['Usuario']['senha']);
                $usuario['Usuario']['activation_key'] = md5(uniqid());

                if ($this->Usuario->save($usuario, false, array('senha', 'activation_key'))) {
                    $this->Session->setFlash('Sua senha foi alterada com sucesso!', 'flash/success');
                    $this->redirect(array('action' => 'login'));
                } else {
                    $this->Session->setFlash('Ocorreu um erro ao tentar alterar a senha, tente novamente.', 'flash/error');
                }
            } elseif (strlen($this->data['Usuario']['senha']) < 6 || strlen($this->data['Usuario']['senha']) > 12) {
                $this->Usuario->invalidate('senha', 'Sua senha deve possuir entre 6 e 12 caracteres');
            } else {

                $this->Usuario->invalidate('senha', 'Campo de preenchimento obrigatório');
            }
        }

        $this->set(compact('usuario', 'email', 'key'));
    }
	
	public function integracaoDinamize($data, $group){
		
		//variaveis
		if(isset($data['Usuario']['nome']) && $data['Usuario']['nome'] != ""){
			$itg['SMT_nome'] = $data['Usuario']['nome'];
		}
		if(isset($data['Usuario']['sexo']) && $data['Usuario']['sexo'] != ""){
			$itg['SMT_sexo'] = $data['Usuario']['sexo'];
		}
		if(isset($data['Usuario']['email']) && $data['Usuario']['email'] != ""){
			$itg['SMT_email'] = $data['Usuario']['email'];
		}
		if(isset($data['Usuario']['telefone']) && $data['Usuario']['telefone'] != ""){
			$itg['SMT_telefone_residencial'] = $data['Usuario']['telefone'];
		}
		if(isset($data['Usuario']['celular']) && $data['Usuario']['celular'] != ""){
			$itg['SMT_telefone_celular'] = $data['Usuario']['celular'];
		}
		if(isset($data['Usuario']['cnpj']) && $data['Usuario']['cnpj'] != ""){
			$itg['SMT_cnpj'] = $data['Usuario']['cnpj'];
		}
		if(isset($data['Usuario']['cpf']) && $data['Usuario']['cpf'] != ""){
			$itg['SMT_cpf'] = $data['Usuario']['cpf'];
		}
		if(isset($data['Usuario']['nome_fantasia']) && $data['Usuario']['nome_fantasia'] != ""){
			$itg['SMT_nome_fantasia'] = $data['Usuario']['nome_fantasia'];
		}
		if(isset($data['Usuario']['razao_social']) && $data['Usuario']['razao_social'] != ""){
			$itg['SMT_razao_social'] = $data['Usuario']['razao_social'];
		}
		if(isset($data['Usuario']['tipo_pessoa']) && $data['Usuario']['tipo_pessoa'] != ""){
			$itg['SMT_tipo_pf_ou_pj'] = $data['Usuario']['tipo_pessoa'];
		}
		if(isset($data['UsuarioEndereco']['endereco']) && $data['UsuarioEndereco']['endereco'] != ""){
			$itg['SMT_endereco'] = $data['UsuarioEndereco']['endereco'];
		}
		if(isset($data['UsuarioEndereco']['complemento']) && $data['UsuarioEndereco']['complemento'] != ""){
			$itg['SMT_complemento'] = $data['UsuarioEndereco']['complemento'];
		}
		if(isset($data['UsuarioEndereco']['bairro']) && $data['UsuarioEndereco']['bairro'] != ""){
			$itg['SMT_bairro'] = $data['UsuarioEndereco']['bairro'];
		}
		if(isset($data['UsuarioEndereco']['cep']) && $data['UsuarioEndereco']['cep'] != ""){
			$itg['SMT_cep'] = $data['UsuarioEndereco']['cep'];
		}
		if(isset($data['UsuarioEndereco']['estado']) && $data['UsuarioEndereco']['estado'] != ""){
			$itg['SMT_estado'] = $data['UsuarioEndereco']['estado'];
		}
		if(isset($data['UsuarioEndereco']['cidade']) && $data['UsuarioEndereco']['cidade'] != ""){
			$itg['SMT_cidade'] = $data['UsuarioEndereco']['cidade'];
		}
		
		if($group != ""){
			$this->DinamizeIntegracao->setGroup($group);
			$this->DinamizeIntegracao->setVar($itg);
			$this->DinamizeIntegracao->register();
		}
		
	}

    public function admin_exportar($conditions) {

        //$filtros_usados = $this->Filter->getStoredData();
        App::import('Helper', 'Calendario');
        $this->Calendario = new CalendarioHelper();

        $rows = $this->Usuario->find('all', array(
            'recursive' => 1,
            'conditions' => $conditions
                )
        );
        $table = "<table>";
        $table .= "
				<tr bgcolor=\"#CECECE\">
					<td><strong>Id</strong></td>
					<td><strong>Nome</strong></td>
					<td><strong>Apelido</strong></td>
					<td><strong>E-mail</strong></td>
					<td><strong>Data de Nascimento</strong></td>
					<td><strong>Tipo de Pessoa</strong></td>
					<td><strong>Sexo</strong></td>
					<td><strong>RG</strong></td>
					<td><strong>CPF</strong></td>
					<td><strong>CNPJ</strong></td>
					<td><strong>Nome Fantasia</strong></td>
					<td><strong>Razão Social</strong></td>
					<td><strong>Inscrição Estadual</strong></td>
					<td><strong>Telefone</strong></td>
					<td><strong>Celular</strong></td>
					<td><strong>Status</strong></td>
					<td><strong>Grupo</strong></td>
					<td><strong>Criado</strong></td>
					<td><strong>Editado</strong></td>					
				</tr>";
        foreach ($rows as $row) {
            $status = ( $row['Usuario']['status'] ) ? "Ativo" : "Inativo";
            $table .= "
				<tr>
					<td>" . $row['Usuario']['id'] . "</td>
					<td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Usuario']['nome']) . "</td>
					<td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Usuario']['apelido']) . "</td>
					<td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Usuario']['email']) . "</td>
					<td>" . $row['Usuario']['data_nascimento'] . "</td>
					<td>" . $row['Usuario']['tipo_pessoa'] . "</td>
					<td>" . $row['Usuario']['sexo'] . "</td>
					<td>" . $row['Usuario']['rg'] . "</td>
					<td>" . $row['Usuario']['cpf'] . "</td>
					<td>" . $row['Usuario']['cnpj'] . "</td>
					<td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Usuario']['nome_fantasia']) . "</td>
					<td>" . $row['Usuario']['razao_social'] . "</td>
					<td>" . $row['Usuario']['inscricao_estadual'] . "</td>
					<td>" . $row['Usuario']['telefone'] . "</td>
					<td>" . $row['Usuario']['celular'] . "</td>
					<td>" . $status . "</td>
					<td>" . $row['Grupo']['nome'] . "</td>
					<td>" . $this->Calendario->DataFormatada("d-m-Y", $row['Usuario']['created']) . "</td>
					<td>" . $this->Calendario->DataFormatada("d-m-Y", $row['Usuario']['modified']) . "</td>
				</tr>";
        }
        $table .= "</table>";

        App::import("helper", "String");
        $this->String = new StringHelper();
        $this->layout = false;
        $this->render(false);
        set_time_limit(0);
        header('Content-type: application/x-msexcel');
        $filename = "usuarios_" . date("d_m_Y_H_i_s");
        header('Content-Disposition: attachment; filename=' . $filename . '.xls');
        header('Pragma: no-cache');
        header('Expires: 0');

        die($table);
    }

}

?>
