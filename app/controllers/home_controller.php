<?php

class HomeController extends AppController {

    public $uses = array('Vitrine');
    //var $components = array('DinamizeIntegracao');
     var $cacheAction = array(
         'index' => '1 hour'
     );
	public $helpers = array('String','Image','Parcelamento','Javascript','Calendario');
    function index() {

		// retorna as vitrines disponiveis
		//vitrines destaque
		if (Cache::read('vitrines_destaque') === false) {
			$vitrines_destaque = $this->Vitrine->BuscaVitrines(true,'DESTAQUE');
			Cache::write('vitrines_destaque', $vitrines_destaque);
		}else{
			$vitrines_destaque = Cache::read('vitrines_destaque');
		}

		//vitrines normal
		if (Cache::read('vitrines_normal') === false) {
			$vitrines_normal = $this->Vitrine->BuscaVitrines(true,'NORMAL');
			Cache::write('vitrines_normal', $vitrines_normal);
		}else{
			$vitrines_normal = Cache::read('vitrines_normal');
		}

		//begin mais vendidos
		if (Cache::read('mais_vendidos') === false) {
			App::import('Model','Produto');
			$this->Produto = new Produto();
			$mais_vendidos = $this->Produto->find('all',
						array(
							'recursive' => -1,
							'limit' 	=> 10,
							'order' 	=> 'quantidade_vendidos DESC',
							'conditions' => array('Produto.status >' => 0, 'Produto.parent_id' => 0),
							'fields'	=> array('Produto.status,Produto.id,Produto.nome,Produto.preco,Produto.preco_promocao,Produto.quantidade_disponivel,Produto.preco_promocao_inicio,Produto.preco_promocao_fim,Produto.preco_promocao_novo,Produto.preco_promocao_velho,
							(SELECT concat("uploads/produto_imagem/filename/",ProdutoImagem.filename) FROM produto_imagens ProdutoImagem WHERE ProdutoImagem.produto_id = Produto.id LIMIT 1) as imagem')));
			Cache::write('mais_vendidos', $mais_vendidos);
		}else{
			$mais_vendidos = Cache::read('mais_vendidos');
		}

		//begin fabricantes
		App::import('Model','Fabricante');
		$this->Fabricante = new Fabricante();
		$fabricantes = $this->Fabricante->find('all', array('conditions' => array('Fabricante.status' => true) ));
		//end fabricantes

		$this->set('fabricantes', $fabricantes);
        $this->set('mais_vendidos', $mais_vendidos);
		$this->set('vitrines_destaque',$vitrines_destaque);
        $this->set('vitrines_normal', $vitrines_normal);

		//end mais vendidos
		//debug($mais_vendidos);
    }

	function admin_index() {
		$this->loadModel('Pedido');
		$this->loadModel('Usuario');
		$this->loadModel('Produto');
		$this->loadModel('ProdutoRating');
		$usuarios_compradores = $this->Usuario->find('all',array('recursive'=>-1,'fields'=>array('Usuario.*,(SELECT COUNT(usuario_id) FROM pedidos WHERE pedidos.usuario_id = Usuario.id) AS compradores'),'limit'=>10,'conditions'=>array('(SELECT COUNT(usuario_id) FROM pedidos WHERE pedidos.usuario_id = Usuario.id) > '=>0),'order'=>array('compradores DESC')));
		$this->set('usuarios_compradores', $usuarios_compradores);

		$pedidos = $this->Pedido->find('all',array('fields'=>array('Pedido.valor_frete,Pedido.valor_pedido,Pedido.parcelas,Pedido.created,Pedido.pedido_status_id,Pedido.entrega_tipo,Pedido.id,Pedido.entrega_prazo,PedidoStatus.nome,PedidoStatus.cor,Usuario.nome,Usuario.id,PagamentoCondicao.nome'),
		'joins' => array(

		array('table' => 'usuarios', 'type' => 'LEFT', 'alias' => 'Usuario', 'conditions' => array("Pedido.usuario_id=Usuario.id")),
		array('table' => 'pagamento_condicoes', 'type' => 'LEFT', 'alias' => 'PagamentoCondicao ', 'conditions' => array("Pedido.pagamento_condicao_id=PagamentoCondicao.id")),
		array('table' => 'pedido_status', 'type' => 'LEFT', 'alias' => 'PedidoStatus', 'conditions' => array("Pedido.pedido_status_id=PedidoStatus.id"))
		),

		'recursive'=>-1,'limit'=>10,'order'=>array('Pedido.id DESC')));

		$this->set('pedidos', $pedidos);
		App::import('Helper', 'Calendario');
		$this->Calendario = new CalendarioHelper();
		$this->set('pedidostotais', $this->Pedido->find('all',array('recursive'=>-1,'fields'=>array('Pedido.valor_frete,Pedido.valor_pedido,Pedido.parcelas,Pedido.created,Pedido.pedido_status_id,Pedido.entrega_tipo,Pedido.id,Pedido.entrega_prazo'),'joins' => array(array('table' => 'pedido_itens', 'type' => 'INNER', 'alias' => 'PedidoItem', 'conditions' => array('AND'=>array("DATE_FORMAT(Pedido.created,'%Y-%m-%d') >= '" . $this->Calendario->DataFormatada("Y-m-d", date('d/m/Y',mktime(0,0,0,date('m')-2,date('d'),date('Y')))) . "'","DATE_FORMAT(Pedido.created,'%Y-%m-%d') <= '" . $this->Calendario->DataFormatada("Y-m-d", date('d/m/Y',mktime(0,0,0,date('m'),date('d'),date('Y')))) . "'",'Pedido.id = PedidoItem.pedido_id')))))));

		$this->set('usuarios', $this->Usuario->find('all',array('fields'=>array('Usuario.nome','Usuario.id','Usuario.email','Usuario.nome','Usuario.cpf','Usuario.cnpj','Usuario.status','Usuario.created','Usuario.modified','Usuario.telefone'),'limit'=>10,'order'=>array('Usuario.id DESC'))));

		$produtos = $this->Produto->find('all',array('recursive'=>-1,'fields'=>array('Produto.id,Produto.sku,Produto.nome,(SELECT COUNT(produto_id) FROM pedido_itens WHERE pedido_itens.produto_id = Produto.id) AS mais_comprados'),'limit'=>10,'order'=>array('mais_comprados DESC')));

		$this->set('ratings', $this->ProdutoRating->find('all',array('contain'=>array('Produto','Usuario'),'fields'=>array('Produto.id','Produto.nome','Usuario.id','Usuario.nome','ProdutoRating.id','ProdutoRating.comentario','ProdutoRating.nota','ProdutoRating.status','ProdutoRating.created','ProdutoRating.modified'),'limit'=>10,'order'=>array('ProdutoRating.id DESC'))));

		$this->set('produtos', $produtos);
		App::import('Helper', 'Calendario');
		$this->Calendario = new CalendarioHelper();

		$mes_1 = date('Y-m',mktime(0,0,0,date('m'),date('d'),date('Y')));
		$mes_2 = date('Y-m',mktime(0,0,0,date('m')-1,date('d'),date('Y')));
		$mes_3 = date('Y-m',mktime(0,0,0,date('m')-2,date('d'),date('Y')));
		$pedido_status = $this->Pedido->PedidoStatus->find('all',array('recursive'=>-1));
		$query_mes = '';
		foreach($pedido_status as $status){
			$status['PedidoStatus']['nome'] = up(Inflector::slug($status['PedidoStatus']['nome'],'_'));
			$query_mes[] = "(select count(1) from pedidos pn where pv.created >= '{$mes_1}-1' and pv.created <= '{$mes_1}-31' and pn.created >= '{$mes_1}-1' and pn.created <= '{$mes_1}-31' and pn.pedido_status_id = {$status['PedidoStatus']['id']} limit 1) {$status['PedidoStatus']['nome']}";
		}

		$query_mes = implode(',',$query_mes);

		$this->set('mes_1_data',$mes_1);
		$this->set('mes_1_dados',$this->Pedido->query("select {$query_mes} from pedidos pv where pv.created >= '{$mes_1}-1' AND  pv.created <= '{$mes_1}-31' limit 1 "));
		$this->set('mes_2_data',$mes_2);
		$this->set('mes_2_dados',$this->Pedido->query("select {$query_mes} from pedidos pv where pv.created >= '{$mes_2}-1' AND  pv.created <= '{$mes_2}-31' limit 1  "));
		$this->set('mes_3_data',$mes_3);
		$this->set('mes_3_dados',$this->Pedido->query("select {$query_mes} from pedidos pv where pv.created >= '{$mes_3}-1' AND  pv.created <= '{$mes_3}-31' limit 1 "));

		$pedidos = $this->Pedido->find('all',array('recursive'=>-1,'fields'=>array('Pedido.valor_frete,Pedido.valor_pedido,Pedido.parcelas,Pedido.created,Pedido.pedido_status_id,Pedido.entrega_tipo,Pedido.id,Pedido.entrega_prazo'),'joins' => array(array('table' => 'pedido_itens', 'type' => 'INNER', 'alias' => 'PedidoItem', 'conditions' => array('Pedido.id = PedidoItem.pedido_id')))));

		$this->set('pedidostatus', $pedido_status);

        $this->loadModel('ImportacaoLog');

        $ultima_importacao = $this->ImportacaoLog->find('first',
        array(
            'order' => array('ImportacaoLog.id DESC')
        ));

        $this->set('ultima_importacao', $ultima_importacao);

	}

	function newsletter() {
		//import model
		App::import("Model", "Newsletter");
		$this->Newsletter = new Newsletter();
		//define debug como 0 para retornar o json
		Configure::write('debug', 0);
		if ($this->Newsletter->save($this->data)) {
			$this->data  = array();
			//importamos o helper html para o redirecionamento
			App::import('Helper', 'Html');
			$html = new HtmlHelper();
			//redirecionamos o usuario quando ocorrer sucesso.
			die(json_encode(array('success' => $html->url("/", true))));
		} else {
			//retorna os campos inv�lidos
			die(json_encode(array('error' => $this->Newsletter->invalidFields())));
		}
		$this->autoRender = false;
		exit();
    }
}

?>