<?php
class LinguagensController extends AppController {

	var $name = 'Linguagens';
	var $components = array('Session','Filter');
	var $helpers = array('Calendario','String','Image','Flash','Javascript');
	
	function admin_index() {
		//filters
		$filtros = array();
        if (isset($this->data["Filter"]["nome"])) {
            $filtros['marca'] = "Linguagen.nome LIKE '%{%value%}%'";
        }
		
        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();
		
		if(isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar"){
			$this->admin_exportar($conditions);
		}
		
		$this->Linguagem->recursive = 0;
		$this->set('linguagens', $this->paginate($conditions));
	}
	public function admin_exportar($conditions){
		
		App::import('Helper', 'Calendario');
		$this->Calendario = new CalendarioHelper();
		
		$rows = $this->Linguagem->find('all',array('conditions' => $conditions));
		
		$table = "<table>";
		$table .= "
				<tr bgcolor=\"#CECECE\">
					<td><strong>Id</strong></td>
					<td><strong>Nome</strong></td>
					<td><strong>Codigo</strong></td>
					<td><strong>Status</strong></td>
					<td><strong>Criado</strong></td>
					<td><strong>Modificado</strong></td>
				</tr>";
		foreach ($rows as $row) {
			$status = ( $row['Linguagem']['status'] ) ? "Ativo" : "Inativo";
			$table .= "
				<tr>
					<td>".$row['Linguagem']['id']."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Linguagem']['nome'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Linguagem']['codigo'])."</td>
					<td>".$status."</td>
					<td>".$this->Calendario->DataFormatada("d-m-Y",  $row['Linguagem']['created'])."</td>
					<td>".$this->Calendario->DataFormatada("d-m-Y",  $row['Linguagem']['modified'])."</td>
				</tr>";
		}
		$table .= "</table>";
		
		App::import("helper", "String");
		$this->String = new StringHelper();
		$this->layout = false;
		$this->render(false);
		set_time_limit(0);		
		header('Content-type: application/x-msexcel');
		$filename = "linguagens_" . date("d_m_Y_H_i_s");
		header('Content-Disposition: attachment; filename='.$filename.'.xls');
		header('Pragma: no-cache');
		header('Expires: 0');
		
		die($table);
	}
	function admin_add() {
		if (!empty($this->data)) {
		
			$this->Linguagem->create();
            
			if ($this->Linguagem->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash('Parâmetro inválidos','flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			$this->data['Linguagem']['id'] = $id;
			$this->Linguagem->id = $id;
                 
			if ($this->Linguagem->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Linguagem->read(null, $id);
		}
	}
	function admin_delete($id = null) {
		if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Linguagem->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}
	
}
?>