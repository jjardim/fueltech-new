<?php

class SettingsController extends AppController {
/**
 * Controller name
 *
 * @var string
 * @access public
 */
    var $name = 'Settings';
/**
 * Models used by the Controller
 *
 * @var array
 * @access public
 */
    var $uses = array('Setting');
/**
 * Helpers used by the Controller
 *
 * @var array
 * @access public
 */
     var $helpers = array('String','Javascript');

    function admin_index() {
        $this->Setting->recursive = 0;
        $this->paginate['Setting']['order'] = "Setting.weight ASC";
        if (isset($this->params['named']['p'])) {
            $this->paginate['Setting']['conditions'] = "Setting.key LIKE '". $this->params['named']['p'] ."%'";
        }
        $this->set('settings', $this->paginate());
    }

    function admin_view($id = null) {
        if (!$id) {
             $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action'=>'index'));
        }
        $this->set('setting', $this->Setting->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Setting->create();
            if ($this->Setting->save($this->data)) {
               	$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
    }

    function admin_edit($id = null) {

        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action'=>'index'));
        }
        if (!empty($this->data)) {
            if ($this->Setting->save($this->data)) {
               	$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action'=>'index'));
            } else {
             	$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Setting->read(null, $id);
        }
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash('parametro inválido', 'flash/error');
            $this->redirect(array('action'=>'index'));
        }
        
        if ($this->Setting->delete($id)) {
            $this->Session->setFlash('Setting deletado com sucesso', 'flash/success');
            $this->redirect(array('action'=>'index'));
        }
    }

    function admin_prefix($prefix = null) {
        $this->set('title_for_layout', sprintf(__('Settings: %s', true), $prefix));
        if(!empty($this->data) && $this->Setting->saveAll($this->data['Setting'])) {
            $this->Session->setFlash('Dados salvos com sucesso!', 'flash/success');
        }

        $settings = $this->Setting->find('all', array(
            'order' => 'Setting.weight ASC',
            'conditions' => array(
                'Setting.key LIKE' => $prefix . '.%',
                'Setting.editable' => 1,
            ),
        ));
        $this->set(compact('settings'));
        if( count($settings) == 0 ) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
        }

        $this->set("prefix", $prefix);
    }

    function admin_moveup($id, $step = 1) {
        if( $this->Setting->moveup($id, $step) ) {
            $this->Session->setFlash('Movido com sucesso', 'flash/success');
        } else {
            $this->Session->setFlash('Ocorreu um erro ao tentar mover', 'flash/error');
        }

        $this->redirect(array('admin' => true, 'controller' => 'settings', 'action' => 'index'));
    }

    function admin_movedown($id, $step = 1) {
        if( $this->Setting->movedown($id, $step) ) {
            $this->Session->setFlash('Movido com sucesso', 'flash/success');
        } else {
            $this->Session->setFlash('Ocorreu um erro ao tentar mover','flash/error');
        }

        $this->redirect(array('admin' => true, 'controller' => 'settings', 'action' => 'index'));
    }
	
	public function limpar_cache(){
		$fullpath = ROOT . DS . APP_DIR . DS . 'tmp' . DS . 'cache' . DS;
		$dir[] = $fullpath;
		$dir[] = $fullpath.'assets' . DS;
		$dir[] = $fullpath.'models' . DS;
		$dir[] = $fullpath.'persistent' . DS;
		$dir[] = $fullpath.'views' . DS;
		
		foreach($dir as $dir){
			if(is_dir($dir)){
				if($handle = opendir($dir)){
					while(($file = readdir($handle)) !== false){
						if($file != 'empty' && is_file($dir.$file)){							
							if(unlink($dir.$file)){
								print('Apagado: ' . $dir.$file.'<br />');
							}
						}
					}
				}
			}
			else{
				print("Erro ao abrir dir: $dir");
				continue;				
			}
		}
		
		$this->layout = '';
		$this->render(false);
		die();
	}
}
?>