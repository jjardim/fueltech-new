<?php

class BuscaController extends AppController {

    public $uses = array('Produto');
    var $helpers = array('Parcelamento', 'String', 'Image', 'Javascript');

    /**
     * Realiza as buscas dos produtos pelo form de busca do site
     */
    function index() {

	if (strpos($this->params['url']['url'], "marca/index") !== false) {
	    $is_marca = true;
	} else {
	    $is_marca = false;
	}

	    $this->params['url']['busca'] = $this->params['pass'][0];
	    $conditions                   = array(
		    'Produto.status >'          => 0,
		    'ProdutoDescricao.language' => Configure::read('Config.language')
	    );
	    $conditions_fabricante        = array();
	    //verifica se a busca é pelo form de busca = variaval setada no routes

	    if ($is_marca) {
		    if (isset($this->params['pass'][0]) && isset($this->params['pass'][1])) {
			    $conditions = array_merge($conditions, array("AND" => array("Produto.fabricante_id" => $this->params['pass'][0])));
			    //$this->params['url']['busca'] = $this->params['pass'][1];
			    $this->data['Busca']['busca'] = $this->params['pass'][1];
		    }
	    } elseif (isset($this->params['url']['busca'])) {
		    //se possuir o parametro todos retira o conditions e adiciona um conditions para todos os produtos
		    if (isset($this->params['url']['busca']) && !empty($this->params['url']['busca']) && $this->params['url']['busca'] != "todos") {
			    $conditions = array_merge($conditions, array(
					    "OR" => array(
						    "MATCH(Produto.nome, Produto.tag) AGAINST('" . addslashes($this->params['url']['busca']) . "' IN BOOLEAN MODE)",
						    "Produto.nome LIKE '%" . addslashes($this->params['url']['busca']) . "%'  OR Produto.sku LIKE '%" . addslashes($this->params['url']['busca']) . "%'  OR ProdutoDescricao.tag LIKE '%" . addslashes($this->params['url']['busca']) . "%' "
					    )
				    )
			    );
		    } else if (isset($this->params['url']['busca']) && !empty($this->params['url']['busca']) && $this->params['url']['busca'] == "todos") {
			    $conditions = array_merge($conditions, array("OR" => array("Produto.nome LIKE" => '%%')));
		    }
		    //die(var_dump($conditions));
		    if (isset($this->params['url']['busca']) && $this->params['url']['busca'] != 'todos') {
			    $this->params['slug']         = $this->params['url']['busca'];
			    $this->data['Busca']['busca'] = $this->params['url']['busca'];
		    } elseif (isset($this->params['url']['busca']) && $this->params['url']['busca'] == 'todos') {
			    $this->params['slug'] = $this->params['url']['busca'];
		    }
	    } else {
		    $this->redirect("/");
	    }


	//se nao tiver paramentros entro direto, redirecionado para home
	if (count($conditions) <= 0)
	    $this->redirect("/");
	$joins = array(
	    array(
		'table' => 'produtos_categorias',
		'alias' => 'Categorias',
		'type' => 'left',
		'conditions' =>
		array(
		    'Categorias.produto_id = Produto.id'
		)
	    ), array(
		'table' => 'categorias',
		'alias' => 'Categoria',
		'type' => 'left',
		'conditions' =>
		array(
		    'Categorias.categoria_id = Categoria.id'
		)
	    ), array(
		'table' => 'produto_descricoes',
		'alias' => 'ProdutoDescricao',
		'type' => 'left',
		'conditions' => array(
		    'ProdutoDescricao.produto_id = Produto.id'
		)
	    )
	);

	$produtos_ids = $this->Produto->find("all", array(
	    'fields' => 'Produto.id',
	    'recursive' => -1,
	    'conditions' => $conditions,
	    'joins' => array(
		array(
		    'table' => 'produto_descricoes',
		    'alias' => 'ProdutoDescricao',
		    'type' => 'left',
		    'conditions' => array('ProdutoDescricao.produto_id = Produto.id')
		)
	    )
		)
	);
	$produtos_ids = Set::extract('{n}./Produto/id', $produtos_ids);

	$produtos_ids_com_atributo = $this->carregaFiltrosBusca($produtos_ids);

	//se há filtro de atributos, acrescento a condition com ids dos produtos
	if (isset($this->params['named']['atributos']) && !empty($this->params['named']['atributos'])) {
	    $conditions['Produto.id'] = explode(',', $produtos_ids_com_atributo);

	    App::import('Model', 'Atributo');
	    $this->Atributo = new Atributo();
	    $atributo_tamanho = false;
	    $filtros_ids = json_decode($this->params['named']['atributos']);
	    foreach ($filtros_ids as $filtros_id) {
		$atributo = $this->Atributo->find('first', array('recursive' => 1, 'contain' => 'AtributoTipo', 'conditions' => array('Atributo.id' => $filtros_id)));
		if ($atributo['AtributoTipo']['nome'] == 'Tamanho') {
		    $atributo_tamanho = true;
		}
	    }
//	    if ($atributo_tamanho != true) {
//		$conditions = array_merge(
//			array('Produto.parent_id' => 0), $conditions
//		);
//	    }
	} /*else {
	    $conditions = array_merge(
		    array('Produto.parent_id' => 0), $conditions
	    );
	}*/

	//debug($conditions);die;

	$this->paginate = array(
	    'limit' => 48,
	    //'group' => 'Produto.id',
	    'order' => array('Produto.ordem' => 'ASC', 'rating' => 'DESC', 'quantidade_disponivel' => 'DESC', 'quantidade_acessos' => 'DESC'),
	    //'recursive'	=>	-1,
	    'joins' => $joins,
	    'contain' => array('ProdutoDescricao'),
	    'fields' => array('(SELECT IF(FreteGratis.label != "",FreteGratis.label,"") FROM frete_gratis FreteGratis WHERE (FreteGratis.categoria_id LIKE concat("%\"",Categorias.categoria_id,"\"%") OR FreteGratis.produto_id LIKE concat("%\"",Categorias.produto_id,"\"%") ) AND ( FreteGratis.status = true AND IF(Produto.preco_promocao > 0,Produto.preco_promocao,Produto.preco) >= FreteGratis.apartir_de AND FreteGratis.data_inicio <= now() AND FreteGratis.data_fim >= now() ) LIMIT 1) as frete_gratis,Produto.id,ProdutoDescricao.nome,Produto.preco,Produto.preco_promocao,Produto.quantidade_disponivel,Produto.status,Produto.preco_promocao_inicio,Produto.preco_promocao_fim,Produto.preco_promocao_novo,Produto.preco_promocao_velho, Produto.status,Categoria.nome,Categoria.seo_url ,(SELECT concat("uploads/produto_imagem/filename/",ProdutoImagem.filename) FROM produto_imagens as ProdutoImagem WHERE ProdutoImagem.produto_id = Produto.id LIMIT 1) as imagem,(MATCH (Produto.nome,Produto.tag) AGAINST ("' . addslashes($this->params['url']['busca']) . '" IN BOOLEAN MODE)) AS rating')
	);
	$produtos = $this->paginate('Produto', $conditions);
	$this->set('produtos', $produtos);

	//debug($conditions);
	//begin salvo o registro da busca no banco
	if (!$is_marca) {
	    App::import("Model", "Busca");
	    $this->Busca = new Busca();
	    $dados_busca = array();
	    $dados_busca['id'] = '';
	    $dados_busca['quantidade_itens'] = $this->params['paging']['Produto']['count'];
	    $dados_busca['palavra_chave'] = trim($this->params['pass'][0]);
	    if ($dados_busca['quantidade_itens'] > 0) {
		$this->Busca->salvar($dados_busca);
	    }

	    $this->set('busca_google', $dados_busca);
	}
	//end salvo o registro da busca no banco

	if ($this->params['url']['busca'] == "todos" && $cat['Categoria']['nome'] != "") {
	    $this->set('seo_title', addslashes($cat['Categoria']['nome']) . ' na ' . Configure::read('Loja.nome') . '.com.br');
	} else {
	    $this->set('seo_title', addslashes($this->params['url']['busca']) . ' na ' . Configure::read('Loja.nome') . '.com.br');
	}
	//start breadcrumb
	$breadcrumb = "";
	if (!$is_marca) {
	    $breadcrumb[] = array('url' => 'javascript:void(0);', 'nome' => "Resultado de Busca: ");
	} else {
	    $breadcrumb[] = array('url' => 'javascript:void(0);', 'nome' => "Marca");
	}
	$breadcrumb[] = array('url' => 'javascript:void(0);', 'nome' => $this->data['Busca']['busca']);

	$this->set('breadcrumb', $breadcrumb);
	//end breadcrumb
	//begin historico de navegação
	//salvo a visita do cliente, e salvo na sessao
	$historico = $this->Session->read("Historico.navegacao");
	$sessao_atual = array('termo' => $this->data['Busca']['busca']);
	if (isset($historico['Buscas'])) {
	    foreach ($historico['Buscas'] as $hp) {
		if ($hp['termo'] != $this->data['Busca']['busca']) {
		    $historico_tmp[] = $hp;
		}
	    }

	    $historico_tmp[] = $sessao_atual;
	} else {
	    $historico_tmp = array($sessao_atual);
	}
	$this->Session->write("Historico.navegacao.Buscas", $historico_tmp);
	//end historico de navegação
    }

    private function carregaFiltrosBusca($produtos_id) {

	//START: isso vai sair daki
	//menu de categorias do topo
	if (Cache::read('menu_topo') === false) {
	    if (!isset($menu_topo)) {
		$menu_topo = $this->Categoria->find('all', array('contain' => array('SubCategory'), 'conditions' => array('Categoria.parent_id' => 0)));
	    }

	    Cache::write('menu_topo', $menu_topo);
	} else {
	    $menu_topo = Cache::read('menu_topo');
	}

	$this->set('menu_topo', $menu_topo);
	$this->set('menu', array());
	//END: isso vai sair daki
	//se passar os produtos resultante da busca, faz o tratamento
	if (!empty($produtos_id)) {
	    //se nao tem filtro na url, busca todos os atributos de todos os produtos resultantes da busca
	    if (!isset($this->params['named']['atributos']) && empty($this->params['named']['atributos'])) {
		// $query = "
		// SELECT 
		// *
		// FROM 
		// atributo_tipos AS AtributoTipo, 
		// atributos AS Atributo 
		// INNER JOIN 
		// atributos_produtos AS AtributoProduto ON Atributo.id = AtributoProduto.atributo_id
		// WHERE
		// Atributo.filtro = true AND
		// AtributoTipo.id = Atributo.atributo_tipo_id AND
		// AtributoProduto.produto_id IN ( ".implode(',',$produtos_id)." )
		// GROUP BY
		// Atributo.id
		// ORDER BY
		// AtributoTipo.nome, Atributo.valor ASC
		// ";
		$query = "
						SELECT 
							Atributo.valor,
							AtributoTipo.nome,
							AtributoTipo.codigo,
							AtributoTipo.id,
							Atributo.id
						FROM 
							atributo_tipos AS AtributoTipo 
								LEFT JOIN atributos AS Atributo ON (Atributo.atributo_tipo_id = AtributoTipo.id) 
								LEFT JOIN atributos_produtos AS ProdutoAtributo ON (Atributo.id = ProdutoAtributo.atributo_id)
								LEFT JOIN produtos AS Produto ON (Produto.id = ProdutoAtributo.produto_id)
						WHERE 
							Atributo.filtro = TRUE 
							AND Produto.status > 0
							AND 
							(Produto.parent_id in (" . implode(",", $produtos_id) . ") OR Produto.id in (" . implode(",", $produtos_id) . "))
						GROUP BY
							Atributo.id
						ORDER BY
							Atributo.valor
						";
		$atributos = $this->Categoria->Query($query);
	    } else {

		//se tiver parametro na url, faco a busca dos atributos considerandos os produtos e atributos
		//capturo os filtros da url
		$filtros_ids = json_decode($this->params['named']['atributos']);
		$produtos_ids_com_atributo = "";
		//percorro os filtros
		foreach ($filtros_ids as $k => $v):

		    //no primeiro atributo do filtro, considero os produtos resultante da busca
		    if ($k == 0) {
			$produtos_id_cond = implode(',', $produtos_id);
		    } else {
			//a partir do primeiro, considero os produtos já filtrados pelo primeiro atributo
			$produtos_id_cond = $produtos_ids_com_atributo;
		    }

		    $query = "
								SELECT group_concat(AtributoProduto.produto_id) as produtos_ids
								FROM 
									atributos_produtos AS AtributoProduto 
								WHERE 
									AtributoProduto.atributo_id = " . $v . " AND
									AtributoProduto.produto_id IN ( " . $produtos_id_cond . " )
								";

		    $produtos_in = $this->Categoria->query($query);
		    $produtos_ids = $produtos_in[0][0]['produtos_ids'];
		    $produtos_ids_com_atributo = $produtos_ids;

		endForeach;

		//monto a query dos atributos
		$query = "	
							SELECT 
								*
							FROM 
									atributo_tipos AS AtributoTipo, 
									atributos AS Atributo 
										INNER JOIN 
									atributos_produtos AS AtributoProduto 
										ON Atributo.id = AtributoProduto.atributo_id
							WHERE
									Atributo.filtro = true AND
									AtributoTipo.id = Atributo.atributo_tipo_id AND
									AtributoProduto.produto_id IN ( " . $produtos_ids_com_atributo . " )
							GROUP BY
									Atributo.id
							ORDER BY
									AtributoTipo.nome, Atributo.valor ASC
						";

		//print('<hr />Query Atributos = '.$query);							

		$atributos = $this->Categoria->Query($query);

		//removo o filtro atual do array de atributos
		//alimento o array de filtros
		$filtros = array();
		foreach (json_decode($this->params['named']['atributos']) as $v) {
		    foreach ($atributos as $key => $atributo) {
			$k = array_search($v, $atributo['Atributo']);
			if ($k == 'id') {
			    $filtros[$key] = am($filtros, $atributos[$key]);
			    unset($atributos[$key]);
			}
		    }
		}

		//monto o menu de filtros selecionados
		$filtros_menu = array();
		foreach ($filtros as $linha) {

		    $filtros_menu[$linha['AtributoTipo']['id']]['nome_filtro'] = $linha['AtributoTipo']['nome'];
		    $filtros_menu[$linha['AtributoTipo']['id']]['codigo_filtro'] = $linha['AtributoTipo']['codigo'];
		    $filtros_menu[$linha['AtributoTipo']['id']]['id_filtro'] = $linha['Atributo']['id'];
		    $filtros_menu[$linha['AtributoTipo']['id']]['valor_filtro'] = $linha['Atributo']['valor'];
		}
		//set
		$this->set('filtros_selecionados', $filtros_menu);
	    }

	    //monto o menu de atributo
	    $atributos_menu = array();
	    foreach ($atributos as $linha) {
		$atributos_menu[$linha['AtributoTipo']['id']]['nome_filtro'] = $linha['AtributoTipo']['nome'];
		$atributos_menu[$linha['AtributoTipo']['id']]['codigo_filtro'] = $linha['AtributoTipo']['codigo'];
		$atributos_menu[$linha['AtributoTipo']['id']]['valor_filtro'][] = $linha['Atributo'];
	    }
	    $this->set('menu_filtro', $atributos_menu);

	    //retorno os produtos resultantes do filtro
	    if (isset($produtos_ids_com_atributo))
		return $produtos_ids_com_atributo;
	}
    }

}

?>
