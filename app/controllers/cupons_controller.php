<?php

class CuponsController extends AppController {

    var $name = 'Cupons';
    var $uses = array('Cupom');
    var $components = array('Session');
    var $helpers = array('Image','Calendario', 'String', 'Javascript');
	function beforeFilter(){
		parent::beforeFilter();
		//retira a validação do cupom para o site;
		$this->Cupom->validaCupomSite = false;		
	}
    function admin_index() {
        $this->Cupom->recursive = 0;
        $this->set('cupons', $this->paginate());
    }

    function admin_add() {
		
        if (!empty($this->data)) {
			$this->Cupom->set($this->data);
            if ($this->Cupom->save($this->data)) {              
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
			    $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
		$this->loadModel('Categoria');
		$this->loadModel('Produto');
		$this->Categoria->displayField = 'nome';
		$categorias = array('' => 'Selecione') + $this->Categoria->generatetreelist(null, null, null, '----');
		if(isset($this->data['Cupom']['produto_id'])){
			$produtos = array('' => 'Selecione') + $this->Produto->find('list',array('conditions'=>array('Produto.id'=>$this->data['Cupom']['produto_id']),'fields'=>array('id','nome')));
		}else{
			$produtos = array('' => 'Selecione');
		}
		$cupomTipos = array('' => 'Selecione') + $this->Cupom->CupomTipo->find('list',array('fields'=>array('id','nome')));
        $this->set(compact('categorias', 'produtos','cupomTipos'));		
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Paramentros inválidos', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
			$this->Cupom->set($this->data);
            if ($this->Cupom->save($this->data)) {
               $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Cupom->read(null, $id);
            if (!$this->data) {
                $this->redirect(array('action' => 'index'));
            }
        }
		$this->loadModel('Categoria');
		$this->loadModel('Produto');
		$this->Categoria->displayField = 'nome';
		$categorias = array('' => 'Selecione') + $this->Categoria->generatetreelist(null, null, null, '----');
		if(isset($this->data['Cupom']['produto_id'])){
			$produtos = array('' => 'Selecione') + $this->Produto->find('list',array('conditions'=>array('Produto.id'=>$this->data['Cupom']['produto_id']),'fields'=>array('id','nome')));
		}else{
			$produtos = array('' => 'Selecione');
		}
        $cupomTipos = array('' => 'Selecione') + $this->Cupom->CupomTipo->find('list',array('fields'=>array('id','nome')));
        $this->set(compact('categorias', 'produtos','cupomTipos'));		
    }
 
	function admin_delete($id = null) {
		if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Cupom->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}

}

?>