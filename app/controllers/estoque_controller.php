<?php
class EstoqueController extends AppController {
	var $components = array('Filter', 'Session');
	var $helpers = array('Calendario','String', 'Javascript');
	var $uses = array('Produto');
	var $name = 'Estoque';
	function admin_index() {
		
		$conditions=array();
		if (isset($this->data["Filter"]["filtro"])) {
			$filtros['filtro'] = "Produto.nome LIKE '%{%value%}%' OR Produto.codigo = '{%value%}' ";
		}else{
			$filtros['filtro'] = "Produto.nome LIKE '%{%value%}%' OR Produto.codigo = '{%value%}' ";
		}
		$this->Filter->setConditions($filtros);
		$this->Filter->check();
		$conditions = $this->Filter->getFilters();
		$this->Filter->setDataToView();
		
		if(isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar"){
			$this->admin_exportar($conditions);
		}
		
		$this->Produto->recursive = 0;
		$this->paginate = array(
				'fields' => 
					array(
						'Produto.id', 'Produto.sku', 'Produto.nome', 'Produto.quantidade','Produto.quantidade_disponivel','Produto.quantidade_alocada',
						'(SELECT SUM(pedido_itens.quantidade) FROM pedido_itens WHERE pedido_itens.produto_id =  Produto.id) as quantidade_comprada'));
		$this->set('produtos', $this->paginate($conditions));
	}
	
	public function admin_exportar($conditions){
	
		App::import('Helper', 'Calendario');
		$this->Calendario = new CalendarioHelper();
		
		$rows = $this->Produto->find('all',array(
										'recursive' => -1,
										'conditions' => $conditions, 
										'fields' => array(
											'Produto.id', 'Produto.sku', 'Produto.nome', 'Produto.quantidade','Produto.quantidade_disponivel','Produto.quantidade_alocada',
											'(SELECT SUM(pedido_itens.quantidade) FROM pedido_itens WHERE pedido_itens.produto_id =  Produto.id) as quantidade_comprada')
											)
									);
		
		$table = "<table>";
		$table .= "
				<tr bgcolor=\"#CECECE\">
					<td><strong>Id</strong></td>
					<td><strong>Sku</strong></td>
					<td><strong>Produto</strong></td>
					<td><strong>Quantidade</strong></td>
					<td><strong>Quantidade Alocada</strong></td>
					<td><strong>".iconv("UTF-8", "ISO-8859-1//IGNORE","Quantidade Disponível")."</strong></td>
					<td><strong>Quantidade Comprada</strong></td>
				</tr>";
		foreach ($rows as $row) {
			$table .= "
				<tr>
					<td>".$row['Produto']['id']."</td>
					<td>".$row['Produto']['sku']."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Produto']['nome'])."</td>
					<td>".$row['Produto']['quantidade']."</td>					
					<td>".$row['Produto']['quantidade_alocada']."</td>
					<td>".$row['Produto']['quantidade_disponivel']."</td>
					<td>".$row[0]['quantidade_comprada']."</td>
				</tr>";
		}
		$table .= "</table>";	
		
		App::import("helper", "String");
		$this->String = new StringHelper();
		$this->layout = false;
		$this->render(false);
		set_time_limit(0);		
		header('Content-type: application/x-msexcel');
		$filename = "estoque_" . date("d_m_Y_H_i_s");
		header('Content-Disposition: attachment; filename='.$filename.'.xls');
		header('Pragma: no-cache');
		header('Expires: 0');
		
		die($table);
	}
}
?>