<?php
class XmlVariaveisController extends AppController {

	var $name = 'XmlVariaveis';
	var $components = array('Session');
	var $helpers = array('Calendario','String','Flash','Javascript');
	
	function admin_index() {
		$this->XmlVariavel->recursive = 0;
		$this->set('xml_variaveis', $this->paginate());
	}

	function admin_add() {
		if (!empty($this->data)) {
		
			$this->XmlVariavel->create();
            
			if ($this->XmlVariavel->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
		
		//set XmlTipo
		App::import('Model', 'XmlTipo');
		$this->XmlTipo = new XmlTipo();
        $xml_tipos = array('' => 'Selecione') +$this->XmlTipo->find('list', array('conditions'=>array('XmlTipo.status'=>true),'fields' => array('id', 'nome')));
		$this->set(compact('xml_tipos'));
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash('Parâmetro inválidos','flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			$this->data['XmlVariavel']['id'] = $id;
			$this->XmlVariavel->id = $id;
                 
			if ($this->XmlVariavel->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
		if (empty($this->data)) {
			$this->data = $this->XmlVariavel->read(null, $id);
		}
		
		//set XmlTipo
		App::import('Model', 'XmlTipo');
		$this->XmlTipo = new XmlTipo();
        $xml_tipos = array('' => 'Selecione') +$this->XmlTipo->find('list', array('conditions'=>array('XmlTipo.status'=>true),'fields' => array('id', 'nome')));
		$this->set(compact('xml_tipos'));
	}
	function admin_delete($id = null) {
		if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->XmlVariavel->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}
	function admin_ajax_get_variaveis_xml(){
		$this->layout = "ajax";
		$xml_tipo_id = $this->params['form']['xml_tipo_id'];
		$variaveis = $this->XmlVariavel->find("all", array(
										'recursive'=>-1,
										'fields' => array('XmlVariavel.id','XmlVariavel.valor'),
										'conditions' => array('XmlVariavel.xml_tipo_id' => $xml_tipo_id),
										'order' => array('XmlVariavel.valor')
										)
							);
		$final = array();
		if($variaveis){
			foreach($variaveis as $variavel){
				$final[$variavel['XmlVariavel']['id']] = $variavel['XmlVariavel']['valor'];
			}
		}
		die(json_encode($final));
		
		//die(json_encode($variaveis));
	}
}
?>