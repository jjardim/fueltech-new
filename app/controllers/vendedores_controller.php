<?php
class VendedoresController extends AppController {

	var $name = 'Vendedores';
	var $components = array('Session','Filter');
	var $helpers = array('Calendario','String','Image','Flash','Javascript','Marcas','Estados');
	
	function admin_index() {
		//filters
		$filtros = array();
        if (isset($this->data["Filter"]["nome_email"])) {
            $filtros['nome_email'] = "Vendedor.nome LIKE '%{%value%}%' OR Vendedor.email LIKE '%{%value%}%'";
        }
		if (isset($this->data["Filter"]["estabelecimento"])) {
            $filtros['estabelecimento'] = "Vendedor.estabelecimento LIKE '%{%value%}%'";
        }
		if (isset($this->data["Filter"]["cidade_estado"])) {
            $filtros['cidade_estado'] = "Vendedor.cidade LIKE '%{%value%}%' OR Vendedor.estado LIKE '%{%value%}%'";
        }
		
        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();
		
		if(isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar"){
			$this->admin_exportar($conditions);
		}
		
		$this->Vendedor->recursive = 0;
		$this->set('vendedores', $this->paginate($conditions));
	}
	public function admin_exportar($conditions){
		
		App::import('Helper', 'Calendario');
		$this->Calendario = new CalendarioHelper();
		
		$rows = $this->Vendedor->find('all',array('conditions' => $conditions));
		
		$table = "<table>";
		$table .= "
				<tr bgcolor=\"#CECECE\">
					<td><strong>Id</strong></td>
					<td><strong>Nome</strong></td>
					<td><strong>E-mail</strong></td>
					<td><strong>Telefone</strong></td>
					<td><strong>Estabelecimento</strong></td>
					<td><strong>Cidade</strong></td>
					<td><strong>Estado</strong></td>
					<td><strong>Status</strong></td>
					<td><strong>Criado</strong></td>
					<td><strong>Modificado</strong></td>
				</tr>";
		foreach ($rows as $row) {
			$status = ( $row['Vendedor']['status'] ) ? "Ativo" : "Inativo";
			$table .= "
				<tr>
					<td>".$row['Vendedor']['id']."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Vendedor']['nome'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Vendedor']['email'])."</td>
					<td>".$row['Vendedor']['telefone']."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Vendedor']['estabelecimento'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Vendedor']['cidade'])."</td>
					<td>".$row['Vendedor']['estado']."</td>
					<td>".$status."</td>
					<td>".$this->Calendario->DataFormatada("d-m-Y",  $row['Vendedor']['created'])."</td>
					<td>".$this->Calendario->DataFormatada("d-m-Y",  $row['Vendedor']['modified'])."</td>
				</tr>";
		}
		$table .= "</table>";
		
		App::import("helper", "String");
		$this->String = new StringHelper();
		$this->layout = false;
		$this->render(false);
		set_time_limit(0);		
		header('Content-type: application/x-msexcel');
		$filename = "vendedores_" . date("d_m_Y_H_i_s");
		header('Content-Disposition: attachment; filename='.$filename.'.xls');
		header('Pragma: no-cache');
		header('Expires: 0');
		
		die($table);
	}
	function admin_add() {
		if (!empty($this->data)) {
		
			$this->Vendedor->create();
            
			if ($this->Vendedor->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash('Parâmetro inválidos','flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			$this->data['Vendedor']['id'] = $id;
			$this->Vendedor->id = $id;
                 
			if ($this->Vendedor->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Vendedor->read(null, $id);
		}
	}
	function admin_delete($id = null) {
		if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Vendedor->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}
	
	public function get_cidades($estado) {
		$this->render(false);
        $this->layout = false;
		$cidades = $this->Vendedor->find('all',array('fields' =>  array('Vendedor.cidade') ,'recursive'=>-1,'order' => array('Vendedor.cidade'), 'conditions'  =>  array('AND'=> array('Vendedor.status'=>true, 'Vendedor.estado' => $estado))));
		die(json_encode(Set::combine($cidades, '{n}/Vendedor/cidade', '{n}/Vendedor/cidade')));
    }
	
	public function busca($url_form, $estado = null, $cidade = null) {
		$this->layout = 'ajax';
		$pagina_element_content = "";
		
		$conditions = array('Vendedor.status'=>true);
		if( $estado != null )
			$conditions = array_merge($conditions, array("Vendedor.estado" => $estado));
		if( $cidade != null)
			$conditions = array_merge($conditions, array("AND" => array("Vendedor.cidade LIKE" => "%$cidade%" )));
		
		$pagina_element_content['vendedor'] = $this->Vendedor->find('all',array('fields' =>  array('Vendedor.id','Vendedor.nome','Vendedor.telefone','Vendedor.estabelecimento') ,'recursive'=>-1,'group' => 'Vendedor.email','conditions'  =>  $conditions ));
		
		$pagina_element_content['url_form'] = $url_form;
		$this->set('pagina_element_content', $pagina_element_content);
    }
	
}
?>