<?php
/**
 * Menus Controller
 *
 * PHP version 5
 *
 * @category Controller
 * @package  Croogo
 * @version  1.0
 * @author   Fahad Ibnay Heylaal <contact@fahad19.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class MenusController extends AppController {
/**
 * Controller name
 *
 * @var string
 * @access public
 */
    var $name = 'Menus';
/**
 * Models used by the Controller
 *
 * @var array
 * @access public
 */
    var $uses = array('Menu');
    var $helpers = array('Html', 'Javascript');

    function admin_index() {
        $this->set('title_for_layout', __('Menus', true));

        $this->Menu->recursive = 0;
        $this->paginate['Menu']['order'] = 'Menu.id ASC';
        $this->set('menus', $this->paginate());
    }

    function admin_add() {
        $this->set('title_for_layout', __('Add Menu', true));

        if (!empty($this->data)) {
            $this->Menu->create();
            if ($this->Menu->save($this->data)) {
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
    }

    function admin_edit($id = null) {
        $this->set('title_for_layout', __('Edit Menu', true));

        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Parametros inv�lidos', 'flash/error');
            $this->redirect(array('action'=>'index'));
        }
        if (!empty($this->data)) {
            if ($this->Menu->save($this->data)) {
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Menu->read(null, $id);
        }
    }
	function admin_delete($id = null) {
		if (!$id) {
            $this->Session->setFlash('Parametros inv�lidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Menu->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro n�o pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}
    

}
?>