<?php

class VideosController extends AppController {

    var $name = 'Videos';
    var $components = array('Session', 'Filter');
    var $helpers = array('Calendario', 'String', 'Image', 'Flash', 'Javascript', 'Marcas');

    function detalhe($id = false) {

        //id
        if (!$id) {
            $id = $this->params['id'];
        }

        //find
        $video_destaque = $this->Video->find("first", array(
            "fields" => array("Video.nome", "Video.descricao", "Video.video_youtube_id", "Video.created"),
            "conditions" => array("Video.id" => $id)
                )
        );

        //redirect
        if (!$video_destaque) {
            $this->redirect("/");
        }

        //set
        $this->set('video_destaque', $video_destaque);

        //mais videos
        App::import('Model', 'Video');
        $this->Video = new Video();
        $this->paginate = array(
            'limit' => 12,
            'conditions' => array("Video.status" => true, "Video.id <>" => $id),
            'order' => array("Video.created DESC")
        );

        $this->set('videos', $this->paginate('Video'));

        //noticias
        $this->set('pagina_atual_tipo', "Mundo FuelTech");
        $this->set('pagina_atual_nome', "Vídeos");
        $this->set('pagina_atual_url', "videos");
    }

    function admin_index() {
        $this->paginate = array('order' => array('Video.created' => 'desc'));
        
        //filters
        $filtros = array();

        if (isset($this->data["Filter"]["filtro"])) {
            $filtros['filtro'] = "Video.nome LIKE '%{%value%}%' OR Video.descricao LIKE '%{%value%}%'";
        }
        if (isset($this->data["Filter"]["video_tipo_id"])) {
            $filtros['video_tipo_id'] = "Video.video_tipo_id = '{%value%}'";
        }

        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();

        if (isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar") {
            $this->admin_exportar($conditions);
        }

        $this->Video->recursive = 0;
        $this->Video->contain('VideoTipo');
        $this->set('videos', $this->paginate($conditions));

        //video tipo
        App::import('Model', 'VideoTipo');
        $this->VideoTipo = new VideoTipo();
        $video_tipos = $this->VideoTipo->find('list', array('recursive' => -1, 'fields' => array('VideoTipo.id', 'VideoTipo.nome')));
        $this->set('video_tipos', $video_tipos);
    }

    public function admin_exportar($conditions) {

        App::import('Helper', 'Calendario');
        $this->Calendario = new CalendarioHelper();

        $rows = $this->Video->find('all', array('conditions' => $conditions));

        $table = "<table>";
        $table .= "
				<tr bgcolor=\"#CECECE\">
					<td><strong>Id</strong></td>
					<td><strong>Tipo</strong></td>
					<td><strong>Nome</strong></td>
					<td><strong>Descricao</strong></td>
					<td><strong>URL</strong></td>
					<td><strong>Criado</strong></td>
					<td><strong>Modificado</strong></td>
				</tr>";
        foreach ($rows as $row) {
            $status = ( $row['Video']['status'] ) ? "Ativo" : "Inativo";
            $table .= "
				<tr>
					<td>" . $row['Video']['id'] . "</td>
					<td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['VideoTipo']['nome']) . "</td>
					<td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Video']['nome']) . "</td>
					<td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Video']['descricao']) . "</td>
					<td>" . $row['Video']['url'] . "</td>
					<td>" . $status . "</td>
					<td>" . $this->Calendario->DataFormatada("d-m-Y", $row['Video']['created']) . "</td>
					<td>" . $this->Calendario->DataFormatada("d-m-Y", $row['Video']['modified']) . "</td>
				</tr>";
        }
        $table .= "</table>";

        App::import("helper", "String");
        $this->String = new StringHelper();
        $this->layout = false;
        $this->render(false);
        set_time_limit(0);
        header('Content-type: application/x-msexcel');
        $filename = "videos_" . date("d_m_Y_H_i_s");
        header('Content-Disposition: attachment; filename=' . $filename . '.xls');
        header('Pragma: no-cache');
        header('Expires: 0');

        die($table);
    }

    function admin_add() {
    
    	$this->Video->admin = true;
        if (!empty($this->data)) {

            $this->Video->create();

            // begin video youtube
            if (isset($this->data['Video']['url'])) {

                //capturo o id do video do yotube
                $video_youtube_id = $this->get_youtube_video_id($this->data['Video']['url']);

                $this->data['Video']['video_youtube_id'] = $video_youtube_id;

                //capturo as infos do video do youtube
                if ($video_youtube_id) {
                    $dados_video = $this->get_youtube_video_infos($video_youtube_id);
                }

                if (empty($this->data['Video']['descricao'])) {
                    $this->data['Video']['descricao'] = $dados_video->data->description;
                }

                if (empty($this->data['Video']['duracao'])) {
                    //capturo a duracao do video em H:m:s
                    $duration = $dados_video->data->duration;
                    $seconds = $duration;
                    $hours = floor($seconds / 3600);
                    $seconds -= $hours * 3600;
                    $minutes = floor($seconds / 60);
                    $seconds -= $minutes * 60;
                    $duracao = "$hours:$minutes:$seconds";

                    $this->data['Video']['duracao'] = $duracao;
                }
            }
            // end video youtube
				
            if ($this->Video->save($this->data)) {

                $data['Video']['id'] = $this->Video->id;

                if ($this->data['Video']['thumb_default'] == 1) {

                    //capturo a url do thumb do video
                    $url_imagem = $dados_video->data->thumbnail->hqDefault;

                    $dir = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'uploads' . DS . 'videos' . DS . 'thumb' . DS;
                    $nome_imagem = low(Inflector::slug($this->data['Video']['nome'], '-')) . '.jpg';
                    $destino = $dir . $nome_imagem;

                    $this->Video->Behaviors->detach('MeioUpload');
                    $data['Video']['thumb_filename'] = $nome_imagem;
                    $data['Video']['thumb_dir'] = 'uploads/videos/thumb';
                    $data['Video']['thumb_mimetype'] = 'image/jpeg';
                    $data['Video']['thumb_filesize'] = '13722';

                    $this->download_remote_file($url_imagem, $destino);
                    $this->Video->save($data, false);
                }

                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }

        //VideoTipo
        App::import('Model', 'VideoTipo');
        $this->VideoTipo = new VideoTipo();
        $videos_tipo = $this->VideoTipo->find('list', array('fields' => array('VideoTipo.id', 'VideoTipo.nome'), 'recursive' => -1, 'conditions' => array('VideoTipo.status' => true)));
        $this->set('video_tipos', array('' => 'Selecione...') + $videos_tipo);
    }

    function admin_edit($id = null) {
    	$this->Video->admin = true;
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Parâmetro inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            $this->data['Video']['id'] = $id;
            $this->Video->id = $id;

            if (isset($this->data['Video']['thumb_remove']) && $this->data['Video']['thumb_remove'] == 1) {
                $this->Video->remover_thumb($this->data['Video']['id']);
            }

            // begin video youtube
            if (isset($this->data['Video']['url'])) {

                //capturo o id do video do yotube
                $video_youtube_id = $this->get_youtube_video_id($this->data['Video']['url']);

                $this->data['Video']['video_youtube_id'] = $video_youtube_id;

                //capturo as infos do video do youtube
                if ($video_youtube_id) {
                    $dados_video = $this->get_youtube_video_infos($video_youtube_id);
                }

                if (empty($this->data['Video']['descricao'])) {
                    $this->data['Video']['descricao'] = $dados_video->data->description;
                }

                if (empty($this->data['Video']['duracao'])) {
                    //capturo a duracao do video em H:m:s

                    $duration = $dados_video->data->duration;
                    $seconds = $duration;
                    $hours = floor($seconds / 3600);
                    $seconds -= $hours * 3600;
                    $minutes = floor($seconds / 60);
                    $seconds -= $minutes * 60;
                    $duracao = "$hours:$minutes:$seconds";

                    $this->data['Video']['duracao'] = $duracao;
                }
            }
            // end video youtube

            if ($this->Video->save($this->data)) {

                $data['Video']['id'] = $this->Video->id;

                if ($this->data['Video']['thumb_default2'] == 1) {
                    //capturo a url do thumb do video
                    $url_imagem = $dados_video->data->thumbnail->hqDefault;

                    $dir = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'uploads' . DS . 'videos' . DS . 'thumb' . DS;
                    $nome_imagem = low(Inflector::slug($this->data['Video']['nome'], '-')) . '.jpg';
                    $destino = $dir . $nome_imagem;

                    $this->Video->Behaviors->detach('MeioUpload');
                    $data['Video']['thumb_filename'] = $nome_imagem;
                    $data['Video']['thumb_dir'] = 'uploads/videos/thumb';
                    $data['Video']['thumb_mimetype'] = 'image/jpeg';
                    $data['Video']['thumb_filesize'] = '13722';

                    $this->download_remote_file($url_imagem, $destino);
                    $this->Video->save($data, false);
                }


                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Video->read(null, $id);
        }

        //VideoTipo
        App::import('Model', 'VideoTipo');
        $this->VideoTipo = new VideoTipo();
        $videos_tipo = $this->VideoTipo->find('list', array('fields' => array('VideoTipo.id', 'VideoTipo.nome'), 'recursive' => -1, 'conditions' => array('VideoTipo.status' => true)));
        $this->set('video_tipos', array('' => 'Selecione...') + $videos_tipo);
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Video->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
    }

    function get_youtube_video_infos($video_id) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://gdata.youtube.com/feeds/api/videos/$video_id?v=2&alt=jsonc");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);

        return json_decode($output);
    }

    function get_youtube_video_id($url) {
        $video_id = false;
        $url = parse_url($url);
        if (strcasecmp($url['host'], 'youtu.be') === 0) {
            #### (dontcare)://youtu.be/<video id>
            $video_id = substr($url['path'], 1);
        } elseif (strcasecmp($url['host'], 'www.youtube.com') === 0) {
            if (isset($url['query'])) {
                parse_str($url['query'], $url['query']);
                if (isset($url['query']['v'])) {
                    #### (dontcare)://www.youtube.com/(dontcare)?v=<video id>
                    $video_id = $url['query']['v'];
                }
            }
            if ($video_id == false) {
                $url['path'] = explode('/', substr($url['path'], 1));
                if (in_array($url['path'][0], array('e', 'embed', 'v'))) {
                    #### (dontcare)://www.youtube.com/(whitelist)/<video id>
                    $video_id = $url['path'][1];
                }
            }
        }
        return $video_id;
    }

    function download_remote_file($file_url, $save_to) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_URL, $file_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $file_content = curl_exec($ch);
        curl_close($ch);

        $downloaded_file = fopen($save_to, 'w');
        fwrite($downloaded_file, $file_content);
        fclose($downloaded_file);
    }

}

?>