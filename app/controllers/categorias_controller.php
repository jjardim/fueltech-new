<?php

/**
 * BakeSale shopping cart
 * Copyright (c)	2006-2009, Matti Putkonen
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright		Copyright (c) 2006-2009, Matti Putkonen
 * @link			http://bakesalepro.com/
 * @package			BakeSale
 * @license			http://www.opensource.org/licenses/mit-license.php The MIT License
 */

/**
 * Manages product categories
 *
 */
class CategoriasController extends AppController {

    public $uses = array('Categoria', 'Produto');
    public $helpers = array('Image', 'Tree', 'String', 'Parcelamento');
    public $components = array('Filter', 'Session');
    private $conditions = array();

    /**
     * Realiza as listagem da categoria
     */
    function index() {

        $this->loadModel('Vitrine');
        $this->Vitrine = new Vitrine();

        App::import('Helper', 'String');
        $this->String = new StringHelper();

        //se encontrar vitrine vinculada a essa categoria, carrega os produtos da vitrine
        if ($vitrine_id = $this->Vitrine->find('first', array('conditions' => array('Vitrine.tipo' => 'CATEGORIA', 'Vitrine.categoria_id' => $this->params['categoria_id'])))) {
            //load model
            $this->loadModel('ProdutoVitrine');
            $this->ProdutoVitrine = new ProdutoVitrine();
            $produtos_vitrines = $this->ProdutoVitrine->find('all', array('contain' => array('Vitrine' => array('fields' => array('nome'))), 'recursive' => -2, 'fields' => array('produto_id'), 'conditions' => array('ProdutoVitrine.vitrine_id' => (int) $vitrine_id['Vitrine']['id'])));
            $this->set('vitrine_nome', $vitrine_id['Vitrine']['nome']);

            if (!$produtos_vitrines) {
                $this->Session->setFlash('Parametros inválidos', 'flash/error');
                $this->redirect(array('action' => '/'));
            }

            $produtos = set::extract('/ProdutoVitrine/produto_id', $produtos_vitrines);
            $joins =
                    array(
                        array(
                            'table' => 'produtos_categorias',
                            'alias' => 'Categorias',
                            'type' => 'left',
                            'conditions' =>
                            array(
                                'Categorias.produto_id = Produto.id'
                            )
                        ), array(
                            'table' => 'categorias',
                            'alias' => 'Categoria',
                            'type' => 'left',
                            'conditions' =>
                            array(
                                'Categorias.categoria_id = Categoria.id'
                            )
                        )
            );

            //debug($produtos);

            $children = $this->Categoria->children($this->params['categoria_id']);
            $categorias_ids = am($this->params['categoria_id'], set::extract('/Categoria/id', $children));

            //get set filter do topo
            $this->_getSetFilters($categorias_ids);

            $this->paginate = array(
                'group' => 'Produto.id',
                'order' => array('quantidade_disponivel' => 'DESC', 'rating' => 'DESC', 'quantidade' => 'DESC'),
                'recursive' => -1,
                'limit' => 48,
                'joins' => $joins,
                'conditions' => $this->conditions,
                'fields' => array('(SELECT IF(FreteGratis.label != "",FreteGratis.label,"") FROM frete_gratis FreteGratis WHERE (FreteGratis.categoria_id LIKE concat("%\"",Categorias.categoria_id,"\"%") OR FreteGratis.produto_id LIKE concat("%\"",Categorias.produto_id,"\"%") ) AND ( FreteGratis.status = true AND IF(Produto.preco_promocao > 0,Produto.preco_promocao,Produto.preco) >= FreteGratis.apartir_de AND FreteGratis.data_inicio <= now() AND FreteGratis.data_fim >= now() ) LIMIT 1) as frete_gratis,Produto.id,Produto.nome,Produto.preco,Produto.preco_promocao,Produto.quantidade_disponivel,Produto.status,Produto.preco_promocao_inicio,Produto.preco_promocao_fim,Produto.preco_promocao_novo,Produto.preco_promocao_velho,Produto.marca,Categoria.*,(SELECT concat("uploads/produto_imagem/filename/",ProdutoImagem.filename) FROM produto_imagens as ProdutoImagem WHERE ProdutoImagem.produto_id = Produto.id LIMIT 1) as imagem,(MATCH (Produto.nome,Produto.descricao,Produto.tag) AGAINST ("' . addslashes($this->params['pass'][0]) . '" IN BOOLEAN MODE)) AS rating')
            );

            $var_produtos = $this->paginate('Produto', array('Produto.id' => $produtos));
            $this->set('produtos', $var_produtos);

            //debug($produtos);
            //begin fabricantes
            if (!$this->data["Filter"]) {
                $this->loadModel('Fabricante');
                $this->Fabricante = new Fabricante();
                $produtos_vitrine_fab = $this->Produto->find('all', array('fields' => array('Produto.fabricante_id', 'Produto.status'), 'conditions' => array('Produto.id' => $produtos, 'Produto.status > ' => 0)));
                $fabricantes_id = set::extract('{.n}/Produto/fabricante_id', $produtos_vitrine_fab);
                $this->Session->write("ProdutosCategoria", $produtos);
                $fabricantes_id = array_unique($fabricantes_id);
                //busco os dados dos fabricantes capturados
                $fabricantes = $this->Fabricante->find('list', array(
                    'recursive' => -1,
                    'fields' => array('Fabricante.id', 'Fabricante.nome'),
                    'conditions' => array('Fabricante.id' => $fabricantes_id)
                        )
                );
                $this->Session->write('fabricantes', $fabricantes);
            } else {
                $fabricantes = $this->Session->read('fabricantes');
            }

            //seto fabricantes for views
            $this->set('fabricantes', $fabricantes);
            //end fabricantes
            //TODO: rever
            $filhos = $this->Categoria->find('all', array('recursive' => -1, 'conditions' => array('Categoria.id' => $this->params['categoria_id'])));
            //start breadcrumb
            $seo_url = $filhos[0]['Categoria']['seo_url'];
            $quebra = explode("/", $seo_url);
            $url_atual = "";
            foreach ($quebra as $key => $cat_url) {
                $url_atual .= $cat_url;

                $categoria_nome = $this->Produto->Categoria->find('first', array('recursive' => -1, 'fields' => 'Categoria.nome', 'conditions' => array('Categoria.seo_url' => $url_atual)));

                $breadcrumb[] = array('url' => 'categorias/' . $url_atual, 'nome' => $this->String->title_case($categoria_nome['Categoria']['nome']));

                $url_atual = $url_atual . "/";
            }
            $this->set('breadcrumb', $breadcrumb);
            //end breadcrumb
            //start seo
            $categoria_atual = $this->Categoria->find('first', array('recursive' => -1, 'conditions' => array('Categoria.id' => $this->params['categoria_id'])));
            if ($categoria_atual['Categoria']['seo_title']) {
                $this->set('seo_title', $categoria_atual['Categoria']['seo_title']);
            }
            if ($categoria_atual['Categoria']['seo_meta_description']) {
                $this->set('seo_meta_description', $categoria_atual['Categoria']['seo_meta_description']);
            }
            if ($categoria_atual['Categoria']['seo_meta_keywords']) {
                $this->set('seo_meta_keywords', $categoria_atual['Categoria']['seo_meta_keywords']);
            }
            if ($categoria_atual['Categoria']['seo_institucional']) {
                $this->set('seo_institucional', $categoria_atual['Categoria']['seo_institucional']);
            }

            $titulo = '';
            if ($breadcrumb) {
                $first = true;
                foreach ($breadcrumb as $valor) {
                    if ($first) {
                        $first = false;
                    } else {
                        $titulo .= ' | ';
                    }
                    $titulo .= $valor['nome'];
                }
            }
            $this->set('title_for_layout', $titulo);
            //end seo
        } else {
            //senao carrega os produtos da categoria
			$arrSubCategorias = $this->Categoria->find('list',
                    array('fields'=>
                        array('Categoria.id','Categoria.id'),
                          'conditions'=>
                        array('Categoria.parent_id'=>$this->params['categoria_id'])
                    )
            );
            $order = ($arrSubCategorias)?'Produto.ordem_pai ASC':'Produto.ordem ASC';
            //begin paginate
            $this->paginate = array(
                'contain' => array('Produto', 'Categoria'),
                'fields' => array('ProdutoDescricao.*, Produto.nome,Produto.preco,Produto.preco_promocao,Produto.quantidade_disponivel,Produto.status,Produto.preco_promocao_inicio,Produto.preco_promocao_fim,Produto.preco_promocao_novo,Produto.preco_promocao_velho,Produto.marca,Produto.fabricante_id,Produto.ordem,Categoria.nome,Categoria.seo_url,
					(SELECT concat("uploads/produto_imagem/filename/",ProdutoImagem.filename) FROM produto_imagens ProdutoImagem WHERE ProdutoImagem.produto_id = Produto.id LIMIT 1) as imagem,
					(SELECT IF(FreteGratis.label != "",FreteGratis.label,"") FROM frete_gratis FreteGratis WHERE (FreteGratis.categoria_id LIKE concat("%\"",ProdutoCategoria.categoria_id,"\"%") OR FreteGratis.produto_id LIKE concat("%\"",ProdutoCategoria.produto_id,"\"%") ) AND ( FreteGratis.status = true AND IF(Produto.preco_promocao > 0,Produto.preco_promocao,Produto.preco) >= FreteGratis.apartir_de AND FreteGratis.data_inicio <= now() AND FreteGratis.data_fim >= now() )  LIMIT 1) as frete_gratis
				'),
                'limit' => 48,
                'joins' => array(
                    array(
                        'table' => 'produto_descricoes',
                        'alias' => 'ProdutoDescricao',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'ProdutoCategoria.produto_id = ProdutoDescricao.produto_id'
                        )
                    )
                ),
                'conditions' => array('ProdutoDescricao.language' => Configure::read('Config.language')),
                'order' => $order.', Produto.quantidade_disponivel DESC, Produto.quantidade_acessos DESC'
            );
            //end paginate
            //load model ProdutoCategoria
            $this->loadModel('ProdutoCategoria');
            //pega todos os id da categoria passada e filtra os produtos
            $filhos = $this->Categoria->find('all', array('recursive' => -1, 'conditions' => array('Categoria.id' => $this->params['categoria_id'])));
            //seto nome da categoria na tela
            if (isset($filhos[0])) {
                $this->set('categoria_data', $filhos[0]);
            }

            //start breadcrumb
            $seo_url = $filhos[0]['Categoria']['seo_url'];
            $quebra = explode("/", $seo_url);
            $url_atual = "";
            foreach ($quebra as $key => $cat_url) {
                $url_atual .= $cat_url;

                $categoria_nome = $this->Produto->Categoria->find('first', array(
                    'recursive' => -1,
                    'fields' => 'CategoriaDescricao.nome',
                    'joins' => array(
                        array(
                            'table' => 'categoria_descricoes',
                            'alias' => 'CategoriaDescricao',
                            'type' => 'LEFT',
                            'conditions' => 'CategoriaDescricao.categoria_id = Categoria.id'
                        )
                    ),
                    'conditions' => array('Categoria.seo_url' => $url_atual, 'CategoriaDescricao.language' => Configure::read('Config.language'))));
                $breadcrumb[] = array('url' => 'categorias/' . $url_atual, 'nome' => $this->String->title_case($categoria_nome['CategoriaDescricao']['nome']));

                $url_atual = $url_atual . "/";
            }
            $this->set('breadcrumb', $breadcrumb);
            //end breadcrumb

            $children = $this->Categoria->children($this->params['categoria_id']);
            $categorias_ids = am($this->params['categoria_id'], set::extract('/Categoria/id', $children));
            $this->conditions = array_merge(array('Categoria.status' => true), $this->conditions);
            $this->conditions = array_merge(array('ProdutoCategoria.categoria_id' => $categorias_ids), $this->conditions);
            $this->conditions = array_merge(array('Produto.status >' => 0), $this->conditions);

            //get filtros categorias
            $produtos_ids_com_atributo = $this->carregaFiltrosCategoria();

            //get set filter do topo
            $filtro_variacao_tamanho = $this->_getSetFilters($categorias_ids);

            //se há filtro de atributos, acrescento a condition com ids dos produtos
            if (isset($this->params['named']['atributos']) && !empty($this->params['named']['atributos'])) {
                $this->conditions = array_merge(
                        array('ProdutoCategoria.produto_id' => explode(',', $produtos_ids_com_atributo)), $this->conditions
                );

                $filtros_ids = json_decode($this->params['named']['atributos']);
                App::import('Model', 'Atributo');
                $this->Atributo = new Atributo();
                $atributo_tamanho = false;
                foreach ($filtros_ids as $filtros_id) {
                    $atributo = $this->Atributo->find('first', array('recursive' => 1, 'contain' => 'AtributoTipo', 'conditions' => array('Atributo.id' => $filtros_id)));
                    if ($atributo['AtributoTipo']['nome'] == 'Tamanho') {
                        $atributo_tamanho = true;
                    }
                }
                // if ($atributo_tamanho != true && $filtro_variacao_tamanho != true) {
                    // $this->conditions = array_merge(
                            // array('Produto.parent_id' => 0), $this->conditions
                    // );
                // }

                $this->Filter->testAndClear(true);
            }/* else {
                if ($filtro_variacao_tamanho != true) {
                    $this->conditions = array_merge(
                            array('Produto.parent_id' => 0), $this->conditions
                    );
                }
            }*/

            //begin produtos_mais_vendidos
            // $mais_vendidos = $this->Produto->find('all',
            // array(
            // 'fields'	=> array('Produto.id,Produto.parent_id,Produto.nome,Produto.preco,Produto.preco_promocao,Produto.quantidade_disponivel,Produto.status,Produto.preco_promocao_inicio,Produto.preco_promocao_fim,Produto.preco_promocao_novo,Produto.preco_promocao_velho,
            // (SELECT concat("uploads/produto_imagem/filename/",ProdutoImagem.filename) FROM produto_imagens ProdutoImagem WHERE ProdutoImagem.produto_id = Produto.id LIMIT 1) as imagem'),
            // 'joins' => array(
            // array('table' => 'produtos_categorias', 'alias' => 'ProdutoCategoria', 'type' => 'LEFT',
            // 'conditions' => array('ProdutoCategoria.produto_id = Produto.id'))),
            // 'conditions' => array('Produto.parent_id' => 0,'Produto.status >' => 0, 'ProdutoCategoria.categoria_id' => am($this->params['categoria_id'], set::extract('/Categoria/id', $filhos))),
            // 'recursive' => -1,
            // 'limit' 	=> 10,
            // 'order' 	=> 'Produto.quantidade_vendidos DESC'
            // )
            // );
            // $this->set('mais_vendidos', $mais_vendidos);
            //end produtos_mais_vendidos
            //set produtos
            //debug($this->conditions);
            $produtos = $this->paginate('ProdutoCategoria', $this->conditions);

            //debug($produtos);die;

            $this->set('produtos', $produtos);

            //begin produtos_ids
            $produtos_categorias = $this->ProdutoCategoria->find("all", array("fields" => array("ProdutoCategoria.produto_id", "Produto.fabricante_id"), "recursive" => 1, "contain" => array("Produto", "Categoria"), "conditions" => $this->conditions));
            $produtos_ids = set::extract('{.n}/ProdutoCategoria/produto_id', $produtos_categorias);
            $this->Session->write("ProdutosCategoria", $produtos_ids);
            //end produtos_ids
            //begin fabricantes
            if (!$this->data["Filter"]) {
                $this->loadModel('Fabricante');
                $this->Fabricante = new Fabricante();
                $fabricantes_id = set::extract('{.n}/Produto/fabricante_id', $produtos);
                $fabricantes_id = array_unique($fabricantes_id);
                //busco os dados dos fabricantes capturados
                $fabricantes = $this->Fabricante->find('list', array(
                    'recursive' => -1,
                    'fields' => array('Fabricante.id', 'Fabricante.nome'),
                    'conditions' => array('Fabricante.id' => $fabricantes_id)
                        )
                );
                $this->Session->write('fabricantes', $fabricantes);
            } else {
                $fabricantes = $this->Session->read('fabricantes');
            }

            //seto fabricantes for views
            $this->set('fabricantes', $fabricantes);
            //end fabricantes
            //start seo
            $categoria_atual = $this->Categoria->find('first', array('recursive' => -1, 'conditions' => array('Categoria.id' => $this->params['categoria_id'])));
            if ($categoria_atual['Categoria']['seo_title']) {
                $this->set('seo_title', $categoria_atual['Categoria']['seo_title']);
            }
            if ($categoria_atual['Categoria']['seo_meta_description']) {
                $this->set('seo_meta_description', $categoria_atual['Categoria']['seo_meta_description']);
            }
            if ($categoria_atual['Categoria']['seo_meta_keywords']) {
                $this->set('seo_meta_keywords', $categoria_atual['Categoria']['seo_meta_keywords']);
            }
            if ($categoria_atual['Categoria']['seo_institucional']) {
                $this->set('seo_institucional', $categoria_atual['Categoria']['seo_institucional']);
            }

            $titulo = '';
            if ($breadcrumb) {
                $first = true;
                foreach ($breadcrumb as $valor) {
                    if ($first) {
                        $first = false;
                    } else {
                        $titulo .= ' | ';
                    }
                    $titulo .= $valor['nome'];
                }
            }
            $this->set('title_for_layout', $titulo);
            //end seo
        }
    }
	
	/**
     * Edits information on a category.
     *
     * @param id int
     * The ID field of the category to edit.
     */
    function admin_order($id = null) {
        
        
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        
        App::import('Model', 'Produto');
        $this->Produto = new Produto();
        
        //EDITAR
        if (!empty($this->data)) {
            
            $ordemPai = 1;
            foreach($this->data['Produto'] as $idProd => $valor){
                $this->Produto->id = $idProd;
                $this->Produto->saveField('ordem_pai',$ordemPai);
                $ordemPai++;
            }
            
        }
        
        //SELECIONA SUBCATEGORIAS
        App::import('Model', 'Categoria');
        $this->Categoria = new Categoria();
        
        $ddCategoria = $this->Categoria->find('first',array('conditions'=>array('Categoria.id'=>$id)));
        
        $arrCategorias = $this->Categoria->find('list',
                array('fields'=>
                    array('Categoria.id','Categoria.id'),
                      'conditions'=>
                    array('Categoria.parent_id'=>$id)
                )
        );
        
        if($arrCategorias){
            $listaCategorias = implode(',', $arrCategorias);
            $listaCategorias .= ','.$id;
            $campoOrdem = 'ordem_pai';
        }else{
            $listaCategorias = $id;
            $campoOrdem = 'ordem';
        }
        
        //EDITAR
        if (!empty($this->data)) {
            
            $ordemPai = 1;
            foreach($this->data['Produto'] as $idProd => $valor){
                $this->Produto->id = $idProd;
                $this->Produto->saveField($campoOrdem,$ordemPai);
                $ordemPai++;
            }
            
        }
        
        //SELECIONA PRODUTOS
        App::import('Model', 'ProdutoCategoria');
        $this->ProdutoCategoria = new ProdutoCategoria();
        
        $arrAllProdutos = $this->ProdutoCategoria->find('list',
                array(
                    'fields'=>
                        array('ProdutoCategoria.produto_id','ProdutoCategoria.produto_id'),
                    'conditions'=>
                        array('ProdutoCategoria.categoria_id IN ('.$listaCategorias.') ')
                )
        );
        if($arrAllProdutos){
            $listaProdutos = implode(',', $arrAllProdutos);
        
        
            $arrProdutos = $this->Produto->find('all',
                    array(
                        'fields'=>
                            array('Produto.id','Produto.sku','Produto.nome'),
                        'conditions'=>
                            array('Produto.id IN ('.$listaProdutos.') ' , 'Produto.status >'=>0),
                        'order'=>array('Produto.ordem_pai ASC'),
                        'recursive'=>-1
                    )
            );

            $arrSelect = array();
            foreach($arrProdutos as $ddProduto){
                $arrSelect[$ddProduto['Produto']['id']] = $ddProduto['Produto']['sku'].' - '.$ddProduto['Produto']['nome'];
            }

            //debug($ddCategoria);die;

            $this->set('produtos' , $arrSelect);
            $this->set('ddCategoria' , $ddCategoria['Categoria']);
            
        }else{
            $this->Session->setFlash('Nenhum produto para ordenar nesta categoria.', 'flash/error');
            $this->redirect(array('action' => 'categorias'));
        }
    }

    /**
     * Realiza as listagem das vitrines da categoria
     */
    function vitrines() {
        $conditions = array();

        $this->paginate = array('recursive' => 1, 'limit' => 12, 'group' => 'produto_id');

        //pega todos os filhos da categoria passada e filtra os produtos
        $filhos = $this->Categoria->children($this->params['pass'][0]);

        $categoria_nome = $this->Categoria->find('first', array('conditions' => array('id' => $this->params['pass'][0])));

        if ($categoria_nome['Categoria']['parent_id'] == 0) {
            $this->Categoria->bindModel(array(
                'hasMany' =>
                array(
                    'Produto' => array('conditions' => array('Produto.categoria_id' => 'Categoria.id'))
                )
                    )
            );
            $dados = $this->Categoria->find('all', array('recursive' => 2, 'conditions' => array('Categoria.parent_id' => $categoria_nome['Categoria']['id'])));
            $this->set(compact('dados'));
        }

        $this->set(compact('categoria_nome'));
        $vitrines = $this->Vitrine->BuscaVitrines($this->params['pass'][0]);

        $parcelamento = current($this->PagamentoCondicao->find('list', array('fields' => array('parcelas'), 'recursive' => -1, 'order' => 'parcelas DESC')));
        $this->set('parcelamento', $parcelamento);

        if (isset($vitrines['vitrines'][0]['Produto']) && count($vitrines['vitrines'][0]['Produto']) > 0) {
            $this->set('vitrines', $vitrines);
        } else {
            $this->redirect("/{$this->params['slug']}-cat-{$this->params['id']}");
        }
        $breadcrumbs = $this->Categoria->getpath($this->params['pass'][0]);
        $this->set(compact('breadcrumbs'));
        $titulo = '';
        if (!empty($breadcrumbs)) {
            foreach ($breadcrumbs as $valor) {
                $titulo .= ' ' . $valor['Categoria']['nome'];
            }
        }
        $this->set('title_for_layout', $titulo);
    }

    /**
     * Displays category menu.
     *
     */
    public function menu($id = false) {
        if (is_numeric($id) && $id == 0) {
            $dados = $this->Categoria->_menu('site', 0);
        } else {
            $dados = $this->Categoria->_menu('site', null);
        }
        $v = array();
        if ($id && $this->params['controller'] == 'categorias') {

            $pai = $this->Categoria->getpath($id);

            foreach ($dados as $chave => $valor) {
                if ($valor['Categoria']['id'] == $pai[0]['Categoria']['id']) {
                    $v[$chave] = $valor;
                }
            }

            $t = array();
            $x = array();
            $s = array();

            foreach ($v as $chave => $valor) {
                foreach ($valor['children'] as $b) {
                    if (isset($pai[1]) && $b['Categoria']['id'] == $pai[1]['Categoria']['id']) {
                        $x[] = $b;
                    } else {
                        $s[] = $b;
                    }
                }
            }
            //zera os indices
            $v = array_values($v);
            $t[0]['Categoria'] = $v[0]['Categoria'];
            $t[0]['children'] = am($x, $s);

            return $t;
        } else {

            return $dados;
        }
    }

    /**
     * add new category
     */
    public function admin_add() {
        if (!empty($this->data)) {
            unset($this->data['thumb_filename']);

            $router = $this->Categoria->find('first', array('recursive' => -1, 'fields' => array('Categoria.seo_url'), 'conditions' => array('Categoria.id' => $this->data['Categoria']['parent_id'])));
            if($router){
                $this->data['Categoria']['seo_url'] = $router['Categoria']['seo_url'].'/'.low(Inflector::slug($this->data['CategoriaDescricao'][0]['nome'], '-'));
				$this->data['Categoria']['nome'] 	= $this->data['CategoriaDescricao'][0]['nome'];
            }else{
                $this->data['Categoria']['seo_url'] = low(Inflector::slug($this->data['CategoriaDescricao'][0]['nome'], '-'));
				$this->data['Categoria']['nome'] 	= $this->data['CategoriaDescricao'][0]['nome'];
            }

            if ($this->Categoria->saveAll($this->data)) {
                //limpo o cache
                $this->limparCache();
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        $this->Categoria->recursive = -1;

        $parents = array('' => 'selecione') + $this->Categoria->generatetreelist(null, null, '{n}.Categoria.nome', '>>');
        $this->set(compact('parents'));

        //set linguas
        App::import('Model', 'Linguagem');
        $this->Linguagem = new Linguagem();
        $idiomas = $this->Linguagem->find('all', array('fields' => array('Linguagem.id', 'Linguagem.codigo', 'Linguagem.nome', 'Linguagem.thumb_filename', 'Linguagem.thumb_dir'), 'conditions' => array('Linguagem.status' => true, 'Linguagem.externo' => 0)));
        $this->set('idiomas', $idiomas);
    }

    /**
     * Edits information on a category.
     *
     * @param id int
     * The ID field of the category to edit.
     */
    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {

            $this->data['Categoria']['id'] = $id;

            if (isset($this->data['Categoria']['thumb_remove']) && $this->data['Categoria']['thumb_remove'] == 1) {
                $this->Categoria->remover_thumb($this->data['Categoria']['id']);
            }

            if (isset($this->data['CategoriaDescricao'][0]['nome'])) {
                $this->data['Categoria']['nome'] = $this->data['CategoriaDescricao'][0]['nome'];
            }

            $router = $this->Categoria->find('first', array('recursive' => -1, 'fields' => array('Categoria.seo_url'), 'conditions' => array('Categoria.id' => $this->data['Categoria']['parent_id'])));
            if($router){
                $this->data['Categoria']['seo_url'] = $router['Categoria']['seo_url'].'/'.low(Inflector::slug($this->data['CategoriaDescricao'][0]['nome'], '-'));
				$this->data['Categoria']['nome'] 	= $this->data['CategoriaDescricao'][0]['nome'];
            }else{
                $this->data['Categoria']['seo_url'] = low(Inflector::slug($this->data['CategoriaDescricao'][0]['nome'], '-'));
				$this->data['Categoria']['nome'] 	= $this->data['CategoriaDescricao'][0]['nome'];
            }

            if ($this->Categoria->saveAll($this->data)) {

                //limpo o cache
                $this->limparCache();

                $filhos = $this->Categoria->children($this->params['pass'][0]);
                $ids = set::extract('/Categoria/id', $filhos);
                foreach ($ids as $id) {
                    $this->Categoria->id = $id;
                    $this->Categoria->saveField('status', $this->data['Categoria']['status']);
                }

                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }

        $this->Categoria->recursive = 1;
        $this->Categoria->unbindModel(array('hasAndBelongsToMany' => array('CategoriaAtributo', 'ProdutoCategoria')));
        $parents = array('' => 'selecione') + $this->Categoria->generatetreelist(null, null, '{n}.Categoria.nome', '>>');
        $this->set(compact('parents'));

        if (empty($this->data)) {
            $this->data = $this->Categoria->read(null, $id);
        }

        //debug($this->data);
        //set linguas
        App::import('Model', 'Linguagem');
        $this->Linguagem = new Linguagem();
        $idiomas = $this->Linguagem->find('all', array('fields' => array('Linguagem.id', 'Linguagem.codigo', 'Linguagem.nome', 'Linguagem.thumb_filename', 'Linguagem.thumb_dir'), 'conditions' => array('Linguagem.status' => true, 'Linguagem.externo' => 0)));
        $this->set('idiomas', $idiomas);
    }

    /**
     * Displays all cateogries in the admin page.
     *
     * The categories are retrieved for display on screen. Makes use of the parent-child relationship of categories.
     *
     * @return The array as returned by the Model::findAllThreaded() method. Results are sorted by sort.
     */
    public function admin_menu() {

        return $this->Categoria->_menu('admin');
    }

    public function admin_index() {

    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if (!$this->Produto->find('first', array('conditions' => array('Produto.categoria_id' => $id)))) {
            if ($this->Categoria->delete($id)) {
                //limpo o cache
                $this->limparCache();
                $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
                $this->redirect(array('action' => 'index'));
            }
        } else {
            $this->Session->setFlash('O Registro não pode ser deletado, pois possui registros vinculados', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
    }

    /**
     * if using storefronts check that the root category is correct.
     */
    private function _validateCategory($id) {
        $valid = $this->Categoria->isValidCategory((int) $id);
        if (!$valid) {
            $this->redirect('/');
        }
        return true;
    }

    function admin_ajax_categorias() {
        $query = $this->params['form']['query'];

        //produtos já selecionados
        $selecteds = $this->params['form']['selecteds'];

        if ($query) {

            $categorias = $this->Categoria->find('all', array('fields' => array('Categoria.id', 'Categoria.seo_url'),
                'recursive' => -1,
                'limit' => '100',
                'conditions' => array('AND' => array('NOT' => array('Categoria.id' => $selecteds), 'Categoria.descricao LIKE' => '%' . $query . '%', 'Categoria.nome LIKE' => '%' . $query . '%'))
                    )
            );
            $categorias_arr = array();
            foreach ($categorias as $categoria) {
                $categorias_arr[$categoria['Categoria']['id']] = $categoria['Categoria']['seo_url'];
            }
            die(json_encode($categorias_arr));
        } else {
            die(json_encode(false));
        }
    }

    private function carregaFiltrosCategoria() {

        $this->Session->write('atributos_selecionados', '');

        if (isset($this->params['named']['atributos']) && !empty($this->params['named']['atributos'])) {
            //se tem filtro na url
            $this->Session->write('atributos_selecionados', $this->params['named']['atributos']);
            $filtros_ids = json_decode($this->params['named']['atributos']);
            $produtos_ids_com_atributo = "";

            //percorro os filtros
            foreach ($filtros_ids as $k => $v):

                //no primeiro atributo do filtro, considero todos os produtos da categoria + atributos
                if ($k == 0) {
                    $condicao_filtro = "ProdutosCategorias.categoria_id = " . $this->params['categoria_id'] . "
											AND
										AtributosProdutos.atributo_id = " . $v . "";
                } else {
                    //a partir do primeiro, considero além dos produtos da categoria + atributos, os produtos já filtrados pelo primeiro atributo
                    $condicao_filtro = "ProdutosCategorias.categoria_id = " . $this->params['categoria_id'] . "
											AND
										AtributosProdutos.atributo_id = " . $v . "
											AND
										AtributosProdutos.produto_id IN ( " . $produtos_ids_com_atributo . ")";
                }

                $query = "
							SELECT group_concat(AtributosProdutos.produto_id) as produtos_ids
							FROM
								atributos_produtos AS AtributosProdutos
							LEFT JOIN
								produtos_categorias AS ProdutosCategorias
								ON
								AtributosProdutos.produto_id = ProdutosCategorias.produto_id
							WHERE " . $condicao_filtro;

                $produtos_in = $this->Categoria->query($query);
                $produtos_ids = $produtos_in[0][0]['produtos_ids'];
                $produtos_ids_com_atributo = $produtos_ids;

                if (!$produtos_ids_com_atributo)
                    return;

            //print($query.'<hr />');

            endForeach;
        }

        //retorno os produtos resultantes do filtro
        if (isset($produtos_ids_com_atributo))
            return $produtos_ids_com_atributo;
    }

    private function limparCache() {
        Cache::write('menu_topo', false);
        Cache::write('categorias_routes', false);
		
    }

    private function _getIntervaloPreco() {

        App::import('Model', 'ProdutoCategoria');
        $this->ProdutoCategoria = new ProdutoCategoria();
        //busco precos dos produtos
        $produto_preco = "";
        $retorno = array();
        $produtos_precos = $this->ProdutoCategoria->find("all", array(
            'recursive' => 1,
            'contains' => array('Produto'),
            'fields' => array('Produto.preco', 'Produto.preco_promocao', 'Produto.preco_promocao_inicio', 'Produto.preco_promocao_fim'),
            'conditions' => $this->conditions)
        );

        //crio array ordenador para os filtro
        foreach ($produtos_precos as $key => $row) {
            $produto_preco[$key] = ($row['Produto']['preco_promocao'] > 0) ? $row['Produto']['preco_promocao'] : $row['Produto']['preco'];
        }

        //debug($produto_preco);
        $produto_preco_min = 0;
        $produto_preco_max = 0;
        if (is_array($produto_preco) && count($produto_preco) > 0) {
            //menor preco
            sort($produto_preco);
            $retorno['produto_preco_min'] = $produto_preco[0];
            //maior preco
            rsort($produto_preco);
            $retorno['produto_preco_max'] = $produto_preco[0];
        } else {
            $retorno['produto_preco_min'] = 0;
            $retorno['produto_preco_max'] = 0;
        }

        return $retorno;
    }

    //metodo que gerencia o filtro do topo das categorias e vitrines
    private function _getSetFilters($categorias_ids) {

        //retorno dos ids dos produtos resultantes do filtro
        //$retorno = array();
        //capturos as categorias e suas filhas
        $categorias_id = $categorias_ids;

        //recupera o intevalo de precos dos produtos listados
        $precos = $this->_getIntervaloPreco();

        //set precos default (sem conditions do filtro)
        $this->set('default_preco_min', $precos['produto_preco_min']);
        $this->set('default_preco_max', $precos['produto_preco_max']);
        //end produtos_precos
        //tem tamanho
        $tem_tamanho = false;

        //import Controller de variacao
        App::import('Controller', 'Variacoes');
        $VariacoesController = new VariacoesController();

        $variacoes_cores = array();
        $variacoes_tamanhos = array();

        //begin filter top
        //se tem data filter ou paramentro na url, utiliza os valores do filtro
        if ($this->data["Filter"] || count($this->params['named']) > 0) {

            //seto filtros
            if ($this->data["Filter"]["marca"]) {
                $filtros['marca'] = "Produto.fabricante_id = '{%value%}'";

                $variacoes_cores = $VariacoesController->ajax_get_cor_fabricante($this->data["Filter"]["marca"], false);
            }

            $produtos_ids_variacao_tamanho = array();
            $produtos_ids_variacao_cor = array();

            if (isset($this->data["Filter"]["cor"]) && $this->data["Filter"]["cor"] != "") {
                $query = "
							SELECT group_concat(VariacoesProdutos.produto_id) as produtos_ids
							FROM
								variacoes_produtos AS VariacoesProdutos
							LEFT JOIN
								produtos_categorias AS ProdutosCategorias
								ON
								VariacoesProdutos.produto_id = ProdutosCategorias.produto_id
							WHERE ProdutosCategorias.categoria_id in (" . implode(',', $categorias_ids) . ")
											AND
										VariacoesProdutos.variacao_id = " . $this->data["Filter"]["cor"];

                $produtos_in = $this->Categoria->query($query);
                $produtos_ids = $produtos_in[0][0]['produtos_ids'];
                $produtos_ids_variacao_cor = $produtos_ids;
                //$produtos_ids_variacao_cor = ( substr($produtos_ids, strlen($produtos_ids)-1) == "," ) ? substr($produtos_ids, 0, strlen($produtos_ids)-2) : $produtos_ids;

                $filtros['cor'] = "Produto.id in (" . $produtos_ids_variacao_cor . ")";
                $variacoes_tamanhos = $VariacoesController->ajax_get_tamanho_cor($this->data["Filter"]["marca"], $this->data["Filter"]["cor"], false);
            }

            if (isset($this->data["Filter"]["tamanho"]) && $this->data["Filter"]["tamanho"] != "") {

                $conditions = "AND VariacoesProdutos.variacao_id = " . $this->data["Filter"]["tamanho"];

                if (count($produtos_ids_variacao_cor) > 0) {
                    $conditions .= " AND VariacoesProdutos.produto_id in (" . $produtos_ids_variacao_cor . ")";
                    //se houver resultado das variacoes das cores, removo a variaveis apos add os produdos na condicao para remover duplicatas.
                    //unset($produtos_ids_variacao_cor);
                }

                $query = "
							SELECT group_concat(VariacoesProdutos.produto_id) as produtos_ids
							FROM
								variacoes_produtos AS VariacoesProdutos
							LEFT JOIN
								produtos_categorias AS ProdutosCategorias
								ON
								VariacoesProdutos.produto_id = ProdutosCategorias.produto_id
							WHERE ProdutosCategorias.categoria_id in (" . implode(',', $categorias_ids) . ") " . $conditions;


                $produtos_in = $this->Categoria->query($query);
                $produtos_ids = $produtos_in[0][0]['produtos_ids'];
                $produtos_ids_variacao_tamanho = $produtos_ids;

                if (count($produtos_ids_variacao_tamanho) > 0) {
                    //$filtros['tamanho'] = "Produto.id in (".$produtos_ids_variacao_tamanho.")";
                    $tem_tamanho = true;
                }
            }

            if (isset($produtos_ids_variacao_tamanho) && count($produtos_ids_variacao_tamanho) > 0) {
                $ids = array();
                $ids = am($produtos_ids_variacao_tamanho);
                $filtros['cor'] = "Produto.id in (" . implode(',', $ids) . ")";
                $filtros['tamanho'] = "Produto.id in (" . implode(',', $ids) . ")";
            }

            //se for post, seto os valores de preco max e min escolhido pelo usuario
            if ($this->data["Filter"]["preco_min"] != "" && $this->data["Filter"]["preco_max"] != "") {
                $produto_preco_min = $this->data["Filter"]["preco_min"];
                $produto_preco_max = $this->data["Filter"]["preco_max"];
                $filtros['preco_min'] = "Produto.preco >= '" . $produto_preco_min . "'";
                $filtros['preco_max'] = "Produto.preco <= '" . $produto_preco_max . "'";
            } else {
                //senao, busco o valores já setados pelo mesmo anteriormente
                $valores = $this->Filter->getStoredData();
                if (array_key_exists('preco_min', $valores)) {
                    $produto_preco_min = $valores["preco_min"];
                }
                if (array_key_exists('preco_max', $valores)) {
                    $produto_preco_max = $valores["preco_max"];
                }
            }

            //seto os filtros na conditions
            if (isset($filtros) && count($filtros) > 0) {
                $this->Filter->setConditions($filtros);
                $this->Filter->check();
            }

            //seto na var conditions
            $this->conditions = array_merge($this->Filter->getFilters(), $this->conditions);

            //debug($this->conditions);

            $this->Filter->setDataToView();
        } else {
            $this->Filter->testAndClear(true);
        }
        //end filter top
        //recupera o intevalo de precos dos produtos listados
        $precos = $this->_getIntervaloPreco();

        //set produtos preco for view
        $this->set('produto_preco_min', $precos['produto_preco_min']);
        $this->set('produto_preco_max', $precos['produto_preco_max']);

        //set variacoes_cores e variacoes_tamanhos for view
        $this->set('variacoes_cores', $variacoes_cores);
        $this->set('variacoes_tamanhos', $variacoes_tamanhos);

        //returna retorno (haha)
        return $tem_tamanho;
    }

}

?>
