<?php
class RevendedoresController extends AppController {

	var $name = 'Revendedores';
	var $components = array('Session','Filter');
	var $helpers = array('Calendario','String','Image','Flash','Javascript','Marcas','Estados');

	function admin_index() {
		//filters
		$filtros = array();
        if (isset($this->data["Filter"]["nome"])) {
            $filtros['nome'] = "Revendedor.nome LIKE '%{%value%}%'";
        }

        if (isset($this->data["Filter"]["filtro"])) {
            $filtros['filtro'] = "Revendedor.cidade LIKE '%{%value%}%' OR Revendedor.estado LIKE '%{%value%}%'";
        }
        
        if (isset($this->data["Filter"]["localization"])) {
        	$filtros['localization'] = "Revendedor.localidade_label LIKE '%{%value%}%'";
        }

        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();

		if(isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar"){
			$this->admin_exportar($conditions);
		}

		$this->Revendedor->recursive = 0;
		$this->set('revendedores', $this->paginate($conditions));
	}
	public function admin_exportar($conditions) {

        App::import('Helper', 'Calendario');
        $this->Calendario = new CalendarioHelper();

        $rows = $this->Revendedor->find('all', array('conditions' => $conditions));

        $table = "<table>";
        $table .= "
				<tr bgcolor=\"#CECECE\">
					<td><strong>Id</strong></td>
					<td><strong>Nome</strong></td>
					<td><strong>Descricao Resumida</strong></td>
					<td><strong>Descricao Completa</strong></td>
					<td><strong>Cidade</strong></td>
                                        <td><strong>Estado</strong></td>
					<td><strong>Status</strong></td>
					<td><strong>Criado</strong></td>
					<td><strong>Modificado</strong></td>
				</tr>";
        foreach ($rows as $row) {
            $status = ( $row['Revendedor']['status'] ) ? "Ativo" : "Inativo";
            $table .= "
				<tr>
					<td>" . $row['Revendedor']['id'] . "</td>
					<td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Revendedor']['nome']) . "</td>
					<td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Revendedor']['descricao_resumida']) . "</td>
					<td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Revendedor']['descricao_completa']) . "</td>
					<td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Revendedor']['cidade']) . "</td>
                                            <td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Revendedor']['estado']) . "</td>
					<td>" . $status . "</td>
					<td>" . $this->Calendario->DataFormatada("d-m-Y", $row['Revendedor']['created']) . "</td>
					<td>" . $this->Calendario->DataFormatada("d-m-Y", $row['Revendedor']['modified']) . "</td>
				</tr>";
        }
        $table .= "</table>";

        App::import("helper", "String");
        $this->String = new StringHelper();
        $this->layout = false;
        $this->render(false);
        set_time_limit(0);
        header('Content-type: application/x-msexcel');
        $filename = "revendedores_" . date("d_m_Y_H_i_s");
        header('Content-Disposition: attachment; filename=' . $filename . '.xls');
        header('Pragma: no-cache');
        header('Expires: 0');

        die($table);
    }
	function admin_add() {
		if (!empty($this->data)) {
			$this->Revendedor->create();

			if ($this->Revendedor->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
		//linguagens
		App::import("Model",'Linguagem');
		$this->Linguagem = new Linguagem();
		$idiomas = $this->Linguagem->find("list", array("fields" => array("Linguagem.codigo", "Linguagem.nome"),"conditions" => array("Linguagem.status" => true)));
		$this->set("idiomas", $idiomas);
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash('Par�metro inv�lidos','flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			$this->data['Revendedor']['id'] = $id;
			$this->Revendedor->id = $id;

			if( isset($this->data['Revendedor']['thumb_remove']) && $this->data['Revendedor']['thumb_remove'] == 1 ){
				$this->Revendedor->remover_thumb($this->data['Revendedor']['id']);
			}

			if ($this->Revendedor->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Revendedor->read(null, $id);
		}
		//linguagens
		App::import("Model",'Linguagem');
		$this->Linguagem = new Linguagem();
		$idiomas = $this->Linguagem->find("list", array("fields" => array("Linguagem.codigo", "Linguagem.nome"),"conditions" => array("Linguagem.status" => true)));
		$this->set("idiomas", $idiomas);
	}

	function admin_delete($id = null) {
		if (!$id) {
            $this->Session->setFlash('Parametros inv�lidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Revendedor->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro n�o pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}

	function get_cidades($uf){
        $this->render(false);
        $this->layout = false;

        $cidades = $this->Revendedor->find('all',
                                                array(
                                                        'recursive'     => -1,
                                                        'conditions'    => array('Revendedor.status' => true, 'Revendedor.localidade_label' => $uf),
                                                        'fields'        => array('Revendedor.pais'),
                                                        'group'         => array('Revendedor.pais'),
                                                    )
                                            );

       die(json_encode(Set::combine($cidades, '{n}/Revendedor/pais', '{n}/Revendedor/pais')));
    }

	function lista_revendedores($local){

		$this->layout = 'ajax';
		$this->paginate = array(
			'limit' 		=> 1000,
			'conditions' 	=> array(
									"Revendedor.status" 				=> true,
									"Revendedor.language" 				=> Configure::read('Config.language'),
									"Revendedor.localidade_label" 	=> $local),
                        'order' => "Revendedor.ordem"
                    
		);

		$this->set('revendedores', $this->paginate('Revendedor'));
		$this->render('lista_revendedores');

	}

	function busca_revendedores($estado = "", $cidade = ""){

		$conditions = array('Revendedor.status' => true, 'Revendedor.language' => Configure::read('Config.language'));

		if($estado != null){
			$conditions = array_merge($conditions, array("AND" => array("Revendedor.localidade_label LIKE" => "%$estado%" )));
		}

		if($cidade != null){
			$conditions = array_merge($conditions, array("AND" => array("Revendedor.pais LIKE" => "%$cidade%" )));
		}

		$this->layout = 'ajax';
		$this->paginate = array(
			'limit' 		=> 1000,
			'conditions' 	=> $conditions
		);

  
                
		//$this->set('revendedores', $this->paginate('Revendedor'));
		$this->render('lista_revendedores');
	}

}
?>
