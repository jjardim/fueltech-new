<?php
/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::import('Helper', 'Helper', false);

/**
 * This is a placeholder class.
 * Create the same file in app/app_helper.php
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       cake
 * @subpackage    cake.cake
 */
class AppHelper extends Helper {

	private $urlcache = array(); 
	
    function url($url = null, $full = false) {
		
		if(isset($this->params['controller']) && ($this->params['controller']=='categorias'||$this->params['controller']=='busca')){
			return parent::url($url, $full);
		}
        if (is_array($url)) {
            $havekey = false;
            $haveint = false;
            $keyafterint = false;
            
            $cacheparts = array();
            $otherparts = array();
            foreach ($url as $key => $value) {
                if (is_int($key)) {
                    $haveint = true;
                    $otherparts[] = $value;
                } else {
                  $havekey = true;
                  if ($haveint) {
                      $keyafterint = true;  
                  }
                  $cacheparts[$key] = $value;
                }              
            }
            
            if (($havekey) && ($haveint) && (!$keyafterint)) {              
                $id = serialize($cacheparts);
                
                if (isset($this->urlcache[$id])) {
                  $url = $this->urlcache[$id];
                } else {
                  $url = parent::url($cacheparts, $full).'/';
                  $this->urlcache[$id] = $url;
                }
                
                foreach ($otherparts as $value) {
                   $url .= $value.'/';
                }                
                
                return $url;
            } else {
                return parent::url($url, $full);
            }          
        } else {
            return parent::url($url, $full);
        }
    }
}
