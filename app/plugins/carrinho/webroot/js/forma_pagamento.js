$(document).ready(function() {

    $(".loading").ajaxStart(function() {
        $(this).css("visibility", "visible");
        $(this).css("vertical-align", "middle");
    });
    $(".loading").ajaxComplete(function(event, request, settings) {
        $(this).css("visibility", "hidden");
        $(this).css("vertical-align", "middle");
    });
    $('.mask-data').setMask({
        mask: '39/9999'
    });
    $('.mask-numerico').setMask({mask: '9', type: 'repeat'});
    $('#PedidoCodigoSegurancaCartao').setMask({
        mask: '999'
    });
    $('#PedidoNumeroCartao').setMask({
        mask: '9999999999999999'
    });

    $('#fretes-disponiveis .tipo-entrega').live('click', function() {
        $.post(PATH.basename + "/carrinho/carrinho/set_frete/", {tipo_entrega: $('.tipo-entrega:checked').val()}, function(response) {
            if (response.dados) {
                $('#frete').html(response.dados.frete);
                $('#total').html(response.dados.total);
                $('#sub_total').html(response.dados.sub_total);

                try {
                    if (response.Cupom.status == true) {

                        $('.cupom-lbl').html(null);
                        $('.produto-totalizador,#total').attr('style', null);
                        $('.cupom-msg').html(response.Cupom.msg);
                        if (response.Cupom.tipo == "FRETE") {
                            $('#frete-com-desconto').html('R$ ' + response.Carrinho.dados.frete_com_desconto);
                            $('#total-com-desconto').html(response.Carrinho.dados.total_com_desconto);
                            $('#frete').css('text-decoration', 'line-through');
                        } else {
                            $.each(response.Carrinho.itens, function(i, v) {
                                $('#produto-total-desconto-' + v.id).html('R$ ' + v.preco_com_desconto);
                                $('#produto-total-' + v.id).css('text-decoration', 'line-through');
                            })
                            $('#total-com-desconto').html(response.Carrinho.dados.total_com_desconto);
                        }
                        $('#total').css('text-decoration', 'line-through');
                    } else {
                        $('.cupom-msg').html(response.Cupom.msg[0]);
                    }
                } catch (e) {
                }

            }


        }, "json");
    });


    $('.tipo-de-pagamento').click(function() {
        $('.forms-pagamentos').hide();
        $('.' + $(this).attr('rel')).css('display', 'block');
        $("html, body").animate({scrollTop:'680px'}, 'slow');
        var rel = $(this).attr('rel')
        $('.tipo_pagamento').attr('checked',false);
        switch (rel) {
            case 'PAGSEGURO':
                $('.PAGSEGURO .tipo_pagamento').attr('checked',true);
                break;
            case 'BOLETO':
                $('.BOLETO .tipo_pagamento').attr('checked','checked');
                break;
             case 'BCASH':
                $('.BCASH .tipo_pagamento').attr('checked','checked');
                break;
        }
    });
    
    if ($('.tipo-de-pagamento:checked').val()) {
        $('.' + $('.tipo-de-pagamento:checked').val()).show();
         var rel = $('.tipo-de-pagamento:checked').attr('rel');
         switch (rel) {
            case 'PAGSEGURO':
				$('.tipo_pagamento').attr('checked',false);
                $('.PAGSEGURO .tipo_pagamento').attr('checked',true);
                break;
            case 'BOLETO':
				$('.tipo_pagamento').attr('checked',false);
                $('.BOLETO .tipo_pagamento').attr('checked',true);
                break;
                  case 'BCASH':
				$('.tipo_pagamento').attr('checked',false);
                $('.BCASH .tipo_pagamento').attr('checked',true);
                break;
        }
    }

    $('.tipo_pagamento').click(function() {
        $.post(PATH.basename + "/carrinho/carrinho/ajax_forma_pagamento/" + $(this).val(), {}, function(data) {
            $(".PedidoParcelas").empty();
            $(".PedidoParcelas").append(data);
        }, "json");
    });
    
    $('.tipo_pagamento').click(function() {
        if ($(this).attr('rel') == 'AMEX') {
            $('#PedidoCodigoSegurancaCartao').setMask({mask: '9999'});
        } else {
            $('#PedidoCodigoSegurancaCartao').setMask({mask: '999'});
        }
    })
    if ($('.tipo_pagamento:checked').val()) {
        if ($('.tipo_pagamento:checked').attr('rel') == 'AMEX') {
            $('#PedidoCodigoSegurancaCartao').setMask({mask: '9999'});
        } else {
            $('#PedidoCodigoSegurancaCartao').setMask({mask: '999'});
        }
    }

    $('.btn-finalizar-compra').click(function() {
        $(this).hide();
        $('.gif_carregando').show();
    });
})