<?php

class CarrinhoComponent extends Object {

    /**
     */
    function __construct() {

		
        App::import("component", "Session");
        $this->Session = new SessionComponent();


        App::import("component", "RequestHandler");
        $this->RequestHandler = new RequestHandlerComponent();
        App::import("component", "Frete");
        $this->Frete = new FreteComponent();
		
		App::import("component", "CarrinhosAbandonados");
		$this->CarrinhosAbandonados = new CarrinhosAbandonadosComponent();
		
		App::import("helper", "Html");
        $this->Html = new HtmlHelper();

        App::import("model", "Produto");
        $this->Produto = new Produto();
    }
    
    function initialize(&$Controller) {
		$this->Controller = $Controller;
	}

    public function Adicionar($id) {

        /**
         * Se ja tiver o item no carrinho e estiver quantidades disponivel, 
         * incrementa em 1 a quantidade
         */
        
			$itens = $this->Session->read("Carrinho.itens");

			if($this->existeItemNoCarrinho($id)){
				$quantidade_pretendida = $itens[$id]['quantidade'] + 1;
			}else{
				$quantidade_pretendida = 1;
			}
			
            if ($this->verificaQuantidade($id, $quantidade_pretendida)) {
				$produto = $this->Produto->find("first", array(
													"fields" => "Produto.kit_desconto as produto_desc, Produto.id as produto_id, Produto.sku, Produto.referencia, Produto.codigo, Produto.codigo_externo, Produto.codigo_erp, ProdutoDescricao.nome,
																ProdutoDescricao.descricao,Produto.custo, Produto.preco, Produto.preco_promocao, Produto.quantidade, Produto.peso, Produto.peso_ramos, 
																Produto.garantia, Produto.largura, Produto.altura, Produto.profundidade, Produto.fabricante_id, Produto.observacao, Produto.unidade_venda, 
																Produto.quantidade_disponivel, Produto.quantidade_alocada, Produto.preco_promocao_inicio, Produto.preco_promocao_fim, 
																Produto.preco_promocao_novo, Produto.preco_promocao_velho, Produto.tempo_producao, Produto.marca, Produto.kit,
																(SELECT concat('uploads/produto_imagem/filename/',ProdutoImagem.filename) FROM produto_imagens ProdutoImagem WHERE ProdutoImagem.produto_id = ". $id ." LIMIT 1) as imagem
																",
													"joins" => array(
																	array(
																		'table' => 'produto_descricoes',
																		'alias' => 'ProdutoDescricao',
																		'type' => 'left',
																		'conditions' => array('ProdutoDescricao.produto_id = Produto.id')
																	)
													),
													"contain"=>array("Categoria","VariacaoProduto"=>array("Variacao"),"AtributoProduto"=>array("Atributo"=>array("AtributoTipo")),"ProdutoPreco"),
													"conditions" => array("Produto.id" => $id, "ProdutoDescricao.language" => Configure::read('Config.language'))
													)
												);
				//adiciona com quantidade
				$produto['Produto']['quantidade'] = $quantidade_pretendida;
				
				//nome e descricao na lingua default
				$produto['Produto']['nome'] = $produto['ProdutoDescricao']['nome'];
				$produto['Produto']['descricao'] = $produto['ProdutoDescricao']['descricao'];
				
				$item = $produto['Produto'];
				$item['imagem'] = $produto[0]['imagem'];
				//$item['agrupador'] = $produto[0]['agrupador'];


				if($item['produto_desc'] > 0){
                    $desconto = $item['preco'] * $item['produto_desc'] / 100;
                    $result = $item['preco'] - $desconto;

					$item['preco'] =  $result;
				}

                if( isset($produto['ProdutoPreco'][0]) ){
                    $item['ProdutoPreco'] =  $produto['ProdutoPreco'][0];
                }

				foreach( $produto['AtributoProduto'] as $atributos ){
					if($atributos['Atributo']['AtributoTipo']['nome'] !=""){
						$item['ProdutoAtributos'][$atributos['Atributo']['AtributoTipo']['nome']] =  $atributos['Atributo']['valor'];
					}
				}
				
				foreach( $produto['VariacaoProduto'] as $variacoes ){
					if(isset($variacoes['valor'])){
						$item['VariacaoProduto'][$variacoes['valor']] = $variacoes['Variacao']['nome'];
					}
				}
				foreach( $produto['Categoria'] as $categoria ){
					$item['Categoria'][$categoria['id']] = $categoria['seo_url'];					
				}	
				
				//seta o produto na sessao existente com ou sem produtos.
				$itens[$id] = $item;
				//adiciona item na sessao os itens
				$this->Session->write("Carrinho.itens", $itens);
				
				//debug($itens);die;
				
            }else{
				return false;
			}
			
			//setCarrinhoAbandonado
			$this->setCarrinhoAbandonado($id, $quantidade_pretendida);
			
		return true;
    }
	
	
	public function AdicionarAjax($id, $quantidade = 1) {

        /**
         * Se ja tiver o item no carrinho e estiver quantidades disponivel, 
         * incrementa em 1 a quantidade
         */
        
			$itens = $this->Session->read("Carrinho.itens");

			if($this->existeItemNoCarrinho($id)){
				$quantidade_pretendida = $itens[$id]['quantidade'] + $quantidade;
			}else{
				$quantidade_pretendida = $quantidade;
			}

            if ($this->verificaQuantidade($id, $quantidade_pretendida)) {
				
				$produto = $this->Produto->find("first", array(
																"fields" => "Produto.id as produto_id, Produto.sku, Produto.referencia, Produto.codigo, Produto.codigo_externo, Produto.codigo_erp,
																			ProdutoDescricao.nome, ProdutoDescricao.descricao,Produto.custo, Produto.preco, Produto.preco_promocao, Produto.quantidade, 
																			Produto.peso, Produto.peso_ramos, Produto.garantia, Produto.largura, Produto.altura, Produto.profundidade, 
																			Produto.fabricante_id, Produto.observacao, Produto.unidade_venda, Produto.quantidade_disponivel, Produto.quantidade_alocada,
																			Produto.preco_promocao_inicio, Produto.preco_promocao_fim, Produto.preco_promocao_novo, Produto.preco_promocao_velho, 
																			Produto.tempo_producao, Produto.marca, Produto.kit,
																			(SELECT concat('uploads/produto_imagem/filename/',ProdutoImagem.filename) FROM produto_imagens ProdutoImagem WHERE ProdutoImagem.produto_id = ". $id ." LIMIT 1) as imagem",
																"joins" => array(
																				array(
																					'table' => 'produto_descricoes',
																					'alias' => 'ProdutoDescricao',
																					'type' => 'left',
																					'conditions' => array('ProdutoDescricao.produto_id = Produto.id')
																				)
																),
																"contain"=>array(
																			"Categoria","VariacaoProduto"=>array("Variacao"),
																			"AtributoProduto"=>array("Atributo"=>array("AtributoTipo")),"ProdutoPreco"),
																"conditions" => array("Produto.id" => $id, "ProdutoDescricao.language" => Configure::read('Config.language'))
																)
												);

				//adiciona com quantidade
				
				$item = $produto['Produto'];
				$item['imagem'] = $produto[0]['imagem'];
				//$item['agrupador'] = $produto[0]['agrupador'];
				$item['quantidade'] = $quantidade_pretendida;
				
				//nome e descricao na lingua default
				$produto['Produto']['nome'] = $produto['ProdutoDescricao']['nome'];
				$produto['Produto']['descricao'] = $produto['ProdutoDescricao']['descricao'];
				
				if( isset($produto['ProdutoPreco'][0]) ){
					$item['ProdutoPreco'] =  $produto['ProdutoPreco'][0];
				}
				
				foreach( $produto['AtributoProduto'] as $atributos ){
					if($atributos['Atributo']['AtributoTipo']['nome'] !=""){
						$item['ProdutoAtributos'][$atributos['Atributo']['AtributoTipo']['nome']] =  $atributos['Atributo']['valor'];
					}
				}
				
				foreach( $produto['VariacaoProduto'] as $variacoes ){
					if($variacoes['valor']){
						$item['VariacaoProduto'][$variacoes['valor']] = $variacoes['Variacao']['nome'];
					}
				}
				foreach( $produto['Categoria'] as $categoria ){
					$item['Categoria'][$categoria['id']] = $categoria['seo_url'];										
				}	
				
				//seta o produto na sessao existente com ou sem produtos.
				$itens[$id] = $item;
				
				//adiciona item na sessao os itens
				$this->Session->write("Carrinho.itens", $itens);		

				//setCarrinhoAbandonado
				$this->setCarrinhoAbandonado($id, $quantidade_pretendida);

            }else{
				return false;
			}
			
		return true;
    }
	
     /**
     * Remove o item do carrinho
     * a partir do id recebido
     *
     * @param int $produto_id
     * @return boolean Removido?
     */
    public function Remover($id) {

        //verifica se a requisição é AJAX
        if ($this->RequestHandler->isAjax()) {
            $produto_id = $this->params['form']['produto_id'];
        } else {
            $produto_id = $id;
        }
		
		$this->CarrinhosAbandonados->remove($produto_id);
        $this->Session->delete("Carrinho.itens.{$produto_id}");
        return!$this->Session->check("Carrinho.itens.{$produto_id}");
    }
	
	public function RemoverAjax($id) {
		
		$this->CarrinhosAbandonados->remove($id);
        $this->Session->delete("Carrinho.itens.{$id}");
        return!$this->Session->check("Carrinho.itens.{$id}");
    }

    /**
     * Retira todos os itens do carrinho
     * e refaz os dados
     *
     * @author Luan Garcia
     */
    public function Esvaziar() {
        $this->Session->write("Carrinho.dados", array("total" => 0, "subtotal" => 0, "frete" => 0,"cupom_total_desconto"=>0,"sub_total_com_desconto"=>0, "total_com_desconto" => 0, "frete_com_desconto" => 0, "prazo_entrega" => 0, "tipo_entrega" => null, "cep" => null,"endereco_entrega"=>null,"step"=>null));
        $this->Session->write("Carrinho.itens", array());
        $this->Session->write("Carrinho.pedido_id", null);
		$this->Session->write("Carrinho.pedido_abandonado", null);
		$this->CarrinhosAbandonados->destroy();
    }

    /**
     * @param int $produto_id Verifica se existe um item no carrinho
     * @return boolean Existe?
     */
    public function existeItemNoCarrinho($produto_id) {
        $itens = $this->Session->read("Carrinho.itens");
        //nao tem nenhum item no carrinho
        if (array_key_exists($produto_id, $itens)) {
            return true;
        }
        return false;
    }
	
	/**
     * Retorna o valor total
     * @return <Array> array com os valores do pedido e frete
     */
    function getCarrinhoTotal(){

        $total = 0;
        $frete = $this->Session->read("Carrinho.dados.frete");
        $sub_total = 0;		
        foreach ($this->getItens() as $produto):
			$valor = $produto['preco_promocao'] > 0 ? $produto['preco_promocao'] : $produto['preco'];
            $sub_total += $valor * $produto['quantidade'];
        endForeach;
		$dados['itens'] = $this->getItens();
		$dados['frete'] = $frete;
        $dados['total'] = ($sub_total + $frete);
        $dados['sub_total'] = $sub_total;
        return $dados;
    }

    /**
     * Troca a quantidade atual, pela quantidade
     * fornecida do item com o id passado no parametro 1
     *
     * @param int $produto_id
     * @param int $quantidade
     * @return boolean Alterou?
     */
    public function Quantidade($produto_id, $quantidade) {
        App::import("helper", "String");
        $this->String = new StringHelper();

        if (!$this->Session->read("Carrinho.itens.{$produto_id}")||$quantidade<=0)return false;
        //verifica se a quantidade solicitada está disponível
        if ($this->verificaQuantidade($produto_id, $quantidade)) {
            //seta a quantidade solicitada.
            $this->Session->write("Carrinho.itens.{$produto_id}.quantidade", $quantidade);

            //se a quantidade passada for igual a da sessao, alterou a quantidade
            if ($quantidade == $this->Session->read("Carrinho.itens.{$produto_id}.quantidade")) {
                //retorna o recalculo dos dados do carrinho
                $retorno = $this->recalcularCarrinho();
                //valor total do produto alterado
                $valor = $this->Session->read("Carrinho.itens.{$produto_id}.preco_promocao") > 0 ? $this->Session->read("Carrinho.itens.{$produto_id}.preco_promocao") : $this->Session->read("Carrinho.itens.{$produto_id}.preco");
                $retorno['produto_total'] = $this->String->bcotoMoeda($valor * $quantidade);

				//setCarrinhoAbandonado
				$this->setCarrinhoAbandonado($produto_id, $quantidade);
			
				return $retorno;
            }
        } else {
            return false;
        }
    }

    /**
     * Retorna um item que esta no carrinho
     *
     * @param Int $produto_id
     * @return array
     */
    public function getItem($produto_id) {
		//debug($this->Session->read("Carrinho.itens.{$produto_id}"));
        return $this->Session->read("Carrinho.itens.{$produto_id}");
    }

    /**
     * Retorna os itens que estão no carrinho
     *
     * @return array
     */
    public function getItens() {
		return $this->Session->read("Carrinho.itens");
    }
    /**
     * Retorna o carrinho de compras do cliente
     *
     * @return array
     */
    public function getCarrinho() {
        return $this->Session->read("Carrinho");
    }

    /**
     * Retorna o valor total de um item
     * @param Int $produto_id
     * @return array
     */
    public function getTotalItem($produto_id) {
        $item = $this->getItem($produto_id);

        $valor = empty($item['preco_promocao']) ? $item['preco'] : $item['preco_promocao'];
        $valor_total = $item['quantidade'] * $valor;
        return $valor_total;
    }

   

    /**
     * Verifica se a quantidade pretentida está disponivel em estoque
     * @param Int $id produto_id
     * @param Int $quantidade quantidade pretendida
     * @return Boolean
     */
    public function verificaQuantidade($id, $quantidade) {
	
		$return = true;
		
		$produto = $this->Produto->find('first', array('recursive' => -1, 'fields' => array('Produto.id', 'Produto.quantidade_disponivel', 'Produto.kit'), 'conditions' => array('Produto.id' => $id)));
		
		if($produto['Produto']['kit'] == false){
			if($produto['Produto']['quantidade_disponivel'] < $quantidade){
				$return = false;
			}
		}else{
			
			App::import('Model', 'ProdutoKit');
			$this->ProdutoKit = new ProdutoKit();
			$produtos_kit = $this->ProdutoKit->find('all', array('recursive' => -1, 'conditions' => array('ProdutoKit.kit_id' => $id)));
			if($produtos_kit){
			
				foreach($produtos_kit as $produto_kit){
					$produto = $this->Produto->find('first', array('recursive' => -1, 'fields' => array('Produto.id', 'Produto.quantidade_disponivel'), 'conditions' => array('Produto.id' => $produto_kit['ProdutoKit']['produto_id'])));
					
					//if($produto && $produto['Produto']['quantidade_disponivel'] < ((int)$quantidade+(int)$produto_kit['ProdutoKit']['quantidade'])){
					if($produto && $produto['Produto']['quantidade_disponivel'] < ((int)$quantidade)){
						$return = false;
					}
				}
			}
		}
		
        return $return;
		
    }

    /**
     * Realiza o recalculo total dos valores carrinho, utilizado após alguma alteração de quantidade ou frete
     * @return Array
     */
    public function recalcularCarrinho() {
		
        App::import("helper", "String");
        $this->String = new StringHelper();

        $total = 0;
        $frete = $this->Session->read("Carrinho.dados.frete");
        $sub_total = 0;
		/**************************
		****
		**** IMPORTANTE OS DESCONTOS NÃO PODEM ACUMULAR, EX: CUPOM DE DESCONTO MAIS TABELA DE PREÇO
		****
		******/
		/*adiciona o desconto por tabela de preço se não tiver cupom de desconto*/
		//$cupom = $this->Cupom->getCupom();
		//if(!$cupom){
			foreach ($this->getItens() as $produto): //var_dump($produto);
				$this->calculaTabelaPreco($produto['produto_id'],$produto['quantidade']);
			endForeach;
		//}
        foreach ($this->getItens() as $produto):
            $valor = $produto['preco_promocao'] > 0 ? $produto['preco_promocao'] : $produto['preco'];
            $sub_total += $valor * $produto['quantidade'];
        endForeach;


        $totais['frete'] = $this->String->bcotoMoeda($frete);
        $totais['total'] = $this->String->bcotoMoeda($sub_total + $frete);
        $totais['sub_total'] = $this->String->bcotoMoeda($sub_total);

        $this->Session->write("Carrinho.dados.total", $sub_total + $frete);
        $this->Session->write("Carrinho.dados.subtotal", $sub_total);
        $this->Session->write("Carrinho.dados.frete", $frete);

        return $totais;
    }
	
	/***
	 * recalcula a tabela de preço e sobreescre o carrinho
	 **/
	public function calculaTabelaPreco($produto_id,$quantidade_pretendida){
		
		$produto = $this->Produto->find("first", array(
			"fields" => "Produto.kit_desconto as produto_desc, Produto.id as produto_id, Produto.sku, Produto.referencia, Produto.codigo, Produto.codigo_externo, Produto.codigo_erp, ProdutoDescricao.nome, ProdutoDescricao.descricao,Produto.custo, Produto.preco, Produto.preco_promocao, Produto.quantidade, Produto.peso, Produto.peso_ramos, Produto.garantia, Produto.largura, Produto.altura, Produto.profundidade, Produto.fabricante_id, Produto.observacao, Produto.unidade_venda, Produto.quantidade_disponivel, Produto.quantidade_alocada, Produto.preco_promocao_inicio, Produto.preco_promocao_fim, Produto.preco_promocao_novo, Produto.preco_promocao_velho, Produto.tempo_producao, Produto.marca, Produto.kit,
			(SELECT concat('uploads/produto_imagem/filename/',ProdutoImagem.filename) FROM produto_imagens ProdutoImagem WHERE ProdutoImagem.produto_id = ". $produto_id ." LIMIT 1) as imagem",
			"contain"=>array("Categoria","VariacaoProduto"=>array("Variacao"),"ProdutoPreco"=>array("conditions"=>array("ProdutoPreco.status"=>true)), "AtributoProduto"=>array("Atributo"=>array("AtributoTipo"))),
			"joins" => array(
							array(
								'table' => 'produto_descricoes',
								'alias' => 'ProdutoDescricao',
								'type' => 'left',
								'conditions' => array('ProdutoDescricao.produto_id = Produto.id')
							)
			),
			"conditions" => array("Produto.id" => $produto_id)));
			
		$itens = $this->Session->read("Carrinho.itens");
		if($produto['ProdutoPreco']){					
			$tabela_menor = null;
			$first = true;
			
			foreach($produto['ProdutoPreco'] as $tabela){
				
				if(($quantidade_pretendida>=$tabela['quantidade'] && ($tabela['preco'] < $tabela_menor['preco'] || $first==true))){
					$first = false; 
					$tabela_menor = $tabela;
				}						
			}
			//se tiver o menor preco relativo a parcela passada
			if($tabela_menor){
				$produto['Produto']['preco'] = $tabela_menor['preco'];
				$produto['Produto']['preco_promocao'] = 0;
			}
		}
		$produto_old = $itens[$produto_id];
		$produto['Produto']['quantidade'] = $produto_old['quantidade'];
		
		//nome e descricao na lingua default
		$produto['Produto']['nome'] = $produto['ProdutoDescricao']['nome'];
		$produto['Produto']['descricao'] = $produto['ProdutoDescricao']['descricao'];
		
		$item = $produto['Produto'];
		$item['imagem'] = $produto[0]['imagem'];
		//$item['agrupador'] = $produto[0]['agrupador'];

		if( isset($produto['ProdutoPreco'][0]) ){
			$item['ProdutoPreco'] =  $produto['ProdutoPreco'][0];
		}

        if($item['produto_desc'] > 0){
            $desconto = $item['preco'] * $item['produto_desc'] / 100;
            $result = $item['preco'] - $desconto;

            $item['preco'] =  $result;
        }
				
		foreach( $produto['AtributoProduto'] as $atributos ){
			if($atributos['Atributo']['AtributoTipo']['nome'] !=""){
				$item['ProdutoAtributos'][$atributos['Atributo']['AtributoTipo']['nome']] =  $atributos['Atributo']['valor'];
			}
		}
		//debug($produto);
		foreach( $produto['VariacaoProduto'] as $variacoes ){
			if($variacoes['Variacao']['valor']){
				$item['Variacao'][$variacoes['Variacao']['valor']] = $variacoes['Variacao']['valor'];
			}
		}
		foreach( $produto['Categoria'] as $categoria ){
			$item['Categoria'][$categoria['id']] = $categoria['seo_url'];						
		}	
		$itens[$produto_id] = $item;				
		//adiciona item na sessao os itens
		
		$this->Session->write("Carrinho.itens", $itens);	
	}
    /**
     * Consulta o prazo e valor de entrega e salva na sessão.
     * @param String CEP $cep de destino
     * @param String TIPO  $tipo tipo de entrega
     */
    public function setFrete($cep, $tipo) {
        App::import("helper", "String");
        $this->String = new StringHelper();
        $cep = preg_replace('/[^0-9]/', '', $cep);
		//pega os fretes consultados na sessão;
		$frete = set::extract("/Frete[id={$tipo}]",$this->Session->read("Carrinho.fretes"));
		
		if ($frete) {
            $this->Session->write("Carrinho.dados.frete", $frete[0]['Frete']['preco']);
            $this->Session->write("Carrinho.dados.prazo_entrega", $frete[0]['Frete']['prazo']);
            $this->Session->write("Carrinho.dados.tipo_entrega", $frete[0]['Frete']['id']);
            $this->Session->write("Carrinho.dados.cep", $cep);
			$retorno['dados'] = $this->recalcularCarrinho(false);
            return $retorno;
        }

        return false;
    }
	/**
     * Limpar frete
     */
    public function limparFrete() {
        $this->Session->write("Carrinho.dados.tipo_entrega",null);
        $this->Session->write("Carrinho.dados.prazo_entrega",0);
        $this->Session->write("Carrinho.dados.frete",0);
    }
    
	/**
     * Consulta os meios de entrega disponiveis para o cep e peso passado.
     * @param String CEP $cep de destino
     */
    public function consultarFretesDisponiveis($cep, $produto=null) {
        $cep = preg_replace('/[^0-9]/', '', $cep);
		if($produto==null)
			$fretes =  $this->Frete->consultarFretesDisponiveis($cep,$this->getCarrinhoTotal());
		else
			$fretes =  $this->Frete->consultarFretesDisponiveis($cep,$produto);
		$this->Session->write("Carrinho.fretes", $fretes);
		return $fretes;
    }
    public function consultaCep($cep) {
        $cep = preg_replace('/[^0-9]/', '', $cep);
        return $this->Frete->consultaEndereco($cep);
    }

    public function getCep() {
        return $this->Session->read("Carrinho.dados.cep");
    }
     public function getPrazo() {
        return $this->Session->read("Carrinho.dados.prazo_entrega");
    }

    /**
     * Seta o cep a ser utilizado para entrega
     * @param String $cep
     * @return Boolean
     */
    public function setCep($cep) {
        return $this->Session->write("Carrinho.dados.cep", $cep);
    }
    /**
     * Seta o id do pedido finalizado para busca dos dados na finalizacao do pedido
     * @param Int $id id do pedido
     * @return Boolean
     */
    public function setPedido($id) {
        return $this->Session->write("Carrinho.pedido_id", $id);
    }
    /**
     * Retorna o id do pedido finalizado para busca dos dados na finalizacao do pedido
     * @return Int
     */
    public function getPedido() {
        return $this->Session->read("Carrinho.pedido_id");
    }

    public function getFrete() {
        return $this->Session->read("Carrinho.dados.tipo_entrega");
    }

    public function getStep() {
        return $this->Session->read("Carrinho.dados.step");
    }

    /**
     * Seta o passo que o usuário está no fluxo da compra.
     * @return Int passo atual do usuário usado no fluxo da compra.
     */
    public function setStep($step) {
        return $this->Session->write("Carrinho.dados.step", $step);
    }

    /**
     * Seta o passo que o usuário está no fluxo da compra.
     */
    public function setEnderecoEntrega($id) {
        return $this->Session->write("Carrinho.dados.endereco_entrega", $id);
    }

    /**
     * Retorna o endereço de entrega selecionado pelo cliente
     */
    public function getEnderecoEntrega() {
        return $this->Session->read("Carrinho.dados.endereco_entrega");
    }
	
	private function setCarrinhoAbandonado($produto_id, $quantidade_pretendida){	
		//CarrinhoAbandonado
		$arrayCookie = array();
		$arrayCookie['produto_id'] = $produto_id;
		$arrayCookie['quantidade'] = $quantidade_pretendida;
		$this->CarrinhosAbandonados->add(serialize($arrayCookie));
		
		//model CarrinhoAbandonado
		App::import('Model','CarrinhoAbandonado');
		$this->CarrinhoAbandonado = new CarrinhoAbandonado();
		
		$u = $this->Controller->Auth->User();
		//dados usuarios
		$data['usuario_id'] = $u['Usuario']['id'];
		$data['produtos'] = $this->CarrinhosAbandonados->getAll();
		
		//atualizo os registros do banco, cada vez que adiciono produto no carrinho
		if($this->Session->read("Carrinho.carrinho_abandonado") > 0){
			$this->CarrinhoAbandonado->salvar($data, $this->Session->read("Carrinho.carrinho_abandonado"));
		}else{
			$this->CarrinhoAbandonado->salvar($data);
		}		
	}

}