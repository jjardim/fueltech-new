<?php

class CarrinhoController extends CarrinhoAppController {

    var $uses = Array('Produto', 'Pedido');
    var $components = array('RequestHandler', 'Carrinho.Cupom', 'PagSeguro.PagSeguro', 'Email', 'Carrinho.Carrinho', 'Session', 'CarrinhosAbandonados', 'DinamizeIntegracao', 'Cielo');
    var $helpers = array('Calendario', 'String', 'Image', 'Javascript', 'Estados');

    public function beforeFilter() {
        parent::beforeFilter();

        if (!$this->Carrinho->getItens()) {
            $this->Carrinho->Esvaziar();
        }

        if ($this->Session->read('linguagem_default') != Configure::read('Config.language')):
            App::import("helper", "Html");
            $this->Html = new HtmlHelper();
            $this->redirect('../');
        endIf;
    }

    /**
     * Seta o cupom de desconto
     * @param String $cupom cupom de desconto
     */
    public function set_cupom($cupom = null) {

        $retorno = false;
        $this->render(false);
        $this->layout = false;

        if (count($this->Carrinho->getItens()) <= 0) {
            die(json_encode(false));
        }

        if ($this->RequestHandler->isAjax()) {
            if (isset($this->params['form']['cupom'])) {
                $cupom = $this->params['form']['cupom'];
            }
        }

        if (!$cupom) {
            $cupom = $this->Cupom->getCupom();
        }

        if ($cupom) {
            $retorno = $this->Cupom->setCupom($cupom);
        }
        if ($this->RequestHandler->isAjax()) {
            die(json_encode($retorno));
        } else {
            return $retorno;
        }
    }

    private function recalcula_cupom() {
        $cupom = $this->Cupom->getCupom();
        if ($cupom) {
            return $this->Cupom->setCupom($cupom["dados"]["cupom"]);
        }
        return false;
    }

    public function remove_cupom() {
        $this->render(false);
        $this->Cupom->removeCupom();
        $this->redirect("/carrinho");
    }

    /**
     * Adiciona o produto no carrinho
     */
    public function add($id = null) {
        $this->Carrinho->limparFrete();
        $this->Carrinho->Adicionar($id);
        $this->redirect('/carrinho');
    }

    /**
     * Adiciona o produto no carrinho
     */
    public function add_ajax() {
        $this->layout = 'ajax';
        if ($this->Carrinho->getItens()) {
            $this->set('data', $this->Carrinho->getItens());
        }
    }

    public function add_model_detalhe($id, $quantidade = 1) {
        $this->layout = 'ajax';
        if ($this->Carrinho->AdicionarAjax($id, $quantidade)) {
            if ($this->Carrinho->getItens()) {
                $this->Carrinho->setStep(1);
                $this->set('data', $this->Session->read("Carrinho.itens.{$id}"));
                $this->set('title', 'Produto adicionado ao carrinho com sucesso!');
                $this->set('error', 'false');
            }
        } else {
            $this->set('title', 'O produto não pode ser adicionado ao carrinho.');
            $this->set('produto_id', $id);
            $this->set('error', 'true');
        }
    }

    public function add_model_voltagem($id, $voltagem) {
        $this->layout = 'ajax';
        $produtos = $this->Produto->find('first', array(
            'fields' => array('Produto.nome'),
            'contain' => array('ProdutoImagem'),
            'conditions' => array('Produto.id' => $id),
            'recursive' => 1
                )
        );
        $data['id'] = $id;
        $data['voltagem'] = $voltagem;
        $data['nome'] = $produtos['Produto']['nome'];
        $data['imagem'] = $produtos['ProdutoImagem'][0]['dir'] . '/' . $produtos['ProdutoImagem'][0]['filename'];
        $this->set('data', $data);
    }

    public function get_itens_ajax($produto_agrupador = null) {
        $this->layout = 'ajax';
        $this->render('add_ajax');
        if ($this->Carrinho->getItens()) {

            $dt = "";
            if ($produto_agrupador != null) {
                foreach ($this->Carrinho->getItens() as $data) {
                    if ($data['agrupador'] == $produto_agrupador) {
                        $dt[] = $data;
                    }
                }
                $this->set('data', $dt);
            } else {
                $this->set('data', $this->Carrinho->getItens());
            }

            $this->render('add_ajax');
        }
    }

    /**
     * Adiciona o produto no carrinho
     */
    public function quantidade($id, $quantidade) {
        $this->layout = false;
        $this->render(false);
        $this->Carrinho->limparFrete();
        $retorno = $this->Carrinho->Quantidade($id, $quantidade);
        $tem_cupom = $this->recalcula_cupom();
        //se tiver cupom, pega os valores totais já com desconto para exibicao
        if ($retorno && isset($tem_cupom['Carrinho']['dados'])) {
            $produto_total = $retorno['produto_total'];
            $retorno = $tem_cupom['Carrinho']['dados'];
            $retorno['produto_total'] = $produto_total;
            $retorno['Cupom'] = $tem_cupom['Cupom'];
            $retorno['Carrinho'] = $tem_cupom['Carrinho'];
        }
        if ($this->RequestHandler->isAjax()) {
            die(json_encode($retorno));
        } else {
            return $retorno;
        }
    }

    /**
     * Remove o produto do carrinho
     */
    public function remove($id = null) {
        $this->Carrinho->limparFrete();
        $this->Carrinho->Remover($id);
        $this->redirect('/carrinho');
    }

    /**
     * Limpa os produtos do carrinho
     */
    public function limpar() {
        $this->Carrinho->Esvaziar();
        $this->redirect('/carrinho');
    }

    /**
     * Lista os produtos do carrinho
     */
    public function index() {

//        var_dump($this->Session->read("Carrinho"));

        if ($this->Session->read('linguagem_default') != Configure::read('Config.language')):
            App::import("helper", "Html");
            $this->Html = new HtmlHelper();
            $this->redirect('../');
        endIf;


        if ($this->Carrinho->getItens()) {
            $this->Carrinho->setStep(1);
            $this->set('data', $this->Carrinho->getItens());
            $totais = $this->Carrinho->recalcularCarrinho();
            $tem_cupom = $this->recalcula_cupom();
            //se tiver cupom, pega os valores totais já com desconto para exibicao
            if (isset($tem_cupom['Carrinho']['dados'])) {
                $this->set('totais', $tem_cupom['Carrinho']['dados']);
                $this->data['Cupom']['cupom'] = $tem_cupom['Cupom']['cupom'];
                $this->data['Cupom']['msg'] = $tem_cupom['Cupom']['msg'];
            } else {
                $this->set('totais', $totais);
            }
            //seta os valores da sessao
            $cep = $this->Carrinho->getCep();
            $this->set('frete_tipos', $this->Carrinho->consultarFretesDisponiveis($cep));
            $this->data['Frete']['cep'] = $cep;
            $this->data['Frete']['frete'] = $this->Carrinho->getFrete();
        }
        //start breadcrumb
        // $breadcrumb = "";
        // $breadcrumb[] = array('url' => 'javascript:void(0);', 'nome' => "Carrinho de compras" );
        // $this->set('breadcrumb', $breadcrumb);
        //end breadcrumb
    }

    /**
     * Seta o tipo de entrega
     * @param String $cep cep destino
     * @param String $tipo tipo de envio
     */
    public function get_frete($cep = null) {
        $this->layout = 'ajax';
        $this->set('frete_tipos', $this->Carrinho->consultarFretesDisponiveis($cep));
    }

    /**
     * Seta o tipo de entrega no detalhe do produto
     * @param String $cep cep destino
     */
    public function get_frete_em_detalhes($cep = null, $produto_id = null) {
        $this->layout = 'ajax';

        $produtos = $this->Produto->find('first', array(
            'fields' => array('Produto.peso', 'Produto.peso_ramos', 'Produto.altura', 'Produto.largura', 'Produto.quantidade',
                'Produto.profundidade', 'Produto.preco_promocao', 'Produto.preco', 'Produto.preco_promocao_inicio',
                'Produto.preco_promocao_fim', 'Produto.preco_promocao_novo', 'Produto.preco_promocao_velho', 'Produto.tempo_producao'),
            'conditions' => array('Produto.status' => true, 'Produto.id' => $produto_id),
            'recursive' => '-1'
                )
        );

        $produto['itens'][$produto_id]['id'] = $produto_id;
        $produto['itens'][$produto_id]['altura'] = $produtos['Produto']['altura'];
        $produto['itens'][$produto_id]['peso'] = $produtos['Produto']['peso'];
        $produto['itens'][$produto_id]['peso_ramos'] = $produtos['Produto']['peso_ramos'];
        $produto['itens'][$produto_id]['largura'] = $produtos['Produto']['largura'];
        $produto['itens'][$produto_id]['quantidade'] = $produtos['Produto']['quantidade'];
        $produto['itens'][$produto_id]['profundidade'] = $produtos['Produto']['profundidade'];
        $produto['itens'][$produto_id]['preco_promocao'] = $produtos['Produto']['preco_promocao'];
        $produto['itens'][$produto_id]['preco'] = $produtos['Produto']['preco'];
        $produto['itens'][$produto_id]['preco_promocao_inicio'] = $produtos['Produto']['preco_promocao_inicio'];
        $produto['itens'][$produto_id]['preco_promocao_fim'] = $produtos['Produto']['preco_promocao_fim'];
        $produto['itens'][$produto_id]['preco_promocao_novo'] = $produtos['Produto']['preco_promocao_novo'];
        $produto['itens'][$produto_id]['preco_promocao_velho'] = $produtos['Produto']['preco_promocao_velho'];
        $produto['itens'][$produto_id]['tempo_producao'] = $produtos['Produto']['tempo_producao'];

        $produto['itens'][$produto_id]['quantidade'] = 1;

        $this->set('frete_disponiveis', $this->Carrinho->consultarFretesDisponiveis($cep, $produto));
    }

    /**
     * Seta o tipo de entrega
     * @param String $cep cep destino
     * @param String $tipo tipo de envio
     */
    public function set_frete($cep = null, $tipo = null) {
        $this->layout = false;

        if ($this->RequestHandler->isAjax()) {
            if (isset($this->params['form']['cep'])) {
                $cep = $this->params['form']['cep'];
            } else {
                $cep = $this->Carrinho->getCep();
            }
            if (isset($this->params['form']['tipo_entrega'])) {
                $tipo = $this->params['form']['tipo_entrega'];
            } else {
                $tipo = $this->Carrinho->getFrete();
            }
        }

        $retorno = $this->Carrinho->setFrete($cep, $tipo);
        $tem_cupom = $this->recalcula_cupom();
        //se tiver cupom, pega os valores totais já com desconto para exibicao
        if ($retorno && isset($tem_cupom['Carrinho']['dados'])) {
            $retorno = $tem_cupom['Carrinho'];
            $retorno['Cupom'] = $tem_cupom['Cupom'];
            $retorno['Carrinho'] = $tem_cupom['Carrinho'];
        }

        if ($this->RequestHandler->isAjax()) {
            die(json_encode($retorno));
        } else {
            return $retorno;
        }
    }

    public function set_tipo_pagamento($tipo) {
        $this->layout = false;
        App::import("helper", "String");
        $this->String = new StringHelper();


        App::import("model", "PagamentoTipo");
        $this->PagamentoTipo = new PagamentoTipo();

        App::import("model", "PagamentoCondicao");
        $this->PagamentoCondicao = new PagamentoCondicao();
        $carrinho = $this->Carrinho->getCarrinho();

        $condicao = $this->PagamentoCondicao->find('first', array('conditions' => array('PagamentoCondicao.id' => $tipo)));

        $pagamento_tipo = $this->PagamentoTipo->find('first', array('conditions' => array('PagamentoTipo.id' => $condicao['PagamentoCondicao']['pagamento_tipo_id'], 'PagamentoTipo.status' => true)));
        //se o tipo de pagamento tiver desconto
        $desconto_meio_pagamento = 0;

        if ($this->String->moedaToBco($pagamento_tipo['PagamentoTipo']['desconto']) > 0) {
            $desconto = ($carrinho['dados']['subtotal'] * $this->String->moedaToBco($pagamento_tipo['PagamentoTipo']['desconto'])) / 100;
            //se o valor do desconto for maior que o valor do produto
            if ($desconto > $carrinho['dados']['subtotal']) {
                $desconto_meio_pagamento = $carrinho['Dados']['subtotal'];
            } else {
                $desconto_meio_pagamento = $desconto;
            }
        }
        $retorno = $this->Carrinho->setDescontoMeioPagto($desconto_meio_pagamento);
        if ($this->RequestHandler->isAjax()) {
            die(json_encode($retorno));
        } else {
            return $retorno;
        }
    }

    /**
     * Define a forma de pagamento da compra
     */

    /**
     * Define a forma de pagamento da compra
     */
    public function forma_pagamento() {
        $this->loadModel("UsuarioEndereco");
        $this->loadModel("PagamentoTipo");
        $this->loadModel("PagamentoCondicao");
        $this->loadModel("PagamentoCondicaoImagem");
		$this->loadModel("Produto");
		$this->loadModel("ProdutoKit");
        $this->loadModel("Pedido");
        $this->loadModel("PedidoItem");
		
		App::import("helper", "String");
        $this->String = new StringHelper();

        if (!$this->Auth->User() || $this->Carrinho->getStep() < 1) {
            $this->redirect(array('plugin' => 'carrinho', 'controller' => 'carrinho', 'action' => 'index'));
        }
        $endereco_entrega = $this->UsuarioEndereco->find('first', array('conditions' => array('UsuarioEndereco.id' => $this->Carrinho->getEnderecoEntrega())));
        if (!$endereco_entrega) {
            $this->Session->setFlash('Paramentros inválidos', 'flash/error');
            $this->redirect(array('plugin' => 'carrinho', 'controller' => 'carrinho', 'action' => 'entrega'));
        }
        $this->set('endereco', $endereco_entrega);

        $pagamento_tipo = $this->PagamentoTipo->find('all', array('recursive' => 2, 'conditions' => array('PagamentoTipo.status' => true)));

        $this->set('pagamento_tipo', $pagamento_tipo);

        $carrinho = $this->Carrinho->getCarrinho();

        //valida o valor mínimo da compra
        if ($carrinho['dados']['subtotal'] <= $this->String->moedaToBco(Configure::read('Loja.valor_minimo_compra'))) {
            $this->Session->setFlash('Seu carrinho não atingiu o valor Mínimo de R$.' . Configure::read('Loja.valor_minimo_compra'), 'flash/error');
            $this->redirect(array('plugin' => 'carrinho', 'controller' => 'carrinho', 'action' => 'index'));
        }
		
		if (!empty($this->data)) {
            $this->data['Pedido']['entrega_prazo'] = $carrinho['dados']['prazo_entrega'];
            $this->data['Pedido']['frete_id'] = $carrinho['dados']['tipo_entrega'];
            $this->data['Pedido']['valor_pedido'] = $carrinho['dados']['subtotal'];
            $this->data['Pedido']['parcelas'] = isset($this->data['Pedido']['parcelas']) && $this->data['Pedido']['parcelas'] >= 1 ? $this->data['Pedido']['parcelas'] : 1;
           
            if($this->data['Pedido']['tipo_pagamento_id']=='BOLETO'){
                $this->data['Pedido']['condicao_pagamento_id']=8;
            }else{
                $this->data['Pedido']['condicao_pagamento_id']=21;
            } 
            $this->Pedido->set($this->data);
           
            if ($this->Pedido->validates()) {

                //pega o nome do tipo de entrega
                $frete = $this->Pedido->Frete->find('first', array('conditions' => array('Frete.id' => $carrinho['dados']['tipo_entrega'])));
                //cupom de desconto
                $cupom = $this->recalcula_cupom();
                $cupom_total_desconto = 0;
                if (isset($cupom['Carrinho']['dados'])) {
                    $cupom_total_desconto = $this->String->moedaTobco($cupom['Carrinho']['dados']['cupom_total_desconto']);
                    switch ($cupom['Cupom']['tipo']) {
                        case 'CARRINHO':
                        case 'PRODUTO':
                            $carrinho['dados']['subtotal'] = $this->String->moedaTobco($cupom['Carrinho']['dados']['sub_total_com_desconto']);
                            break;
                        case 'FRETE':
                            $carrinho['dados']['frete'] = $this->String->moedaTobco($cupom['Carrinho']['dados']['frete_com_desconto']);
                            break;
                    }
                }

                $condicao = $this->PagamentoCondicao->find('first', array('conditions' => array('PagamentoCondicao.id' => (int) $this->data['Pedido']['condicao_pagamento_id'])));
                //desconto por meio de pagamento
                $preco_desconto = 0;
                if ($carrinho['dados']['subtotal'] > $condicao['PagamentoCondicao']['desconto_apartir'] && $condicao['PagamentoCondicao']['desconto_de'] > 0 && $this->data['Pedido']['parcelas'] == 1) {
                    $preco_desconto = $carrinho['dados']['subtotal'] * $condicao['PagamentoCondicao']['desconto_de'] / 100;
                    if ($preco_desconto > $carrinho['dados']['subtotal']) {
                        $preco_desconto = $carrinho['dados']['subtotal'];
                    }
                }
                //juros por meio de pagamento
                $juros_adicional = 0;
                if (($this->data['Pedido']['parcelas'] > $condicao['PagamentoCondicao']['juros_apartir']) && ($condicao['PagamentoCondicao']['juros_de'] > 0)) {
                    $dados = array_values($this->Carrinho->getItens());
                    //adiciona o juros em cada produto
                    foreach ($dados as $chave => $valor) {
                        if ($valor['preco_com_desconto'] > 0) {
                            $preco = $valor['preco_com_desconto'];
                        } else {
                            $preco = $valor['preco_promocao'] > 0 ? $valor['preco_promocao'] : $valor['preco'];
                        }
                        $preco_unitario = $valor['quantidade'] * $preco;
                        $juros_adicional += $preco_unitario * $condicao['PagamentoCondicao']['juros_de'] / 100;
                    }
                }

                $usuario = $this->Auth->User();

                if (isset($cupom['Cupom']['cupom'])) {
                    $Pedido['Pedido']['cupom'] = $cupom['Cupom']['cupom'];
                }
                $Pedido['Pedido']['usuario_nome'] = $usuario['Usuario']['nome'];
                $Pedido['Pedido']['usuario_email'] = $usuario['Usuario']['email'];
                $Pedido['Pedido']['usuario_telefone'] = $usuario['Usuario']['telefone'];
                $Pedido['Pedido']['usuario_celular'] = $usuario['Usuario']['celular'];
                $Pedido['Pedido']['usuario_cpf'] = $usuario['Usuario']['cpf'];
                $Pedido['Pedido']['usuario_cnpj'] = $usuario['Usuario']['cnpj'];
                $Pedido['Pedido']['usuario_nome_fantasia'] = $usuario['Usuario']['nome_fantasia'];
                $Pedido['Pedido']['usuario_inscricao_estadual'] = $usuario['Usuario']['inscricao_estadual'];
                $Pedido['Pedido']['usuario_data_nascimento'] = $usuario['Usuario']['data_nascimento'];
                $Pedido['Pedido']['usuario_tipo_pessoa'] = $usuario['Usuario']['tipo_pessoa'];
                $Pedido['Pedido']['usuario_sexo'] = $usuario['Usuario']['sexo'];
                $Pedido['Pedido']['usuario_razao_social'] = $usuario['Usuario']['razao_social'];
                $Pedido['Pedido']['usuario_rg'] = $usuario['Usuario']['rg'];
                $Pedido['Pedido']['valor_pedido'] = (($juros_adicional + $carrinho['dados']['subtotal']) - ($preco_desconto));
                $Pedido['Pedido']['valor_desconto_cupom'] = $cupom_total_desconto;
                $Pedido['Pedido']['valor_desconto_meio_pagamento'] = $preco_desconto;
                $Pedido['Pedido']['valor_juros_meio_pagamento'] = $juros_adicional;
                $Pedido['Pedido']['valor_frete'] = $carrinho['dados']['frete'];
                $Pedido['Pedido']['entrega_tipo'] = $frete['Frete']['nome'];
                $Pedido['Pedido']['entrega_prazo'] = $carrinho['dados']['prazo_entrega'];
                $Pedido['Pedido']['parcelas'] = isset($this->data['Pedido']['parcelas']) && $this->data['Pedido']['parcelas'] >= 1 ? $this->data['Pedido']['parcelas'] : 1;
                //busca o endereço que o usuário escolheu para entrega
                $endereco = $this->UsuarioEndereco->find('first', array('conditions' => array('UsuarioEndereco.id' => $this->Carrinho->getEnderecoEntrega())));
                $Pedido['Pedido']['endereco_cep'] = $endereco['UsuarioEndereco']['cep'];
		$Pedido['Pedido']['endereco_id'] = $endereco['UsuarioEndereco']['id'];
                $Pedido['Pedido']['endereco_rua'] = $endereco['UsuarioEndereco']['rua'];
                $Pedido['Pedido']['endereco_numero'] = $endereco['UsuarioEndereco']['numero'];
                $Pedido['Pedido']['endereco_complemento'] = $endereco['UsuarioEndereco']['complemento'];
                $Pedido['Pedido']['endereco_bairro'] = $endereco['UsuarioEndereco']['bairro'];
                $Pedido['Pedido']['endereco_cidade'] = $endereco['UsuarioEndereco']['cidade'];
                $Pedido['Pedido']['endereco_estado'] = $endereco['UsuarioEndereco']['uf'];
                $Pedido['Pedido']['pagamento_condicao_id'] = $this->data['Pedido']['condicao_pagamento_id'];
                $Pedido['Pedido']['pedido_status_id'] = 1;
                $Pedido['Pedido']['pedido_processado'] = 0;
                $Pedido['Pedido']['observacoes'] = $carrinho['dados']['observacoes'];
                $Pedido['Pedido']['usuario_ip'] = $_SERVER['REMOTE_ADDR'];
                //pega o id do usuário na sessão

                $Pedido['Pedido']['usuario_id'] = $usuario['Usuario']['id'];
                $Pedido['Pedido']['finalizado'] = 0;
                $Pedido['Pedido']['carrinho_abandonado_id'] = $this->Session->read("Carrinho.carrinho_abandonado");
                //salvando pedido
                if ($this->Pedido->save($Pedido)) {
                    //atualizado o registro de carrinho abandonado
                    App::import("Model", "CarrinhoAbandonado");
                    $this->CarrinhoAbandonado = new CarrinhoAbandonado();
                    $carrinho_abandonado['finalizado'] = true;
                    $this->CarrinhoAbandonado->salvar($carrinho_abandonado, $this->Session->read("Carrinho.carrinho_abandonado"));
                }

                //seta o historico de status
                $status = $this->Pedido->PedidoStatus->find('first', array('conditions' => array('PedidoStatus.id' => 1)));

                //log
                $save['PedidoHistoricoStatus']['id'] = null;
                $save['PedidoHistoricoStatus']['pedido_status_id'] = $status['PedidoStatus']['id'];
                $save['PedidoHistoricoStatus']['pedido_id'] = $this->Pedido->id;
                $save['PedidoHistoricoStatus']['status'] = $status['PedidoStatus']['nome'];
                $this->Pedido->PedidoHistoricoStatus->save($save);


                //seta o id do pedido finalizado na sessao para exibir na finalizacao
                $this->Carrinho->setPedido($this->Pedido->id);

                //salvando itens do pedido
                $dados = array_values($this->Carrinho->getItens());
				
				//begin tratamento itens de pedido
				//trata os itens do pedido
				//se for do tipo kit, adiciona todos os produtos do kit no itens de pedido para melhor controle de estoque
				foreach($dados as $k => $dado){
					if(isset($dado['kit']) && $dado['kit'] == 1){
						$produtos_kits = $this->ProdutoKit->find('all', array('recursive' => -1, 'conditions' => array('ProdutoKit.kit_id' => $dado['produto_id'])));
						
						if(is_array($produtos_kits) && count($produtos_kits) > 0){
						
							foreach($produtos_kits as $produto_kit){
								//begin iten
								$produto = $this->Produto->find("first", array(
																	"fields" => "Produto.id as produto_id, Produto.sku, Produto.referencia, Produto.codigo, Produto.codigo_externo, Produto.codigo_erp, ProdutoDescricao.nome, 
																				ProdutoDescricao.descricao,Produto.custo, Produto.preco, Produto.preco_promocao, Produto.quantidade, Produto.peso, Produto.peso_ramos, 
																				Produto.garantia, Produto.largura, Produto.altura, Produto.profundidade, Produto.fabricante_id, Produto.observacao, Produto.unidade_venda, 
																				Produto.quantidade_disponivel, Produto.quantidade_alocada, Produto.preco_promocao_inicio, Produto.preco_promocao_fim, 
																				Produto.preco_promocao_novo, Produto.preco_promocao_velho, Produto.tempo_producao, Produto.marca, Produto.kit,
																				(SELECT concat('uploads/produto_imagem/filename/',ProdutoImagem.filename) FROM produto_imagens ProdutoImagem WHERE ProdutoImagem.produto_id = ". $produto_kit['ProdutoKit']['produto_id'] ." LIMIT 1) as imagem
																				",
																	"joins" => array(
																					array(
																						'table' => 'produto_descricoes',
																						'alias' => 'ProdutoDescricao',
																						'type' => 'left',
																						'conditions' => array('ProdutoDescricao.produto_id = Produto.id')
																					)
																	),
																	"contain"=>array("Categoria","VariacaoProduto"=>array("Variacao"),"AtributoProduto"=>array("Atributo"=>array("AtributoTipo")),"ProdutoPreco"),
																	"conditions" => array("Produto.id" => $produto_kit['ProdutoKit']['produto_id'], "ProdutoDescricao.language" => Configure::read('Config.language'))
																	)
																);
								//adiciona com quantidade
								$produto['Produto']['quantidade'] = $produto_kit['ProdutoKit']['quantidade'] * $dado['quantidade'];
								$produto['Produto']['preco'] = $produto_kit['ProdutoKit']['preco'];
								
								//nome e descricao na lingua default
								$produto['Produto']['nome'] = $produto['ProdutoDescricao']['nome'];
								$produto['Produto']['descricao'] = $produto['ProdutoDescricao']['descricao'];
								
								$item = $produto['Produto'];
								$item['imagem'] = $produto[0]['imagem'];
								
								if( isset($produto['ProdutoPreco'][0]) ){
									$item['ProdutoPreco'] =  $produto['ProdutoPreco'][0];
								}
								
								foreach( $produto['AtributoProduto'] as $atributos ){
									if($atributos['Atributo']['AtributoTipo']['nome'] !=""){
										$item['ProdutoAtributos'][$atributos['Atributo']['AtributoTipo']['nome']] =  $atributos['Atributo']['valor'];
									}
								}
								
								foreach( $produto['VariacaoProduto'] as $variacoes ){
									if(isset($variacoes['valor'])){
										$item['VariacaoProduto'][$variacoes['valor']] = $variacoes['Variacao']['nome'];
									}
								}
								foreach( $produto['Categoria'] as $categoria ){
									$item['Categoria'][$categoria['id']] = $categoria['seo_url'];					
								}
								//end item
								
								array_push($dados, $item);
							}
						
						}
						
						unset($dados[$k]);
						
					}
				}
				//end tratamento itens de pedido
			
                foreach ($dados as $chave => $valor) {
				
                    $desconto_por_produto = 0;
                    $juros_por_produto = 0;
                    //adiciona o valor de desconto no item
                    if ($condicao['PagamentoCondicao']['desconto_de'] > 0 && $carrinho['dados']['subtotal'] > $condicao['PagamentoCondicao']['desconto_apartir'] && $Pedido['Pedido']['parcelas'] == 1) {
                        //se tiver cupom pega o valor já descontado para adicionar o desconto por meio de pagamento

                        if (isset($valor['preco_com_desconto']) && $valor['preco_com_desconto'] > 0) {
                            $preco = $valor['preco_com_desconto'];
                        } else {
                            $preco = $valor['preco_promocao'] > 0 ? $valor['preco_promocao'] : $valor['preco'];
                        }
                        $preco_unitario = $preco * $valor['quantidade'];
                        $desconto_por_produto = $preco_unitario * $condicao['PagamentoCondicao']['desconto_de'] / 100;

                        if ($preco_desconto > $carrinho['dados']['subtotal']) {
                            $desconto_por_produto = $carrinho['dados']['subtotal'];
                        }
                        $valor['preco_promocao'] = ($preco - ($desconto_por_produto / $valor['quantidade']));
                    }

                    //adiciona o valor do juros no item
                    if (($Pedido['Pedido']['parcelas'] > $condicao['PagamentoCondicao']['juros_apartir']) && ($condicao['PagamentoCondicao']['juros_de'] > 0)) {
                        //se tiver cupom pega o valor já descontado para adicionar o juros por meio de pagamento
                        if ($valor['preco_com_desconto'] > 0) {
                            $preco = $valor['preco_com_desconto'];
                        } else {
                            $preco = $valor['preco_promocao'] > 0 ? $valor['preco_promocao'] : $valor['preco'];
                        }

                        $preco_unitario = $preco * $valor['quantidade'];
                        $juros_por_produto = $preco_unitario * $condicao['PagamentoCondicao']['juros_de'] / 100;
                        if ($preco_desconto > $carrinho['dados']['subtotal']) {
                            $juros_por_produto = $carrinho['dados']['subtotal'];
                        }
                        $valor['preco_promocao'] = $preco + ($juros_por_produto / $valor['quantidade']);
                    }

                    //se não tiver desconto por meio de pagamento ou juros e tiver cupom altera o valor do produto para o valor com desconto.
                    if ($desconto_por_produto == 0 && $juros_por_produto == 0 && isset($valor['preco_com_desconto']) && $valor['preco_com_desconto'] > 0) {
                        $valor['preco_promocao'] = $valor['preco_com_desconto'] / $valor['quantidade'];
                    }
                    //seta que o produto saiu de graça, para não enviar o produto para o pagseguro;
                    if (isset($valor['preco_com_desconto']) && $valor['preco_com_desconto'] == 0) {
                        $PedidoItem['PedidoItem']['gratis'] = true;
                    }

                    $PedidoItem['PedidoItem'] = $valor;
                    //seta os atributos se possuir
                    $PedidoItem['PedidoItem']['atributos'] = json_encode($valor['ProdutoAtributos']);
                    if (isset($valor['VariacaoProduto'])) {
                        //seta as variacoes se possuir
                        $PedidoItem['PedidoItem']['variacoes'] = json_encode($valor['VariacaoProduto']);
                    }
                    if (isset($valor['Categoria'])) {
                        //seta as variacoes se possuir
                        $PedidoItem['PedidoItem']['categorias'] = json_encode($valor['Categoria']);
                    }

                    //seta a tabela de preço utilizada
                    $PedidoItem['PedidoItem']['produto_preco'] = json_encode($valor['ProdutoPreco']);
                    //seta o valor
                    $PedidoItem['PedidoItem']['pedido_id'] = $this->Pedido->id;
                    $PedidoItem['PedidoItem']['id'] = '';
                    $this->PedidoItem->id = null;
                    $this->PedidoItem->save($PedidoItem);
						$quantidades = $this->PedidoItem->query(
								"SELECT 
							(`Produto`.`quantidade` - ifnull((SELECT SUM(`pedido_itens`.`quantidade`) FROM (`pedido_itens` join `pedidos`) WHERE ((`Produto`.`id` = `pedido_itens`.`produto_id`) AND (`pedido_itens`.`pedido_id` = `pedidos`.`id`) AND (`pedidos`.`finalizado` = 0))),0)) AS disponivel,
							(ifnull((SELECT SUM(`pedido_itens`.`quantidade`)  FROM (`pedido_itens` join `pedidos`) WHERE ((`Produto`.`id` = `pedido_itens`.`produto_id`) AND (`pedido_itens`.`pedido_id` = `pedidos`.`id`) AND (`pedidos`.`finalizado` = 0))),0)) AS alocado,
							(ifnull((SELECT SUM(`pedido_itens`.`quantidade`) FROM (`pedido_itens` join `pedidos`) WHERE `Produto`.`id` = `pedido_itens`.`produto_id` AND `pedido_itens`.`pedido_id` = `pedidos`.`id` AND `pedidos`.`finalizado` = 1),0)) AS quantidade_vendido
							FROM produtos Produto WHERE Produto.id  = '" . (int) $valor['produto_id'] . "'");
						//aloca o estoque e notifica quantos disponiveis
						$this->Produto->saveAll(array('id' => $valor['produto_id'], 'quantidade_disponivel' => $quantidades[0][0]['disponivel'], 'quantidade_alocada' => $quantidades[0][0]['alocado'], 'quantidade_vendidos' => $quantidades[0][0]['quantidade_vendido']));
						
						//desativa kit com produto sem estoque
						if($quantidades[0][0]['disponivel'] <= 0){
							$this->ProdutoKit->desativa_kit_sem_estoque($valor['produto_id']);
						}
						
                    //verifica o estoque do item, e muda o parent do mesmo, caso o estoque seja 0
                    $this->Produto->altera_produto_estoque((int) $valor['produto_id']);
                }

                //atualiza o xml de cliente
                App::import('Controller', 'Usuarios');
                $Usuarios = new UsuariosController;
                $Usuarios->exportar_usuario($usuario['Usuario']['id']);

                //atualiza o estoque do produto
                //realiza a queima do cupom de desconto
                if (isset($cupom['Cupom']['cupom'])) {
                    $this->loadModel('CupomQueimado');
                    $cupom_queimado['CupomQueimado']['cupom'] = $cupom['Cupom']['cupom'];
                    $cupom_queimado['CupomQueimado']['cupom_id'] = $cupom['Cupom']['id'];
                    $cupom_queimado['CupomQueimado']['pedido_id'] = $this->Pedido->id;
                    $this->CupomQueimado->save($cupom_queimado);
                }

                $pedido = $this->Pedido->find('first', array('conditions' => array('Pedido.id' => $this->Pedido->id)));

                if (in_array($pedido['PagamentoCondicao']['sigla'], array('BCASH'))) {
                    //envia o email da compra pro cliente.
                    $this->emailNotificacaoCompra();
					$this->emailNotificacaoCompraLojista();
                    //efetua o pagamento do pedido;
                    $this->processaBcash($pedido);
                }elseif(in_array($pedido['PagamentoCondicao']['sigla'], array('BOLETO'))){
                    $retorno = $this->processaUrlBoleto($pedido);
                    //salva o link do boleto
                    $this->Pedido->saveField('url_boleto', $retorno);
                    $this->emailNotificacaoCompra();
                    $this->emailNotificacaoCompraLojista();
                    $this->redirect(array('plugin' => 'carrinho', 'controller' => 'carrinho', 'action' => 'finalizacao'));
                }


            } else {
                 $this->Session->setFlash('Preencha corretamente os campos em destaque.', 'flash/error');
            }
        }
        $fretes_disponiveis = $this->Carrinho->consultarFretesDisponiveis($endereco_entrega['UsuarioEndereco']['cep']);
        $this->set('frete_tipos', $fretes_disponiveis);
        //se o usuario setou o cupom e alterou o endereço de entrega e veio direto para a tela de forma de pagamento
        if ($endereco_entrega['UsuarioEndereco']['cep'] != $this->Carrinho->getCep()) {
            //se tiver cupom guarda ele
            $cupom = $this->recalcula_cupom();
            $this->Carrinho->limparFrete();
            $frete = $this->Carrinho->consultarFretesDisponiveis($endereco_entrega['UsuarioEndereco']['cep']);
            $this->set_frete($endereco_entrega['UsuarioEndereco']['cep'], $frete[0]['Frete'][0]['id']);
            $this->set_cupom($cupom['Cupom']['cupom']);
        } else {
            $totais = $this->Carrinho->recalcularCarrinho();
            $this->set('dados', $this->Carrinho->getItens());
            $tem_cupom = $this->recalcula_cupom();
        }
        //se tiver cupom, pega os valores totais já com desconto para exibicao
        if (isset($tem_cupom['Carrinho']['dados'])) {
            $this->set('cupom', $tem_cupom['Cupom']);
            $this->set('totais', $tem_cupom['Carrinho']['dados']);
            $this->data['Cupom']['cupom'] = $tem_cupom['Cupom']['cupom'];
            $this->data['Cupom']['msg'] = $tem_cupom['Cupom']['msg'];
        } else {
            $this->set('totais', $totais);
        }

        //busca os fretes disponiveis e seta na seção.

        $this->data['Pedido']['frete_id'] = $this->Carrinho->getFrete();

        if (isset($this->data['Pedido']['condicao_pagamento_id']) && is_numeric($this->data['Pedido']['condicao_pagamento_id'])) {
            $this->set('parcelas', $this->ajax_forma_pagamento($this->data['Pedido']['condicao_pagamento_id']));
        } else {
            $this->set('parcelas', array('' => 'SELECIONE'));
        }
    }

    function finalizacao() {

        $this->loadModel("UsuarioEndereco");
        $this->loadModel("PagamentoCondicaoImagem");
        $this->loadModel('Pedido');
        $this->loadModel('Frete');

        
         App::import("model", "Pedido");
        $this->Pedido = new Pedido();
        App::import("model", "PagamentoCondicao");
        $this->PagamentoCondicao = new PagamentoCondicao();

        if ($this->RequestHandler->isPost()) {

            $this->log('incio','bcash');
            $this->log(var_export($_POST,true),'bcash');
            $token = '5BEC47721F85B3EDB545F78A1BF51364';

            $id_transacao = $_POST['id_transacao'];
            $data_transacao = $_POST['data_transacao'];
            $data_credito = $_POST['data_credito'];
            $valor_original = $_POST['valor_original'];
            $valor_loja = $_POST['valor_loja'];
            $valor_total = $_POST['valor_total'];
            $desconto = $_POST['desconto'];
            $acrescimo = $_POST['acrescimo'];
            $tipo_pagamento = $_POST['tipo_pagamento'];
            $parcelas = $_POST['parcelas'];
            $cliente_nome = $_POST['cliente_nome'];
            $cliente_email = $_POST['cliente_email'];
            $cliente_rg = $_POST['cliente_rg'];
            $cliente_data_emissao_rg = $_POST['cliente_data_emissao_rg'];
            $cliente_orgao_emissor_rg = $_POST['cliente_orgao_emissor_rg'];
            $cliente_estado_emissor_rg = $_POST['cliente_estado_emissor_rg'];
            $cliente_cpf = $_POST['cliente_cpf'];
            $cliente_sexo = $_POST['cliente_sexo'];
            $cliente_data_nascimento = $_POST['cliente_data_nascimento'];
            $cliente_endereco = $_POST['cliente_endereco'];
            $cliente_complemento = $_POST['cliente_complemento'];
            $status = $_POST['status'];
            $cod_status = $_POST['cod_status'];
            $cliente_bairro = $_POST['cliente_bairro'];
            $cliente_cidade = $_POST['cliente_cidade'];
            $cliente_estado = $_POST['cliente_estado'];
            $cliente_cep = $_POST['cliente_cep'];
            $frete = $_POST['frete'];
            $tipo_frete = $_POST['tipo_frete'];
            $informacoes_loja = $_POST['informacoes_loja'];
            $id_pedido = $_POST['id_pedido'];
            $free = $_POST['free'];

            /* Essa variável indica a quantidade de produtos retornados */
            $qtde_produtos = $_POST['qtde_produtos'];

            /* Verificando ID da transação */
            /* Verificando status da transação */
            /* Verificando valor original */
            /* Verificando valor da loja */

            $post = "transacao=$id_transacao" .
                    "&status=$status" .
                    "&cod_status=$cod_status" .
                    "&valor_original=$valor_original" .
                    "&valor_loja=$valor_loja" .
                    "&token=$token";
            $enderecoPost = "https://www.bcash.com.br/checkout/verify/";

            ob_start();
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $enderecoPost);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            curl_exec($ch);
            $resposta = ob_get_contents();
            ob_end_clean();

            if (trim($resposta) == "VERIFICADO") {
                /**
                 * Boleto Bancário
                 * Visa
                 * Mastercard
                 * American Express
                 * Diners
                 * Aura
                 * HiperCard
                 * Elo
                 * Transferência OnLine Banco do Brasil
                 * Transferência OnLine Bradesco
                 * Transferência OnLine Itaú
                 * Transferência OnLine Banrisul
                 * Transferência OnLine HSBC
                 */
                

                switch (utf8_encode($tipo_pagamento)) {
                    case 'Boleto Bancário':
                        $condicao_id = 8;
                        break;
                    case 'Visa':
                        $condicao_id = 9;
                        break;
                    case 'Mastercard':
                        $condicao_id = 10;
                        break;
                    case 'American Express':
                        $condicao_id = 11;
                        break;
                    case 'Diners':
                        $condicao_id = 12;
                        break;
                    case 'Aura':
                        $condicao_id = 13;
                        break;
                    case 'HiperCard':
                        $condicao_id = 14;
                        break;
                    case 'Elo':
                        $condicao_id = 15;
                        break;
                    case 'Transferência OnLine Banco do Brasil':
                        $condicao_id = 16;
                        break;
                    case 'Transferência OnLine Bradesco':
                        $condicao_id = 17;
                        break;
                    case 'Transferência OnLine Itaú':
                        $condicao_id = 18;
                        break;
                    case 'Transferência OnLine Banrisul':
                        $condicao_id = 19;
                        break;
                    case 'Transferência OnLine HSBC':
                        $condicao_id = 20;
                        break;
                }
                /**
                 *   Código do status da transação.
                 *
                 *   cod_status = 0 - Transação em Andamento = Bcash recebeu a transação, está analisando ou aguardando o pagamento.
                 *
                 *   cod_status = 1 - Transação Concluída = quando a transação já passou por todo o processo e foi finalizada, ou foi confirmado o pagamento.
                 *
                 *   cod_status = 2 - Transação Cancelada = por qualquer motivo a transação foi cancelada, pagamento foi negado, estornado, ocorreu um chargeback.
                 * 
                 */
                switch ($cod_status) {
                     case '0':
                        $status = 1;
                        break;
                    case '1':
                        $status = 2;
                        break;
                    case '2':
                        $status = 3;
                        break;
                    default:
                        $status = 1;
                        break;
                }
                $this->Pedido->id = (int)$id_pedido;
                $this->Pedido->saveField('pagamento_condicao_id', $condicao_id);
                $this->Pedido->saveField('pedido_status_id', $status);
                $this->Pedido->saveField('pedido_processado', true);
                $this->Pedido->saveField('parcelas', $parcelas);
                $this->log("PEDIDO - {$pedido_id} alterado para o status  {$status} e para a condicao {$condicao_id}",'bcash');
                $this->log('fim','bcash');
		
            }else{
                $this->log('passou da verificacao','bcash_log');
            }
        }
        
                $usuario = $this->Auth->User();
        
	if(isset($_GET['pid'])){
		$this->Carrinho->setPedido(base64_decode($_GET['pid']));
	}
	$dados = $this->Pedido->find('first', array('conditions' => array('Pedido.id' => $this->Carrinho->getPedido())));

        if (!$dados) {
            $this->redirect('/');
        }
        
        $frete = $this->Frete->find('first', array('conditions' => array('Frete.id' => $dados['Pedido']['frete_id'])));
        $dados['FreteTipo'] = $frete['FreteTipo'];
        $forma_pagamento_imagem = $this->PagamentoCondicaoImagem->find('first', array('conditions' => array('PagamentoCondicaoImagem.pagamento_condicao_id' => $dados['Pedido']['pagamento_condicao_id'])));
        $dados['PagamentoCondicaoImagem'] = $forma_pagamento_imagem['PagamentoCondicaoImagem'];

       
        $this->set('dados', $dados);
        $this->set('endereco', $this->UsuarioEndereco->find('first', array('conditions' => array('UsuarioEndereco.id' => $dados['Pedido']['endereco_id']))));
        //só esvazia o carrinho e envia o email apos o retorno do pedido no pagseguro
		$this->Carrinho->Esvaziar();
    }

    function entrega() {
        
        // se o campo observações existir e não estiver vazio salva o conteudo na session
        if (isset($this->params['form']['observacoes']) && !empty($this->params['form']['observacoes'])) {
            $this->Session->write("Carrinho.dados.observacoes", $this->params['form']['observacoes']);
        }

        $this->loadModel('UsuarioEndereco');
        if (!$this->Auth->User() || $this->Carrinho->getStep() < 1) {
            $this->redirect(array('plugin' => 'carrinho', 'controller' => 'carrinho', 'action' => 'index'));
        }

        $usuario = $this->Auth->User();

        if (!$usuario['Usuario']['id']) {
            $this->Session->setFlash('Paramentros inválidos', 'flash/error');
            $this->redirect(array('controller' => 'usuarios', 'action' => 'add'));
        }


        if (!empty($this->data)) {
            $this->UsuarioEndereco->create();
            $this->data['UsuarioEndereco']['usuario_id'] = $usuario['Usuario']['id'];
            if ($this->UsuarioEndereco->save($this->data['UsuarioEndereco'])) {
                //se o endereço passado for setado como cobrança altera todos os outros enderecos para não entrega
                if ($this->data['UsuarioEndereco']['cobranca'] == true && $this->data['UsuarioEndereco']['id'] != "") {
                    $this->UsuarioEndereco->updateAll(array('cobranca' => '0'), array('UsuarioEndereco.usuario_id' => $usuario['Usuario']['id'], 'UsuarioEndereco.id !=' => $this->data['UsuarioEndereco']['id']));
                } else if ($this->data['UsuarioEndereco']['cobranca'] == true) {
                    $this->UsuarioEndereco->updateAll(array('cobranca' => '0'), array('UsuarioEndereco.usuario_id' => $usuario['Usuario']['id'], 'UsuarioEndereco.id !=' => $this->UsuarioEndereco->id));
                }
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->data = array();
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }

        $this->set('enderecos', $this->UsuarioEndereco->find('all', array('conditions' => array('usuario_id' => $usuario['Usuario']['id']))));
        App::import("helper", "Estados");
        $this->Estados = new EstadosHelper();
        $this->set('Estados', $this->Estados);
        $carrinho = $this->Carrinho->getCarrinho();


        App::import("helper", "String");
        $this->String = new StringHelper();
        //valida o valor mínimo da compra
        if ($carrinho['dados']['subtotal'] <= $this->String->moedaToBco(Configure::read('Loja.valor_minimo_compra'))) {
            $this->Session->setFlash('Seu carrinho não atingiu o valor Mínimo de R$.' . Configure::read('Loja.valor_minimo_compra'), 'flash/error');
            $this->redirect(array('plugin' => 'carrinho', 'controller' => 'carrinho', 'action' => 'index'));
        }
    }

    function usar_este($id = null) {
        $this->layout = false;
        $this->render(false);
        //verifica se o usuario está logado
        if (!$this->Auth->User()) {
            $this->redirect(array('controller' => 'usuarios', 'action' => 'edit'));
        }
        $usuario = $this->Auth->User();
        App::import("model", "UsuarioEndereco");
        $this->UsuarioEndereco = new UsuarioEndereco();
        $endereco_valido = $this->UsuarioEndereco->find('first', array('conditions' => array('UsuarioEndereco.id' => $id, 'usuario_id' => $usuario['Usuario']['id'])));
        //verifica se o endereço passado é do usuário
        if (!$endereco_valido) {
            $this->redirect(array('plugin' => 'carrinho', 'controller' => 'carrinho', 'action' => 'entrega'));
        }
        //valida se está logado e já passou no passo 1
        if (!$this->Auth->User() || $this->Carrinho->getStep() < 1) {
            $this->redirect(array('controller' => 'usuarios', 'action' => 'edit'));
        }

        //grava o endereço setado
        $this->Carrinho->setEnderecoEntrega($id);
        //busca o endereço setado e salva o novo cep na sessão
        $endereco = $this->UsuarioEndereco->find('first', array('conditions' => array('UsuarioEndereco.id' => $this->Carrinho->getEnderecoEntrega())));
        //se o endereço escolhido for diferente do setado no carrinho, limpa o frete
        if ($endereco['UsuarioEndereco']['cep'] != $this->Carrinho->getCep()) {
            //se tiver cupom guarda ele
            $cupom = $this->recalcula_cupom();
            $this->Carrinho->limparFrete();
            $frete = $this->Carrinho->consultarFretesDisponiveis($endereco['UsuarioEndereco']['cep']);

            $this->set_frete($endereco['UsuarioEndereco']['cep'], $frete[0]['Frete'][0]['id']);
            $this->set_cupom($cupom['Cupom']['cupom']);
        }
        //recalcula os valores pois mudou o cep
        $this->Carrinho->recalcularCarrinho();

        $this->redirect(array('plugin' => 'carrinho', 'controller' => 'carrinho', 'action' => 'forma_pagamento'));
    }

    /**
     * Email da notificação da compra do cliente, enviado na tela de finalização;
     * @return Boolean
     */
    public function emailNotificacaoCompra() {

        App::import("helper", "Templates");
        $this->Templates = new TemplatesHelper();
        App::import("helper", "Calendario");
        $this->Calendario = new CalendarioHelper();

        App::import("model", "Pedido");
        $this->Pedido = new Pedido();
        $this->data = $this->Pedido->read(null, $this->Carrinho->getPedido());


        //seta o debug como zero para evitar aparecer bugs no email do cliente.
        Configure::write('debug', 0);
        if (Configure::read('Loja.smtp_host') != 'localhost') {
            $this->Email->smtpOptions = array(
                'port' => (int) Configure::read('Loja.smtp_port'),
                'host' => Configure::read('Loja.smtp_host'),
                'username' => Configure::read('Loja.smtp_user'),
                'password' => Configure::read('Loja.smtp_password')
            );
            $this->Email->delivery = 'smtp';
        }
        $this->Email->lineLength = 120;
        $this->Email->sendAs = 'html';
        $this->Email->from = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_compra') . ">";
        $this->Email->to = "{$this->data['Usuario']['nome']} <{$this->data['Usuario']['email']}>";
        $this->Email->subject = Configure::read('Loja.nome') . " - Pedido #" . $this->data['Pedido']['id'];

        if (Configure::read('Reweb.nome_bcc')) {
            $bccs = array();
            $assunto = Configure::read('Reweb.nome_bcc');
            $emails = Configure::read('Reweb.email_bcc');
            foreach (explode(';', $emails) as $bcc) {
                $bccs[] = "{$assunto} <{$bcc}>";
            }
            $this->Email->bcc = $bccs;
        }
        
        //URL BOLETO
        if($this->data['Pedido']['url_boleto'] != ""){
            $url_boleto = '<a href="'.$this->data['Pedido']['url_boleto'].'" target="_blank">IMPRIMIR BOLETO</a>';
        }else{
            $url_boleto = '&nbsp;';
        }
        
        $email = str_replace(array(
            '{USUARIO_NOME}',
            '{PEDIDO_ID}',
            '{PEDIDO_DATA}',
            '{PEDIDO_ENDERECO_ENTREGA}',
            '{PEDIDO_ENDERECO_COBRANCA}',
            '{PEDIDO_PAGAMENTO}',
            '{PEDIDO_FRETE}',
            '{PEDIDO_ITENS}',
            '{IMPRIMIR_BOLETO}',
            '{LOJA_NOME}',
            '{PEDIDO_SUBTOTAL}',
            '{VALOR_FRETE}',
            '{PEDIDO_TOTAL}',
                ), array(
            $this->data['Usuario']['nome'],
            $this->data['Pedido']['id'],
            $this->Calendario->DataFormatada("d/m/Y H:i:s", $this->data['Pedido']['created']),
            $this->Templates->PedidoEnderecoEntrega($this->data),
            $this->Templates->PedidoEnderecoCobranca($this->data),
            $this->Templates->PedidoPagamento($this->data),
            $this->Templates->PedidoFrete($this->data),
            $this->Templates->PedidoItens($this->data),
            $url_boleto,
            Configure::read('Loja.smtp_assinatura_email'),
            "R$ " . $this->String->bcoToMoeda($this->String->moedaToBco($this->data['Pedido']['valor_pedido'])),
            "R$ " . $this->data['Pedido']['valor_frete'],
            "R$ " . $this->String->bcoToMoeda($this->String->moedaToBco($this->data['Pedido']['valor_pedido']) + $this->String->moedaToBco($this->data['Pedido']['valor_frete']))
                ), Configure::read('LojaTemplate.pedido')
        );
        //setando os dados para o email
        if ($this->Email->send($email)) {
            return true;
        } else {
            return false;
        }
    }
	
	/**
     * Email da notificação da compra do cliente, enviado na tela de finalização;
     * @return Boolean
     */
    public function emailNotificacaoCompraLojista() {

        App::import("helper", "Templates");
        $this->Templates = new TemplatesHelper();
        App::import("helper", "Calendario");
        $this->Calendario = new CalendarioHelper();

        App::import("model", "Pedido");
        $this->Pedido = new Pedido();
        $this->data = $this->Pedido->read(null, $this->Carrinho->getPedido());


        //seta o debug como zero para evitar aparecer bugs no email do cliente.
        Configure::write('debug', 0);
        if (Configure::read('Loja.smtp_host') != 'localhost') {
            $this->Email->smtpOptions = array(
                'port' => (int) Configure::read('Loja.smtp_port'),
                'host' => Configure::read('Loja.smtp_host'),
                'username' => Configure::read('Loja.smtp_user'),
                'password' => Configure::read('Loja.smtp_password')
            );
            $this->Email->delivery = 'smtp';
        }
        $this->Email->lineLength = 120;
        $this->Email->sendAs = 'html';
        $this->Email->from = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_compra') . ">";
		$this->Email->to = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_compra') . ">";
        //$this->Email->to = "{Configure::read('Loja.smtp_remetente_email')} <{Configure::read('Loja.email_compra')}>";
        $this->Email->subject = Configure::read('Loja.nome') . " - Pedido #" . $this->data['Pedido']['id'];

        // if (Configure::read('Reweb.nome_bcc')) {
            // $bccs = array();
            // $assunto = Configure::read('Reweb.nome_bcc');
            // $emails = Configure::read('Reweb.email_bcc');
            // foreach (explode(';', $emails) as $bcc) {
                // $bccs[] = "{$assunto} <{$bcc}>";
            // }
            // $this->Email->bcc = $bccs;
        // }

        //URL BOLETO
        if($this->data['Pedido']['url_boleto'] != ""){
            $url_boleto = '<a href="'.$this->data['Pedido']['url_boleto'].'" target="_blank">IMPRIMIR BOLETO</a>';
        }else{
            $url_boleto = '&nbsp;';
        }
        
        $email = str_replace(array(
            '{USUARIO_NOME}',
            '{PEDIDO_ID}',
            '{PEDIDO_DATA}',
            '{PEDIDO_ENDERECO_ENTREGA}',
            '{PEDIDO_ENDERECO_COBRANCA}',
            '{PEDIDO_PAGAMENTO}',
            '{PEDIDO_FRETE}',
            '{PEDIDO_ITENS}',
            '{IMPRIMIR_BOLETO}',
            '{LOJA_NOME}',
            '{PEDIDO_SUBTOTAL}',
            '{VALOR_FRETE}',
            '{PEDIDO_TOTAL}',
                ), array(
            $this->data['Usuario']['nome'],
            $this->data['Pedido']['id'],
            $this->Calendario->DataFormatada("d/m/Y H:i:s", $this->data['Pedido']['created']),
            $this->Templates->PedidoEnderecoEntrega($this->data),
            $this->Templates->PedidoEnderecoCobranca($this->data),
            $this->Templates->PedidoPagamento($this->data),
            $this->Templates->PedidoFrete($this->data),
            $this->Templates->PedidoItensLojista($this->data),
            $url_boleto,
            Configure::read('Loja.smtp_assinatura_email'),
            "R$ " . $this->String->bcoToMoeda($this->String->moedaToBco($this->data['Pedido']['valor_pedido'])),
            "R$ " . $this->data['Pedido']['valor_frete'],
            "R$ " . $this->String->bcoToMoeda($this->String->moedaToBco($this->data['Pedido']['valor_pedido']) + $this->String->moedaToBco($this->data['Pedido']['valor_frete']))
                ), Configure::read('LojaTemplate.pedido_lojista')
        );
        //setando os dados para o email
        if ($this->Email->send($email)) {
            return true;
        } else {
            return false;
        }
    }

    public function meus_pedidos() {
        App::import("model", "Pedido");
        $this->Pedido = new Pedido();
        $usuario = $this->Auth->User();

        $this->paginate = array(
            'conditions' => array('Pedido.usuario_id' => $usuario['Usuario']['id']),
            'order' => array('Pedido.id' => 'DESC'),
            'limit' => 100
        );
        $this->set('data', $this->paginate('Pedido'));

        //start breadcrumb
        $breadcrumb[] = array('url' => 'javascript:void(0);', 'nome' => "Área do Cliente");
        $this->set('breadcrumb', $breadcrumb);
        //end breadcrumb
    }

    /**
     * Metodo que realiza o envio dos dados para o pagseguro
     */
    function processaPagSeguro($data) {

        $this->render(false);
        $this->data = $data;
        App::import("helper", "Html");
        $this->Html = new HtmlHelper();
        //Form
        $form = '<html>
                    <head>
                        <title>Pagamento</title>
                        <script language="JavaScript">
                        function Pagar(){
                            document.getElementById(\'formPagamento\').submit();
                        }
                        </script>
                    </head>
                    <body onload="Pagar()">
						<center><img src="https://www.fueltech.com.br/img/site/gateway.png" alt="aguarde" title="aguarde" /></center>
                        <form style="display:none" action="https://pagseguro.uol.com.br/checkout/checkout.jhtml" name="formPagamento" method="post" id="formPagamento">';
        $form .= '<input type="hidden" value="' . $this->data['init']['pagseguro']['email'] . '" name="email_cobranca" />';
        $form .= '<input type="hidden" value="' . $this->data['init']['pagseguro']['type'] . '" name="tipo" />';
        $form .= '<input type="hidden" value="' . $this->data['init']['pagseguro']['currency'] . '" name="moeda" />';
        $form .='<input type="hidden" value="' . $this->data['init']['pagseguro']['reference'] . '" name="ref_transacao" />';
        // $form .= '<input type="hidden" value="' . $this->data['init']['pagseguro']['freight_type'] . '" name="tipo_frete" />';
        $form .= '<input type="hidden" value="' . $this->data['init']['definitions']['encode'] . '" name="encoding" />';
        $form .='<input type="hidden" value="' . $this->data['init']['pagseguro']['extra'] . '" name="extras" />';
        foreach ($this->data['init']['customer'] as $field => $value) {
            if (!is_null($value))
                $form .= '<input type="hidden" value="' . $value . '" name="' . $field . '" />';
        }
        foreach ($this->data['data'] as $item) {
            foreach ($item as $key => $att) {
                foreach ($att as $key2 => $val) {
                    $form .= '<input type="hidden" value="' . $val . '" name="' . $key2 . '" />';
                }
            }
        }

        $form .= '
                        </form>
                    </body>
            </html>';
        die($form);
    }

    public function meus_pedidos_nao_finalizados($id) {
        $this->render(false);
        App::import("model", "Pedido");
        $this->Pedido = new Pedido();
        $usuario = $this->Auth->User();

        $this->data = $this->Pedido->find('first', array('conditions' => array('Pedido.id' => $id)));
        $venda = array(
            'pagseguro' => array(// Array com informações pertinentes ao pagseguro
                'type' => 'CP', // Obrigatório passagem para pagseguro:tipo
                'reference' => $this->data['Pedido']['id'], // Obrigatório passagem para pagseguro:ref_transacao
                'theme' => 1, // Opcional Este parametro aceita valores de 1 a 5, seu efeito é a troca dos botões padrões do pagseguro
                //'freight_type' => $this->data['Pedido']['entrega_tipo'] == 'PAC' ? 'EN' : 'SD', // Opcional Este parametro aceita valores de 1 a 5, seu efeito é a troca dos botões padrões do pagseguro
                'currency' => 'BRL', // Obrigatório passagem para pagseguro:moeda,
            //'extra' => 0 // Um valor extra que você queira adicionar no valor total da venda, obs este valor pode ser negativo
            ),
            'definitions' => array(// Array com informações para manusei das informações
                'currency_type' => 'dolar', // Especifica qual o tipo de separador de decimais, suportados (dolar, real)
                'weight_type' => 'kg', // Especifica qual a medida utilizada para peso, suportados (kg, g)
                'encode' => 'utf-8' // Especifica o encode não implementado
            ),
            'customer' => array(// Array com informações do cliente, opcional para o pagseguro
                'cliente_nome' => $usuario['Usuario']['nome'],
                'cliente_cep' => $this->data['Pedido']['endereco_cep'],
                'cliente_end' => $this->data['Pedido']['endereco_rua'],
                'cliente_num' => $this->data['Pedido']['endereco_numero'],
                'cliente_compl' => $this->data['Pedido']['endereco_complemento'],
                'cliente_bairro' => $this->data['Pedido']['endereco_bairro'],
                'cliente_cidade' => $this->data['Pedido']['endereco_cidade'],
                'cliente_uf' => $this->data['Pedido']['endereco_estado'],
                'cliente_pais' => 'BR',
                'cliente_ddd' => substr($usuario['Usuario']['telefone'], 1, 2),
                'cliente_tel' => substr($usuario['Usuario']['telefone'], 2),
                'cliente_email' => $usuario['Usuario']['email']
            ),
            // Array especificando o cruzamento das informações de entrada para as de saída
            // O primeiro parametro é fixo sempre deve ser estes o segundo é setavel de acordo com as informações de entrada
            // exemplo:
            // format => (item_id => id, item_descr => description)
            'data' => array(
            )
        );

        $this->PagSeguro->init($venda);
        $produtos = array();
        App::import("helper", "String");
        $this->String = new StringHelper();

        $compra_gratuita = true;
        $first = true;
        foreach ($this->data['PedidoItem'] as $valor):
            $preco = $valor['preco_promocao'] > 0 ? $valor['preco_promocao'] : $valor['preco'];
            if ($valor['gratis'] == false) {
                $produtos[] = array(
                    'item_id' => $valor['produto_id'],
                    'item_descr' => $this->String->cropSentence($valor['nome']),
                    'item_quant' => $valor['quantidade'],
                    'item_valor' => $preco,
                    'item_peso' => $valor['peso'],
                    'item_frete' => ($this->String->moedaToBco($this->data['Pedido']['valor_frete']) && $first == true ) > 0 ? $this->String->moedaToBco($this->data['Pedido']['valor_frete']) : 0,
                );
                $compra_gratuita = false;
                $first = false;
            }
        endForeach;


        /**
         * SE A COMPRA FOR GRATUITA NÃO ENVIA O CLIENTE PARA O PAGSEGURO
         */
        if ($compra_gratuita) {
            $this->redirect(array('plugin' => 'carrinho', 'controller' => 'carrinho', 'action' => 'finalizacao'));
        } else {
            $this->PagSeguro->create($produtos);
        }
        return $this->PagSeguro->render();
    }

    public function ajax_forma_pagamento($condicao) {
        App::import("helper", "String");
        $this->String = new StringHelper();

        $this->loadModel("PagamentoCondicao");
        $condicao = $this->PagamentoCondicao->find('first', array('conditions' => array('PagamentoCondicao.id' => (int) $condicao)));
        $carrinho = $this->Carrinho->getCarrinho();
        //cupom de desconto
        $cupom = $this->recalcula_cupom();
        if (isset($cupom['Carrinho']['dados'])) {
            switch ($cupom['Cupom']['tipo']) {
                case 'CARRINHO':
                case 'PRODUTO':
                    $carrinho['dados']['subtotal'] = $this->String->moedaTobco($cupom['Carrinho']['dados']['sub_total_com_desconto']);

                    break;
                case 'FRETE':
                    $carrinho['dados']['frete'] = $this->String->moedaTobco($cupom['Carrinho']['dados']['frete_com_desconto']);
                    break;
            }
        }
        $valor = $carrinho['dados']['subtotal'];
        if ($this->RequestHandler->isAjax()) {
            $parcela = '<option value="">SELECIONE</option>';
            $juros = false;
            for ($i = 1; $i <= $condicao['PagamentoCondicao']['parcelas']; $i++) {
                $preco = $valor / $i;
                $frete = $carrinho['dados']['frete'] / $i;
                if ($i > 1 && $preco < (float) Configure::read('Loja.valor_minimo_parcelamento'))
                    break;

                //se existir juros
                if (($i > $condicao['PagamentoCondicao']['juros_apartir']) && ($condicao['PagamentoCondicao']['juros_de'] > 0)) {
                    $juros = true;
                    $juros_adicional = ($preco * $condicao['PagamentoCondicao']['juros_de']) / 100;
                }
                //se existir desconto
                $preco_desconto = null;
                if ($condicao['PagamentoCondicao']['desconto_de'] > 0 && $i == 1) {
                    $preco_desconto = $preco * $condicao['PagamentoCondicao']['desconto_de'] / 100;

                    if ($preco_desconto) {
                        if (($preco - $preco_desconto) < 0) {
                            $valor_com_desconto = 0;
                        } else {
                            $valor_com_desconto = $preco - $preco_desconto;
                        }
                        $valor_com_desconto = $valor_com_desconto + $frete;
                        $parcela .= '<option value="' . $i . '">' . "$i x " . $this->String->bcoToMoeda($valor_com_desconto) . ' - ' . (round($condicao['PagamentoCondicao']['desconto_de'])) . '% de desconto À VISTA</option>';
                    }
                } elseif ($juros) {
                    $parcela .= '<option value="' . $i . '">' . "$i x " . $this->String->bcoToMoeda($preco + $frete + $juros_adicional) . ' c/ Juros</option>';
                } else {
                    $parcela .= '<option value="' . $i . '">' . "$i x " . $this->String->bcoToMoeda($preco + $frete) . '</option>';
                }
            }

            die(json_encode($parcela));
        } else {
            $parcela = array();
            $juros = false;
            for ($i = 1; $i <= $condicao['PagamentoCondicao']['parcelas']; $i++) {
                $preco = $valor / $i;
                $frete = $carrinho['dados']['frete'] / $i;
                if ($i > 1 && $preco < (float) Configure::read('Loja.valor_minimo_parcelamento'))
                    break;

                //se existir desconto
                $preco_desconto = null;
                if ($condicao['PagamentoCondicao']['desconto_de'] > 0 && $i == 1) {
                    $preco_desconto = $preco * $condicao['PagamentoCondicao']['desconto_de'] / 100;
                }
                //se existir juros
                if (($i > $condicao['PagamentoCondicao']['juros_apartir']) && ($condicao['PagamentoCondicao']['juros_de'] > 0)) {
                    $juros = true;
                    $juros_adicional = ($preco * $condicao['PagamentoCondicao']['juros_de']) / 100;
                }

                //se existir desconto
                $preco_desconto = null;
                if ($condicao['PagamentoCondicao']['desconto_de'] > 0 && $i == 1) {
                    $preco_desconto = $preco * $condicao['PagamentoCondicao']['desconto_de'] / 100;

                    if ($preco_desconto) {
                        if (($preco - $preco_desconto) < 0) {
                            $valor_com_desconto = 0;
                        } else {
                            $valor_com_desconto = $preco - $preco_desconto;
                        }
                        $valor_com_desconto = $valor_com_desconto + $frete;
                        $parcela[$i] = "$i x " . $this->String->bcoToMoeda($valor_com_desconto) . ' - ' . (round($condicao['PagamentoCondicao']['desconto_de'])) . '% de desconto À VISTA';
                    }
                } elseif ($juros) {
                    $parcela[$i] = "$i x " . $this->String->bcoToMoeda($preco + $frete + $juros_adicional) . ' c/ Juros';
                } else {
                    $parcela[$i] = "$i x " . $this->String->bcoToMoeda($preco + $frete);
                }
            }
            return $parcela;
        }
    }
    
    /**
     * Metodo que gera  aurl do boleto
     */
    public function processaUrlBoleto($pedido) {
        
        App::import("helper", "String");
        $this->String = new StringHelper();
        App::import("helper", "Html");
        $this->Html = new HtmlHelper();
        $valor = $this->String->bcoToMoeda($this->String->moedaToBco($pedido['Pedido']['valor_pedido']) + $this->String->moedaToBco($pedido['Pedido']['valor_frete']));
        $valor = preg_replace('/[^0-9]/', '', $valor);
        $args['idConv'] = '008019';
        $args['versao'] = '002';
        $args['moeda'] = '986';
        $args['convClasse'] = '001';
        $args['tpPagamento'] = '21';
        $args['refTran'] = '2550751' . str_pad($pedido['Pedido']['id'], 10, "0", STR_PAD_LEFT);
        $args['valor'] = $valor;
        $args['dtVenc'] = date('dmY', mktime(0, 0, 0, date('m'), date('d') + 5, date('Y')));
        $args['urlRetorno'] = $this->Html->Url('/carrinho/finalizacao', true);
        $args['nome'] = $this->removeAcento($pedido['Usuario']['nome']);
        $args['msgLoja'] = '';
        $args['endereco'] = $pedido['Pedido']['endereco_rua'] . ', ' . $pedido['Pedido']['endereco_numero'] . ' ' . $pedido['Pedido']['endereco_complemento'];
        $args['bairro'] = $pedido['Pedido']['endereco_bairro'];
        $args['cep'] = $pedido['Pedido']['endereco_cep'];
        $args['cidade'] = $pedido['Pedido']['endereco_cidade'];
        $args['uf'] = $pedido['Pedido']['endereco_estado'];

        $url = 'https://www16.bancodobrasil.com.br/site/mpag/?';

        foreach ($args as $c => $v) {
            $url .= '&' . $c . '=' . $v;
        }
        
        return $url;
    }
    
    function removeAcento($var){ 
        $trocarIsso = array('à','á','â','ã','ä','å','ç','è','é','ê','ë','ì','í','î','ï','ñ','ò','ó','ô','õ','ö','ù','ü','ú','ÿ','À','Á','Â','Ã','Ä','Å','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï','Ñ','Ò','Ó','Ô','Õ','Ö','O','Ù','Ü','Ú','Ÿ');
        $porIsso = array('a','a','a','a','a','a','c','e','e','e','e','i','i','i','i','n','o','o','o','o','o','u','u','u','y','A','A','A','A','A','A','C','E','E','E','E','I','I','I','I','N','O','O','O','O','O','0','U','U','U','Y');
        $titletext = str_replace($trocarIsso, $porIsso, $var); return $titletext;         
    }

    
    function processaBcash($data) {

       App::import("helper", "Html");
        $this->Html = new HtmlHelper();
        $preco = $data['Pedido']['valor_pedido'];
       
        $form = '<html>
                    <head>
                            <title></title>
                            <script language="JavaScript">
                                    function Pagar(){
                                            document.getElementById(\'formPagamento\').submit();
                                    }
                            </script>
                    </head>
                    <body onload="Pagar()" style="display:block">
                    <img src="https://www.fueltech.com.br/img/site/gateway.png" />';

        $form .= '<form  action="https://www.bcash.com.br/checkout/pay/" name="formPagamento" method="post" id="formPagamento">';

        $form .= '<input name="email_loja" type="hidden" value="financeiro@fueltech.com.br">';

        for ($i = 0; $i < count($data['PedidoItem']); $i++) {
            $cont = $i + 1;
            $form .= '<input name="produto_codigo_' . $cont . '" type="hidden" value="' . trim($data['PedidoItem'][$i]['id']) . '">';
            $form .= '<input name="produto_qtde_' . $cont . '" type="hidden" value="' . trim($data['PedidoItem'][$i]['quantidade']) . '">';
            $form .= '<input name="produto_descricao_' . $cont . '" type="hidden" value="' . trim(strip_tags($data['PedidoItem'][$i]['nome'])) . '">';
            if ($data['PedidoItem'][$i]['preco_promocao'] > 0) {
                $valorProd = $data['PedidoItem'][$i]['preco_promocao'];
            } else {
                $valorProd = $data['PedidoItem'][$i]['preco'];
            }
            $form .= '<input name="produto_valor_' . $cont . '" type="hidden" value="' . trim($valorProd) . '">';
            $form .= '<input name="produto_extra_' . $cont . '" type="hidden" value="' . trim(strip_tags($data['PedidoItem'][$i]['descricao'])) . '">';
        }

        $form .= '<input name="tipo_integracao" type="hidden" value="PAD">';
        $valorFrete = str_replace(',', '.', $data['Pedido']['valor_frete']);
        $form .= '<input name="frete" type="hidden" value="' . trim($valorFrete) . '">';

        $form .= '<input name="id_pedido" type="hidden" value="' . trim($data['Pedido']['id']) . '">';
        $form .= '<input name="email" type="hidden" value="' . trim($data['Usuario']['email']) . '"> ';
        $form .= '<input name="nome" type="hidden" value="' . trim($data['Usuario']['nome']) . '">';
        $form .= '<input name="rg" type="hidden" value="' . trim($data['Usuario']['rg']) . '">';
        $form .= '<input name="data_emissao_rg" type="hidden" value="">';
        $form .= '<input name="orgao_emissor_rg" type="hidden" value="">';
        $form .= '<input name="estado_emissor_rg" type="hidden" value="">';
        $form .= '<input name="cpf" type="hidden" value="' . trim($data['Usuario']['cpf']) . '">';
        $form .= '<input name="sexo" type="hidden" value="' . trim($data['Usuario']['sexo']) . '">';
        $dataNasc = str_replace('-', '/', $data['Usuario']['data_nascimento']);

        $form .= '<input name="data_nascimento" type="hidden" value="' . $dataNasc . '">';
        $form .= '<input name="telefone" type="hidden" value="' . $data['Usuario']['telefone'] . '">';
        $form .= '<input name="endereco" type="hidden" value="' . $data['Pedido']['endereco_rua'] . "," . $data['Pedido']['endereco_numero'] . '">';
        $form .= '<input name="complemento" type="hidden" value="' . $data['Pedido']['endereco_complemento'] . '">';
        $form .= '<input name="bairro" type="hidden" value="' . $data['Pedido']['endereco_bairro'] . '">';
        $form .= '<input name="cidade" type="hidden" value="' . $data['Pedido']['endereco_cidade'] . '">';
        $form .= '<input name="estado" type="hidden" value="' . $data['Pedido']['endereco_estado'] . '">';
        $form .= '<input name="cep" type="hidden" value="' . $data['Pedido']['endereco_cep'] . '">';
        $form .= '<input name="free" type="hidden" value="">';
        $form .= '<input name="tipo_frete" type="hidden" value="' . ($data['Pedido']['entrega_tipo']) . '">';

        $form .= '<input name="desconto" type="hidden" value="0.00">';
        $form .= '<input name="acrescimo" type="hidden" value="0.00">';
        $form .= '<input name="url_retorno" type="hidden" value="https://www.fueltech.com.br/carrinho/finalizacao/?pid='.base64_encode($data['Pedido']['id']).'">';
        $form .= '<input name="url_aviso" type="hidden" value="">';
        $form .= '<input name="cliente_razao_social" type="hidden" value="">';
        $form .= '<input name="cliente_cnpj" type="hidden" value="">';

        $form .= '<input name="parcela_maxima" type="hidden" value="2">';
        $form .= '<input name="meio_pagamento" type="hidden" value="1">';
        $form .= '<input name="redirect" type="hidden" value="true">';

        $form .= '</form></body></html>';

        die($form);
    }
    
    
}

?>
