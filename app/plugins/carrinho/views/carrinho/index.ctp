<?php
$javascript->link('common/jquery.meio_mask.js', false);
$javascript->link("/carrinho/js/carrinho.js", false);
?>

<!-- start breadcrumb -->
<div class="breadcrumb third">
	<ul class="right">
		<li><a href="javascript:void(0);" title="Voltar" class="btn_voltar">Voltar</a></li>
	</ul>
	<h2 class="page-title fnt-size second">MEU CARRINHO</h2>      	
</div>	
<!-- end breadcrumb -->
<!-- start cart-block -->
<form action="<?php echo $this->Html->Url('/carrinho/entrega'); ?>" method="post">
<div class="cart-block">
	<!-- start row1 -->
	<div class="row1">
		<div class="td1">PRODUTO</div>
		<div class="td2">QUANTIDADE</div>
		<div class="td3">VALOR UNITÁRIO</div>
		<div class="td4">VALOR TOTAL</div>
	</div>
	<!-- end row1 -->
	
	<?php if(!empty($data)): ?>
	
		<!-- start cart-box -->
		<div class="cart-box">		
			<?php foreach ($data as $k => $i): ?>
				<!-- start row2 -->
				<div class="row2">
					<!-- start td1 -->
					<div class="td1 green">
						<a target="_blank" href="<?php e($this->Html->Url("/" . low(Inflector::slug($i['nome'], '-')) . '-' . 'prod-' . $i['produto_id'])) ?>.html" title="<?php echo $i['nome'] ?>">
							<?php echo $image->resize( ( $i['imagem'] ) ? $i['imagem'] : 'uploads/produto_imagem/filename/sem_imagem.jpg', 54, 37); ?>
							<span><?php echo $i['nome']; ?></span>
						</a>
					</div>
					<!-- end td1 -->
					<!-- start td2 -->
					<div class="td2">
					   <?php
							echo $form->input("Produto.{$i['produto_id']}.quantidade",
									array("class" => "input input2 quantidade quantidade-produto",
										"id" => "produto-{$i['produto_id']}",
										"value" => $i['quantidade'],
										"label" => false,
										"div" => false,
										"rel" => $i['produto_id']));
						?>
						<div class="clear"></div>
						<span class="gray error"id="quantidade-erro-<?php echo $i['produto_id']; ?>" style="display:none; color: red; font-size: 12px;">Quantidade Indisponível</span>
						<div class="clear"></div>
						<span><a href="javascript:void(0);" title="Alterar">Alterar quantidade</a>
						<a href="<?php echo $html->url("/carrinho/carrinho/remove/{$i['produto_id']}"); ?>" title="Retirar da cesta">Retirar da cesta</a></span>
					</div>
					<!-- end td2 -->
					<!-- start td3 -->
					<div class="td3">
						R$ <?php $valor = ($i['preco_promocao'] > 0) ? $i['preco_promocao'] : $i['preco']; ?> <?php echo $string->decimalFormat("human", $valor); ?>
					</div>
					<!-- end td3 -->
					<!-- start td4 -->
					<div class="td4">
						R$ <span class="produto-totalizador" id="produto-total-<?php e($i['produto_id']); ?>" style="<?php if(isset($i['preco_com_desconto'])){echo "text-decoration:line-through";}?>"><?php echo $string->decimalFormat("human", ($valor * $i['quantidade'])); ?></span>
						<div class="cupom-lbl produto-totalizador-cupom" id="produto-total-desconto-<?php e($i['produto_id']); ?>"><?php if(isset($i['preco_com_desconto'])){echo 'R$ '.$string->decimalFormat("human", ($i['preco_com_desconto'] ));} ?></span></div>
					</div>
					<!-- start td4 -->
				</div>
				<!-- end row2 -->
			<?php endForeach; ?>
		
			<!-- start row3 -->
			<div class="row3">
				<div class="td1 green">
					<?php echo $this->Html->image('site/ico_delivery.png', array('alt' => 'Frete', 'width' => '27', 'height' => '17'))?>
					<span>Calcule o valor do frete e prazo de entrega:</span>
					<?php echo $form->text("Frete.cep", array("class" => "input input1 input2 cep mask-cep", "id" => "cep")); ?>
					<a href="javascript:void(0);" class="button" id="consultar-frete">ok</a>
					<span class="url-correios">Não sabe seu CEP? <a href="http://www.buscacep.correios.com.br/" target="_blank">Clique aqui</a></span>
				</div>
				<div class="td2" style="position: relative;">				
					<!-- start frete-right -->
					<div class="frete-right">
						<label id="loading" style="float: left; vertical-align: middle; margin-top: -1px; margin-left: -10px; display: none; visible: hidden;">
							<?php echo $this->Html->image('/img/site/zoomloader.gif', array('alt' => 'Carregando','title' => 'Carregando'));?>
						</label>
						<!-- start frete-disponiveis -->
						<div class="fretes-disponiveis">
							<?php 
							if(!empty($frete_tipos)){
								foreach($frete_tipos as $chave=>$frete){
									$frete['Frete'][0]['preco'] = $this->String->bcoToMoeda($frete['Frete'][0]['preco']);
									echo $form->input('Frete.frete', 
														array(
															'class'=>'tipo-entrega',
															'type' => 'radio',
															'name' => 'Frete.frete',
															'options' => array($frete['Frete'][0]['id'] => "{$frete['FreteTipo']['nome']} - Prazo de entrega em até {$frete['Frete'][0]['prazo']} dias úteis - R$ {$frete['Frete'][0]['preco']}")
															)
													);
								}
							}
							else{
							?>
								<div class="fretes-disponiveis-msg"></div>
							<?php
							}
							?>
						</div>
						<!-- end frete-disponiveis -->
					</div>
					<!-- end frete-right -->		

				</div>
				<div class="td3">
					<!-- start valor_frete -->
					<div id="valor_frete">
						FRETE: R$ <span id="frete" style="<?php if(isset($totais['frete_com_desconto'])&&$totais['frete_com_desconto']>0){echo "text-decoration:line-through";}?>"><?php echo $totais['frete']; ?></span>
						<!-- start td2 fright -->
						<div class="td2 fright">
							<span class="cupom-lbl" id="frete-com-desconto"><?php echo (isset($totais['frete_com_desconto'])&&$totais['frete_com_desconto']>0)?'<strong>R$ </strong> '.$totais['frete_com_desconto']:''; ?></span>
						</div>
						<!-- end td2 fright -->
					</div>
					<!-- end valor_frete -->
				</div>
			</div>
			<!-- end row3 -->
                        
			<!-- start row4 -->
			<div class="row3">
				<div class="td1 green">
				  <?php echo $this->Html->image('site/ico_coupon.png', array('alt' => 'coupon', 'width' => '25', 'height' => '18'))?>
				   <span>Digite aqui seu Cupom / Vale presente</span>
					<?php echo $form->text("Cupom.cupom", array('div'=>false,'label'=>false,'class' => 'input input1 input2', 'id' => 'cupom'));?>
					 <a href="javascript:void(0);" class="button" id="calcular-cupom">ok</a>
				</div>
				<!-- start td2 -->		
				<div class="td2">								
					<!-- start frete-right -->
					<div class="frete-right">
						<label id="loading" style="float: left; margin-left: -15px; display:none">
							<?php echo $this->Html->image('/img/site/zoomloader.gif', array('alt' => 'Carregando','title' => 'Carregando'));?>
						</label>
						<em class="cupom-lbl cupom-msg"><?php echo isset($this->data["Cupom"]["msg"])?$this->data["Cupom"]["msg"]:'';?></em>
					</div>
					<!-- end frete-right -->
					<!-- start clear -->
					<div class="clear"></div>
					<!-- end clear -->
				</div>
				<!-- end td2 -->
			</div>
			<!-- end row4 -->
                        
                        <!-- start row3 -->
			<div class="row3 last">
                            <div class="observacoes">
                            <label>Observações:</label>
                            <textarea name="observacoes"></textarea>
                            </div>
			</div>
			<!-- end row3 -->
                        
		</div>
		<!-- end cart-box -->
		<div class="row4">
			<div class="left">
				<strong>Atenção: O prazo de entrega dos Correios conta a partir da aprovação do pagamento do pedido pela FuelTech, que pode demorar até 3 dias úteis. Para qualquer forma de pagamento o prazo para expedição do pedido é de 5 (cinco) dias úteis, após a confirmação do pagamento.</strong>
			</div>
			<!-- start right -->
			<div class="right">
				<div class="clear"></div>
				TOTAL: R$ <strong id="total"><?php echo $totais['total']; ?></strong>
			</div>
			<!-- end right -->
		</div>
		
	<?php else: ?>
	
		<div class="carrinho">
			<p style="text-align: center; padding: 15px; font-size: 13px; margin-top: 30px;">Você não possui nenhum produto no seu carrinho.</p>
		</div>
	
	<?php endIf; ?>
</div>
<!-- end cart-block -->
<!-- start button-block -->
<div class="button-block">
	<div class="left">
		<a href="<?php echo $this->Html->Url('/'); ?>" class="gary-button" title="COMPRAR MAIS PRODUTOS">COMPRAR MAIS PRODUTOS</a>
	</div>
	<?php if(!empty($data)): ?>
	<div class="right">
		<!--<a href="<?php echo $this->Html->Url('/carrinho/entrega'); ?>" class="red-button" title="FINALIZAR COMPRA">FINALIZAR COMPRA</a>-->
                <input type="submit" class="red-button" value="FINALIZAR COMPRA">
	</div>
	<?php endIf; ?>
</div>
</form>
<!-- end button-block -->
<!-- start product-price -->
<div class="product-price second" style="display: none;">
	<!-- start left-col1 -->
	<div class="left-col1">
		<h3>COMPRE JUNTO</h3>
		<ul>
			<li>
				<h4>FT 200</h4>
				<?php echo $this->Html->image('site/img_product.jpg', array('alt' => 'product', 'width' => '152', 'height' => '113'))?>
				<p><strong><small>R$ 1.379,00</small> à vista</strong>ou em 4x R$ 344,75.</p>
			</li>
			<li>
				<h4>FT 200</h4>
				<?php echo $this->Html->image('site/img_product2.jpg', array('alt' => 'product', 'width' => '152', 'height' => '113'))?>
				<p><strong><small>R$ 1.379,00</small> à vista</strong>ou em 4x R$ 344,75.</p>
			</li>
			<li>
				<h4>FT 200</h4>
				<?php echo $this->Html->image('site/img_product.jpg', array('alt' => 'product', 'width' => '152', 'height' => '113'))?>
				<p><strong><small>R$ 1.379,00</small> à vista</strong>ou em 4x R$ 344,75.</p>
			</li>
		</ul>
	</div>
	<!-- end left-col1 -->
	<!-- start right-col1 -->
	<div class="right-col1">
		<span class="red">
			<b>À vista</b>
			<strong>R$ 409,90</strong>
			<b>ou em até 12x sem juros</b>
			<small>R$ 40,90</small>
		</span>
		<span class="gray">
			<b>Economize: R$ 17,34</b>
			<strong>(4,00%)</strong>
		</span>
		<div class="clear"></div>
		<a href="#" title="Comprar os 3 juntos" class="button7"><strong>Comprar</strong> os <strong>3</strong> juntos</a>
	</div>
	<!-- end right-col1 -->
</div>
<!-- end product-price -->
<div class="clear"></div>