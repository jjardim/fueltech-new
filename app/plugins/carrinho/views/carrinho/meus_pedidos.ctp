<?php $javascript->link("/carrinho/js/meus_pedidos.js", false);  ?>

<!-- start leftcol -->
<div id="leftcol">
	<?php echo $this->element('site/left'); ?>
</div>
<!-- end leftcol -->

<!-- start rightcol -->
<div id="rightcol">
	<h2 class="page-title"><small>Meus Pedidos</small></h2>
	<!-- start table2 -->
	<div class="table2">
		<h3>Status do pedido </h3>
		<!-- start row -->
		<div class="row">
			<div class="td1"><?php echo $this->Paginator->sort('Número do Pedido', 'id'); ?></div>
			<div class="td2"><?php echo $this->Paginator->sort('Data', 'created'); ?></div>
			<div class="td3"><?php echo $this->Paginator->sort('Status', 'PedidoStatus.nome'); ?></div>
			<div class="td4">ver detalhes</div>
		</div>
		<!-- end row -->
		
		<?php
		if( count($data) >= 1 ):
		
			$i = 1;
			foreach ($data as $valor):
			?>
				<!-- start row -->
				<div class="row odd">
					<div class="td1"><?php echo $valor['Pedido']['id'] ?></div>
					<div class="td2"><?php echo $calendario->dataFormatada("d/m/Y", $valor['Pedido']['created']) ?></div>
					<div class="td3"><?php echo $valor['PedidoStatus']['nome'] ?></div>
					<div class="td4">
						<a href="javascript:void(0);" title="Ver detalhe" class="pedido icon-search" rel="<?php echo $valor['Pedido']['id'];?>" >
							<span style="display: none;">ocultar detalhes</span>
							<?php echo $this->Html->image('site/ic_search.png', array('alt' => 'search', 'width' => '15', 'height' => '18'))?>
						</a>
					</div>
				</div>
				<!-- end row -->
				
				<!-- start pedido-detalhe -->
				<div id="pedido-detalhe-<?php echo $valor['Pedido']['id'] ?>" style="display:none;">
				
					<!-- start row -->
					<div class="row red">
						<div class="td1">Produto</div>
						<div class="td2">&nbsp;</div>
						<div class="td3">Quantidade</div>
						<div class="td4">Valor unitário</div>
					</div>
					<!-- end row -->
					
					<!-- start row -->
					<div class="row row2">
						<?php
						$total = 0;
						foreach ($valor['PedidoItem'] as $item):
							$valor_item = $item['preco_promocao'] > 0 ? $item['preco_promocao'] : $item['preco'];
							$subtotal   = $item['quantidade'] * $valor_item;
							$total     += $subtotal;
						?>
							<div class="td1">
								<a href="<?php e($this->Html->Url("/" . low(Inflector::slug($item['nome'], '-')) . '-' . 'prod-' . $item['produto_id'])) ?>.html" title="<?php echo htmlentities($item['nome'], ENT_QUOTES, 'utf-8')?>">
									<span style="width: 70px; display: block; margin-right: 5px; float: left; text-align: center;">
										<?php echo $image->resize( ( $item['imagem'] ) ? $item['imagem'] : 'uploads/produto_imagem/filename/sem_imagem.jpg', 70, 70); ?>
									</span>
									<span><?php echo $item['nome'] ?></span>
								</a>
							</div>
							<div class="td2"><?php echo $item['quantidade'] ?></div>
							<div class="td3">&nbsp;</div>
							<div class="td4">R$ <?php echo $this->String->bcoToMoeda($valor_item) ?></div>
							<div class="clear"></div>
						<?php
						endForeach;
						?>
						<div class="row row2">
							<div class="td1">&nbsp;</div>
							<div class="td2">&nbsp;</div>
							<div class="td3">Frete</div> 
							<div class="td4">R$ <?php echo $valor['Pedido']['valor_frete']; ?></div>
							<div class="clear"></div>
							<div class="td1">&nbsp;</div>
							<div class="td2">&nbsp;</div>
							<div class="td3">Valor total</div>
							<div class="td4">R$ <?php echo $this->String->bcoToMoeda($total + $this->String->moedaToBco($valor['Pedido']['valor_frete'])); ?></div>
							<div class="clear"></div>
							<div class="td5">Endereço de entrega:</div>
							<div class="td6"><?php echo $valor['Pedido']['endereco_rua'] ?>, <?php echo $valor['Pedido']['endereco_numero'] ?> - <?php echo $valor['Pedido']['endereco_bairro'] ?> - CEP <?php echo $valor['Pedido']['endereco_cep'] ?> - <?php echo $valor['Pedido']['endereco_cidade'] ?> - <?php echo $valor['Pedido']['endereco_estado'] ?></div>
							<div class="clear"></div>
							<div class="td5">Forma de pagamento:</div>
							<div class="td6"><?php echo $valor['PagamentoCondicao']['nome']; ?></div>
							<div class="td5">Rastreamento:</div>
							
							<?php if(count($valor['PedidoHistoricoTracking']) > 0):
								$tracking = array_reverse($valor['PedidoHistoricoTracking']);
							?>
								<div class="td6"><a href="<?php echo "http://websro.correios.com.br/sro_bin/txect01$.QueryList?P_LINGUA=001&P_TIPO=001&P_COD_UNI=". trim($tracking[0]['tracking']); ?>" target="_blank"><?php echo trim($tracking[0]['tracking']); ?></a></div>
							<?php else: ?>
								<div class="td6">Indisponível</div>
							<?php
								endIf;
							?>
                                                                
                                                        <?php if($valor['Pedido']['url_boleto'] != ""): ?>
                                                                <div class="td5">Reimpressão de Boleto:</div>
                                                                <div class="td6"><a href="<?php echo $valor['Pedido']['url_boleto']; ?> " target="_blank">Imprimir Boleto</a></div>
                                                        <?php endIf; ?>
							<div class="clear"></div>
						</div>
						<!-- end row -->
					</div>
				</div>
				<!-- end pedido-detalhe -->			
				
			<?php
				$i++;
			endforeach;
			
		else:
			?>
			
			<!-- start row -->
			<div class="row odd" style="text-align: center;">
				Nenhum pedido encontrado.
			</div>
			<!-- end row -->
			
		<?php endIf; ?>
		
		<div class="clear"></div>
	</div>
	<!-- end table2 -->
	<div class="clear"></div>
</div>
<!-- end rightcol -->

<div class="clear"></div>