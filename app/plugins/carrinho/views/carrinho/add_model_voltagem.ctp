<!-- start detail-lightbox -->
<div class="detail-lightbox detail-lightbox-voltagem">						
	<h2>Atenção</h2>
	<div class="imgb" style="width: 155px;">
		<?php //echo $image->resize( isset($produto['ProdutoImagem'][0] ) ? $produto['ProdutoImagem'][0]['dir'] . '/' . $produto['ProdutoImagem'][0]['filename']  : 'uploads/produto_imagem/filename/sem_imagem.jpg', 210, 125); ?>
		<?php echo $image->resize( isset($data['imagem'] ) ? $data['imagem']  : 'uploads/produto_imagem/filename/sem_imagem.jpg', 210, 125); ?>
	</div>	
	<h4><?php e($data['nome']) ?></h4>
	<p class="texto-voltagem" style="width: 300px; float: left; margin-top: 25px; padding-bottom: 20px">O produto escolhido é <?php e($data['voltagem']); ?>. Adicionar no carrinho?</p>
	<br /><br />
	<a href="<?php e($this->Html->url("/carrinho/add/{$data['id']}"))?>" title="Comprar" class="button last">Sim</a>
	<a href="javascript:void(0);" title="Comprar" class="nyroModalClose button">Não</a>
</div>
<!-- end detail-lightbox -->