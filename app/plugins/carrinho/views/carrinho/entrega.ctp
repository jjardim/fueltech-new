<?php echo $this->Javascript->link('common/jquery.meio_mask.js'); ?>
<?php echo $javascript->link("/carrinho/js/entrega.js", false);?>

<!-- start header -->
<div id="header">
	<!-- start logo -->
	<div id="logo">
		<a href="<?php e($this->Html->Url('/')) ?>" title="Fuel Tech">
			<?php echo $this->Html->image('site/logo.png', array('alt' => 'Fuel Tech', 'width' => '219', 'height' => '53'))?>
		</a>
	</div>
	<!-- end logo -->
	<!-- start header mid -->
	<div id="header-mid">
		&nbsp;
	</div>
	<!-- end header mid -->
	<!-- start header right -->
	<div id="header-right">
		<!-- start language -->
		<div class="language">
			<ul>
                <li><span><?php echo __("VERSION"); ?>: </span></li>
				<?php 
					if(isset($menu_idiomas) && count($menu_idiomas) > 0):
						$first = true;
						foreach($menu_idiomas as $idioma): 
							if(Configure::read('Config.language') == $idioma['Linguagem']['codigo']){
								$class = "class='active'";
							}else{
								$class = "";
							}
				?>		
							<?php if(!$first): ?>
								<li>&nbsp; | &nbsp;</li>
							<?php endIf; ?>
							<li <?php echo $class ?>>
								<?php if($idioma['Linguagem']['externo'] == 0): ?>
									<a href="<?php echo $this->Html->Url('/').$idioma['Linguagem']['codigo']; ?>" title="<?php echo $idioma['Linguagem']['nome']; ?>">
										<?php //echo $image->resize(isset($idioma['Linguagem']['thumb_filename']) ? $idioma['Linguagem']['thumb_dir'] . '/' . $idioma['Linguagem']['thumb_filename'] : 'uploads/produto_imagem/filename/sem_imagem.jpg', 12, 8, true, array('alt' => $idioma['Linguagem']['nome'])); ?>
										<?php echo $idioma['Linguagem']['nome']; ?>
									</a>
								<?php else: ?>
									<a href="<?php echo $idioma['Linguagem']['url']; ?>" title="<?php echo $idioma['Linguagem']['nome']; ?>">
										<?php //echo $image->resize(isset($idioma['Linguagem']['thumb_filename']) ? $idioma['Linguagem']['thumb_dir'] . '/' . $idioma['Linguagem']['thumb_filename'] : 'uploads/produto_imagem/filename/sem_imagem.jpg', 12, 8, true, array('alt' => $idioma['Linguagem']['nome'])); ?>
										<?php echo $idioma['Linguagem']['nome']; ?>
									</a>
								<?php endIf; ?>
							</li>
				<?php
							if($first){ $first = false; }
						endForeach;
					endIf;
				?>
            </ul>
		</div>
		<!-- end language -->
		<div class="clear"></div>
	</div>
	<!-- end header right -->
	<!-- start numerical -->
	<div id="numerical">
		<ul>
			<li><a href="<?php echo $this->Html->url("/carrinho") ?>" title="CARRINHO" class="visited"><span>01</span><strong>CARRINHO</strong></a></li>
			<li><a href="<?php echo $this->Html->url("/usuario/login") ?>" title="Identificação" class="visited"><span>02</span><strong>Identificação</strong></a></li>
			<li><a href="javascript:void(0);" title="Entrega" class="active"><span>03</span><strong>Entrega<b>Você está aqui</b></strong></a></li>
			<li><a href="javascript:void(0);" title="Pagamento"><span>04</span><strong>Pagamento</strong></a></li>
			<li class="last"><a href="javascript:void(0);" title="Confirmação"><span>05</span><strong>Confirmação</strong></a></li>
		</ul>
	</div>
	<!-- end numerical -->
	<div class="clear"></div>
</div>
<!-- end header -->

<!-- start container -->
<div id="container">
	<h2 class="page-title fnt-size">ENDEREÇO DE ENTREGA</h2>
	<!-- col2 -->
	<div class="col2">
		<h3>Meus Endereços</h3>
		<!-- start column3 -->
		<div class="column3">
			<!-- start column1-box -->
			<div class="column1-box">
				<ul class="list1">
					<?php if ($enderecos):
						foreach ($enderecos as $valor):
					?>
						<li><a href="javascript:void(0);" title="<?php e($valor['UsuarioEndereco']['nome']) ?>" rel="<?php e($valor['UsuarioEndereco']['id']) ?>"><?php e($valor['UsuarioEndereco']['nome']) ?></a></li>
					<?php
						endforeach;
					endif;
					?>
				</ul>
				<a href="javascript:void(0);" title="CADASTRAR NOVO ENDEREÇO" class="button1" id="btn-cadastrar-novo-endereco">CADASTRAR NOVO ENDEREÇO</a>
				<div class="clear"></div>
			</div>
			<!-- end column1-box -->
		</div>
		<!-- end column3 -->
	</div>
	<!-- col2 -->
	<!-- start col3 -->
	<div class="col3">
		<?php if ($enderecos):
				foreach ($enderecos as $valor):
		?>
				<!-- start box-dados-endereco -->
				<div class="box-dados-endereco" id="endereco-<?php e($valor['UsuarioEndereco']['id']); ?>" >
					<h3><?php e($valor['UsuarioEndereco']['nome']); ?></h3>
					<!-- start column4 -->
					<div class="column4">
						<!-- start column1-box -->
						<div class="column1-box">
							<address>
								<strong><?php e($valor['UsuarioEndereco']['rua']); ?>, <?php e($valor['UsuarioEndereco']['numero']); ?></strong>
								<br />
								<?php e($valor['UsuarioEndereco']['bairro']); ?>
								<br />
								<?php e($valor['UsuarioEndereco']['cidade']); ?> 
								<?php e($valor['UsuarioEndereco']['uf']); ?> - <?php e($valor['UsuarioEndereco']['cep']); ?>
								<br />
								Endereço de cobrança ( <?php echo ($valor['UsuarioEndereco']['cobranca']?'Sim':'Não'); ?> )  <?php echo $this->Html->link(__('Editar', true), array('plugin' => false, 'controller' => 'usuario_enderecos', 'action' => 'edit', $valor['UsuarioEndereco']['id']), array('rel'=>$valor['UsuarioEndereco']['id'],'class' => 'editar')); ?>
							</address>
							<?php echo $this->Html->link(__('ENTREGAR NESTE ENDEREÇO', true), array('plugin' => 'carrinho', 'controller' => 'carrinho', 'action' => 'usar_este', $valor['UsuarioEndereco']['id']), array('class' => 'button1')); ?>
							<span><?php echo $this->Html->link(__('Editar endereço', true), array('plugin' => false, 'controller' => 'usuario_enderecos', 'action' => 'edit', $valor['UsuarioEndereco']['id']), array('rel'=>$valor['UsuarioEndereco']['id'],'class' => 'editar')); ?> | <?php echo $this->Html->link(__('Excluir endereço', true), array('plugin' => false, 'controller' => 'usuario_enderecos', 'action' => 'delete', $valor['UsuarioEndereco']['id']), array('title' => 'Excluir endereço'), sprintf(__('Deseja mesmo remover o endereço # %s?', true), $valor['UsuarioEndereco']['nome'])); ?></span>
						</div>
						<!-- end column1-box -->
					</div>
					<!-- end column4 -->
				</div>
				<!-- end box-dados-endereco -->
			<?php
				endforeach;
			endif;
		?>
	</div>
	<!-- start col3 -->
	<div class="clear"></div>
	<!-- start common1st -->
	<div class="common1st form-endereco-content-entrega" id="form-endereco-content">
		<h3>CADASTRAR NOVO ENDEREÇO DE ENTREGA</h3>
		<!-- start column5 -->
		<div class="column1 column5">
			<!-- start column1-box -->
			<div class="column1-box">
				<?php echo $this->Form->create(null, array('class'=>'','url' => array('plugin' => 'carrinho', 'controller' => 'carrinho', 'action' => 'entrega'))); ?>
					<div class="row1 radio">
						<?php echo $this->Form->input('UsuarioEndereco.cobranca', array('default'=>0,'div'=>false,'legend' => 'Tipo de Endereço *', 'id' => 'UsuarioEnderecoCobranca','class' => 'radio', 'type' => 'radio', 'options' => array(1 => 'Endereço de Cobrança', 0 => 'Endereço de Entrega'))); ?>
					</div>
					<div class="row1">
						<?php echo $this->Form->input('UsuarioEndereco.id', array('div'=>false,'id' => 'UsuarioEnderecoId', 'label' => false)); ?>
						<?php echo $this->Form->input('UsuarioEndereco.cep', array('div'=>false,'id' => 'UsuarioEnderecoCep', 'class' => 'input input2','rel'=>'Cep * (somente números)', 'label' => 'Cep *(somente números)' )); ?>
						<?php echo $this->Html->image('/img/site/zoomloader.gif', array('id' => 'loading', 'style' => 'visibility: hidden; vertical-align: middle; margin-left: 10px; margin-top: 5px;', 'alt' => 'Carregando', 'title' => 'Carregando'));?>
						<span class="url-correios-2">Não sabe seu CEP? <a href="http://www.buscacep.correios.com.br/" target="_blank">Clique aqui.</a></span>
					</div>					
					<div class="row1">
						<?php echo $this->Form->input('UsuarioEndereco.nome', array('div'=>false,'id' => 'UsuarioEnderecoNome', 'class' => 'input','rel'=>'Identificação do Endereço * (Ex: Residencial, Comercial, etc.)', 'label' => 'Identificação do Endereço * (Ex: Residencial, Comercial, etc.)')); ?>
					</div>
					<div class="row1">
						<?php echo $this->Form->input('UsuarioEndereco.rua', array('div'=>false,'id' => 'UsuarioEnderecoRua', 'class' => 'input','rel'=>'Endereço *', 'label' => 'Endereço *')); ?>
					</div>
					<div class="row1">
						<?php echo $this->Form->input('UsuarioEndereco.numero', array('div'=>false,'id' => 'UsuarioEnderecoNumero', 'class' => 'input','rel'=>'Número *', 'label' => 'Número *')); ?>
					</div>
					<div class="row1">
						<?php echo $this->Form->input('UsuarioEndereco.complemento', array('div'=>false,'id' => 'UsuarioEnderecoComplemento', 'class' => 'input','rel'=>'Complemento', 'label' => 'Complemento')); ?>
					</div>
					<div class="row1">
						<?php echo $this->Form->input('UsuarioEndereco.bairro', array('div'=>false,'id' => 'UsuarioEnderecoBairro', 'class' => 'input','rel'=>'Bairro *', 'label' => 'Bairro *')); ?>
					</div>
					<div class="row1">
						<?php echo $this->Form->input('UsuarioEndereco.uf', array('div'=>false,'id' => 'UsuarioEnderecoUf','class' => 'select','rel'=>'Estado *', 'label' => 'Estado *', 'options' => $this->Estados->estadosBrasileiros())); ?>
					</div>
					<div class="row1">
						<?php echo $this->Form->input('UsuarioEndereco.cidade', array('div'=>false,'id' => 'UsuarioEnderecoCidade',  'class' => 'input input2','rel'=>'Cidade *', 'label' => 'Cidade *')); ?>
					</div>
					<div class="row1">
						<input name="button" type="submit" value="SALVAR" class="button1" />
					</div>					
				<?php echo $this->Form->end(); ?>
				<div class="clear"></div>
			</div>
			<!-- end column1-box -->
		</div>
		<!-- end column5 -->
		<div class="clear"></div>
	</div>
	<!-- end common1st -->
	<div class="clear"></div>
</div>
<!-- end container -->

<div class="clear"></div>
<div class="clear"></div>