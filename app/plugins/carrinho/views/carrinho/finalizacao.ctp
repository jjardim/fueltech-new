<!-- start header -->
<div id="header">
	<!-- start logo -->
	<div id="logo">
		<a href="<?php e($this->Html->Url('/')) ?>" title="Fuel Tech">
			<?php echo $this->Html->image('site/logo.png', array('alt' => 'Fuel Tech', 'width' => '219', 'height' => '53'))?>
		</a>
	</div>
	<!-- end logo -->
	<!-- start header mid -->
	<div id="header-mid">
		&nbsp;
	</div>
	<!-- end header mid -->
	<!-- start header right -->
	<div id="header-right">
		<!-- start language -->
		<div class="language">
			<ul>
                <li><span><?php echo __("VERSION"); ?>: </span></li>
				<?php 
					if(isset($menu_idiomas) && count($menu_idiomas) > 0):
						$first = true;
						foreach($menu_idiomas as $idioma): 
							if(Configure::read('Config.language') == $idioma['Linguagem']['codigo']){
								$class = "class='active'";
							}else{
								$class = "";
							}
				?>		
							<?php if(!$first): ?>
								<li>&nbsp; | &nbsp;</li>
							<?php endIf; ?>
							<li <?php echo $class ?>>
								<?php if($idioma['Linguagem']['externo'] == 0): ?>
									<a href="<?php echo $this->Html->Url('/').$idioma['Linguagem']['codigo']; ?>" title="<?php echo $idioma['Linguagem']['nome']; ?>">
										<?php //echo $image->resize(isset($idioma['Linguagem']['thumb_filename']) ? $idioma['Linguagem']['thumb_dir'] . '/' . $idioma['Linguagem']['thumb_filename'] : 'uploads/produto_imagem/filename/sem_imagem.jpg', 12, 8, true, array('alt' => $idioma['Linguagem']['nome'])); ?>
										<?php echo $idioma['Linguagem']['nome']; ?>
									</a>
								<?php else: ?>
									<a href="<?php echo $idioma['Linguagem']['url']; ?>" title="<?php echo $idioma['Linguagem']['nome']; ?>">
										<?php //echo $image->resize(isset($idioma['Linguagem']['thumb_filename']) ? $idioma['Linguagem']['thumb_dir'] . '/' . $idioma['Linguagem']['thumb_filename'] : 'uploads/produto_imagem/filename/sem_imagem.jpg', 12, 8, true, array('alt' => $idioma['Linguagem']['nome'])); ?>
										<?php echo $idioma['Linguagem']['nome']; ?>
									</a>
								<?php endIf; ?>
							</li>
				<?php
							if($first){ $first = false; }
						endForeach;
					endIf;
				?>
            </ul>
		</div>
		<!-- end language -->
		<div class="clear"></div>
	</div>
	<!-- end header right -->
	<!-- start numerical -->
	<div id="numerical">
		<ul>
			<li><a href="<?php echo $this->Html->url("/carrinho") ?>" title="CARRINHO" class="visited"><span>01</span><strong>CARRINHO</strong></a></li>
			<li><a href="<?php echo $this->Html->url("/usuario/login") ?>" title="Identificação" class="visited"><span>02</span><strong>Identificação</strong></a></li>
			<li><a href="<?php echo $this->Html->url("/carrinho/entrega") ?>" title="Entrega" class="visited"><span>03</span><strong>Entrega</strong></a></li>
			<li><a href="<?php echo $this->Html->url("/carrinho/forma_pagamento") ?>" title="Pagamento" class="visited"><span>04</span><strong>Pagamento</strong></a></li>
			<li class="last"><a href="javascript:void(0);" title="Confirmação" class="active"><span>05</span><strong>Confirmação<b>Você está aqui</b></strong></a></li>
		</ul>
	</div>
	<!-- end numerical -->
	<div class="clear"></div>
</div>
<!-- end header -->

<!-- start container -->
<div id="container">
	<h2 class="page-title fnt-size second">CONFIRMAÇAO DA COMPRA</h2>
	<h4 class="page-title">Obrigado por comprar na FuelTech<span style="float: right; width: auto; margin-top: -40px;"><a class="link-voltar-home gray" href="<?php e($this->Html->Url('/')) ?>" title="Voltar para a Loja">VOLTAR PARA A LOJA</a></span></h4>
	<!-- start column8 -->
	<div class="column8 bg-color">
		<!-- start col6 -->
		<div class="col6">
			<h3>NÚMERO DO PEDIDO</h3>
			<!-- start paypalcol -->
			<div class="paypalcol">
				<!-- start paypalcol-box -->
				<div class="paypalcol-box">
					<span class="code"><?php echo $dados['Pedido']['id']; ?></span>
				</div>
				<!-- end paypalcol-box -->
			</div>
			<!-- end paypalcol -->
		</div>
		<!-- end col6 -->
		<!-- start col6 -->
		<div class="col6">
			<h3>FORMA DE PAGAMENTO</h3>
			<!-- start paypalcol -->
			<div class="paypalcol">
                <!-- start paypalcol-box -->
				<div class="paypalcol-box" style="text-align: center">	
					<?php
                                        if(isset($dados['PagamentoCondicaoImagem'])){
                                                $imagem = $dados['PagamentoCondicaoImagem']['dir'] . DS . $dados['PagamentoCondicaoImagem']['filename'];
                                                $lbl = '<img src="' . $this->Html->url('/' . str_replace('\\', '/', $imagem)) . '" alt="'.$dados['PagamentoCondicao']['nome'].'" title="'.$dados['PagamentoCondicao']['nome'].'" />';
                                        }else{
                                                $lbl = $dados['PagamentoCondicao']['nome'];
                                        }
                                        echo '<span style="width: 100%; text-align: center;display: block;">'.$lbl.'</span>';
                                        ?>
					<?php echo $dados['PagamentoCondicao']['nome']; ?> em <?php echo $dados['Pedido']['parcelas']; ?>x sem juros
					<?php if($dados['Pedido']['url_boleto']!=""): ?>
						<a class="button1" target="_blank" style="margin-top: 5px;" href="<?php echo $dados['Pedido']['url_boleto'];?>" title="Imprimir Boleto" alt="Imprimir Boleto">Imprimir Boleto</a>
					<?php endIf;?>
				</div>
				<!-- end paypalcol-box -->
			</div>
			<!-- end paypalcol -->
		</div>
		<!-- end col6 -->
		<!-- start col6 -->
		<div class="col6 last">
			<h3>ENDEREÇO DE ENTREGA</h3>
			<!-- start paypalcol -->
			<div class="paypalcol">
				<!-- start paypalcol-box -->
				<div class="paypalcol-box">
					<address>
						<span><strong class="new">&nbsp;</strong></span>
						<span><?php echo $dados['Pedido']['endereco_rua']; ?> , <?php echo $dados['Pedido']['endereco_numero']; ?></span>
						<span><?php echo $dados['Pedido']['endereco_bairro']; ?></span>
						<span><?php echo $dados['Pedido']['endereco_cidade']; ?> <?php echo $dados['Pedido']['endereco_estado']; ?> - <?php echo $dados['Pedido']['endereco_cep']; ?></span>
					</address>
				</div>
				<!-- end paypalcol-box -->
			</div>
			<!-- end paypalcol -->
		</div>
		<!-- end col6 -->
		<div class="clear"></div>
	</div>
	<!-- end column8 -->
	<div class="clear"></div>
	<h3 class="page-title">ITENS DA COMPRA</h3>
	<!-- start column8 -->
	<div class="column8 last">
		<!-- th1 -->
		<div class="tr1">
			<div class="td1"><strong>PRODUTO</strong></div>
			<div class="td2"><strong>QUANTIDADE</strong></div>
			<div class="td3"><strong>VALOR UNITÁRIO</strong></div>
			<div class="td4"><strong>VALOR TOTAL</strong></div>
		</div>
		<!-- th1 -->
		<div class="column1-box">
			<?php foreach ($dados['PedidoItem'] as $k => $i): ?>
				<!-- start tr2 -->
				<div class="tr2">
				   <div class="td1">
						<?php echo $image->resize( ( $i['imagem'] ) ? $i['imagem'] : 'uploads/produto_imagem/filename/sem_imagem.jpg', 54, 37, true, array('class' => 'thumb')); ?>
						<span class="name green"><?php echo $i['nome']; ?></span>
						<?php
						$valor = ($i['preco_promocao'] > 0) ? $i['preco_promocao'] : $i['preco'];
						$valor_total = $valor * $i['quantidade'];
						?>						
					</div>
					<div class="td2 pad"><?php echo $i['quantidade']; ?></div>
					<div class="td3">R$ <strong><?php echo $string->decimalFormat("human", $valor); ?></strong></div>
					<span class="td4">R$ <strong><?php echo $string->decimalFormat("human", $valor_total); ?></strong></span>
					<div class="clear"></div>
				</div>
				 <!-- end tr2 -->
			<?php endForeach;?>
			<div class="row2">
				<span class="free">FRETE: R$ <?php echo $dados['Pedido']['valor_frete']; ?></span>
			</div>
		</div>
		<div class="clear"></div>
		<div class="row3">
			<span class="orange new">TOTAL: R$ <?php echo $string->decimalFormat("human", $string->moedaToBco($dados['Pedido']['valor_pedido']) + $string->moedaToBco($dados['Pedido']['valor_frete'])); ?></span>
		</div>
	</div>
	<!-- end column8 -->
	<div class="clear"></div>
</div>
<!-- end container -->


<?php if(Configure::read('Reweb.analytics_id')!=""): ?>  
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', '<?php echo Configure::read('Reweb.analytics_id'); ?>']);
  _gaq.push(['_trackPageview']);
  _gaq.push(['_addTrans',
    '<?php echo $dados['Pedido']['id']; ?>',           // order ID - required
    '<?php echo Configure::read('Loja.nome'); ?>',  // affiliation or store name
    '<?php echo $string->moedaToBco($dados['Pedido']['valor_pedido'])+$string->moedaToBco($dados['Pedido']['valor_frete']); ?>',          // total - required
    '0.00',           // tax
    '<?php echo $string->moedaToBco($dados['Pedido']['valor_frete']); ?>',              // shipping
    '<?php echo  $dados['Pedido']['endereco_cidade']; ?>',       // city
    '<?php echo  $dados['Pedido']['endereco_estado']; ?>',     // state or province
    'BRA'             // country
  ]);
   // add item might be called for every item in the shopping cart
   // where your ecommerce engine loops through each item in the cart and
   // prints out _addItem for each
<?php foreach($dados['PedidoItem'] as $i): ?>
  _gaq.push(['_addItem',
		"<?php echo $dados['Pedido']['id']; ?>",  // order ID - necessary to associate item with transaction
      "<?php echo $i['sku']; ?>",           	// SKU/code - required
      "<?php echo $i['nome']; ?>",        		// product name
      "<?php 
	  
	  $categoria_json = json_decode($i['categorias']);
	  echo current($categoria_json); 
	  
	  ?>",   						// category or variation
      "<?php echo ($i['preco_promocao'] > 0) ? $i['preco_promocao'] : $i['preco']; ?>",          // unit price - required
      "<?php echo $i['quantidade']; ?>"         // quantity - required
  ]);
  <?php endforeach; ?>
  _gaq.push(['_trackTrans']); //submits transaction to the Analytics servers

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<?php endIf; ?>

<?php if(Configure::read('Reweb.conversion_id')!=""&&Configure::read('Reweb.conversion_label')): ?>
<!-- Google Code for Venda OK Conversion Page -->
<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = <?php echo Configure::read('Reweb.conversion_id'); ?>;
	var google_conversion_language = "pt";
	var google_conversion_format = "3";
	var google_conversion_color = "ffffff";
	var google_conversion_label = "<?php echo Configure::read('Reweb.conversion_label'); ?>";
	var google_conversion_value = "<?php echo $string->bcoToMoeda($string->moedaToBco($dados['Pedido']['valor_pedido'])+$string->moedaToBco($dados['Pedido']['valor_frete'])); ?>";

	/* ]]> */
</script>
<script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion.js"></script>
<noscript>
<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="https://www.googleadservices.com/pagead/conversion/<?php echo Configure::read('Reweb.conversion_id'); ?>/?value=<?php echo $string->bcoToMoeda($string->moedaToBco($dados['Pedido']['valor_pedido'])+$string->moedaToBco($dados['Pedido']['valor_frete'])); ?>&amp;label=<?php echo Configure::read('Reweb.conversion_label'); ?>&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<?php endIf;?>