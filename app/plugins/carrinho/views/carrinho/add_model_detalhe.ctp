<!-- start lightbox-product-add -->
<!-- start detail-lightbox -->
<div class="detail-lightbox" id="lightbox-product-add">
	
	<h2><?php e($title); ?></h2>
	
	<?php if( $error == 'false'): ?>
	
		<div class="imgb">
			<?php echo $image->resize( isset($data['imagem'] ) ? $data['imagem']  : 'uploads/produto_imagem/filename/sem_imagem.jpg', 210, 125); ?>
		</div>
		<!-- start txtb -->
		<div class="txtb">
			<h4><?php e($data['nome']); ?></h4>
			<p>
				<p>
					Referência: <?php e($data['referencia']); ?> <br />
					Unidade de Venda: <?php e($data['unidade_venda']); ?>
				</p>
			</p>
			<span><b>Quantidade:</b> <?php e($data['quantidade']); ?></span>
		</div>
		<!-- end txtb -->
		<div class="clear"></div>
		<!-- start button-block2 -->
		<div class="button-block2">
			<a href="#" title="COMPRAR MAIS PRODUTOS" class="nyroModalClose gary-button" id="btn-fechar-modal">COMPRAR MAIS PRODUTOS</a>
			<a href="<?php echo $this->Html->Url('/carrinho/entrega') ?>" title="FINALIZAR COMPRA" class="button1">FINALIZAR COMPRA</a>
			<div class="clear"></div>
		</div>
		<!-- end button-block2 -->
		
	<?php else: ?>
	
		<!-- start txtb -->
		<div style="display: block; float: left; height: auto;">
			<p>
				Você está tentando adicionar ao carrinho, uma quantidade que atualmente não temos em estoque para esse produto. 
				Por favor, escolha outra quantidade ou solicite para ser avisado quando atualizarmos o estoque.
			</p>
			<!-- start aviseme -->
			<div class="aviseme" style="margin-left: 150px; margin-top: 7px;">
				<?php 
						if(isset($produto_id)):
						echo $this->Form->create('Produto', 
											array(	
												"onsubmit"=>"return validaFormAviseMeModal();",
												"class"=>"form-aviseme",
												"url" => $this->Html->url('/produtos/detalhe/'.$produto_id,true)
												)
											); ?>							
					<?php if(!isset($enviado)){ ?>
						<!-- start linha -->
						<div class="linha">
							<label>Nome*</label>
							<input id="nome" class="input input7 " type="text" maxlength="255" name="data[Avise][nome]"/>
							<span id="nome-msg-error" class="aviseme-msg-error"></span>
						</div>
						<!-- end linha -->
						<!-- start linha -->
						<div class="linha">
							<label>Email*</label>
							<input id="email" class="input input7 " type="text" maxlength="255" name="data[Avise][email]"/>
							<span id="email-msg-error" class="aviseme-msg-error"></span>
						</div>
						<!-- end linha -->
						<?php echo $this->Form->hidden("Avise.produto_id", array("value"=>$produto_id)); ?>
						<div class="clear"></div>
						<p>* campos obrigatórios.</p>
						<input type="submit" name="enviaravise" value="Enviar" class="link button left" style="margin-top: 3px;" />
						
						
					<?php } ?>
				<?php 
						echo $this->Form->end();
						endIf;
				?>
			</div>
			<!-- end aviseme -->
		</div>
		<!-- end txtb -->
		<a href="javascript:void(0);" title="CONTINUAR COMPRANDO" class="nyroModalClose gary-button" id="btn-fechar-modal" style="float: right; margin-top: -10px; margin-right: 15px">CONTINUAR COMPRANDO</a>
		
	<?php endIf; ?>
</div>
<!-- end detail-lightbox -->
<!-- end lightbox-product-add -->