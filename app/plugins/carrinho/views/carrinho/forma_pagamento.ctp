<?php $javascript->link("common/jquery.meio_mask.js", false); ?>
<?php $javascript->link("/carrinho/js/forma_pagamento.js", false); ?>

<!-- start header -->
<div id="header">
    <!-- start logo -->
    <div id="logo">
        <a href="<?php e($this->Html->Url('/')) ?>" title="Fuel Tech">
            <?php echo $this->Html->image('site/logo.png', array('alt' => 'Fuel Tech', 'width' => '219', 'height' => '53')) ?>
        </a>
    </div>
    <!-- end logo -->
    <!-- start header mid -->
    <div id="header-mid">
        &nbsp;
    </div>
    <!-- end header mid -->
    <!-- start header right -->
    <div id="header-right">
        <!-- start language -->
        <div class="language">
            <ul>
                <li><span><?php echo __("VERSION"); ?>: </span></li>
                <?php
                if (isset($menu_idiomas) && count($menu_idiomas) > 0):
                    $first = true;
                    foreach ($menu_idiomas as $idioma):
                        if (Configure::read('Config.language') == $idioma['Linguagem']['codigo']) {
                            $class = "class='active'";
                        } else {
                            $class = "";
                        }
                        ?>		
                        <?php if (!$first): ?>
                            <li>&nbsp; | &nbsp;</li>
                            <?php endIf; ?>
                        <li <?php echo $class ?>>
                            <?php if ($idioma['Linguagem']['externo'] == 0): ?>
                                <a href="<?php echo $this->Html->Url('/') . $idioma['Linguagem']['codigo']; ?>" title="<?php echo $idioma['Linguagem']['nome']; ?>">
                                    <?php //echo $image->resize(isset($idioma['Linguagem']['thumb_filename']) ? $idioma['Linguagem']['thumb_dir'] . '/' . $idioma['Linguagem']['thumb_filename'] : 'uploads/produto_imagem/filename/sem_imagem.jpg', 12, 8, true, array('alt' => $idioma['Linguagem']['nome'])); ?>
                                    <?php echo $idioma['Linguagem']['nome']; ?>
                                </a>
                            <?php else: ?>
                                <a href="<?php echo $idioma['Linguagem']['url']; ?>" title="<?php echo $idioma['Linguagem']['nome']; ?>">
                                    <?php //echo $image->resize(isset($idioma['Linguagem']['thumb_filename']) ? $idioma['Linguagem']['thumb_dir'] . '/' . $idioma['Linguagem']['thumb_filename'] : 'uploads/produto_imagem/filename/sem_imagem.jpg', 12, 8, true, array('alt' => $idioma['Linguagem']['nome'])); ?>
                                    <?php echo $idioma['Linguagem']['nome']; ?>
                                </a>
                            <?php endIf; ?>
                        </li>
                        <?php
                        if ($first) {
                            $first = false;
                        }
                    endForeach;
                endIf;
                ?>
            </ul>
        </div>
        <!-- end language -->
        <div class="clear"></div>
    </div>
    <!-- end header right -->
    <!-- start numerical -->
    <div id="numerical">
        <ul>
            <li><a href="<?php echo $this->Html->url("/carrinho") ?>" title="CARRINHO" class="visited"><span>01</span><strong>CARRINHO</strong></a></li>
            <li><a href="<?php echo $this->Html->url("/usuario/login") ?>" title="Identificação" class="visited"><span>02</span><strong>Identificação</strong></a></li>
            <li><a href="<?php echo $this->Html->url("/carrinho/entrega") ?>" title="Entrega" class="visited"><span>03</span><strong>Entrega</strong></a></li>
            <li><a href="javascript:void(0);" title="Pagamento" class="active"><span>04</span><strong>Pagamento<b>Você está aqui</b></strong></a></li>
            <li class="last"><a href="javascript:void(0);" title="Confirmação"><span>05</span><strong>Confirmação</strong></a></li>
        </ul>
    </div>
    <!-- end numerical -->
    <div class="clear"></div>
</div>
<!-- end header -->

<!-- start container -->
<div id="container">

    <h2 class="page-title fnt-size second">FORMA DE PAGAMENTO</h2>

    <?php echo $this->Form->create('Pedido', array('id' => 'final', 'url' => '/carrinho/carrinho/forma_pagamento')); ?>

    <!-- start col4 -->
    <div class="col4">
        <h3>ESCOLHA O FRETE <?php echo $this->Html->image('/img/site/zoomloader.gif', array('class' => 'loading', 'style' => 'visibility:hidden', 'alt' => 'Carregando', 'title' => 'Carregando')); ?></h3>
        <!-- start column6 -->
        <div class="column6">
            <!-- start column1-box -->
            <div class="column1-box">
                <!-- start row -->
                <div class="row" id="fretes-disponiveis">
                    <?php
                    foreach ($frete_tipos as $chave => $frete) {
                        $frete['Frete'][0]['preco'] = $this->String->bcoToMoeda($frete['Frete'][0]['preco']);
                        if ($frete['FreteTipo']['nome'] == 'JUNDIAI' || $frete['FreteTipo']['nome'] == 'RAMOS') {
                            $frete['FreteTipo']['nome'] = 'TRANSPORTADORA';
                        }
                        echo "<label class='radio1'>" . $form->input('Pedido.frete_id', array('div' => false, 'label' => false, 'class' => 'tipo-entrega', 'type' => 'radio', 'name' => 'data[Pedido][frete_id]', 'options' => array($frete['Frete'][0]['id'] => "<strong>{$frete['FreteTipo']['nome']}</strong>  <span style='float: left; line-height: normal; margin: -4px 0 0 18px; width: 170px;'> Prazo de entrega em até {$frete['Frete'][0]['prazo']} dias úteis: R$ {$frete['Frete'][0]['preco']}</span>"))) . "</label>";
                    }
                    ?>
                    <div class="clear"></div>
                </div>
                <!-- end row -->
                <!-- start row2 -->
                <div class="row2">
                    <span class="line1"><b>Total em Produtos(<?php echo count($dados); ?> item) :</b> R$ <?php echo $totais['sub_total']; ?></span>
                    <span class="line1"><b>Subtotal :</b> R$ <?php echo $totais['sub_total']; ?></span>
                    <span class="line1"><b>Frete para <?php echo $endereco['UsuarioEndereco']['cidade']; ?>:</b>	
                        R$ 
                        <span id="frete" style="<?php if (isset($totais['frete_com_desconto'])) {
                        echo "text-decoration:line-through";
                    } ?>">
                        <?php echo $totais['frete']; ?>
                        </span>
                    </span>
                </div>
                <!-- end row2 -->
                <!-- start row2 -->
                <div class="row2 gap">
                <?php if (isset($totais['frete_com_desconto'])): ?>
                        <span class="line1 gap2" id="frete-com-desconto"><b>Cupom de Desconto</b>R$ <?php echo $totais['frete_com_desconto']; ?></span>
                <?php endif; ?>
                </div>
                <!-- end row2 -->
                <?php if (isset($cupom)): ?>
                    <!-- start row2 -->
                    <div class="row2 gap">					
                        <span class="line1 gap2"><b>Cupom de Desconto <a href="<?php echo $this->Html->url("/carrinho/remove_cupom/") ?>">Remover cupon</a></b> 
                            R$ <?php echo (isset($totais['cupom_total_desconto']) && $totais['cupom_total_desconto'] > 0) ? $totais['cupom_total_desconto'] : ''; ?>
                        </span>
                    </div>
                    <!-- end row2 -->
                <?php endIf; ?>
                <div class="clear"></div>
                <span class="total">TOTAL: <span id="total"> R$ <?php echo ( isset($totais['total_com_desconto']) && $totais['total_com_desconto'] > 0 ) ? $totais['total_com_desconto'] : $totais['total']; ?></span> </span>
            </div>
            <!-- end column1-box -->
        </div>
        <!-- end column6 -->
    </div>
    <!-- end col4 -->

    <!-- start col4 -->
    <div class="col4">
        <h3>ENDEREÇO DE ENTREGA <?php echo $this->Html->image('/img/site/zoomloader.gif', array('class' => 'loading', 'style' => 'visibility:hidden', 'alt' => 'Carregando', 'title' => 'Carregando')); ?></h3>
        <!-- start column6 -->
        <div class="column6 gap">
            <!-- start column1-box -->
            <div class="column1-box">
                <address>
                    <strong class="new"><?php echo $endereco['UsuarioEndereco']['nome']; ?></strong>
                    <strong><?php echo $endereco['UsuarioEndereco']['rua']; ?>, <?php echo $endereco['UsuarioEndereco']['numero']; ?></strong>
                    <?php echo $endereco['UsuarioEndereco']['bairro']; ?><br />
                    <?php echo $endereco['UsuarioEndereco']['cidade']; ?> <?php echo $endereco['UsuarioEndereco']['uf']; ?> - <?php echo $endereco['UsuarioEndereco']['cep']; ?>
                </address>
                <div class="clear"></div>
                <a href="<?php echo $this->Html->url("/carrinho/entrega/") ?>" title="ALTERAR ENDEREÇO" class="button1 gray">ALTERAR ENDEREÇO DE ENTREGA</a>
            </div>
            <!-- end column1-box -->
        </div>
        <!-- end column6 -->
    </div>
    <!-- end col4 -->

    <div class="clear"></div>

    <!-- start col5 -->
    <div class="col5">
        <h3>
            ESCOLHA A FORMA DE PAGAMENTO
            <?php echo $this->Html->image('/img/site/zoomloader.gif', array('class' => 'loading', 'style' => 'visibility:hidden', 'alt' => 'Carregando', 'title' => 'Carregando')); ?>
        </h3>
        <!-- start column7 -->
        <div class="column7">
            <div class="column1-box">
                <?php foreach ($pagamento_tipo as $v): ?>
                <!-- start paypalcol -->
                <div class="paypalcol">
                    <div class="imgb">
                        <img alt="<?php echo $v['PagamentoTipo']['nome']; ?>" src="<?php echo $this->Html->Url('/img/site/') ?>img_pp<?php echo $v['PagamentoTipo']['id']; ?>.png">
                    </div>
                    <strong class="fs12"><?php echo $v['PagamentoTipo']['nome']; ?></strong>
                    <label for="PedidoTipoPagamentoId<?php echo $v['PagamentoTipo']['codigo']; ?>" class="radio-pgto button1 link link3 center">
                    <?php echo $this->Form->radio('Pedido.tipo_pagamento_id', array($v['PagamentoTipo']['codigo'] => ""), array('label' => false, 'class' => 'tipo-de-pagamento', 'rel' => $v['PagamentoTipo']['codigo'], 'default' => 1, 'hiddenField' => false, 'legend' => false)); ?>selecionar
                    </label>
                </div>
                <!-- end paypalcol -->
                <?php endForeach; ?>
            </div>
            <div class="clear"></div>
        </div>
        <!-- end column7 -->
        <div class="clear"></div>
    </div>
    <!-- end col5 -->
    
    <!-- start col5 -->
    <div class="col5">
        <!-- start column7 -->
        <div class="column7">
            <?php
            // print_r($pagamento_tipo);
            foreach ($pagamento_tipo as $k => $v):
                ?>
                <!-- start forms-pagamentos -->
                <div style="display:none" class="forms-pagamentos <?php echo $v['PagamentoTipo']['codigo']; ?>">
                    <!-- start grayblock -->
                    <div class="grayblock third">
                        <!-- start column1-box -->
                        <div class="column1-box">
                            <h3>
                                <?php echo $v['PagamentoTipo']['nome']; ?>
                                <?php echo $this->Html->image('/img/site/zoomloader.gif', array('class' => 'loading', 'style' => 'visibility:hidden', 'alt' => 'Carregando', 'title' => 'Carregando')); ?>
                            </h3>
                            <?php if ($v['PagamentoTipo']['codigo'] == 'CARTAO') { ?>
                            <div class="whiteblock">
                                <!-- start payment type -->
                                <div class="payment-type">
                                    <div style="margin-left: 20px;">
                                        <?php echo $form->error('condicao_pagamento_id', 'Selecione uma forma de pagamento'); ?>
                                    </div>
                                    <ul>
                                        <?php
                                        foreach ($v['PagamentoCondicao'] as $valor):
                                            if (isset($valor['PagamentoCondicaoImagem'][0])) {
                                                $imagem = $valor['PagamentoCondicaoImagem'][0]['dir'] . DS . $valor['PagamentoCondicaoImagem'][0]['filename'];
                                                $lbl = '<img src="' . $this->Html->url('/' . str_replace('\\', '/', $imagem)) . '" alt="' . $valor['nome'] . '" title="' . $valor['nome'] . '" />';
                                            } else {
                                                $lbl = $valor['nome'];
                                            }
                                            ?>
                                            <li>
                                                <div class="visaradio">
                                                    <label class="first radio1" for="PedidoCondicaoPagamentoId<?php echo $valor['id']; ?>"> 
                                                        <span><?php echo $lbl; ?></span>
                                                        <?php echo $this->Form->radio('Pedido.condicao_pagamento_id', array($valor['id'] => ""), array('label' => false, 'class' => 'tipo_pagamento radio', 'rel' => $valor['nome'], 'default' => 1, 'hiddenField' => false, 'legend' => false)); ?>
                                                    </label>
                                                </div>
                                            </li>
                                        <?php endforeach; ?>
                                        <div class="clear"></div>
                                    </ul>
                                    <div class="clear"></div>
                                    <!-- start row -->
                                    <div class="row4" style="margin-top: 20px;">
                                        <div class="grid_4">
                                            <div class="inputfield">
                                                <label>Parcelar em:</label> 
                                                <?php echo $this->Form->input("Pedido.parcelas", array('id' => 'PedidoParcelasCartao', 'class' => 'input2 input4 select1 PedidoParcelas', 'div' => false, 'label' => false, 'options' => array('' => 'SELECIONE') + $parcelas)); ?>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <!-- end row -->
                                    <!-- start row -->
                                    <div class="row4">
                                        <div class="grid_4">
                                            <div class="inputfield">
                                                <?php echo $this->Form->input("Pedido.nome_portador_cartao", array('div' => false, 'class' => 'input2 input4 PedidoPortadorCartao', 'autocomplete' => 'off', 'label' => 'Nome do titular (como gravado no cartão):')); ?>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <!-- end row -->
                                    <!-- start row -->
                                    <div class="row4">
                                        <div class="grid_4">
                                            <div class="inputfield">
                                                <?php echo $this->Form->input("Pedido.numero_cartao", array('div' => false, 'class' => 'mask-numerico input2 input4 PedidoPortadorCartao', 'label' => 'Número do Cartão (somente números):', 'autocomplete' => 'off')); ?>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <!-- end row -->  
                                    <!-- start row -->
                                    <div class="row4">
                                        <div class="grid_4">
                                            <?php
                                            $meses = array('01' => '1', '02' => '2', '03' => '3', '04' => '4', '05' => '5', '06' => '6', '07' => '7', '08' => '8', '09' => '9', '10' => '10', '11' => '11', '12' => '12');
                                            $anos = array();
                                            for ($i = date('Y'); $i <= date('Y') + 10; $i++) {
                                                $anos[$i] = $i;
                                            }
                                            ?>
                                            <span style="display: block; float: left;">
                                                <div class="inputfield">
                                                    <?php echo $this->Form->input("Pedido.validade_cartao_mes", array('options' => $meses, 'div' => false, 'class' => 'input2 input4 select1', 'label' => 'Data de Validade:', 'autocomplete' => 'off')); ?> 
                                                </div>
                                            </span>
                                            <span style="height: 25px; font-size: 15px; display: block; float: left; line-height: 20px; margin-left: 10px; width: 15px; margin-right: 5px;">
                                                &nbsp;/&nbsp;
                                            </span>
                                            <span style="width: 150px; display: block; float: left;">
                                                <div class="inputfield">
                                        <?php echo $this->Form->input("Pedido.validade_cartao_ano", array('options' => $anos, 'div' => false, 'class' => 'input2 input4 select1', 'label' => false, 'autocomplete' => 'off')); ?>
                                                </div>
                                            </span>    
                                            <div class="clear"></div>
                                        </div>
                                        <?php echo $form->error('validade_cartao'); ?> 
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <!-- end row -->
                                <!-- start row -->
                                <div class="row4">
                                    <div class="grid_4">
                                        <div class="inputfield">
                                        <?php echo $this->Form->input("Pedido.codigo_seguranca_cartao", array('div' => false, 'class' => 'mask-numerico input2 input4', 'autocomplete' => 'off', 'label' => 'Código de Segurança:')); ?>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <!-- end row -->
                            </div>
                            <!-- end payment type -->
                            <input name="" type="submit" value="FINALIZAR" title="FINALIZAR" class="button2 button8 right btn-finalizar-compra" id="btn-finalizar-compra2 btn-finalizar-compra" />
                            <?php echo $this->Html->image('/img/site/gif_carregando.gif', array('class' => 'gif_carregando', 'style' => 'display:none; float: right; margin-top: -20px;', 'alt' => 'Carregando', 'title' => 'Carregando')); ?>
                            <div class="clear"></div>
                        <?php }elseif($v['PagamentoTipo']['codigo'] == 'BOLETO'){ ?>
                            <?php $condition = reset($v['PagamentoCondicao']) ?>
                            <input type="radio" value="<?php echo $condition['id'] ?>" name="data[Pedido][condicao_pagamento_id]" style="display:none" class="tipo_pagamento"/>
                        <div class="whiteblock">
                                <!-- start payment type -->
                                <div class="payment-type">
                                    <div class="row4" style="float: left; width: 580px;">
                                        <p>Esta opção de pagamento não permite parcelamento. Ao fechar um pedido, o sistema gerará um boleto bancário na sua tela. Imprima o documento e pague em qualquer banco ou pelo Internet Bank.</p>
                                        <p>A data de vencimento do boleto é sempre 5 (cinco) dias após o fechamento do pedido. Após esta data, o documento perde a validade e será necessário realizar a compra novamente. A confirmação de pagamento é feita automaticamente pelos bancos em até 3 (três) dias úteis, sendo assim, não é preciso enviar-nos qualquer notificação.</p>
                                        <br />
                                        <p>O boleto gerado após a confirmação de sua compra deve ser:</p>
                                        <p>Impresso e pago na agência bancária de sua preferência;</p>
                                        <p>Pago pela Internet, utilizando a sequência numérica do código de barras (contido na parte superior esquerda do boleto).</p>
                                    </div>
                                    <div style="float: right; width: 250px;">
			                            <input name="submit" type="submit" value="FINALIZAR COMPRA" class="button2 button8 right btn-finalizar-compra" style="margin-top: 10px;" id="btn-finalizar-compra" />
			                            <?php echo $this->Html->image('/img/site/gif_carregando.gif', array('class' => 'gif_carregando', 'style' => 'display:none; float: right; margin-top: -20px; margin-right: -8px;', 'alt' => 'Carregando', 'title' => 'Carregando')); ?>
                                    </div>
                                </div>
                                <!-- end payment type -->
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div> 
                        <?php }elseif($v['PagamentoTipo']['codigo'] == 'BCASH'){ ?>
                            <?php $condition = reset($v['PagamentoCondicao']) ?>
                            <input type="radio" value="<?php echo $condition['id'] ?>" name="data[Pedido][condicao_pagamento_id]" style="display:none" class="tipo_pagamento"/>
                        <div class="whiteblock">
                                <!-- start payment type -->
                                <div class="payment-type">
                                    <div class="row4" style="float: left; width: 580px;">
                                        <p>É possível fazer o pagamento pelos cartões Visa, Elo, Diners Club, American Express, Aura, Hipercard e MasterCard em até 2x com acréscimo de juros. Para garantir sua segurança a transação é toda feita em ambiente bCash.</p>
                                        <br />
                                        <p>Atenção: essa opção é valida somente para clientes residentes no Brasil.</p>
                                        <p>Você será redirecionado para o sistema de pagamento.</p>
                                    </div>
                                    <div style="float: right; width: 250px;">
		            	                <input name="submit" type="submit" value="FINALIZAR COMPRA" class="button2 button8 right btn-finalizar-compra" style="margin-top: 10px;" id="btn-finalizar-compra" />
        			                    <?php echo $this->Html->image('/img/site/gif_carregando.gif', array('class' => 'gif_carregando', 'style' => 'display:none; float: right; margin-top: -20px; margin-right: -8px;', 'alt' => 'Carregando', 'title' => 'Carregando')); ?>
                                    </div>
                                </div>
                                <!-- end payment type -->
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div> 
                        <?php } ?>
                    </div>
                    <!-- end grayblock -->
                    <div class="clear"></div>
                </div>
                <!-- end forms-pagamentos -->
            </div>
            <!-- end column1-box -->
            <div class="clear"></div>
            <?php endforeach ?>
        </div>
        <!-- end col5 -->
    </div>
</div>
        
<?php echo $this->Form->end(); ?>
<!-- end container -->