<h3>Fretes Disponíveis:</h3>
<?php 
if(is_array($frete_disponiveis)):
	foreach($frete_disponiveis as $frete){
		$frete['Frete'][0]['preco'] = $this->String->bcoToMoeda($frete['Frete'][0]['preco']);
		if($frete['FreteTipo']['nome']=='JUNDIAI'||$frete['FreteTipo']['nome']=='RAMOS'){
			$frete['FreteTipo']['nome'] = 'TRANSPORTADORA';
		}
		echo "<p><strong>{$frete['FreteTipo']['nome']}</strong> - Prazo de entrega em até {$frete['Frete'][0]['prazo']} dias úteis após a postagem: <strong>R$ {$frete['Frete'][0]['preco']}</strong></p>";
	}
endIf;
?>