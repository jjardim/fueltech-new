<div class="acl_actions form">
    <h2>Adicionar Ação</h2>
    <?php echo $form->create('Aco', array('url' => array('controller' => 'acl_actions', 'action' => 'add'))); ?>
        <fieldset>
        <?php
            echo $form->input('parent_id', array(
                'options' => $acos,
                'label' => 'Ação Pai',
                'empty' => true,
                'rel' => __('Escolha a ação pai, deixe vazio para default.', true),
            ));
            echo $form->input('alias', array());
        ?>
        </fieldset>
    <?php echo $form->end('Enviar'); ?>
</div>