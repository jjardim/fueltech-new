<?php
    $html->script('/acl/js/acl_permissions.js', false);
    $html->scriptBlock("$(document).ready(function(){ AclPermissions.documentReady(); });", array('inline' => false));
?>
<div class="acos ">
    <h2>Ações</h2>

    <div class="actions">
        <ul>
            <li><?php echo $html->link(__('Adicionar Ação', true), array('action'=>'add')); ?></li>
            <li><?php echo $html->link(__('Gerar Ações', true), array('action'=>'generate')); ?></li>
        </ul>
    </div>

    <table cellpadding="0" cellspacing="0">
    <?php
        $tableHeaders =  $html->tableHeaders(array(
            __('Id', true),
            __('Alias', true),
            __('Ações', true),
        ));
        echo $tableHeaders;

        $currentController = '';
        foreach ($acos AS $id => $alias) {
            $class = '';
            if(substr($alias, 0, 1) == '_') {
                $level = 1;
                $class .= 'level-'.$level;
                $oddOptions = array('class' => 'hidden controller-'.$currentController);
                $evenOptions = array('class' => 'hidden controller-'.$currentController);
                $alias = substr_replace($alias, '', 0, 1);
            } else {
                $level = 0;
                $class .= ' controller expand';
                $oddOptions = array();
                $evenOptions = array();
                $currentController = $alias;
            }

            $actions  = $html->link(__('Editar', true), array('action' => 'edit', $id));
            $actions .= ' ' . $html->link(__('Deletar', true), array(
                'action' => 'delete',
                $id,
            ), null, __('Você tem certeza?', true));
            $actions .= ' ' . $html->link(__('Mover para cima', true), array('action' => 'move', $id, 'up'));
            $actions .= ' ' . $html->link(__('Mover para baixo', true), array('action' => 'move', $id, 'down'));

            $row = array(
                $id,
                $html->div($class, $alias),
                $actions,
            );

            echo $html->tableCells(array($row), $oddOptions, $evenOptions);
        }
        echo $tableHeaders;
    ?>
    </table>
</div>