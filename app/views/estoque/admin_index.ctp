<div class="index">
	<h2>Estoque</h2>
	
    <?php echo $form->create('Estoque', array('action' => '/index', 'class' => 'formBusca')); ?>
        <fieldset>
            <div class="left">
				<?php echo $form->input('Filter.filtro', array('div' => false, 'label' => 'Produto / Código:', 'class' => 'estoqueFiltro')); ?>
			</div>
			<div class="submit">
				<input name="submit" type="submit" class="button1" value="Busca" />
			</div>
			<div class="submit">
				<input name="submit" type="submit" class="button1" value="Exportar" />
			</div>
			<div class="submit">
				<a href="<?php e($this->Html->Url('index/filter:clear')) ?>" title="Limpar Filtro" class="button" >Limpar Filtro</a>
			</div>
        </fieldset>
	<?php echo $form->end(); ?>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('sku');?></th>
			<th><?php echo $this->Paginator->sort('nome');?></th>
			<th><?php echo $this->Paginator->sort('quantidade');?></th>
			<th><?php echo $this->Paginator->sort('quantidade_alocada');?></th>
			<th><?php echo $this->Paginator->sort('Quantidade Disponível','quantidade_disponivel');?></th>
			<th><?php echo $this->Paginator->sort('Comprados','quantidade_comprada');?></th>
			<th>Ações</th>
	</tr>
	<?php
	$i = 0;
	//debug($produtos);
	foreach ($produtos as $estoque):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td align="center"><?php echo $estoque['Produto']['sku']; ?>&nbsp;</td>
        <td><?php echo $this->Html->link($estoque['Produto']['nome'], array('controller' => 'produtos', 'action' => 'edit', $estoque['Produto']['id'])); ?>&nbsp;</td>
		<td align="center"><?php echo $estoque['Produto']['quantidade']; ?>&nbsp;</td>
		<td align="center"><?php echo $estoque['Produto']['quantidade_alocada']; ?>&nbsp;</td>
		<td align="center"><?php echo $estoque['Produto']['quantidade_disponivel']; ?>&nbsp;</td>
		<td align="center"><?php echo $estoque[0]['quantidade_comprada']; ?>&nbsp;</td>
		<td align="center"><?php echo $this->Html->link('Ver Pedidos', array('controller' => 'pedidos', 'Pedido.produto_id' => $estoque['Produto']['id'])); ?></td>
	</tr>
<?php endforeach; ?>
	</table>
	 <p><?php echo $this->Paginator->counter(array('format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true))); ?></p>
           

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('Próximo', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>