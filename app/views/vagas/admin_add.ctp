<?php
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('admin/paginas/crud.js',false);
?>
<div class="index">
<?php echo $this->Form->create('Vaga');?>
	<fieldset>
 		<legend><?php printf(__('Adicionar %s', true), __('Vaga', true)); ?></legend>
	<?php
		echo $this->Form->input('status', array('default'=>true,'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
        echo $this->Form->input('tipo', array('class'=>'w312'));
		echo $this->Form->input('tipo', array( 'options' => array(' ' => 'Selecione', 'LOJAS'=> 'Lojas','LOGÍSTICA'=> 'Logística','TELEVENDAS'=>'Televendas')));
        echo $this->Form->input('cargo',array('class'=>'w312'));
		echo $this->Form->input('localidade',array('class'=>'w312'));
		echo $this->Form->input('resumo',array('type'=> 'textarea', 'class'=>'w312'));
		echo $this->Form->input('requisitos',array('class'=>'w312'));
		echo $this->Form->end(__('Inserir', true));
	?>
	</fieldset>
</div>