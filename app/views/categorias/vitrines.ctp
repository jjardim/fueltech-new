<?php
$javascript->link('site/categorias/index', false);
?>
<div id="rightcol_hp">
    <!-- start breadcrumb_box -->
    <div class="breadcrumb_box">
        <ul>
        <li><span><?php echo __('Você está em',true) ?></span></li>
            <?php
            $total = count($breadcrumbs);
            $i     = 1;
            foreach( $breadcrumbs AS $categoria ){
                if( $i < $total ){
                ?>
                    <li><a href="<?php echo $this->Tree->url($categoria['Categoria'])?>"><?php echo $categoria['Categoria']['nome']?></a></li>
                    <li>&gt;</li>
            <?php
                }else{
            ?>
                    <li><a href="<?php echo $this->Tree->url($categoria['Categoria'])?>"><?php echo $categoria['Categoria']['nome']?></a></li>
            <?php
                }

                $i++;
            }
            ?>
        </ul>
    </div>
    <!-- end  breadcrumb_box -->
    <!-- start pro box1 -->
    <div id="pro_box1">
        <h2 class="red"><?php echo $categoria_nome['Categoria']['nome']?></h2>
        <div id="tab_box_nav">
            <ul>
                <?php
                foreach ($vitrines['vitrines'] as $indice => $valor):
                ?>
                    <li><a href="javascript:;" class="titulo-vitrine <?php if( $indice == 0 ){?>active<?php }?>" rel="<?php echo $indice?>"><?php echo $valor['Vitrine']['nome'];?></a></li>
                <?php
                endForeach;
                ?>
            </ul>
        </div>
        <div class="clear"></div>
        <?php
        foreach ($vitrines['vitrines'] as $j => $valor):
            $cont = 1;
        ?>
            <div class="content_vitrine vitrine<?php echo $j?>">
        <?php
            foreach ($valor['Produto'] as $produto):
                $class = ( $cont % 3 == 0 ) ? 'none' : '';
        ?>
                <!-- start inner box -->
                <div class="tab_box_common <?php echo $class?>">
                    <label><a href="<?php e($this->Html->Url("/" . low(Inflector::slug($produto['nome'], '-')) . '-' . 'prod-' . $produto['id'])) ?>.html"><?php echo $image->resize(isset($produto['ProdutoImagem'][0]['filename']) ? $produto['ProdutoImagem'][0]['dir'] . '/' . $produto['ProdutoImagem'][0]['filename'] : "uploads/produto_imagem/filename/sem_imagem.jpg", 125, 147); ?></a></label>
                    <p><span><a href="<?php e($this->Html->Url("/" . low(Inflector::slug($produto['nome'], '-')) . '-' . 'prod-' . $produto['id'])) ?>.html">Câmera Digital</a></span></p>
                   <p>
                        <?php
                        $preco = ($produto['preco_promocao'] > 0 && $produto['preco_promocao'] < $produto['preco']) ? $produto['preco_promocao'] : $produto['preco'];
                        $parcela_valida = $this->Parcelamento->parcelaValida($produto['preco_promocao'], $parcelamento);
                        if ($produto['preco_promocao'] > 0 && $produto['preco_promocao'] < $produto['preco']) {
                        ?>
                            <span class="blue">de: R$<?php echo $this->String->bcoToMoeda($produto['preco']) ?> </span>
                        <?php
                        }
                        ?>

                        <span class="red">por: R$<?php echo $this->String->bcoToMoeda($preco) ?></span>
                        <?php if( $parcela_valida['parcela'] > 1 ):?>
                            <span>ou em <?php e($parcela_valida['parcela']) ?> X de R$<?php echo $this->String->bcoToMoeda($parcela_valida['valor'])?></span>
                        <?php endIf;?>
                    </p>
                </div>
                <!-- end inner box -->
        <?php
                $cont++;
            endForeach;
        ?>
            </div>
        <?php
        endForeach;
        ?>
        <div class="clear"></div>
    </div>
    <!-- end pro box1 -->
	<?php /**/?>
    <!-- start new tab box -->
    <div id="new_tab_box">
        <?php
        $cont_vitrine = 1;
        foreach( $dados AS $categoria ){
            if( !empty($categoria['Produto']) ):
        ?>
            <!-- new tab comm1 -->
            <div class="new_tab_common">
                <div class="tab_box_nav"><span class="active"><a href="<?php echo $this->Tree->url($categoria['Categoria'])?>" title="<?php echo htmlentities($categoria['Categoria']['nome'], ENT_QUOTES, 'utf-8')?>"><?php echo $categoria['Categoria']['nome']?></a></span></div>
                <?php
                foreach($categoria['Produto'] AS $indice => $produto):
                ?>
                    <?php
                    if( $indice == 0 ){
                    ?>
                        <div class="tab_detail_box">
                            <a href="<?php e($this->Html->Url("/" . low(Inflector::slug($produto['nome'], '-')) . '-' . 'prod-' . $produto['id'])) ?>.html"><?php echo $image->resize(isset($produto['ProdutoImagem'][0]['filename']) ? $produto['ProdutoImagem'][0]['dir'] . '/' . $produto['ProdutoImagem'][0]['filename'] : "uploads/produto_imagem/filename/sem_imagem.jpg", 128, 90); ?></a>
                            <p><span><a href="<?php e($this->Html->Url("/" . low(Inflector::slug($produto['nome'], '-')) . '-' . 'prod-' . $produto['id'])) ?>.html"><?php echo $produto['nome'] ?></a></span></p>
                            <p>
                        <?php
                        $preco = ($produto['preco_promocao'] > 0) ? $produto['preco_promocao'] : $produto['preco'];
                        $parcela_valida = $this->Parcelamento->parcelaValida($produto['preco_promocao'], $parcelamento);
                        if ($produto['preco_promocao'] > 0) {
                        ?>
                            <span class="blue">de: R$<?php echo $this->String->bcoToMoeda($produto['preco']) ?> </span>
                        <?php
                        }
                        ?>

                        <span class="red">por: R$<?php echo $this->String->bcoToMoeda($preco) ?></span>
                        <?php if( $parcela_valida['parcela'] > 1 ):?>
                            <span>ou em <?php e($parcela_valida['parcela']) ?> X de R$<?php echo $this->String->bcoToMoeda($parcela_valida['valor'])?></span>
                        <?php endIf;?>
                    </p>
                        </div>
                    <?php
                    }
                endForeach;
                ?>
                    <a href="<?php echo $this->Tree->url($categoria['Categoria'])?>" title="<?php echo htmlentities($categoria['Categoria']['nome'], ENT_QUOTES, 'utf-8')?>" class="red"><?php echo __('ver todos',true) ?></a>
            </div>
            <!-- new tab comm1 -->
            <?php
            if( $cont_vitrine % 2 == 0 ){
            ?>
                <div class="clear"></div>
        <?php
            }

            $cont_vitrine++;
            endIf;

        }
        ?>
    </div>
    <!-- end new tab box -->
	<?php /**/?>
</div>
