<?php if(isset($filtros_selecionados)): ?> 
	<!-- start filter -->
	<div class="filter" style="padding: 15px 0px 0px 0px; margin: 0px 0px -10px 0px">
		<div id="filtros-selecionados">
			<h4>Filtros Selecionados</h4>
			<?php foreach($filtros_selecionados as $tipo_filtro):?>
			<a class="gray filtros_selecionados" href="javascript:void(0);" rel="<?php echo $tipo_filtro['id_filtro']; ?>" ><strong>X</strong> <span><?php echo $tipo_filtro['nome_filtro']; ?> : <?php echo $tipo_filtro['valor_filtro']; ?></span></a>
			<?php endforeach;?>
			<div class="clear"></div>
		</div>
	</div>
	<!-- end filter -->
<?php endIf;?>
	
<?php if($menu_filtro): ?>

	<!-- start filter -->
	<div class="filter">
	
		<h3>Filtrar por:</h3>
		<!-- filter search -->
		<form action="#" class="filter-search" style="display: none;">
			<input type="text" value="Marca" name="field" class="input" onfocus="if(this.value == 'Marca') { this.value = ''; }" onblur="if(this.value == '') { this.value = 'Marca'; }" />
			<input type="submit" value="" name="button" class="button" />
		</form>
			
	</div>
	<!-- end filter -->
		
	<?php foreach($menu_filtro as $tipo_filtro):?>
	
		<?php 
			if(isset($tipo_filtro['nome_filtro']) && $tipo_filtro['nome_filtro'] == 'Tamanho'):
		?>	
			<!-- start size bar -->
			<div class="size-bar">
				<h3><?php echo $this->String->title_case($tipo_filtro['nome_filtro']); ?></h3>
				<ul class="atributos-tamanhos">
				<?php foreach($tipo_filtro['valor_filtro'] as $filtro):?>
					<li>
						<!--<a href="javascript:void(0);" rel="<?php echo $filtro['id']; ?>" title="<?php echo $this->String->title_case($filtro['valor']); ?>">
							<?php //echo $this->String->title_case($filtro['valor']); ?>
						</a>-->
						<input type="checkbox" class="checkbox" id="<?php echo $filtro['id']; ?>" rel="<?php echo low(Inflector::slug($tipo_filtro['nome_filtro'], '-')); ?>" value="<?php e($filtro['id']); ?>" name="data[Filtro][id]" />
						<label for="<?php echo $filtro['id']; ?>"><?php echo $this->String->title_case($filtro['valor']); ?></label>
					</li>
				<?php endforeach;?>
				</ul>
			</div>
			<!-- end size bar -->
		<?php endIf; ?>	
		
		<?php 
			if(isset($tipo_filtro['nome_filtro']) && $tipo_filtro['nome_filtro'] == 'Cor'):
		?>	
			<!-- start color bar -->
			<div class="color-bar">
				<h3><?php echo $this->String->title_case($tipo_filtro['nome_filtro']); ?></h3>
				<ul class="atributos-cores">
				<?php foreach($tipo_filtro['valor_filtro'] as $filtro):?>
					<li>
						<?php $img = ( isset($filtro['thumb_filename']) ) ? 'uploads/atributo/thumb/' . $filtro['thumb_filename'] : "uploads/atributo/thumb/sem_imagem.jpg"; ?>
						<input type="checkbox" class="checkbox" id="<?php echo $filtro['id']; ?>" rel="<?php echo low(Inflector::slug($tipo_filtro['nome_filtro'], '-')); ?>" value="<?php e($filtro['id']); ?>" name="data[Filtro][id]" />
						<label for="<?php echo $filtro['id']; ?>" style="background-image: url('<?php echo $this->Html->url("/", true).$img; ?>');" title="<?php echo $this->String->title_case($filtro['valor']); ?>"></label>
					</li>
				<?php endforeach;?>
				</ul>
			</div>
			<!-- end color bar -->
		<?php endIf; ?>	
		
	<?php endforeach;?>

<?php endIf;?>	