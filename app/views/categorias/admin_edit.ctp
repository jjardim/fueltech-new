<?php 
echo $javascript->link('common/tree_view/jquery.treeview.js',false);
echo $javascript->link('common/jquery-ui-1.8.16.custom.min',false);
echo $this->Html->css('common/ui-lightness/jquery-ui-1.8.16.custom.css');
echo $javascript->link('admin/categorias/index.js',false);
?>
<div class="index">
<?php
echo $form->create('Categoria',array('type' => 'file', 'action'=>'edit/'.$this->params['pass'][0]));
?>
      <fieldset>
        <legend><?php __('Editar Categoria'); ?></legend>
		<?php 
			echo $this->Html->link(__('Alterar a ordem dos produtos', true), array('action' => 'order/'.$this->params['pass'][0])); 
			echo $this->Form->input('id');
			echo $this->Form->input('status',array('type'=>'radio','options'=>array(true=>'Ativo',false=>'Inativo')));
			//echo $this->Form->input('destaque_home',array('type'=>'radio','options'=>array(true=>'Sim',false=>'Não')));
			echo $this->Form->input('parent_id',array('label'=>'Categoria Pai'));
			echo $this->Form->input('sort',array('label'=>'Ordem','class'=>'w147'));
			echo $this->Form->input('destaque_menu',array('label'=>'Destaque Menu','type'=>'radio','options'=>array(true=>'Sim',false=>'Não')));
			echo $this->Form->input('ordem_menu',array('label'=>'Ordem Menu','class'=>'w147'));
		?>
		<br /><br />		
		
		<div class="left clear tab" id="detail-tabs" style="margin-top: 15px;">
			<ul>
				<?php foreach($idiomas as $key => $idioma): ?>
					<li>
						<a href="#tab-content-idioma-<?php echo $idioma['Linguagem']['id']; ?>">
							<?php $img = ( isset($idioma['Linguagem']['thumb_filename']) ) ? $idioma['Linguagem']['thumb_dir'] . '/' . $idioma['Linguagem']['thumb_filename'] : "uploads/produto_imagem/thumb/sem_imagem.jpg"; ?>
							<?php echo $image->resize($img, 20, 20); ?>
							<?php echo $idioma['Linguagem']['nome']; ?>
						</a>
					</li>
				<?php endForeach; ?>
			</ul>
			<br /><br />
			<?php foreach($idiomas as $key => $idioma): ?>
				<div id="tab-content-idioma-<?php echo $idioma['Linguagem']['id']; ?>">					
					
					<?php echo $this->Form->input("CategoriaDescricao.".$key.".id"); ?>
					<?php echo $this->Form->input("CategoriaDescricao.".$key.".categoria_id", array('hiddenField'=>false,'type' => 'hidden','value' => $this->params['pass'][0])); ?>
					<?php echo $this->Form->input("CategoriaDescricao.".$key.".language", array('hiddenField'=>false,'type' => 'hidden','value' => $idioma['Linguagem']['codigo'])); ?>
					<?php echo $this->Form->input("CategoriaDescricao.".$key.".nome",array('class'=>'w312')); ?>
					<?php echo $this->Form->input("CategoriaDescricao.".$key.".descricao",array('label'=>'Descrição','class'=>'w312')); ?>
					<br /><br />
					<legend>SEO</legend>
					<div class="left">
						<?php echo $this->Form->input("CategoriaDescricao.".$key.".seo_title",array('label'=>'Seo Title','class'=>'inputs')); ?>
					</div>
					<div class="left clear">
						<?php echo $this->Form->input("CategoriaDescricao.".$key.".seo_institucional",array('label'=>'Seo Institucional','class'=>'inputs')); ?>
					</div>
					<div class="left clear">
						<?php echo $this->Form->input("CategoriaDescricao.".$key.".seo_meta_description",array('label'=>'Seo Meta Description','class'=>'inputs')); ?>
					</div>
					<div class="left clear">
						<?php echo $this->Form->input("CategoriaDescricao.".$key.".seo_meta_keywords",array('label'=>'Seo Meta Keywords','class'=>'inputs')); ?>
					</div>
					
				</div>
			<?php endForeach; ?>
		</div>
			
		<div class="clear"></div>
		<div class="clear"></div>
		
		<legend>Thumb</legend>
		<?php
			$img = ( isset($this->data['Categoria']['thumb_filename']) ) ? $this->data['Categoria']['thumb_dir'] . '/' . $this->data['Categoria']['thumb_filename'] : "uploads/produto_imagem/thumb/sem_imagem.jpg";
			echo $this->Form->input('Categoria.thumb_filename', array('type' => 'file'));
			echo $image->resize($img, 80, 80);		
			echo $this->Form->input('Categoria.thumb_dir', array('type' => 'hidden'));
			echo $this->Form->input('Categoria.thumb_mimetype', array('type' => 'hidden'));
			echo $this->Form->input('Categoria.thumb_filesize', array('type' => 'hidden'));
			
			if( isset($this->data['Categoria']['thumb_filename']) ){
				echo $form->input('Categoria.thumb_remove', array('type' => 'checkbox')); 
			}
			//echo $this->Form->input('CategoriaAtributo', array( 'options' => $atributos));
		?>
		
		<div class="clear"></div>
		
		<?php
		echo $form->end('Salvar');
		?>
	</fieldset>
</div>
