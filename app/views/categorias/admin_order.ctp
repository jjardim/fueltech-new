<?php
 
    echo $javascript->link('common/jquery-ui-1.8.16.custom.min',false);
    echo $this->Html->css('common/ui-lightness/jquery-ui-1.8.16.custom');

?>

<style>
  #sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
  #sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px; }
  #sortable li span { position: absolute; margin-left: -1.3em; }
</style>

<div class="index">
    <div class="btAddProduto clear">
    <?php echo $this->Html->link(__('[<<] voltar', true), array('action' => 'edit/'.$ddCategoria['id'])); ?>
    </div>
    <?php 
    echo $this->Form->create('Categoria',array('action' => 'order/'.$ddCategoria['id'])); 
    echo $this->Form->input('id',array('value'=>$ddCategoria['id']));
    ?>
    
    <fieldset>
        <legend><?php __('Editar ordem dos produtos da categoria "'.$ddCategoria['nome'].'"'); ?></legend>
        <ul id="sortable">
            <?php
            foreach($produtos as $id => $nome){
                ?>
                <li class="ui-state-default" style="list-style: none;">
                    <span class="ui-icon ui-icon-arrowthick-2-n-s" >
                        <input type="hidden" name="data[Produto][<?=$id?>]" value="<?=$id?>">
                    </span><?=$nome?>
                </li>
                <?php
            }
            ?>
        </ul>    
        <?php
            echo $this->Form->end(__('Salvar', true));
        ?>
    </fieldset>
    </div>

<script>
  $(function() {
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();
  });
</script>
