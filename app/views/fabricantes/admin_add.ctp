<div class="index">
<?php echo $this->Form->create('Fabricante',array('type' => 'file')); ?>
	<fieldset>
		<legend><?php __('Adicionar Fabricante'); ?></legend>
	<?php
		echo $this->Form->input('status',array('type'=>'radio','default'=>true,'options'=>array(true=>'Ativo',false=>'Inativo')));
		echo $this->Form->input('nome',array('class'=>'w312'));
	?>
	</fieldset>
        <fieldset><legend>Imagem</legend>
        <?php
                echo $this->Form->input('FabricanteImagem.filename', array('type' => 'file'));
                echo $this->Form->input('FabricanteImagem.dir', array('type' => 'hidden'));
                echo $this->Form->input('FabricanteImagem.mimetype', array('type' => 'hidden'));
                echo $this->Form->input('FabricanteImagem.filesize', array('type' => 'hidden'));
                echo $this->Form->end(__('Inserir', true));
        ?>
        </fieldset>
</div>
