<div class="index ">
    <h2><?php __('Parceiros'); ?></h2>
	<div class="btAddProduto">
    	<?php echo $this->Html->link(__('[+] Adicionar Parceiro', true), array('action' => 'add')); ?>
    </div>
    <?php echo $form->create('Parceiro', array('class' => 'formBusca', 'url' => '/admin/parceiros/index')); ?>
    <fieldset>
        <div class="left">
            <?php echo $form->input('Filter.nome', array('div' => false, 'label' => 'Nome:', 'class' => 'produtosFiltro')); ?>
        </div>
        <div class="left">
            <?php echo $form->input('Filter.filtro', array('div' => false, 'label' => 'Cidade/Estado:', 'class' => 'produtosFiltro')); ?>
        </div>
        <div class="submit">
            <input name="submit" type="submit" class="button1" value="Busca" />
        </div>
        <div class="submit">
            <input name="submit" type="submit" class="button1" value="Exportar" />
        </div>
        <div class="submit">
            <a href="<?php e($this->Html->Url('index/filter:clear')) ?>" title="Limpar Filtro" class="button" >Limpar Filtro</a>
        </div>
    </fieldset>	
    <?php echo $form->end(); ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('nome'); ?></th>
			<th><?php echo $this->Paginator->sort('cidade'); ?></th>
            <th><?php echo $this->Paginator->sort('localidade_label'); ?></th>           
			<th><?php echo $this->Paginator->sort('status'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>

            <th class="actions"><?php __('Ações'); ?></th>
        </tr>
        <?php
        $i = 0;
        foreach ($parceiros as $parceiro):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
            <tr<?php echo $class; ?>>
                <td align="center"><?php echo $parceiro['Parceiro']['id']; ?>&nbsp;</td>
				<td><?php echo $parceiro['Parceiro']['nome']; ?>&nbsp;</td>
				<td><?php echo $parceiro['Parceiro']['cidade']; ?>&nbsp;</td>
                <td align="center"><?php echo $parceiro['Parceiro']['localidade_label']; ?>&nbsp;</td>                
				<td align="center"><?php echo ( $parceiro['Parceiro']['status'] ) ? "Ativo" : "Inativo"; ?>&nbsp;</td>
				<td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $parceiro['Parceiro']['created']); ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $parceiro['Parceiro']['modified']); ?>&nbsp;</td>
                <td class="actions">
                    <?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $parceiro['Parceiro']['id'])); ?>
					<?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $parceiro['Parceiro']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $parceiro['Parceiro']['id'])); ?>
				</td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, mostrando %current% registros de %count%, começando no registro %start% e terminando no %end%', true)
        ));
        ?>	</p>

    <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('anterior', true), array(), null, array('class' => 'disabled')); ?>
	 | 	<?php echo $this->Paginator->numbers(); ?>
        |
        <?php echo $this->Paginator->next(__('próxima', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
    </div>
    
</div>
