<?php
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('common/jquery-ui-1.8.16.custom.min',false);
echo $this->Html->css('common/ui-lightness/jquery-ui-1.8.16.custom.css');
echo $javascript->link('admin/parceiros/index.js',false);
?>
<div class="index">
    <?php echo $form->create('Parceiro',array('type' => 'file', 'action'=>'edit/'.$this->params['pass'][0])); ?>
    <fieldset>
        <legend><?php printf(__('Editar %s', true), __('Parceiro', true)); ?></legend>
        <div class="left clear">
			<?php echo $this->Form->input('status', array('type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo'))); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('nome', array('class'=>'w312')); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('cidade', array('class'=>'w312')); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('estado',array('div'=>false,'options' => $this->Estados->estadosBrasileiros(),'class'=>'w312')); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('pais', array('class'=>'w312', 'default' => 'Brasil')); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('localidade_label', array('class'=>'w312')); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('descricao_resumida', array('type'=> 'textArea','class'=>'mceEditor wCEM h300 w700')); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('descricao_completa', array('class'=>'mceEditor wCEM h300 w700')); ?>
		<div class="left clear">
			<legend>Thumb</legend>
			<?php
				$img = ( isset($this->data['Parceiro']['thumb_filename']) ) ? $this->data['Parceiro']['thumb_dir'] . '/' . $this->data['Parceiro']['thumb_filename'] : "uploads/produto_imagem/thumb/sem_imagem.jpg";
				echo $this->Form->input('Parceiro.thumb_filename', array('type' => 'file'));
				echo $image->resize($img, 80, 80);
				echo $this->Form->input('Parceiro.thumb_dir', array('type' => 'hidden'));
				echo $this->Form->input('Parceiro.thumb_mimetype', array('type' => 'hidden'));
				echo $this->Form->input('Parceiro.thumb_filesize', array('type' => 'hidden'));

				if( isset($this->data['Parceiro']['thumb_filename']) ){
					echo $form->input('Parceiro.thumb_remove', array('type' => 'checkbox'));
				}
			?>
		</div>
		<div class="clear"></div>
		<?php
			echo $this->Form->end(__('Salvar', true));
        ?>
    </fieldset>
</div>