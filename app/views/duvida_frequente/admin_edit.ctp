<?php
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('common/jquery-ui-1.8.16.custom.min',false);
echo $this->Html->css('common/ui-lightness/jquery-ui-1.8.16.custom.css');
echo $javascript->link('admin/duvidas_frequentes/index.js',false);
?>
<div class="index">
    <?php echo $this->Form->create('DuvidaFrequente'); ?>
    <fieldset>
        <legend><?php printf(__('Editar %s', true), __('Dúvida Frequente', true)); ?></legend>
        <div class="left clear">
			<?php echo $this->Form->input('status', array('default'=>true,'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo'))); ?>
		</div>
		<div class="left clear tab" id="detail-tabs" style="margin-top: 15px;">
			<ul>
				<?php foreach($idiomas as $key => $idioma): ?>
					<li>
						<a href="#tab-content-idioma-<?php echo $idioma['Linguagem']['id']; ?>">
							<?php $img = ( isset($idioma['Linguagem']['thumb_filename']) ) ? $idioma['Linguagem']['thumb_dir'] . '/' . $idioma['Linguagem']['thumb_filename'] : "uploads/produto_imagem/thumb/sem_imagem.jpg"; ?>
							<?php echo $image->resize($img, 20, 20); ?>
							<?php echo $idioma['Linguagem']['nome']; ?>
						</a>
					</li>
				<?php endForeach; ?>
			</ul>
			
			<?php foreach($idiomas as $key => $idioma): ?>
				<div id="tab-content-idioma-<?php echo $idioma['Linguagem']['id']; ?>">		
					<?php echo $this->Form->input("DuvidaFrequenteDescricao.".$key.".id"); ?>
					<?php echo $this->Form->input("DuvidaFrequenteDescricao.".$key.".duvida_frequente_id", array('hiddenField'=>false,'type' => 'hidden','value' => $this->params['pass'][0])); ?>
					<?php echo $this->Form->input("DuvidaFrequenteDescricao.".$key.".language", array('hiddenField'=>false,'type' => 'hidden','value' => $idioma['Linguagem']['codigo'])); ?>
					<?php echo $this->Form->input("DuvidaFrequenteDescricao.".$key.".pergunta",array('class'=>'w312')); ?>
					<?php echo $this->Form->input("DuvidaFrequenteDescricao.".$key.".resposta",array('label'=>'Resposta','class'=>'w312')); ?>
				</div>
			<?php endForeach; ?>
		</div>
		
		<div class="clear"></div>
		<div class="clear"></div>
		<?php
			echo $this->Form->end(__('Salvar', true));
        ?>
    </fieldset>
</div>