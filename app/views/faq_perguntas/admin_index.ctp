<div class="index ">
    <h2><?php __('Perguntas'); ?></h2>
	<div class="btAddProduto">
    	<?php echo $this->Html->link(__('[+] Adicionar Pergunta', true), array('action' => 'add')); ?>
    </div>    
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('faq_id'); ?></th>
			<th><?php echo $this->Paginator->sort('pergunta'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>

            <th class="actions"><?php __('Ações'); ?></th>
        </tr>
        <?php
        $i = 0;
        foreach ($faq_perguntas as $faq_pergunta):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
            <tr<?php echo $class; ?>>
                <td align="center"><?php echo $faq_pergunta['FaqPergunta']['id']; ?>&nbsp;</td>
				<td><?php echo $faq_pergunta['Faq']['nome']; ?>&nbsp;</td>
				<td><?php echo $faq_pergunta['FaqPergunta']['pergunta']; ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $faq_pergunta['FaqPergunta']['created']); ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $faq_pergunta['FaqPergunta']['modified']); ?>&nbsp;</td>
				<td class="actions">
                    <?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $faq_pergunta['FaqPergunta']['id'])); ?>
					<?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $faq_pergunta['FaqPergunta']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $faq_pergunta['FaqPergunta']['id'])); ?>
				</td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, mostrando %current% registros de %count%, começando no registro %start% e terminando no %end%', true)
        ));
        ?>	</p>

    <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('anterior', true), array(), null, array('class' => 'disabled')); ?>
	 | 	<?php echo $this->Paginator->numbers(); ?>
        |
        <?php echo $this->Paginator->next(__('próxima', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
    </div>
    
</div>
