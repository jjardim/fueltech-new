<?php
	echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
	echo $javascript->link('common/jquery-ui-1.8.16.custom.min',false);
	echo $this->Html->css('common/ui-lightness/jquery-ui-1.8.16.custom.css');
	echo $javascript->link('admin/faqs/index.js',false);
?>
<div class="index">
    <?php echo $this->Form->create('FaqPergunta'); ?>
    <fieldset>
        <legend><?php printf(__('Editar %s', true), __('Pergunta', true)); ?></legend>
        <div class="left clear">
			<?php echo $this->Form->input('status', array('default'=>true,'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo'))); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('faq_id',array('type'=>'select','options'=>$faqs)); ?>
		</div>
		<div class="left clear tab" id="detail-tabs" style="margin-top: 15px;">
			<ul>
				<?php foreach($idiomas as $key => $idioma): ?>
					<li>
						<a href="#tab-content-idioma-<?php echo $idioma['Linguagem']['id']; ?>">
							<?php $img = ( isset($idioma['Linguagem']['thumb_filename']) ) ? $idioma['Linguagem']['thumb_dir'] . '/' . $idioma['Linguagem']['thumb_filename'] : "uploads/produto_imagem/thumb/sem_imagem.jpg"; ?>
							<?php echo $image->resize($img, 20, 20); ?>
							<?php echo $idioma['Linguagem']['nome']; ?>
						</a>
					</li>
				<?php endForeach; ?>
			</ul>
			<?php foreach($idiomas as $key => $idioma): ?>
				<div id="tab-content-idioma-<?php echo $idioma['Linguagem']['id']; ?>">	
					<div class="left clear">
						<?php echo $this->Form->input("FaqPerguntaDescricao.".$key.".id"); ?>
						<?php echo $this->Form->input("FaqPerguntaDescricao.".$key.".language", array('hiddenField'=>false,'type' => 'hidden','value' => $idioma['Linguagem']['codigo'])); ?>
						<?php echo $this->Form->input("FaqPerguntaDescricao.".$key.".pergunta",array('class'=>'w312')); ?>
					</div>
					<div class="left clear">
						<?php echo $this->Form->input("FaqPerguntaDescricao.".$key.".resposta",array('class'=>'w312')); ?>
					</div>
				</div>
			<?php endForeach; ?>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
    </fieldset>
	<?php echo $this->Form->end(__('Salvar', true)); ?>
</div>