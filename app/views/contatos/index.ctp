<?php echo $javascript->link('common/jquery.meio_mask.js', false); ?>
<?php echo $javascript->link('site/contato/index.js', false); ?>
<?php //echo $this->element('site/search');?>
<!-- start entry box -->
<div class="entry">
    <h4>Contato</h4>
    <?php echo $form->create('Contato', array('id' => 'comente', 'class' => 'Contato', 'url' => array('controller' => 'contato', 'action' => 'index'))); ?>
        <ul class="contato">
            <li>
                <?php echo $this->Session->flash(); ?>
            </li>
            <li>
                <?php echo $form->input('Contato.nome', array('class' => 'input2 ', 'label' => 'Nome:')); ?>
            </li>
            <li>
                <?php echo $form->input('Contato.email', array('class' => 'input2 ', 'label' => 'Email:')); ?>
            </li>
            <li>
                <?php echo $this->Form->input('telefone', array('class' => 'input2  mask-telefone', 'label' => 'Telefone:')); ?>
            </li>
            <li><?php echo $this->Form->input('Contato.estado', array('label'=>'Estado:','class' => 'pequeno',   'options' => $this->Estados->estadosBrasileiros())); ?></li>
            <li>
                <?php echo $form->input('Contato.cidade', array('class' => 'input2 ', 'label' => 'Cidade:')); ?>
            </li>
            <li>
                <?php echo $form->input('Contato.mensagem', array('value'=>'', 'label' => 'Mensagem:','type' => 'textarea','class'=>'textarea ')); ?>
            </li>
            <li class="clearl">
               <?php echo $form->submit('Enviar', array( 'class' => 'link', 'value' => '')); ?>
            </li>
        </ul>
    <?php e($form->end())?>
</div>
<div class="clear"></div>
<!-- end entry box -->