<div class="index">
	<h2 class="left"><?php __('Avise-me');?></h2>
	<div class="btAddProduto">
				<?php echo $this->Html->link(__('Rodar Notificação', true), array('action' => 'enviar')); ?>
			</div>
    <?php echo $form->create('Avise', array('url'=>'/admin/avise/index','class'=>'formBusca'));?>
		<fieldset>
			<div class="left">
				<?php echo $form->input('Filter.filtro',array('div'=>false,'label'=>'Nome:','class'=>'produtosFiltro'));  ?>
			</div>
			
		</fieldset>
			<?php echo $form->end('Busca'); ?>
	<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo $this->Paginator->sort('id');?></th>
		<th><?php echo $this->Paginator->sort('nome');?></th>
		<th><?php echo $this->Paginator->sort('email');?></th>
		<th><?php echo $this->Paginator->sort('produto_id');?></th>
        <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
        <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($avises as $avise): 
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td align="center"><?php echo $avise['Avise']['id']; ?>&nbsp;</td>
		<td><?php echo $avise['Avise']['nome']; ?>&nbsp;</td>
		<td><?php echo $avise['Avise']['email']; ?>&nbsp;</td>
		<td><?php echo $this->Html->link($avise['Produto']['nome'],array('controller'=>'produtos','action'=>'edit',$avise['Produto']['id'])); ?>&nbsp;</td>
		<td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s',$avise['Avise']['created']); ?>&nbsp;</td>
		<td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s',$avise['Avise']['modified']); ?>&nbsp;</td>
		
	</tr>
<?php endforeach; ?>
	</table>
	 <p><?php echo $this->Paginator->counter(array('format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true))); ?></p>
        

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('Próximo', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
