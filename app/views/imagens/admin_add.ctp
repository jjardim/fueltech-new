<?php
	echo $this->Html->css('admin/style');
    echo $javascript->codeBlock(
        "function selectURL(imagem) {
            if (imagem == '') return false;

            url = '".$this->Html->url('/uploads/imagem/filename/',true)."' + imagem;

            field = window.top.opener.browserWin.document.forms[0].elements[window.top.opener.browserField];
            field.value = url;
            if (field.onchange != null) field.onchange();
            window.top.close();
            window.top.opener.browserWin.focus();
        }"
    );

?>
<div id="container">
<div id="content">
<?php
				echo $this->Session->flash();
				echo $this->Session->flash('auth');
				?>
<fieldset>
			<div class="left">
				 <?php echo $this->Form->create('Imagem', array('type' => 'file', 'action' => 'add' ));
    echo $this->Form->input('filename', array('label'=>'Arquivo','type' => 'file'));
    echo $this->Form->input('dir', array('type' => 'hidden'));
    echo $this->Form->input('mimetype', array('type' => 'hidden'));
    echo $this->Form->input('filesize', array('type' => 'hidden'));
	 echo $this->Form->submit('Enviar');
?>
			</div>
			
		</fieldset>	
   

    <div class="index">
        <h2><?php __('Imagens'); ?></h2>
        <table cellpadding="0" cellspacing="0">
            <tr>

                <th><?php echo $this->Paginator->sort('id'); ?></th>
                <th><?php echo $this->Paginator->sort('filename','Imagem'); ?></th>
                <th><?php echo $this->Paginator->sort('filename'); ?></th>
                <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
                <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>
                <th class="actions">Ações</th>
            </tr>
        <?php
        $i = 0;
        foreach ($imagens as $imagem):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
        ?>
            <tr<?php echo $class; ?>>
                <td><?php echo $imagem['Imagem']['id']; ?>&nbsp;</td>
                <td><?php echo $image->resize($imagem['Imagem']['dir'].DS.$imagem['Imagem']['filename'], 100, 100,true);?>&nbsp;</td>
                <td><?php echo $imagem['Imagem']['filename']; ?>&nbsp;</td>
                <td><?php echo $imagem['Imagem']['created']; ?>&nbsp;</td>
                <td><?php echo $imagem['Imagem']['modified']; ?>&nbsp;</td>
                <td class="actions">
                <?php
                echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $imagem['Imagem']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $imagem['Imagem']['id']));
		echo $html->link('selecionar','#',array('onclick' => 'selectURL("'.$imagem['Imagem']['filename'].'");'));
		?>
            </td>
        </tr>
        <?php endforeach; ?>
            </table>
            <p><?php echo $this->Paginator->counter(array('format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true))); ?></p>


            <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array(), null, array('class' => 'disabled')); ?>
        	 | 	<?php echo $this->Paginator->numbers(); ?>
                |
        <?php echo $this->Paginator->next(__('Próximo', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
            </div>
        </div>
<?php echo $this->Form->end(); ?>
        </div>
        </div>
