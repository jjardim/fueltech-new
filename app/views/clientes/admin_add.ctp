<?php
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('admin/paginas/crud.js',false);
?>
<div class="index">
<?php echo $this->Form->create('Cliente');?>
	<fieldset>
 		<legend><?php printf(__('Adidionar %s', true), __('Cliente', true)); ?></legend>
	<?php
        echo $this->Form->input('status', array('default'=>true,'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
		echo $this->Form->input('nome',array('class'=>'w312'));
        echo $this->Form->input('url',array('class'=>'w312'));
	?>
		<legend>Thumb</legend>
        <?php
        echo $this->Form->input('Categoria.thumb_filename', array('type' => 'file'));
        echo $this->Form->input('Categoria.thumb_dir', array('type' => 'hidden'));
        echo $this->Form->input('Categoria.thumb_mimetype', array('type' => 'hidden'));
        echo $this->Form->input('Categoria.thumb_filesize', array('type' => 'hidden'));
		echo $this->Form->end(__('Inserir', true));
	?>
	</fieldset>
</div>