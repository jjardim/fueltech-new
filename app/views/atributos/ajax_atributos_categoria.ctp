	<?php if(isset($filtros_selecionados)): ?> 
		<!-- start filter -->
		<div class="filter" style="padding: 15px 0px 0px 0px; margin: -20px 0 -10px">
			<div id="filtros-selecionados">
				<h4>Filtros Selecionados</h4>
				<?php foreach($filtros_selecionados as $tipo_filtro):?>
				<a title="Remove filtro: <?php echo $tipo_filtro['nome_filtro']; ?> <?php echo $tipo_filtro['valor_filtro']; ?>" class="gray filtros_selecionados" href="javascript:void(0);" rel="<?php echo $tipo_filtro['id_filtro']; ?>" ><strong>X</strong> <span><?php echo $tipo_filtro['nome_filtro']; ?> : <?php echo $tipo_filtro['valor_filtro']; ?></span></a>
				<?php endforeach;?>
				<div class="clear"></div>
			</div>
		</div>
		<!-- end filter -->
	<?php endIf;?>

	<?php if($menu_filtro): ?>

		<!-- start filter -->
		<div class="filter">
		
			<h2 class="title"><span>FILTRO</span></h2>
			<!-- filter search -->
			<form action="#" class="filter-search" style="display: none;">
				<input type="text" value="Marca" name="field" class="input" onfocus="if(this.value == 'Marca') { this.value = ''; }" onblur="if(this.value == '') { this.value = 'Marca'; }" />
				<input type="submit" value="" name="button" class="button" />
			</form>
				
		</div>
		<!-- end filter -->
			
		<?php foreach($menu_filtro as $tipo_filtro):?>
		
			<h3><?php echo $this->String->title_case($tipo_filtro['nome_filtro']); ?></h3>
			<br />
			<ul>
				<?php foreach($tipo_filtro['valor_filtro'] as $filtro):?>
					<li>
						<input type="checkbox" class="checkbox" id="<?php echo $filtro['id']; ?>" rel="<?php echo low(Inflector::slug($tipo_filtro['nome_filtro'], '-')); ?>" value="<?php e($filtro['id']); ?>" name="data[Filtro][id]" />
						<label for="<?php echo $filtro['id']; ?>"><?php echo $this->String->title_case($filtro['valor']); ?></label>
					</li>
				<?php endforeach;?>
			</ul>
			
		<?php endforeach;?>

	<?php endIf;?>	