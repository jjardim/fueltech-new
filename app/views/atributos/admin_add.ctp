<div class="index">
<?php
echo $form->create('Atributo',array('type' => 'file', 'action'=>'add'));
?>
    <fieldset>
        <legend><?php __('Adicionar Atributos'); ?></legend>
        <?php 
			echo $this->Form->input('status',array('default'=>true,'type'=>'radio','options'=>array(true=>'Ativo',false=>'Inativo')));
			echo $this->Form->input('atributo_tipo_id',array('label'=>'Tipo de Atributo','options' => $atributo_tipos));
		?>
		<?php echo $this->Form->input("valor", array('class'=>'w312')); ?>

	<!--	<ul style="list-style: none outside none; padding-left: 0px; margin-left: -10px;">
		<?php #foreach($idiomas as $key => $idioma): ?>
			<li>
				<?php #$img = ( isset($idioma['Linguagem']['thumb_filename']) ) ? $idioma['Linguagem']['thumb_dir'] . '/' . $idioma['Linguagem']['thumb_filename'] : "uploads/produto_imagem/thumb/sem_imagem.jpg"; ?>
				<div style="display: inline-block; float: left; clear: both;"><?php #echo $image->resize($img, 20, 20); ?> - <?php #echo $idioma['Linguagem']['nome']; ?></div>
				<?php #echo $this->Form->input("AtributoDescricao.".$key.".valor", array('class'=>'w312')); ?>
			</li>
		<?php #endForeach; ?>
		</ul> -->
		<br /><br />
        <br class="clear" />
        <legend>Thumb</legend>
        <?php
			echo $this->Form->input('Atributo.thumb_filename', array('type' => 'file'));
			echo $this->Form->input('Atributo.thumb_dir', array('type' => 'hidden'));
			echo $this->Form->input('Atributo.thumb_mimetype', array('type' => 'hidden'));
			echo $this->Form->input('Atributo.thumb_filesize', array('type' => 'hidden'));
		?>
		<br class="clear" />
		<?php
			echo $form->end('Inserir');
        ?>
    </fieldset>
</div>