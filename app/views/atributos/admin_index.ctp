<?php echo $javascript->link('admin/atributos/index.js',false);?>

<div class="index">
    <h2><?php __('Atributos'); ?></h2>
	<div class="btAddProduto">
    	<?php echo $this->Html->link(__('[+] Adicionar Atributo', true), array('action' => 'add')); ?>
    </div>
	<?php echo $form->create('Atributo', array('action'=>'/index','class'=>'formBusca'));?>
	<fieldset>
		<div class="left">
            <?php echo $form->input('Filter.atributo_tipo_id',array('div'=>false,'label'=>'Tipo de Atributo:','class'=>'produtosFiltro','options' => $atributo_tipos));  ?>
		</div>
		<div class="left">
            <?php echo $form->input('Filter.valor',array('div'=>false,'label'=>'Valor:','class'=>'produtosFiltro'));  ?>
		</div>
		<div class="left">
            <?php echo $form->input('Filter.status',array('div'=>false,'label'=>'Status:','class'=>'produtosFiltro','options' => array(true => 'Ativo', false => 'Inativo')));  ?>
		</div>
		<div class="submit">
			<input name="submit" type="submit" class="button1" value="Busca" />
		</div>
		<div class="submit">
			<input name="submit" type="submit" class="button1" value="Exportar" />
		</div>
		<div class="submit">
			<a href="<?php e($this->Html->Url('index/filter:clear')) ?>" title="Limpar Filtro" class="button" >Limpar Filtro</a>
		</div>
	</fieldset>
	<?php echo $form->end(); ?>
	<table cellpadding="0" cellspacing="0">
		<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('Tipo de Atributo','atributo_tipo_id'); ?></th>
			<th><?php echo $this->Paginator->sort('valor'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
			<th><?php echo $this->Paginator->sort('Data Modificação', 'updated'); ?></th>
			<th class="actions"><?php __('Ações'); ?></th>
		</tr>
		<?php
		$i = 0;
		foreach ($atributos as $atributo):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
			?>
			<tr<?php echo $class; ?>>
				<td align="center"><?php echo $atributo['Atributo']['id']; ?>&nbsp;</td>
				<td><?php echo $atributo['AtributoTipo']['nome']; ?>&nbsp;</td>
				<td><?php echo $atributo['Atributo']['valor']; ?>&nbsp;</td>
				<td align="center"><?php echo ( $atributo['Atributo']['status'] ) ? 'Ativo' : 'Inativo' ; ?>&nbsp;</td>                
				<td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $atributo['Atributo']['created']); ?>&nbsp;</td>
				<td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $atributo['Atributo']['updated']); ?>&nbsp;</td>

				<td class="actions">
					<?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $atributo['Atributo']['id'])); ?>
					<?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $atributo['Atributo']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $atributo['Atributo']['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>
	<p>
		<?php
		echo $this->Paginator->counter(array(
			'format' => __('Página %page% de %pages%, mostrando %current% registros de %count%, começando no registro %start% e terminando no %end%', true)
		));
		?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('anterior', true), array(), null, array('class' => 'disabled')); ?>
	 | 	<?php echo $this->Paginator->numbers(); ?>
		|
		<?php echo $this->Paginator->next(__('próxima', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
	</div>
</div>
