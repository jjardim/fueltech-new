<?php echo $javascript->link('common/jquery.meio_mask.js',false); ?>
<?php echo $javascript->link('admin/usuarios/crud.js',false); ?>
<div class="index">
<?php echo $this->Form->create('Usuario');?>
	<fieldset>
		<legend><?php __('Editar Usuário'); ?></legend>
	        <?php
                echo $this->Form->input('id');
                echo $this->Form->input('grupo_id');
	?>
	<div class="left">
		<?php echo $this->Form->input('nome',array('div'=>false,'label'=>'Nome','class'=>'w312')); ?>
	</div>
	<div class="left">
		<?php echo $this->Form->input('apelido',array('div'=>false,'label'=>'Apelido')); ?>
	</div>
	<div class="left clear">
		<?php echo $this->Form->input('email',array('div'=>false,'label'=>'E-mail','class'=>'w312')); ?>
	</div>
	<div class="left clear">
		<?php echo $this->Form->input('senha_nova',array('div'=>false,'type'=>'password','div'=>false,'label'=>'Senha')); ?>
	</div>
	<div class="left">
		<?php echo $this->Form->input('confirm',array('div'=>false,'type'=>'password','label'=>'Confirmar senha','div'=>false)); ?>
	</div>
	<div class="left clear">
	<?php echo $this->Form->input('tipo_pessoa',array('div'=>false,'options'=>array(''=>'Selecione','F'=>'Fisica','J'=>'Juridica'),'class'=>'produtosSel')); ?>
	</div>
	<div class="left clear">
	<?php echo $this->Form->input('data_nascimento',array('div'=>false,'type'=>'text','class'=>'mask-data')); ?>
	</div>
	<div class="left">
	<?php echo $this->Form->input('sexo',array('div'=>false,'options'=>array(''=>'Selecione','F'=>'Feminino','M'=>'Masculino'))); ?>
	</div>
	<div class="left clear">
	<?php echo $this->Form->input('rg',array('div'=>false)); ?>
	</div>
	<div class="left">
	<?php echo $this->Form->input('cpf',array('div'=>false,'class'=>'mask-cpf')); ?>
	</div>
	<div class="left clear">
	<?php echo $this->Form->input('cnpj',array('div'=>false,'class'=>'mask-cnpj')); ?>
	</div>
	<div class="left clear">	
	<?php echo $this->Form->input('nome_fantasia',array('div'=>false,'class'=>'mask-cpf w312')); ?>
	</div>
	<div class="left clear">	
	<?php 	echo $this->Form->input('razao_social',array('div'=>false,'class'=>'mask-cpf w312')); ?>
	</div>
	<div class="left clear">
	<?php echo $this->Form->input('inscricao_estadual',array('div'=>false)); ?>
	</div>
	<div class="left clear">
	<?php echo $this->Form->input('telefone',array('div'=>false,'class'=>'mask-telefone', 'maxlength' => '15')); ?>
	</div>
	<div class="left">
	<?php echo $this->Form->input('celular',array('div'=>false,'class'=>'mask-telefone', 'maxlength' => '15')); ?>
	</div>
	<br class="clear" />
	<?php echo $this->Form->end(__('Salvar', true));?>
	</fieldset>
</div>
