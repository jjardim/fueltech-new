<div style="width: 960px; display: block; overflow: hidden;">
	<?php echo $this->Session->flash(); ?>
</div>

<?php echo $form->create('Usuario', array('url' => array('controller' => 'usuarios', 'action' => 'reset', $email, $key)));?>
<!-- start column1 -->
<div class="col1" style="margin: 0 auto; display: block; margin-top: 15px; padding-bottom: 20px">
	<h3>Alterar Senha</h3>
	<div class="login-block">
		<div class="column1-box">
			<div class="row1">
				<?php echo $this->Form->input('senha', array('div'=>false,'type'=>'password','class' => 'input', 'label' => 'Nova senha')); ?>
			</div>
			<?php echo $form->submit('Salvar', array( 'class' => 'button1 button_reset')); ?>
		</div>
	</div>
</div>
<!-- end column1 -->
<?php echo $this->Form->end(); ?>