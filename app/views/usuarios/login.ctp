<?php echo $this->Html->css('common/nyro_modal/nyroModal.css'); ?>
<?php echo $javascript->link('common/nyro_modal/jquery.nyroModal.custom'); ?>
<?php echo $javascript->link('common/jquery.meio_mask'); ?>
<?php echo $this->Javascript->link('common/use_default.js'); ?>
<?php echo $this->Javascript->link('site/usuarios/login.js'); ?>

<!-- start header -->
<div id="header">
    <!-- start logo -->
    <div id="logo">
        <a href="<?php e($this->Html->Url('/')) ?>" title="Fuel Tech">
            <?php echo $this->Html->image('site/logo.png', array('alt' => 'Fuel Tech', 'width' => '219', 'height' => '53')) ?>
        </a>
    </div>
    <!-- end logo -->
    <!-- start header mid -->
    <div id="header-mid">
        &nbsp;
    </div>
    <!-- end header mid -->
    <!-- start header right -->
    <div id="header-right">
        <!-- start language -->
        <div class="language">
            <ul>
                <li><span><?php echo __("VERSION"); ?>: </span></li>
                <?php
                if (isset($menu_idiomas) && count($menu_idiomas) > 0):
                    $first = true;
                    foreach ($menu_idiomas as $idioma):
                        if (Configure::read('Config.language') == $idioma['Linguagem']['codigo']) {
                            $class = "class='active'";
                        } else {
                            $class = "";
                        }
                        ?>		
                        <?php if (!$first): ?>
                            <li>&nbsp; | &nbsp;</li>
                            <?php endIf; ?>
                        <li <?php echo $class ?>>
                            <?php if ($idioma['Linguagem']['externo'] == 0): ?>
                                <a href="<?php echo $this->Html->Url('/') . $idioma['Linguagem']['codigo']; ?>" title="<?php echo $idioma['Linguagem']['nome']; ?>">
                                    <?php //echo $image->resize(isset($idioma['Linguagem']['thumb_filename']) ? $idioma['Linguagem']['thumb_dir'] . '/' . $idioma['Linguagem']['thumb_filename'] : 'uploads/produto_imagem/filename/sem_imagem.jpg', 12, 8, true, array('alt' => $idioma['Linguagem']['nome'])); ?>
                                    <?php echo $idioma['Linguagem']['nome']; ?>
                                </a>
                            <?php else: ?>
                                <a href="<?php echo $idioma['Linguagem']['url']; ?>" title="<?php echo $idioma['Linguagem']['nome']; ?>">
                                    <?php //echo $image->resize(isset($idioma['Linguagem']['thumb_filename']) ? $idioma['Linguagem']['thumb_dir'] . '/' . $idioma['Linguagem']['thumb_filename'] : 'uploads/produto_imagem/filename/sem_imagem.jpg', 12, 8, true, array('alt' => $idioma['Linguagem']['nome'])); ?>
                                    <?php echo $idioma['Linguagem']['nome']; ?>
                                </a>
                            <?php endIf; ?>
                        </li>
                        <?php
                        if ($first) {
                            $first = false;
                        }
                    endForeach;
                endIf;
                ?>
            </ul>
        </div>
        <!-- end language -->
        <div class="clear"></div>
    </div>
    <!-- end header right -->
    <!-- start numerical -->
    <div id="numerical">
        <ul>
            <li><a href="<?php echo $this->Html->url("/carrinho") ?>" title="CARRINHO" class="visited"><span>01</span><strong>CARRINHO</strong></a></li>
            <li><a href="javascript:void(0);" title="Identificação" class="active"><span>02</span><strong>Identificação<b>Você está aqui</b></strong></a></li>
            <li><a href="javascript:void(0);" title="Entrega"><span>03</span><strong>Entrega</strong></a></li>
            <li><a href="javascript:void(0);" title="Pagamento"><span>04</span><strong>Pagamento</strong></a></li>
            <li class="last"><a href="javascript:void(0);" title="Confirmação"><span>05</span><strong>Confirmação</strong></a></li>
        </ul>
    </div>
    <!-- end numerical -->
    <div class="clear"></div>
</div>
<!-- end header -->

<!-- start container -->
<div id="container">

    <h2 class="page-title fnt-size">IDENTIFICAÇÃO 
            <!--<a href="#" title="facebook"><img src="images/img_facebook.png" width="154" height="22" alt="facebook" class="left" /></a>-->
    </h2>
    <?php echo $this->Form->create('Usuario', array('id' => 'cad', 'controller' => 'usuarios', 'action' => 'login')); ?>
    <!-- start column1 -->
    <div class="col1">
        <h3>JÁ SOU CLIENTE</h3>
        <div class="login-block">
            <div class="column1-box">
                <div class="row1">
                    <?php echo $this->Form->input('login_email', array('div' => false, 'class' => 'input useDefault', 'rel' => 'Digite seu email', 'label' => 'Digite seu email')); ?>
                </div>
                <div class="row1">
                    <?php echo $this->Form->input('login_senha', array('type' => 'password', 'div' => false, 'class' => 'input useDefault', 'rel' => 'Digite sua senha', 'label' => 'Digite sua senha',)); ?>
                </div>
                <input name="submit" type="submit" value="CONTINUAR" class="button1" />
                <span><a href="<?php e($this->Html->Url("/usuarios/esqueci")) ?>"  target="_blank" class="nyroModal" title="Esqueci minha senha">Esqueci minha senha</a></span>            
            </div>
        </div>
    </div>
    <!-- end column1 -->
    <?php echo $this->Form->end(); ?>

    <?php echo $this->Form->create('Usuario', array('class' => '', 'url' => '/usuarios/add')); ?>
    <!-- start column1 -->
    <div class="col1 last">
        <h3>QUERO ME CADASTRAR</h3>
        <div class="login-block">
            <div class="column1-box">
                <form action="#">
                    <div class="row1">
                        <?php echo $this->Form->input('usuario_cep', array('div' => false, 'class' => 'input mask-cep useDefault', 'rel' => 'Digite seu CEP', 'label' => 'Digite seu CEP',)); ?>
                    </div>
                    <div class="row1">
                        <?php echo $this->Form->input('usuario_email', array('div' => false, 'class' => 'input useDefault', 'rel' => 'Digite seu email', 'label' => 'Digite seu email',)); ?>
                    </div>    
                    <input name="submit" type="submit" value="CONTINUAR" class="button1" />
                    <span><a href="http://www.buscacep.correios.com.br/" target="_blank" title="Não sei meu cep" style="float: left;">Não sei meu CEP</a></span>
                </form>
            </div>
        </div>
    </div>
    <!-- end column1 -->	
    <?php echo $this->Form->end(); ?>

    <div class="clear"></div>

</div>
<!-- end container -->