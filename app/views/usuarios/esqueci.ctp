<?php echo $this->Html->css('common/nyro_modal/default',null,array('inline'=>false));?>
<div class="container-modal" id="content-esqueci-senha">
    <div class="container-header" style="width: 500px;">
        <h1>Esqueci minha senha</h1>
    </div>
    <div style="display: block; clear: both; height: 120px;">
        <?php echo $form->create('Usuario', array('url' => array('controller' => 'usuarios', 'action' => 'esqueci')));?>
            <ul>
                <li class="clear"><?php echo $this->Session->flash(); ?></li>
                <li><?php echo $form->input("email", array("label" => "Digite seu email")) ?></li>
                <li><?php echo $form->button("Enviar", array("class" => "link2","value" => "enviar")); ?></li>
            </ul>
        <?php echo $form->end(); ?>	
    </div>
	<div class="logo"></div>
</div>
