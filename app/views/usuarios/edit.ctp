<?php echo $this->Javascript->link('common/jquery.meio_mask.js'); ?>
<?php echo $this->Javascript->link('site/usuarios/crud.js'); ?>

<div style="width: 960px; display: block; overflow: hidden;">
	<?php echo $this->Session->flash(); ?>
</div>

<!-- start leftcol -->
<div id="leftcol">
	<?php echo $this->element('site/left'); ?>
</div>
<!-- end leftcol -->

<!-- start rightcol -->
<div id="rightcol" class="cadastro">
	
	<h2 id="title-cadastro-pessoa-fisica" class="page-title"><small>Dados cadastrais: PESSOA FÍSICA</small></h2>
	<h2 id="title-cadastro-pessoa-juridica" class="page-title"><small>Dados cadastrais: PESSOA JURÍDICA</small></h2>
			
	<!-- start fillform2 -->
	<div class="fillform2">
		<?php echo $this->Form->create('Usuario'); ?>
		
		<!-- start row1-top -->
		<div class="row1-top" style="display: none;">
			<?php
				echo $this->Form->input('tipo_pessoa', array(
					'div' => false,
					'label' => true,
					'type' => 'radio',
					'options' => array('F' => 'Pessoa física', 'J' => 'Pessoa jurídica'),
					'default' => 'F'
				));
			?>
		</div>
		<!-- end row1-top -->
		
		<!-- start content -->
		<div class="content">
		
			<!-- start row -->
			<div class="row">
				<?php echo $this->Form->input('nome', array('class' => 'input1', 'label' => 'Nome Completo *', 'div' => false)); ?>
				<div class="clear"></div>
			</div>
			<!-- end row -->
			<!-- start row -->
			<div class="row">
				<?php echo $this->Form->input('data_nascimento', array('class' => 'input1 input5 mask-data', 'type' => 'text', 'label' => 'Data de Nascimento', 'div' => false)); ?>
				<div class="clear"></div>
			</div>
			<!-- end row -->
			
			<!-- start row -->
			<div class="row-cen" style="padding-left: 0px; margin-top: 10px">
				<?php echo $this->Form->input('sexo', array(
															'div' => false,
															'label' => true,
															'type' => 'radio',
															'options' => array('M' => 'Masculino', 'F' => 'Feminino')
														)
											); ?>
				<div class="clear"></div>
			</div>
			<!-- end row -->
			
			<!-- start row -->
			<div class="row">
				<?php echo $this->Form->input('rg', array('class' => 'input1 input5', 'label' => 'RG', 'div' => false)); ?>
				<div class="clear"></div>
			</div>
			<!-- end row -->
			
			<!-- start row -->
			<div class="row">
				<?php echo $this->Form->input('cpf', array('class' => 'input1 input5 mask-cpf', 'label' => 'CPF *', 'div' => false)); ?>
				<div class="clear"></div>
			</div>
			<!-- end row -->
			
			<!-- start row -->
			<div class="row">
				<?php echo $this->Form->input('razao_social', array('class' => 'input1', 'label' => 'Razão Social *', 'div' => false)); ?>
				<div class="clear"></div>
			</div>
			<!-- end row -->
			
			<!-- start row -->
			<div class="row">
				<?php echo $this->Form->input('cnpj', array('class' => 'input1 mask-cnpj', 'label' => 'CNPJ *', 'div' => false)); ?>
				<div class="clear"></div>
			</div>
			<!-- end row -->
			
			<!-- start row -->
			<div class="row">
				<?php echo $this->Form->input('inscricao_estadual', array('class' => 'input1', 'label' => 'Inscrição Estadual', 'div' => false)); ?>
				<div class="clear"></div>
			</div>
			<!-- end row -->
			
			<!-- start row -->
			<div class="row">
				<?php echo $this->Form->input('nome_fantasia', array('class' => 'input1', 'label' => 'Nome Fantasia', 'div' => false)); ?>
				<div class="clear"></div>
			</div>
			<!-- end row -->
			
			<!-- start row -->
			<div class="row">
				<?php echo $this->Form->input('email', array('class' => 'input1', 'label' => 'E-mail *', 'div' => false)); ?>
				<div class="clear"></div>
			</div>
			<!-- end row -->
			
			<!-- start row -->
			<div class="row">
				<?php echo $this->Form->input('senha_nova', array('label'=>'Senha *','value' => null, 'type' => 'password', 'class' => 'input1 input2', 'div' => false)); ?>
				<span style="margin-left: 5px; font-size: 11px; line-height: 26px;">( Mínimo 6 caracteres )</span>
				<div class="clear"></div>
			</div>
			<!-- end row -->
			
			<!-- start row -->
			<div class="row">
				<?php echo $this->Form->input('confirm', array('label'=>'Confirmar Senha *','value' => null, 'type' => 'password', 'class' => 'input1 input2', 'div' => false)); ?>					
				<div class="clear"></div>
			</div>
			<!-- end row -->
			
			<!-- start row -->
			<div class="row">
				<?php echo $this->Form->input('nomeTmp', array('class' => 'input1', 'label' => 'Pessoa de Contato *', 'div' => false)); ?>
				<div class="clear"></div>
			</div>
			<!-- end row -->
			
			<!-- start row -->
			<div class="row">
				<?php echo $this->Form->input('telefone', array('class' => 'input1 input5 mask-telefone', 'label' => 'Telefone', 'div' => false)); ?>
				<div class="clear"></div>
			</div>
			<!-- end row -->
			
			<!-- start row -->
			<div class="row">
				<?php echo $this->Form->input('celular', array('class' => 'input1 input5 mask-telefone', 'label' => 'Celular *', 'div' => false)); ?>
				<div class="clear"></div>
			</div>
			<!-- end row -->
			<div class="row">
				<input name="SALVAR" type="submit" value="SALVAR" class="button1" />
			</div>
			<div class="clear"></div>
		</div>
		<!-- end content -->
		<?php echo $this->Form->end(); ?>
	</div>
	<!-- end fillform2 -->
	<div class="clear"></div>
</div>
<!-- end rightcol -->
<div class="clear"></div>