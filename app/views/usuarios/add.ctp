<?php echo $this->Javascript->link('common/jquery.meio_mask.js'); ?>
<?php echo $this->Javascript->link('site/usuarios/crud.js'); ?>

<!-- start breadcrumb -->
<div class="breadcrumb">
	<h2>CADASTRE-SE</h2>        	
</div>	
<!-- end breadcrumb -->

<!-- start cadastro -->
<div class="cadastro">

	<!-- start flash error -->
	<div style="width: 960px; display: block; overflow: hidden;">
		<?php echo $this->Session->flash(); ?>
	</div>
	<!-- end flash error -->

	<?php echo $this->Form->create('Usuario'); ?>
	
		<!-- start row1-top -->
		<div class="row1-top">
			<?php
				echo $this->Form->input('tipo_pessoa', array(
					'div' => false,
					'label' => true,
					'type' => 'radio',
					'options' => array('F' => 'Pessoa física', 'J' => 'Pessoa jurídica'),
					'default' => 'F'
				));
			?>
		</div>
		<!-- end row1-top -->
		
		<!-- start column1st -->
		<div class="column1st fillform2">
			<h3 id="title-cadastro-pessoa-fisica">CADASTRO PESSOA FÍSICA</h3>
			<h3 id="title-cadastro-pessoa-juridica">CADASTRO PESSOA JURÍDICA</h3>
			
			<!-- start commoncol -->
			<div class="commoncol">
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('nome', array('class' => 'input1', 'label' => 'Nome Completo *', 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('data_nascimento', array('class' => 'input1 input5 mask-data', 'type' => 'text', 'label' => 'Data de Nascimento', 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row-cen">
					<?php echo $this->Form->input('sexo', array(
																'div' => false,
																'label' => true,
																'type' => 'radio',
																'options' => array('M' => 'Masculino', 'F' => 'Feminino')
															)
												); ?>
					
					<?php //echo $this->Form->input('sexo', array('label'=>'Sexo','class' => ' select3', 'options' => array('' => 'Selecione o Sexo', 'F' => 'Feminino', 'M' => 'Masculino'), 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('rg', array('class' => 'input1 input5', 'label' => 'RG', 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('cpf', array('class' => 'input1 input5 mask-cpf', 'label' => 'CPF *', 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('razao_social', array('class' => 'input1', 'label' => 'Razão Social *', 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('cnpj', array('class' => 'input1 mask-cnpj', 'label' => 'CNPJ *', 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('inscricao_estadual', array('class' => 'input1', 'label' => 'Inscrição Estadual', 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('nome_fantasia', array('class' => 'input1', 'label' => 'Nome Fantasia', 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('email', array('class' => 'input1', 'label' => 'E-mail *', 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('senha_nova', array('label'=>'Senha *','value' => null, 'type' => 'password', 'class' => 'input1 input2', 'div' => false)); ?>
					<span style="margin-left: 5px; font-size: 11px; line-height: 26px;">( Mínimo 6 caracteres )</span>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('confirm', array('label'=>'Confirmar Senha *','value' => null, 'type' => 'password', 'class' => 'input1 input2', 'div' => false)); ?>					
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('nomeTmp', array('class' => 'input1', 'label' => 'Pessoa de Contato *', 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('telefone', array('class' => 'input1 input5 mask-telefone', 'label' => 'Telefone', 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('celular', array('class' => 'input1 input5 mask-telefone', 'label' => 'Celular *', 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
			</div>
			<!-- end commoncol -->
		</div>
		<!-- end column1st -->
	
		<!-- start column1st -->
		<div class="column1st fillform2">
			<h3>CADASTRO DE ENDEREÇO</h3>
			<!-- start commoncol -->
			<div class="commoncol">
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->hidden('UsuarioEndereco.0.cobranca',array('value'=>true)); ?>
					<?php echo $this->Form->input('UsuarioEndereco.0.cep', array('id'=>'UsuarioEnderecoCep','class' => 'input1 input6 mask-cep', 'label' => 'Primeiro digite o CEP *','after'=>$this->Html->image('/img/site/zoomloader.gif', array('id'=>'loading','style'=>'visibility:hidden; margin-top: 3px; margin-left: 3px;','alt' => 'Carregando', 'title' => 'Carregando')), 'div' => false)); ?>
					<span class="url-correios" style="clear: both; float: left; margin-left: auto; margin-top: 5px; width: auto;">Não sabe seu CEP? <a href="http://www.buscacep.correios.com.br/" target="_blank">Clique aqui.</a></span>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('UsuarioEndereco.0.nome', array('class' => 'input1 input7', 'label' => 'Identifique o Endereço *', 'div' => false)); ?>
					<span style="margin-left: 5px; font-size: 11px; line-height: 26px;">( Ex: casa, empresa, etc. )</span>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('UsuarioEndereco.0.rua', array('id'=>'UsuarioEnderecoRua','class' => 'input1 input10', 'label' => 'Endereço *', 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('UsuarioEndereco.0.numero', array('class' => 'input1 input8', 'label' => 'Número *', 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('UsuarioEndereco.0.complemento', array('class' => 'input1 input7', 'label' => 'Complemento', 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('UsuarioEndereco.0.bairro', array('id'=>'UsuarioEnderecoBairro','class' => 'input1 input9', 'label' => 'Bairro *', 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('UsuarioEndereco.0.uf', array('id'=>'UsuarioEnderecoUf','label'=>'Estado','class' => 'select3', 'options' => $this->Estados->estadosBrasileiros(), 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('UsuarioEndereco.0.cidade', array('id'=>'UsuarioEnderecoCidade','class' => 'input1 input9', 'label' => 'Cidade', 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<input name="submit" type="submit" class="button" value="SALVAR CADASTRO" />
					<p style="float: right; margin-top: 107px; margin-right: 35px;">* Preenchimento obrigatório</p>
					<div class="clear"></div>
				</div>
				<!-- end row -->
			
			</div>
			<!-- end commoncol -->
		</div>
		<!-- end column1st -->
	
	<?php echo $this->Form->end(); ?>
	
</div>
<!-- end cadastro -->
<div class="clear"></div>