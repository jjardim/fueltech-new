<div class="index">
	<h2 class="left"><?php __('Usuários');?></h2>
	<div class="btAddProduto">
    	<?php echo $this->Html->link(__('[+] Adicionar Usuário', true), array('action' => 'add')); ?>
    </div>
    <?php echo $form->create('Usuario', array('action'=>'/index','class'=>'formBusca'));?>
		<fieldset>
			<div class="left">
				<?php echo $form->input('Filter.filtro',array('div'=>false,'label'=>'Nome / CPF / CNPJ:','class'=>'produtosFiltro'));  ?>
			</div>
			<?php echo $form->end('Busca'); ?>
		</fieldset>
	<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo $this->Paginator->sort('id');?></th>
		<th><?php echo $this->Paginator->sort('nome');?></th>
		<th><?php echo $this->Paginator->sort('email');?></th>
		<th>CPF/CNPJ</th>
		<th><?php echo $this->Paginator->sort('telefone');?></th>
		<th><?php echo $this->Paginator->sort('grupo_id');?></th>
		<th><?php echo $this->Paginator->sort('status');?></th>
        <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
		<th class="actions">Ações</th>
	</tr>
	<?php
	$i = 0;
	foreach ($usuarios as $usuario): 
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td align="center"><?php echo $usuario['Usuario']['id']; ?>&nbsp;</td>
		<td><?php echo $usuario['Usuario']['nome']; ?>&nbsp;</td>
		<td><?php echo $usuario['Usuario']['email']; ?>&nbsp;</td>
		<td align="center"><?php echo !empty($usuario['Usuario']['cnpj'])?$usuario['Usuario']['cnpj']:$usuario['Usuario']['cpf']; ?>&nbsp;</td>
		<td><?php echo $usuario['Usuario']['telefone']; ?>&nbsp;</td>
		<td><?php echo $usuario['Grupo']['nome']; ?>&nbsp;</td>
		<td align="center"><?php echo ($usuario['Usuario']['status'])?'Ativo':'Inativo'; ?>&nbsp;</td>
		<td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s',$usuario['Usuario']['created']); ?>&nbsp;</td>
		
		<td class="actions">
			<?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $usuario['Usuario']['id'])); ?>
			<?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $usuario['Usuario']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $usuario['Usuario']['id'])); ?>
			<?php echo $this->Html->link(__('Endereços', true), array('controller'=>'usuario_enderecos','action' => 'index/user_id:'. $usuario['Usuario']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	 <p><?php echo $this->Paginator->counter(array('format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true))); ?></p>
        

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('Próximo', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
