<?php echo $this->Html->css('admin/login/index.css',null,array('inline'=>false)); ?>
<?php echo $javascript->link('common/jquery.meio_mask.js',false); ?>
<?php echo $javascript->link('admin/clientes/crud.js',false); ?>
<div class="index">
<?php echo $this->Form->create('Usuario', array('id'=>'form-login','controller' => 'usuarios','action' => 'login', 'admin' => true)); ?>
	<fieldset>
		<h1>Login Administrativo</h1>
		<legend><?php echo $html->image('admin/login/loja_logo.png')?></legend>
	<?php
		echo $this->Form->input('login_email',array('label'=>'Login','div'=>'inputLogin'));
		echo $this->Form->input('login_senha',array('label'=>'Senha','type'=>'password','div'=>'inputLogin'));
		echo $this->Form->submit('Entrar',array('div'=>false,'id'=>'bt_login'));
        echo $this->Session->flash();
        echo $this->Session->flash('auth');
	?>
	</fieldset>
<?php echo $this->Form->end();?>
</div>
