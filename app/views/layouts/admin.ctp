<?php
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php 
	echo $this->Html->charset(); 
?>

<title><?php echo Configure::read('Loja.titulo') ?> - <?php echo $title_for_layout; ?></title>
<?php 
echo $this->element('common/js_path'); 

echo $this->Html->meta('icon');

echo $this->Javascript->link('common/jquery.js');

echo $this->Html->css('admin/style');

echo $scripts_for_layout;
?>
</head>
<body>
<div class="container-info" style="display:none;z-index: 999;position: fixed;width: 100%;height: 100%;background: url('<?php echo $this->Html->Url('/img/site/img-modal.png'); ?>');">
    <div class='info-upload' style='position: absolute;border:1px solid #DDD;padding: 10px;font-size: 14px;color:#FFF;font-weight: bold;left:50%;width: 300px;margin-left: -150px;top:50%;'></div>
</div>
<div id="container" >
    <div id="header">
    	<div class="topo">
			<h1><?php e($html->link($html->image("/img/admin/login/reweb_logo.png"),array('controller' => 'home','action' => 'index'),array('escape'=>false) )); ?></h1>
			<div class="logotext"><?php isset($auth['Usuario']['nome']) ? e('Olá ' . $auth['Usuario']['nome'] .'&nbsp;&nbsp;'. $this->Html->link('Logout', '/admin/usuarios/logout', array('class' => 'link-login'))) : ''; ?></div>
    	</div>
    </div>
    <?php if(isset($auth['Usuario']['nome'])):
            echo $layout->menu('admin');
        endIf; ?>
    <div id="content">
		
		<?php
		echo $this->Session->flash();
		echo $this->Session->flash('auth');
		?>

        <?php echo $content_for_layout; ?>

    </div>
   
</div>
<?php  echo $this->element('sql_dump');  ?>
</body>
</html>
