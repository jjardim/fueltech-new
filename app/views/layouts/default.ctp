<?php
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php
            if (isset($seo_title)):
                $title_site = $seo_title;
                echo $seo_title;
            else:
                ?>
                <?php
                if (isset($title_for_layout_produto)) {
                    $title_site = $title_for_layout_produto;
                    echo $title_for_layout_produto;
                } else {
                    if ($this->params['controller'] != "home") {
                        $title_site = $title_for_layout . ' - ' . Configure::read('Loja.titulo');
                        echo $title_for_layout . ' - ' . Configure::read('Loja.titulo');
                    } else {
                        $title_site = Configure::read('Loja.titulo');
                        echo Configure::read('Loja.titulo');
                    }
                }
                ?>
        <?php endIf; ?>
        </title>

        <?php if (isset($seo_meta_description)): ?>
            <meta name="description" content="<?php echo $seo_meta_description; ?>" />
            <meta property="og:description" content="<?php echo $seo_meta_description; ?>"/>
        <?php else: ?>
            <meta name="description" content="<?php echo Configure::read('Loja.seo_meta_description'); ?>" />
            <meta property="og:description" content="<?php echo Configure::read('Loja.seo_meta_description'); ?>"/>
<?php endIf; ?>
        <meta name="robots" content="index,follow" />
        <meta property="og:image" content="<?php e($this->Html->Url("/img/site/logo_face.png", true)); ?>"/>
        <meta property="og:site_name" content="<?php echo $title_site; ?>"/>
		<meta name="google-site-verification" content="rWgt5Om-e8mbZ8_e_IJSpnTWT5q2C5Gk7TpBpz2Tqa4" />

        <?php if (Configure::read('Reweb.webmaster_id') != ""): ?>
            <meta name="google-site-verification" content="<?php echo Configure::read('Reweb.webmaster_id'); ?>" />
            <?php endIf; ?>

        <?php if (isset($seo_meta_keywords)): ?>
            <meta name="keywords" content="<?php echo $seo_meta_keywords; ?>" />
        <?php else: ?>
            <meta name="keywords" content="<?php echo Configure::read('Loja.seo_meta_keywords'); ?>" />
        <?php endIf; ?>

        <?php echo $this->element('common/js_path'); ?>
        <?php
        echo $this->Html->meta('icon');
        echo $this->Javascript->link('common/jquery.js');
        echo $this->Javascript->link('common/jquery-ui-1.8.16.custom.min.js');
        echo $this->Javascript->link('common/use_default.js');
        echo $this->Javascript->link('common/jquery.slideshow');
        echo $this->Javascript->link('common/jcarousel/jquery.jcarousel.min.js');
        echo $this->Javascript->link('site/common/index.js');
        echo $this->Javascript->link('site/common/https');
        if (isset($jquery_map_plugin) && $jquery_map_plugin === true) {
            echo $this->Javascript->link('common/jquery.imagemapster.js');
        }
        echo $this->Html->css('site/style.css');
        ?>
        <!--[if lte IE 7]>
        <?php
        echo $this->Html->css('site/ie7.css');
        ?>
        <![endif]--> 
        <!--[if lte IE 8]>
        <?php
        echo $this->Html->css('site/ie8.css');
        ?>
        <![endif]--> 
        <!--[if lte IE 9]>
        <?php
        echo $this->Html->css('site/ie9.css');
        ?>
        <![endif]--> 

        <?php
        echo $scripts_for_layout;
        ?>
    </head>

    <?php
    $carrinho['action'][] = 'login';
    $carrinho['action'][] = 'entrega';
    $carrinho['action'][] = 'forma_pagamento';
    $carrinho['action'][] = 'finalizacao';
    ?>

    <body <?php echo (in_array($this->params['action'], $carrinho['action'])) ? "class='inner-bg'" : "" ?>>

		<div class="container-info" style="display:none;z-index: 999;position: fixed;width: 100%;height: 100%;background: url('<?php echo $this->Html->Url('/img/site/img-modal.png'); ?>');">
			<div class='info-upload' style='position: absolute;border:1px solid #DDD;padding: 10px;font-size: 14px;color:#FFF;font-weight: bold;left:50%;width: 300px;margin-left: -150px;top:50%;'></div>
		</div>
		
        <?php
        echo $this->Session->flash();
        if (isset($referer) && $referer == '/login') {
            echo $this->Session->flash('auth');
        }
        ?>

        <!-- start box -->
        <div class="box">

            <!-- start header -->
            <?php
            if (!in_array($this->params['action'], $carrinho['action'])):
                echo $this->element('site/header');
            endIf;
            ?>
            <!-- end header -->	

            <?php if ($this->params['controller'] == "home"): ?>

                <!-- start header -->
                <?php
                if (isset($banners_topo) && is_array($banners_topo) && count($banners_topo) > 0) {
                    echo $this->element('site/banner_top', array("banner_top" => $banners_topo));
                }
                ?>
                <!-- end header -->	

    <?php endIf; ?>

            <div class="clear"></div>

            <!-- start container -->
            <div id="container">

<?php if ($this->params['controller'] == "home" && count($ultimas_noticias) > 0): ?>

                    <!-- start productcol -->
                    <div class="productcol">
                        <h2 class="title" style="border: 0px!important;">
                            <a href="<?php echo $this->Html->Url("/noticias"); ?>" title="<?php echo __("Ver todas novidades"); ?>" class="link orange"><?php echo __("Ver todas novidades"); ?></a>
                            <!--a href="#" title="RSS Feed" class="rss orange">RSS Feed</a><?php echo $this->String->formata_estilo_titulo(__("ÚLTIMAS NOVIDADES", true), 2); ?></span-->
                        </h2>
                        <!-- start product-list -->
                        <div class="product-list" style="float: left; margin-bottom: 15px;">
                            <ul>
                                <?php
                                $cont = 1;
                                foreach ($ultimas_noticias as $noticia):
                                    $img = ( isset($noticia['Noticia']['thumb_filename']) ) ? $noticia['Noticia']['thumb_dir'] . "/" . $noticia['Noticia']['thumb_filename'] : "uploads/produto_imagem/filename/sem_imagem.jpg";
                                    ?>

                                    <li class="<?php echo ($cont >= 3) ? 'last' : '' ?>">
                                        <h3><a href="<?php echo $this->Html->Url("/noticias/" . low(Inflector::slug($noticia['Noticia']['titulo'], '-')) . '-' . 'news-' . $noticia['Noticia']['id']); ?>.html" title="<?php echo $noticia['Noticia']['titulo']; ?>"><?php echo $noticia['Noticia']['titulo']; ?></a></h3>
                                        <span><a href="<?php echo $this->Html->Url("/noticias/" . low(Inflector::slug($noticia['Noticia']['titulo'], '-')) . '-' . 'news-' . $noticia['Noticia']['id']); ?>.html" title="<?php echo $noticia['Noticia']['titulo']; ?>"><?php echo $this->String->formata_intervalo_data($noticia['Noticia']['created']); ?></a></span>
                                        <div class="imgblock">
                                            <a href="<?php echo $this->Html->Url("/noticias/" . low(Inflector::slug($noticia['Noticia']['titulo'], '-')) . '-' . 'news-' . $noticia['Noticia']['id']); ?>.html" title="<?php echo $noticia['Noticia']['titulo']; ?>"><?php echo $image->resize($img, 280, 105, true, array('alt' => $noticia['Noticia']['titulo'])); ?></a>
                                        </div>
                                        <p><a href="<?php echo $this->Html->Url("/noticias/" . low(Inflector::slug($noticia['Noticia']['titulo'], '-')) . '-' . 'news-' . $noticia['Noticia']['id']); ?>.html" title="<?php echo $noticia['Noticia']['titulo']; ?>"><?php echo $noticia['Noticia']['descricao']; ?></a></p>
                                    </li>

                                    <?php
                                    $cont++;
                                    if ($cont % 4 == 0):
									$cont = 1;
                                        ?>
                                    </ul>
                                    <div class="clear"></div>
                                </div>
                                <!-- end product-list -->
                                <!-- start product-list -->
                                <div class="product-list last">
                                    <ul>
            <?php endIf; ?>
        <?php endForeach; ?>
                            </ul>
                            <div class="clear"></div>
                        </div>
                        <!-- end product-list -->
                    </div>
                    <!-- end productcol -->

                <?php endif; ?>

<?php if (!in_array($this->params['action'], $carrinho['action'])): ?>
    <?php if (isset($breadcrumb)): ?>
                        <!-- start breadcrumb -->
                        <div class="breadcrumb orange">
                            <ul class="right">
                                <li><a href="javascript:void(0);" title="<?php echo __("Voltar"); ?>" class="btn_voltar"><?php echo __("Voltar"); ?></a></li>
                            </ul>
                            <ul>
                                <li><a href="<?php e($this->Html->Url("/")); ?>" title="<?php echo __("Página Inicial"); ?>"><?php echo __("Página Inicial"); ?></a></li>
                                <?php
                                $cont = count($breadcrumb);
                                foreach ($breadcrumb as $key => $breadcrumb):
                                    if (($key + 1) != $cont):
                                        ?>
                                        <?php if ($breadcrumb['url'] != "javascript:void(0);"): ?>
                                            <li><a href="<?php e($this->Html->Url("/") . $breadcrumb['url']); ?>" title="<?php e($breadcrumb['nome']); ?>"> <?php e($breadcrumb['nome']); ?></a></li>
                                        <?php else: ?>
                                            <li><a href="javascript:void(0);" title="<?php e($breadcrumb['nome']); ?>"> <?php e($breadcrumb['nome']); ?></a></li>
                                        <?php endif; ?>
                                        <?php
                                    else:
                                        ?>
                                        <li><?php e($breadcrumb['nome']); ?></li>
                                    <?php
                                    endIf;
                                endforeach;
                                ?>
                            </ul>
                            <div class="clear"></div>
                        </div>
                        <!-- end breadcrumb -->
                    <?php endif; ?>	
                    <?php endIf; ?>

		<?php
		
		$exibir_page_container = (($this->params['controller'] == 'vitrines' || $this->params['controller'] == 'categorias' && $this->params['action'] == 'index')
					    || ($this->params['controller'] == 'busca' && $this->params['action'] == 'index'))
					    ? TRUE : FALSE; ?>

		<?php if ($exibir_page_container) : ?><div class="cat-container"><?php endif; ?>

                <!-- start left -->
                <?php
                //deixar só o necessario depiois
                if (($this->params['controller'] != "produto" && $this->params['action'] != "detalhe") &&
                        ($this->params['controller'] != "usuarios" && $this->params['action'] != "edit") &&
                        ($this->params['controller'] != "carrinho") &&
                        $this->params['action'] != "login" &&
                        $this->params['controller'] != "usuario_enderecos" &&
                        $this->params['controller'] != "categorias" &&
                        $this->params['controller'] != "busca" &&
                        $this->params['controller'] != "vitrines"
                ):

                    echo $this->element('site/left');

                endIf;
                ?>
                <!-- end left -->

                <?php echo $content_for_layout; ?>
		
		<?php if ($exibir_page_container) : ?></div><?php endif; ?>

                <div class="clear"></div>

                    <?php if (!in_array($this->params['action'], $carrinho['action'])): ?>
                    <!-- start history-block -->
                    <div class="history-block">
                        <h3><?php echo __("HISTÓRICO DE NAVEGAÇÃO"); ?></h3>
                        <?php
                        //historico de navegação
                        $historico_geral = $this->Session->read("Historico.navegacao");
                        ?>
                        <ul>
                            <li>
                                <h2><?php echo __("PALAVRAS PESQUISADAS"); ?></h2>
                                <?php
                                if (isset($historico_geral['Buscas'])):
                                    $historcio_buscas = array_reverse($historico_geral['Buscas']);
                                    $i = 1;
                                    foreach ($historcio_buscas as $busca):
                                        if ($i <= 5):
                                            ?>
                                            <p><a href="<?php echo $this->Html->Url("/busca/index/" . $busca['termo']); ?>"><?php echo $busca['termo']; ?></a></p>
                                            <?php
                                        endIf;
                                        $i++;
                                    endForeach;
                                endIf;
                                ?>
                            </li>
                            <li>
                                <h2><?php echo __("SEÇÕES VISITADAS"); ?></h2>
                                <?php
                                if (isset($historico_geral['Paginas'])):
                                    $historcio_paginas = array_reverse($historico_geral['Paginas']);
                                    $x = 1;
                                    foreach ($historcio_paginas as $pagina):
                                        if ($x <= 5):
                                            ?>
                                            <p><a href="<?php echo $this->Html->Url("/" . $pagina['url']); ?>"><?php echo $pagina['nome']; ?></a></p>
                                            <?php
                                        endIf;
                                        $x++;
                                    endForeach;
                                endIf;
                                ?>
                            </li>
                            <li>
                                <h2><?php echo __("PRODUTOS VISITADOS"); ?></h2>
                                <?php
                                if (isset($historico_geral['Produtos'])):
                                    $historcio_produtos = array_reverse($historico_geral['Produtos']);
                                    $z = 1;
                                    foreach ($historcio_produtos as $produto):
                                        if ($z <= 5):
                                            ?>
                                            <p><a href="<?php echo $this->Html->Url("/" . low(Inflector::slug($produto['nome'], '-')) . '-' . 'prod-' . $produto['id']) ?>.html" title="<?php echo $produto['nome']; ?>"><?php echo $produto['nome']; ?></a></p>
                                            <?php
                                        endIf;
                                        $z++;
                                    endForeach;
                                endIf;
                                ?>
                            </li>
                        </ul>
                        <div class="clear"></div>
                    </div>
                    <!-- end history-block -->
                    <div class="clear"></div>
    <?php endIf; ?>

            </div>
            <!-- end container -->

        </div>
        <!-- end box -->

        <!-- start footer -->
        <?php
        if (!in_array($this->params['action'], $carrinho['action'])):
            echo $this->element('site/footer');
        else:
            echo $this->element('site/footer-half');
        endIf;
        ?>
        <!-- end footer -->

<?php echo $this->element('sql_dump'); ?>
<?php if (Configure::read('Reweb.analytics_id') != ""): ?>  
            <script type="text/javascript">
                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', '<?php echo Configure::read('Reweb.analytics_id'); ?>']);
                _gaq.push(['_trackPageview']);

                (function() {
                    var ga = document.createElement('script');
                    ga.type = 'text/javascript';
                    ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(ga, s);
                })();

            </script>
    <?php endIf; ?>

	<script type="text/javascript">
		setTimeout(function(){var a=document.createElement("script");
		var b=document.getElementsByTagName("script")[0];
		a.src=document.location.protocol+"//cdn.reweb-corp.com/hm.0.0.1.js?"+Math.floor(new Date().getTime()/3600000);
		a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
	</script>

    </body>
</html>