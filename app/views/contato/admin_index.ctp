<div class="index">
	<h2 class="left"><?php __('Contatos');?></h2>
	
    <?php echo $form->create('Contato', array('action'=>'/index','class'=>'formBusca'));?>
		<fieldset>
			<div class="left">
				<?php echo $form->input('Filter.filtro',array('div'=>false,'label'=>'Nome / Cidade / Estado:','class'=>'produtosFiltro'));  ?>
			</div>
			<?php echo $form->end('Busca'); ?>
		</fieldset>
	<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo $this->Paginator->sort('id');?></th>
		<th><?php echo $this->Paginator->sort('nome');?></th>
		<th><?php echo $this->Paginator->sort('email');?></th>
		<th><?php echo $this->Paginator->sort('telefone');?></th>
		<th><?php echo $this->Paginator->sort('cidade');?></th>
		<th><?php echo $this->Paginator->sort('estado');?></th>
		<th><?php echo $this->Paginator->sort('lido');?></th>
        <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
        <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>
		<th class="actions">Ações</th>
	</tr>
	<?php
	$i = 0;
	foreach ($contatos as $contato): 
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td align="center"><?php echo $contato['Contato']['id']; ?>&nbsp;</td>
		<td><?php echo $contato['Contato']['nome']; ?>&nbsp;</td>
		<td><?php echo $contato['Contato']['email']; ?>&nbsp;</td>
		<td><?php echo $contato['Contato']['telefone']; ?>&nbsp;</td>
		<td><?php echo $contato['Contato']['cidade']; ?>&nbsp;</td>
		<td><?php echo $contato['Contato']['estado']; ?>&nbsp;</td>
		<td><?php echo ($contato['Contato']['lido'])?'Lido':'Não Lido'; ?>&nbsp;</td>
		<td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s',$contato['Contato']['created']); ?>&nbsp;</td>
		<td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s',$contato['Contato']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php 
			if(!$contato['Contato']['lido']){
				echo $this->Html->link(__('Marcar como lido', true), array('action' => 'lido', $contato['Contato']['id'])); 
			}else{
				echo $this->Html->link(__('Marcar como não lido', true), array('action' => 'nlido', $contato['Contato']['id'])); 
			}
			?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	 <p><?php echo $this->Paginator->counter(array('format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true))); ?></p>
        

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('Próximo', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
