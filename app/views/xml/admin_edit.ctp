<?php
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('admin/xml/index.js',false);
?>
<div class="index">
    <?php echo $this->Form->create('Xml'); ?>
    <fieldset>
        <legend><?php printf(__('Editar %s', true), __('XML', true)); ?></legend>
        <?php
		echo $this->Form->input('status', array('type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
		echo $this->Form->input('xml_tipo_id', array('label' => 'Tipo de Xml', 'id' => 'xml_tipo_id', 'class'=>'w312','options' => $xml_tipos));
		echo $this->Form->input('nome', array('class'=>'w312'));
		echo '<br /><br />';
		echo $this->Form->input('template_cabecalho', array('class'=>'w312 textarea_w100'));
		echo $this->Form->input('template_centro', array('class'=>'w312 textarea_w100'));
		echo $this->Form->input('template_rodape', array('class'=>'w312 textarea_w100'));
		
		echo $this->Form->input('Buscar', array('class'=>'w312','after' => $this->Form->Button('OK', array('id' => 'buscar-produtos'))));
		$botoes = $this->Html->link('Adicionar', 'javascript:;', array('id' => 'add'));
		$botoes .= $this->Html->link('Remover', 'javascript:;', array('id' => 'rm'));
		echo $this->Form->input('Selecionar', array('id' => 'buscados', 'type' => 'select', 'multiple' => true, 'after' => $botoes));
		echo $this->Form->input('produtos_ids', array('id' => 'selecionados','type' => 'select', 'multiple' => true));
		
		echo $this->Form->end(__('Salvar', true));
        ?>
    </fieldset>
</div>