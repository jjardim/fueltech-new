<?php
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('admin/xml/index.js',false);
?>
<div class="index">
<?php echo $this->Form->create('Xml', array('id' => 'Xml', 'class' => 'form3', 'url' => '/admin/xml/add')); ?>
	<fieldset>
 		<legend><?php printf(__('Criar %s', true), __('XML', true)); ?></legend>
		
		<div class="left">
			<?php echo $this->Form->input('xml_tipo_id', array('label' => 'Tipo de Xml', 'id' => 'xml_tipo_id', 'class'=>'w312','options' => $xml_tipos)); ?>
			<?php echo $this->Form->input('nome', array('label'=>'Nome','class'=>'w312')); ?>
			<?php echo $this->Html->image('/img/site/zoomloader.gif', array('class' => 'loading', 'style' => 'display:none', 'alt' => 'Carregando', 'title' => 'Carregando')); ?>
		</div>
		<div class="left" style="width: 500px">
			<?php echo $this->Form->input('template_variaveis', array('label'=>'Variáveis disponíveis','id'=>'variaveis_disponiveis','class'=>'w312 textarea_w100','rows' => 5, 'type' => 'textarea'));	?>
		</div>		
		
		<legend>Template</legend>		
		<div class="clear">
			<?php echo $this->Form->input('template_cabecalho', array('label'=>'Cabeçalho','class'=>'w312 textarea_w100')); ?>
			<?php echo $this->Form->input('template_centro', array('label'=>'Conteúdo','class'=>'w312 textarea_w100','rows'=>13)); ?>
			<?php echo $this->Form->input('template_rodape', array('label'=>'Rodapé','class'=>'w312 textarea_w100')); ?>
		</div>
		
		<legend>Produtos</legend>
		<div class="left clear">
			<?php echo $this->Form->input('Buscar', array('label'=>'Buscar produto','class'=>'w312','after' => $this->Form->Button('OK', array('id' => 'buscar-produtos')))); ?>
		</div>
		<div class="left clear">
			<?php 
				$botoes = $this->Html->link('Adicionar', 'javascript:;', array('id' => 'add'));
				$botoes .= $this->Html->link('Remover', 'javascript:;', array('id' => 'rm'));
				echo $this->Form->input('Selecionar', array('id' => 'buscados', 'type' => 'select', 'multiple' => true, 'after' => $botoes));
			?>
		</div>
		<div class="left clear selectgrande">
			<?php
				if(empty($this->data['Xml']['produtos_ids']))
					$this->data['Xml']['produtos_ids'] = '';
					echo $this->Form->input('produtos_ids', array('options'=>$this->data['Xml']['produtos_ids'],'label'=>'Produtos Selecionados','id' => 'selecionados','type' => 'select', 'multiple' => true)); ?>
		</div>
	</fieldset>
<?php echo $this->Form->end(__('Criar', true)); ?>
</div>