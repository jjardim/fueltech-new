<?php
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('admin/assistencias/index.js',false);
?>
<div class="index">
    <?php echo $this->Form->create('VideoTipo'); ?>
    <fieldset>
        <legend><?php printf(__('Editar %s', true), __('Video Tipo', true)); ?></legend>
        <?php
		echo $this->Form->input('status', array('type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
		echo $this->Form->input('nome', array('class'=>'w312'));
		echo $this->Form->end(__('Salvar', true));
        ?>
    </fieldset>
</div>