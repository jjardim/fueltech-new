<?php
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('admin/parceiros/index.js',false);
?>
<div class="index">
<?php echo $this->Form->create('Revendedor',array('type' => 'file', 'action'=>'add'));?>
	<fieldset>
 		<legend><?php printf(__('Adicionar %s', true), __('Revendedor', true)); ?></legend>
		<div class="left clear">
			<?php echo $this->Form->input('status', array('default'=>true,'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo'))); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('language',array('default'=>$session->read('linguagem_default'),'type'=>'select','options'=>$idiomas)); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('nome', array('class'=>'w312')); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('cidade', array('class'=>'w312')); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('estado',array('div'=>false,'options' => $this->Estados->estadosBrasileiros(),'class'=>'w312')); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('pais', array('class'=>'w312', 'default' => 'Brasil')); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('localidade_label', array('class'=>'w312')); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('descricao_resumida', array('type'=> 'textArea','class'=>'w312 mceEditor h300 w500')); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('descricao_completa', array('class'=>'w312 mceEditor h300 w500')); ?>
		</div>
		<div class="left clear">
			<legend>Imagem</legend>
			<?php
				echo $this->Form->input('Revendedor.thumb_filename', array('type' => 'file'));
				echo $this->Form->input('Revendedor.thumb_dir', array('type' => 'hidden'));
				echo $this->Form->input('Revendedor.thumb_mimetype', array('type' => 'hidden'));
				echo $this->Form->input('Revendedor.thumb_filesize', array('type' => 'hidden'));		
			?>
		</div>
		<?php
			echo $this->Form->end(__('Inserir', true));
		?>
		</fieldset>
</div>