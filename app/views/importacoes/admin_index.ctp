<div class="pedidos index">
	<h2><?php __('Importação'); ?></h2>
    <?php echo $form->create(null,array('type' => 'file'));?>
		<fieldset>
			<?php 
			echo $form->input('file',array('label'=>'Arquivo','type'=>'file'));
			?>
		</fieldset>
	<?php echo $form->end('Enviar'); ?>
	<table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
             <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
        </tr>
        <?php
        $i = 0;
        foreach ($importacoes as $importacao):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
        ?>
            <tr <?php echo $class; ?>>
                <td><?php echo $importacao['Importacao']['id']; ?>&nbsp;</td>
                <td><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s',$importacao['Importacao']['created']); ?>&nbsp;</td>
            
        </tr>
        <?php endforeach; ?>
            </table>
            <p>
        <?php
                echo $this->Paginator->counter(array(
                    'format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true)
                ));
        ?>	</p>

            <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array(), null, array('class' => 'disabled')); ?>
        	 | 	<?php echo $this->Paginator->numbers(); ?>
                |
        <?php echo $this->Paginator->next(__('Próximo', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
            </div>
        </div>
