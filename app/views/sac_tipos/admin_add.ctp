<?php
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('admin/assistencias/index.js',false);
?>
<div class="index">
<?php echo $this->Form->create('SacTipo');?>
	<fieldset>
 		<legend><?php printf(__('Adicionar %s', true), __('Tipo Sac', true)); ?></legend>
	<?php
		echo $this->Form->input('status', array('default'=>true,'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
		echo $this->Form->input('nome',array('class'=>'w312'));
		echo $this->Form->input('codigo',array('class'=>'w312'));
		echo $this->Form->end(__('Inserir', true));
	?>
	</fieldset>
</div>