<?php 
echo $javascript->link('common/jquery.meio_mask.js',false);
echo $javascript->link('admin/frete_tipos/crud.js',false);
?>
<div class="index">
<?php echo $this->Form->create('Frete');?>
	<fieldset>
 		<legend><?php printf(__('Adidionar %s', true), __('Frete', true)); ?></legend>
	<?php
        echo $this->Form->input('status', array('default'=>true,'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
        echo $this->Form->input('cep_inicial',array('label'=>'Cep Inicial','class'=>'mask-cep'));
        echo $this->Form->input('cep_final',array('label'=>'Cep Final','class'=>'mask-cep'));
        echo $this->Form->input('peso',array('label'=>'Peso em gramas.','class'=>'mask-numerico'));
        echo $this->Form->input('preco',array('label'=>'Preço','class'=>'mask-moeda'));
        echo $this->Form->input('prazo',array('label'=>'Prazo','class'=>'mask-numerico'));                
        echo $this->Form->input('descricao',array('label'=>'Descrição')); 
        echo $this->Form->end(__('Inserir', true));               
	?>
	</fieldset>
</div>
