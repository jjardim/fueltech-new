<?php
//echo $javascript->link('admin/galerias/index.js',false);
?>
<div class="index">
	<?php echo $this->Form->create('GaleriaTipo', array('type' => 'file'));?>
		<fieldset>
			<legend><?php printf(__('Adicionar %s', true), __('GaleriaTipo', true)); ?></legend>
			<?php
				echo $this->Form->input('status', array('default'=>true,'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
				echo $this->Form->input('nome', array('class'=>'w312'));
			?>
		</fieldset>
	<?php echo $this->Form->end(__('Inserir', true)); ?>
</div>