<?php
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('admin/assistencias/index.js',false);
?>
<div class="index">
    <?php echo $this->Form->create('GaleriaTipo'); ?>
    <fieldset>
        <legend><?php printf(__('Editar %s', true), __('GaleriaTipo', true)); ?></legend>
        <?php
			echo $this->Form->input('id');
			echo $this->Form->input('status', array('type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
			echo $this->Form->input('nome', array('class'=>'w312'));
		?>
	</fieldset>
	<?php echo $this->Form->end(__('Salvar', true)); ?>
</div>