<div class="index ">
    <h2><?php __('Urls'); ?></h2>
	<div class="btAddProduto">
    	<?php echo $this->Html->link(__('[+] Adicionar redirecionamento', true), array('action' => 'add')); ?>
    </div>    
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('url_antiga'); ?></th>
            <th><?php echo $this->Paginator->sort('url_nova'); ?></th>
            <th><?php echo $this->Paginator->sort('header'); ?></th>
            
            <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>

            <th class="actions"><?php __('Ações'); ?></th>
        </tr>
        <?php
        $i = 0;
        foreach ($urls as $url):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
            <tr<?php echo $class; ?>>
                <td align="center"><?php echo $url['Url']['id']; ?>&nbsp;</td>
                <td><?php echo $url['Url']['url_antiga']; ?>&nbsp;</td>
                <td><?php echo $url['Url']['url_nova']; ?>&nbsp;</td>
                <td><?php echo $url['Url']['header']; ?>&nbsp;</td>
                
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $url['Url']['created']); ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $url['Url']['modified']); ?>&nbsp;</td>

                <td class="actions">
                    <?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $url['Url']['id'])); ?>
					<?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $url['Url']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $url['Url']['id'])); ?>
				</td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, mostrando %current% registros de %count%, começando no registro %start% e terminando no %end%', true)
        ));
        ?>	</p>

    <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('anterior', true), array(), null, array('class' => 'disabled')); ?>
	 | 	<?php echo $this->Paginator->numbers(); ?>
        |
        <?php echo $this->Paginator->next(__('próxima', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
    </div>
    
</div>
