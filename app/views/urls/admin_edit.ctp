<div class="index">
<?php echo $this->Form->create('Url');?>
	<fieldset>
 		<legend><?php printf(__('Editar %s', true), __('Url', true)); ?></legend>
 		<div class="clear">
			<?php echo $this->Form->input('status', array('default'=>true,'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo'))); ?>
    	</div>
		<div class="clear">
			<?php echo $this->Form->input('url_antiga',array('class'=>'w312')); ?>
    	</div>
    	<div class="clear">
			<?php echo $this->Form->input('id'); ?>
			<?php echo $this->Form->input('url_nova',array('class'=>'w312')); ?>
    	</div>
		<div class="clear">
			<?php echo $this->Form->input('header',array('options'=>array('204'=>'204','301'=>'301','302'=>'302','303'=>'303','307'=>'307','403'=>'403','404'=>'404','503'=>'503'),'class'=>'w312')); ?>
    	</div>
    <?php  echo $this->Form->end(__('Salvar', true)); ?>
	</fieldset>
</div>
