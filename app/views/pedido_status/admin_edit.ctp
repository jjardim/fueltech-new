<?php echo $html->css('common/colourPicker', null, array('inline' => false)); 
echo $javascript->link('common/colourPicker.js',false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('admin/pedido_status/crud.js',false);
?>
<div class="index">
    <?php echo $this->Form->create('PedidoStatus',array('url'=>'/admin/pedido_status/edit/'.$this->params['pass'][0])); ?>
    <fieldset>
        <legend><?php printf(__('Editar %s', true), __('Pedido Status', true)); ?></legend>
        <?php
        echo $this->Form->input('id');
        echo $this->Form->input('status', array('disabled'=>$this->data['PedidoStatus']['disabled'],'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
		echo $this->Form->input('restaurar_template',array('onclick'=>"javascript:if(confirm('Você tem certeza? esta acão irá apagar o Template de Email Atual')){return true}else{return false;};",'value'=>true,'type'=>'checkbox','label'=>'Restaurar configurações (Template de E-mail)'));
		echo $this->Form->input('debitar_produtos',array('disabled'=>$this->data['PedidoStatus']['disabled'],'type'=>'checkbox'));
        echo $this->Form->input('desalocar_produtos',array('disabled'=>$this->data['PedidoStatus']['disabled'],'type'=>'checkbox'));
        echo $this->Form->input('enviar_email',array('disabled'=>$this->data['PedidoStatus']['disabled'],'type'=>'checkbox'));
        echo $this->Form->input('nome',array('class'=>'w312','disabled'=>$this->data['PedidoStatus']['disabled']));    ?>    
       <div class="input select picker">
		<?php 
			$options  = array('ffffff'=>'#ffffff','ffccc9'=>'#ffccc9','ffce93'=>'#ffce93','fffc9e'=>'#fffc9e','ffffc7'=>'#ffffc7','9aff99'=>'#9aff99','96fffb'=>'#96fffb','cdffff'=>'#cdffff','cbcefb'=>'#cbcefb','cfcfcf'=>'#cfcfcf','fd6864'=>'#fd6864','fe996b'=>'#fe996b','fffe65'=>'#fffe65','fcff2f'=>'#fcff2f','67fd9a'=>'#67fd9a','38fff8'=>'#38fff8','68fdff'=>'#68fdff','9698ed'=>'#9698ed','c0c0c0'=>'#c0c0c0','fe0000'=>'#fe0000','f8a102'=>'#f8a102','ffcc67'=>'#ffcc67','f8ff00'=>'#f8ff00','34ff34'=>'#34ff34','68cbd0'=>'#68cbd0','34cdf9'=>'#34cdf9','6665cd'=>'#6665cd','9b9b9b'=>'#9b9b9b','cb0000'=>'#cb0000','f56b00'=>'#f56b00','ffcb2f'=>'#ffcb2f','ffc702'=>'#ffc702','32cb00'=>'#32cb00','00d2cb'=>'#00d2cb','3166ff'=>'#3166ff','6434fc'=>'#6434fc','656565'=>'#656565','9a0000'=>'#9a0000','ce6301'=>'#ce6301','cd9934'=>'#cd9934','999903'=>'#999903','009901'=>'#009901','329a9d'=>'#329a9d','3531ff'=>'#3531ff','6200c9'=>'#6200c9','343434'=>'#343434','680100'=>'#680100','963400'=>'#963400','986536'=>'#986536','646809'=>'#646809','036400'=>'#036400','34696d'=>'#34696d','00009b'=>'#00009b','303498'=>'#303498','000000'=>'#000000','330001'=>'#330001','643403'=>'#643403','663234'=>'#663234','343300'=>'#343300','013300'=>'#013300','003532'=>'#003532','010066'=>'#010066','340096'=>'#340096');
			echo $this->Form->input('cor',array('div'=>false,'options'=>$options,'class'=>'colourPicker w312'));
		?>
		</div>
		<?php
        echo $this->Form->input('template_email', array('class' => 'mceEditor Settings ', 'type' => 'textarea', 'label' => 'Texto','after'=>'Variáveis {USUARIO_NOME},{PEDIDO_ID},{PEDIDO_DATA},{PEDIDO_STATUS},{LOJA_NOME}'));
        echo $this->Form->end(__('Salvar', true));
        ?>
    </fieldset>
</div>
