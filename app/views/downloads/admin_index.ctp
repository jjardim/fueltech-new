<div class="index ">
    <h2><?php __('Download'); ?></h2>
	<div class="btAddProduto">
    	<?php echo $this->Html->link(__('[+] Adicionar Download', true), array('action' => 'add')); ?>
    </div>
	
	<?php echo $form->create('Download', array('action'=>'/index','class'=>'formBusca'));?>
	<fieldset>
		<div class="left">
            <?php echo $form->input('Filter.download_tipo_id',array('options'=>array(''=>'Selecione')+$download_tipos, 'div'=>false, 'class'=> 'produtosFiltro')); ?>
        </div>
		<div class="left">
			<?php echo $form->input('Filter.filtro',array('div'=>false,'label'=>'Nome / Descrição:','class'=>'produtosFiltro'));  ?>
		</div>
		<div class="submit">
			<input name="submit" type="submit" class="button1" value="Busca" />
		</div>
		<div class="submit">
			<input name="submit" type="submit" class="button1" value="Exportar" />
		</div>
		<div class="submit">
			<a href="<?php e($this->Html->Url('index/filter:clear')) ?>" title="Limpar Filtro" class="button" >Limpar Filtro</a>
		</div>
	</fieldset>
	<?php echo $form->end(); ?>
	
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('download_tipo_id'); ?></th>
			<th><?php echo $this->Paginator->sort('nome'); ?></th>
			<th><?php echo $this->Paginator->sort('url'); ?></th>
            <th><?php echo $this->Paginator->sort('status'); ?></th>   
            <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>

            <th class="actions"><?php __('Ações'); ?></th>
        </tr>
        <?php
        $i = 0;
        foreach ($downloads as $download):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
            <tr<?php echo $class; ?>>
                <td align="center"><?php echo $download['Download']['id']; ?>&nbsp;</td>
				<td><?php echo $download['DownloadTipo']['nome']; ?>&nbsp;</td>
				<td><?php echo $download['Download']['nome']; ?>&nbsp;</td>
				<td><?php echo $this->Html->Url("/downloads/download/", true).$download['Download']['id']."/".low(Inflector::slug($download['Download']['nome'], '-'))."/"; ?>&nbsp;</td>
				<td><?php echo ( $download['Download']['status'] ) ? 'Ativo' : 'Inativo'; ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $download['Download']['created']); ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $download['Download']['modified']); ?>&nbsp;</td>

                <td class="actions">
                    <?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $download['Download']['id'])); ?>
					<?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $download['Download']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $download['Download']['id'])); ?>
				</td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, mostrando %current% registros de %count%, começando no registro %start% e terminando no %end%', true)
        ));
        ?>	</p>

    <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('anterior', true), array(), null, array('class' => 'disabled')); ?>
	 | 	<?php echo $this->Paginator->numbers(); ?>
        |
        <?php echo $this->Paginator->next(__('próxima', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
    </div>
    
</div>
