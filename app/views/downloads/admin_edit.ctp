<?php
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js', false);
echo $javascript->link('admin/downloads/index.js', false);
?>
<div class="index">
    <?php echo $this->Form->create('Download', array('type' => 'file', 'url' => '/admin/downloads/edit/' . $this->params['pass'][0])); ?>
    <fieldset>
        <legend><?php printf(__('Editar %s', true), __('Download', true)); ?></legend>

        <div class="left clear">
            <?php echo $this->Form->input('status', array('default' => true, 'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo'))); ?>
        </div>
        <div class="left clear">
            <?php echo $this->Form->input('language', array('default' => $session->read('linguagem_default'), 'type' => 'select', 'options' => $idiomas)); ?>
        </div>
        <div class="left clear">
            <?php echo $this->Form->input('download_tipo_id', array('type' => 'select', 'options' => $download_tipos)); ?>
        </div>
        <div class="left clear">
            <?php echo $this->Form->input('destaque', array('default' => false, 'type' => 'radio', 'options' => array(true => 'Sim', false => 'Não'))); ?>
        </div>
        <div class="left clear">
            <?php echo $this->Form->input('nome', array('class' => 'w312')); ?>
        </div>
        <div class="left clear">
            <?php echo $this->Form->input('descricao', array('class' => 'w312')); ?>
        </div>
        <div class="left clear">
            <?php echo $this->Form->input('versao', array('class' => 'w312')); ?>
        </div>
        <div class="left clear">
            <?php echo $this->Form->input('ordem_versao', array('class' => 'w312')); ?>
        </div>        
        <div class="left clear">		
            <?php echo $this->Form->input('ordem', array('class' => 'w312')); ?>
        </div>
        <div class="left clear">
            <?php echo $this->Form->input('upload', array('default' => true, 'type' => 'radio', 'options' => array(true => 'Upload de arquivo', false => 'Nome de Arquivo'))); ?>
        </div>
        <div class="left clear">
            <?php echo $this->Form->input('nome_arquivo', array('class' => 'w312')); ?>
            <?php echo $this->Form->input('tamanho_arquivo', array('label' => 'Tamanho Arquivo (MB)', 'class' => 'w312 mask-moeda')); ?>
        </div>
        <div class="left clear">
            <?php
            echo $this->Form->input('Download.filename', array('type' => 'file'));
            echo $this->Form->input('Download.dir', array('type' => 'hidden'));
            echo $this->Form->input('Download.mimetype', array('type' => 'hidden'));
            echo $this->Form->input('Download.filesize', array('type' => 'hidden'));
            ?>
        </div>
        <div class="left clear">
            <?php echo $this->Form->input('thumb', array('default' => false, 'type' => 'radio', 'options' => array(true => 'Com thumb', false => 'Sem thumb'))); ?>
        </div>
        <div class="left clear">
            <?php
            $img = ( isset($this->data['Download']['thumb_filename']) ) ? $this->data['Download']['thumb_dir'] . '/' . $this->data['Download']['thumb_filename'] : "uploads/produto_imagem/thumb/sem_imagem.jpg";
            echo $this->Form->input('Download.thumb_filename', array('type' => 'file'));
            echo $image->resize($img, 80, 80);
            echo $this->Form->input('Download.thumb_dir', array('type' => 'hidden'));
            echo $this->Form->input('Download.thumb_mimetype', array('type' => 'hidden'));
            echo $this->Form->input('Download.thumb_filesize', array('type' => 'hidden'));
            if (isset($this->data['Download']['thumb_filename'])) {
                echo $form->input('Download.thumb_remove', array('type' => 'checkbox'));
            }
            ?>
        </div>
        <?php echo $this->Form->end(__('Salvar', true)); ?>
    </fieldset>
</div>