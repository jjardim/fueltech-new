<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "b5c8b4f8-9290-467f-9537-078d59eb7697"});</script>

<!-- start leftcol -->
<div id="leftcol">
	<?php echo $this->element('site/left'); ?>
</div>
<!-- end leftcol -->

<!-- start rightcol -->
<div id="rightcol" style="margin-left: -5px">
	
	<!-- start showcase -->
	<div id="showcase">
		<div class="caption"><h2><?php echo $pagina_atual_nome; ?></h2></div>
		<?php echo $this->Html->image('site/img_showcase2.jpg', array('alt' => 'showcase', 'width' => '719', 'height' => '187'))?>
	</div>
	<!-- end showcase -->
	
	<!-- start youtube -->
	<div class="youtube">
		<iframe width="642" height="391" src="http://www.youtube.com/embed/<?php echo $video_destaque['Video']['video_youtube_id'] ?>" frameborder="0" allowfullscreen></iframe>
		<!-- start record -->
		<div class="record">
			<h3><?php echo $video_destaque['Video']['nome'] ?></h3>
                        <span><?php echo $this->String->formata_intervalo_data($video_destaque['Video']['created']); ?></span>
			<span><?php echo $video_destaque['Video']['descricao']; ?></span>
		</div>
		<!-- end record -->
		<!-- start social -->
		<div class="social">
			<h4><?php echo __("Compartilhe"); ?>:</h4>
			<ul>
				<li><span class='st_orkut' displayText=''></span></li>
				<li><span class='st_twitter' displayText=''></span></li>
				<li><span class='st_facebook' displayText=''></span></li>
				<li><span class='st_email' displayText=''></span></li>
				<li><span class='st_googleplus' displayText=''></span></li>
			</ul>
		</div>
		<!-- end social -->
		<div class="clear"></div>
	</div>
	<!-- end youtube -->
	<!-- start video -->
	<div class="video">
		<h2>Lista de vídeos</h2>
		<ul>
			<?php
				$cont = 0;
				foreach($videos as $video): 
				$img = ( isset($video['Video']['thumb_filename']) ) ? $video['Video']['thumb_dir']."/".$video['Video']['thumb_filename'] : "uploads/produto_imagem/filename/sem_imagem.jpg";
			?>
			<li <?php if($cont == 3) { echo "class='first'"; } ?>>
				<a href="<?php echo $this->Html->Url("/videos/" . low(Inflector::slug($video['Video']['nome'], '-')) . '-' . 'video-' . $video['Video']['id']); ?>.html" title="<?php echo $video['Video']['nome']; ?>">
					<?php echo $image->resize($img, 190, 142, true, array('alt' => $video['Video']['nome'])); ?>
				</a>
				<h3>
					<a href="<?php echo $this->Html->Url("/videos/" . low(Inflector::slug($video['Video']['nome'], '-')) . '-' . 'video-' . $video['Video']['id']); ?>.html" title="<?php echo $video['Video']['nome']; ?>">
						<?php echo $video['Video']['nome']; ?>
					</a>
				</h3>
                                <span><?php echo $this->String->formata_intervalo_data($video_destaque['Video']['created']); ?></span>
				<span>
					<a href="<?php echo $this->Html->Url("/videos/" . low(Inflector::slug($video['Video']['nome'], '-')) . '-' . 'video-' . $video['Video']['id']); ?>.html" title="<?php echo $video['Video']['nome']; ?>">
						<?php echo substr($video['Video']['descricao'],0,100); ?>
					</a>
				</span>
			</li>
			<?php if($cont == 3){ $cont = 0; } 
				$cont++;
				endForeach;
			?>
		</ul>
		<div class="clear"></div>
	</div>
	<!-- end video -->
	<!-- start pagination1st -->
	<div class="pagination1st">
		<ul>
			<?php echo $this->Paginator->numbers(array( 'tag' => 'li', 'separator' => '<li>.</li>' )); ?>
		</ul>
		<div class="clear"></div>
	</div>
	<!-- end pagination1st -->
	<div class="clear"></div>
	
</div>
<!-- end rightcol -->
<div class="clear"></div>