<?php
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('common/jquery-ui-1.8.16.custom.min',false);
echo $this->Html->css('common/ui-lightness/jquery-ui-1.8.16.custom.css');
echo $javascript->link('admin/videos/index.js',false);
?>
<div class="index">
    <?php echo $this->Form->create('Video', array('type' => 'file', 'action'=>'edit/'.$this->params['pass'][0])); ?>
    <fieldset>
        <legend><?php printf(__('Editar %s', true), __('Video', true)); ?></legend>
        <?php
			echo $this->Form->input('status', array('default'=>true,'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
			echo $this->Form->input('video_tipo_id', array('label' => 'Tipo de Vídeo', 'id' => 'xml_tipo_id', 'class'=>'w312','options' => $video_tipos));
			echo $this->Form->input('nome', array('class'=>'w312'));
			echo $this->Form->input('descricao', array('class'=>'w312'));
			echo $this->Form->input('url', array('class'=>'w312'));
			echo $this->Form->input('duracao', array('class'=>'w312'));
			echo $this->Form->input('created', array('type'=>'text','class'=>'datePicker w312'));
		?>
		<legend>Thumb</legend>
		<?php
			$img = ( isset($this->data['Video']['thumb_filename']) ) ? $this->data['Video']['thumb_dir'] . '/' . $this->data['Video']['thumb_filename'] : "uploads/produto_imagem/thumb/sem_imagem.jpg";
			echo $this->Form->input('Video.thumb_filename', array('type' => 'file'));
			echo $image->resize($img, 80, 80);		
			echo $this->Form->input('Video.thumb_dir', array('type' => 'hidden'));
			echo $this->Form->input('Video.thumb_mimetype', array('type' => 'hidden'));
			echo $this->Form->input('Video.thumb_filesize', array('type' => 'hidden'));
			
			if( isset($this->data['Video']['thumb_filename']) ){
				echo $form->input('Video.thumb_remove', array('type' => 'checkbox')); 
			}else{
				echo $form->input('Video.thumb_default2', array('type' => 'checkbox')); 
			}
		?>
		
		<div class="clear"></div>
		<?php 
			echo $this->Form->end(__('Salvar', true));
        ?>
    </fieldset>
</div>