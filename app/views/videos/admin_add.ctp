<?php
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('common/jquery-ui-1.8.16.custom.min',false);
echo $this->Html->css('common/ui-lightness/jquery-ui-1.8.16.custom.css');
echo $javascript->link('admin/videos/index.js',false);
?>
<div class="index">
<?php echo $this->Form->create('Video',array('type' => 'file', 'action'=>'add'));?>
	<fieldset>
 		<legend><?php printf(__('Adicionar %s', true), __('Video', true)); ?></legend>
	<?php
		echo $this->Form->input('status', array('default'=>true,'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
		echo $this->Form->input('video_tipo_id', array('label' => 'Tipo de Vídeo', 'id' => 'xml_tipo_id', 'class'=>'w312','options' => $video_tipos));
		echo $this->Form->input('nome', array('class'=>'w312'));
		echo $this->Form->input('descricao', array('class'=>'w312'));
		echo $this->Form->input('url', array('class'=>'w312'));
		echo $this->Form->input('duracao', array('class'=>'w312'));
		echo $this->Form->input('created', array('type'=>'text','label' => 'Data Criação', 'class' => 'datePicker'));
	?>
	<legend>Imagem</legend>
	
	<?php echo $this->Form->input('thumb_default', array('label' => false,'default'=>true,'type' => 'radio', 'options' => array(true => 'Thumb Default', false => 'Cadastrar Thumb'))); ?>
	
	<?php
		echo $this->Form->input('Video.thumb_filename', array('type' => 'file'));
		echo $this->Form->input('Video.thumb_dir', array('type' => 'hidden'));
		echo $this->Form->input('Video.thumb_mimetype', array('type' => 'hidden'));
		echo $this->Form->input('Video.thumb_filesize', array('type' => 'hidden'));	
	?>
	<?php
		echo $this->Form->end(__('Inserir', true));
	?>
	</fieldset>
</div>