<?php

/**
 * @version 1.1
 * @author Luan Garcia 
 */
class UploadifyHelper extends Helper {

    var $helpers = array('Html','Image');

    public function build($imgs, $model = "ProdutoImagem", $destaque = false) {
    	App::import("helper", "Image");
		$this->Image = new ImageHelper();
		$string = '';
		if($imgs):
			foreach($imgs as $img):		
				if(isset($img['tmp_file'])){		
					$imagem = '../../'.$this->Image->resize('uploads/tmp/'.$img['tmp_file'], 40, 40,false,null,true);
				}else{
					$imagem = '../../../../'.$this->Image->resize($img['dir'].DS.$img['filename'], 40, 40,false,null,true);
				}
		
				$id = isset($img['tmp_file'])?$img['tmp_file']:$img['id'];
				$name = isset($img['tmp_file'])?$img['tmp_file']:$img['filename'];			
				
				$string .= '<div class="uploadifyQueue" id="file_uploadQueue">
								<div class="uploadifyQueueItem">
									<div class="cancel">'.(is_numeric($id)?'<a href="javascript:;" rel="'.$id.'" class="rm rm-img-old">remover</a>':'<a href="javascript:;" rel="'.$id.'" class="rm rm-img">remover</a>').'</div>										
									<span class="fileName">
										<label>Ordem <input type="text" class="ordem" value="'.$img['ordem'].'" rel="'.$id.'" name="data['.$model.'][ordem]" /></label>';
						if($destaque == true){
							$checked = "";
							if($img['destaque'] == 1){
								$checked = "checked='checked'";
							}
							$string .= '<label>Destaque <input type="checkbox" class="destaque" value="'.$img['destaque'].'" rel="'.$id.'" name="data['.$model.'][destaque]" '.$checked.' /></label>';
						}
				$string .=				'<img src="'.$imagem.'" alt="Foto" /> '.$name.'
									</span>
									<span class="percentage"> - 100%</span>											
									</div>
								</div>';
			endForeach;
		endIf;
		return $string;
    }
}