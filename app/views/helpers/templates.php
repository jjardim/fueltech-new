<?php

/**
 *
 * PHP version 5
 *
 * @category Helper
 * @version  1.0
 * @author   Luan Garcia <luan.garcia@gmail.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 */
class TemplatesHelper extends AppHelper {

    /**
     * Email Finaliza��o
     * @return String
     */
    function PedidoFrete($pedido) {
        return '
		<table cellspacing="0" cellpadding="0">
			<tr><td colspan="2">' . $pedido['Pedido']['entrega_tipo'] . ' - Em m�dia ' . $pedido['Pedido']['entrega_prazo'] . ' dia(s)</td></tr>
			<tr><td colspan="2">R$ ' . $pedido['Pedido']['valor_frete'] . '</td></tr>
		</table>';
    }

    function PedidoPagamento($pedido) {
        App::import('Helper', 'String');
        $this->String = new StringHelper();
        $string = '
		<table cellspacing="0" cellpadding="0">
			<tr><td colspan="2">' . $pedido['PagamentoCondicao']['nome'] . '</td></tr>
			<tr><td colspan="2">Valor: R$   ' . $this->String->bcoToMoeda($this->String->moedaToBco($pedido['Pedido']['valor_pedido']) + $this->String->moedaToBco($pedido['Pedido']['valor_frete'])) . '</td></tr>';
        if ($pedido['Pedido']['url_boleto'] != ""):
            $string .= '<tr><td colspan="2">Para realizar o pagamento do pedido <a href="' . $pedido['Pedido']['url_boleto'] . '" target="_blank" title="Imprimir">clique aqui</a></td></tr>';
        endIf;
        $string .= '</table>';
        return $string;
    }

    function PedidoEnderecoEntrega($pedido) {
        return '
		<table cellspacing="0" cellpadding="0">
		<tr><td colspan="2">' . $pedido['Pedido']['endereco_rua'] . ', ' . $pedido['Pedido']['endereco_numero'] . ' - ' . $pedido['Pedido']['endereco_complemento'] . '</td></tr>
		<tr><td colspan="2">' . $pedido['Pedido']['endereco_bairro'] . ' ' . $pedido['Pedido']['endereco_cidade'] . ' - ' . $pedido['Pedido']['endereco_estado'] . '</td></tr>
		</table>';
    }

    function PedidoEnderecoCobranca($pedido) {
        App::import('Model', 'UsuarioEndereco');
        $this->UsuarioEndereco = new UsuarioEndereco();
        $usuario_endereco = $this->UsuarioEndereco->find('first', array('conditions' =>
            array('AND' => array(
                    'UsuarioEndereco.usuario_id' => $pedido['Usuario']['id'],
                    'UsuarioEndereco.cobranca' => true
                )
            )
                )
        );
        return '
		<table cellspacing="0" cellpadding="0">
		<tr><td colspan="2">' . $usuario_endereco['UsuarioEndereco']['rua'] . ', ' . $usuario_endereco['UsuarioEndereco']['numero'] . ' - ' . $usuario_endereco['UsuarioEndereco']['complemento'] . '</td></tr>
		<tr><td colspan="2">' . $usuario_endereco['UsuarioEndereco']['bairro'] . ' ' . $usuario_endereco['UsuarioEndereco']['cidade'] . ' - ' . $usuario_endereco['UsuarioEndereco']['uf'] . '</td></tr>
		</table>';
    }

    function PedidoItens($pedido) {
        $string = '<tbody>';
        foreach ($pedido['PedidoItem'] as $item):
            if (isset($item['preco_promocao']) && $item['preco_promocao'] > 0) {
                $preco_unit = $item['preco_promocao'];
            } else {
                $preco_unit = $item['preco'];
            }
            $string .= '<tr>
					<td valign="top" style="border:0;padding-top:2.25pt;padding-right:6.75pt;padding-bottom:2.25pt;padding-left:6.75pt">
						<div style="margin-right:0cm;margin-left:0cm;font-size:12pt;font-family:\'Times New Roman\',serif;margin-top:0cm;margin-bottom:0.0001pt;line-height:16.2pt">
							<strong>
								<span style="font-size:8.5pt;font-family:Verdana,sans-serif;color:rgb(47,47,47)">' . $item['nome'] . '</span>
							</strong>
						</div>										
					</td>
					<td valign="top" style="border:0;padding-top:2.25pt;padding-right:6.75pt;padding-bottom:2.25pt;padding-left:6.75pt">
						<div style="border:0;margin-right:0cm;margin-left:0cm;font-size:12pt;font-family:\'Times New Roman\',serif;margin-top:0cm;margin-bottom:0.0001pt;line-height:16.2pt">
							<span style="font-size:8.5pt;font-family:Verdana,sans-serif;color:rgb(47,47,47)">' . $item['sku'] . '</span>
						</div>
					</td>
					<td valign="top" style="border:0;padding-top:2.25pt;padding-right:6.75pt;padding-bottom:2.25pt;padding-left:6.75pt">
						<div style="margin-right:0cm;margin-left:0cm;font-size:12pt;font-family:\'Times New Roman\',serif;margin-top:0cm;margin-bottom:0.0001pt;text-align:center;line-height:16.2pt">
							<span style="font-size:8.5pt;font-family:Verdana,sans-serif;color:rgb(47,47,47)">' . $item['quantidade'] . '</span>
						</div>
					</td>
					<td valign="top" style="border:0;padding-top:2.25pt;padding-right:6.75pt;padding-bottom:2.25pt;padding-left:6.75pt">
						<div style="margin-right:0cm;margin-left:0cm;font-size:12pt;font-family:\'Times New Roman\',serif;margin-top:0cm;margin-bottom:0.0001pt;text-align:center;line-height:16.2pt">
							<span style="font-size:8.5pt;font-family:Verdana,sans-serif;color:rgb(47,47,47)">R$ ' . $this->String->bcoToMoeda($item['quantidade'] * $preco_unit) . '</span>
						</div>
					</td>
				</tr>';
        endforeach;
        $string .= '</tbody>';
        return $string;
    }
	
	function PedidoItensLojista($pedido) {
        $string = '<tbody>';
        foreach ($pedido['PedidoItem'] as $item):
            if (isset($item['preco_promocao']) && $item['preco_promocao'] > 0) {
                $preco_unit = $item['preco_promocao'];
            } else {
                $preco_unit = $item['preco'];
            }
            $string .= '<tr>
					<td valign="top" style="border:0;padding-top:2.25pt;padding-right:6.75pt;padding-bottom:2.25pt;padding-left:6.75pt">
						<div style="margin-right:0cm;margin-left:0cm;font-size:12pt;font-family:\'Times New Roman\',serif;margin-top:0cm;margin-bottom:0.0001pt;line-height:16.2pt">
							<strong>
								<span style="font-size:8.5pt;font-family:Verdana,sans-serif;color:rgb(47,47,47)">' . $item['nome'] . '</span>
							</strong>
						</div>										
					</td>
					<td valign="top" style="border:0;padding-top:2.25pt;padding-right:6.75pt;padding-bottom:2.25pt;padding-left:6.75pt">
						<div style="border:0;margin-right:0cm;margin-left:0cm;font-size:12pt;font-family:\'Times New Roman\',serif;margin-top:0cm;margin-bottom:0.0001pt;line-height:16.2pt">
							<span style="font-size:8.5pt;font-family:Verdana,sans-serif;color:rgb(47,47,47)">' . $item['sku'] . '</span>
						</div>
					</td>
					<td valign="top" style="border:0;padding-top:2.25pt;padding-right:6.75pt;padding-bottom:2.25pt;padding-left:6.75pt">
						<div style="border:0;margin-right:0cm;margin-left:0cm;font-size:12pt;font-family:\'Times New Roman\',serif;margin-top:0cm;margin-bottom:0.0001pt;line-height:16.2pt">
							<span style="font-size:8.5pt;font-family:Verdana,sans-serif;color:rgb(47,47,47)">' . $item['codigo_erp'] . '</span>
						</div>
					</td>
					<td valign="top" style="border:0;padding-top:2.25pt;padding-right:6.75pt;padding-bottom:2.25pt;padding-left:6.75pt">
						<div style="margin-right:0cm;margin-left:0cm;font-size:12pt;font-family:\'Times New Roman\',serif;margin-top:0cm;margin-bottom:0.0001pt;text-align:center;line-height:16.2pt">
							<span style="font-size:8.5pt;font-family:Verdana,sans-serif;color:rgb(47,47,47)">' . $item['quantidade'] . '</span>
						</div>
					</td>
					<td valign="top" style="border:0;padding-top:2.25pt;padding-right:6.75pt;padding-bottom:2.25pt;padding-left:6.75pt">
						<div style="margin-right:0cm;margin-left:0cm;font-size:12pt;font-family:\'Times New Roman\',serif;margin-top:0cm;margin-bottom:0.0001pt;text-align:center;line-height:16.2pt">
							<span style="font-size:8.5pt;font-family:Verdana,sans-serif;color:rgb(47,47,47)">R$ ' . $this->String->bcoToMoeda($item['quantidade'] * $preco_unit) . '</span>
						</div>
					</td>
				</tr>';
        endforeach;
        $string .= '</tbody>';
        return $string;
    }

}

?>