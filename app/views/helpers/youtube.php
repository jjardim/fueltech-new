<?php

class YoutubeHelper extends AppHelper {
    public function get_code($url)
    {
        $results = array();
        
        preg_match('/youtu(be.com|.b)(\/v\/|\/watch\\?v=|e\/)(.{11})/', $url, $results);
        
        if (is_array($results)) {
            return $results[3];
        } else {
            return false;
        }
    }
}