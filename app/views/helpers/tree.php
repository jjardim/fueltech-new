<?php

/**
 * An extended helper class.
 **/

class TreeHelper extends Helper {
	public $tab = "  ";
	

	public $helpers = array('Html', 'Seo');

	public function show($name, $data, $extra = '', $url = '', $form = false, $selected = false,$max_level=1) {
		
		list($modelName, $fieldName) = explode('/', $name);
		$output = $this->_List($data, $extra, $url, $form, $selected, $modelName, $fieldName, 0,$max_level);
		return $this->output($output);
	}


	public function flat($name, $data, $selected = null, $legend) {
		list($modelName, $fieldName) = explode('/', $name);
		debug($data);
		$complex = '<fieldset id="' . $fieldName . '" class="habtm">';
		$complex .= '<legend>' . $legend . '</legend>';
		foreach ($data as $key => $row) {
		//$complex .= $this->checkboxMultiple($fieldName, $data, $selected);
			$this->_generate_input($data, $modelName, $fieldName, $selected);
			//debug($data[$key]);
		}
		$complex .= '</fieldset>';
		return $complex;
	}

	public function _List($data, $extra, $url, $form, $selected, $modelName, $fieldName, $level,$max_level) {
		
		$tabs = "\n" . str_repeat($this->tab, $level * 2);
		$li_tabs = $tabs . $this->tab;

		$output = $tabs. '<ul' . $extra . ' rel="'.$level.'">';
		foreach ($data as $key => $row) {
			if($form) {
				$text = $this->_generate_input($row, $modelName, $fieldName, $selected);
			} else {
				$text = $this->_generate_link($row[$modelName], $url,$selected);
			}
			if(isset($row['children'][0]) && ($form) && (!in_array($row[$modelName]['id'], $selected))) {
				$text = strip_tags($text, '<label>');
			}			
			$output .= $li_tabs . '<li>' . $text;
	
			if(isset($row['children'][0]) && $level<$max_level) {
				$output .= $this->_List($row['children'], '', $url, $form, $selected, $modelName, $fieldName, $level+1,$max_level);
				$output .= $li_tabs . '</li>';
			} else {
				$output .= '</li>';
			}
		}
		$output .= $tabs . '</ul>';
		return $output;
	}


	public function _generate_input($data, $modelName, $fieldName, $selected) {
		$checked = '';
		if(in_array($data[$modelName]['id'], $selected)) {
			$checked = ' checked="checked"';
		}
		$text = '<input type="checkbox" name="data[' . $modelName . '][' . $modelName . '][]" value="' . $data[$modelName]['id'] . '"' . $checked . ' /><label>' . $data[$modelName][$fieldName] . '</label>';
		return $text;
	}
	public function _generate_link($data, $url,$selected) {
		if(strstr($url, 'admin')) {
                        if(isset($data['parent_id'])&&$data['parent_id']==0){
				$tipo = 'parent';
			}else{
				$tipo = 'child';
			}
			$text = '<span class="status-' . $data['status'] .' '. $tipo .'">' . $data['nome'] . '</span>';
			$text .= '<a  onclick="return confirm(\'Deseja mesmo remover o registro # '.$data['nome'].'?\');" href="' . $this->Html->url($url.'../delete/'. $data['id']) . '" class="rm-min status-' . $data['status'] . '">Deletar</a>';
			$text .= '<a  href="' . $this->Html->url($url.'../edit/'. $data['id']) . '" class="edit-min status-' . $data['status'] . '">Deletar</a>';
		} else {
			if(isset($data['parent_id'])&&$data['parent_id']==0){
				$tipo = 'parent selected';
			}else{
				$tipo = 'child';
			}
			if($selected==$data['id']){
				$tipo .= '  selected';
			}
			$text = '<a href="' . $this->Html->url('/').$this->url($data) . '" class="'.($tipo).'">' . $data['nome'] . '</a>';
                        
		}
		return $text;
	}
}
?>