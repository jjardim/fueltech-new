<?php

class StringHelper extends AppHelper {

    public function cropSentence($str, $size = 50, $endOfStr = "...", $cutchars = null, $htmlencode = false, $htmldecode = false) {
        $str = strip_tags($str);
        $tamanho_inicial = strlen($str);
        if (is_null($cutchars))
            $cutchars = str_split(" ,.;!/\"'?(){}[]\t\n\r+-@#$%&*\\`");
        elseif (!is_string($cutchars))
            return $str;
        else
            $cutchars = str_split($cutchars);
        if ($htmldecode)
            $str = html_entity_decode($str);
        if (strlen($str) > $size) {
            for ($i = $size; $i > 0; $i--) {
                if (in_array($str[$i], $cutchars))
                    break;
            }
            $str = substr($str, 0, $i);
        }
        $str = rtrim($str, implode('', $cutchars));
        $tamanho_final = strlen($str);
        if ($tamanho_inicial == $tamanho_final)
            $endOfStr = "";

        if ($htmlencode)
            return htmlentities($str);

        return $str . $endOfStr;
    }

    public function changeNullTo($string, $changeStr) {
        return empty($string) ? $changeStr : $string;
    }

    public function decimalFormat($to, $valor, $prec = 2) {
        if ($to == "db") {
            return (float) $this->moedaToBco($valor);
        } elseif ($to == "human") {
            return $this->bcoToMoeda($valor, $prec);
        }
    }

    public function moedaToBco($valor) {
        return (float) str_replace(",", ".", str_replace(".", "", $valor));
    }

    public function bcoToMoeda($valor, $prec = 2) {
        return number_format($valor, $prec, ",", ".");
    }

    /**
     * Remover os acentos de uma string
     *
     * @param string $str
     * @return string
     */
    public function removerAcentos($str) {
        $from = 'ÀÁÃÂÉÊÍÓÕÔÚÜÇàáãâéêíóõôúüç';
        $to = 'AAAAEEIOOOUUCaaaaeeiooouuc';
        return strtr($str, $from, $to);
    }

    public function remove_accent($texto) {
        $oque = array("/(?i)á|ã|â|Á|Ã|Â/", "/(?i)é|ê|É|Ê/", "/(?i)í|î|Í|Î/", "/(?i)ó|õ|ô|Ó|Ô|Õ/", "/(?i)ú|û|Ú|Û/", "/(?i)ç|Ç/", "/(?i)º|ª/");
        $peloque = array("a", "e", "i", "o", "u", "c", "");
        return preg_replace($oque, $peloque, $texto);
    }

    //metodo que deixa a primeira letra de cada palavra em maiuscula, com tratamento de exceção
    function title_case($title) {
        return $title;
        //termos que não serão deixados em maiusculo
        $smallwordsarray = array(
            'da', 'de', 'do', 'e', 'das', 'dos', 'para', 'p/'
        );

        //caracteres especiais
        $trans = array('á' => 'Á', 'é' => 'É', 'í' => 'Í', 'ó' => 'Ó', 'ç' => 'Ç');
        $trans2 = array('Á' => 'á', 'Ã' => 'ã', 'É' => 'é', 'Í' => 'í', 'Ó' => 'ó', 'Ç' => 'ç');

        //separo as palavras
        $words = explode(' ', $title);
        foreach ($words as $key => $word) {
            if ($key == 0 or !in_array($word, $smallwordsarray))
                $words[$key] = ucwords(strtolower($word));
        }

        // volto a juntar as palavras e retorno o termo
        $newtitle = implode(' ', $words);
        //die;

        $primeira_letra = mb_substr($newtitle, 0, 1);
        $primeira_letra = strtr($primeira_letra, $trans);
        return strtr($primeira_letra . mb_substr($newtitle, 1), $trans2);

        //return $newtitle;
    }

    //metodo que formata o titulo da categoria
    function formata_estilo_titulo($texto_titulo, $tipo) {

        $title = '';

        switch ($tipo) {

            case 1:
                //formato
                /* <span><b>INJEÇÃO</b> E IGNIÇÃO/</span> CONTROLES ELETRÔNICOS */

                $title .= '<span>';
                foreach ($texto_titulo as $key => $titulo) {
                    if ($key == 0) {
                        $titulo_cat = explode(" ", trim($titulo['nome']));
                        $title .= "<b>" . $titulo_cat[0] . "</b>";
                        if (count($titulo_cat) > 1) {
                            for ($i = 0; $i < count($titulo_cat); $i++) {
                                $title .= " " . $titulo_cat[$i];
                            }
                        }
                        $title .= '/ </span> ';
                    } else {
                        if ($key == 1) {
                            $title .= $titulo['nome'];
                        } else {
                            $title .= '/ ' . $titulo['nome'];
                        }
                    }
                }
                break;

            case 2:
                //formato
                /* ÚLTIMAS <span>NOVIDADES</span> */
                $titulo_cat = explode(" ", trim($texto_titulo));
                $cont_palavra = count($titulo_cat);
                foreach ($titulo_cat as $key => $titulo) {
                    if ($cont_palavra - 1 == $key) {
                        $title .= " <span>" . $titulo . "</span>";
                    } else {
                        $title .= $titulo . " ";
                    }
                }
                break;

            case 3:
                //formato
                /* <span>COMPRE</span> AGORA MESMO */
                $titulo_cat = explode(" ", trim($texto_titulo));
                foreach ($titulo_cat as $key => $titulo) {
                    if ($key == 0) {
                        $title .= " <span>" . $titulo . "</span>";
                    } else {
                        $title .= " " . $titulo;
                    }
                }
                break;

            case 4:
                //formato
                /* <h2>MAIS <small class="orange">NOTÍCIAS</small></h2> */
                $titulo_cat = explode(" ", trim($texto_titulo));
                $cont_palavra = count($titulo_cat);
                foreach ($titulo_cat as $key => $titulo) {
                    if ($cont_palavra - 1 == $key) {
                        $title .= " <small class='orange'>" . $titulo . "</small>";
                    } else {
                        $title .= $titulo . " ";
                    }
                }
                break;

            case 5:
                //formato
                /* <span>MUNDO<br /> FUELTECH</span> */
                $titulo_cat = explode(" ", trim($texto_titulo));
                $title .= " <span>";
                foreach ($titulo_cat as $key => $titulo) {
                    $title .= $titulo . "<br />";
                }
                $title .= "</span>";
                break;
        }

        return $title;
    }

    public function formata_intervalo_data($time) {

        $time = strtotime($time);
        return date('d/m/Y', $time);
    }
    
    function type_file($filename){
        
        $ext = end(explode('.', $filename));
        switch ($ext) {
            case "pdf":
                $ctype = "application/pdf";
                break;

            case "exe":
                $ctype = "application/octet-stream";
                break;

            case "zip":
            case "rar":
                $ctype = "application/zip";
                break;

            case "doc":
            case "docx":
                $ctype = "application/msword";
                break;

            case "xls":
            case "xlsx":
                $ctype = "application/vnd.ms-excel";
                break;

            case "ppt":
            case "pptx":
                $ctype = "application/vnd.ms-powerpoint";
                break;

            case "gif":
                $ctype = "image/gif";
                break;

            case "png":
                $ctype = "image/png";
                break;

            case "jpeg":
            case "jpg":
                $ctype = "image/jpg";
                break;

            default: $ctype = "application/force-download";
        }
        
        return $ctype;
    }

}

?>