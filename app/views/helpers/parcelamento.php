<?php

class ParcelamentoHelper extends AppHelper {
	var $helpers = array('String');
	
	var $coef = array(4=>1.02490,5=>1.03750,6=>1.05019,	7=> 1.06300,8=> 1.07589,9=> 1.08889,10=> 1.10200,11=> 1.11530,12=>  1.12860);

	public function parcelaValida($valor, $parcelamento) {
        $retorno['valor'] = null;
        $retorno['parcela'] = null;
        $retorno['juros'] = null;
		if(is_string($valor)){
			$valor=(str_replace(',','.',$valor));
		}
		$juros_apartir = $parcelamento['juros_apartir'];
		$juros = false;
		
        for ($i = 1; $i <= (int)$parcelamento['parcelas']; $i++):
			$parcela = $valor / $i;			
			if($i>$juros_apartir && up($parcelamento['sigla']) == 'PAGSEGURO'){
				$parcela = $valor*$this->coef[$i]/$i;		
				$juros = true;
			}	
            if ($i == 1 || $parcela > $this->String->moedaToBco(Configure::read('Loja.valor_minimo_parcelamento'))):
                $retorno['valor'] = $parcela;
                $retorno['parcela'] = $i;
                $retorno['juros'] = $juros;
            endIf;
        endFor;
        return $retorno;
    }
	
	public function listaParcelamentos($valor, $parcelamento) {
        $retorno['valor'] = null;
        $retorno['parcela'] = null;
        $retorno['juros'] = null;
		if(is_string($valor)){
			$valor=(str_replace(',','.',$valor));
		}
		$juros_apartir = $parcelamento['juros_apartir'];
		$juros = false;
		
		//abro a div de divisao de coluna
		echo '<div class="divisao" style="float: left;">';
		
		for ($i = 1; $i <= (int)$parcelamento['parcelas']; $i++):
			$parcela = $valor / $i;			
			if($i>$juros_apartir && up($parcelamento['sigla']) == 'PAGSEGURO'){
				$parcela = $valor*$this->coef[$i]/$i;		
				$juros = true;
			}	
            if ($i == 1 || $parcela > $this->String->moedaToBco(Configure::read('Loja.valor_minimo_parcelamento'))){
                $retorno['valor'] = $parcela;
                $retorno['parcela'] = $i;
                $retorno['juros'] = $juros;
				
				echo '<div class="row1"><div class="td1">'.$retorno['parcela'].'x</div><div class="td2">R$ '.$this->String->bcoToMoeda($retorno['valor']).'</div><div class="td3">'.($retorno['juros']==true?'com juros':'sem juros').'</div></div>';
				
				//se o index for igual a metade de valor da parcela, corto a lista ao meio com uma coluna
				if($i%(int)((int)$parcelamento['parcelas']/2) == 0){echo '</div><div class="divisao" style="float: left;">';}
            }
			
        endFor;
		
		//fecho a div de divisao de coluna
		echo '</div>';
    }
	
}
?>