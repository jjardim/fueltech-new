<?php

class MarcasHelper extends AppHelper {

	public function sort_marcas($array, $field = 'nome', $order = SORT_ASC)
	{
		$novo_array = array();
		$array_letras = array();

		if (count($array) > 0) {
		
			//percorro o array com as marcas
			foreach ($array as $k => $v) {
			
				//se cada index do array, for outro array...
				if (is_array($v)) {				
					
					//percorro o mesmo para montar o array de letras
					foreach ($v as $k2 => $v2)
					{	
						//se o field atual for o campo "NOME", 
						//crio o array de letras ou preencho o memso que j� existe, apenas com o nome da marca da respectiva letra como valor
						//estrutura: array_letrar[LETRA][0][INDEX_DO_ARRAY_PRINCIPAL] = NOME_DA_MARCA
						if ($k2 == $field) {
							if( is_numeric(substr($v2,0,1)) )
								$letra = "#";
							else
								$letra = substr($v2,0,1);
							$array_letras[strtoupper($letra)][0][$k] = $v2;
						}		
					}
				} 
			}
			
			//ordem
			switch ($order) {
				case SORT_ASC:
					//percorro o array de letras, e ordeno os valores da mesma
					foreach( $array_letras as $key => $al )
					{
						asort($array_letras[$key][0]);
					}
					//ordeno o array de letras, pelo index da letra
					array_multisort(array_keys($array_letras), SORT_ASC, $array_letras);					
				break;
				case SORT_DESC:
					//percorro o array de letras, e ordeno os valores da mesma
					foreach( $array_letras as $key => $al )
					{
						 arsort($array_letras[$key][0]);
					}
					//ordeno o array de letras, pelo index da letra
					array_multisort(array_keys($array_letras), SORT_DESC, $array_letras);
				break;
			}
			
			//percorro o array de letras novamente, criando o novo array, com as informa��es completas
			foreach ($array_letras as $letra => $arr) {
				//novo_array[LETRA] recebe os valores do INDEX atual do array principal
				foreach( $arr[0] as $i => $v2 )
				{
					$novo_array[$letra][] = $array[$i];
				}
			}
		}
		
		return $novo_array;
	}
	
	public function getListMarcas( $tipo = "all", $ordem = false ) {
	
		App::import('Model', 'Categoria');
		$this->Categoria = new Categoria();
		
		$categorias = $this->Categoria->find($tipo,array('contain'=>array('SubCategory'),'conditions'=>array('Categoria.parent_id' => 0)));	
		//divido as marcas das categorias
		foreach( $categorias as $mt )
		{
			if( $mt['Categoria']['seo_url'] == 'marcas' && $mt['Categoria']['parent_id'] == 0)
				$menu_topo_marcas[] = $mt; 
		}
		
		$retorno[] = "";
		foreach( $menu_topo_marcas[0]['SubCategory'] as $mtm )
		{	
			$retorno = array_merge($retorno, array($mtm['descricao'] => $mtm['descricao']));
		}
		
		if( $ordem == true ){
			asort($retorno);
			return $retorno;
		}
		else{
			return $retorno;
		}
		
	}
	
}
?>