<?php echo $this->Javascript->link('common/jquery.meio_mask.js'); ?>
<script>
	$(document).ready(function(){
		$('.mask-telefone').setMask({mask:'(99) 9999-99999'});
		$('.mask-cpf').setMask({mask:'999.999.999-99'});
		$('.mask-cep').setMask({mask:'99999-999'});

		<?php if (isset($this->params['data'])): ?>
			$(window).scrollTop($('#anchor-ft-education').offset().top).scrollLeft($('#anchor-ft-education').offset().left);
		<?php endif; ?>

	});
</script>

<?php if( isset($pagina_element_content) ){
	$tipo_form = $pagina_element_content['tipo_form'];
} ?>

<!-- start fill-form -->
<div class="content" id="anchor-ft-education">
	<?php echo $this->Form->create('FtEducation', array('class' => 'fillform2',  'type' => 'file', 'url' => $this->Html->url(null,true))); ?>
	<div class="contato">
		<!-- start left -->
		<div class="row">
			<?php echo $this->Session->flash(); ?>
		</div>
		<!-- end left -->
		<div class="clear"></div>
		<!-- start left -->
		<div class="row">
			<?php echo $this->Form->input('FtEducation.nome', array('class' => 'input1', 'label' => 'Nome*', 'div' => false, 'rel' => 'Nome*')); ?>
		</div>
		<!-- end left -->
		<div class="clear"></div>
		<!-- start left -->
		<div class="row">
			<?php echo $this->Form->input('FtEducation.cpf', array('class' => 'input1 input5 mask-cpf', 'label' => 'CPF*', 'div' => false, 'rel' => 'CPF*')); ?>
		</div>
		<!-- end left -->
		<div class="clear"></div>
		<!-- start left -->
		<div class="row">
			<?php echo $this->Form->input('FtEducation.endereco_residencial', array('class' => 'input1', 'label' => 'Endereço Residencial*', 'div' => false, 'rel' => 'Endereço Residencial*')); ?>
		</div>
		<div class="clear"></div>
		<!-- start left -->
		<div class="row">
			<?php echo $this->Form->input('FtEducation.cep', array('class' => 'input1 input5 mask-cep', 'label' => 'CEP*', 'div' => false, 'rel' => 'CEP*')); ?>
		</div>
		<!-- end left -->
		<div class="clear"></div>
		<!-- start left -->
		<div class="row">
			<?php echo $this->Form->input('FtEducation.telefone', array('class' => 'input1 input5 mask-telefone', 'label' => 'Telefone*', 'maxlength' => '15', 'div' => false, 'rel' => 'Telefone*')); ?>
		</div>
		<!-- end left -->
		<div class="clear"></div>
		<!-- start left -->
		<div class="left">
			<?php echo $this->Form->input('FtEducation.estado', array('label'=> 'Estado*','class' => 'select3',  'options' => $this->Estados->estadosBrasileiros(),'div' => false, 'rel' => 'Estado*' )) ?>
		</div>
		<!-- end left -->
		<div class="clear"></div>
		<!-- start left -->
		<div class="left">
			<?php echo $this->Form->input('FtEducation.cidade', array('class' => 'input1', 'label' => 'Cidade*', 'div' => false, 'rel' => 'Cidade*')); ?>
		</div>
		<!-- end left -->
		<div class="clear"></div>
		<!-- start left -->
		<div class="left">
			<?php echo $this->Form->input('FtEducation.empresa_oficina', array('class' => 'input1', 'label' => 'Empresa/Oficina', 'div' => false, 'rel' => 'Empresa/Oficina')); ?>
		</div>
		<!-- end left -->
		<div class="clear"></div>
		<!-- start left -->
		<div class="left">
			<?php echo $this->Form->input('FtEducation.possui_cursos_de',array(
				'label' => 'Possui Cursos de:',
				'type' => 'select',
				'multiple' => 'checkbox',
				'options' => array('Mecânica' => 'Mecânica', 'Eletricidade' => 'Eletricidade', 'Injeção Original' => 'Injeção Original', 'FTEducation' => 'FTEducation'),
				)); ?>
			</div>

			<!-- end left -->
			<div class="clear"></div>
			<!-- start left -->
			<div class="row">
				<?php echo $this->Form->input('FtEducation.email', array('class' => 'input1', 'label' => 'E-mail*', 'div' => false, 'rel' => 'E-mail*')); ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
			<!-- start left -->
			<div class="left">
				<?php
					$msg = '';
					if(isset($this->params['data']['FtEducation']['mensagem'])) {
						$msg = htmlentities ($this->params['data']['FtEducation']['mensagem']);
					}
	         			?>
				<?php echo $this->Form->input('FtEducation.mensagem', array('value'=>$msg, 'label' => 'Mensagem* ','type' => 'textarea','class'=>'textarea input1', 'div' => false, 'default' => 'Mensagem')); ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
			<!-- start left -->
			<div class="row">
				<?php echo $this->Form->input('FtEducation.sac_tipo_id', array('value'=> $tipo_form, 'type' => 'hidden', 'div' => false)); ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
			<!-- start left -->
			<div class="row">
				<span>* Campos Obrigatórios</span>
			</div>
			<!-- end left -->
			<div class="clear"></div>
			<br />
			<div class="clear"></div>
			<!-- start left -->
			<div class="row">
				<?php
				echo $this->Form->submit('Enviar', array( 'class' => 'button', 'value' => ''));
				?>
			</div>
			<div class="clear"></div>
			<!-- end left -->
		</div>
		<?php echo $this->Form->end(); ?>
	</div>
	<!-- end fill-form -->
	<div class="clear"></div>