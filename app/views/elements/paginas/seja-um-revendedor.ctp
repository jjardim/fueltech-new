<?php if( isset($pagina_element_content) ){
	$tipo_form = $pagina_element_content['tipo_form'];
} ?>

<!-- start fill-form -->
<div class="fill-form" style="margin: 0px; width: auto;">
	<?php echo $this->Form->create('Sac', array('class' => 'fillform2', 'url' => $this->Html->url(null,true))); ?>
		<div class="seja-um-revendedor">
			<!-- start left -->
			<div class="left">
				<?php echo $this->Session->flash(); ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
                        
                        <!-- start left -->
			<div class="left">
				<p>Se você tem interesse em ser um revendedor autorizado FuelTech, preencha o formulário e siga as instruções abaixo:</p>
			</div>
			<!-- end left -->
			<div class="clear"></div>
                        
			<!-- start left -->
			<div class="left">
				<?php echo $this->Form->input('SejaUmRevendedor.nome_do_responsavel', array('class' => 'input1', 'label' => 'Nome do responsável*', 'div' => false)); ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
                        
			<!-- start left -->
			<div class="left">
				<?php echo $this->Form->input('SejaUmRevendedor.cpf', array('class' => 'input1', 'label' => 'CPF*', 'div' => false)); ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
                        
			<!-- start left -->
			<div class="left">
				<?php echo $this->Form->input('SejaUmRevendedor.rg', array('class' => 'input1', 'label' => 'RG*', 'div' => false)); ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
                        
                        <!-- start left -->
			<div class="left">
				<?php echo $this->Form->input('SejaUmRevendedor.data_de_nascimento', array('class' => 'input1', 'label' => 'Data de nascimento*', 'div' => false)); ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
                        
                        <!-- start left -->
			<div class="left">
				<?php echo $this->Form->input('SejaUmRevendedor.email', array('class' => 'input1', 'label' => 'E-mail*', 'div' => false)); ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
                        
                        <!-- start left -->
			<div class="left">
				<?php echo $this->Form->input('SejaUmRevendedor.nome_fantasia', array('class' => 'input1', 'label' => 'Nome fantasia*', 'div' => false)); ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
                        
                        <!-- start left -->
			<div class="left">
				<?php echo $this->Form->input('SejaUmRevendedor.razao_social', array('class' => 'input1', 'label' => 'Razão social*', 'div' => false)); ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
                        
                        <!-- start left -->
			<div class="left">
				<?php echo $this->Form->input('SejaUmRevendedor.cnpj', array('class' => 'input1', 'label' => 'CNPJ*', 'div' => false)); ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
                        
                        <!-- start left -->
			<div class="left">
				<?php echo $this->Form->input('SejaUmRevendedor.ie', array('class' => 'input1', 'label' => 'Inscrição Estadual*', 'div' => false)); ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
                        
                        <!-- start left -->
			<div class="left">
				<?php echo $this->Form->input('SejaUmRevendedor.endereco', array('class' => 'input1', 'label' => 'Endereço*', 'div' => false)); ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
                        
                        <!-- start left -->
			<div class="left">
				<?php echo $this->Form->input('SejaUmRevendedor.bairro', array('class' => 'input1', 'label' => 'Bairro*', 'div' => false)); ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
                        
                        <!-- start left -->
			<div class="left">
				<?php echo $this->Form->input('SejaUmRevendedor.cidade', array('class' => 'input1', 'label' => 'Cidade*', 'div' => false)); ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
                        
			<!-- start left -->
			<div class="left">
				<?php echo $this->Form->input('SejaUmRevendedor.estado', array('label'=> 'Estado*','class' => 'select3',  'options' => $this->Estados->estadosBrasileiros(),'div' => false, 'rel' => 'Estado' )) ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
                        
                        <!-- start left -->
			<div class="left">
				<?php echo $this->Form->input('SejaUmRevendedor.cep', array('class' => 'input1', 'label' => 'CEP*', 'div' => false)); ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
                        
                        <!-- start left -->
			<div class="left">
				<?php echo $this->Form->input('SejaUmRevendedor.telefone', array('class' => 'input1', 'label' => 'Telefone*', 'div' => false)); ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
                        
                        <!-- start left -->
			<div class="left">
				<?php echo $this->Form->input('SejaUmRevendedor.celular', array('class' => 'input1', 'label' => 'Celular*', 'div' => false)); ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
                        
                        <!-- start left -->
			<div class="left">
				<?php echo $this->Form->input('SejaUmRevendedor.fax', array('class' => 'input1', 'label' => 'Fax', 'div' => false)); ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
                        
                        <!-- start left -->
			<div class="left">
				<?php echo $this->Form->input('SejaUmRevendedor.site', array('class' => 'input1', 'label' => 'Site', 'div' => false)); ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
                        
			<!-- start left -->
			<div class="left">
                                <br/>
				<span>* Campos Obrigatórios</span>
			</div>
			<!-- end left -->
			<div class="clear"></div>
                        
                        <!-- start left -->
			<div class="left" style="margin: 20px 0px;">
				<p>Após o preenchimento do formulário, enviar a seguinte documentação para o email comercial@fueltech.com.br:</p>
                                <br/>
                                <ul style="list-style: disc inside;">
                                    <li>Cópia do Contrato social e as últimas alterações;</li>
                                    <li>Balancete dos últimos 12 meses assinado e carimbado pelo Contador;</li>
                                    <li>Referências Bancárias: (Número do Banco, da Agência e Conta, Gerente de conta, Telefone, Cliente desde)</li>
                                    <li>Fotos da fachada e instalações da empresa;</li>
                                </ul>
                                <br/>
                                <p><span style="color:#ff0000;">Importante:</span><br /><b>
                                   <b>- O simples preenchimento do formulário não caracteriza o credenciamento de revenda.</b><br />
                                    A documentação completa será submetida à análise e, caso aprovado, será fixado uma<br />
                                    tabela de preços;<br />
                                    - O estabelecimento precisa possuir showroom para exposição dos produtos;<br />
                                    - É imprescindível que a atividade principal da empresa conste no CNPJ como comércio e<br />
                                    Varejo de acessórios para veículoos automotores.<br />
                                </p>
			</div>
			<!-- end left -->
			<div class="clear"></div>

			<div class='left'>
				<?php $recaptcha->display_form('echo'); ?>
			</div>
			
			<div class="clear"></div>
                        
			<br />
			<div class="clear"></div>

			<!-- start left -->
			<div class="left">
			   <?php //echo $this->Form->submit('Enviar', array( 'class' => 'button', 'value' => '')); ?>
			   <?php echo $this->Form->submit('Enviar', array( 'class' => 'button', 'value' => '', 'onClick' => "_gaq.push(['_trackEvent', 'Button', 'Click', 'Seja um Revendedor - Enviar'])")); ?>
			   
			</div>
			<!-- end left -->
		</div>

	<?php echo $this->Form->end(); ?>
</div>
<!-- end fill-form -->
<div class="clear"></div>