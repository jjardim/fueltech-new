<?php echo $this->Form->create('Sac', array('id' => 'SacMin' , 'class' => 'form3', 'url' => $this->Html->url('/'.$url_form,true))); ?>
	<ul class="contato">
		<a href="javascript:void(0);" class="btn-fecha-form" style="float: right; margin-right: -15px; margin-top: -5px;">[ X ]</a>
		<li>
			<?php echo $this->Session->flash(); ?>
		</li>
		<li>
			<?php echo $this->Form->input('Sac.nome', array('class' => 'inputfield ', 'label' => false, 'div' => false, 'default' => 'Nome*')); ?>
			<div id="nome-msg-error" class="form-min-msg-error"></div>
		</li>
		<li>
			<?php echo $this->Form->input('Sac.email', array('class' => 'inputfield ', 'label' => false, 'div' => false, 'default' => 'E-mail*')); ?>
			<div id="email-msg-error" class="form-min-msg-error"></div>
		</li>
		<li>
			<?php echo $this->Form->input('Sac.mensagem', array('value'=>'', 'label' => false,'type' => 'textarea','class'=>'textarea ', 'div' => false, 'default' => 'Mensagem')); ?>
			<div id="mensagem-msg-error" class="form-min-msg-error"></div>
		</li>
		<li>
			<?php echo $this->Form->input('Sac.tipo', array('value'=> $tipo_form, 'type' => 'hidden', 'div' => false)); ?>
		</li>
		<li>
			<span>* Campos Obrigatórios</span>
		</li>
		<li class="clear">
		   <?php echo $this->Form->submit('Enviar', array( 'class' => 'button', 'value' => '')); ?>
		</li>
	</ul>
<?php echo $this->Form->end(); ?>