<?php 
	$ultimo_estado = "";
	if(isset($pagina_element_content)): 
?>
	<?php foreach($pagina_element_content as $loja): ?>
	<!-- start content-loja -->
	<div class="content-lojas">
		<?php if($ultimo_estado != $loja['Loja']['estado']): ?>
			<h4 class="green"><?php e($this->Estados->getNomeEstado($loja['Loja']['estado'])); ?></h4>
		<?php endIf; ?>
		
		<img src="img/site/ic_location.png" width="17" height="21" alt="location" class="left" style="display: none; float: none; margin-bottom: -15px;" />
		
		<p>
			<a href="javascript:void(0);" id="btn-loja-<?php e($loja['Loja']['id']); ?>" class="btn-nome-loja"><strong><?php e($loja['Loja']['cidade']); ?></strong></a>
		</p>
		
		<div class="dados-lojas">
			<?php e($loja['Loja']['conteudo']); ?>
		</div>
		
		<!-- start dados-loja (usado no google-mapa)-->
		<div id="dados-loja-<?php e($loja['Loja']['id']); ?>" style="display: none;">
			<div class="dados-lojas-content" style="width: 370px; height: 130px;">
				<h3 style="color: #0C5230;"><?php e($loja['Loja']['cidade']); ?></h3><br />
				<?php e($loja['Loja']['conteudo']); ?>
				<?php if($loja['Loja']['thumb_filename'] != ""): ?>
					<span style="float: right; margin-right: 10px;"><?php echo $image->resize( ( $loja['Loja']['thumb_filename'] ) ? 'uploads/lojas/'.$loja['Loja']['thumb_filename'] : 'uploads/produto_imagem/filename/sem_imagem.jpg', 117, 90); ?></span>
				<?php endIf; ?>
			</div>
		</div>
		<!-- end dados-loja  (usado no google-mapa) -->
		
		<?php 
			$ultimo_estado = $loja['Loja']['estado'];
		?>
	</div>
	<!-- end content-loja -->
	<?php endForeach; ?>
<?php endIf; ?>


<!-- start script google maps -->
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">

//seto as variaveis utilizadas
var image = '';
var gmarkers = [];
var btnForm = '';

//coordenadas do Brasil, para centralizar o mapa no Brasil
var myLatlng = new google.maps.LatLng(-14.235004, -51.92528);

//defino as configuracoes iniciais
var myOptions = {
	zoom: 4,
	center: myLatlng,
	mapTypeId: google.maps.MapTypeId.ROADMAP
}

//renderizo o mapa na div, com as configuracoes definidas
var map = new google.maps.Map(document.getElementById("map"), myOptions);

//instancio os 'baloes', dos pins
var infowindow = new google.maps.InfoWindow({
	content: '',
	maxWidth : 450
});

//moto o array com as coordenadas e o id da loja
var waypoints = [
<?php foreach($pagina_element_content as $loja): ?>
	<?php if($loja['Loja']['coordenadas'] != ""): ?>
		[<?php e($loja['Loja']['coordenadas']); ?>,<?php e($loja['Loja']['id']); ?>],
	<?php endIf; ?>
<?php endForeach; ?>
];

//seto os locais no mata, de acordo com a coordena
if (waypoints.length > 1) {
	for (var i = 0; i < waypoints.length; i++) {
		//coordena atual
		var atual = ""+waypoints[i]+"";
		//separo as coordenas e o id da loja
		var coord1 = atual.split(',')[0];
		var coord2 = atual.split(',')[1];
		var id = atual.split(',')[2];
		//capturo as informacoes da loja, que esta na listagem de loja da pagina
		html = ''+ $("#dados-loja-"+id).html() + '';
		//crio o marcador e seto no mapa
		var myLatLng = new google.maps.LatLng(coord1,coord2);		
		var marker = createMarker(myLatLng,id,html);
		marker.setMap(map);
	}
}	

//metodo que cria o marcador no mapa
function createMarker(point,id,html) {
	
	var image = "img/site/ic_location_mapa.png";
	
	var marker = new google.maps.Marker({
		  position: point,
		  map: map,
		  icon: image,
		  animation: google.maps.Animation.DROP,
	});
	
	google.maps.event.addListener(marker, 'click', function() {
	  infowindow.setContent(html);
	  infowindow.open(map, marker);
	  toggleBounce();
	});
	
	gmarkers.push(marker);
	//seto no nome da loja, o evento click do link para a exibicao dos dados no mapa
	btnForm = document.getElementById("btn-loja-"+id);
    btnForm.setAttribute("onclick","javascript:click_link(" + (gmarkers.length-1) + ");");
	
	return marker;
}

//metodo de exibe a windowinfo, quando eh clicaco no nome da loja
function click_link(i) {
	google.maps.event.trigger(gmarkers[i], 'click');
}

//metodo de animacao dos bullets
function toggleBounce() {
	if (marker.getAnimation() != null) {
  		marker.setAnimation(null);
	} else {
  		marker.setAnimation(google.maps.Animation.BOUNCE);
	}
}
</script>
<!-- end script google maps -->