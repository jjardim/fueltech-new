<?php echo $this->Form->create('Sac', array('class' => 'form3', 'url' => $this->Html->url(null,true))); ?>
	<ul class="contato">
		<li>
			<?php echo $this->Session->flash(); ?>
		</li>
		<li>
			<?php echo $this->Form->input('Sac.nome', array('class' => 'inputfield ', 'label' => false, 'div' => false, 'default' => 'Nome*')); ?>
		</li>
		<li>
			<?php echo $this->Form->input('Sac.email', array('class' => 'inputfield ', 'label' => false, 'div' => false, 'default' => 'E-mail*')); ?>
		</li>
		<li>
			<?php echo $this->Form->input('Sac.mensagem', array('value'=>'', 'label' => false,'type' => 'textarea','class'=>'textarea ', 'div' => false, 'default' => 'Mensagem')); ?>
		</li>
		<li>
			<?php echo $this->Form->input('Sac.tipo', array('value'=> $tipo_form, 'type' => 'hidden', 'div' => false)); ?>
		</li>
		<li>
			<span>* Campos Obrigatórios</span>
		</li>
		<li class="clear">
		   <?php echo $this->Form->submit('Enviar', array( 'class' => 'button', 'value' => '')); ?>
		</li>
	</ul>
<?php echo $this->Form->end(); ?>