<?php echo $this->Javascript->link('common/jquery.meio_mask.js'); ?>
<script>
  $(document).ready(function(){
    $('.mask-telefone').setMask({mask:'(99) 9999-99999'});
  });
</script>

<?php if( isset($pagina_element_content) ){
  $tipo_form = $pagina_element_content['tipo_form'];
} ?>

<!-- start fill-form -->
<div class="fill-form" style="margin: 0px; width: auto;">
  <?php echo $this->Form->create('Ouvidoria', array('class' => 'fillform2', 'type' => 'file', 'url' => $this->Html->url(null,true))); ?>
  <div class="ouvidoria">

  <p>
      A FuelTech tem como princípio oferecer um atendimento com Qualidade, Excelência e Seriedade na
      Prestação de Serviços, visando a Satisfação total de seus clientes. Estamos à sua disposição
      sempre que necessário para esclarecer dúvidas, fazer comentários ou ainda contribuir com
      sugestões ou críticas para melhoria de nossos serviços.
    </p>
    <!-- start left -->
    <div class="left">
      <?php echo $this->Session->flash(); ?>
    </div>
    <!-- end left -->
    <div class="clear"></div>
    <!-- start left -->
    <div class="left">
      <?php echo $this->Form->input('Ouvidoria.nome', array('class' => 'input1', 'label' => 'Nome*', 'div' => false, 'rel' => 'Nome*')); ?>
    </div>
    <!-- end left -->
    <div class="clear"></div>
    <!-- start left -->
    <div class="left">
      <?php echo $this->Form->input('Ouvidoria.email', array('class' => 'input1', 'label' => 'E-mail*', 'div' => false, 'rel' => 'E-mail*')); ?>
    </div>
    <!-- end left -->
    <div class="clear"></div>
    <!-- start left -->
    <div class="left">
      <?php echo $this->Form->input('Ouvidoria.telefone', array('class' => 'input1 mask-telefone', 'label' => 'Telefone*', 'maxlength' => '15', 'div' => false, 'rel' => 'Telefone*')); ?>
    </div>
    <!-- end left -->
    <div class="clear"></div>
    <!-- start left -->
    <div class="left">
      <?php echo $this->Form->input('Ouvidoria.celular', array('class' => 'input1 mask-telefone', 'label' => 'Celular*', 'maxlength' => '15', 'div' => false, 'rel' => 'Celular*')); ?>
    </div>
    <!-- end left -->
    <div class="clear"></div>
    <!-- start left -->
    <div class="left">
      <?php echo $this->Form->input('Ouvidoria.cidade', array('class' => 'input1', 'label' => 'Cidade', 'div' => false, 'rel' => 'Cidade')); ?>
    </div>
    <!-- end left -->
    <div class="clear"></div>
    <!-- start left -->
    <div class="left">
      <?php echo $this->Form->input('Ouvidoria.estado', array('label'=> 'Estado','class' => 'select3',  'options' => $this->Estados->estadosBrasileiros(),'div' => false, 'rel' => 'Estado' )) ?>
    </div>
    <!-- end left -->
    <div class="clear"></div>

    <!-- start left -->
    <div class="left assunto">
      <p>Assunto</p>
      <?php echo $this->Form->radio('Ouvidoria.assunto', array('Dúvida' => 'Dúvida', 'Elogio' => 'Elogio', 'Sugestão' => 'Sugestão', 'Crítica' => 'Crítica'), array('legend' => false)
      ); ?>
    </div>

    <!-- end left -->
    <div class="clear"></div>

    <!-- start left -->
    <div class="left">
      <?php echo $this->Form->input('Ouvidoria.mensagem', array('label' => 'Mensagem* ','type' => 'textarea','class'=>'textarea input1', 'div' => false, 'placeholder' => 'Mensagem')); ?>
    </div>
    <!-- end left -->
    <div class="clear"></div>
    <!-- start left -->
    <div class="left">
      <?php echo $this->Form->input('Ouvidoria.sac_tipo_id', array('value'=> $tipo_form, 'type' => 'hidden', 'div' => false)); ?>
    </div>
    <!-- end left -->
    <div class="clear"></div>
    <div class="row">
      <?php
      echo $this->Form->input('Ouvidoria.filename', array('label' => '', 'type' => 'file', 'class' => ''));
      ?>
    </div>
    <div class="clear"></div>
    <!-- start left -->
    <div class="left" style='margin-top: 15px;'>
      <span>* Campos Obrigatórios</span>
    </div>
    <!-- end left -->
    <div class="clear"></div>
    <br />
    <div class="clear"></div>
    <!-- start left -->
    <div class="left">
      <?php if (isset($tipo_form) && $tipo_form == '13') {
          //echo $this->Form->submit('Enviar', array( 'onClick' => "_gaq.push(['_trackEvent', 'Button', 'Click', 'Fale Conosco - Enviar'])", 'class' => 'button', 'value' => ''));
        echo $this->Form->submit('Enviar', array( 'class' => 'button', 'value' => '', 'onClick' => "_gaq.push(['_trackEvent', 'Button', 'Click', 'Ouvidoria - Enviar'])"));

      }else{
        echo $this->Form->submit('Enviar', array( 'class' => 'button', 'value' => ''));
      }
      ?>
    </div>
    <!-- end left -->
  </div>
  <?php echo $this->Form->end(); ?>
</div>
<!-- end fill-form -->
<div class="clear"></div>