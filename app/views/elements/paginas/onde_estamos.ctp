<?php 
	$ultimo_estado = "";
	if(isset($pagina_element_content)): 
	$cont = 1;
?>
	<!-- start content-loja -->
	<ul class="content-loja">
	
	<?php foreach($pagina_element_content['lojas'] as $loja): ?>
		
		<li>
	
		<div id="dados-loja-<?php e($loja['Loja']['id']); ?>">
			<div class="dados-lojas-content">
				<div style="display: block; float: left; width: 270px;">
					<h2><?php e($this->Estados->getNomeEstado($loja['Loja']['estado'])); ?> - <?php e($loja['Loja']['cidade']); ?></h2>
				</div>
				<?php if($loja['Loja']['thumb_filename'] != ""): ?>
					<span><?php echo $image->resize( ( $loja['Loja']['thumb_filename'] ) ? 'uploads/lojas/'.$loja['Loja']['thumb_filename'] : 'uploads/produto_imagem/filename/sem_imagem.jpg', 300, 150); ?></span>
				<?php endIf; ?>
				<div style="display: block; float: left; width: 270px;">
					<?php e($loja['Loja']['conteudo']); ?>
				</div>
			</div>
		</div>
		
		<?php 
			$ultimo_estado = $loja['Loja']['estado'];
		?>
		
		</li>
		
		<?php 
			if($cont%2 == 0){
		?>
			</ul>
			<!-- end content-loja -->
			<!-- start content-loja -->
			<ul class="content-loja">
		<?php
			}
		?>
		
	<?php 
		$cont++;
		endForeach; 
	?>
	
	</ul>
	<!-- end content-loja -->
	
<?php endIf; ?>