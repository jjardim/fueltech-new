<!-- start content -->
<div class="content product-box">
	<?php echo $this->Form->create('Veiculo', array( 'id' => 'form-veiculo', 'url' => $this->Html->url(null,true), 'type' => 'file' ) );?>
	<?php echo $this->Session->flash(); ?>
	<!-- start form box -->
	<div class="form-box">
		<h3 class="heading">Cadastre seu veículo</h3>
		<div class="row">
			<?php echo $this->Form->input('veiculo', array('label'=>'Veículo:','class'=>'input','div' => false)); ?>
			<div id="veiculo-msg-error" class="form-min-msg-error"></div>
		</div>
		<div class="row">
			<?php echo $this->Form->input('proprietario', array('label'=>'Proprietário:','class'=>'input','div' => false)); ?>
			<div id="proprietario-msg-error" class="form-min-msg-error"></div>
		</div>
		<div class="row">
			<?php echo $this->Form->input('preparador', array('label'=>'Preparador:','class'=>'input','div' => false)); ?>
		</div>
		<div class="row">
			<?php echo $this->Form->input('cilindradas', array('label'=>'Cilindrada/Nº de Válvulas:','class'=>'input','div' => false)); ?>
			<div id="cilindradas-msg-error" class="form-min-msg-error"></div>
		</div>
		<div class="row">
			<?php echo $this->Form->input('potencia_estimada', array('label'=>'Potência estimada ou aferida:','class'=>'input','div' => false)); ?>
			<div id="potencia-estimada-msg-error" class="form-min-msg-error"></div>
		</div>
		<div class="row">
			<?php echo $this->Form->input('dinamometro', array('label'=>'Dinamômetro utilizado:','class'=>'input','div' => false)); ?>
		</div>
		<div class="row">
			<?php echo $this->Form->input('comando_valvulas', array('label'=>'Comando de válvulas:','class'=>'input','div' => false)); ?>
			<div id="comando-valvulas-msg-error" class="form-min-msg-error"></div>
		</div>
		<div class="row">
			<?php echo $this->Form->input('injetores', array('label'=>'Injetores:','class'=>'input','div' => false)); ?>
			<div id="injetores-msg-error" class="form-min-msg-error"></div>
		</div>
		<div class="row">
			<?php echo $this->Form->input('combustivel', array('label'=>'Combustível:','class'=>'input','div' => false)); ?>
			<div id="combustivel-msg-error" class="form-min-msg-error"></div>
		</div>
		<div class="row">
			<?php echo $this->Form->input('coletor_admissao', array('label'=>'Coletor de admissão:','class'=>'input','div' => false)); ?>
		</div>
		<div class="row">
			<?php echo $this->Form->input('coletor_escape', array('label'=>'Coletor de escape:','class'=>'input','div' => false)); ?>
		</div>
		<div class="row">
			<?php echo $this->Form->input('turbina', array('label'=>'Turbina:','class'=>'input','div' => false)); ?>
		</div>
		<div class="row">
			<?php echo $this->Form->input('pressao_utilizada', array('label'=>'Pressão utilizada:','class'=>'input','div' => false)); ?>
		</div>
		<div class="row">
			<?php echo $this->Form->input('bobina', array('label'=>'Bobina:','class'=>'input','div' => false)); ?>
			<div id="bobina-msg-error" class="form-min-msg-error"></div>
		</div>
	</div>
	<!-- end form box -->
	<!-- start form box -->
	<div class="form-box2" id="content-galeria-veiculos">
		<h3 class="heading">Fotos</h3>
		 	
			<div class="content-fotos-veiculo">
				<div class="row">
								<small>Extensões permitidas: BMP, JPG, PNG</small>
					<?php 
						echo $this->Form->input('GaleriaFoto.0.filename', array('hiddenField'=>false,'label' => false,'class' => 'input file-original', 'type' => 'file', 'div' => false));	
						echo $this->Form->input('GaleriaFoto.0.dir', array('type' => 'hidden'));
						echo $this->Form->input('GaleriaFoto.0.mimetype', array('type' => 'hidden'));
						echo $this->Form->input('GaleriaFoto.0.filesize', array('type' => 'hidden'));
					?>
					<ul class="div-input-falso">
						<li><input name="file-falso" type="text" class="file-falso" value="" /></li>
					</ul>
				</div>
			</div>
			<div class="content-fotos-veiculo-tmp"></div>
			
			<div class="row">
				<a href="javascript:void(0);" class="button9 btn-add-foto">ADICIONAR</a>
			</div>
			<div class="row2">
				<input name="ENVIAR" type="submit" value="SALVAR SEU VEÍCULO" class="button10" />
			</div>
	</div>
	<!-- end form box -->
	<div class="clear"></div>   
	<?php echo $this->Form->end(); ?>
</div>
<!-- end content -->
<div class="clear"></div>
