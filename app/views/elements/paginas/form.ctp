<?php if( isset($pagina_element_content) ){
	$tipo_form = $pagina_element_content['tipo_form'];
} ?>

<p class="btns-revendedores">
    <a href="<?php echo $this->Html->url('/seja-um-revendedor') ?>" style="width: 140px; float: right;">Seja um revendedor</a>
</p>

<p class="btns-trabalhe-conosco">
    <a href="<?php echo $this->Html->url('/trabalhe-conosco') ?>" style="width: 140px; float: right;">Trabalhe Conosco</a>
</p>

<!-- start fill-form -->
<div class="fill-form" style="margin: 0px; width: auto;">
	<?php echo $this->Form->create('Sac', array('class' => 'fillform2', 'url' => $this->Html->url(null,true))); ?>
		<div class="contato">
			<!-- start left -->
			<div class="left">
				<?php echo $this->Session->flash(); ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
			<!-- start left -->
			<div class="left">
				<?php echo $this->Form->input('Sac.nome', array('class' => 'input1', 'label' => 'Nome*', 'div' => false, 'rel' => 'Nome*')); ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
			<!-- start left -->
			<div class="left">
				<?php echo $this->Form->input('Sac.email', array('class' => 'input1', 'label' => 'E-mail*', 'div' => false, 'rel' => 'E-mail*')); ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
			<!-- start left -->
			<div class="left">
				<?php echo $this->Form->input('Sac.telefone', array('class' => 'input1 mask-telefone', 'label' => 'Telefone', 'maxlength' => '15', 'div' => false, 'rel' => 'Telefone')); ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
			<!-- start left -->
			<div class="left">
				<?php echo $this->Form->input('Sac.estado', array('label'=> 'Estado','class' => 'select3',  'options' => $this->Estados->estadosBrasileiros(),'div' => false, 'rel' => 'Estado' )) ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
			<!-- start left -->
			<div class="left">
				<?php echo $this->Form->input('Sac.cidade', array('class' => 'input1', 'label' => 'Cidade', 'div' => false, 'rel' => 'Cidade')); ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
			<!-- start left -->
			<div class="left">
				<?php echo $this->Form->input('Sac.mensagem', array('value'=>'', 'label' => 'Mensagem* ','type' => 'textarea','class'=>'textarea input1', 'div' => false, 'default' => 'Mensagem')); ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
			<!-- start left -->
			<div class="left">
				<?php echo $this->Form->input('Sac.sac_tipo_id', array('value'=> $tipo_form, 'type' => 'hidden', 'div' => false)); ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
			<!-- start left -->
			<div class="left">
				<span>* Campos Obrigatórios</span>
			</div>
			<div class="clear"></div>

			<div class='left'>
				<?php $recaptcha->display_form('echo'); ?>
			</div>
			<!-- end left -->
			<div class="clear"></div>
			<br />
			<div class="clear"></div>
			<!-- start left -->
			<div class="left">
				<?php if (isset($tipo_form) && $tipo_form == '8') {
					//echo $this->Form->submit('Enviar', array( 'onClick' => "_gaq.push(['_trackEvent', 'Button', 'Click', 'Fale Conosco - Enviar'])", 'class' => 'button', 'value' => ''));
					echo $this->Form->submit('Enviar', array( 'class' => 'button', 'value' => '', 'onClick' => "_gaq.push(['_trackEvent', 'Button', 'Click', 'Fale Conosco - Enviar'])"));
																												
				}else{
					echo $this->Form->submit('Enviar', array( 'class' => 'button', 'value' => ''));
				}
			   ?>		   
			</div>
			<!-- end left -->
		</div>
	<?php echo $this->Form->end(); ?>
</div>
<!-- end fill-form -->
<div class="clear"></div>