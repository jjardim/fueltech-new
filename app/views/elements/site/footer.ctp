<!-- start footer -->
<div id="footer">
	<!-- start box -->
    <div class="box">
    	<!-- start footer top -->
        <div id="footer-top">
        	<!-- start toplink -->
            <div class="toplink">
            	<ul>
                	<li><a href="<?php echo $this->Html->Url('/produtos') ?>" title="<?php echo __("PRODUTOS"); ?>"><?php echo __("PRODUTOS"); ?></a></li>
                    <li><a href="<?php echo $this->Html->Url('/manutencao-atualizacao') ?>" title="<?php echo __("SUPORTE"); ?>"><?php echo __("SUPORTE"); ?></a></li>
                    <li class="last"><a href="<?php echo $this->Html->Url('/sobre-a-empresa') ?>" title="<?php echo __("MUNDO FUELTECH"); ?>"><?php echo __("MUNDO FUELTECH"); ?></a></li>
                </ul>
            </div>
            <!-- end toplink -->
            <!-- start search -->
            <?php echo $this->Form->create('BuscaFooter', array('class'=>'search header_form','id' => 'header_form', 'url' => '/busca/index', 'type' => 'post')); ?>
				<?php echo $this->Form->input('busca', array('rel' => __('Busque no site', true), 'label' => '', 'div' => false, 'class' => 'input BuscaBusca', 'value' => (isset($this->data['Busca']['busca']) && !empty($this->data['Busca']['busca'])) ? $this->data['Busca']['busca'] : __('Busque no site', true))); ?>
				<input name="submit" type="submit" class="button" value="" style="display: none;" />
			<?php echo $this->Form->end(); ?>
            <!-- end search -->
            <!-- start info-link -->
            <div class="info-link">
            	<ul>
                	<li><a href="http://www.fueltech.com.br/livezilla/chat.php" target="_blank"" title="<?php echo __("ATENDIMENTO"); ?> ONLINE"><span><?php echo __("ATENDIMENTO"); ?></span><span>ONLINE</span></a></li>
                    <li><a href="<?php echo $this->Html->Url('/fale-conosco'); ?>" style="width: 49px; display: block;"  title="<?php echo __("Entre em contato"); ?>"><span><?php echo __("Entre em contato"); ?></span></a></li>
                    <li class="last"><span><?php echo __("LIGUE PARA NÓS"); ?></span><span><?php echo Configure::read('Loja.televendas')?></span></li>
                </ul>
            </div>
            <!-- end info-link -->
        </div>
        <!-- end footer top -->
		<?php 
			foreach( $paginas_lista as $paginas ):
				if($paginas['Pagina']['categoria'] == 'Mundo FuelTech')
					$paginas_mundo_fueltech[] = $paginas;
				elseif( $paginas['Pagina']['categoria'] == 'Suporte' )
					$paginas_suportes[] = $paginas;				
			endForeach;
		?>
        <!-- start footer-mid -->
		<div id="footer-mid">
        	<!-- start footer link -->
            <div class="footer-link">
            	<h2 class="title"><span><?php echo __("PRODUTOS"); ?></span></h2>
                <ul>
					<?php 
						if(isset($menu_topo) && count($menu_topo) > 0): 
							$cont = 1;
							$qnt = count($menu_topo);
							foreach($menu_topo as $mt):
								if($cont == $qnt){
									$class = "class='last'";
								}else{
									$class = "";
								}
						  ?>
							<li <?php echo $class; ?>><a href="<?php echo $this->Html->Url('/categorias/'.$mt['Categoria']['seo_url']); ?>" title="<?php echo $mt['CategoriaDescricao']['nome']; ?>"><?php echo $mt['CategoriaDescricao']['nome']; ?></a></li>
					<?php 
							endForeach;
					endIf; ?>
				</ul>
            </div>
            <!-- end footer link -->
            <!-- start footer link -->
            <div class="footer-link second">
            	<h2 class="title"><span><?php echo __("SUPORTE"); ?></span></h2>
               <?php if(!empty($paginas_suportes)): ?>
					<ul>	
					<?php foreach($paginas_suportes as $pag): ?>
						<li><a href="<?php echo $this->Html->Url('/'.$pag['Pagina']['url']); ?>" title="<?php echo $pag['PaginaDescricao']['nome']; ?>"><?php echo $pag['PaginaDescricao']['nome']; ?></a></li>
					<?php endForeach; ?>
					</ul>
				<?php endIf; ?>
            </div>
            <!-- end footer link -->
            <!-- start footer link -->
            <div class="footer-link third">
            	<h2 class="title"><span><?php echo __("MUNDO FUELTECH"); ?></span></h2>
				<?php if(!empty($paginas_mundo_fueltech)): ?>
					<ul>	
					<?php foreach($paginas_mundo_fueltech as $pag): ?>
						<li><a href="<?php echo $this->Html->Url('/'.$pag['Pagina']['url']); ?>" title="<?php echo $pag['PaginaDescricao']['nome']; ?>"><?php echo $pag['PaginaDescricao']['nome']; ?></a></li>
					<?php endForeach; ?>
					</ul>
				<?php endIf; ?>
            </div>
            <!-- end footer link -->
			<!-- start footer right -->
            <div id="footer-right">
				<a href="<?php e($this->Html->Url('/')) ?>" title="Fuel Tech" class="logo-footer">
					<?php echo $this->Html->image('site/'.Configure::read('Config.language').'/logo_footer.png', array('alt' => 'Fuel Tech'))?>
				</a>
            	<ul class="social">
                	<!--<li><a href="<?php echo Configure::read('Loja.facebook'); ?>" title="Facebook" target="_blank"><?php echo $this->Html->image('site/ic_facebook.png', array('alt' => 'Facebook', 'width' => '40', 'height' => '40'))?></a></li>-->
                    <li><a href="<?php echo Configure::read('Loja.twitter'); ?>" title="Twitter" target="_blank"><?php echo $this->Html->image('site/ic_twitter.png', array('alt' => 'Twitter', 'width' => '40', 'height' => '40'))?></a></li>
                    <li><a href="<?php echo Configure::read('Loja.youtube'); ?>" title="Youtube" target="_blank"><?php echo $this->Html->image('site/ic_youtube.png', array('alt' => 'Youtube', 'width' => '40', 'height' => '40'))?></a></li>
                </ul>
            	<div class="clear"></div>
                <address><?php echo Configure::read('Loja.endereco'); ?></address>
        	</div>	
            <!-- end footer right -->
			<!-- start footer link -->
			<div class="footer-link four" id="box_facebook" style="width: 268px; padding: 0px; margin-left: 4px;">
				<div id="fb-root"></div>
				<script>(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=177474058989879";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));</script>
				<div class="fb-like-box" data-href="http://www.facebook.com/FuelTechEMS" data-width="288" data-height="210" data-show-faces="true" data-stream="false" data-header="true"></div>
			</div>
			<!-- end footer link -->
			<div class="clear"></div>
        </div>
        <!-- end footer-mid -->
        <div class="clear"></div>
    </div>
    <!-- end box -->
    <!-- start footer-bottom -->
    <div id="footer-bottom">
    	<!-- start box -->
        <div class="box">
            <?php 
                    if (isset($seo_institucional)):
                            echo "<p>" . $seo_institucional . "</p>";
                     else:
                            echo "<p>" . Configure::read('Loja.seo_institucional') . "</p>";					
                    endIf;
            ?>
            <p style="text-align: center;">© Copyright <?php echo date('Y'); ?> FuelTech LTDA EPP - Todos direitos reservados. Produzido por <a href="http://www.reweb.com.br" target="_blank">Reweb</a></p>
        </div>
        <!-- end box -->
    </div>
    <!-- end footer-bottom -->
</div>
<!-- end footer -->