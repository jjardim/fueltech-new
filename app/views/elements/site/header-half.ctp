    <?php
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <?php
        echo $this->Html->charset();
        ?>
        <title>
            <?php if (isset($seo_title)):
				echo $seo_title;
			else: ?>
			<?php
				if (isset($title_for_layout_produto)) {
					echo $title_for_layout_produto;
				} else {
					if($this->params['controller']!="home")
						echo $title_for_layout . ' - ' . Configure::read('Loja.nome');
					else
						echo Configure::read('Loja.nome');
				}
				?>
			<?php endIf; ?>
        </title>
		
		<?php if (isset($seo_meta_description)):?>
		   <meta name="description" content="<?php echo $seo_meta_description; ?>" />
		<?php else: ?>
			<meta name="description" content="<?php echo Configure::read('Loja.seo_meta_description'); ?>" />
		<?php endIf; ?>
		
		<?php if (isset($seo_meta_keywords)):?>
		   <meta name="keywords" content="<?php echo $seo_meta_keywords; ?>" />
		   <?php else:?>
		   <meta name="keywords" content="<?php echo Configure::read('Loja.seo_meta_keywords'); ?>" />
		<?php endIf; ?>
		
        <?php echo $this->element('common/js_path'); ?>
        <?php
            echo $this->Html->meta('icon');
            echo $this->Javascript->link('common/jquery.js');
            echo $this->Javascript->link('common/jquery-ui-1.8.16.custom.min.js');
            echo $this->Javascript->link('common/use_default.js');
            echo $this->Javascript->link('common/jquery.slideshow');
            echo $this->Javascript->link('common/jcarousel/jquery.jcarousel.min.js');
            echo $this->Javascript->link('site/common/index.js');
			echo $this->Javascript->link('site/common/https');
            echo $this->Html->css('site/style.css');
			?>
		<!--[if lte IE 7]>
		<?php
            echo $this->Html->css('site/ie7.css');
        ?>
		<![endif]--> 
        
        <?php
            echo $scripts_for_layout;
        ?>
    </head>
	<body>

	<?php
		echo $this->Session->flash();
		if(isset($referer)&&$referer=='/login'){
			echo $this->Session->flash('auth');
		}		
	?>
     <?php 
				if( ($this->params['controller'] != "produto" && $this->params['action'] != "detalhe")
					&& ($this->params['controller'] != "usuarios" && $this->params['action'] != "edit")
					&& $this->params['action'] != "login" && $this->params['controller'] != "carrinho" 
					&& $this->params['controller'] != "usuario_enderecos"
				): 
			?>
		
			<!-- start left -->
			<?php echo $this->element('site/left'); ?>
			<!-- end left -->
			
			<?php endif; ?>
        
        
			<?php 
				if( $this->params['action'] != "login" && $this->params['action'] != "entrega" && $this->params['action'] != "forma_pagamento" && $this->params['action'] != "finalizacao" ): 
			?>
			
				
			<?php else: ?>
				<!-- start box-100 -->
				<div class="box-100">
			<?php endIf; ?>
			
			<?php echo $content_for_layout; ?>
			
		
		
	</div>
	<!-- end container -->
	
	</div>
	
	 
		<?php if(Configure::read('Reweb.analytics_id')!=""): ?>  
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', '<?php echo Configure::read('Reweb.analytics_id'); ?>']);
			_gaq.push(['_trackPageview']);

		  (function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();

		</script>
	<?php endIf; ?>
	</body>
</html>