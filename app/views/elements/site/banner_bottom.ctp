<?php 
if(!empty($banner_bottom)): 
    $width = isset($width)?$width:306;
    $height = isset($height)?$height:136;
?>
	<!-- start pro_box -->
	<div class="pro_box">
        <?php
        foreach($banner_bottom as $indice => $banners):
			?>
				<?php
					if($banners['Banner']['filename']!=""&&$banners['Banner']['mimetype']=='application/x-shockwave-flash'){
						echo $flash->renderSwf($banners['Banner']['dir'].DS.$banners['Banner']['filename'], $width, $height,false);
					}elseif($banners['Banner']['filename']!=""){
						if($banners['Banner']['link']){
							echo $this->Html->link($image->resize($banners['Banner']['dir'].DS.$banners['Banner']['filename'],$width, $height,false),$banners['Banner']['link'],array('escape'=>false));
						}else{
							echo $image->resize($banners['Banner']['dir'].DS.$banners['Banner']['filename'],$width, $height,false);
						}
					}
				?>
			<?php
        endForeach;
        ?>
	</div>
	<!-- end pro_box -->
<?php
endIf;
?>
