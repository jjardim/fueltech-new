<?php 
if(isset($banner_top) && !empty($banner_top)):
    $width = isset($width)?$width:965;
    $height = isset($height)?$height:290;
?>
	<!-- start showcase -->
	<div id="showcase">
	
		<!-- start banner-topo -->
		<div id="banner-topo">		
			<?php if(count($banner_top)>1){ ?>
				<div class="control-nav">
					<ul class="pager">
						<?php foreach($banner_top as $indice => $banners): ?>
							<li><a href="javascript:;" title=""><?php echo ($indice+1); ?></a></li>
						<?php endForeach; ?>
					</ul>
				</div>
			<?php } ?>			
			<ul class="carousel_topo">
				<?php foreach($banner_top as $indice => $banners): ?>
					<li>
						<?php
						if($banners['Banner']['filename']!=""&&$banners['Banner']['mimetype']=='application/x-shockwave-flash'){
							echo $flash->renderSwf($banners['Banner']['dir'].DS.$banners['Banner']['filename'], $width, $height,false);
						}elseif($banners['Banner']['filename']!=""){
							 if($banners['Banner']['link']){
								echo $this->Html->image('/uploads/banner/filename/'.$banners['Banner']['filename'], array('alt'=>Inflector::slug($banners['Banner']['filename'], ' '), 'url'=>$banners['Banner']['link']));
							}else{
								echo $this->Html->image('/uploads/banner/filename/'.$banners['Banner']['filename'], array('alt' => Inflector::slug($banners['Banner']['filename'], ' ')));
							}						
						}
						?>
					</li>
				<?php endForeach; ?>
			</ul>			
		</div>
		<!-- end banner-topo -->
		
	</div>
	<!-- end showcase -->
<?php
endIf;
?>