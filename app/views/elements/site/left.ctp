<!-- start leftcol
<div id="leftcol">-->

<!-- start column1st -->

<?php
//GAMBA DE FORMATACAO

if($this->params['controller'] != "paginas" && $this->params['controller'] != "noticias"):
    ?>
    <div class="column1st">
    <?php else: ?>
        <div class="column1st" style="border: 0 none;">
        <?php endIf; ?>

        <?php
        if (
                ($this->params['controller'] != "paginas") && $this->params['controller'] != "carrinho" && $this->params['controller'] != "contato" && $this->params['controller'] != "usuarios" && $this->params['controller'] != "usuario_enderecos" && $this->params['controller'] != "noticias" && $this->params['controller'] != "videos" && $this->params['controller'] != "veiculos"
        ):
            ?>

            <?php
            if (isset($breadcrumb[0]['nome'])) {
                $categoriaPrincipal = $breadcrumb[0]['nome'] != "" ? $breadcrumb[0]['nome'] : "Produtos";
                $urlPrincipal = $breadcrumb[0]['url'] != "" ? $breadcrumb[0]['url'] : "";
            } else {
                $categoriaPrincipal = "Produtos";
                $urlPrincipal = "";
            }
            ?>

            <?php if ($this->params['controller'] != 'busca' && $this->params['controller'] != 'usuarios'): ?>
                <?php
                //trato o array, separando pelas categorias pais
                //debug($menu);
                foreach ($menu as $valor) {
                    if ($valor['Categoria']['parent_id'] == 0) {
                        if ($valor['Categoria']['seo_url'] == 'ftx') {
                            $menu_ftx[] = $valor;
                        } elseif ($valor['Categoria']['seo_url'] == 'fueltech') {
                            $menu_fueltech[] = $valor;
                        }
                    } else {
                        $menu_categoria[] = $valor;
                    }
                }
                ?>

                <?php if (isset($menu_fueltech) && count($menu_fueltech) > 0): ?>
                    <!-- start subnav -->
                    <a href="<?php echo $this->Html->Url('/downloads/') . 'download/116/catalogo-de-produtos-2015/'; ?>" title="<?php echo $this->String->title_case('Catálogo - Baixe aqui'); ?>">
                        <?php echo $this->Html->image('site/miniatura_catalogo_br.jpg', array('alt' => 'Fuel Tech', 'width' => '205', 'height' => '88'))?>
                    </a>
                    <div class="subnav second"
                    <?php
                     //gamba de formatacao
                     if(!isset($niveis) || $niveis != 2): ?>
                     style="border-top: 5px solid #CCCCCC; margin-top: 15px"
                     <?php endIf; ?>
                    >
                        <h2 class="title"><span><?php echo __("PRODUTOS"); ?></span></h2>
                        <ul>
                            <?php foreach ($menu_fueltech as $valor): ?>
                                <?php
                                foreach ($valor['SubCategory'] as $sub):
                                    if (isset($categoria_data) && $sub['id'] == $categoria_data['Categoria']['id']) {
                                        $class = "class='active'";
                                    } else {
                                        $class = "";
                                    }
                                    ?>
                                    <li><a <?php echo $class; ?> href="<?php echo $this->Html->Url('/categorias/') . $sub['seo_url']; ?>" title="<?php echo $this->String->title_case($sub['nome']); ?>"><?php echo $sub['nome']; ?></a></li>
                                    <?php
                                endforeach;
                                ?>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <!-- end subnav -->
                    <?php endIf; ?>

                <?php if (isset($menu_ftx) && count($menu_ftx) > 0): ?>
                    <!-- start subnav -->
                    <div class="subnav second"
                        <?php
                         //gamba de formatacao
                         if(!isset($niveis) || $niveis != 2): ?>
                         style="border-top: 5px solid #CCCCCC; margin-top: 15px"
                         <?php endIf; ?>
                    >
                        <h2 class="title"><span><?php echo __("FTX"); ?></span></h2>
                        <ul>
                            <?php foreach ($menu_ftx as $valor): ?>
                                <?php
                                foreach ($valor['SubCategory'] as $sub):
                                    if (isset($categoria_data) && $sub['id'] == $categoria_data['Categoria']['id']) {
                                        $class = "class='active'";
                                    } else {
                                        $class = "";
                                    }
                                    ?>
                                    <li><a <?php echo $class; ?> href="<?php echo $this->Html->Url('/categorias/') . $sub['seo_url']; ?>" title="<?php echo $this->String->title_case($sub['nome']); ?>"><?php echo $sub['nome']; ?></a></li>
                                    <?php
                                endforeach;
                                ?>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <!-- end subnav -->
                    <?php endIf; ?>

                <?php if (isset($menu_categoria) && count($menu_categoria) > 0): ?>
                    <!-- start subnav -->
                    <div class="subnav second">
                        <h2 class="title"><span><?php echo __("PRODUTOS"); ?></span></h2>
                        <ul>
                            <?php foreach ($menu_categoria as $valor): ?>

                                <li>
                                    <?php if( ((isset($subNivel)) && ($subNivel == true)) || ($niveis>3)): ?>
                                        <a href="<?php e($this->Html->Url("/categorias/fueltech/ft-by-expert")); ?>" title="<?php echo __("FT by Expert"); ?>"><?php echo __("FT by Expert"); ?></a>
                                        <span class="arrow_right"></span>
                                    <?php endif; ?>

                                    <a href="<?php echo $this->Html->Url('/categorias/') . $valor['Categoria']['seo_url']; ?>" title="<?php echo $this->String->title_case($valor['Categoria']['nome']); ?>"><?php echo $valor['Categoria']['nome']; ?></a>
                                    <ul>
                                        <?php
                                        foreach ($valor['SubCategory'] as $sub):
                                            if (isset($categoria_data) && $sub['id'] == $categoria_data['Categoria']['id']) {
                                                $class = "class='active'";
                                            } else {
                                                $class = "";
                                            }
                                            ?>
                                            <li><a <?php echo $class; ?> href="<?php echo $this->Html->Url('/categorias/') . $sub['seo_url']; ?>" title="<?php echo $this->String->title_case($sub['nome']); ?>"><?php echo $sub['nome']; ?></a></li>
                                            <?php
                                        endforeach;
                                        ?>
                                    </ul>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <!-- end subnav -->
                    <?php endIf; ?>
                <?php endIf; ?>

            <form>
                <input class="filter-current" name="busca-current" type="hidden" value="<?php echo $this->Html->url(null, true); ?>" />
            </form>

            <?php if ($this->params['controller'] == 'categorias' && (isset($niveis) && $niveis >= 3)): ?>
                <!-- start block1 -->
                <div class="block1">
                    <?php echo $this->Html->image('/img/site/carregando_gif.gif', array('class' => 'loading', 'style' => 'visibility:hidden; margin-left: 0px; margin-top: 10px;', 'alt' => 'Carregando', 'title' => 'Carregando')); ?>
                    <div id="content_menu_filtro" rel="<?php echo $categoria_data['Categoria']['id']; ?>"></div>
                </div>
                <!-- end block1 -->
                <div class="clear"></div>
                <?php endIf; ?>

            <?php if (isset($menu_filtro) || isset($filtros_selecionados) || $this->params['controller'] == 'busca' && (isset($niveis) && $niveis >= 3)): ?>
        <div class="container-filtros">
                <?php if (isset($filtros_selecionados)): ?>
                    <!-- start filter -->
                    <div class="filter" style="padding: 15px 0px 0px 0px; margin: 0px 0px -10px 0px">
                        <div id="filtros-selecionados">
                            <h4>Filtros Selecionados</h4>
                            <?php foreach ($filtros_selecionados as $tipo_filtro): ?>
                                <a title="Remove filtro: <?php echo $tipo_filtro['nome_filtro']; ?> <?php echo $tipo_filtro['valor_filtro']; ?>" class="gray filtros_selecionados" href="javascript:void(0);" rel="<?php echo $tipo_filtro['id_filtro']; ?>" ><strong>X</strong> <span><?php echo $tipo_filtro['nome_filtro']; ?> : <?php echo $tipo_filtro['valor_filtro']; ?></span></a>
                            <?php endforeach; ?>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <!-- end filter -->
                    <div class="clear"></div><br />
                    <?php endIf; ?>

                <?php if ($menu_filtro): ?>

                    <!-- start filter -->
                    <div class="filter" style="margin-top: -30px;">

                        <h2 class="title"><span>FILTRO</span></h2>
                        <!-- filter search -->
                        <form action="#" class="filter-search" style="display: none;">
                            <input type="text" value="Marca" name="field" class="input" onfocus="if (this.value == 'Marca') {
                                        this.value = '';
                                    }" onblur="if (this.value == '') {
                                        this.value = 'Marca';
                                    }" />
                            <input type="submit" value="" name="button" class="button" />
                        </form>

                    </div>
                    <!-- end filter -->

                    <?php foreach ($menu_filtro as $tipo_filtro): ?>

                        <h3><?php echo $this->String->title_case($tipo_filtro['nome_filtro']); ?></h3>
                        <br />
                        <ul>
                            <?php foreach ($tipo_filtro['valor_filtro'] as $filtro): ?>
                                <li>
                                    <input type="checkbox" class="checkbox" id="<?php echo $filtro['id']; ?>" rel="<?php echo low(Inflector::slug($tipo_filtro['nome_filtro'], '-')); ?>" value="<?php e($filtro['id']); ?>" name="data[Filtro][id]" />
                                    <label for="<?php echo $filtro['id']; ?>"><?php echo $this->String->title_case($filtro['valor']); ?></label>
                                </li>
                            <?php endforeach; ?>
                        </ul>

                    <?php endforeach; ?>

                    <?php endIf; ?>
        </div>

                <?php endIf; ?>

            <?php if ($this->params['controller'] != "categorias" && $this->params['controller'] != "usuarios"): ?>

                <div class="clear"></div>

                <!-- start newsletter -->
                <div class="newsletter">
                    <h2 class="title"><span>NEWSLETTER</span></h2>
                    <p>Fique por dentro do que acontece na FuelTech. Cadastre-se, e receba nossas novidades.</p>
                    <?php echo $this->Form->create(null, array('id' => 'newsbox', 'url' => '/newsletter')); ?>
                    <?php echo $this->Form->input('Newsletter.nome', array('rel' => 'Nome', 'class' => 'input useDefault', 'div' => false, 'label' => false)); ?>
                    <?php echo $this->Form->input('Newsletter.email', array('rel' => 'E-mail', 'div' => false, 'class' => 'input useDefault', 'label' => false)); ?>
                    <div class="row">
                        <?php // <input name="Submit" type="submit" value="ENVIAR" class="button" onclick="_gaq.push(['_trackEvent', 'Button', 'Click', 'Home - Newsletter']);" /> ?>
                        <?php echo $this->Form->submit('ENVIAR', array( 'class' => 'button', 'value' => '', 'onClick' => "_gaq.push(['_trackEvent', 'Button', 'Click', 'Home - Newsletter'])")); ?>
                    </div>
                    <?php echo $this->Form->end() ?>
                </div>
                <!-- end newsletter -->

                <?php endIf; ?>

            <?php

        elseif($this->params['controller'] == "paginas"):
        ?>
            <!--
            <div class="subnav">
                <h2 class="title"><span><?php //echo $breadcrumb[1]['nome'] ?></span></h2>
            </div>
            -->

            <?php
                $paginas_suportes = '';
                $paginas_mundo_fueltech = '';
                foreach( $paginas_lista as $paginas ):
                    if( $paginas['Pagina']['categoria'] == 'Suporte' && $breadcrumb[0]['nome'] == 'Suporte' ){
                        $paginas_suportes[] = $paginas;
                    }

                    if( $paginas['Pagina']['categoria'] == 'Mundo FuelTech' && $breadcrumb[0]['nome'] == 'Mundo FuelTech' ){
                        $paginas_mundo_fueltech[] = $paginas;
                    }
                endForeach;
            ?>



            <div class="subnav">

               <?php if(!empty($paginas_suportes)): ?>
                 <h2 class="title"><span><?php echo __("Suporte"); ?></span></h2>
                    <ul>
                    <?php foreach($paginas_suportes as $pag): ?>
                        <li>
                            <a href="<?php echo $this->Html->Url('/'.$pag['Pagina']['url']); ?>" title="<?php echo $pag['PaginaDescricao']['nome']; ?>">
                                <?php echo $pag['PaginaDescricao']['nome']; ?>
                            </a>
                            <span class="orange">
                                <?php
                                    if(isset($pag['SubPage']) && count($pag['SubPage']) > 0):
                                        foreach($pag['SubPage'] as $subpag):
                                            echo '
                                                <p class="orange-small">
                                                    <a href="' . $this->Html->Url("/" . $subpag['url']) . '">
                                                            ' . $subpag['nome'] . '
                                                    </a>
                                                </p>';
                                        endforeach;
                                    endif;
                                ?>
                            </span>
                        </li>
                    <?php endForeach; ?>
                    </ul>
                <?php endIf; ?>

               <?php if(!empty($paginas_mundo_fueltech)): ?>
                 <h2 class="title"><span><?php echo __("Mundo FuelTech"); ?></span></h2>
                    <ul>
                    <?php foreach($paginas_mundo_fueltech as $pag): ?>
                        <li>
                            <a href="<?php echo $this->Html->Url('/'.$pag['Pagina']['url']); ?>" title="<?php echo $pag['PaginaDescricao']['nome']; ?>">
                                <?php echo $pag['PaginaDescricao']['nome']; ?>
                            </a>
                            <span class="orange">
                                <?php
                                    if(isset($pag['SubPage']) && count($pag['SubPage']) > 0):
                                        foreach($pag['SubPage'] as $subpag):
                                            echo '
                                                <p class="orange-small">
                                                    <a href="' . $this->Html->Url("/" . $subpag['url']) . '">
                                                            ' . $subpag['nome'] . '
                                                    </a>
                                                </p>';
                                        endforeach;
                                    endif;
                                ?>
                            </span>
                        </li>
                    <?php endForeach; ?>
                    </ul>
                <?php endIf; ?>

            </div>
        <?php
        elseif (
                $this->params['controller'] == "carrinho" || ($this->params['controller'] == "usuarios" && $this->params['action'] == "edit") || ($this->params['controller'] == "usuario_enderecos")
        ):
            ?>
            <!-- start subnav -->
            <div class="subnav">
                <h2 class="title"><span>ÁREA DO <br />CLIENTE</span></h2>
                <ul>
                    <li><a href="#" title="Meus Pedidos">Meus Pedidos</a>
                        <ul>
                            <li><a href="<?php echo $this->Html->Url('/meus_pedidos') ?>" title="Status do pedido">Status dos pedidos</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" title="Dados Cadatrais">Dados Cadastrais</a>
                        <ul>
                            <li><a href="<?php echo $this->Html->Url('/usuarios/edit/') ?>" title="Criar lista">Meus Dados</a></li>
                            <li><a href="<?php echo $this->Html->Url('/usuario_enderecos/edit/') ?>" title="Editar lista ">Meus Endereços</a></li>
                        </ul>
                    </li>
                    <?php if(1 == 2): ?>
                    <li><a href="#" title="Lista de Desejos">Lista de Desejos</a>
                        <ul>
                            <li><a href="#" title="Criar lista">Criar lista</a></li>
                            <li><a href="#" title="Editar lista ">Editar lista </a></li>
                            <li><a href="#" title="Enviar lista por email">Enviar lista por email</a></li>
                            <li><a href="#" title="Compartilhar lista no twitter e facebook">Compartilhar lista no twitter e facebook</a></li>
                        </ul>
                    </li>
                    <?php endIf; ?>
                </ul>
            </div>
            <!-- end subnav -->
            <div class="clear"></div>
        <?php else: ?>
        <?php echo $this->element('site/left_menu_institucional', array('paginas_lista' => $paginas_lista)); ?>
        <?php endIf; ?>

    </div>
    <!-- end column1st -->

    <!--</div>
     end leftcol -->