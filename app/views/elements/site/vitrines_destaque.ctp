<?php if(isset($produtos_vitrines['vitrines'])): ?>
	<?php foreach ($produtos_vitrines['vitrines'] as $vitrine): ?>
	<!-- start column2nd -->
    <div class="column2nd">
		<!-- start productcol -->
		<div class="productcol2">
			<h2 class="title"><?php echo $this->String->formata_estilo_titulo(__("COMPRE AGORA MESMO", true), 3); ?></h2>
			
			<!-- start product-list -->
			<div class="product-list">
				<ul>
					<?php 
						$count = 1;
						foreach ($vitrine as $produto):
							$img = ( isset($produto[0]['imagem']) && file_exists($produto[0]['imagem']) ) ? $produto[0]['imagem'] : "uploads/produto_imagem/filename/sem_imagem.jpg";
					?>
					<li>       
						<h4><a href="<?php e($this->Html->Url("/" . low(Inflector::slug($produto['ProdutoDescricao']['nome'], '-')) . '-' . 'prod-' . $produto['id'])) ?>.html" title="<?php e($produto['ProdutoDescricao']['nome']) ?>"><?php echo $produto['ProdutoDescricao']['nome'] ?></a></h4>
						<div class="imgblock">
							<a href="<?php e($this->Html->Url("/" . low(Inflector::slug($produto['ProdutoDescricao']['nome'], '-')) . '-' . 'prod-' . $produto['id'])) ?>.html" title="<?php e($produto['ProdutoDescricao']['nome']) ?>"><?php echo $image->resize($img, 150, 110, true, array('alt' => $produto['ProdutoDescricao']['nome'])); ?></a>
						</div>
						<?php
							
								$preco = ($produto['preco_promocao'] > 0) ? $produto['preco_promocao'] : $produto['preco'];
								$parcela_valida = $this->Parcelamento->parcelaValida($preco, $parcelamento); 
						?>
						<a href="<?php echo $this->Html->Url("/" . low(Inflector::slug($produto['ProdutoDescricao']['nome'], '-')) . '-' . 'prod-' . $produto['id']); ?>.html" title="<?php echo $produto['ProdutoDescricao']['nome']; ?>">
							<?php if ($this->String->bcoToMoeda($produto['preco_promocao']) > 0): ?>								
								<span> <?php echo __("De"); ?>: <strong>R$ <?php e($this->String->bcoToMoeda($produto['preco'])); ?></strong></span>
								<p class="orange"><strong><small><?php echo __("Por"); ?>: R$ <?php echo $this->String->bcoToMoeda($produto['preco_promocao']) ?></small> <?php echo __("À vista"); ?></strong></p>
							<?php else: ?>
								<p class="orange"><strong><small><?php echo __("Por"); ?>: R$ <?php echo $this->String->bcoToMoeda($produto['preco']) ?></small> <?php echo __("À vista"); ?></strong></p>
							<?php endif; ?>	

							<?php if( $parcela_valida['parcela'] > 1 ):?>
								<span class="red"><?php echo __("ou"); ?> <?php echo $parcela_valida['parcela']; ?>x de R$ <?php echo $this->String->bcoToMoeda($parcela_valida['valor'])?> <?php if( $parcela_valida['juros']==true ){ echo __('com'); } else { echo __('sem'); } ?> <?php echo __("juros"); ?></span>
							<?php endif;?>						
						</a>
							<?php 
								if ($produto['quantidade_disponivel'] <= 0): ?>
							    <p class="esgotado">Consulte Disponibilidade</p>
							<?php endif ?>
					</li>
					
					<?php if($count%4 == 0): ?>
						</ul> <div class="clear"></div><!-- end product-list --> </div> <!-- start product-list --><div class="product-list last"><ul>
					<?php endIf; ?>
					<?php 				
						$count++;
						endforeach; 
					?>
				</ul>
				<div class="clear"></div>
			</div>
			<!-- end product-list -->
		</div>
		<!-- end productcol -->
	</div>
	<!-- end column2nd -->
	<?php endforeach; ?>
<?php endif; ?>