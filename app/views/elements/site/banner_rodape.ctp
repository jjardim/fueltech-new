<?php if(isset($banners_rodape)): ?>
   
	<?php
		foreach($banners_rodape as $indice => $banners):
	?>
		<!-- start right-banner -->
		<div class="banner1">
        	<?php
			if($banners['Banner']['filename']!=""&&$banners['Banner']['mimetype']=='application/x-shockwave-flash'){
				echo $flash->renderSwf($banners['Banner']['dir'].DS.$banners['Banner']['filename'], 150, 285,false);
			}elseif($banners['Banner']['filename']!=""){
				if($banners['Banner']['link']){
					echo $this->Html->image('/uploads/banner/filename/'.$banners['Banner']['filename'], array('alt'=>Inflector::slug($banners['Banner']['filename'], ' '), 'url'=>$banners['Banner']['link']));
				}else{
					echo $this->Html->image('/uploads/banner/filename/'.$banners['Banner']['filename'], array('alt' => Inflector::slug($banners['Banner']['filename'], ' ')));
				}
			}
			?>
		</div>
		<!-- end right-banner -->
	<?php
		endForeach;
	?>
    
<?php endIf; ?>