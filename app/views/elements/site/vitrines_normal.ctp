<?php if(isset($produtos_vitrines['vitrines'])): ?>
	<?php foreach ($produtos_vitrines['vitrines'] as $vitrine): ?>
	<!-- start slider -->
	<div class="slider listing vitrineNormal">
		<a href="javascript:void(0);" title="Avançar" class="next1 jcarousel-next">next</a>
		<a href="javascript:void(0);" title="Voltar" class="prev1 jcarousel-prev">Previous</a>
		<ul>
			<?php 
				foreach ($vitrine as $produto):
					$img = ( isset($produto[0]['imagem']) && file_exists($produto[0]['imagem']) ) ? $produto[0]['imagem'] : "uploads/produto_imagem/filename/sem_imagem.jpg";
			?>
			<li>       
				<span class="model"><a href="<?php e($this->Html->Url("/" . low(Inflector::slug($produto['ProdutoDescricao']['nome'], '-')) . '-' . 'prod-' . $produto['id'])) ?>.html" title="<?php e($produto['ProdutoDescricao']['nome']) ?>"><?php echo $produto['ProdutoDescricao']['nome'] ?></a></span>
				<div class="imgblock"><a href="<?php e($this->Html->Url("/" . low(Inflector::slug($produto['ProdutoDescricao']['nome'], '-')) . '-' . 'prod-' . $produto['id'])) ?>.html" title="<?php e($produto['ProdutoDescricao']['nome']) ?>"><?php echo $image->resize($img, 150, 110, true, array('alt' => $produto['ProdutoDescricao']['nome'])); ?></a></div>
				<div style="display: block; margin-top: 25px;"></div>
				<?php
					
					
						$preco = ($produto['preco_promocao'] > 0) ? $produto['preco_promocao'] : $produto['preco'];
						$parcela_valida = $this->Parcelamento->parcelaValida($preco, $parcelamento);
				?>
					<a href="<?php e($this->Html->Url("/" . low(Inflector::slug($produto['ProdutoDescricao']['nome'], '-')) . '-' . 'prod-' . $produto['id'])) ?>.html" title="<?php e($produto['ProdutoDescricao']['nome']) ?>">
						<?php if ($this->String->bcoToMoeda($produto['preco_promocao']) > 0): ?>
							<span> De: <strong>R$ <?php e($this->String->bcoToMoeda($produto['preco'])); ?></strong></span>
							<span class="red">Por: <strong>R$ <?php echo $this->String->bcoToMoeda($produto['preco_promocao']) ?></strong> ou</span>
						<?php else: ?>
							<span class="red">Por: <strong>R$ <?php echo $this->String->bcoToMoeda($produto['preco']) ?></strong></span>
						<?php endif; ?>
						<?php if( $parcela_valida['parcela'] > 1 ):?>							
							<span class="red">ou <?php e($parcela_valida['parcela']) ?>x de <strong>R$ <?php echo $this->String->bcoToMoeda($parcela_valida['valor'])?></strong> <?php echo ($parcela_valida['juros']==true)?'com':'sem'; ?> juros</span>
						<?php endif;?>
					</a>
					<a href="<?php e($this->Html->Url("/" . low(Inflector::slug($produto['ProdutoDescricao']['nome'], '-')) . '-' . 'prod-' . $produto['id'])) ?>.html" title="Comprar" class="button2">Comprar</a>
					
			</li>
			<?php
				endforeach; 
			?>	
		</ul>
		<div class="clear"></div>
	</div>
	<!-- end slider -->
	<?php endforeach; ?>
<?php endif; ?>