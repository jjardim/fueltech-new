            <!-- start leftcol -->
            <div id="leftcol">
                <!-- start subnav -->
                <div class="subnav">
                    <?php
                    foreach ($paginas_lista as $paginas) {
                        if ($paginas['Pagina']['categoria'] == $pagina_atual_tipo)
                            $paginas_institucionais[] = $paginas;
                    }
                    ?>			
                    <h2 class="title"><?php echo $this->String->formata_estilo_titulo($pagina_atual_tipo, 5); ?></h2>
                    <?php if (!empty($paginas_institucionais)): ?>
                        <ul>	
                            <?php
                            foreach ($paginas_institucionais as $pag) {
                                $class = ($pag['Pagina']['url'] == $pagina_atual_url) ? "class='active'" : "";
                                ?>
                                <li>
                                    <a <?php echo $class; ?> href="<?php echo $this->Html->Url(( (strstr($pag['Pagina']['url'], 'javascript:')) ? '' : '/') . $pag['Pagina']['url']); ?>" title="<?php echo $pag['PaginaDescricao']['nome']; ?>"> <?php echo $pag['PaginaDescricao']['nome']; ?></a>
                                    <?php if (isset($pag['SubPage']) && count($pag['SubPage']) > 0): ?>
                                        <ul>

                                            <?php foreach ($pag['SubPage'] as $pg): ?>
                                                <li><a <?php echo $class; ?> href="<?php echo $this->Html->Url('/' . $pg['url']); ?>" title="<?php echo $pg['nome']; ?>"> <?php echo $pg['nome']; ?></a></li>
                                                <?php endForeach; ?>
                                        </ul>
                                        <?php endIf; ?>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                        <?php endIf; ?>			
                </div>
                <!-- end subnav -->			
            </div>
            <!-- end leftcol -->