<!-- start search box -->
<div id="search_box">
    <?php echo $this->Form->create('Busca', array('class' => 'search_field', 'url' => '/busca/index', 'type' => 'post')); ?>
        <span>Encontre um produto:</span>                
		<?php echo $this->Form->input('Busca.categoria_id',array('div'=>true,'class'=>'list fieldSearch','options'=> array(),'label'=>false)); ?>
        <?php echo $this->Form->input('busca', array('label' => false, 'div' => false, 'class' => 'list useDefault fieldTextSearch', 'rel' => 'Busque no site')); ?>
        <input name="" type="submit" value="" class="link" />
    <?php echo $this->Form->end(); ?>
</div>
<!-- end search box -->