<!-- start navigation -->
<div id="navigation">
    <ul>
        <?php
        if (isset($menu_topo) && count($menu_topo) > 0):
            $cont = 1;
            $qnt = count($menu_topo);
            foreach ($menu_topo as $mt):

                if ($cont == $qnt) {
                    $class = "class='last'";
                } else {
                    $class = "";
                }
                ?>
                <li <?php echo $class; ?>><a href="<?php echo $this->Html->Url('/categorias/' . $mt['Categoria']['seo_url']); ?>" title="<?php echo $mt['CategoriaDescricao']['nome']; ?>"><?php echo $mt['CategoriaDescricao']['nome']; ?></a></li>
                <?php
            endForeach;
        endIf;
        ?>
    </ul>
</div>	
<!-- end navigation -->