<?php if(isset($fabricantes)): ?>
	<!-- slide nav -->
	<div class="slide-nav" id="vitrine_marcas">
		<ul class="vitrine_marcas">
			<?php foreach($fabricantes as $fabricante): 
					if(!isset($fabricante['FabricanteImagem'][0])) continue;
			?>
			<li>
				<?php				
					echo $this->Html->image('/uploads/fabricante_imagem/filename/'.$fabricante['FabricanteImagem'][0]['filename'], array('alt'=> $fabricante['Fabricante']['nome'], 'url'=> '/marca/index/'.$fabricante['Fabricante']['id'].'/'.low(Inflector::slug($fabricante['Fabricante']['nome'], '-'))));
				?>
			</li>
			<?php endforeach; ?>
		</ul>
	</div>
	<!-- slide nav -->
<?php endif; ?>