<!-- start leftcol -->
<div id="leftcol_hp">
    <!-- start sub navigation -->
    <div id="sub_navigation" class="sub_navigation_institucional">
        <div class="gray_box">
            <div class="gray_box_top">
                <div class="gray_box_bottom">                    
                    <ul>
                        <li>MINHA CONTA</li>
                        <li><a href="<?php e($this->Html->Url("/usuarios/edit")) ?>" title="Meu Cadastro">Meu Cadastro</a></li>
                        <li><a href="<?php e($this->Html->Url("/meus_pedidos")) ?>" title="Meus Pedidos">Meus Pedidos</a></li>
                        <li><a href="<?php e($this->Html->Url("/lista_desejos")) ?>" title="Lista de Desejos">Lista de Desejos</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
	<div id="sub_navigation" class="sub_navigation_institucional">
        <div class="gray_box">
            <div class="gray_box_top">
                <div class="gray_box_bottom">                    
                    <ul>
                        <li>CENTRAL DE AJUDA</li>
                        <li><a href="<?php e($this->Html->Url("/contato")) ?>" title="Contato">Contato</a></li>
                        
                        <?php
                        $total = count($paginas_lista);
                        foreach ($paginas_lista as $indice => $valor):
                            $class = ( $total == $indice + 1 ) ? 'class="last"' : '';
                            $url = !empty($valor['Pagina']['url']) ? $valor['Pagina']['url'] : 'paginas/view/' . $valor['Pagina']['id'];
                        ?>
                            <li <?php echo $class?>><a href="<?php e($this->Html->Url("/" . $url))?>" title="<?php echo addslashes($valor['Pagina']['nome'])?>"><?php echo $valor['Pagina']['nome'] ?></a></li>
                        <?php endForeach; ?>
						
						<?php
                        $total = count($faqs_lista);
                        foreach ($faqs_lista as $indice => $valor):
                            $class = ( $total == $indice + 1 ) ? 'class="last"' : '';
                            $url = !empty($valor['Faq']['url']) ? $valor['Faq']['url'] : 'faqs/index/' . $valor['Pagina']['id'];
                        ?>
                            <li <?php echo $class?>><a href="<?php e($this->Html->Url("/" . $url))?>" title="<?php echo addslashes($valor['Faq']['texto'])?>"><?php echo $valor['Faq']['texto'] ?></a></li>
                        <?php endForeach; ?>
						
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- end sub navigation -->
    <!-- start ad box -->
    <?php echo $this->element('site/banner_left'); ?>
    <!-- end add box -->
    <!-- start receba_offer -->
    <div id="receba_offer">
        <div class="gray_box">
            <div class="gray_box_top">
                <div class="gray_box_bottom">
                    <!-- row -->
                    <div class="row">
                        <h3>RECEBA OFERTAS</h3>
                        <?php echo $this->Form->create(null, array('id'=>'newsbox','url' =>  '/newsletter')); ?>
                        <?php echo $this->Form->input('Newsletter.nome', array('rel'=>'Nome','class' => 'input useDefault','div'=>false, 'label' => false));?>
                        <?php echo $this->Form->input('Newsletter.email', array('rel'=>'Email','div'=>false,'class' => 'input useDefault', 'label' => false)); ?>
                        <input name="" type="submit" value="" class="link" /><div class="clear"></div>
                        <?php echo $this->Form->end() ?>
                    </div>
                    <!-- row -->
                </div>
            </div>
        </div>
    </div>
    <!-- end receba_offer -->
</div>
<!-- end leftcol -->