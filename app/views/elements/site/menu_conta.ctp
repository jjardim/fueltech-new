<div id="leftcol_hp">
	<!-- start category box -->
	<div class="category_box">
		<div class="category_box_top">
			<div class="category_box_bottom">
				<h2>MINHA CONTA</h2>
				<ul class="inner3">
				  <li><a title="Meu cadastro" href="<?php echo $this->Html->Url('/usuarios/add') ?>">Meu cadastro</a></li>
				  <li><a title="Meus pedidos" href="<?php echo $this->Html->Url('/meus_pedidos') ?>">Meus pedidos</a></li>
				  <li><a title="Lista de desejos" href="<?php echo $this->Html->Url('/desejos') ?>">Lista de desejos</a></li>
			  </ul>
		  </div>
	  </div>
	</div>
	<!-- end category box -->
</div>