<!-- start header -->
<div id="header">
    <!-- start logo -->
    <div id="logo">
        <h1>
            <a href="<?php echo $this->Html->Url('/'); ?>" title="Fuel Tech">
                <?php echo $this->Html->image('site/'.Configure::read('Config.language').'/logo_header.png', array('alt' => 'FuelTech', 'width' => '219', 'height' => '53'))?>
            </a>
        </h1>
    </div>
    <!-- end logo -->
    <!-- start header mid -->
    <div id="header-mid">
        <!-- start toplink -->
        <div class="toplink">
            <ul>
                <li><a href="<?php echo $this->Html->Url('/produtos'); ?>" title="<?php echo __("PRODUTOS"); ?>"><?php echo __("PRODUTOS"); ?></a></li>
                <li><a href="<?php echo $this->Html->Url('/manutencao-atualizacao'); ?>" title="<?php echo __("SUPORTE"); ?>"><?php echo __("SUPORTE"); ?></a></li>
                <li class="last"><a href="<?php echo $this->Html->Url('/sobre-a-empresa'); ?>" title="<?php echo __("MUNDO FUELTECH"); ?>"><?php echo __("MUNDO FUELTECH"); ?></a></li>
            </ul>
        </div>
        <!-- end toplink -->
        <div class="clear"></div>
        <!-- start search -->
        <div class="search">
            <?php echo $this->Form->create('Busca', array('class'=>'header_form','id' => 'header_form', 'url' => '/busca', 'type' => 'get')); ?>
                <?php echo $this->Form->input('busca', array('rel' => __('Busque no site', true), 'label' => false, 'div' => false, 'class' => 'input useDefault BuscaBusca', 'value' => (isset($this->data['Busca']['busca']) && !empty($this->data['Busca']['busca'])) ? $this->data['Busca']['busca'] : __('Busque no site', true))); ?>
                <input name="submit" type="submit" class="button" value="" style="display: none;" />
            <?php echo $this->Form->end(); ?>
        </div>
            
        <!-- end search -->
        <!-- start info-link -->
        <div class="info-link">
            <ul>
                <li><a href="http://www.fueltech.com.br/livezilla/chat.php" target="_blank" title="<?php echo __("ATENDIMENTO"); ?>"><span><?php echo __("ATENDIMENTO"); ?></span><span><small>ONLINE</small></span></a></li>
                <li><a href="<?php echo $this->Html->Url('/fale-conosco'); ?>" style="width: 49px; display: block;" title="<?php echo __("FALE CONOSCO"); ?>"><span><?php echo __("FALE CONOSCO"); ?></span></a></li>
                <li class="last"><span><?php echo __("LIGUE PARA NÓS"); ?></span><span><?php echo Configure::read('Loja.televendas')?></span></li>
            </ul>
        </div>
        <!-- end info-link -->
    </div>
    <!-- end header mid -->
    <!-- start header right -->
    <div id="header-right">
        <!-- start language -->
        <div class="language">
            <ul>
                <li><span><?php echo __("VERSION"); ?>: </span></li>
				<?php 
					if(isset($menu_idiomas) && count($menu_idiomas) > 0):
						$first = true;
						foreach($menu_idiomas as $idioma): 
							if(Configure::read('Config.language') == $idioma['Linguagem']['codigo']){
								$class = "class='active'";
							}else{
								$class = "";
							}
				?>		
							<?php if(!$first): ?>
								<li>&nbsp; | &nbsp;</li>
							<?php endIf; ?>
							<li <?php echo $class ?>>
								<?php if($idioma['Linguagem']['externo'] == 0): ?>
									<a href="<?php echo $this->Html->Url('/').$idioma['Linguagem']['codigo']; ?>" title="<?php echo $idioma['Linguagem']['nome']; ?>">
										<?php //echo $image->resize(isset($idioma['Linguagem']['thumb_filename']) ? $idioma['Linguagem']['thumb_dir'] . '/' . $idioma['Linguagem']['thumb_filename'] : 'uploads/produto_imagem/filename/sem_imagem.jpg', 12, 8, true, array('alt' => $idioma['Linguagem']['nome'])); ?>
										<?php echo $idioma['Linguagem']['nome']; ?>
									</a>
								<?php else: ?>
									<a href="<?php echo $idioma['Linguagem']['url']; ?>" title="<?php echo $idioma['Linguagem']['nome']; ?>">
										<?php //echo $image->resize(isset($idioma['Linguagem']['thumb_filename']) ? $idioma['Linguagem']['thumb_dir'] . '/' . $idioma['Linguagem']['thumb_filename'] : 'uploads/produto_imagem/filename/sem_imagem.jpg', 12, 8, true, array('alt' => $idioma['Linguagem']['nome'])); ?>
										<?php echo $idioma['Linguagem']['nome']; ?>
									</a>
								<?php endIf; ?>
							</li>
				<?php
							if($first){ $first = false; }
						endForeach;
					endIf;
				?>
            </ul>
        </div>
        <!-- end language -->
        <div class="clear"></div>
		
		<?php //begin restrição de idioma 
			if($session->read('linguagem_default') == Configure::read('Config.language')): ?>
        <!-- start shopping-cart -->
        <div class="shopping-cart">
            <?php if (!$auth['Usuario']['nome']): ?>
                <p><?php echo __("Olá"); ?> <a href="<?php echo $this->Html->url("/usuarios/edit") ?>" title="Visitante"><?php echo __("Visitante"); ?></a>, <?php echo __("seja bem vindo"); ?>! <br />(<?php echo __("Faça o"); ?> <a href="<?php echo $this->Html->Url("/login") ?>" title="login">login</a> <?php echo __("para ver sua página"); ?>)</p>
            <?php else: ?>
                <p>Olá 
				<a href="<?php echo $this->Html->url("/usuarios/edit") ?>" title="<?php 
					if(strpos($auth['Usuario']['nome'], " "))
						echo substr($auth['Usuario']['nome'],0,strpos($auth['Usuario']['nome'], " "));
					else
						echo $auth['Usuario']['nome'];
					?>">
					<?php 
						if(strpos($auth['Usuario']['nome'], " "))
							echo substr($auth['Usuario']['nome'],0,strpos($auth['Usuario']['nome'], " "));
						else
							echo $auth['Usuario']['nome'];
					?>
				</a>, seja bem vindo! &nbsp;<a href="<?php echo $this->Html->Url("/logout") ?>" title="Sair">Sair</a></p>
            <?php endif; ?>
            <div class="clear"></div>
            <ul>
                <li>
                    <a href="<?php echo $this->Html->url("/carrinho") ?>" title="<?php echo __("MEU CARRINHO"); ?>">
                        <?php echo $this->Html->image('site/ic_basket.png', array('alt' => 'Carrinho', 'width' => '14', 'height' => '14'))?>
						<?php echo __("MEU CARRINHO"); ?>
                    </a>
                </li>
                 <li class="last">
                    <a href="<?php echo $this->Html->url("/carrinho/meus_pedidos") ?>" title="<?php echo __("MEUS PEDIDOS"); ?>">
                        <?php echo $this->Html->image('site/ic_list.png', array('alt' => 'Meus Pedidos', 'width' => '14', 'height' => '14'))?>
                        <?php echo __("MEUS PEDIDOS"); ?>
                    </a>
                </li>
            </ul>
        </div>
        <!-- end shopping-cart -->
		<?php //end restrição de idioma 
			endIf;
		?>
		
    </div>
    <!-- end header right -->
</div>
<!-- end header -->
<div class="clear"></div>

<?php echo $this->element('site/menu_topo', array('cache'=>'1 day')); ?>