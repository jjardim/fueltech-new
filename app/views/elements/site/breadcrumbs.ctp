<?php if(isset($breadcrumbs)) { ?>
	<!-- start bread_crumb -->
	<div class="breadcrumb">
			<ul>
		
			<li><?php echo $this->Html->link('Home', '/');?></li>
			<?php
			if(true) {
				$indice = 1;
				$total  = count($breadcrumbs);
				foreach($breadcrumbs as $row) {
					echo '<li>';
					if(isset($row['Categoria']['nome'])){
						echo $this->Html->link($row['Categoria']['nome'], '/'.$this->Tree->url($row['Categoria']));
					}else{
						echo $this->Html->link($row['nome'],$row['link']);
					}
					echo '</li>';
					$indice++;
				}
			}else{
			?>
				<li>Busca</li>
			<?php
			}
			?>
		</ul>
		<div class="clear"></div>
	</div>
	<!-- end bread_crumb -->
<?php } ?>