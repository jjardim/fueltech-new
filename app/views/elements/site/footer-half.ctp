<!-- start footer -->
<div id="footer">
	<!-- start box -->
    <div class="box">
        <!-- start footer-mid -->
		<div id="footer-mid">
            <!-- start footer right -->
            <div id="footer-right" class="gap">
            	<a href="<?php e($this->Html->Url('/')) ?>" title="Fuel Tech" class="logo-footer">
					<?php echo $this->Html->image('site/'.Configure::read('Config.language').'/logo_footer.png', array('alt' => 'Fuel Tech'))?>
				</a>
                <ul class="social">
                	<li><a href="<?php echo Configure::read('Loja.twitter'); ?>" title="Twitter" target="_blank"><?php echo $this->Html->image('site/ic_twitter.png', array('alt' => 'Twitter', 'width' => '40', 'height' => '40'))?></a></li>
                    <li><a href="<?php echo Configure::read('Loja.youtube'); ?>" title="Youtube" target="_blank"><?php echo $this->Html->image('site/ic_youtube.png', array('alt' => 'Youtube', 'width' => '40', 'height' => '40'))?></a></li>
                </ul>
            	<div class="clear"></div>
                <address><?php echo Configure::read('Loja.endereco'); ?></address>
        	</div>	
            <!-- end footer right -->
			<div class="clear"></div>
        </div>
        <!-- end footer-mid -->
        <div class="clear"></div>
    </div>
    <!-- end box -->
</div>
<!-- end footer -->