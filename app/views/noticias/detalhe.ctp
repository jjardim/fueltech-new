<?php echo $html->css('common/nyro_modal/nyroModal.css', null, array('inline' => false)); ?>
<?php echo $javascript->link('common/nyro_modal/jquery.nyroModal.custom', false); ?>
<?php echo $javascript->link('site/noticias/index', false); ?>
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "b5c8b4f8-9290-467f-9537-078d59eb7697"});</script>

<!-- start leftcol -->
<div id="leftcol">
	<?php echo $this->element('site/left'); ?>
</div>
<!-- end leftcol -->

<!-- start rightcol -->
<div id="rightcol">
	<!-- start showcase -->
	<div id="showcase">
		<div class="caption"><h2><?php echo __("Notícias"); ?></h2></div>
		<?php echo $this->Html->image('site/img_showcase2.jpg', array('alt' => __("Notícias", true), 'width' => '719', 'height' => '187'))?>
	</div>
	<!-- end showcase -->
	<!-- start content -->
	<div class="content">
		<?php if(isset($noticia) && !empty($noticia)): ?>
			<!-- end entry -->
			<div class="entry">
				<h5><?php echo $noticia['Noticia']['titulo']; ?></h5>
                                <span><?php echo $this->String->formata_intervalo_data($noticia['Noticia']['created']); ?></span>
				<?php echo $noticia['Noticia']['conteudo']; ?>
			</div>
			<!-- end entry -->
		<?php endIf; ?>
		
		<?php if(isset($noticia['Galeria']['GaleriaFoto']) && count($noticia['Galeria']['GaleriaFoto']) > 0): ?>
		<!-- start gallery1st -->
		<div class="gallery1st">
			<h2 class="title"><?php echo $this->String->formata_estilo_titulo(__("GALERIA DE FOTOS", true), 3) ?></h2>
			<ul>
				<?php 
					$cont = 1;
					foreach($noticia['Galeria']['GaleriaFoto'] as $gl):
						$img = ( isset($gl['filename']) ) ? $gl['dir']."/".$gl['filename'] : "uploads/produto_imagem/filename/sem_imagem.jpg";
				?>
					<li>
						<a rel="galeria_noticia" class="zoom" href="<?php echo $this->Html->url("/uploads/galeria/filename/". $gl['filename']) ?>">
							<?php echo $image->resize($img, 136, 84, true, array('alt' => $noticia['Noticia']['titulo'])); ?>
						</a>
					</li>
					
					<?php if($cont%4 == 0): ?>
						</ul><ul>
					<?php endIf; ?>
				<?php 
					$cont++;
					endForeach;
				?>
			</ul>
			<div class="clear"></div>
		</div>
		<!-- end gallery1st -->
		<?php endIf; ?>
		<br />
		<!-- start social2 -->
		<div class="social2">
			<h3><?php echo __("Compartilhe"); ?>:</h3>
			<div class="clear"></div>
			<ul>
				<li><span class='st_orkut' displayText=''></span></li>
				<li><span class='st_twitter' displayText=''></span></li>
				<li><span class='st_facebook' displayText=''></span></li>
				<li><span class='st_email' displayText=''></span></li>
				<li><span class='st_googleplus' displayText=''></span></li>
			</ul>
			<div class="clear"></div>
		</div>
		<!-- end social2 -->
		<?php if(isset($mais_noticias) && count($mais_noticias) > 0): ?>
			<!-- start productcol -->
			<div class="product-list notice-list">
				<h2><?php echo $this->String->formata_estilo_titulo(__("MAIS NOTÍCIAS", true), 4) ?></small></h2>
				<ul>
					<?php foreach($mais_noticias as $noticia_lista): ?>
					<li>
						<h3><a href="<?php echo $this->Html->Url("/noticias/" . low(Inflector::slug($noticia_lista['Noticia']['titulo'], '-')) . '-' . 'news-' . $noticia_lista['Noticia']['id']); ?>.html" title="<?php echo $noticia_lista['Noticia']['titulo']; ?>"><?php echo $noticia_lista['Noticia']['titulo']; ?></a></h3>
						<span><a href="<?php echo $this->Html->Url("/noticias/" . low(Inflector::slug($noticia_lista['Noticia']['titulo'], '-')) . '-' . 'news-' . $noticia_lista['Noticia']['id']); ?>.html" title="<?php echo $noticia_lista['Noticia']['titulo']; ?>"><?php echo $this->String->formata_intervalo_data($noticia_lista['Noticia']['created']); ?></a></span>
						<p><a href="<?php echo $this->Html->Url("/noticias/" . low(Inflector::slug($noticia_lista['Noticia']['titulo'], '-')) . '-' . 'news-' . $noticia_lista['Noticia']['id']); ?>.html" title="<?php echo $noticia_lista['Noticia']['titulo']; ?>"><?php echo $noticia_lista['Noticia']['descricao']; ?></a></p>
					</li>
					<?php endForeach; ?>
				</ul>
				<div class="clear"></div>
			</div>
			<!-- end productcol -->
		<?php endIf; ?>
	</div>
	<!-- end content -->
	<div class="clear"></div>
</div>	
<!-- end rightcol -->