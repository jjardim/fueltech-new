<!-- start rightcol -->
<div id="rightcol" style="margin-left: -5px;">
	<!-- start showcase -->
	<div id="showcase">
		<div class="caption"><h2><?php echo __("Notícias"); ?></h2></div>
		<?php echo $this->Html->image('site/img_showcase2.jpg', array('alt' => __("Notícias", true), 'width' => '719', 'height' => '187'))?>
	</div>
	<!-- end showcase -->
	<!-- start content -->
	<div class="content">
		<!-- start productcol -->
		<div class="product-list gap first">
			<ul>
				<?php
					$cont = 0;
					foreach($noticias as $noticia): 
					$img = ( isset($noticia['Noticia']['thumb_filename']) ) ? $noticia['Noticia']['thumb_dir']."/".$noticia['Noticia']['thumb_filename'] : "uploads/produto_imagem/filename/sem_imagem.jpg";
				?>
					
					<li>
						<h3><a href="<?php echo $this->Html->Url("/noticias/" . low(Inflector::slug($noticia['Noticia']['titulo'], '-')) . '-' . 'news-' . $noticia['Noticia']['id']); ?>.html" title="<?php echo $noticia['Noticia']['titulo']; ?>"><?php echo $noticia['Noticia']['titulo']; ?></a></h3>
						<span><a href="<?php echo $this->Html->Url("/noticias/" . low(Inflector::slug($noticia['Noticia']['titulo'], '-')) . '-' . 'news-' . $noticia['Noticia']['id']); ?>.html" title="<?php echo $noticia['Noticia']['titulo']; ?>"><?php echo $noticia['Noticia']['created']; ?></a></span>
						<a href="<?php echo $this->Html->Url("/noticias/" . low(Inflector::slug($noticia['Noticia']['titulo'], '-')) . '-' . 'news-' . $noticia['Noticia']['id']); ?>.html" title="<?php echo $noticia['Noticia']['titulo']; ?>"><?php echo $image->resize($img, 280, 105, true, array('alt' => $noticia['Noticia']['titulo'])); ?></a>
						<p><a href="<?php echo $this->Html->Url("/noticias/" . low(Inflector::slug($noticia['Noticia']['titulo'], '-')) . '-' . 'news-' . $noticia['Noticia']['id']); ?>.html" title="<?php echo $noticia['Noticia']['titulo']; ?>"><?php echo $noticia['Noticia']['descricao']; ?></a></p>
					</li>
					
				<?php 
					$cont++;
					if($cont%2 == 0): ?>
					 </ul>
				<div class="clear"></div>
				</div>
				<!-- end product-list -->
				<!-- start product-list -->
				<div class="product-list last">
					<ul>
				<?php endIf; ?>
				<?php
					endForeach; ?>
			</ul>
			<div class="clear"></div>
		</div>
		<!-- end productcol -->
		<!-- start pagination -->
		<div class="pagination">
			<ul>
				<?php echo $this->Paginator->numbers(array( 'tag' => 'li', 'separator' => '<li>.</li>' )); ?>
			</ul>
			<div class="clear"></div>
		</div>
		<!-- end pagination -->
	</div>
	<!-- end content -->
	<div class="clear"></div>
</div>	
<!-- end rightcol -->