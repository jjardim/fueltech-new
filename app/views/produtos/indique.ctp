<?php echo $this->Html->css('common/nyro_modal/default',null,array('inline'=>false));?>
<div class="container-modal">
    <div class="container-header">
        <h1 class="logo">Indique</h1>
    </div>
    <div>
        <?php echo $form->create(null, array("controller" => "produtos", "action" => "indique/{$this->params['pass'][0]}")); ?>
            <ul>
				<li><?php echo $this->Session->flash(); ?></li>
                <li class="clear"><br/></li>
                <li class="clear"><?php echo $form->input("Indique.seu_nome", array("label" => "Seu Nome")) ?></li>
                <li class="clear"><?php echo $form->input("Indique.seu_email", array("label" => "Seu E-mail")) ?></li>
                <li class="clear"><?php echo $form->input("Indique.amigo_nome", array("label" => "Amigo Nome")) ?></li>
                <li class="clear"><?php echo $form->input("Indique.amigo_email", array("label" => "Amigo E-mail")) ?></li>
                <li class="clear"><br/></li>
                <li class="clear"><?php echo $form->button("Enviar", array("class" => "link2")); ?></li>
            </ul>        
        <?php echo $form->end(); ?>
    </div>
</div>
