<div class="index">
    <h2 class="left"><?php __('Produtos'); ?></h2>
    <div class="btAddProduto">
        <?php echo $this->Html->link(__('[+] Adicionar Produto', true), array('action' => 'add')); ?>
    </div>       
    <?php echo $form->create('Produto', array('class' => 'formBusca', 'action' => '/index')); ?>
    <fieldset>
        <div class="left">
            <?php echo $form->input('Filter.filtro', array('div' => false, 'label' => 'Produto / SKU:', 'class' => 'produtosFiltro')); ?>
        </div>
         <div class="left">
            <?php echo $form->input('Filter.status', array('default' => 1, 'div' => false, 'options' => array(1=>'Ativo',2 =>'Indisponível',0=>'Inativo' ), 'label' => 'Status:', 'class' => 'produtosFiltro')); ?>
        </div>
        <div class="submit">
            <input name="submit" type="submit" class="button1" value="Busca" />
        </div>
        <div class="submit">
            <input name="submit" type="submit" class="button1" value="Exportar" />
        </div>
        <div class="submit">
            <a href="<?php e($this->Html->Url('index/filter:clear')) ?>" title="Limpar Filtro" class="button" >Limpar Filtro</a>
        </div>
    </fieldset>	
    <?php echo $form->end(); ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('ID', 'Produto.id'); ?></th>
            <th>Thumb</th>
            <th><?php echo $this->Paginator->sort('SKU', 'Produto.codigo'); ?></th>
            <th><?php echo $this->Paginator->sort('Nome', 'Produto.nome'); ?></th>
            <th><?php echo $this->Paginator->sort('Preço', 'Produto.preco'); ?></th>
            <th><?php echo $this->Paginator->sort('Quantidade Dispinível', 'Produto.quantidade_disponivel'); ?></th>
            <th><?php echo $this->Paginator->sort('Quantidade Alocada', 'Produto.quantidade_alocada'); ?></th>
            <th><?php echo $this->Paginator->sort('Principal', 'Produto.parent_id'); ?></th>
            <th><?php echo $this->Paginator->sort('Status', 'Produto.status'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Modificação', 'Produto.modified'); ?></th>
            <th class="actions">Ações</th>
        </tr>
        <?php
        $i = 0;
        $categoria = null;
        foreach ($produtos as $produto):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
            <tr <?php echo $class; ?>>
                <td align="center"><?php echo $produto['Produto']['id']; ?>&nbsp;</td>
				<?php 
                	$is_file = false;
                	$fullpath = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS;
                	if (isset($produto['ProdutoImagem'][0]['filename'])) {
						if (is_file($fullpath . 'uploads/produto_imagem/filename/' . $produto['ProdutoImagem'][0]['filename'])) {
							$is_file = true;
						}						
					}
                ?>
                <td align="center"><?php echo $image->resize(( isset($produto['ProdutoImagem'][0]) && $produto['ProdutoImagem'][0]['filename'] != "" ) ? 'uploads/produto_imagem/filename/' . $produto['ProdutoImagem'][0]['filename'] : 'uploads/produto_imagem/filename/sem_imagem.jpg', 97, 70); ?>&nbsp;</td>
                <td align="center"><?php echo $produto['Produto']['sku']; ?>&nbsp;</td>
                <td><?php echo $produto['Produto']['nome']; ?>&nbsp;</td>
                <td style="width: 100px; text-align: center;">R$ <?php echo ($produto['Produto']['preco_promocao'] > 0) ? $produto['Produto']['preco_promocao'] : $produto['Produto']['preco']; ?>&nbsp;</td>
                <td style="text-align: center;"><?php echo $produto['Produto']['quantidade_disponivel']; ?>&nbsp;</td>
                <td style="text-align: center;"><?php echo $produto['Produto']['quantidade_alocada']; ?>&nbsp;</td>
                <td align="center"><?php echo ($produto['Produto']['parent_id'] == 0) ? 'Sim' : 'Não'; ?>&nbsp;</td>
                <td align="center"><?php echo ($produto['Produto']['status']) ? 'Ativo' : 'Inativo'; ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $produto['Produto']['modified']); ?>&nbsp;</td>
                <td class="actions">
                    <?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $produto['Produto']['id'])); ?>
                    <?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $produto['Produto']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $produto['Produto']['id'])); ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true)
        ));
        ?>	</p>

    <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array(), null, array('class' => 'disabled')); ?>
        | 	<?php echo $this->Paginator->numbers(); ?>
        |
        <?php echo $this->Paginator->next(__('Próximo', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
    </div>
</div>
