<?php
echo $javascript->link('common/jquery-ui-1.8.16.custom.min',false);
echo $this->Html->css('common/ui-lightness/jquery-ui-1.8.16.custom.css');
echo $javascript->link('common/jquery.meio_mask.js',false);
echo $javascript->link('admin/produtos/promocao.js',false);
?>
<div class="index">
    <?php echo $this->Form->create('Produto', array('type' => 'file')); ?>
    <fieldset>
        <legend><?php __('Adicionar promoção em lote'); ?></legend>
		<div class="abas" rel="0">			
			<div class="left clear">
				<?php echo $this->Form->input('categoria_id',array('label'=>'Categoria')); ?>				
			</div>				
			<div class="left clear">
				<?php echo $this->Form->input('preco_promocao_inicio', array('type'=>'text','class' => 'datePicker inputs w147', 'label' => 'Promoção de:')); ?>
				<?php echo $this->Form->input('preco_promocao_fim', array('type'=>'text','class' => 'datePicker inputs w147', 'label' => 'Promoção até:')); ?>
			</div>
			<div class="clear left">
				<?php echo $this->Form->input('preco_promocao_pct', array('class' => 'mask-moeda inputs w147', 'label' => '% de desconto')); ?>
			</div>
			<br class="clear" />
			<?php
			echo $this->Form->end(__('Salvar', true));
			?>
		</div>
	</fieldset>
</div>
