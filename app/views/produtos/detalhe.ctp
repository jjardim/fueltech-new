<?php echo $html->css('common/jqzoom/jquery.jqzoom', null, array('inline' => false)); ?>
<?php echo $html->css('common/nyro_modal/nyroModal.css', null, array('inline' => false)); ?>
<?php echo $javascript->link('common/jquery.meio_mask', false); ?>
<?php echo $javascript->link('common/jquery.jqzoom-core-pack', false); ?>
<?php echo $javascript->link('common/nyro_modal/jquery.nyroModal.custom', false); ?>
<?php echo $javascript->link('site/produtos/jquery.ratings.js', false); ?>
<?php echo $javascript->link('site/produtos/index', false); ?>
<script type="text/javascript">var switchTo5x = true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "b5c8b4f8-9290-467f-9537-078d59eb7697"});</script>

<!-- start product-detail -->
<div class="product-detail">
    <!-- start left-col -->
    <div class="left-col">
        <h2 class="nome-produto"><?php echo $produto['ProdutoDescricao']['nome']; ?></h2>

        <!-- start product1 -->
        <div class="product1">

            <!-- start showcase2 -->
            <div class="showcase2">
                <?php
                if (count($produto['ProdutoImagem']) > 0):
                    foreach ($produto['ProdutoImagem'] as $indi => $img):
                        ?>
                        <!-- start seleto -->
                        <div class="seleto sel_foto<?php echo $indi; ?>" <?php
                if ($indi > 0) {
                    echo ' style="display:none;" ';
                }
                ?> >
                            <table align="center" style="margin: auto; height: 237px; ">
                                <tr>
                                    <td style="vertical-align: middle;">
                                        <a class="zoom" href="<?php echo $this->Html->url("/uploads/produto_imagem/filename/" . $img['filename']) ?>">
                                            <?php echo $image->resize(isset($img['filename']) ? $img['dir'] . '/' . $img['filename'] : 'uploads/produto_imagem/filename/sem_imagem.jpg', 455, 455, true, array('alt' => Inflector::slug($produto['Produto']['nome'], ' '), 'class' => 'nyroModal')); ?>
                                        </a>
                                    </td>
                                </tr>
                            </table>
                            <a href="<?php echo $this->Html->url("/uploads/produto_imagem/filename/" . $img['filename']) ?>" rel="gal" class="nyroModal search2 icon-zoom-thumb-product" title="<?php echo __("Ampliar"); ?>" ><?php echo $this->Html->image('site/ico_search.png', array('alt' => '<?php echo __("Ampliar"); ?>', 'width' => '17', 'height' => '17')) ?></a>
                        </div>
                        <!-- end seleto -->
                        <?php
                    endForeach;
                else:
                    ?>
			<?php echo $image->resize('uploads/produto_imagem/filename/sem_imagem.jpg', 380, 380, true, array('alt' => Inflector::slug($produto['Produto']['nome'], ' '), 'class' => 'nyroModal')); ?>
			<?php endIf; ?>
            </div>
            <!-- end showcase2 -->

            <?php
            if (count($produto['ProdutoImagem']) > 1):
                ?>
                <!-- start product1-list -->
                <div class="product1-list produtos_imagens_thumb" id="carousel-thumbs">
                    <a href="javascript:void(0);" title="Voltar" class="left jcarousel-prev">
                        <?php echo $this->Html->image('site/bg_arrow_left.png', array('alt' => 'Voltar', 'width' => '9', 'height' => '16')) ?>
                    </a>
                    <a href="javascript:void(0);" title="Avançar" class="right jcarousel-next">
                        <?php echo $this->Html->image('site/bg_arrow_right.png', array('alt' => 'Avançar', 'width' => '9', 'height' => '16')) ?>
                    </a>
                    <ul>
                        <?php foreach ($produto['ProdutoImagem'] as $indi => $img): ?>
                            <li>
                                <a class="clique_sel" rel="foto<?php echo $indi; ?>" href="javascript:;">
                            <?php e($html->image("/resizer/view/75/65/false/true/" . $img['filename'], array("alt" => "Foto"))); ?>
                                </a>
                            </li>
						<?php endForeach ?>
                    </ul>
                </div>
                <!-- end product1-list -->
                <?php
            endIf;
            ?>
        </div>
        <!-- end product1 -->
        <!-- start column -->
        <div class="column2">
            <!-- start social -->
            <div class="social">
                <h4><?php echo __("Compartilhe"); ?>:</h4>
                <ul>
                    <li><span class='st_orkut' displayText=''></span></li>
                    <li><span class='st_twitter' displayText=''></span></li>
                    <li><span class='st_facebook' displayText=''></span></li>
                    <li><span class='st_email' displayText=''></span></li>
                    <li><span class='st_googleplus' displayText=''></span></li>
                </ul>
            </div>
            <!-- end social -->
            <div class="clear"></div>
            <h3><?php echo __("Sobre o produto"); ?>:</h3>
            <?php
            if (!empty($produto['ProdutoDescricao']['descricao_resumida'])):
                $temp = explode("\n", $produto['ProdutoDescricao']['descricao_resumida']);
                ?>
                <p>
                    <?php
                    foreach ($temp AS $valor):
                        ?>
                        <?php echo trim($valor) . '<br />'; ?>
                        <?php
                    endForeach;
                    ?>
                </p>
                <?php
            else:
                ?>
                <p><?php echo __("Descrição Indisponível"); ?></p>
			<?php endIf; ?>
			<?php if($produto['Produto']['codigo_erp'] != "" && $produto['Produto']['kit'] != true): ?>
				<br />
				Código do Produto: <?php echo $produto['Produto']['codigo_erp']; ?>
			<?php elseif( $produto['Produto']['kit'] == true ): ?>
				<?php $produtos_kit = json_decode($produto['Produto']['kit_produtos'], true); ?>
				<?php
					$codigo_erp = '';
					$first = true;
					foreach($produtos_kit as $prd_kit):
						if($first){
							$first = false;
							$codigo_erp = $prd_kit['Produto']['codigo_erp'];
						}else{
							$codigo_erp .= ' + '.$prd_kit['Produto']['codigo_erp'];
						}
					?>
				<?php endForeach; ?>
				<?php if($codigo_erp != ''): ?>
					<br />
					Código do Produto: <?php echo $codigo_erp; ?>
				<?php endIf; ?>
			<?php endIf; ?>
            <!-- start Cores -->
            <div class="Cores bloco1">
                <?php
                if (count($variacoes) > 0):

                    foreach ($variacoes as $tipoVariacao => $variacao):
                        ?>
                        <!-- start size-block -->
                        <div class="choose-block">
                            <h4><?php echo $tipoVariacao; ?></h4>
                            <ul>
                                <?php
                                foreach ($variacao as $chave => $valor):
                                    $class = "";
                                    $class2 = "";
                                    foreach ($produto['VariacaoProduto'] as $va) {

                                        if (count($variacao) == 1 || $valor['Data']['valor'] == $va['Variacao']['valor']) {
                                            $class = "variacaoSelected";
                                            break;
                                        }
                                    }

                                    $img = "";
                                    ?>
                                    <li class="<?php
                                    echo $class;
                                    echo $class2;
                                    ?>">
                                        <?php /* verifica se tem um ou mais tipos de variação (cor, tamanho e etc) */ ?>
                                        <?php if (isset($valor['Data']['tipo_variacao'])) : ?>
                                                <?php /* se tiver mais de um tipo de variação... */ ?>
                                                <?php if ($valor['Data']['thumb'] == true): ?>
                                                    <a title="<?php echo $valor['Data']['valor']; ?>"  rel="<?php echo Inflector::slug($valor['Data']['valor']); ?>"  href="javascript:;">
                                                        <?php echo $image->resize(isset($valor['Data']['thumb_filename']) ? $valor['Data']['thumb_dir'] . '/' . $valor['Data']['thumb_filename'] : 'uploads/produto_imagem/filename/sem_imagem.jpg', 72, 31, true, array('alt' => Inflector::slug($valor['Data']['valor'], ' '))); ?>
                                                    </a>
                                                <?php else: ?>
                                                    <a rel="<?php echo Inflector::slug($valor['Data']['valor']); ?>" title="<?php echo $valor['Data']['valor']; ?>" href="javascript:;">
														<?php echo trim($valor['Data']['valor']); ?>
													</a>
                                                <?php endIf; ?>
                                        <?php else : ?>
                                                <?php /* se tiver só um tipo de variação... */ ?>
                                                <?php if ($valor['Data']['thumb'] == true): ?>
                                                    <a title="<?php echo $valor['Data']['valor']; ?>"  rel="<?php echo Inflector::slug($valor['Data']['valor']); ?>"  href="<?php echo $this->Html->Url("/" . low(Inflector::slug($valor['Produtos'][0]['nome'], '-')) . '-' . 'prod-' . $valor['Produtos'][0]['id']) ?>.html">
                                                        <?php echo $image->resize(isset($valor['Data']['thumb_filename']) ? $valor['Data']['thumb_dir'] . '/' . $valor['Data']['thumb_filename'] : 'uploads/produto_imagem/filename/sem_imagem.jpg', 72, 31, true, array('alt' => Inflector::slug($valor['Data']['valor'], ' '))); ?>
                                                    </a>
                                                <?php else: ?>
                                                    <a rel="<?php echo Inflector::slug($valor['Data']['valor']); ?>" title="<?php echo $valor['Data']['valor']; ?>" href="<?php echo $this->Html->Url("/" . low(Inflector::slug($valor['Produtos'][0]['nome'], '-')) . '-' . 'prod-' . $valor['Produtos'][0]['id']) ?>.html">
														<?php echo trim($valor['Data']['valor']); ?>
													</a>
                                                <?php endIf; ?>
                                        <?php endif ?>
                                    </li>
                                        <?php
                                    endForeach;
                                    ?>
                            </ul>
                            <div class="clear"></div>
                        </div>
                        <!-- end size-block -->
						<?php
					endForeach;
				endIf;
				?>
            </div>
            <div class="selecione_tamanho" style="color: #F00; font-size: 14px; margin-top: 18px; font-weight: bold;"></div>
            <div class="Cores bloco2">
<?php

if (count($variacoes) > 0):

    foreach ($variacoes as $tipoVariacao2 => $v):
        foreach ($v as $tipoVariacao => $variacao):
            $attr = "display:none";
            foreach ($produto['VariacaoProduto'] as $va) {

                if (count($variacao) == 1 || $variacao['Data']['valor'] == $va['Variacao']['valor']) {
                    $attr = "display:block";
                    break;
                }
            }
            ?>
                            <!-- start size-block -->
                            <div class="choose-block variacoes variacoes-<?php echo Inflector::slug($variacao['Data']['valor']); ?>" style="<?php echo $attr; ?>">
                                <h4><?php echo $variacao['Data']['tipo_variacao']; ?></h4>
                                <ul>
                            <?php
                            foreach ($variacao['tamanhos'] as $chave => $valor):


                                $class = "";
                                $img = "";
                                $class2 = "";
                                if (count($variacao) == 1 || $valor['Produto']['id'] == $produto['Produto']['id']) {
                                    $class = "variacaoSelected";
                                }
                                if ($valor['Produto']['quantidade_disponivel'] <= 0 || $valor['Produto']['status'] == 2) {
                                    $class2 = " false";
                                } else {
                                    $class2 = "";
                                }
                                ?>
                                        <li class="<?php
                        echo $class;
                        echo $class2;
                                ?>">
                                            <a title="<?php echo $valor['Produto']['nome']; ?>" href="<?php echo $this->Html->Url("/" . low(Inflector::slug($valor['Produto']['nome'], '-')) . '-' . 'prod-' . $valor['Produto']['id']) ?>.html"><?php echo trim($valor['Variacao']['valor']); ?></a>

                                        </li>
                                        <?php
                                    endForeach;
                                    ?>
                                </ul>
                                <div class="clear"></div>
                            </div>
                            <!-- end size-block -->
                                    <?php
                                endForeach;
                            endForeach;
                        endIf;
                        ?>
            </div>
            <!-- end Cores -->

            <div class="clear"></div>
        </div>
        <!-- end column -->
        <div class="clear"></div>
        <!-- start tab -->
        <div class="tab" id="detail-tabs">
            <ul>
                <li><a href="#tab_descricao_produto" title="<?php echo __("Descrição"); ?>"><?php echo __("Descrição"); ?></a></li>
                <?php if ($produto['Produto']['video'] != ""): ?>
                    <li><a href="#tab_video_produto" title="<?php echo __("Vídeos"); ?>"><?php echo __("Vídeos"); ?></a></li>
    <?php endIf; ?>
<?php if (!empty($produto['ProdutoDescricao']['ficha_tecnica'])): ?>
                    <li><a href="#tab_ficha_tecnica" title="<?php echo __("Ficha Técnica"); ?>"><?php echo __("Ficha Técnica"); ?></a></li>
    <?php endIf; ?>
            </ul>
            <div class="clear"></div>

            <!-- start txtb -->
            <div class="txtb detail-tabs-content" id="tab_descricao_produto">
                <?php if (!empty($produto['ProdutoDescricao']['descricao'])): ?>
                    <p>
		    <?php echo trim($produto['ProdutoDescricao']['descricao']) . '<br />'; ?>
                    </p>
                    <?php
                else:
                    ?>
                    <p><?php echo __("Descrição Indisponível"); ?></p>
                    <?php endIf; ?>
            </div>
            <!-- end txtb -->

                    <?php if ($produto['Produto']['video'] != ""): ?>
                <!-- start video-produto -->
                <div class="txtb detail-tabs-content" id="tab_video_produto" style="align: center;">
                    <?php echo $produto['Produto']['video']; ?>
                </div>
                <!-- end video-produto -->
                    <?php endIf; ?>

                <?php
                if (!empty($produto['ProdutoDescricao']['ficha_tecnica'])):
                    $temp2 = explode("\n", $produto['ProdutoDescricao']['ficha_tecnica']);
                    ?>
                <div class="txtb detail-tabs-content" id="tab_ficha_tecnica" style="align: center;">
                    <p>
                    <?php
                    foreach ($temp2 AS $valor):
                        ?>
                    <?php echo trim($valor) . '<br />'; ?>
                    <?php
                endForeach;
                ?>
                    </p>
                </div>
                <?php endIf; ?>

        </div>
        <!-- end tab -->


		<?php if($produto['Produto']['kit'] == true && $produto['Produto']['kit_produtos'] != ""): ?>
		<div class="produtos_kit">
			<div class="category2">
				<div class="product-list">
					<h3>Produtos que compõem o kit</h3>
					<ul>
						<?php $produtos_kit = json_decode($produto['Produto']['kit_produtos'], true); ?>
						<?php foreach($produtos_kit as $prd_kit): ?>
							<li>
								<?php $img = ( isset($prd_kit[0]['imagem']) && file_exists($prd_kit[0]['imagem']) ) ? $prd_kit[0]['imagem'] : "uploads/produto_imagem/filename/sem_imagem.jpg"; ?>
								<span>
								<a href="<?php e($this->Html->Url("/" . low(Inflector::slug($prd_kit['Produto']['nome'], '-')) . '-' . 'prod-' . $prd_kit['Produto']['id'])) ?>.html" title="<?php echo $prd_kit['Produto']['nome'] ?>">
									<?php echo $prd_kit['Produto']['nome']; ?>
								</a>
								</span>
								<a href="<?php e($this->Html->Url("/" . low(Inflector::slug($prd_kit['Produto']['nome'], '-')) . '-' . 'prod-' . $prd_kit['Produto']['id'])) ?>.html" title="<?php echo $prd_kit['Produto']['nome'] ?>">
									<?php echo $image->resize($img, 113, 113); ?>
								</a>

								<?php if(($prd_kit['Produto']['preco_real'] > $prd_kit['Produto']['preco'])): ?>
									<p>Fora do KIT: R$ <?php echo $prd_kit['Produto']['preco_real']; ?></p>
									<p>No KIT: R$ <?php echo $prd_kit['Produto']['preco']; ?></p>
								<?php else: ?>
									<p>R$ <?php echo $prd_kit['Produto']['preco_real']; ?></p>
								<?php endIf; ?>

								<?php if(isset($prd_kit['Produto']['quantidade'])): ?>
									<?php if($prd_kit['Produto']['quantidade'] == 1): ?>
										<p><?php echo $prd_kit['Produto']['quantidade']; ?> Unidade</p>
									<?php else: ?>
										<p><?php echo $prd_kit['Produto']['quantidade']; ?> Unidades</p>
									<?php endIf; ?>
								<?php endIf; ?>
							</li>
						<?php endForeach; ?>
					</ul>
				</div>
			</div>
		</div>
		<?php endIf; ?>

    </div>
    <!-- end left-col -->
    <!-- start right-col -->
    <div class="right-col">
        <!-- start column3rd -->
        <div class="column3rd">

            <?php if ($produto['Produto']['quantidade_disponivel'] > 0 && $produto['Produto']['status'] != 2): ?>

                <!-- start price-tag -->
                <div class="price-tag">

    <?php
    //begin restrição de idioma
    if ($session->read('linguagem_default') == Configure::read('Config.language')):

        if ($produto['Produto']['preco_promocao'] > 0 && $produto['Produto']['preco_promocao']<$produto['Produto']['preco']){
            ?>
                    <span><?php echo __("À vista"); ?></span>
                    <span class="price_top"><?php echo __("De"); ?>: R$ <?php e($this->String->bcoToMoeda($produto['Produto']['preco'])) ?></span>
                    <strong><?php echo __("Por"); ?>: <?php e($this->String->bcoToMoeda($produto['Produto']['preco_promocao'])) ?></strong>
            <?php }else{ ?>
                <?php if($produto['Produto']['kit_desconto'] > 0){ ?>
                    <span><?php echo __("À vista"); ?></span>
                    <span class="price_top"><?php echo __("De"); ?>: R$ <?php e($this->String->bcoToMoeda($produto['Produto']['preco'])) ?></span>
                    <?php
                        $desconto = $produto['Produto']['preco'] * $produto['Produto']['kit_desconto'] / 100;
                        $result = $produto['Produto']['preco'] - $desconto;
//                        var_dump($result);die;
                    ?>
                    <strong><?php echo __("Por"); ?>: <?php e($this->String->bcoToMoeda($result)) ?></strong>

                <?php }else{ ?>
                    <span><?php echo __("À vista"); ?></span>
                    <strong>R$ <?php e($this->String->bcoToMoeda($produto['Produto']['preco'])) ?></strong>
                <?php } ?>
            <?php } ?>

        <?php
        $preco = ($produto['Produto']['preco_promocao'] > 0) ? $produto['Produto']['preco_promocao'] : $produto['Produto']['preco'];
        $parcela_valida = $this->Parcelamento->parcelaValida($preco, $parcelamento);
        if ($parcela_valida['parcela'] > 1):
            ?>
                            <span><?php echo __("ou em até"); ?> <?php echo $parcela_valida['parcela'] ?>x <?php
                if ($parcela_valida['juros'] == true) {
                    echo __('com');
                } else {
                    echo __('sem');
                }
            ?> <?php echo __("juros"); ?></span>
                            <strong class="first">R$ <b><?php e($this->String->bcoToMoeda($parcela_valida['valor'])); ?></b></strong>
                            <?php endIf; ?>

                        <?php
                    endIf;
                    //begin restrição de idioma
                    ?>

                    <!-- start col -->
                    <div class="col">
                        <span><?php echo __("Disponível em estoque"); ?></span>
			<strong>&nbsp;</strong>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end price-tag -->

                <form action="#" class="search3">
                    <label for="cep"><?php echo __("Calcule o valor do frete"); ?>:</label>
    <?php echo $this->Form->text("Frete.cep", array("class" => "input3 cep mask-cep", "id" => "cep", "default" => "CEP")); ?>
                    <a href="javascript:void(0);" class="button3" id="consultar-frete-em-detalhes" rel="<?php echo $produto['Produto']['id'] ?>" style="float: left; text-align: center;"></a>
                </form>
    <?php echo $this->Html->image('/img/site/zoomloader.gif', array('class' => 'loading', 'style' => 'margin-left: 5px;
    margin-top: 5px; visibility: hidden;', 'alt' => 'Carregando', 'title' => 'Carregando')); ?>
                <span class="url-correios"><?php echo __("Não sabe seu CEP?"); ?> <a href="http://www.buscacep.correios.com.br/" target="_blank"><?php echo __("Clique aqui"); ?>.</a></span>

                <div class="clear"></div>

                <!-- start fretes-disponiveis -->
                <span class="red fretes-disponiveis-detalhe"></span>
                <!-- end fretes-disponiveis -->

                <!-- start fretes-disponiveis-msg -->
                <div class="fretes-disponiveis-msg"></div>
                <!-- end fretes-disponiveis-msg -->

                <div class="clear"></div>

                <!-- start right-lind -->
                <div class="right-link">
                    <ul>
                        <li style="display: none;"><a href="javascript:void(0);" title="<?php echo __("Adicionar a Lista de Presentes"); ?>"><?php echo __("Adicionar a Lista de Presentes"); ?></a></li>
                        <li><a href="<?php echo $this->Html->url('/tire-suas-duvidas'); ?>" title="<?php echo __("Tire suas dúvidas"); ?>"><?php echo __("Tire suas dúvidas"); ?></a></li>
                    </ul>

    <?php
    //begin restrição de idioma
    if ($session->read('linguagem_default') == Configure::read('Config.language')):
        ?>


                        <a href="<?php e($this->Html->url("/carrinho/carrinho/add/{$produto['Produto']['id']}")) ?>" title="Comprar" class="button4">
                            Comprar
                        </a>


        <?php
    endIf;
    //end restrição de idioma
    ?>

                    <div class="clear"></div>
                    <ul class="last">
                        <li><a href="<?php echo $this->Html->url('/saiba-como-comprar'); ?>" title="<?php echo __("Saiba como comprar"); ?>"><?php echo __("Saiba como comprar"); ?></a></li>
                    </ul>
                </div>
                <!-- end right-link -->

                <?php else: ?>


		<!-- start price-tag -->
                <div class="price-tag">


    <?php
    //begin restrição de idioma
    if ($session->read('linguagem_default') == Configure::read('Config.language')):

        if ($produto['Produto']['preco_promocao'] > 0):
            ?>
                            <span><?php echo __("À vista"); ?></span>
                            <span class="price_top"><?php echo __("De"); ?>: R$ <?php e($this->String->bcoToMoeda($produto['Produto']['preco'])) ?></span>
                            <strong><?php echo __("Por"); ?>: <?php e($this->String->bcoToMoeda($produto['Produto']['preco_promocao'])) ?></strong>
            <?php
        else:
            ?>
                            <span><?php echo __("À vista"); ?></span>
                            <strong>R$ <?php e($this->String->bcoToMoeda($produto['Produto']['preco'])) ?></strong>
                        <?php
                        endIf;
                        ?>

        <?php
        $preco = ($produto['Produto']['preco_promocao'] > 0) ? $produto['Produto']['preco_promocao'] : $produto['Produto']['preco'];
        $parcela_valida = $this->Parcelamento->parcelaValida($preco, $parcelamento);
        if ($parcela_valida['parcela'] > 1):
            ?>
                            <span><?php echo __("ou em até"); ?> <?php echo $parcela_valida['parcela'] ?>x <?php
                if ($parcela_valida['juros'] == true) {
                    echo __('com');
                } else {
                    echo __('sem');
                }
            ?> <?php echo __("juros"); ?></span>
                            <strong class="first">R$ <b><?php e($this->String->bcoToMoeda($parcela_valida['valor'])); ?></b></strong>
                            <?php endIf; ?>

                        <?php
                    endIf;
                    //begin restrição de idioma
                    ?>


                    <div class="box-produto-indisponivel">
                        <h2><?php echo __("Consulte Disponibilidade"); ?></h2>
                    </div>
                    <!-- start col -->
                    <?php /* 
                    <div class="col">
                        <span><?php echo __("Indisponível"); ?></span>
                        <strong>&nbsp;</strong>
                    </div>
                    */?>
                    <!-- end col -->
                </div>
                <!-- end price-tag -->

                <!-- start aviseme -->
                <div class="aviseme">
                     
                    <div class="right-link">
                        <ul>
                            <li>
                                <h2 class="title"><?php echo __("Avise-me quando chegar"); ?></h2>
                            <li>
                        </ul>
                    </div>
                    
    <?php echo $this->Form->create('Produto', array("class" => "form-aviseme search3", "url" => $this->Html->url(null, true))); ?>
                    <!-- start linha -->
                    <div class="linha">
    <?php echo $this->Session->flash(); ?>
                    </div>
                    <!-- end linha -->
    <?php if (!isset($enviado)) { ?>
                        <!-- start fill-form -->
                        <div class="fill-form">
                            <!-- start col-white -->
                            <div class="col-white">
                                <!-- start linha -->
                                <div class="row1">
                                    <label><?php echo __("Nome"); ?>*</label>
        <?php echo $this->Form->input("Avise.nome", array('label' => false, 'div' => true, 'class' => 'input3')); ?>
                                </div>
                                <!-- end linha -->
                                <!-- start linha -->
                                <div class="row1">
                                    <label><?php echo __("Email"); ?>*</label>
        <?php echo $this->Form->input("Avise.email", array('label' => false, 'div' => true, 'class' => 'input3')); ?>
                                </div>
                                <!-- end linha -->
                                    <?php echo $this->Form->hidden("Avise.produto_id", array("value" => $produto['Produto']['id'])); ?>
                                <input type="submit" name="enviaravise" value="<?php echo __("ENVIAR"); ?>" class="link button1 gray left"/>
                                <div class="clear"></div>
                                <p>* <?php echo __("campos obrigatórios"); ?>.</p>
                            </div>
                            <!-- end col-white -->
                        </div>
                        <!-- end fill-form -->
    <?php } ?>
                            <?php echo $this->Form->end(); ?>
                </div>
                <!-- end aviseme -->
<?php endIf; ?>

            <div class="clear"></div>
        </div>
        <!-- end column3rd -->
        

        <?php if ($produto['Produto']['quantidade_disponivel'] <= 0 && $produto['Produto']['status'] != 2): ?>
        
        <!-- start column3rd -->
        <div class="column3rd" style="background-color: #fff;">
            	
            	<div class="box-produto-indisponivel">
                	<img src="<?php echo $this->Html->Url('/img/site/'); ?>fale-conosco.png" />
                </div>
            	<!-- start content -->
                <!-- FT-379div style="font-size: 12px; line-height: 14px">
                        
                        < span>E-mail: comercial@fueltech.com.br</span><br />
                        <span>Telefone: +55 (51) 3019-0500</span><br />
                        <span>Nextel:</span><br />
                        <span>Comercial - 82*109584.</span><br />
                        <span>SAC - 82*6009</span><br />
                        <span>SAC - Manuten&ccedil;&atilde;o 82*107006/51-7815 1241</span></div>
                        <br /><br />
                        <span style="font-size: 12px; line-height: 14px">
                                Hor&aacute;rio de atendimento:<br />
                                De segunda &agrave; sexta-feira:<br />
                                das 8h30min &agrave;s 12h e das 13h30min &agrave;s 18h<br />
                        </span>
                </div-->
				<!-- end content -->

            <div class="clear"></div>
        </div>
        <!-- end column3rd -->
        
        <?php endif;?>
        
                <?php if (count($produto['ProdutoDownload']) > 0): ?>
            <!-- start Downloads-block -->
            <div class="Download-block">
                <h3><?php echo __("Downloads e Softwares"); ?></h3>
                <?php foreach ($produto['ProdutoDownload'] as $prod_down): ?>
                    <p>
                        <a href="<?php echo $this->Html->Url("/downloads/download/") . $prod_down['Download']['id'] . "/" . low(Inflector::slug($prod_down['Download']['nome'], '-')) . "/"; ?>" title="<?php echo $prod_down['Download']['nome']; ?>">
        <?php echo $prod_down['Download']['nome']; ?>
                        </a>
                    </p>
                <?php endForeach; ?>
            </div>
            <!-- end Downloads-block -->
                <?php endIf; ?>
    </div>
    <!-- end right-col -->
</div>
<!-- end product-detail -->

<div class="clear"></div>

<!-- início produtos relacionados -->
<div class="category2 first">
	<div class="product-list">
	<h3>Compre também</h3>
	<ul>
	    <?php $count = 1; ?>
	    <?php foreach ($produtos_relacionados as $produto_relacionado) : ?>
	    <?php
	    $img = $produto_relacionado[0]['imagem'];
	    ?>
	    <li class="<?php echo ($count === 5) ? 'last' : ''; ?>">
		<h4>
		    <a href="<?php echo $this->Html->url('/' . low(Inflector::slug($produto_relacionado['Produto']['nome'], '-')) . '-prod-' . $produto_relacionado['Produto']['id'] . '.html'); ?>">
		    <?php echo $produto_relacionado['Produto']['nome']; ?>
		    </a>
		</h4>

		<a href="<?php echo $this->Html->url('/' . low(Inflector::slug($produto_relacionado['Produto']['nome'], '-')) . '-prod-' . $produto_relacionado['Produto']['id'] . '.html'); ?>">
		<?php echo $image->resize(isset($img) ? $img : 'uploads/produto_imagem/filename/sem_imagem.jpg', 152, 113, true, array('alt' => Inflector::slug($produto_relacionado['Produto']['nome'], ' '))); ?>
		</a>

		<p class="orange">
		    <strong><small>R$
		    <?php if ($produto_relacionado['Produto']['preco_promocao'] > 0) : ?>
		    <?php e($this->String->bcoToMoeda($produto_relacionado['Produto']['preco_promocao'])) ?>
		    <?php else : ?>
		    <?php e($this->String->bcoToMoeda($produto_relacionado['Produto']['preco'])) ?>
		    <?php endif; ?>
		    </small> à vista</strong> a

		    <?php
		    $preco = ($produto['Produto']['preco_promocao'] > 0) ? $produto_relacionado['Produto']['preco_promocao'] : $produto_relacionado['Produto']['preco'];
		    $parcela_valida = $this->Parcelamento->parcelaValida($preco, $parcelamento);
		    ?>
		    <?php if ($parcela_valida['parcela'] > 1) : ?>
		    ou em até <?php echo $parcela_valida['parcela'] ?>x
		    R$ <?php e($this->String->bcoToMoeda($parcela_valida['valor'])); ?>.
		    <?php endIf; ?>
		</p>

	    </li>
	    <?php $count++; ?>
	    <?php endforeach; ?>
	</ul>
	<div class="clear"></div>
    </div>
</div>
<!-- fim produtos relacionados -->
