<?php
echo $javascript->link('common/swfobject.js', false);
echo $javascript->link('common/jquery.uploadify.v2.1.4.min' ,false);
echo $javascript->link('common/jquery.meio_mask.js',false);
echo $javascript->link('common/tiny_mce/tiny_mce.js',false);
echo $javascript->link('common/jquery.cookie.js',false);
echo $javascript->link('common/jquery-ui-1.8.16.custom.min',false);
echo $this->Html->css('common/ui-lightness/jquery-ui-1.8.16.custom.css');
echo $javascript->link('admin/produtos/crud.js',false);
echo $javascript->link('common/asmselect/jquery.asmselect',false);
echo $this->Html->css('common/asmselect/jquery.asmselect.css');
?>
<div class="index">
<?php echo $this->Form->create('Produto',array('type' => 'file'));?>
	<?php
		if(isset($validationErrors)){
			$this->Form->validationErrors = $validationErrors;
		}
	?>
	<fieldset>
		<legend><?php __('Editar Produto'); ?></legend>

		<div class="listaabas">
			<div class="clicaaba active" rel="0">GERAL</div>
			<div class="clicaaba" rel="1">DADOS</div>
			<div class="clicaaba" rel="2">CATEGORIZAÇÃO</div>
			<div class="clicaaba" rel="3">RELACIONAMENTOS</div>
			<div class="clicaaba" rel="4">IMAGENS</div>
			<div class="clicaaba" rel="5">PREÇO</div>
			<div class="clicaaba" rel="6">ATRIBUTOS</div>
			<div class="clicaaba" rel="7">VARIAÇÕES</div>
			<div class="clicaaba" rel="8">DOWNLOADS</div>
		</div>
		<br class="clear" />
		<div class="abas" rel="0">
			<?php echo $this->Form->input('status', array('default'=>1,'type' => 'radio', 'options' => array(1 => 'Ativo', 2 => 'Indisponível', 0 => 'Inativo'))); ?>
			<div class="left">

				<div class="left">
					<?php echo $this->Form->input('sku',array('label'=>'SKU','class'=>'inputs w147')); ?>
					<?php echo $this->Form->input('id'); ?>
				</div>
                                <div class="left">
					<?php echo $this->Form->input('codigo_erp',array('label'=>'COD-ERP','class'=>'inputs w147')); ?>
				</div>
                                <div class="left">
					<?php echo $this->Form->input('ordem',array('label'=>'Ordem','class'=>'inputs w147')); ?>
				</div>
				<div class="left clear tab" id="detail-tabs" style="margin-top: 15px;">
					<ul>
						<?php foreach($idiomas as $key => $idioma): ?>
							<li>
								<a href="#tab-content-idioma-<?php echo $idioma['Linguagem']['id']; ?>">
									<?php $img = ( isset($idioma['Linguagem']['thumb_filename']) ) ? $idioma['Linguagem']['thumb_dir'] . '/' . $idioma['Linguagem']['thumb_filename'] : "uploads/produto_imagem/thumb/sem_imagem.jpg"; ?>
									<?php echo $image->resize($img, 20, 20); ?>
									<?php echo $idioma['Linguagem']['nome']; ?>
								</a>
							</li>
						<?php endForeach; ?>
					</ul>

					<?php foreach($idiomas as $key => $idioma): ?>
					<div id="tab-content-idioma-<?php echo $idioma['Linguagem']['id']; ?>">

						<?php echo $this->Form->input("ProdutoDescricao.".$key.".id"); ?>
						<?php echo $this->Form->input("ProdutoDescricao.".$key.".produto_id", array('hiddenField'=>false,'type' => 'hidden','value' => $this->params['pass'][0])); ?>
						<?php echo $this->Form->input("ProdutoDescricao.".$key.".language", array('hiddenField'=>false,'type' => 'hidden','value' => $idioma['Linguagem']['codigo'])); ?>

						<div class="left clear">
							<?php echo $this->Form->input("ProdutoDescricao.".$key.".nome",array('label'=>'Nome do Produto','class'=>'w312 inputs')); ?>
						</div>
						<div class="left clear">
							<?php echo $this->Form->input("ProdutoDescricao.".$key.".descricao", array('class'=>'mceEditor wCEM h300 w700','type' => 'textarea', 'label' => 'Descrição')); ?>
						</div>
						<div class="left">
							<?php echo $this->Form->input("ProdutoDescricao.".$key.".descricao_resumida", array('class'=>'mceEditor wCEM h300 w700','type' => 'textarea', 'label' => 'Descrição Resumida')); ?>
						</div>
						<div class="left">
							<?php echo $this->Form->input("ProdutoDescricao.".$key.".ficha_tecnica", array('class'=>'mceEditor wCEM h300 w700','type' => 'textarea', 'label' => 'Ficha Técnica')); ?>
						</div>
						<div class="left clear">
							<?php echo $this->Form->input("ProdutoDescricao.".$key.".seo_title",array('label'=>'Seo Title','class'=>'inputs')); ?>
						</div>
						<div class="left clear">
							<?php echo $this->Form->input("ProdutoDescricao.".$key.".seo_meta_description",array('type'=>'textArea','label'=>'Seo Meta Description','class'=>'inputs')); ?>
						</div>
						<div class="left clear">
							<?php echo $this->Form->input("ProdutoDescricao.".$key.".seo_institucional",array('label'=>'Seo Institucional','class'=>'inputs')); ?>
						</div>
						<div class="left clear">
							<?php echo $this->Form->input("ProdutoDescricao.".$key.".seo_meta_keywords",array('label'=>'Seo Meta Keywords','class'=>'inputs')); ?>
						</div>
						<div class="left clear">
							<?php echo $this->Form->input("ProdutoDescricao.".$key.".tag",array('type'=>'textArea','label'=>'Tags (escrever palavras-chave separadas por vírgula)','class'=>'w312')); ?>
						</div>

					</div>
					<?php endForeach; ?>

				</div>

			</div>
		</div>

		<div class="abas shirion" rel="1">
			<div class="left clear">

				<div style="">
					<?php echo $this->Form->input('kit', array('default' => 0, 'legend' => 'Tipo KIT', 'type' => 'radio', 'options' => array(1 => 'Sim', 0 => 'Não'))); ?>
				</div>

				<div class="box_produtos_kit">

					<legend>Produto que compõem o KIT</legend>

					<div class="container-kit container">
						<?php echo $this->Html->image('/img/site/zoomloader.gif', array('class' => 'loading', 'style' => 'display:none', 'alt' => 'Carregando', 'title' => 'Carregando')); ?>
						<div class="left">
							<?php
								echo $this->Form->input("ProdutoKit.0.kit_id", array('hiddenField'=>false,'value' => $this->params['pass'][0], 'type' => 'hidden'));
								echo $this->Form->input("ProdutoKit.0.produto", array('class'=>'w147 busca_produto','hiddenField'=>false,'label' => 'Buscar Produto', 'after'=>$this->Html->link('Adicionar','javascript:;',array('class'=>'buscar-produtos'))));
							?>
						</div>
						<div class="left">
							<?php
								$kit_sel0 = (isset($kit_sel[0])) ? $kit_sel[0] : array();
								echo $this->Form->input("ProdutoKit.0.produto_id", array('hiddenField'=>false,'label' => 'Selecionar Produto','type' => 'select', 'options' => $kit_sel0, 'class' => 'produtos select_produto'));
							?>
						</div>
						<div class="left">
							<?php
								echo $this->Form->input("ProdutoKit.0.quantidade", array('hiddenField'=>false, 'label' => 'Quantidade', 'class' => 'mask-numerico field_quantidade', 'default' => 1));
							?>
						</div>
						<div class="left">
							<?php
                                echo $this->Form->input("ProdutoKit.0.preco", array('hiddenField'=>false, 'label' => 'Preço Unitário', 'class' => 'mask-moeda field_preco'));
                            ?>
                            <div class="input_hidden field_preco_real">
								<?php echo $this->Form->input("ProdutoKit.0.preco_real", array('hiddenField'=>false, 'label' => false, 'class' => 'mask-moeda')); ?>
							</div>
						</div>
						<div class="left" style="padding-top: 37px">
							<a href="#" class="add">add</a>
							<a href="#" class="rm" style="display:none">rm</a>
						</div>
						<div class="left" style="padding-top: 30px">
						<?php
							if(isset($this->data['ProdutoKit'][0])){
								?>
								<div class="content-kit-rm-0">
								<?php
								echo $this->Form->input("ProdutoKit.0.delete", array('type' => 'checkbox'));
								?>
								</div>
								<?php
							}
						?>
						</div>
						<div class="clear"></div>
					</div>

					<div class="container-kit-tmp">
						<?php
						if (isset($this->data['ProdutoKit']) && count($this->data['ProdutoKit']) > 1) {

							$first = true;
							foreach ($this->data['ProdutoKit'] as $id => $quantidade):
								if ($first) {
									$first = false;
									continue;
								}
							?>
							<div class="container">
								<div class="left">
									<?php
										echo $this->Form->input("ProdutoKit.{$id}.kit_id", array('hiddenField'=>false,'value' => $this->params['pass'][0], 'type' => 'hidden'));
										echo $this->Form->input("ProdutoKit.{$id}.produto", array('class'=>'w147 busca_produto','hiddenField'=>false,'label' => 'Buscar Produto', 'after'=>$this->Html->link('Adicionar','javascript:;',array('class'=>'buscar-produtos'))));
									?>
								</div>
								<div class="left">
									<?php
										echo $this->Form->input("ProdutoKit.{$id}.produto_id", array('hiddenField'=>false, 'label' => 'Selecionar Produto', 'type' => 'select', 'options' => $kit_sel[$id], 'class' => 'produtos select_produto'));
									?>
								</div>
								<div class="left">
									<?php
										echo $this->Form->input("ProdutoKit.{$id}.quantidade", array('hiddenField'=>false, 'label' => 'Quantidade', 'class' => 'mask-int field_quantidade', 'default' => 1));
									?>
								</div>
								<div class="left">
									<?php
                                        echo $this->Form->input("ProdutoKit.{$id}.preco", array('hiddenField'=>false, 'label' => 'Preço', 'class' => 'mask-moeda field_preco'));
                                    ?>
                                    <div class="input_hidden field_preco_real">
										<?php echo $this->Form->input("ProdutoKit.{$id}.preco_real", array('hiddenField'=>false, 'label' => false, 'class' => 'mask-moeda')); ?>
									</div>
								</div>
								<div class="left" style="padding-top: 35px">
									<?php
										if($quantidade["produto_id"]){
											echo $this->Form->input("ProdutoKit.{$id}.delete", array('type' => 'checkbox'));
										}else{
											echo $this->Html->link("rm",array(), array('class' => 'rm'));
										}
									?>
								</div>
							</div>

							<div class="clear"></div>
							<?php endForeach; ?>
						<?php } ?>
					</div>
                                        <div class="clear"></div>
                                        <div style="display:block;width:100%;height:160px;border-top:1px solid #D1D1D1">
                                            <h3>Campos Calculados:</h3>
                                            <div class="left clear">
                                                    <?php echo $this->Form->input('largura',array('label'=>'Largura', 'readonly'=>'readonly','class'=>'inputs w147 mask-moeda')); ?>
                                            </div>
                                            <div class="left">
                                                    <?php echo $this->Form->input('altura',array('label'=>'Altura', 'readonly'=>'readonly','class'=>'inputs w147 mask-moeda')); ?>
                                            </div>
                                            <div class="left">
                                                    <?php echo $this->Form->input('profundidade',array('label'=>'Profundidade', 'readonly'=>'readonly','class'=>'inputs w147 mask-moeda')); ?>
                                            </div>
                                            <div class="left clear">
                                                    <?php echo $this->Form->input('peso',array('label'=>'Peso', 'readonly'=>'readonly','class'=>'inputs w147')); ?>
                                            </div>
                                        </div>

				</div>

				<div class="box_produtos_sem_kit">

					<div class="left clear">
						<?php echo $this->Form->input('preco', array('class' => 'mask-moeda inputs w147', 'label' => 'Preço de')); ?>
					</div>
					<div class="left">
						<?php echo $this->Form->input('preco_promocao', array('class' => 'mask-moeda inputs w147', 'label' => 'Preço por')); ?>
					</div>
					<div class="left clear">
						<?php echo $this->Form->input('preco_promocao_inicio', array('type'=>'text','class' => 'datePicker inputs w147', 'label' => 'Promoção de:')); ?>
					</div>
					<div class="left">
						<?php echo $this->Form->input('preco_promocao_fim', array('type'=>'text','class' => 'datePicker inputs w147', 'label' => 'Promoção até:')); ?>
					</div>
					<div class="left">
						<?php echo $this->Form->input('preco_promocao_velho', array('class' => 'mask-moeda inputs w147', 'label' => 'Preço promoção de')); ?>
					</div>
					<div class="left">
						<?php echo $this->Form->input('preco_promocao_novo', array('class' => 'mask-moeda inputs w147', 'label' => 'Preço promoção por')); ?>
					</div>
					<div class="left clear">
						<?php echo $this->Form->input('largura',array('label'=>'Largura','class'=>'inputs w147 mask-moeda')); ?>
					</div>
					<div class="left">
						<?php echo $this->Form->input('altura',array('label'=>'Altura','class'=>'inputs w147 mask-moeda')); ?>
					</div>
					<div class="left">
						<?php echo $this->Form->input('profundidade',array('label'=>'Profundidade','class'=>'inputs w147 mask-moeda')); ?>
					</div>
					<div class="left clear">
						<?php echo $this->Form->input('peso',array('label'=>'Peso','class'=>'inputs w147')); ?>
					</div>
					<div class="left">
						<?php echo $this->Form->input('peso_ramos',array('label'=>'Peso Ramos','class'=>'inputs w147')); ?>
					</div>

				</div>

				<div class="left clear" id="div_quantidade">
					<?php echo $this->Form->input('quantidade', array('label' => 'Quantidade','class'=>'inputs w147')); ?>
				</div>
				<div class="left">
					<?php echo $this->Form->input('unidade_venda', array('label' => 'Unidade Venda','class'=>'inputs w147')); ?>
				</div>
				<div class="left" id="div_kit_desconto">
					<?php echo $this->Form->input('kit_desconto', array('label' => 'Desconto (%)','class'=>'inputs mask-int w147')); ?>
				</div>
				<div class="left clear">
					<?php  echo $this->Form->input('video',array('type'=>'textArea','label'=>'Vídeo (colar o embed do video aqui)','class'=>'w312')); ?>
				</div>
			</div>
		</div>

        <div class="abas shirion" rel="2">

			<div class="left">
				<?php echo $this->Form->input('Categoria',array('div'=>false,'label'=>'Categoria','class'=>'categoria_produtos_sel')); ?>
			</div>

			<div class="left clear">
				<?php if(count($criancas)==0){ ?>
					<div class="left w210">
						<?php echo $this->Form->input('Buscarpai',array('id'=>'querypai','label'=>'Buscar','after'=>$this->Html->link('Adicionar','javascript:;',array('id'=>'buscar-pai')))); ?>
					</div>
					<div class="left ">
						<?php echo $this->Form->input('parent_id',array('label'=>'Pai','class'=>'produtosSel w147')); ?>
					</div>
				<?php }else{ ?>
					<h2>Este produto possui <?php echo (count($criancas)); ?> filhos.</h2>
					<table cellpadding="0" cellspacing="0">
						<tr>
							<th>Codigo</th>
							<th>Nome</th>
							<th>Ação</th>
						</tr>
						<?php
							$i = 0;
							foreach ($criancas as $item):
								$class = null;
								if ($i++ % 2 == 0) {
									$class = ' class="altrow"';
								}
						?>
								<tr <?php echo $class; ?>>
									<td><?php echo $item['Produto']['sku']; ?>&nbsp;</td>
									<td>
										<?php echo $this->Html->link($item['Produto']['nome'], array('action' => 'edit/'.$item['Produto']['id'])); ?>
									</td>
									<td class="childrens-radio">
										<?php echo $this->Form->radio('Produto.children_parent_id', array($item['Produto']['id'] => 'Tornar Principal'),array('class'=>'childrens-produto','hiddenField'=>false,'id'=>microtime())); ?>
									</td>
								</tr>
						<?php endforeach; ?>
					</table>

				<?php } ?>
			</div>

        </div>

        <div class="abas shirion" rel="3">
			<div class="left">
				<div class="left selectgrande">
					<fieldset>
						<legend><?php __('Produtos Relacionados'); ?></legend>
						<?php
						if(empty($this->data['Produto']['Relacionados'])){
							$this->data['Produto']['Relacionados'] = '';
						}

						echo $this->Form->input('Buscar',array('id'=>'query','after'=>$this->Html->link('Adicionar','javascript:;',array('id'=>'buscar-produtos'))));
						$botoes = $this->Html->link('Adicionar','javascript:;',array('id'=>'add','class'=>'add'));
						$botoes .= $this->Html->link('Remover','javascript:;',array('id'=>'rm','class'=>'rm'));
						echo $this->Form->input('Produto.Selecionados',array('id'=>'buscados','type' => 'select', 'multiple' => true,'after'=>"<p>$botoes</p>"));
						echo $this->Form->input('Produto.Relacionados',array('options'=>$this->data['Produto']['Relacionados'],'id'=>'selecionados','type' => 'select', 'multiple' => true));
						?>
					</fieldset>
				</div>
			</div>
			<br class="clear" />
        </div>

        <div class="abas shirion" rel="4">
			<fieldset>
				<legend>Adicionar Imagens</legend>
				<input id="edit-produto" name="file_upload" type="file" />
				<div class="container-edit">
					<?php echo $this->Uploadify->build($imgs_old); ?>
				</div>
			</fieldset>
        </div>

		<div class="abas shirion" rel="5">
			<fieldset>
				<legend>Produto Preço</legend>
				<div class="container-preco">
				<?php
					echo $this->Form->input("ProdutoPreco.0.id", array('hiddenField'=>false,'type' => 'hidden'));
					echo $this->Form->input("ProdutoPreco.0.produto_id", array('hiddenField'=>false,'value' => $this->params['pass'][0], 'type' => 'hidden'));
					echo $this->Form->input("ProdutoPreco.0.grupo_id", array('hiddenField'=>false,'value' => 1, 'type' => 'hidden'));
					echo $this->Form->input('ProdutoPreco.0.preco', array('hiddenField'=>false,'label' => 'Preço','class' => 'mask-moeda'));
					echo $this->Form->input('ProdutoPreco.0.quantidade', array('hiddenField'=>false,'label' => 'Quantidade'));
				?>
				<a href="#" class="add">add</a>
				<a href="#" class="rm" style="display:none">rm</a>
				</div>

				<?php
					if(isset($this->data['ProdutoPreco'][0]) && $this->data['ProdutoPreco'][0]['quantidade']){
						?>
						<div class="content-rm-0">
						<?php
						echo $this->Form->input("ProdutoPreco.0.delete", array('type' => 'checkbox'));
						?>
						</div>
						<?php
					}
				?>

				<div class="container-preco-tmp">
					<?php
					if (count($this->data['ProdutoPreco']) > 1) {
						$first = true;
						foreach ($this->data['ProdutoPreco'] as $id => $quantidade):
							if ($first) {
								$first = false;
								continue;
							}
						?>
						<div class="container">
						<?php
							echo $this->Form->input("ProdutoPreco.{$id}.id", array('hiddenField'=>false,'type' => 'hidden'));
							echo $this->Form->input("ProdutoPreco.{$id}.produto_id", array('hiddenField'=>false,'value' => $this->params['pass'][0], 'type' => 'hidden'));
							echo $this->Form->input("ProdutoPreco.0.grupo_id", array('hiddenField'=>false,'value' => 1, 'type' => 'hidden'));
							echo $this->Form->input("ProdutoPreco.{$id}.preco", array('hiddenField'=>false,'label' => 'Preço.','class' => 'mask-moeda'));
							echo $this->Form->input("ProdutoPreco.{$id}.quantidade", array('hiddenField'=>false,'label' => 'Quantidade'));
							if($quantidade["id"]){
								echo $this->Form->input("ProdutoPreco.{$id}.delete", array('type' => 'checkbox'));
							}else{
								echo $this->Html->link("rm",array(), array('class' => 'rm'));
							}
						?>
						</div>
						<?php endForeach; ?>
					<?php } ?>
				</div>
			</fieldset>
		</div>

		<div class="abas shirion" rel="6">

			<fieldset>
				<legend>Produto Atributo</legend>

					<div class="container-atributos container">
						<?php echo $this->Html->image('/img/site/zoomloader.gif', array('class' => 'loading', 'style' => 'display:none', 'alt' => 'Carregando', 'title' => 'Carregando')); ?>

						<?php
							echo $this->Form->input("AtributoProduto.0.produto_id", array('hiddenField'=>false,'value' => $this->params['pass'][0], 'type' => 'hidden'));
							echo $this->Form->input("AtributoProduto.0.atributo_tipo_id", array('hiddenField'=>false,'label' => 'Tipo de Atributo','type' => 'select', 'options' => $atributos_tipos, 'class' => 'atributos_tipos'));
							$atributo_sel2 = (isset($atributos_sel[0])) ? $atributos_sel[0] : array();
							echo $this->Form->input('AtributoProduto.0.atributo_id', array('hiddenField'=>false,'label' => 'Atributo','type' => 'select','options' => $atributo_sel2, 'class' => 'atributos'));
						?>
						<a href="#" class="add">add</a>
						<a href="#" class="rm" style="display:none">rm</a>
					</div>

					<?php
						if(isset($this->data['AtributoProduto'][0])){
							?>
							<div class="content-atributo-rm-0">
							<?php
							echo $this->Form->input("AtributoProduto.0.delete", array('type' => 'checkbox'));
							?>
							</div>
							<?php
						}
					?>

					<div class="container-atributos-tmp">
						<?php
						if (count($this->data['AtributoProduto']) > 1) {
							$first = true;
							foreach ($this->data['AtributoProduto'] as $id => $quantidade):
								if ($first) {
									$first = false;
									continue;
								}
							?>
							<div class="container">
							<?php
								echo $this->Form->input("AtributoProduto.{$id}.produto_id", array('hiddenField'=>false,'value' => $this->params['pass'][0], 'type' => 'hidden'));
								echo $this->Form->input("AtributoProduto.{$id}.atributo_tipo_id", array('hiddenField'=>false,'type' => 'select', 'options' => $atributos_tipos, 'class' => 'atributos_tipos', 'label' => 'Tipo de Atributo'));
								echo $this->Form->input("AtributoProduto.{$id}.atributo_id", array('hiddenField'=>false,'label' => 'Atributo','type' => 'select', 'options' => $atributos_sel[$id], 'class' => 'produtos'));
								if($quantidade["atributo_id"]){
									echo $this->Form->input("AtributoProduto.{$id}.delete", array('type' => 'checkbox'));
								}else{
									echo $this->Html->link("rm",array(), array('class' => 'rm'));
								}
							?>
							</div>
							<?php endForeach; ?>
						<?php } ?>
					</div>

			</fieldset>

		</div>

		<div class="abas shirion" rel="7">

			<fieldset>

				<legend>Produto Variações</legend>

				<div class="container-variacoes container">

					<?php echo $this->Html->image('/img/site/zoomloader.gif', array('class' => 'loading', 'style' => 'display:none', 'alt' => 'Carregando', 'title' => 'Carregando')); ?>

					<?php
						echo $this->Form->input("VariacaoProduto.0.produto_id", array('hiddenField'=>false,'value' => $this->params['pass'][0], 'type' => 'hidden'));
						echo $this->Form->input("VariacaoProduto.0.agrupador", array('hiddenField'=>false,'value' => $this->data['Produto']['agrupador'], 'type' => 'hidden'));
						echo $this->Form->input("VariacaoProduto.0.variacao_tipo_id", array('hiddenField'=>false,'label' => 'Tipo de Variação','type' => 'select', 'options' => $variacoes_tipos, 'class' => 'variacoes_tipos'));
						$variacao_sel2 = (isset($variacoes_sel[0])) ? $variacoes_sel[0] : array();
						echo $this->Form->input('VariacaoProduto.0.variacao_id', array('hiddenField'=>false,'label' => 'Variação','type' => 'select','options' => $variacao_sel2, 'class' => 'variacoes'));
					?>
					<a href="#" class="add">add</a>
					<a href="#" class="rm" style="display:none">rm</a>

				</div>

				<?php
					if(isset($this->data['VariacaoProduto'][0])){
						?>
						<div class="content-atributo-rm-0">
						<?php
							echo $this->Form->input("VariacaoProduto.0.delete", array('type' => 'checkbox'));
						?>
						</div>
						<?php
					}
				?>

				<div class="container-variacoes-tmp">
					<?php
						if (count($this->data['VariacaoProduto']) > 1) {
							$first = true;
							foreach ($this->data['VariacaoProduto'] as $id => $quantidade):
								if ($first) {
									$first = false;
									continue;
								}
							?>
							<div class="container">
								<?php
									echo $this->Form->input("VariacaoProduto.{$id}.produto_id", array('hiddenField'=>false,'value' => $this->params['pass'][0], 'type' => 'hidden'));
									echo $this->Form->input("VariacaoProduto.{$id}.agrupador", array('hiddenField'=>false,'value' => $this->data['Produto']['agrupador'], 'type' => 'hidden'));
									echo $this->Form->input("VariacaoProduto.{$id}.variacao_tipo_id", array('hiddenField'=>false,'type' => 'select', 'options' => $variacoes_tipos, 'class' => 'variacoes_tipos', 'label' => 'Tipo de Variação'));
									echo $this->Form->input("VariacaoProduto.{$id}.variacao_id", array('hiddenField'=>false,'label' => 'Variação','type' => 'select', 'options' => $variacoes_sel[$id], 'class' => 'variacoes'));
									if($quantidade["variacao_id"]){
										echo $this->Form->input("VariacaoProduto.{$id}.delete", array('type' => 'checkbox'));
									}else{
										echo $this->Html->link("rm",array(), array('class' => 'rm'));
									}
								?>
							</div>
							<?php endForeach; ?>
					<?php } ?>

				</div>

			</fieldset>

		</div>

		<div class="abas shirion" rel="8">

			<fieldset>

				<legend>Produto Downloads</legend>

				<div class="container-downloads container">

					<?php echo $this->Html->image('/img/site/zoomloader.gif', array('class' => 'loading', 'style' => 'display:none', 'alt' => 'Carregando', 'title' => 'Carregando')); ?>

					<?php
						echo $this->Form->input("ProdutoDownload.0.produto_id", array('hiddenField'=>false,'value' => $this->params['pass'][0], 'type' => 'hidden'));
						echo $this->Form->input("ProdutoDownload.0.download_tipo_id", array('hiddenField'=>false,'label' => 'Tipo de Download','type' => 'select', 'options' => $download_tipos, 'class' => 'download_tipos'));
						$downloads_sel2 = (isset($downloads_sel[0])) ? $downloads_sel[0] : array();
						echo $this->Form->input('ProdutoDownload.0.download_id', array('hiddenField'=>false,'label' => 'Download','type' => 'select','options' => $downloads_sel2, 'class' => 'downloads'));
					?>
					<a href="#" class="add">add</a>
					<a href="#" class="rm" style="display:none">rm</a>

				</div>

				<?php
					if(isset($this->data['ProdutoDownload'][0])){
						?>
						<div class="content-atributo-rm-0">
						<?php
							echo $this->Form->input("ProdutoDownload.0.delete", array('type' => 'checkbox'));
						?>
						</div>
						<?php
					}
				?>

				<div class="container-downloads-tmp">
					<?php
						if (count($this->data['ProdutoDownload']) >= 1) {
							$first = true;
							foreach ($this->data['ProdutoDownload'] as $id => $quantidade):
                                                               if ($first) {
									$first = false;
									continue;
								}
							?>
							<div class="container">
								<?php
									echo $this->Form->input("ProdutoDownload.{$id}.produto_id", array('hiddenField'=>false,'value' => $this->params['pass'][0], 'type' => 'hidden'));
									echo $this->Form->input("ProdutoDownload.{$id}.download_tipo_id", array('hiddenField'=>false,'type' => 'select', 'options' => $download_tipos, 'class' => 'download_tipos', 'label' => 'Tipo de Download'));
									echo $this->Form->input("ProdutoDownload.{$id}.download_id", array('hiddenField'=>false,'label' => 'Download','type' => 'select', 'options' => $downloads_sel[$id], 'class' => 'downloads'));
									if($quantidade["download_id"]){
										echo $this->Form->input("ProdutoDownload.{$id}.delete", array('type' => 'checkbox'));
									}else{
										echo $this->Html->link("rm",array(), array('class' => 'rm'));
									}
								?>
							</div>
							<?php endForeach; ?>
					<?php } ?>

				</div>

			</fieldset>

		</div>

        <br class="clear" />
		<?php echo $this->Form->end(__('Enviar', true));?>
	</fieldset>
</div>
