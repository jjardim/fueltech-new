<?php
echo $this->Html->css('common/nyro_modal/default', null, array('inline' => false));
echo $javascript->link('site/produtos/prazo_entrega', false);
?>
<div class="container-modal">
    <div class="container-header">
        <h1 class="logo"></h1>
    </div>
    <?php echo $form->create(null, array("controller" => "produtos", "action" => "prazo_entrega/{$this->params['pass'][0]}")); ?>
    <fieldset>
        <legend><b class="l"></b><b class="c">Calcular Prazo de Entrega</b><b class="r"></b></legend>
        <ul>
            <li class="clear"><?php echo $this->Session->flash(); ?></li>
            <li><?php echo $form->text("cep", array("label" => "Informe seu CEP:", "class" => "mask-cep", "id" => "cep")) ?></li>
            <li><?php echo $form->button("Enviar", array("class" => "bt-enviar")); ?></li>
        </ul>
        <ul>
            <?php
            $tipos = array(41106 => 'PAC', 40010 => 'SEDEX', 40215 => 'SEDEX10');
            if (isset($fretes)):
                foreach ($fretes as $valor): if (!isset($valor['Servicos']['CServico']['Codigo'])
                        )continue;
            ?>
                    <li><strong><?php echo $tipos[$valor['Servicos']['CServico']['Codigo']] ?></strong></li>
                    <li class="inside"><strong>Valor</strong> R$ <?php echo $valor['Servicos']['CServico']['Valor'] ?></li>
                    <li class="inside"><strong>Prazo</strong> <?php echo $valor['Servicos']['CServico']['PrazoEntrega'] ?> Dia(s)</li>
            <?php
                    endForeach;
                endIf;
            ?>
            <?php if (isset($error)) { ?>
                    <p><?php e($error) ?></p>
            <?php } ?>
        </ul>
        </fieldset>
<?php echo $form->end(); ?>
</div>
