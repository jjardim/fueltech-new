<?php
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js', false);
echo $javascript->link('admin/assistencias/index.js', false);
?>
<div class="index">
    <h2><?php __('Seja um revendedor'); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td>Nome do responsável</td>
            <td><?php e($data['SejaUmRevendedor']['nome_do_responsavel']); ?></td>
        </tr>
        <tr>
            <td>CPF</td>
            <td><?php e($data['SejaUmRevendedor']['cpf']); ?></td>
        </tr>
        <tr>
            <td>RG</td>
            <td><?php e($data['SejaUmRevendedor']['rg']); ?></td>
        </tr>
        <tr>
            <td>Data de nascimento</td>
            <td><?php e($data['SejaUmRevendedor']['data_de_nascimento']); ?></td>
        </tr>
        <tr>
            <td>E-mail</td>
            <td><?php e($data['SejaUmRevendedor']['email']); ?></td>
        </tr>
        <tr>
            <td>Nome fantasia</td>
            <td><?php e($data['SejaUmRevendedor']['nome_fantasia']); ?></td>
        </tr>
        <tr>
            <td>Razão social</td>
            <td><?php e($data['SejaUmRevendedor']['razao_social']); ?></td>
        </tr>
        <tr>
            <td>CNPJ</td>
            <td><?php e($data['SejaUmRevendedor']['cnpj']); ?></td>
        </tr>
        <tr>
            <td>Inscrição estadual</td>
            <td><?php e($data['SejaUmRevendedor']['ie']); ?></td>
        </tr>
        <tr>
            <td>Endereço</td>
            <td><?php e($data['SejaUmRevendedor']['endereco']); ?></td>
        </tr>
        <tr>
            <td>Bairro</td>
            <td><?php e($data['SejaUmRevendedor']['bairro']); ?></td>
        </tr>
        <tr>
            <td>Cidade</td>
            <td><?php e($data['SejaUmRevendedor']['cidade']); ?></td>
        </tr>
        <tr>
            <td>Estado</td>
            <td><?php e($data['SejaUmRevendedor']['estado']); ?></td>
        </tr>
        <tr>
            <td>CEP</td>
            <td><?php e($data['SejaUmRevendedor']['cep']); ?></td>
        </tr>
        <tr>
            <td>Telefone</td>
            <td><?php e($data['SejaUmRevendedor']['telefone']); ?></td>
        </tr>
        <tr>
            <td>Celular</td>
            <td><?php e($data['SejaUmRevendedor']['celular']); ?></td>
        </tr>
        <tr>
            <td>Fax</td>
            <td><?php e($data['SejaUmRevendedor']['fax']); ?></td>
        </tr>
        <tr>
            <td>Site</td>
            <td><?php e($data['SejaUmRevendedor']['site']); ?></td>
        </tr>
        <tr>
            <td>Data</td>
            <td><?php e($data['SejaUmRevendedor']['created']); ?></td>
        </tr>
    </table>
</div>