<?php
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('admin/assistencias/index.js',false);
?>
<div class="index">
<?php echo $this->Form->create('Assistencia');?>
	<fieldset>
 		<legend><?php printf(__('Adicionar %s', true), __('Assistência', true)); ?></legend>
	<?php
		echo $this->Form->input('status', array('default'=>true,'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
		echo $this->Form->input('marca', array('class' => 'w312',  'options' => $this->Marcas->getListMarcas("all", true)));
		echo $this->Form->input('empresa', array('class'=>'w312'));
		echo $this->Form->input('endereco', array('class'=>'w312'));
		echo $this->Form->input('bairro', array('class'=>'w312'));
		echo $this->Form->input('cidade', array('class'=>'w312'));
		echo $this->Form->input('estado', array('class' => 'w312', 'options' => $this->Estados->estadosBrasileiros()));
		echo $this->Form->input('cep', array('class'=>'w312 mask-cep'));
		echo $this->Form->input('telefone', array('class'=>'w312 mask-telefone'));
		echo $this->Form->input('celular', array('class'=>'w312 mask-telefone'));
		echo $this->Form->input('email', array('class'=>'w312'));
		echo $this->Form->input('contato', array('class'=>'w312'));
		echo $this->Form->end(__('Inserir', true));
	?>
	</fieldset>
</div>