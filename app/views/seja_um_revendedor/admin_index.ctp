<?php echo $javascript->link('admin/sac/index.js', false); ?>
<div class="index ">
    <h2><?php __('Seja um revendedor'); ?></h2>
    <?php echo $form->create('SejaUmRevendedor', array('url' => '/admin/seja_um_revendedor/', 'class' => 'formBusca')); ?>
    <fieldset>
        <div class="left">
            <?php echo $form->input('Filter.filtro', array('div' => false, 'label' => 'Nome do responsável / Nome fantasia / CNPJ:', 'class' => 'produtosFiltro')); ?>
        </div>
        <div class="submit">
            <input name="submit" type="submit" class="button1" value="Busca" />
        </div>
        <div class="submit">
            <a href="<?php e($this->Html->Url('index/filter:clear')) ?>" title="Limpar Filtro" class="button" >Limpar Filtro</a>
        </div>
    </fieldset>
    <?php echo $form->end(); ?>

    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('nome_do_responsavel'); ?></th>
            <th><?php echo $this->Paginator->sort('cpf'); ?></th>           
            <th><?php echo $this->Paginator->sort('nome_fantasia'); ?></th>
            <th><?php echo $this->Paginator->sort('cnpj'); ?></th>
            <th><?php echo $this->Paginator->sort('cidade'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>

            <th class="actions"><?php __('Ações'); ?></th>
        </tr>
        <?php
        $i = 0;
        foreach ($seja_um_revendedor as $seja_um_revendedor):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
            <tr<?php echo $class; ?>>
                <td align="center"><?php echo $seja_um_revendedor['SejaUmRevendedor']['id']; ?>&nbsp;</td>
                <td><?php echo $seja_um_revendedor['SejaUmRevendedor']['nome_do_responsavel']; ?>&nbsp;</td>
                <td><?php echo $seja_um_revendedor['SejaUmRevendedor']['cpf']; ?>&nbsp;</td>                
                <td><?php echo $seja_um_revendedor['SejaUmRevendedor']['nome_fantasia']; ?>&nbsp;</td>
                <td><?php echo $seja_um_revendedor['SejaUmRevendedor']['cnpj']; ?>&nbsp;</td>
                <td><?php echo $seja_um_revendedor['SejaUmRevendedor']['cidade']; ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $seja_um_revendedor['SejaUmRevendedor']['created']); ?>&nbsp;</td>

                <td class="actions">
                    <?php echo $this->Html->link(__('Visualizar', true), array('action' => 'view', $seja_um_revendedor['SejaUmRevendedor']['id'])); ?>
                    <?php echo $this->Html->link(__('Apagar', true), array('action' => 'delete', $seja_um_revendedor['SejaUmRevendedor']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $seja_um_revendedor['SejaUmRevendedor']['id'])); ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, mostrando %current% registros de %count%, começando no registro %start% e terminando no %end%', true)
        ));
        ?>	</p>

    <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('anterior', true), array(), null, array('class' => 'disabled')); ?>
        | 	<?php echo $this->Paginator->numbers(); ?>
        |
        <?php echo $this->Paginator->next(__('próxima', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
    </div>

</div>
