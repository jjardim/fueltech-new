<?php
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
?>
<div class="index">
<?php echo $this->Form->create('NoticiaTipo');?>
	<fieldset>
 		<legend><?php printf(__('Adicionar %s', true), __('Tipo	de Notícia', true)); ?></legend>
	
		<div class="left clear">
			<?php echo $this->Form->input('status', array('default'=>true,'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo'))); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('nome', array('class'=>'w312')); ?>
		</div>
		<?php echo $this->Form->end(__('Inserir', true)); ?>
	</fieldset>
</div>