<?php 
echo $this->Html->script(array(
	'common/jquery.meio_mask.js',
	'admin/frete_gratis/crud.js'          
));

?>
<div class="index">
<?php echo $this->Form->create('FreteGratis',array('url'=>'/admin/frete_gratis/edit/'.$this->params['pass'][0]));?>
	<fieldset>
 		<legend>Editar Regra</legend>
	<?php
		 
        echo $this->Form->input('id');
        echo $this->Form->input('status', array('default'=>true,'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
        echo $this->Form->input('codigo', array('label'=>'Tipo','options' => array('CATEGORIA' => 'Categoria', 'PRODUTO' => 'Produto','CARRINHO' => 'Carrinho' )));
		echo $this->Form->input('nome',array('label'=>'Descrição')); 
        echo $this->Form->input('label',array('label'=>'Label')); 
		echo $this->Form->input('cep_inicial',array('label'=>'Cep Inicial','class'=>'mask-cep'));
        echo $this->Form->input('cep_final',array('label'=>'Cep Final','class'=>'mask-cep'));
		echo $this->Form->input('apartir_de',array('label'=>'Frete válido apartir de R$','class'=>'mask-moeda'));
        ?>
		<br/>
		<br/>
		<span class="box-produtos">
 	
		<?php
		echo $this->Form->input('Buscar',array('class'=>'w312','after'=>$this->Form->Button('OK',array('id'=>'buscar-produtos'))));
		echo $this->Form->input('produto_categoria_id',array('options'=>$produtos_categorias,'label'=>'Categorias'));
        $botoes = $this->Html->link('Adicionar','javascript:;',array('id'=>'add','class'=>'add-bt'));
        $botoes .= $this->Html->link('Remover','javascript:;',array('id'=>'rm','class'=>'rm-bt'));
        echo $this->Form->input('Selecionar',array('type' => 'select', 'multiple' => true,'after'=>$botoes,'label'=>'Selecionar Produtos'));
        echo $this->Form->input('produto_id',array('options'=>$produtos_produtos,'type' => 'select', 'multiple' => true,'label'=>'Produtos selecionados'));?>
		</span>
		
		<span class="box-categorias">
			<?php
			echo $this->Form->input('Buscar2',array('label'=>'Buscar','class'=>'w312','after'=>$this->Form->Button('OK',array('id'=>'buscar-categorias'))));
		
        $botoes = $this->Html->link('Adicionar','javascript:;',array('id'=>'add2','class'=>'add-bt'));
        $botoes .= $this->Html->link('Remover','javascript:;',array('id'=>'rm2','class'=>'rm-bt'));
        echo $this->Form->input('Selecionar2',array('type' => 'select', 'multiple' => true,'after'=>$botoes,'label'=>'Selecionar Categorias'));
        echo $this->Form->input('categoria_id',array('options'=>$categorias_categorias,'type' => 'select', 'multiple' => true,'label'=>'Categorias selecionadas'));?>
		</span>		
		<?php echo $this->Form->end(__('Alterar', true)); ?>
	</fieldset>
</div>
