<?php 
echo $javascript->link('common/jquery.meio_mask.js',false);
echo $javascript->link('admin/frete_gratis/crud.js',false);
?>
<div class="index">
    <h2 class="left"><?php __('Fretes Grátis'); ?></h2>
    <div class="btAddProduto">
    	<?php echo $this->Html->link(__('[+] Adicionar Regra de Frete', true), array('action' => 'add')); ?>
			
    	
    </div>       
    <?php echo $form->create('FreteGratis', array('class'=>'formBusca','url'=>'/admin/frete_gratis/index'));?>
		<fieldset>
			<div class="">
				<?php echo $form->input('Filter.codigo',array('label'=>'Tipo','div'=>false,'options'=>array('PRODUTO'=>'Produto','CATEGORIA'=>'Categoria','CARRINHO'=>'Carrinho'),'class'=>'produtosSel')); ?>
				<?php echo $form->input('Filter.cep_inicial',array('div'=>false,'label'=>'Cep Inicial:','class'=>'mask-cep produtosFiltro'));  ?>
				<?php echo $form->input('Filter.cep_final',array('div'=>false,'label'=>'Cep Final:','class'=>'mask-cep produtosFiltro'));  ?>
			</div>			
			<?php echo $form->end('Busca'); ?>
			<?php echo $this->Html->link(__('Limpar Filtro', true), array('action' => 'index','filter:clear'),array('class'=>'bt-clear')); ?>
		</fieldset>	
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('Nome','nome'); ?></th>        
            <th><?php echo $this->Paginator->sort('Label','label'); ?></th>        
            <th><?php echo $this->Paginator->sort('Tipo','codigo'); ?></th>
            <th><?php echo $this->Paginator->sort('cep_inicial'); ?></th>
            <th><?php echo $this->Paginator->sort('cep_final'); ?></th>
            <th><?php echo $this->Paginator->sort('apartir_de'); ?></th>            
            <th><?php echo $this->Paginator->sort('Status','status'); ?></th>        
            <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>

            <th class="actions"><?php __('Ações'); ?></th>
        </tr>
        <?php
        $i = 0;
        foreach ($fretes as $frete):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
            <tr<?php echo $class; ?>>
                <td align="center"><?php echo $frete['FreteGratis']['id']; ?>&nbsp;</td>
                <td align="center"><?php echo $frete['FreteGratis']['nome']; ?>&nbsp;</td>
                <td align="center"><?php echo $frete['FreteGratis']['label']; ?>&nbsp;</td>
                <td align="center"><?php 
				
				if($frete['FreteGratis']['codigo']=='CATEGORIA'){
					echo 'Categoria';
				}elseif($frete['FreteGratis']['codigo']=='PRODUTO'){
					echo 'Produto';
				}else{
					echo 'Carrinho';
				}
				?>&nbsp;</td>
                <td align="center"><?php echo $frete['FreteGratis']['cep_inicial']; ?>&nbsp;</td>
                <td align="center"><?php echo $frete['FreteGratis']['cep_final']; ?>&nbsp;</td>
                <td><?php echo ($frete['FreteGratis']['codigo']=='PRODUTO')?'---':$this->String->bcoToMoeda($frete['FreteGratis']['apartir_de']); ?>&nbsp;</td>                
                <td align="center"><?php echo $frete['FreteGratis']['status']?'Ativo':'Inativo'; ?>&nbsp;</td>      
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $frete['FreteGratis']['modified']); ?>&nbsp;</td>

                <td class="actions">
                    <?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $frete['FreteGratis']['id'])); ?>
					<?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $frete['FreteGratis']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $frete['FreteGratis']['id'])); ?>
				</td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, mostrando %current% registros de %count%, começando no registro %start% e terminando no %end%', true)
        ));
        ?>	</p>

    <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('anterior', true), array(), null, array('class' => 'disabled')); ?>
	 | 	<?php echo $this->Paginator->numbers(); ?>
        |
        <?php echo $this->Paginator->next(__('próxima', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
    </div>
    
</div>
