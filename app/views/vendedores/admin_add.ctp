<?php
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('admin/vendedores/index.js',false);
?>
<div class="index">
<?php echo $this->Form->create('Vendedor');?>
	<fieldset>
 		<legend><?php printf(__('Adicionar %s', true), __('Vendedor', true)); ?></legend>
	<?php
		echo $this->Form->input('status', array('default'=>true,'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
		echo $this->Form->input('nome', array('class'=>'w312'));
		echo $this->Form->input('email', array('class'=>'w312'));
		echo $this->Form->input('telefone', array('class'=>'w312 mask-telefone'));
		echo $this->Form->input('cidade', array('class'=>'w312'));
		echo $this->Form->input('estado', array('class' => 'w312', 'options' => $this->Estados->estadosBrasileiros()));
		echo $this->Form->end(__('Inserir', true));
	?>
	</fieldset>
</div>