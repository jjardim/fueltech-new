<div class="index ">
    <h2><?php __('Vendedores'); ?></h2>
	<div class="btAddProduto">
    	<?php echo $this->Html->link(__('[+] Adicionar Vendedor', true), array('action' => 'add')); ?>
    </div>    
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('nome'); ?></th>
            <th><?php echo $this->Paginator->sort('email'); ?></th>    
			<th><?php echo $this->Paginator->sort('telefone'); ?></th> 			
			<th><?php echo $this->Paginator->sort('estado'); ?></th>
			<th><?php echo $this->Paginator->sort('cidade'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>

            <th class="actions"><?php __('Ações'); ?></th>
        </tr>
        <?php
        $i = 0;
        foreach ($vendedores as $vendedor):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
            <tr<?php echo $class; ?>>
                <td align="center"><?php echo $vendedor['Vendedor']['id']; ?>&nbsp;</td>
				<td><?php echo $vendedor['Vendedor']['nome']; ?>&nbsp;</td>
                <td><?php echo $vendedor['Vendedor']['email']; ?>&nbsp;</td>
				<td><?php echo $vendedor['Vendedor']['telefone']; ?>&nbsp;</td>                
				<td><?php echo $vendedor['Vendedor']['estado']; ?>&nbsp;</td>
				<td><?php echo $vendedor['Vendedor']['cidade']; ?>&nbsp;</td>
				<td><?php echo $vendedor['Vendedor']['status']; ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $vendedor['Vendedor']['created']); ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $vendedor['Vendedor']['modified']); ?>&nbsp;</td>

                <td class="actions">
                    <?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $vendedor['Vendedor']['id'])); ?>
					<?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $vendedor['Vendedor']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $vendedor['Vendedor']['id'])); ?>
				</td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, mostrando %current% registros de %count%, começando no registro %start% e terminando no %end%', true)
        ));
        ?>	</p>

    <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('anterior', true), array(), null, array('class' => 'disabled')); ?>
	 | 	<?php echo $this->Paginator->numbers(); ?>
        |
        <?php echo $this->Paginator->next(__('próxima', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
    </div>
    
</div>
