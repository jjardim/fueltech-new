<?php if(isset($pagina_element_content)): ?>
	<?php
	foreach($pagina_element_content['vendedor'] as $vendedor):
	?>
	
	<!-- start common -->
	<div class="common">
		<span class="green"><?php e($vendedor['Vendedor']['nome']); ?></span> 
		<span class="green"><?php e($vendedor['Vendedor']['telefone']); ?></span>
		<!-- start gray -->
		<div class="gray">
			<a title="Entre em contato" class="btn-vendedor-entrar-contato" rel="<?php echo $pagina_element_content['url_form']; ?>" id="<?php echo Inflector::slug("Contato Vendedor: ".$vendedor['Vendedor']['nome'], '-') ?>" href="javascript:;">Entre em contato</a>
			<!-- start form5 -->
			<div class="form5" style="display: none;">
				<a href="javascript:void(0);" class="btn-fechar-modal">[X]</a>
				<span class="green second"><?php e($vendedor['Vendedor']['nome']); ?></span> 
				<span class="green second"><?php e($vendedor['Vendedor']['telefone']); ?></span>
				<?php echo $this->Html->image('/img/site/zoomloader.gif', array('class' => 'loading', 'style' => 'display:none', 'alt' => 'Carregando', 'title' => 'Carregando')); ?>
				<div class="formulario-content"></div>
			</div>
			<!-- end form5 -->
		</div>
		<!-- end gray -->
	</div>
	<!-- end common -->

	<?php
	endForeach;
	?>
<?php endIf; ?>