<div class="index ">
    <h2><?php __('Linguagem'); ?></h2>
	<div class="btAddProduto">
    	<?php echo $this->Html->link(__('[+] Adicionar Linguagem', true), array('action' => 'add')); ?>
    </div>    
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('nome'); ?></th>
            <th><?php echo $this->Paginator->sort('codigo'); ?></th> 
			<th><?php echo $this->Paginator->sort('status'); ?></th> 
            <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>

            <th class="actions"><?php __('Ações'); ?></th>
        </tr>
        <?php
        $i = 0;
        foreach ($linguagens as $linguagem):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
            <tr <?php echo $class; ?>>
                <td align="center"><?php echo $linguagem['Linguagem']['id']; ?>&nbsp;</td>
				<td align="center"><?php echo $linguagem['Linguagem']['nome']; ?>&nbsp;</td>
                <td align="center"><?php echo $linguagem['Linguagem']['codigo']; ?>&nbsp;</td>                
				<td align="center"><?php echo ($linguagem['Linguagem']['status']) ? "Ativo" : "Inativo"; ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $linguagem['Linguagem']['created']); ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $linguagem['Linguagem']['modified']); ?>&nbsp;</td>

                <td class="actions">
                    <?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $linguagem['Linguagem']['id'])); ?>
					<?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $linguagem['Linguagem']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $linguagem['Linguagem']['id'])); ?>
				</td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, mostrando %current% registros de %count%, começando no registro %start% e terminando no %end%', true)
        ));
        ?>	</p>

    <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('anterior', true), array(), null, array('class' => 'disabled')); ?>
	 | 	<?php echo $this->Paginator->numbers(); ?>
        |
        <?php echo $this->Paginator->next(__('próxima', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
    </div>
    
</div>
