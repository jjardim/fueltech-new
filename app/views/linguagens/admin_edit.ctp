<?php
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('admin/linguagens/index.js',false);
?>
<div class="index">
    <?php echo $this->Form->create('Linguagem',array('type' => 'file', 'action'=>'edit/'.$this->params['pass'][0])); ?>
    <fieldset>
        <legend><?php printf(__('Editar %s', true), __('Linguagem', true)); ?></legend>
        <?php
			echo $this->Form->input('status', array('type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
			echo $this->Form->input('nome', array('class'=>'w312'));
			echo $this->Form->input('codigo', array('class'=>'w312'));
			echo $this->Form->input('externo', array('type' => 'radio', 'options' => array(true => 'Sim', false => 'Não')));
			echo $this->Form->input('url',array('class'=>'w312'));
		?>
   
		<legend>Thumb</legend>
		<?php
			$img = ( isset($this->data['Linguagem']['thumb_filename']) ) ? $this->data['Linguagem']['thumb_dir'] . '/' . $this->data['Linguagem']['thumb_filename'] : "uploads/produto_imagem/thumb/sem_imagem.jpg";
			echo $this->Form->input('Linguagem.thumb_filename', array('type' => 'file'));
			echo "<br />";
			echo $image->resize($img, 40, 40);		
			echo $this->Form->input('Linguagem.thumb_dir', array('type' => 'hidden'));
			echo $this->Form->input('Linguagem.thumb_mimetype', array('type' => 'hidden'));
			echo $this->Form->input('Linguagem.thumb_filesize', array('type' => 'hidden'));
			
			/*if( isset($this->data['Linguagem']['thumb_filename']) ){
				echo $form->input('Linguagem.thumb_remove', array('type' => 'checkbox')); 
			}*/
			
			echo "<br />";
			echo $this->Form->end(__('Salvar', true));
		?>
		<div class="clear"></div>
	</fieldset>
</div>