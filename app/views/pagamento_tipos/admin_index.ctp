<div class="index">
	<h2><?php __('Pagamento Tipos');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('nome');?></th>
			<th><?php echo $this->Paginator->sort('status');?></th>
                <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
                <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>
			<th class="actions">Ações</th>
	</tr>
	<?php
	$i = 0;
	foreach ($pagamentoTipos as $pagamentoTipo):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $pagamentoTipo['PagamentoTipo']['id']; ?>&nbsp;</td>
		<td><?php echo $pagamentoTipo['PagamentoTipo']['nome']; ?>&nbsp;</td>
		<td><?php echo ($pagamentoTipo['PagamentoTipo']['status'])?'Ativo':'Inativo'; ?>&nbsp;</td>
		<td><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s',$pagamentoTipo['PagamentoTipo']['created']); ?>&nbsp;</td>
		<td><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s',$pagamentoTipo['PagamentoTipo']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $pagamentoTipo['PagamentoTipo']['id'])); ?>
			<?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $pagamentoTipo['PagamentoTipo']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $pagamentoTipo['PagamentoTipo']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('Próximo', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
