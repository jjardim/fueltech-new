<div class="index ">
    <h2><?php __('Tipos de XML'); ?></h2>
	<div class="btAddProduto">
    	<?php echo $this->Html->link(__('[+] Adicionar Tipo de XML', true), array('action' => 'add')); ?>
    </div>    
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('nome'); ?></th>
            <th><?php echo $this->Paginator->sort('status'); ?></th> 
            <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>

            <th class="actions"><?php __('Ações'); ?></th>
        </tr>
        <?php
        $i = 0;
        foreach ($xml_tipos as $xml_tipo):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
            <tr<?php echo $class; ?>>
                <td align="center"><?php echo $xml_tipo['XmlTipo']['id']; ?>&nbsp;</td>
				<td><?php echo $xml_tipo['XmlTipo']['nome']; ?>&nbsp;</td>
                <td align="center"><?php echo ( $xml_tipo['XmlTipo']['status'] ) ? "Ativo" : "Inativo"; ?>&nbsp;</td>                
			    <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $xml_tipo['XmlTipo']['created']); ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $xml_tipo['XmlTipo']['modified']); ?>&nbsp;</td>

                <td class="actions">
                    <?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $xml_tipo['XmlTipo']['id'])); ?>
					<?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $xml_tipo['XmlTipo']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $xml_tipo['XmlTipo']['id'])); ?>
				</td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, mostrando %current% registros de %count%, começando no registro %start% e terminando no %end%', true)
        ));
        ?>	</p>

    <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('anterior', true), array(), null, array('class' => 'disabled')); ?>
	 | 	<?php echo $this->Paginator->numbers(); ?>
        |
        <?php echo $this->Paginator->next(__('próxima', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
    </div>
    
</div>
