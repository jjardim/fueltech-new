<div class="index ">
    <h2><?php __('Assistência'); ?></h2>
	<div class="btAddProduto">
    	<?php echo $this->Html->link(__('[+] Adicionar Assistência', true), array('action' => 'add')); ?>
    </div>    
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('marca'); ?></th>
            <th><?php echo $this->Paginator->sort('empresa'); ?></th>           
			<th><?php echo $this->Paginator->sort('estado'); ?></th>
			<th><?php echo $this->Paginator->sort('cidade'); ?></th>
			<th><?php echo $this->Paginator->sort('contato'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>

            <th class="actions"><?php __('Ações'); ?></th>
        </tr>
        <?php
        $i = 0;
        foreach ($assistencias as $assistencia):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
            <tr<?php echo $class; ?>>
                <td align="center"><?php echo $assistencia['Assistencia']['id']; ?>&nbsp;</td>
				<td><?php echo $assistencia['Assistencia']['marca']; ?>&nbsp;</td>
                <td><?php echo $assistencia['Assistencia']['empresa']; ?>&nbsp;</td>                
				<td><?php echo $assistencia['Assistencia']['estado']; ?>&nbsp;</td>
				<td><?php echo $assistencia['Assistencia']['cidade']; ?>&nbsp;</td>
				<td><?php echo $assistencia['Assistencia']['contato']; ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $assistencia['Assistencia']['created']); ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $assistencia['Assistencia']['modified']); ?>&nbsp;</td>

                <td class="actions">
                    <?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $assistencia['Assistencia']['id'])); ?>
					<?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $assistencia['Assistencia']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $assistencia['Assistencia']['id'])); ?>
				</td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, mostrando %current% registros de %count%, começando no registro %start% e terminando no %end%', true)
        ));
        ?>	</p>

    <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('anterior', true), array(), null, array('class' => 'disabled')); ?>
	 | 	<?php echo $this->Paginator->numbers(); ?>
        |
        <?php echo $this->Paginator->next(__('próxima', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
    </div>
    
</div>
