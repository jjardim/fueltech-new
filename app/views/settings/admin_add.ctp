<div class="index">
<h2>Adicionar Settings</h2>
<?php echo $form->create('Setting');?>
	<fieldset>
        <div class="tabs">
            <div id="setting-basic">
                <?php
                    echo $form->input('key', array('label'=>'Chave','rel' => __("e.g., 'Site.title'", true)));
                    echo $form->input('value',array('label'=>'Valor'));
                ?>
            </div>

            <div id="setting-misc">
                <?php
                    echo $form->input('title',array('label'=>'Título'));
                    echo $form->input('description',array('label'=>'Descrição'));
                    echo $form->input('input_type', array('label'=>'Tipo Campo','rel' => __("e.g., 'text' or 'textarea'", true)));
                    echo $form->input('editable', array('label'=>'Editável','checked' => 'checked'));
                    echo $form->input('params',array('label'=>'Parâmetros'));
                ?>
            </div>

        </div>
	</fieldset>
<?php echo $form->end('Enviar');?>

</div>
