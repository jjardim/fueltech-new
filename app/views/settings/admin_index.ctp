<div class="index">
    <h2>Settings</h2>

    <table cellpadding="0" cellspacing="0">
        <?php
        $tableHeaders = $html->tableHeaders(array(
                    $paginator->sort('id'),
                    $paginator->sort('Chave', 'key'),
                    $paginator->sort('Valor', 'value'),
                    $paginator->sort('Editável', 'editable'),
                    __('Ações', true),
                ));
        echo $tableHeaders;

        $rows = array();
        foreach ($settings AS $setting) {
            $actions = $html->link(__('Mover para cima', true), array('controller' => 'settings', 'action' => 'moveup', $setting['Setting']['id']));
            $actions .= ' ' . $html->link(__('Mover para baixo', true), array('controller' => 'settings', 'action' => 'movedown', $setting['Setting']['id']));
            $actions .= ' ' . $html->link(__('Editar', true), array('controller' => 'settings', 'action' => 'edit', $setting['Setting']['id']));
            $actions .= ' ' . $html->link(__('Deletar', true), array(
                        'controller' => 'settings',
                        'action' => 'delete',
                        $setting['Setting']['id']
                            ), null, __('Deseja mesmo remover o registro # %s?', true));

            $key = $setting['Setting']['key'];
            $keyE = explode('.', $key);
            $keyPrefix = $keyE['0'];
            if (isset($keyE['1'])) {
                $keyTitle = '.' . $keyE['1'];
            } else {
                $keyTitle = '';
            }

            $rows[] = array(
                $setting['Setting']['id'],
                $html->link($keyPrefix, array('controller' => 'settings', 'action' => 'index', 'p' => $keyPrefix)) . $keyTitle,
                $setting['Setting']['value'],
                ($setting['Setting']['editable']?'Sim':'Não'),
                $actions,
            );
        }

        echo $html->tableCells($rows);
        
        ?>
    </table>

    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true)
        ));
        ?>	</p>

    <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array(), null, array('class' => 'disabled')); ?>
                	 | 	<?php echo $this->Paginator->numbers(); ?>
        |
        <?php echo $this->Paginator->next(__('Próximo', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
    </div>



</div>
