<?php
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('admin/variacoes/index.js',false);
?>
<div class="index">
<?php echo $this->Form->create('VariacaoProduto');?>
	<fieldset>
 		<legend><?php printf(__('Adicionar %s', true), __('Variacões', true)); ?></legend>
	<?php
		echo $this->Form->input('status', array('default'=>true,'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
		echo $this->Form->input('produto_id', array('options' => $produtos));
		echo $this->Form->input('variacao_id', array('label' => 'Variação','options' => $variacoes));
		echo $this->Form->input('valor', array('class'=>'w312'));
		echo $this->Form->input('agrupador', array('class'=>'w312'));
		echo $this->Form->input('ordem', array('class'=>'w312'));
		echo $this->Form->end(__('Inserir', true));
	?>
	</fieldset>
</div>