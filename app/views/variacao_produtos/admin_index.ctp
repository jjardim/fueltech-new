<div class="index ">
    <h2><?php __('Variacões de Produtos'); ?></h2>
	<div class="btAddProduto">
    	<?php echo $this->Html->link(__('[+] Adicionar Variação', true), array('action' => 'add')); ?>
    </div>
	<?php echo $form->create('VariacaoProduto', array('action'=>'/index','class'=>'formBusca'));?>
	<fieldset>
		<div class="left">
            <?php echo $form->input('Filter.produto_id',array('div'=>false,'label'=>'Produto:','class'=>'produtosFiltro','options' => $produtos));  ?>
		</div>	
		<div class="left">
            <?php echo $form->input('Filter.variacao_id',array('div'=>false,'label'=>'Variação:','class'=>'produtosFiltro','options' => $variacoes));  ?>
		</div>
		<div class="left clear">
            <?php echo $form->input('Filter.valor',array('div'=>false,'label'=>'Valor:','class'=>'produtosFiltro'));  ?>
		</div>			
		<div class="left">
            <?php echo $form->input('Filter.status',array('div'=>false,'label'=>'Status:','class'=>'produtosFiltro','options' => array(true => 'Ativo', false => 'Inativo')));  ?>
		</div>
		<div class="submit">
			<input name="submit" type="submit" class="button1" value="Busca" />
		</div>
		<div class="submit">
			<input name="submit" type="submit" class="button1" value="Exportar" />
		</div>
		<div class="submit">
			<a href="<?php e($this->Html->Url('index/filter:clear')) ?>" title="Limpar Filtro" class="button" >Limpar Filtro</a>
		</div>
	</fieldset>
	<?php echo $form->end(); ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('produto_id'); ?></th>
			<th><?php echo $this->Paginator->sort('variacao_id'); ?></th>
			<th><?php echo $this->Paginator->sort('valor'); ?></th>
			<th><?php echo $this->Paginator->sort('agrupador'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>

            <th class="actions"><?php __('Ações'); ?></th>
        </tr>
        <?php
        $i = 0;
        foreach ($variacao_produtos as $variacao_produto):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
            <tr<?php echo $class; ?>>
                <td align="center"><?php echo $variacao_produto['VariacaoProduto']['id']; ?>&nbsp;</td>
				<td><?php echo $variacao_produto['Produto2']['nome']; ?>&nbsp;</td>
				<td><?php echo $variacao_produto['Variacao']['nome']; ?>&nbsp;</td>
				<td><?php echo $variacao_produto['VariacaoProduto']['valor']; ?>&nbsp;</td>
				<td><?php echo $variacao_produto['VariacaoProduto']['agrupador']; ?>&nbsp;</td>
                <td align="center"><?php echo ( $variacao_produto['VariacaoProduto']['status'] ) ? 'Ativo' : 'Inativo'; ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $variacao_produto['VariacaoProduto']['created']); ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $variacao_produto['VariacaoProduto']['modified']); ?>&nbsp;</td>

                <td class="actions">
                    <?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $variacao_produto['VariacaoProduto']['id'])); ?>
					<?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $variacao_produto['VariacaoProduto']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $variacao_produto['VariacaoProduto']['id'])); ?>
				</td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, mostrando %current% registros de %count%, começando no registro %start% e terminando no %end%', true)
        ));
        ?>	</p>

    <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('anterior', true), array(), null, array('class' => 'disabled')); ?>
	 | 	<?php echo $this->Paginator->numbers(); ?>
        |
        <?php echo $this->Paginator->next(__('próxima', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
    </div>
    
</div>
