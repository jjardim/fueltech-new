<?php
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('admin/variacao_produtos/index.js',false);
?>
<div class="index">
    <?php echo $this->Form->create('VariacaoProduto'); ?>
    <fieldset>
        <legend><?php printf(__('Editar %s', true), __('Variação', true)); ?></legend>
        <?php
		echo $this->Form->input('status', array('type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
		echo $this->Form->input('produto_id', array('options' => $produtos));
		echo $this->Form->input('variacao_id', array('label' => 'Variação','options' => $variacoes));
		echo $this->Form->input('valor', array('class'=>'w312'));
		echo $this->Form->input('agrupador', array('class'=>'w312'));
		echo $this->Form->input('ordem', array('class'=>'w312'));
		echo $this->Form->end(__('Salvar', true));
        ?>
    </fieldset>
</div>