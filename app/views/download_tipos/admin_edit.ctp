<?php
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('admin/wallpapers/index.js',false);
?>
<div class="index">
    <?php echo $this->Form->create('DownloadTipo'); ?>
    <fieldset>
        <legend><?php printf(__('Editar %s', true), __('Tipo de Download', true)); ?></legend>
		<div class="left clear">
			<?php echo $this->Form->input('status', array('type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo'))); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('nome', array('class'=>'w312')); ?>
		</div>
	</fieldset>
		<?php
			echo $this->Form->end(__('Salvar', true));
        ?>
    </fieldset>
</div>