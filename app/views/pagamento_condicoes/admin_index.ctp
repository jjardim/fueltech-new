<div class="index">
	<h2><?php __('Pagamento Condições');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('pagamento_tipo_id');?></th>
			<th><?php echo $this->Paginator->sort('nome');?></th>
			<th><?php echo $this->Paginator->sort('sigla');?></th>
			<th><?php echo $this->Paginator->sort('parcelas');?></th>
			
			<th><?php echo $this->Paginator->sort('status');?></th>
			<th class="actions">Ações</th>
	</tr>
	<?php
	$i = 0;
	foreach ($pagamentoCondicoes as $pagamentoCondicao):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td align="center"><?php echo $pagamentoCondicao['PagamentoCondicao']['id']; ?>&nbsp;</td>
                <td>
			<?php echo $this->Html->link($pagamentoCondicao['PagamentoTipo']['nome'], array('controller' => 'pagamento_tipos', 'action' => 'edit', $pagamentoCondicao['PagamentoTipo']['id'])); ?>
		</td>
		<td><?php echo $pagamentoCondicao['PagamentoCondicao']['nome']; ?>&nbsp;</td>
		<td><?php echo $pagamentoCondicao['PagamentoCondicao']['sigla']; ?>&nbsp;</td>
		<td align="center"><?php echo $pagamentoCondicao['PagamentoCondicao']['parcelas']; ?>&nbsp;</td>
		<td align="center"><?php echo ($pagamentoCondicao['PagamentoCondicao']['status'])?'Ativo':'Inativo'; ?>&nbsp;</td>
		
		<td class="actions">
			<?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $pagamentoCondicao['PagamentoCondicao']['id'])); ?>
			<?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $pagamentoCondicao['PagamentoCondicao']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $pagamentoCondicao['PagamentoCondicao']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('Próximo', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
