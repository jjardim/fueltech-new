<?php
echo $javascript->link('common/jquery.meio_mask.js',false);
echo $javascript->link('admin/pagamento_condicoes/crud.js',false); ?>
<div class="index">
    <?php echo $this->Form->create('PagamentoCondicao',array('type' => 'file')); ?>
    <fieldset>
        <legend><?php __('Editar Pagamento Condição'); ?></legend>
        <?php
        echo $this->Form->input('id');
        echo $this->Form->input('status', array('type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
        echo $this->Form->input('pagamento_tipo_id');
        echo $this->Form->input('nome',array('class'=>'w312'));
        echo $this->Form->input('sigla',array('class'=>'w147'));
        echo $this->Form->input('parcelas', array('class' => 'mask-numerico w147', 'label' => 'Máximo Parcelas'));
        echo $this->Form->input('desconto_apartir', array('label' => 'Desconto Apartir (R$)', 'class' => 'mask-moeda w147'));
        echo $this->Form->input('desconto_de', array('label' => 'Desconto Avista em %', 'class' => 'mask-moeda w147'));
        echo $this->Form->input('juros_apartir', array('label' => 'Juros apartir de (parcela)', 'class' => 'mask-numerico w147'));
        echo $this->Form->input('juros_de', array('label' => 'Juros de %', 'class' => 'mask-moeda w147'));
        ?>
    </fieldset>
    <fieldset>
        <legend>Imagem</legend>
        <?php if(isset($this->data['PagamentoCondicaoImagem'][0]['dir'])){?>
            <div class="img">
                <?php
                echo $image->resize($this->data['PagamentoCondicaoImagem'][0]['dir'].DS.$this->data['PagamentoCondicaoImagem'][0]['filename'], 100, 100,false);
                //deletar imagem na alteração
                echo $this->Form->input('PagamentoCondicaoImagem.id', array('type' => 'hidden','value'=>$this->data['PagamentoCondicaoImagem'][0]['id']));
                ?>
            </div>
        <?php } ?>
        <?php 
            echo $this->Form->input('PagamentoCondicaoImagem.filename', array('type' => 'file'));
            echo $this->Form->input('PagamentoCondicaoImagem.pagamento_condicao_id', array('type' => 'hidden','value'=>$this->Form->value('PagamentoCondicao.id')));
            echo $this->Form->input('PagamentoCondicaoImagem.dir', array('type' => 'hidden'));
            echo $this->Form->input('PagamentoCondicaoImagem.mimetype', array('type' => 'hidden'));
            echo $this->Form->input('PagamentoCondicaoImagem.filesize', array('type' => 'hidden'));
            echo $this->Form->end(__('Salvar', true));
        ?>
    </fieldset>
    </div>
