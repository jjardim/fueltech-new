<?php
echo $javascript->link('common/jquery.meio_mask.js',false);
echo $javascript->link('admin/pagamento_condicoes/crud.js',false); ?>
<div class="index">
    <?php echo $this->Form->create('PagamentoCondicao',array('type' => 'file')); ?>
    <fieldset>
        <legend><?php __('Adicionar Pagamento Condição'); ?></legend>
        <?php
        echo $this->Form->input('status', array('default'=>true,'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
        echo $this->Form->input('pagamento_tipo_id');
        echo $this->Form->input('nome');
        echo $this->Form->input('sigla');
        echo $this->Form->input('parcelas', array('class' => 'mask-numerico', 'label' => 'Máximo Parcelas'));
        echo $this->Form->input('desconto_apartir', array('label' => 'Desconto Apartir (R$)', 'class' => 'mask-moeda'));
        echo $this->Form->input('desconto_de', array('label' => 'Desconto Avista em %', 'class' => 'mask-moeda'));
        echo $this->Form->input('juros_apartir', array('label' => 'Juros apartir de (parcela)', 'class' => 'mask-numerico'));
        echo $this->Form->input('juros_de', array('label' => 'Juros de %', 'class' => 'mask-moeda'));
        ?>
    </fieldset>
        <fieldset><legend>Imagem</legend>
        <?php
                echo $this->Form->input('PagamentoCondicaoImagem.filename', array('type' => 'file'));
                echo $this->Form->input('PagamentoCondicaoImagem.dir', array('type' => 'hidden'));
                echo $this->Form->input('PagamentoCondicaoImagem.mimetype', array('type' => 'hidden'));
                echo $this->Form->input('PagamentoCondicaoImagem.filesize', array('type' => 'hidden'));
        ?>
        </fieldset>
    <?php echo $this->Form->end(__('Enviar', true)); ?>
    </div>
