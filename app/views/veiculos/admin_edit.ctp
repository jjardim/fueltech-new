<?php
echo $javascript->link('common/swfobject.js', false);
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('common/jquery.uploadify.v2.1.4.min', false);
echo $javascript->link('admin/veiculos/index.js',false);
?>
<div class="index">
    <?php echo $this->Form->create('Veiculo'); ?>
    <fieldset>
        <legend><?php printf(__('Editar %s', true), __('Veiculo', true)); ?></legend>
		<div class="left clear">
			<?php echo $this->Form->input('status', array('type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo'))); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('nome', array('class'=>'w312')); ?>
		</div>
                <div class="left clear">
			<?php echo $this->Form->input('video_url', array('label' => 'URL do vídeo', 'class'=>'w312')); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('descricao', array('label' => 'Descrição','class'=>'h300 w700 mceEditor', 'type' => 'textarea')); ?>
		</div>
    </fieldset>
    <fieldset>
	<legend>Adicionar Imagens</legend>
	<input type="hidden" value="<?php echo $galeria_id ?>" name="galeria_id" id="GaleriaId" />
	<input id="edit-produto" name="file_upload" type="file" />
	<div class="container-edit">
	    <?php echo $this->Uploadify->build($imgs_old, "GaleriaFoto", true); ?>
	</div>
    </fieldset>
    
	<!--<?php if(isset($galeria_fotos) && count($galeria_fotos) > 0): ?>
		<table style="float: left; clear: both; width: auto; margin-left: 20px;">
			<tr>
			<?php foreach($galeria_fotos as $k => $gf): ?>
				<td align="center">
					<table>
						<tr>
							<td>
								<?php $img = ( isset($gf['GaleriaFoto']['filename']) ) ? $gf['GaleriaFoto']['dir'] . '/' . $gf['GaleriaFoto']['filename'] : "uploads/produto_imagem/thumb/sem_imagem.jpg"; ?>
								<?php echo $image->resize($img, 120, 120); ?>
							</td>
						</tr>
						<tr>
							<td>
								<?php echo $this->Form->input("GaleriaFoto.{$k}.delete", array('type' => 'checkbox')); ?>
								<?php echo $this->Form->input("GaleriaFoto.{$k}.id", array('type' => 'hidden', 'default' => $gf['GaleriaFoto']['id'])); ?>
							</td>
						</tr>
					</table>
				</td>
			<?php endForeach; ?>
			</tr>
		</table>
	<?php endIf; ?>-->
	<div class="clear"></div>
	<?php echo $this->Form->end(__('Salvar', true)); ?>
</div>