<?php echo $html->css('common/nyro_modal/nyroModal.css', null, array('inline' => false)); ?>
<?php echo $javascript->link('common/nyro_modal/jquery.nyroModal.custom', false); ?>
<?php echo $javascript->link('site/veiculos/index', false); ?>

<!-- start leftcol -->
<div id="leftcol">
	<?php echo $this->element('site/left'); ?>
</div>
<!-- end leftcol -->

<!-- start rightcol -->
<div id="rightcol">
	<!-- start showcase -->
	<div id="showcase">
		<div class="caption"><h2><?php echo __("Veículos com Fueltech"); ?></h2></div>
		<?php echo $this->Html->image('site/img_showcase2.jpg', array('alt' => __("Notícias", true), 'width' => '719', 'height' => '187'))?>
	</div>
	<!-- end showcase -->

	<!-- start content -->
	<div class="content product-box">
		<!-- start detail -->
		<div class="detail">
			
			<!-- start img product -->
			<!--<div class="img-product">
				<div class="detail2">
					<span>FT 200</span>
					<img src="images/img_product1.png" width="92" height="71" alt="product" />
					<span><b>ver produto</b></span> 
				</div>
			</div>-->
			<!-- end img product -->
			
			<?php if(isset($veiculo) && !empty($veiculo)): ?>
					<h3 class="heading"><?php echo $veiculo['Veiculo']['nome']; ?></h3>
					<div>
						<?php echo $veiculo['Veiculo']['descricao']; ?>
					</div>
			<?php endIf; ?>
			
		</div>
		<!-- end detail -->
		<!-- start thumbnail-->
		<div class="thumbnail">
			<?php 
				if(count($veiculo['Galeria']['GaleriaFoto']) > 0):
					foreach($veiculo['Galeria']['GaleriaFoto'] as $indi => $img): 
			?>
				<!-- start seleto -->
				<div class="seleto sel_foto<?php echo $indi; ?>" <?php if($indi>0){ echo ' style="display:none;" '; } ?> >
					<a class="zoom" href="<?php echo $this->Html->url("/uploads/galeria/filename/". $img['filename'])?>">
						<?php echo $image->resize(isset($img['filename']) ? $img['dir'] . '/' . $img['filename'] : 'uploads/produto_imagem/filename/sem_imagem.jpg', 432, 324, true, array('alt' => Inflector::slug($veiculo['Veiculo']['nome'], ' '),'class'=>'nyroModal')); ?>
					</a>
				</div>
				<!-- end seleto -->
			<?php 
					endForeach;
				else:
			?>
				<?php echo $image->resize('uploads/produto_imagem/filename/sem_imagem.jpg', 432, 324, true, array('alt' => Inflector::slug($veiculo['Veiculo']['nome'], ' '),'class'=>'nyroModal')); ?>
			<?php endIf; ?>	
			
			
			<?php if( count($veiculo['Galeria']['GaleriaFoto']) > 1 ): ?>
			<ul>
				<?php 
					foreach($veiculo['Galeria']['GaleriaFoto'] as $indi => $img): 
						if($img['status'] == 1):
				?>
					<li>
						<a href="<?php echo $this->Html->url("/uploads/galeria/filename/". $img['filename'])?>" class="clique_sel zoom2" title="thumb" data-rel="foto<?php echo $indi; ?>" rel="zoom2">
							<?php echo $image->resize(isset($img['filename']) ? $img['dir'] . '/' . $img['filename'] : 'uploads/produto_imagem/filename/sem_imagem.jpg', 130, 95, true, array('alt' => Inflector::slug($veiculo['Veiculo']['nome'], ' '))); ?>
						</a>
					</li>
				<?php
						endIf;
					endForeach; 
				?>
			</ul>
			<?php endIf; ?>
		</div>
		<!-- end thumbnail -->
		<div class="clear"></div>    
                
                <?php if (isset($veiculo['Veiculo']['video_url']) && !empty($veiculo['Veiculo']['video_url'])) : ?>
                <iframe width="520" height="415" src="http://www.youtube.com/embed/<?php echo $this->Youtube->get_code($veiculo['Veiculo']['video_url']); ?>" frameborder="0" allowfullscreen>
                </iframe>
                <?php endif ?>
	</div>
	<!-- end content -->
	<div class="clear"></div>
</div>
<!-- end rightcol -->