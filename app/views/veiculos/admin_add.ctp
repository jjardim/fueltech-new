<?php
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('admin/assistencias/index.js',false);
?>
<div class="index">
<?php echo $this->Form->create('Veiculo');?>
	<fieldset>
 		<legend><?php printf(__('Adicionar %s', true), __('Veiculo', true)); ?></legend>
		<div class="left clear">
			<?php echo $this->Form->input('status', array('default'=>true,'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo'))); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('nome', array('class'=>'w312')); ?>
		</div>
                <div class="left clear">
			<?php echo $this->Form->input('video_url', array('label' => 'URL do vídeo', 'class'=>'w312')); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('descricao', array('class'=>'w312')); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->end(__('Inserir', true)); ?>
		</div>
	</fieldset>
</div>