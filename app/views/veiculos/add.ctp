<!-- start content -->
<div class="content product-box">
	<!-- start form box -->
	<div class="form-box">
		<h3 class="heading">Cadastre seu veículo</h3>
		<form action="#">
			<div class="row">
			<label for="Veículo">Veículo:</label>
			<input name="field" type="text" value="" id="Veículo" class="input" />
			</div>
			<div class="row">
			<label for="Proprietario">Proprietário: </label>
			<input name="field" type="text" value="" id="Proprietario" class="input" />
			</div>
			<div class="row">
			<label for="Preparador">Preparador:</label>
			<input name="field" type="text" value="" id="Preparador" class="input" />
			</div>
			<div class="row">
			<label for="Cilindrada">Cilindrada/Nº de Válvulas:</label>
			<input name="field" type="text" value="" id="Cilindrada" class="input" />
			</div>
			<div class="row">
			<label for="estimada">Potência estimada ou aferida: </label>
			<input name="field" type="text" value="" id="estimada" class="input" />
			</div>
			<div class="row">
			<label for="Dinam">Dinamômetro utilizado: </label>
			<input name="field" type="text" value="" id="Dinam" class="input" />
			</div>
			<div class="row">
			<label for="Comando">Comando de válvulas: </label>
			<input name="field" type="text" value="" id="Comando" class="input" />
			</div>
			<div class="row">
			<label for="Injetores">Injetores: </label>
			<input name="field" type="text" value="" id="Injetores" class="input" />
			</div>
			<div class="row">
			<label for="Combustível">Combustível:</label>
			<input name="field" type="text" value="" id="Combustível" class="input" />
			</div>
			<div class="row">
			<label for="Coletor">Coletor de admissão: </label>
			<input name="field" type="text" value="" id="Coletor" class="input" />
			</div>
			<div class="row">
			<label for="escape">Coletor de escape: </label>
			<input name="field" type="text" value="" id="escape" class="input" />
			</div>
			<div class="row">
			<label for="Turbina">Turbina: </label>
			<input name="field" type="text" value="" id="Turbina" class="input" />
			</div>
			<div class="row">
			<label for="utilizada">Pressão utilizada: </label>
			<input name="field" type="text" value="" id="utilizada" class="input" />
			</div>
			<div class="row">
			<label for="Bobina">Bobina: </label>
			<input name="field" type="text" value="" id="Bobina" class="input" />
			</div>
		</form>
	</div>
	<!-- end form box -->
	<!-- start form box -->
	<div class="form-box2">
		<h3 class="heading">Fotos</h3>
		 <form action="#">
			<div class="row">
			<input name="field" type="text" value="" class="input" />
			<input name="submit" type="button" value="PESQUISAR" class="button8" />
			</div>
			<div class="row">
			<input name="submit" type="button" value="ADICIONAR" class="button9" />
			</div>
			<div class="row2">
				<input name="ENVIAR" type="submit" value="SALVAR SEU VEÍCULO" class="button10" />
			</div>
		 </form>
	</div>
	<!-- end form box -->
  <div class="clear"></div>    
</div>
<!-- end content -->
<div class="clear"></div>
</div>
<!-- end rightcol -->


<div class="index">
<?php echo $this->Form->create('Veiculo');?>
	<fieldset>
 		<legend><?php printf(__('Adicionar %s', true), __('Veiculo', true)); ?></legend>
	<?php
		echo $this->Form->input('status', array('default'=>true,'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
		echo $this->Form->input('nome', array('class'=>'w312'));
		echo $this->Form->input('descricao', array('class'=>'w312'));
		echo $this->Form->end(__('Inserir', true));
	?>
	</fieldset>
</div>