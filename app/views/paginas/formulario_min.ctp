<?php echo $this->Form->create('Sac', array('id' => 'SacMin' , 'class' => 'form3', 'url' => $this->Html->url('/'.$url_form,true))); ?>
	<ul class="contato">
		<li>
			<?php echo $this->Session->flash(); ?>
		</li>
		<li>
			<?php echo $this->Form->input('Sac.nome', array('class' => 'inputfield ', 'label' => false, 'div' => false, 'default' => 'Nome*')); ?>
			<div id="nome-msg-error" class="form-min-msg-error"></div>
		</li>
		<li>
			<?php echo $this->Form->input('Sac.email', array('class' => 'inputfield ', 'label' => false, 'div' => false, 'default' => 'E-mail*')); ?>
			<div id="email-msg-error" class="form-min-msg-error"></div>
		</li>
		<li>
			<?php echo $this->Form->input('Sac.sac_tipo_id', array('value'=> $tipo_form, 'type' => 'hidden', 'div' => false)); ?>
			<?php 
				if(isset($loja_id) && !empty($loja_id)){
					echo $this->Form->input('Sac.loja_id', array('value'=> $loja_id, 'type' => 'hidden', 'div' => false));
				}
				if(isset($vendedor_id) && !empty($vendedor_id)){
					echo $this->Form->input('Sac.vendedor_id', array('value'=> $vendedor_id, 'type' => 'hidden', 'div' => false));
				}
			?>			
			<?php echo $this->Form->input('Sac.mensagem', array('value'=>'', 'label' => false,'type' => 'textarea','class'=>'textarea ', 'div' => false, 'default' => 'Mensagem')); ?>
			<div id="mensagem-msg-error" class="form-min-msg-error"></div>
			<?php echo $this->Form->submit('Enviar', array( 'class' => 'button', 'value' => '')); ?>
		</li>
		<li>
			<span style="clear: none; float: left;">* Campos Obrigatórios</span>
		</li>
	</ul>
<?php echo $this->Form->end(); ?>