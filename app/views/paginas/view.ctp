<?php 
	echo $html->css('common/jqzoom/jquery.jqzoom', null, array('inline' => false));
	echo $html->css('common/nyro_modal/nyroModal.css', null, array('inline' => false));
	echo $javascript->link('common/jquery.meio_mask.js',false);
	echo $javascript->link('common/jquery.jqzoom-core-pack',false);
	echo $javascript->link('common/nyro_modal/jquery.nyroModal.custom', false);
	
	echo $javascript->link('site/paginas/index.js',false);
	
	if($pagina['Pagina']['dinamico'] == 1):
		$pagina_element = $this->element('paginas/'.$pagina['Pagina']['element'].'',  array('pagina_element_content' => $pagina_element_content)); 
		$pagina_content = $pagina['PaginaDescricao']['texto'];
		$pagina_content = str_replace( '{VAR_CONTENT}' , $pagina_element, $pagina_content );
	else:
		$pagina_content = $pagina['PaginaDescricao']['texto'];
	endIf;
?>

<!-- start rightcol -->
<div id="rightcol" style="margin-left: -5px">
	<!-- start showcase -->
	<div id="showcase">
		<div class="caption"><h2><?php echo $pagina_atual_nome; ?></h2></div>
		<?php 
			if($pagina_atual_tipo != "Suporte"):
				echo $this->Html->image('site/img_showcase2.jpg', array('alt' => 'showcase', 'width' => '719', 'height' => '187'));
			else:
				echo $this->Html->image('site/img_showcase4.jpg', array('alt' => 'showcase', 'width' => '719', 'height' => '187'));
			endIf;
		?>
	</div>
	<!-- end showcase -->
	<div class="pagina-estatica-content">
	<?php echo $pagina_content; ?>
	</div>
	</div>
	<!-- end content -->
</div>
<!-- end rightcol -->
<div class="clear"></div>

<?php // echo $this->Js->writeBuffer(); ?>