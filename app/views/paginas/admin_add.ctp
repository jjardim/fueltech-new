<?php
	echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
	echo $javascript->link('common/jquery-ui-1.8.16.custom.min',false);
	echo $this->Html->css('common/ui-lightness/jquery-ui-1.8.16.custom.css');
	echo $javascript->link('admin/paginas/crud.js',false);
?>
<div class="index">
<?php echo $this->Form->create('Pagina');?>
	<fieldset>
 		<legend><?php printf(__('Adidionar %s', true), __('Página', true)); ?></legend>
	<?php
		echo $this->Form->input("site_id", array('hiddenField'=>false,'type' => 'hidden','value' => 1));
        echo $this->Form->input('status', array('default'=>true,'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
		echo $this->Form->input('visivel_menu', array('default'=>true,'type' => 'radio', 'options' => array(true => 'Sim', false => 'Não')));
		echo $this->Form->input('categoria', array( 'options' => array(' ' => 'Selecione', 'Mundo Fueltech'=> 'Mundo FuelTech','Suporte'=> 'Suporte','Outros'=> 'Outros')));
		echo $this->Form->input('dinamico', array('default'=>false,'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
		echo $this->Form->input('element',array('class'=>'w312'));
	
	?>
		<br /><br />		
		<div class="left clear tab" id="detail-tabs" style="margin-top: 15px;">
			<ul>
				<?php foreach($idiomas as $key => $idioma): ?>
					<li>
						<a href="#tab-content-idioma-<?php echo $idioma['Linguagem']['id']; ?>">
							<?php $img = ( isset($idioma['Linguagem']['thumb_filename']) ) ? $idioma['Linguagem']['thumb_dir'] . '/' . $idioma['Linguagem']['thumb_filename'] : "uploads/produto_imagem/thumb/sem_imagem.jpg"; ?>
							<?php echo $image->resize($img, 20, 20); ?>
							<?php echo $idioma['Linguagem']['nome']; ?>
						</a>
					</li>
				<?php endForeach; ?>
			</ul>
			<br /><br />
			<?php foreach($idiomas as $key => $idioma): ?>
				<div id="tab-content-idioma-<?php echo $idioma['Linguagem']['id']; ?>">					
					
					<?php echo $this->Form->input("PaginaDescricao.".$key.".id"); ?>
					<?php echo $this->Form->input("PaginaDescricao.".$key.".language", array('hiddenField'=>false,'type' => 'hidden','value' => $idioma['Linguagem']['codigo'])); ?>
					<?php echo $this->Form->input("PaginaDescricao.".$key.".nome",array('class'=>'w312')); ?>
					<?php echo "<br /><br />"; ?>
					<?php echo $this->Form->input("PaginaDescricao.".$key.".texto",array('label'=>'Conteudo','class'=>'mceEditor wCEM h400 w700')); ?>
					<br /><br />
					<legend>SEO</legend>
					<div class="left">
						<?php echo $this->Form->input("PaginaDescricao.".$key.".seo_title",array('label'=>'Seo Title','class'=>'inputs')); ?>
					</div>
					<div class="left clear">
						<?php echo $this->Form->input("PaginaDescricao.".$key.".seo_institucional",array('label'=>'Seo Institucional','class'=>'inputs')); ?>
					</div>
					<div class="left clear">
						<?php echo $this->Form->input("PaginaDescricao.".$key.".seo_meta_description",array('label'=>'Seo Meta Description','class'=>'inputs')); ?>
					</div>
					<div class="left clear">
						<?php echo $this->Form->input("PaginaDescricao.".$key.".seo_meta_keywords",array('label'=>'Seo Meta Keywords','class'=>'inputs')); ?>
					</div>
					
				</div>
			<?php endForeach; ?>
		</div>
		<div class="clear"></div>
		<?php echo $this->Form->end(__('Salvar', true)); ?>
		<div class="clear"></div>
	</fieldset>
</div>