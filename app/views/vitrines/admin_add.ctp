<?php
    echo $this->Html->css('admin/vitrines/index.css',null,array('inline'=>false));
    echo $javascript->link('common/jquery-ui-1.8.16.custom.min',false);
    echo $javascript->link('admin/vitrines/crud.js',false);
    echo $javascript->link('common/asmselect/jquery.asmselect',false);
    echo $this->Html->css('common/asmselect/jquery.asmselect.css');
?>
<div class="index">
    <?php echo $this->Form->create('Vitrine'); ?>
    <fieldset>
        <legend><?php __('Adicionar Vitrine'); ?></legend>
        <?php
        echo $this->Form->input('status', array('default'=>true,'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
        echo $this->Form->input('nome',array('class'=>'w312'));
        echo "<br class='clear' />";
        echo $this->Form->input('capa', array('type' => 'radio', 'options' => array(true => 'Sim', false => 'Não')));
        echo $this->Form->input('tipo', array( 'options' => array('NORMAL'=> 'Normal','DESTAQUE'=> 'Destaque','LANCAMENTOS'=>'Lançamentos','PROMOCOES'=>'Promoções','CATEGORIA'=>'Categoria', 'CALCADO_MASC'=>'Calçado Masculino', 'CALCADO_FEM'=>'Calçado Feminino', 'CONFECCOES' => 'Confecções', 'ACESSORIOS' => 'Acessórios' )));
        echo $this->Form->input('categoria_id',array('options'=> $categorias,'label'=>'Categoria'));
        echo $this->Form->input('Buscar',array('class'=>'w312','after'=>$this->Form->Button('OK',array('id'=>'buscar-produtos'))));
        $botoes = $this->Html->link('Adicionar','javascript:;',array('id'=>'add'));
        $botoes .= $this->Html->link('Remover','javascript:;',array('id'=>'rm'));
        echo $this->Form->input('Selecionar',array('type' => 'select', 'multiple' => true,'after'=>$botoes));
        echo $this->Form->input('Produto',array('class'=>'categoria_produtos_sel'));
        echo $this->Form->input('quantidade_exibida',array('class'=>'w312'));
        echo $this->Form->end(__('Inserir', true));
        ?>
    </fieldset>
</div>  
