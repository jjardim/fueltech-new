<div class="index">
    <h2>Menus</h2>
	<div class="btAddProduto">
    	<?php echo $this->Html->link(__('Adicionar Menu', true), array('action' => 'add')); ?>
    </div>    
    <table cellpadding="0" cellspacing="0">
    <?php
        $tableHeaders =  $this->Html->tableHeaders(array(
            $paginator->sort('id'),
            $paginator->sort('Título','title'),
            $paginator->sort('alias'),
            __('Ações', true),
        ));
        echo $tableHeaders;

        $rows = array();
        foreach ($menus AS $menu) {
            $actions  = $this->Html->link(__('Visualizar links', true), array('controller' => 'links', 'action' => 'index', 'menu' => $menu['Menu']['id']));
            $actions .= ' ' . $this->Html->link(__('Editar', true), array('controller' => 'menus', 'action' => 'edit', $menu['Menu']['id']));
            $actions .= ' ' . $this->Html->link(__('Deletar', true), array(
                'controller' => 'menus',
                'action' => 'delete',
                $menu['Menu']['id'],
            ), null, __('Deseja mesmo remover o registro # %s?', true));

            $rows[] = array(
                $menu['Menu']['id'],
                $this->Html->link($menu['Menu']['title'], array('controller' => 'links', 'action' => 'index', 'menu' => $menu['Menu']['id'])),
                $menu['Menu']['alias'],
                $actions,
            );
        }

        echo $this->Html->tableCells($rows);
    ?>
    </table>
    	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('Próximo', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
