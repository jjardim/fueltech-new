<div class="index">
    <h2>Editar Menu</h2>
    <?php echo $form->create('Menu');?>
        <fieldset>
            <div class="tabs">
                <div id="menu-basic">
                    <?php
                       echo $this->Form->input('status',array('default'=>true,'type'=>'radio','options'=>array(true=>'Ativo',false=>'Inativo')));
                        echo $form->input('title',array('label'=>'Título'));
                        echo $form->input('alias');
                        echo $form->input('description',array('label'=>'Descrição'));
                    ?>
                </div>
                <div id="menu-misc">
                    <?php
                        echo $form->input('params',array('label'=>'Parâmetros'));
                    ?>
                </div>
            </div>
        </fieldset>
    <?php echo $form->end('Enviar');?>
</div>