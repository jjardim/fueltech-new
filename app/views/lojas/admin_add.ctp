<?php
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('admin/lojas/index.js',false);
?>
<div class="index">
<?php echo $this->Form->create('Loja',array('type' => 'file'));?>
	<fieldset>
 		<legend><?php printf(__('Adicionar %s', true), __('Loja', true)); ?></legend>
	<?php
		echo $this->Form->input('status', array('default'=>true,'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
		echo $this->Form->input('cidade', array('class'=>'w312'));
		echo $this->Form->input('estado', array('class' => 'w312', 'options' => $this->Estados->estadosBrasileiros()));
		echo $this->Form->input('ordem_estado', array('class'=>'w312'));
		echo $this->Form->input('coordenadas', array('class'=>'w312'));
		echo $this->Form->input('conteudo', array('class' => 'mceEditor wCEM h400', 'type' => 'textarea'));
		echo $this->Form->input('ordem', array('class'=>'w312'));
	?>
	<legend>Thumb</legend>
    <?php
        echo $this->Form->input('Loja.thumb_filename', array('type' => 'file'));
        echo $this->Form->input('Loja.thumb_dir', array('type' => 'hidden'));
        echo $this->Form->input('Loja.thumb_mimetype', array('type' => 'hidden'));
        echo $this->Form->input('Loja.thumb_filesize', array('type' => 'hidden'));
		echo $this->Form->end(__('Inserir', true));
	?>
	</fieldset>
</div>