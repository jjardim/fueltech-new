<div class="index">
	<h2><?php __('Banner Tipos');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('nome');?></th>
			<th class="actions">Ações</th>
	</tr>
	<?php
	$i = 0;
	foreach ($bannerTipos as $bannerTipo):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $bannerTipo['BannerTipo']['id']; ?>&nbsp;</td>
		<td><?php echo $bannerTipo['BannerTipo']['nome']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $bannerTipo['BannerTipo']['id'])); ?>
			<?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $bannerTipo['BannerTipo']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $bannerTipo['BannerTipo']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('Próximo', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
