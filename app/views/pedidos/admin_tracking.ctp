<?php 
echo $javascript->link('common/nyro_modal/jquery.nyroModal.custom.js', false);
echo $this->Html->css('common/nyro_modal/nyroModal.css', null, array('inline' => false));
echo $javascript->link('admin/pedido_tracking/crud.js', false);
echo $this->Html->css('admin/style');
?>

<div class="index">
<?php echo $this->Form->create('PedidoHistoricoTracking',array('url' => array('admin'=>true,'controller' => 'pedidos', 'action' => 'tracking', $this->params['pass'][0])));?>
	<fieldset>
		<legend><?php __('Adicionar Tracking'); ?></legend>
            <?php 
			echo $this->Form->input('tracking',array('type'=>'textarea','class'=>'w312'));
			echo $this->Form->input('pedido_id',array('type'=>'hidden','value'=>$this->params['pass'][0]));?>
	</fieldset>
        <?php echo $this->Form->end(__('Enviar', true));?>
</div>