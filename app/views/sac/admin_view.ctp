<?php
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('admin/assistencias/index.js',false);
?>
<div class="index">
	<h2><?php __('SAC'); ?>: <?php e($sac['SacTipo']['nome']); ?></h2>
    <table cellpadding="0" cellspacing="0">
		<tr>
			<td>Nome</td>
			<td><?php e($sac['Sac']['nome']); ?></td>
		</tr>
		<tr>
			<td>E-mail</td>
			<td><?php e($sac['Sac']['email']); ?></td>
		</tr>
		<tr>
			<td>Telefone</td>
			<td><?php e($sac['Sac']['telefone']); ?></td>
		</tr>

		<?php 
			// se o tipo de sac for fale conosco
			if($sac['SacTipo']['id'] == 8):
		?>
		<tr>
			<td>Cidade</td>
			<td><?php e($sac['Sac']['cidade']); ?></td>
		</tr>
		<tr>
			<td>Estado</td>
			<td><?php e($sac['Sac']['estado']); ?></td>
		</tr>
		<tr>
			<td>Mensagem</td>
			<td><?php e($sac['Sac']['mensagem']); ?></td>
		</tr>
		<tr>
			<td>Formulario</td>
			<td><?php e($sac['SacTipo']['nome']); ?></td>
		</tr>
		<?php 
			endif;
			// se o tipo de sac for trabalhe conosco
			if($sac['SacTipo']['id'] == 11):
		?>
		<tr>
			<td>CPF</td>
			<td><?php e($sac['Sac']['cpf']); ?></td>
		</tr>
		<tr>
			<td>Anexo</td>
			<td><a target="_blank" href="<?php echo $this->Html->Url('/app/webroot/', true); echo $sac['Sac']['dir'] . '/'; echo $sac['Sac']['filename']; ?>"><?php echo $sac['Sac']['filename']; ?></a></td>
		</tr>
		<?php endif; ?>

		<tr>
			<td>Data</td>
			<td><?php e($sac['Sac']['created']); ?></td>
		</tr>
		

		<?php if($sac['Sac']['vendedor_id'] != null): ?>
		<tr>
			<td>Vendedor</td>
			<td><?php e($sac['Vendedor']['nome']); ?></td>
		</tr>
		<?php endIf; ?>
		<?php if($sac['Sac']['loja_id'] != null): ?>
		<tr>
			<td>Loja</td>
			<td><?php e($sac['Loja']['cidade']); ?></td>
		</tr>
		<?php endIf; ?>
    </table>
</div>