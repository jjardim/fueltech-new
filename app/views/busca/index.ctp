<?php echo $javascript->link('site/busca/index', false); ?>
<?php echo $javascript->link('site/categorias/customSelect.jquery.min',false); ?>

<!-- start leftcol -->
<div id="leftcol">
	<?php echo $this->element('site/left'); ?>
</div>
<!-- end leftcol -->

<!-- start rightcol -->
<div id="rightcol" class="inner">
	<!-- start column2nd -->
	<div class="column2nd inner">
		<!-- start productcol2 -->
		<div class="productcol2">
			
			<?php if(isset($categoria_data)): ?>
			<h2 class="title inner"><?php echo $this->String->formata_estilo_titulo($breadcrumb, 1); ?></h2>
			<?php endIf; ?>
			
			<!-- start gray-row -->
			<div class="gray-row">
				<!-- start gray-row-in -->
				<div class="gray-row-in first">
					<strong><?php 
								echo $this->Paginator->counter('%count%');  
								echo "&nbsp;";
								echo __("itens encontrado(s)");
							?>
					</strong>
					<!-- start search2 -->
					<div class="search2">
						<form action="#">
							<div style="float: left; position: relative; width: 160px;">
								<?php 
									$ordenacao = array(
										'sort:Produto.preco/direction:asc'      			 => __("Menor preço", true),
										'sort:Produto.preco/direction:desc'     			 => __("Maior preço", true),
										'sort:Produto.nome/direction:asc'     				 => __("Ordenar A-Z", true),
										'sort:Produto.nome/direction:desc'     				 => __("Ordenar Z-A", true),
										'sort:Produto.quantidade_acessos/direction:desc'     => __("Mais acessados", true),
										'sort:Produto.quantidade_vendidos/direction:desc'    => __("Mais vendidos", true),
									);
									echo $this->Form->input('order',
																array(	
																	'label'		=> false, 
																	'div' 		=> false,
																	'class'		=> 'select ordenacao',
																	'id'		=> 'ordenacao',
																	'rel'		=> $this->Html->url(),
																	'value' 	=> (isset($this->params['named']['sort'])&&isset($this->params['named']['direction']))?'sort:'.$this->params['named']['sort'].'/direction:'.$this->params['named']['direction']:false,
																	'options'	=>	array(''=>__("Selecione", true))+$ordenacao)
															); 
								?>
							</div>
						</form>
					</div>	
					<!-- end search2 -->
				</div>
				<!-- end gray-row-in -->
			</div>
			<!-- end gray-row -->
			
			<!-- start category -->
			<div class="category">
				
				<!-- start product-list -->
				<div class="product-list">
					<ul>
					<?php			
						foreach ($produtos as $key => $produto):
							$img = ( isset($produto[0]['imagem']) && file_exists($produto[0]['imagem']) ) ? $produto[0]['imagem'] : "uploads/produto_imagem/filename/sem_imagem.jpg";
						?>
						<li>
							<h4>
								<a href="<?php e($this->Html->Url("/" . low(Inflector::slug($produto['ProdutoDescricao']['nome'], '-')) . '-' . 'prod-' . $produto['Produto']['id'])) ?>.html" title="<?php echo $produto['ProdutoDescricao']['nome'] ?>">
									<?php echo $produto['ProdutoDescricao']['nome'] ?>
								</a>
							</h4>							
							<div class="imgblock">
								<a href="<?php e($this->Html->Url("/" . low(Inflector::slug($produto['ProdutoDescricao']['nome'], '-')) . '-' . 'prod-' . $produto['Produto']['id'])) ?>.html" title="<?php echo $produto['ProdutoDescricao']['nome'] ?>">
									<?php echo $image->resize($img, 152, 113); ?>
								</a>
							</div>		
							<?php
								if ($produto['Produto']['quantidade_disponivel'] > 0 && $produto['Produto']['status'] != 2):
									$preco = ($produto['Produto']['preco_promocao'] > 0) ? $produto['Produto']['preco_promocao'] : $produto['Produto']['preco'];
									$parcela_valida = $this->Parcelamento->parcelaValida($preco, $parcelamento); 
							?>
							<a href="<?php e($this->Html->Url("/" . low(Inflector::slug($produto['ProdutoDescricao']['nome'], '-')) . '-' . 'prod-' . $produto['Produto']['id'])) ?>.html" title="<?php e($produto['ProdutoDescricao']['nome']) ?>">
								<?php if ($this->String->bcoToMoeda($produto['Produto']['preco_promocao']) > 0 && $produto['Produto']['preco_promocao']<$produto['Produto']['preco']): ?>								
									<span> <?php echo __("De"); ?>: <strong>R$ <?php e($this->String->bcoToMoeda($produto['Produto']['preco'])); ?></strong></span>
									<p class="orange"><strong><small><?php echo __("Por"); ?>: R$ <?php echo $this->String->bcoToMoeda($produto['Produto']['preco_promocao']) ?></small> à vista</strong></p>
								<?php else: ?>
									<p class="orange"><strong><small><?php echo __("Por"); ?>: R$ <?php echo $this->String->bcoToMoeda($produto['Produto']['preco']) ?></small> à vista</strong></p>
								<?php endif; ?>	

								<?php if( $parcela_valida['parcela'] > 1 ):?>
									<span class="red"><?php echo __("ou"); ?> <?php e($parcela_valida['parcela']) ?>x de R$ <?php echo $this->String->bcoToMoeda($parcela_valida['valor'])?> <?php if( $parcela_valida['juros']==true ){ echo __('com'); } else { echo __('sem'); } ?> <?php echo __("juros"); ?></span>
								<?php endif;?>						
							</a>
							<?php if( isset($produto[0]['frete_gratis'])&&$produto[0]['frete_gratis']!=""): ?>
								<a href="javascript:void(0);" title="<?php echo $produto[0]['frete_gratis']; ?>" class="button freteGratis"><?php echo $produto[0]['frete_gratis']; ?></a>
							<?php endIf; ?>
							<?php else: ?>
								<p class="esgotado"><?php echo __("Consulte Disponibilidade"); ?></p>
							<?php 
								endif; 
							?>			
						</li>
					
						<?php //quebra da lista a cada 4 registros ?>
						<?php if( ($key+1) % 4== 0 ): ?>
							</ul>
						<div class="clear"></div>
					</div>
					<div class="product-list last">
						<ul>
						<?php endif; ?>			
					<?php
						endforeach;
					?>
					</ul>
					<div class="clear"></div>
				</div>
				<!-- end product-list -->
			</div>
			<!-- end category -->
		</div>
		<!-- end productcol -->
		<div class="gray-row">
			<div class="gray-row-in">
				<!-- start pagination -->
				<div class="pagination">
					<ul>
						<?php echo $this->Paginator->numbers(array( 'tag' => 'li', 'separator' => '<li>.</li>' )); ?>
					</ul>
				</div>
				<!-- end pagination -->
			</div>
		</div>
	</div>
	<!-- end column2nd -->
	<div class="clear"></div>
</div>
<!-- end rightcol -->
<div class="clear"></div>