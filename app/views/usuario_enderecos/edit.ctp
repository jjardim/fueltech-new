<?php echo $javascript->link('common/jquery.meio_mask.js'); ?>
<?php echo $this->Javascript->link('site/usuarios/crud.js'); ?>

<div style="width: 960px; display: block; overflow: hidden;">
	<?php echo $this->Session->flash(); ?>
</div>

<!-- start leftcol -->
<div id="leftcol">
	<?php echo $this->element('site/left'); ?>
</div>
<!-- end leftcol -->

<!-- start rightcol -->
<div id="rightcol" class="cadastro">	
	<h2 class="page-title"><small>Dados de endereço</small></h2>
	
	<div class="clear"></div><br /><br />
	
	<!-- begin box_enderecos -->
	<div id="box_enderecos" style="margin-left: 15px; padding-bottom: 15px">
		<!-- begin col2 -->
		<div class="col2">
			<!-- start column3 -->
			<div class="column3">
				<!-- start column1-box -->
				<div class="column1-box">
					<ul class="list1">
						<?php if ($enderecos):
							foreach ($enderecos as $valor):
						?>
							<li><a href="javascript:void(0);" title="<?php e($valor['nome']) ?>" rel="<?php e($valor['id']) ?>"><?php e($valor['nome']) ?></a></li>
						<?php
							endforeach;
						endif;
						?>
					</ul>
					<a href="<?php e($this->Html->Url("/usuario_enderecos/add")) ?>" title="CADASTRAR NOVO ENDEREÇO" class="button1" id="btn-cadastrar-novo-endereco">CADASTRAR NOVO ENDEREÇO</a>
					<div class="clear"></div>
				</div>
				<!-- end column1-box -->
			</div>
			<!-- end column3 -->
		</div>
		<!-- col2 -->
		<!-- start col3 -->
		<div class="col3" style="width: 365px;;">
			<?php if ($enderecos):
					foreach ($enderecos as $valor):
			?>
					<!-- start box-dados-endereco -->
					<div class="box-dados-endereco" id="endereco-<?php e($valor['id']); ?>" >
						<!-- start column4 -->
						<div class="column4" style="width: 345px;">
							<h3 style=" line-height: normal; margin-top: -5px; padding-bottom: 5px; padding-left: 5px;"><?php e($valor['nome']); ?></h3>
							<!-- start column1-box -->
							<div class="column1-box" style="width: 330px;">
								<address>
									<strong><?php e($valor['rua']); ?>, <?php e($valor['numero']); ?></strong>
									<br />
									<?php e($valor['bairro']); ?>
									<br />
									<?php e($valor['cidade']); ?> 
									<?php e($valor['uf']); ?> - <?php e($valor['cep']); ?>
									<br />
									Endereço de cobrança ( <?php echo ($valor['cobranca']?'Sim':'Não'); ?> )  <?php echo $this->Html->link(__('Editar', true), array('plugin' => false, 'controller' => 'usuario_enderecos', 'action' => 'edit', $valor['id']), array('rel'=>$valor['id'],'class' => 'editar')); ?>
								</address>
								<span><?php echo $this->Html->link(__('Editar endereço', true), array('plugin' => false, 'controller' => 'usuario_enderecos', 'action' => 'edit', $valor['id']), array('rel'=>$valor['id'],'class' => 'editar')); ?> | <?php echo $this->Html->link(__('Excluir endereço', true), array('plugin' => false, 'controller' => 'usuario_enderecos', 'action' => 'delete', $valor['id']), array('title' => 'Excluir endereço'), sprintf(__('Deseja mesmo remover o endereço # %s?', true), $valor['nome'])); ?></span>
							</div>
							<!-- end column1-box -->
						</div>
						<!-- end column4 -->
					</div>
					<!-- end box-dados-endereco -->
				<?php
					endforeach;
				endif;
			?>
		</div>
		<!-- start col3 -->
		<div class="clear"></div>
	</div>
	<!-- end box_enderecos -->
	
	<!-- start column1st -->
	<div class="column1st fillform2" id="form-endereco-content">
		
		<?php echo $this->Form->create('UsuarioEndereco',array('url'=>'/usuarios/edit/endereco')); ?>		
		
			<!-- start content -->
			<div class="content">
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->hidden('UsuarioEndereco.id',array('id'=>'UsuarioEnderecoId')); ?>
					<?php echo $this->Form->hidden('UsuarioEndereco.cobranca',array('value'=>true)); ?>
					<?php echo $this->Form->input('UsuarioEndereco.cep', array('id'=>'UsuarioEnderecoCep','class' => 'input1 input6 mask-cep', 'label' => 'Primeiro digite o CEP *','after'=>$this->Html->image('/img/site/zoomloader.gif', array('id'=>'loading','style'=>'visibility:hidden; margin-top: 3px; margin-left: 3px;','alt' => 'Carregando', 'title' => 'Carregando')), 'div' => false)); ?>
					<span class="url-correios" style="clear: both; float: left; margin-left: auto; margin-top: 5px; width: auto;">Não sabe seu CEP? <a href="http://www.buscacep.correios.com.br/" target="_blank">Clique aqui.</a></span>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('UsuarioEndereco.nome', array('class' => 'input1 input7', 'label' => 'Identifique o Endereço *', 'div' => false)); ?>
					<span style="margin-left: 5px; font-size: 11px; line-height: 26px;">( Ex: casa, empresa, etc. )</span>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('UsuarioEndereco.rua', array('id'=>'UsuarioEnderecoRua','class' => 'input1 input10', 'label' => 'Endereço *', 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('UsuarioEndereco.numero', array('class' => 'input1 input8', 'label' => 'Número *', 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('UsuarioEndereco.complemento', array('class' => 'input1 input7', 'label' => 'Complemento', 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('UsuarioEndereco.bairro', array('id'=>'UsuarioEnderecoBairro','class' => 'input1 input9', 'label' => 'Bairro *', 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('UsuarioEndereco.uf', array('id'=>'UsuarioEnderecoUf','label'=>'Estado','class' => 'select3', 'options' => $this->Estados->estadosBrasileiros(), 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('UsuarioEndereco.cidade', array('id'=>'UsuarioEnderecoCidade','class' => 'input1 input9', 'label' => 'Cidade', 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row" style="padding-top: 30px;">
					<input name="submit" type="submit" class="button" value="SALVAR CADASTRO" />
					<p style="margin-top: 11px; float: left; clear: both;">* Preenchimento obrigatório</p>
					<div class="clear"></div>
				</div>
				<!-- end row -->
			
			</div>
			<!-- end content -->
			
		<?php echo $this->Form->end(); ?>
	
	</div>
	<!-- end column1st -->

</div>
<!-- end rightcol -->