<?php echo $javascript->link('common/jquery.meio_mask.js'); ?>
<?php echo $this->Javascript->link('site/usuarios/crud.js'); ?>

<div style="width: 960px; display: block; overflow: hidden;">
	<?php echo $this->Session->flash(); ?>
</div>

<!-- start leftcol -->
<div id="leftcol">
	<?php echo $this->element('site/left'); ?>
</div>
<!-- end leftcol -->

<!-- start rightcol -->
<div id="rightcol" class="cadastro">	
	<h2 class="page-title"><small>Cadastrar endereço</small></h2>
	
	<!-- start column1st -->
	<div class="column1st fillform2" id="form-endereco-content">
		
		<?php echo $this->Form->create('UsuarioEndereco'); ?>
		
			<!-- start content -->
			<div class="content">
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->hidden('UsuarioEndereco.cobranca',array('value'=>true)); ?>
					<?php echo $this->Form->input('UsuarioEndereco.cep', array('id'=>'UsuarioEnderecoCep','class' => 'input1 input6 mask-cep', 'label' => 'Primeiro digite o CEP *','after'=>$this->Html->image('/img/site/zoomloader.gif', array('id'=>'loading','style'=>'visibility:hidden; margin-top: 3px; margin-left: 3px;','alt' => 'Carregando', 'title' => 'Carregando')), 'div' => false)); ?>
					<span class="url-correios" style="clear: both; float: left; margin-left: auto; margin-top: 5px; width: auto;">Não sabe seu CEP? <a href="http://www.buscacep.correios.com.br/" target="_blank">Clique aqui.</a></span>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('UsuarioEndereco.nome', array('class' => 'input1 input7', 'label' => 'Identifique o Endereço *', 'div' => false)); ?>
					<span style="margin-left: 5px; font-size: 11px; line-height: 26px;">( Ex: casa, empresa, etc. )</span>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('UsuarioEndereco.rua', array('id'=>'UsuarioEnderecoRua','class' => 'input1 input10', 'label' => 'Endereço *', 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('UsuarioEndereco.numero', array('class' => 'input1 input8', 'label' => 'Número *', 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('UsuarioEndereco.complemento', array('class' => 'input1 input7', 'label' => 'Complemento', 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('UsuarioEndereco.bairro', array('id'=>'UsuarioEnderecoBairro','class' => 'input1 input9', 'label' => 'Bairro *', 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('UsuarioEndereco.uf', array('id'=>'UsuarioEnderecoUf','label'=>'Estado','class' => 'select3', 'options' => $this->Estados->estadosBrasileiros(), 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row">
					<?php echo $this->Form->input('UsuarioEndereco.cidade', array('id'=>'UsuarioEnderecoCidade','class' => 'input1 input9', 'label' => 'Cidade', 'div' => false)); ?>
					<div class="clear"></div>
				</div>
				<!-- end row -->
				
				<!-- start row -->
				<div class="row" style="padding-top: 30px;">
					<input name="submit" type="submit" class="button" value="SALVAR CADASTRO" />
					<p style="margin-top: 11px; float: left; clear: both;">* Preenchimento obrigatório</p>
					<div class="clear"></div>
				</div>
				<!-- end row -->
			
			</div>
			<!-- end content -->
			
		<?php echo $this->Form->end(); ?>
	
	</div>
	<!-- end column1st -->
	
</div>
<!-- end rightcol -->