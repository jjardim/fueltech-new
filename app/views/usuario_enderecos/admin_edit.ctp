<?php echo $javascript->link('common/jquery.meio_mask.js',false); ?>
<?php echo $javascript->link('admin/usuario_enderecos/crud.js',false); ?>
<div class="index">
<?php echo $this->Form->create('UsuarioEndereco',array('action'=>'add/user_id:' . $this->params['named']['user_id']));?>
	<fieldset>
		<legend><?php __('Editar Usuario Endereco'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('status',array('type'=>'radio','options'=>array(true=>'Ativo',false=>'Inativo')));
	?>
	<div class="left">
	<?php echo $this->Form->input('nome',array('div'=>false,'label'=>'Nome','class'=>'w312 inputs')); ?>
	</div>
	<div class="left clear">
	<?php echo $this->Form->input('cep',array('div'=>false,'class'=>'mask-cep inputs')); ?>
	</div>
	<div class="left">
	<?php echo $this->Form->input('rua',array('div'=>false,'label'=>'Rua','class'=>'w312 inputs')); ?>
	</div>
	<div class="left">
	<?php echo $this->Form->input('numero',array('div'=>false,'label'=>'Número','class'=>'inputs')); ?>
	</div>
	<div class="left clear">
	<?php echo $this->Form->input('complemento',array('div'=>false,'label'=>'Complemento','class'=>'inputs')); ?>
	</div>
	<div class="left">
	<?php echo $this->Form->input('bairro',array('div'=>false,'label'=>'Bairro','class'=>'inputs')); ?>
	</div>
	<div class="left clear">
	<?php echo $this->Form->input('cidade',array('div'=>false,'label'=>'Cidade','class'=>'inputs')); ?>
	</div>
	<div class="left">
	<?php echo $this->Form->input('uf',array('div'=>false,'options' => $this->Estados->estadosBrasileiros(),'class'=>'produtosSel')); ?>
	</div>
	<div class="left clear">
	<?php echo $this->Form->input('principal',array('options'=>array(true=>'Sim',false=>'Não'))); ?>
	</div>
	<br class="clear" />
	<?php echo $this->Form->end(__('Salvar', true));?>
	</fieldset>
</div>
