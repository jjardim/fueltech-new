<?php
echo $javascript->link('common/swfobject.js', false);
echo $javascript->link('common/jquery.uploadify.v2.1.4.min' ,false);
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('admin/galerias/index.js',false);
?>
<div class="index">
    <?php echo $this->Form->create('Galeria'); ?>
    <fieldset>
        <legend><?php printf(__('Editar %s', true), __('Galeria', true)); ?></legend>
        <div class="left clear">
			<?php echo $this->Form->input('id'); ?>
			<?php echo $this->Form->input('status', array('type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo'))); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('galeria_tipo_id',array('type'=>'select','options'=>$galeria_tipos)); ?> 
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('nome', array('class'=>'w312')); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('descricao', array('class'=>'w312','type' => 'textArea', 'label' => 'Descrição')); ?>
		</div>
	</fieldset>
	<fieldset>
		<legend>Adicionar Imagens</legend>
		<input id="edit-produto" name="file_upload" type="file" />
		<div class="container-edit">
			<?php echo $this->Uploadify->build($imgs_old, "GaleriaFoto", true); ?>
		</div>
	</fieldset>
	<?php echo $this->Form->end(__('Salvar', true)); ?>
</div>