<?php
echo $javascript->link('common/swfobject.js', false);
echo $javascript->link('common/jquery.uploadify.v2.1.4.min' ,false);
echo $javascript->link('common/tiny_mce/tiny_mce.js',false);
echo $javascript->link('common/jquery.cookie.js',false);
echo $javascript->link('common/jquery-ui-1.8.16.custom.min',false);
echo $this->Html->css('common/ui-lightness/jquery-ui-1.8.16.custom.css');
echo $javascript->link('admin/galerias/index.js',false);
?>
<div class="index">
<?php echo $this->Form->create('Galeria', array('type' => 'file'));?>
	<fieldset>
 		<legend><?php printf(__('Adicionar %s', true), __('Galeria', true)); ?></legend>
		<div class="left clear">
			<?php echo $this->Form->input('status', array('default'=>true,'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo'))); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('galeria_tipo_id',array('type'=>'select','options'=> $galeria_tipos)); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('nome', array('class'=>'w312')); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('descricao', array('class'=>'w312','type' => 'textArea', 'label' => 'Descrição')); ?>
		</div>
	</fieldset>
	<fieldset>
		<legend>Adicionar Imagens</legend>
		<input id="add-produto" name="file_upload" type="file" />
		<div class="container-add">
			<div class="container-imgs">
				<?php echo $imgs ?>
			</div>
		</div>
	</fieldset>
<?php echo $this->Form->end(__('Inserir', true)); ?>
</div>