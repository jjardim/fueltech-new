<?php echo $javascript->link('admin/banners/crud.js',false); ?>
<div class="index">
 <?php echo $this->Form->create('Banner', array('url'=>'/admin/banners/edit/'.$this->params['pass'][0],'type' => 'file'));?>
    <fieldset>
        <legend><?php __('Editar Banner'); ?></legend>
        <?php
        echo $this->Form->input('id');
        echo $this->Form->input('status',array('default'=>true,'type'=>'radio','options'=>array(true=>'Ativo',false=>'Inativo')));
        echo $this->Form->input('link',array('class'=>'w312 inputs'));
		echo $this->Form->input('ordem',array('class'=>'w312 inputs'));
		echo "<br class='clear' />";
        echo $this->Form->input('banner_tipo_id',array('class'=>'produtosSel'));
        echo $this->Form->input('capa',array('class'=>'banner-tipo','default'=>true,'type'=>'radio','options'=>array(true=>'Sim',false=>'Não')));
		echo $this->Form->input('Categoria',array('class'=>'h400','label'=>'Categorias'));
        ?>
    </fieldset>
    <fieldset>
        <legend>Imagem</legend>
        <?php if(isset($this->data['Banner']['dir'])){?>
            <div class="img">
                <?php echo $image->resize($this->data['Banner']['dir'].DS.$this->data['Banner']['filename'], 300, 100,true); ?>
            </div>
        <?php } ?>
        <?php
            echo $this->Form->input('filename', array('label'=>'Arquivo','type' => 'file'));
            echo $this->Form->input('dir', array('type' => 'hidden'));
            echo $this->Form->input('mimetype', array('type' => 'hidden'));
            echo $this->Form->input('filesize', array('type' => 'hidden'));
            //echo $this->Form->input('ordem',array('class'=>'inputs w147'));
            echo $this->Form->end(__('Salvar', true));
        ?>
    </fieldset>
</div>
