<div class="index">
    <h2 class="left"><?php __('Newsletters'); ?></h2>
    <div class="btAddProduto">
    	<?php echo $this->Html->link(__('Exportar Newsletters', true), array('controller' => 'newsletters','action' => 'export')); ?>
    </div>
	<?php echo $form->create('newsletters', array('url'=>'/admin/newsletters/index','class'=>'formBusca'));?>
	<fieldset>
		<div class="left">
			<?php echo $form->input('Filter.nome',array('div'=>false,'label'=>'Nome:','class'=>'produtosFiltro'));  ?>
		</div>
		<div class="left">
			<?php echo $form->input('Filter.email',array('div'=>false,'label'=>'E-mail:','class'=>'produtosFiltro'));  ?>
		</div>
		<div class="submit">
			<input name="submit" type="submit" class="button1" value="Busca" />
		</div>
		<div class="submit">
			<input name="submit" type="submit" class="button1" value="Exportar" />
		</div>
		<div class="submit">
			<a href="<?php e($this->Html->Url('index/filter:clear')) ?>" title="Limpar Filtro" class="button" >Limpar Filtro</a>
		</div>
	</fieldset>
	<?php echo $form->end(); ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('nome'); ?></th>
            <th><?php echo $this->Paginator->sort('email'); ?></th>
            <th><?php echo $this->Paginator->sort('status'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>
            <th class="actions">Ações</th>
        </tr>
       <?php
        $i = 0;
        foreach ($newsletters as $newsltter):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
        ?>
            <tr<?php echo $class; ?>>
                <td align="center"><?php echo $newsltter['Newsletter']['id']; ?>&nbsp;</td>
                <td><?php echo $newsltter['Newsletter']['nome']; ?>&nbsp;</td>
                <td><?php echo $newsltter['Newsletter']['email']; ?>&nbsp;</td>
                <td align="center"><?php echo ($newsltter['Newsletter']['status'])?'Ativo':'Inativo'; ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $newsltter['Newsletter']['created']); ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $newsltter['Newsletter']['modified']); ?>&nbsp;</td>
                <td class="actions">
                <?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $newsltter['Newsletter']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $newsltter['Newsletter']['id'])); ?>
            </td>
        </tr>
        <?php endforeach; ?>
            </table>
            <p>
        <?php
                echo $this->Paginator->counter(array(
                    'format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true)
                ));
        ?>	</p>

            <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array(), null, array('class' => 'disabled')); ?>
                	 | 	<?php echo $this->Paginator->numbers(); ?>
                |
        <?php echo $this->Paginator->next(__('Próximo', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
            </div>
        </div>
