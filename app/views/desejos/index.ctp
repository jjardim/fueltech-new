

<!-- start rightcol -->
<div id="rightcol_hp">
    <!-- start minha_conta_lista_desejos -->
    <div class="meus_desejos">
     <h4>Lista de Desejos</h4>	
        <!-- start row -->
        <div class="row">
            <span class="td1"><?php echo $this->Paginator->sort('nome'); ?></span>
            <span class="td2"><?php echo $this->Paginator->sort('status'); ?></span>
            <span class="td3"><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></span>
            <span class="td3"><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></span>
            <span class="td3">Ações</span>
            <div class="clear"></div>
        </div>
        <!-- end row -->
        <?php
        if( count($desejos) > 0 ){
            $i = 0;
            foreach ($desejos as $desejo):
                $class = null;
                if ($i++ % 2 == 0) {
                    $class = 'change';
                }
        ?>
                <!-- start row -->
                <div class="row2 <?php echo ($i % 2 == 0 ) ? 'change' : '';?>">
                    <span class="td1"><?php echo $desejo['Desejo']['nome']; ?></span>
                    <span class="td2"><?php echo ($desejo['Desejo']['status']) ? 'Ativo' : 'Inativo'; ?></span>
                    <span class="td3"><?php echo $this->Calendario->dataFormatada('d/m/Y H:i:s', $desejo['Desejo']['created']); ?></span>
                    <span class="td3"><?php echo $this->Calendario->dataFormatada('d/m/Y H:i:s', $desejo['Desejo']['modified']); ?></span>
                    <span class="td5"><?php echo $this->Html->link(__('editar', true), array('action' => 'edit', $desejo['Desejo']['id'])); ?>
                    <?php echo $this->Html->link(__('excluir', true), array('action' => 'delete', $desejo['Desejo']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $desejo['Desejo']['id'])); ?>
                    <?php echo $this->Html->link(__('visualizar', true), array('action' => 'produtos', $desejo['Desejo']['id'])); ?></span>
                </div>
                <!-- end row -->
        <?php
            endforeach;
        }else{
        ?>
            <div class="row2 change">
				<span class="td4">
					Nenhum produto na sua lista
				</span>
			</div>
        <?php
        }
        ?>
        <div class="clear"></div>
         <div id="pagination">
        <ul>
            <?php 
            if( $this->Paginator->current() > 1 ){
            ?>
                <li><?php echo $this->Paginator->prev('<img src="' . $this->Html->Url("/img/site/") . 'pagination_left.gif" alt="left" />', array('escape' => false, 'title' => 'Anterior'), null, array('class' => 'disabled')); ?></li>
            <?php
            }
            
            echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '<li>-</li>'));
            
            if( $this->Paginator->current() < $this->Paginator->params['paging']['Desejo']['pageCount'] ){
            ?>
                <li><?php echo $this->Paginator->next('<img src="' . $this->Html->Url("/img/site/") . 'pagination_right.gif" alt="right" />', array('escape' => false, 'title' => 'Próximo'), null, array('class' => 'disabled')); ?></li>
            <?php
            }
            ?>
            <li><?php echo $this->Paginator->counter(array('format' => __('Página %page% de %pages% página(s), exibindo %current% no total de %count% produtos.', true))); ?></li>
        </ul>
    </div>
    </div>
    <!-- end minha_conta_lista_desejos -->
</div>
