
<div id="rightcol">
    <h1>Adicionar na Lista de Desejos</h1>
    <?php echo $this->Form->create('Desejo', array('action' => 'add/' . $this->params['pass'][0])); ?>
		<!-- start minha_conta_lista_desejos -->
		<div id="meu_desejo">
			<?php
				$img = ( isset($produto['ProdutoImagem'][0]['filename']) && file_exists($produto['ProdutoImagem'][0]['dir'] . '/' . $produto['ProdutoImagem'][0]['filename']) ) ? $produto['ProdutoImagem'][0]['dir'] . '/' . $produto['ProdutoImagem'][0]['filename'] : "uploads/produto_imagem/filename/sem_imagem.jpg";
			?>
			<!-- start common_product -->
			<div class="common_product">
				<span class="imagebox2">
					<a href="<?php e($this->Html->Url("/" . low(Inflector::slug($produto['Produto']['nome'], '-')) . '-' . 'prod-' . $produto['Produto']['id'])) ?>.html">
						<?php echo $image->resize($img, 129, 121); ?>
					</a>
				</span>
				<span class="price"><?php echo $produto['Produto']['nome'] ?></span>
				<span class="price">
					
					<?php
					$preco = ($produto['Produto']['preco_promocao'] > 0) ? $produto['Produto']['preco_promocao'] : $produto['Produto']['preco'];
					$parcela_valida = $this->Parcelamento->parcelaValida($preco, $parcelamento);
					if ($produto['Produto']['preco_promocao'] > 0) {
						?>
							<span class="blue">de: R$<?php echo $this->String->bcoToMoeda($produto['Produto']['preco']) ?> </span><br/>
						<?php
					}
					?>
					<b>por: R$<?php echo $this->String->bcoToMoeda($preco) ?></b>
					<?php if( $parcela_valida['parcela'] > 1 ):?>
						<br/><span>ou em <?php e($parcela_valida['parcela']) ?>x de R$<?php echo $this->String->bcoToMoeda($parcela_valida['valor'])?></span>
					<?php endIf;?>
					
				</span>
			</div>
			<!-- end common product -->
		</div>
		
		
		<fieldset class="container-listas">
			<legend>Selecione uma ou mais listas para adicionar o produto</legend>
			<ul class="listas">
				<?php if ($listas): ?>
					<?php foreach ($listas as $lista): ?>
						<li>
							<?php echo $this->Form->input("DesejoProduto.desejo_id.{$lista['Desejo']['id']}", array('div' => false, 'label' => $lista['Desejo']['nome'], 'type' => 'checkbox')); ?>
						</li>
					<?php endForeach; ?>
				<?php endIf; ?>
			</ul>
			<div class="add">
				<?php echo $this->Form->input("Desejo.nome", array('div'=>false,'label' => 'Adicionar em uma nova lista', 'class'=>'input input2')); ?>
				<?php echo $this->Form->input("Produto.id", array('value' => $produto['Produto']['id'], 'type' => 'hidden')); ?>
				<div class="clear"></div>
				<?php echo $this->Form->button('Enviar', array('class' => 'button')); ?>
			</div>
		</fieldset>
    <?php echo $this->Form->end(); ?>
</div>
<?php /**/ ?>