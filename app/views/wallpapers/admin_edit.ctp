<?php
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('admin/wallpapers/index.js',false);
?>
<div class="index">
    <?php echo $this->Form->create('Wallpaper', array('type' => 'file', 'url' => '/admin/wallpapers/edit/'.$this->params['pass'][0])); ?>
    <fieldset>
        <legend><?php printf(__('Editar %s', true), __('Wallpaper', true)); ?></legend>
        <?php
			echo $this->Form->input('status', array('type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
			echo $this->Form->input('nome', array('class'=>'w312'));
			echo $this->Form->input('ordem', array('class'=>'w312'));
		?>
		<fieldset>
		<legend>Variações</legend>
		
		<div class="container-variacoes-tmp">
			<table>
				<tr>
					<?php
						if (isset($this->data['WallpaperGrade']) && count($this->data['WallpaperGrade']) >= 1) {
							foreach ($this->data['WallpaperGrade'] as $id => $quantidade):
							?>
							<td>
								<table>
									<tr>
										<td align="center">
											<?php $img = ( isset($quantidade['filename']) ) ? $quantidade['dir'] . '/' . $quantidade['filename'] : "uploads/produto_imagem/thumb/sem_imagem.jpg"; ?>
											<?php echo $quantidade['WallpaperTamanho']['valor']; ?>
										</td>
									</tr>
									<tr>
										<td align="center">
											<?php echo $image->resize($img, 80, 80); ?>
										</td>
									</tr>
									<tr>
										<td align="center">
											<?php echo $this->Form->input("WallpaperGrade.{$id}.delete", array('type' => 'checkbox')); ?>
											<?php echo $this->Form->input("WallpaperGrade.{$id}.id", array('type' => 'hidden', 'default' => $quantidade['id'])); ?>
										</td>
									</tr>
								</table>
							</td>
							<?php endForeach; ?>
					<?php } ?>
				</tr>
			</table>					
		</div>
		
		<br /><br />
		
		<div class="container-variacoes container">
			<?php echo $this->Html->image('/img/site/zoomloader.gif', array('class' => 'loading', 'style' => 'display:none', 'alt' => 'Carregando', 'title' => 'Carregando')); ?>
			<?php
				if(isset($id) && $id != null){
					$id = $id+1;
				}else{
					$id = 0;
				}
				
				echo $this->Form->input("WallpaperGrade.{$id}.wallpaper_tamanho_id", array('hiddenField'=>false,'label' => 'Wallpaper Tamanho','type' => 'select', 'options' => $wallpaper_tamanhos, 'class' => 'variacoes_tipos'));
				echo $this->Form->input("WallpaperGrade.{$id}.filename", array('hiddenField'=>false,'label' => 'Wallpaper','class' => 'variacoes', 'type' => 'file'));	
				echo $this->Form->input("WallpaperGrade.{$id}.dir", array('type' => 'hidden'));
				echo $this->Form->input("WallpaperGrade.{$id}.mimetype", array('type' => 'hidden'));
				echo $this->Form->input("WallpaperGrade.{$id}.filesize", array('type' => 'hidden'));						
			?>
			<a href="#" class="add">add</a>
			<a href="#" class="rm" style="display:none">rm</a>
		</div>
		
	</fieldset>
		<?php
			echo $this->Form->end(__('Salvar', true));
        ?>
    </fieldset>
</div>