<?php
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('admin/wallpapers/index.js',false);
?>
<div class="index">
<?php echo $this->Form->create('Wallpaper', array('type' => 'file', 'url' => '/admin/wallpapers/add'));?>
	<fieldset>
 		<legend><?php printf(__('Adicionar %s', true), __('Wallpaper', true)); ?></legend>
	<?php
		echo $this->Form->input('status', array('default'=>true,'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
		echo $this->Form->input('nome', array('class'=>'w312'));
		echo $this->Form->input('ordem', array('class'=>'w312'));
	?>
	
	<fieldset>
		<legend>Variações</legend>
		<div class="container-variacoes container">
			<?php echo $this->Html->image('/img/site/zoomloader.gif', array('class' => 'loading', 'style' => 'display:none', 'alt' => 'Carregando', 'title' => 'Carregando')); ?>
			<?php
				echo $this->Form->input("WallpaperGrade.0.wallpaper_tamanho_id", array('hiddenField'=>false,'label' => 'Wallpaper Tamanho','type' => 'select', 'options' => $wallpaper_tamanhos, 'class' => 'variacoes_tipos'));
				$variacao_sel2 = (isset($variacoes_sel[0])) ? $variacoes_sel[0] : array();
				echo $this->Form->input('WallpaperGrade.0.filename', array('hiddenField'=>false,'label' => 'Wallpaper','class' => 'variacoes', 'type' => 'file'));	
				echo $this->Form->input('WallpaperGrade.0.dir', array('type' => 'hidden'));
				echo $this->Form->input('WallpaperGrade.0.mimetype', array('type' => 'hidden'));
				echo $this->Form->input('WallpaperGrade.0.filesize', array('type' => 'hidden'));						
			?>
			<a href="#" class="add">add</a>
			<a href="#" class="rm" style="display:none">rm</a>
		</div>
		<?php
			if(isset($this->data['WallpaperGrade'][0])){
				?>
				<div class="content-atributo-rm-0">
				<?php
					echo $this->Form->input("WallpaperGrade.0.delete", array('type' => 'checkbox'));
				?>
				</div>
				<?php
			}	
		?>
		<div class="container-variacoes-tmp">
			<?php
				if (isset($this->data['WallpaperGrade']) && count($this->data['WallpaperGrade']) > 1) {
					$first = true;
					foreach ($this->data['WallpaperGrade'] as $id => $quantidade):
						if ($first) {
							$first = false;
							continue;
						}
					?>
					<div class="container">
						<?php
							echo $this->Form->input("WallpaperGrade.{$id}.variacao_tipo_id", array('hiddenField'=>false,'type' => 'select', 'options' => $wallpaper_tamanhos, 'class' => 'variacoes_tipos', 'label' => 'Wallpaper Tamanho'));
							echo $this->Form->input("WallpaperGrade.{$id}.filename", array('hiddenField'=>false,'label' => 'Variação', 'class' => 'variacoes', 'type' => 'file'));
							echo $this->Form->input('WallpaperGrade.{$id}.dir', array('type' => 'hidden'));		
							echo $this->Form->input('WallpaperGrade.{$id}.mimetype', array('type' => 'hidden'));		
							echo $this->Form->input('WallpaperGrade.{$id}.filesize', array('type' => 'hidden'));		
							if(isset($quantidade["variacao_id"])){
								echo $this->Form->input("WallpaperGrade.{$id}.delete", array('type' => 'checkbox'));
							}else{
								echo $this->Html->link("rm",array(), array('class' => 'rm'));
							}
						?>
					</div>
					<?php endForeach; ?>
			<?php } ?>
		</div>
	</fieldset>
	
	<?php	
		echo $this->Form->end(__('Inserir', true));
	?>
	</fieldset>
</div>