<?php
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('admin/variacoes/index.js',false);
?>
<div class="index">
    <?php echo $this->Form->create('Variacao'); ?>
    <fieldset>
        <legend><?php printf(__('Editar %s', true), __('Variações', true)); ?></legend>
        <?php
			echo $this->Form->input('status', array('type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
			echo $this->Form->input('variacao_tipo_id',array('label'=>'Tipo de Variação','options' => $variacao_tipos));
		?>
		
		<ul style="list-style: none outside none; padding-left: 0px; margin-left: -10px;">
		<?php foreach($idiomas as $key => $idioma): ?>
			<li>
				<?php $img = ( isset($idioma['Linguagem']['thumb_filename']) ) ? $idioma['Linguagem']['thumb_dir'] . '/' . $idioma['Linguagem']['thumb_filename'] : "uploads/produto_imagem/thumb/sem_imagem.jpg"; ?>
				<div style="display: inline-block; float: left; clear: both;"><?php echo $image->resize($img, 20, 20); ?> - <?php echo $idioma['Linguagem']['nome']; ?></div>
				
				
			</li>
		<?php endForeach; ?>		
		</ul>
		<br /><br />
		<?php		
			echo $this->Form->input("Variacao.valor", array('class'=>'w312'));		
			echo $this->Form->input('ordem', array('class'=>'w312'));
			echo $this->Form->input('agrupador', array('class'=>'w312'));
		?>
		<legend>Thumb</legend>
		<?php
			echo $this->Form->input('Variacao.thumb', array('type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
			$img = ( isset($this->data['Variacao']['thumb_filename']) ) ? $this->data['Variacao']['thumb_dir'] . '/' . $this->data['Variacao']['thumb_filename'] : "uploads/produto_imagem/thumb/sem_imagem.jpg";
			echo $this->Form->input('Variacao.thumb_filename', array('type' => 'file'));
			echo $image->resize($img, 80, 80);		
			echo $this->Form->input('Variacao.thumb_dir', array('type' => 'hidden'));
			echo $this->Form->input('Variacao.thumb_mimetype', array('type' => 'hidden'));
			echo $this->Form->input('Variacao.thumb_filesize', array('type' => 'hidden'));
			
			if( isset($this->data['Variacao']['thumb_filename']) ){
				echo $form->input('Variacao.thumb_remove', array('type' => 'checkbox')); 
			}
		?>
		<?php
			echo $this->Form->end(__('Salvar', true));
        ?>
    </fieldset>
</div>