<div class="index">
    <h2>Adicionar Link</h2>
    <?php echo $form->create('Link', array('url' => array('controller' => 'links', 'action' => 'add', 'menu' => $menu)));?>
        <fieldset>
            <div class="tabs">
                <div id="link-basic">
                    <?php
                        echo $form->input('menu_id', array('selected' => $menu));
                        echo $form->input('parent_id', array(
                            'label' => __('Parent', true),
                            'options' => $parentLinks,
                            'empty' => true,
                        ));
                        echo $form->input('title');
                        echo $form->input('link');
                        echo $form->input('status',array('default'=>true,'type'=>'radio','options'=>array(true=>'Ativo',false=>'Inativo')));
                    ?>
                </div>

                <div id="link-access">
                    <?php
                        echo $form->input('Grupo.Grupo');
                    ?>
                </div>

                <div id="link-misc">
                    <?php
                        echo $form->input('description',array('label'=>'Descrição'));
                        echo $form->input('rel');
                        echo $form->input('target');
                        echo $form->input('params',array('label'=>'Parâmetros'));
                    ?>
                </div>

            </div>
        </fieldset>
    <?php echo $form->end('Enviar');?>
</div>