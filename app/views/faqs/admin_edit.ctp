<div class="index">
    <?php echo $this->Form->create('Faq'); ?>
    <fieldset>
        <legend><?php printf(__('Editar %s', true), __('Faq', true)); ?></legend>
        <div class="left clear">
			<?php echo $this->Form->input('status', array('default'=>true,'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo'))); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input("nome",array('class'=>'w312')); ?>
		</div>
		<div class="clear"></div>
		<div class="clear"></div>
    </fieldset>
	<?php echo $this->Form->end(__('Salvar', true)); ?>
</div>