<?php
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('admin/assistencias/index.js',false);
?>
<div class="index">
<?php echo $this->Form->create('GaleriaFoto');?>
	<fieldset>
 		<legend><?php printf(__('Adicionar %s', true), __('GaleriaFoto', true)); ?></legend>
	<?php
		echo $this->Form->input('titulo', array('class'=>'w312'));
		echo $this->Form->input('descricao', array('class'=>'w312','type' => 'textArea'));
		echo $this->Form->end(__('Inserir', true));
	?>
	</fieldset>
</div>