<div class="index ">
    <h2><?php __('Galeria'); ?></h2>
	<div class="btAddProduto">
    	<?php echo $this->Html->link(__('[+] Adicionar Galeria', true), array('action' => 'add')); ?>
    </div>    
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('galeria_id'); ?></th>
            <th><?php echo $this->Paginator->sort('titulo'); ?></th> 
			<th><?php echo $this->Paginator->sort('filename'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>

            <th class="actions"><?php __('Ações'); ?></th>
        </tr>
        <?php
        $i = 0;
        foreach ($galeria_fotos as $galeria_foto):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
            <tr<?php echo $class; ?>>
                <td align="center"><?php echo $galeria_foto['GaleriaFoto']['id']; ?>&nbsp;</td>
				<td><?php echo $galeria_foto['GaleriaFoto']['galeria_id']; ?>&nbsp;</td>
                <td><?php echo $galeria_foto['GaleriaFoto']['titulo']; ?>&nbsp;</td>
				<td><?php echo ($galeria_foto['GaleriaFoto']['filename']) ? 'Ativo' : 'Inativo'; ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $galeria_foto['GaleriaFoto']['created']); ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $galeria_foto['GaleriaFoto']['modified']); ?>&nbsp;</td>

                <td class="actions">
                    <?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $galeria_foto['GaleriaFoto']['id'])); ?>
					<?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $galeria_foto['GaleriaFoto']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $galeria_foto['GaleriaFoto']['id'])); ?>
				</td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, mostrando %current% registros de %count%, começando no registro %start% e terminando no %end%', true)
        ));
        ?>	</p>

    <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('anterior', true), array(), null, array('class' => 'disabled')); ?>
	 | 	<?php echo $this->Paginator->numbers(); ?>
        |
        <?php echo $this->Paginator->next(__('próxima', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
    </div>
    
</div>
