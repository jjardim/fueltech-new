<?php 
echo $javascript->link('common/jquery.meio_mask.js',false);
echo $javascript->link('admin/frete_tipos/crud.js',false);
?>
<div class="index">
<?php echo $this->Form->create('FreteTipo');?>
	<fieldset>
 		<legend><?php printf(__('Adidionar %s', true), __('Tipo de Frete', true)); ?></legend>
 		<div class="clear">
	<?php echo $this->Form->input('status', array('default'=>true,'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo'))); ?>
    	</div>
    	<div class="clear">
    <?php echo $this->Form->input('nome',array('class'=>'w312')); ?>
    	</div>
    	<div class="clear">
    <?php echo $this->Form->input('descricao',array('label'=>'Descrição','class'=>'w312')); ?>
    	</div>
    	<div class="clear">
    <?php echo $this->Form->input('altura_max',array('class'=>'mask-numerico w147','label'=>'Altura Máx em cm.','after'=>'Utilize 0 para sem limite')); ?>

    <?php echo $this->Form->input('largura_max',array('class'=>'mask-numerico w147','label'=>'Altura Máx em cm.','after'=>'Utilize 0 para sem limite')); ?>

    <?php echo $this->Form->input('profundidade_max',array('class'=>'mask-numerico w147','label'=>'Altura Máx em cm.','after'=>'Utilize 0 para sem limite')); ?>

    <?php echo $this->Form->input('cubagem_max',array('class'=>'mask-numerico w147','label'=>'Altura Máx em cm.','after'=>'Soma da A+L+P, Utilize 0 para sem limite')); ?>
    	</div>
    <?php echo $this->Form->end(__('Inserir', true)); ?>
	</fieldset>
</div>
