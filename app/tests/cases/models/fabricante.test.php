<?php
/* Fabricante Test cases generated on: 2011-04-07 16:35:42 : 1302204942*/
App::import('Model', 'Fabricante');

class FabricanteTestCase extends CakeTestCase {
	var $fixtures = array('app.fabricante', 'app.pedido_item', 'app.produto');

	function startTest() {
		$this->Fabricante =& ClassRegistry::init('Fabricante');
	}

	function endTest() {
		unset($this->Fabricante);
		ClassRegistry::flush();
	}

}
?>