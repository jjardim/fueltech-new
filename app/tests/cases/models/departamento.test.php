<?php
/* Departamento Test cases generated on: 2011-04-07 16:06:02 : 1302203162*/
App::import('Model', 'Departamento');

class DepartamentoTestCase extends CakeTestCase {
	var $fixtures = array('app.departamento', 'app.categoria', 'app.produto', 'app.subcategoria', 'app.vitrine_tipo');

	function startTest() {
		$this->Departamento =& ClassRegistry::init('Departamento');
	}

	function endTest() {
		unset($this->Departamento);
		ClassRegistry::flush();
	}

}
?>