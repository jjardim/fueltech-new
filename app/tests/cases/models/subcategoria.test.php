<?php
/* Subcategoria Test cases generated on: 2011-04-07 16:07:45 : 1302203265*/
App::import('Model', 'Subcategoria');

class SubcategoriaTestCase extends CakeTestCase {
	var $fixtures = array('app.subcategoria', 'app.categoria', 'app.departamento', 'app.produto', 'app.vitrine_tipo');

	function startTest() {
		$this->Subcategoria =& ClassRegistry::init('Subcategoria');
	}

	function endTest() {
		unset($this->Subcategoria);
		ClassRegistry::flush();
	}

}
?>