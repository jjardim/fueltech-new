<?php
/* Categorias Test cases generated on: 2011-04-07 16:09:59 : 1302203399*/
App::import('Controller', 'Categorias');

class TestCategoriasController extends CategoriasController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class CategoriasControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.categoria', 'app.departamento', 'app.produto', 'app.subcategoria', 'app.vitrine_tipo');

	function startTest() {
		$this->Categorias =& new TestCategoriasController();
		$this->Categorias->constructClasses();
	}

	function endTest() {
		unset($this->Categorias);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
?>