<?php
/* CondicaoPagamentoTipos Test cases generated on: 2011-04-07 16:09:59 : 1302203399*/
App::import('Controller', 'CondicaoPagamentoTipos');

class TestCondicaoPagamentoTiposController extends CondicaoPagamentoTiposController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class CondicaoPagamentoTiposControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.condicao_pagamento_tipo');

	function startTest() {
		$this->CondicaoPagamentoTipos =& new TestCondicaoPagamentoTiposController();
		$this->CondicaoPagamentoTipos->constructClasses();
	}

	function endTest() {
		unset($this->CondicaoPagamentoTipos);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
?>