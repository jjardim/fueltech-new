<?php
/* Clientes Test cases generated on: 2011-04-07 16:38:59 : 1302205139*/
App::import('Controller', 'Clientes');

class TestClientesController extends ClientesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class ClientesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.cliente');

	function startTest() {
		$this->Clientes =& new TestClientesController();
		$this->Clientes->constructClasses();
	}

	function endTest() {
		unset($this->Clientes);
		ClassRegistry::flush();
	}

}
?>