<?php
/* Produtos Test cases generated on: 2011-04-07 16:37:05 : 1302205025*/
App::import('Controller', 'Produtos');

class TestProdutosController extends ProdutosController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class ProdutosControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.produto');

	function startTest() {
		$this->Produtos =& new TestProdutosController();
		$this->Produtos->constructClasses();
	}

	function endTest() {
		unset($this->Produtos);
		ClassRegistry::flush();
	}

}
?>