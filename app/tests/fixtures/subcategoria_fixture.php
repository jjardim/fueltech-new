<?php
/* Subcategoria Fixture generated on: 2011-04-07 16:34:00 : 1302204840 */
class SubcategoriaFixture extends CakeTestFixture {
	var $name = 'Subcategoria';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'categoria_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'index'),
		'departamento_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'index'),
		'nome' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'status' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'length' => 4),
		'data_criacao' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'data_modificacao' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'usuario_criacao' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'usuario_modificacao' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'fk_subcategoria_categoria1' => array('column' => 'categoria_id', 'unique' => 0), 'fk_subcategoria_departamento1' => array('column' => 'departamento_id', 'unique' => 0)),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'categoria_id' => 1,
			'departamento_id' => 1,
			'nome' => 'Lorem ipsum dolor sit amet',
			'status' => 1,
			'data_criacao' => '2011-04-07 16:34:00',
			'data_modificacao' => '2011-04-07 16:34:00',
			'usuario_criacao' => 1,
			'usuario_modificacao' => 1
		),
	);
}
?>