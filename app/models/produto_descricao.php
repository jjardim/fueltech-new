<?php
class ProdutoDescricao extends AppModel {
    var $name = 'ProdutoDescricao';
    var $actsAs = array('Containable');
    var $useTable = 'produto_descricoes';
	var $validate = array(
        'nome' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigatório.'
            )
        ),
	);
    var $belongsTo = array(
		'Produto' => array(
			'className' => 'Produto',
			'foreignKey' => 'produto_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
?>