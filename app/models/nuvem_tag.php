<?php

class NuvemTag extends AppModel {
    var $name = 'NuvemTag';
    var $useTable = 'nuvem_tags';
    var $displayField = 'nome';
    public $actsAs =  array('Cached','Containable');
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    var $validate = array(
        'nome' => array(
            'preenchido' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório'
            ),
            'isUnique' => array(
                'rule' => array('isUnique'),
                'message' => 'Tag já existe no banco'
            )
        ),
        'quantidade' => array(
            'preenchido' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório'
            )
        )
    );
}

?>