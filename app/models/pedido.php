<?php

class Pedido extends AppModel {

    var $name = 'Pedido';
    var $useTable = 'pedidos';
    var $displayField = 'id';
	public $actsAs =  array('Cached','Containable');
    var $validate = array(
        'entrega_prazo' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório.',
                'required' => true
            ),'valid' => array(
                'rule' => array('comparison', '>', 0),
                'message' => 'Campo de preenchimento obrigatório.',
                'required' => true
            )
        ),
		'valor_pedido' => array(
            'valid' => array(
                'rule' => array('comparison', '>', 0),
                'message' => 'Campo de preenchimento obrigatório.',
                'required' => true
            )
        ),
         'frete_id' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Selecione uma Forma de Entrega.',
                'required' => true,
                'on' => 'create',
            )
        ),
        'condicao_pagamento_id' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigatório.',
                'required' => true
            )
        ),
        'parcelas' => array(
            'noempty' => array(
                'rule' => array('validaParcela'),
                'message' => 'Campo de preenchimento obrigatório.',
                'required' => true
            )
        )
    );
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    var $belongsTo = array(
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'usuario_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'PedidoStatus' => array(
            'className' => 'PedidoStatus',
            'foreignKey' => 'pedido_status_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'PagamentoCondicao' => array(
            'className' => 'PagamentoCondicao',
            'foreignKey' => 'pagamento_condicao_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Frete' => array(
            'className' => 'Frete',
            'foreignKey' => 'frete_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
	 var $hasOne = array(
        'CupomQueimado' => array(
            'className' => 'CupomQueimado',
            'foreignKey' => 'pedido_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
	);
    var $hasMany = array(
        'PedidoItem' => array(
            'className' => 'PedidoItem',
            'foreignKey' => 'pedido_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),'PedidoHistoricoStatus' => array(
            'className' => 'PedidoHistoricoStatus',
            'foreignKey' => 'pedido_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
		'PedidoHistoricoTracking' => array(
            'className' => 'PedidoHistoricoTracking',
            'foreignKey' => 'pedido_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
		
    );

     public function isCartao() {

        App::import("model", "PagamentoTipo");
        $this->PagamentoTipo = new PagamentoTipo();
        if (isset($this->data['Pedido']['condicao_pagamento_id'])) {
            $condicao = $this->PagamentoTipo->find('first', array('conditions' => array('PagamentoTipo.id' => (int) $this->data['Pedido']['condicao_pagamento_id'])));
            if ($condicao['PagamentoTipo']['codigo'] != 'CARTAO'
            )
                return false;
            return true;
        }
    }

    public function validaParcela($dados) {
        if (!$this->isCartao()
        )
            return true;
        if (isset($this->data['Pedido']['condicao_pagamento_id']) && is_numeric($this->data['Pedido']['condicao_pagamento_id'])) {
            $condicao = $this->PagamentoCondicao->find('first', array('recursive' => -1, 'conditions' => array('PagamentoCondicao.parcelas >=' => $dados['parcelas'], 'PagamentoCondicao.id' => (int) $this->data['Pedido']['condicao_pagamento_id'])));
            if (!$condicao) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    public function validaNomePortador() {
        if (!$this->isCartao()
        )
            return true;
        if (Validation::notEmpty($this->data['Pedido']['nome_portador_cartao'])) {
            return true;
        }
        return false;
    }

    public function validaNumeroCartao() {

        if (!$this->isCartao()
        )
            return true;
        if (isset($this->data['Pedido']['condicao_pagamento_id']) && is_numeric($this->data['Pedido']['condicao_pagamento_id'])) {
            $condicao = $this->PagamentoCondicao->find('first', array('recursive' => -1, 'conditions' => array('PagamentoCondicao.id' => (int) $this->data['Pedido']['condicao_pagamento_id'])));
            $condicao['PagamentoCondicao']['sigla'] = up($condicao['PagamentoCondicao']['sigla']);
            if ($condicao['PagamentoCondicao']['sigla'] == 'MASTERCARD') {
                $cartao = array('mc');
            } else if ($condicao['PagamentoCondicao']['sigla'] == 'DINERS') {
                $cartao = array('diners');
            } else if ($condicao['PagamentoCondicao']['sigla'] == 'ELO') {
                $cartao = array('elo');
            } else if ($condicao['PagamentoCondicao']['sigla'] == 'AMEX') {
                $cartao = array('amex');
            } else if ($condicao['PagamentoCondicao']['sigla'] == 'VISA') {
                $cartao = array('visa');
            }
            if (Validation::cc($this->data['Pedido']['numero_cartao'], $cartao, false, null)) {
                return true;
            }
        }
        return false;
    }

    public function validaValidadeCartao2() {
        if (!$this->isCartao())return true;
        if (Validation::date($this->data['Pedido']['validade_cartao'], 'my')) {
            return true;
        }
        return false;
    }

    public function validaValidadeCartao3() {
        if (!$this->isCartao()
        )
            return true;

        if (Validation::notEmpty($this->data['Pedido']['validade_cartao'])) {
            return true;
        }
        return false;
    }

    public function validaValidadeCartao($data) {
        if (!$this->isCartao()
        )
            return true;
        if (isset($data['validade_cartao']) && empty($data['validade_cartao'])
        )
            return false;
        list($mes, $ano) = explode('/', $data['validade_cartao']);
       
        if ($ano > date('Y')) {
            return true;
        }
        if ($ano = date('Y') && $mes >= date('m')) {
            return true;
        } else {
            return false;
        }
    }

    public function validaCodigoSeguranca($codigo) {
        if (!$this->isCartao()
        )
            return true;
        if (Validation::notEmpty($codigo['codigo_seguranca_cartao'])) {
            return true;
        }
        return false;
    }

    public function validaCodigoSeguranca2($codigo) {
        if (!$this->isCartao())
            return true;
        $condicao = $this->PagamentoCondicao->find('first', array('recursive' => -1, 'conditions' => array('PagamentoCondicao.id' => (int) $this->data['Pedido']['condicao_pagamento_id'])));
        if (!in_array($condicao['PagamentoCondicao']['sigla'], array('AMEX'))) {
            if (preg_match('/^[0-9]{3}$/', $codigo['codigo_seguranca_cartao'])) {
                return true;
            } else {
                return false;
            }
        } else {
            if (preg_match('/^[0-9]{4}$/', $codigo['codigo_seguranca_cartao'])) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function afterFind($results, $primary = false) {
        if ($this->isCount($results))
            return parent::afterFind($results, $primary);
        App::import("helper", "String");
        $string = new StringHelper();
        if (!empty($results)) {
            foreach ($results as $k => &$r) {
                @$r[$this->alias]['valor_frete'] = $string->bcoToMoeda($r[$this->alias]['valor_frete']);
                @$r[$this->alias]['valor_pedido'] = $string->bcoToMoeda($r[$this->alias]['valor_pedido']);
                @$r[$this->alias]['valor_desconto_cupom'] = $string->bcoToMoeda($r[$this->alias]['valor_desconto_cupom']);
                @$r[$this->alias]['valor_desconto_meio_pagamento'] = $string->bcoToMoeda($r[$this->alias]['valor_desconto_meio_pagamento']);
            }
        }
        return parent::afterFind($results, $primary);
    }

}

?>