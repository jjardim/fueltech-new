<?php
/**
 * Setting
 *
 * PHP version 5
 *
 * @category Model
 * @version  1.0
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 */
class Newsletter extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    var $name = 'Newsletter';
    var $useTable = 'newsletters';
	public $actsAs =  array('Cached','Containable');
/**
 * Validation
 *
 * @var array
 * @access public
 */
    var $validate = array(
        'email' => array(
            'isUnique' => array(
                'rule' => 'isUnique',
                'message' => 'Você já efetuou o cadastro com este email',
            ),
            'valid' => array(
                'rule' => array('email'),
                'message' => 'Email inválido',
            ),
            'noEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo E-mail é obrigatório.',
                'required' =>true,
            ),
        ),
        'nome' => array(
            'rule' => array('notEmpty'),
            'required' =>true,
            'message' => 'Campo Nome é obrigatório.'
        ),
    );
}
?>