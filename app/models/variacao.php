<?php

class Variacao extends AppModel {
    
    public $actsAs =  array('MeioUpload' => array(
							'thumb_filename' => array(								
									'dir' => 'uploads/variacao/thumb', 
									'allowed_mime' => array( 'image/gif', 'image/jpeg', 'image/pjpeg', 'image/png'),
									'allowed_ext' => array('.jpg', '.jpeg', '.png', '.gif'),
									 'fields' => array(
											'filesize' => 'thumb_filesize',
											'mimetype' => 'thumb_mimetype',
											'dir' => 'thumb_dir'
										)
									)
							),'Cached','Containable');
    var $name = 'Variacao';
    var $useTable = 'variacoes';
	var $validate = array(
						'thumb_filename'=>array(
							'Empty' => array(
   								'check' => 
   									false
   								)
							)
						);
	public $hasMany = array(
        'VariacaoDescricao' => array(
            'className' => 'VariacaoDescricao',
            'foreignKey' => 'variacao_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
	
	var $belongsTo = array(
		'VariacaoTipo' => array(
			'className' => 'VariacaoTipo',
			'foreignKey' => 'variacao_tipo_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
		// ,
		// 'Produto2' => array(
			// 'className' => 'Produto',
			// 'foreignKey' => 'produto_id',
			// 'dependent' => false,
			// 'conditions' => '',
			// 'fields' => '',
			// 'order' => '',
			// 'limit' => '',
			// 'offset' => '',
			// 'exclusive' => '',
			// 'finderQuery' => '',
			// 'counterQuery' => ''
		// )
	);
	
	public function beforeSave() {
		
		App::import('Model','Produto');
		$this->Produto = new Produto();		
       
	    if (isset($this->data[$this->alias]['produto_id'])) {
			$produto = $this->Produto->find('first', array('fields' => array('Produto.sku'), 'conditions' => array('Produto.id' => $this->data[$this->alias]['produto_id'])));
            $this->data[$this->alias]['sku'] = $produto['Produto']['sku'];
        }
		
        return parent::beforeSave();
    }
	
	public function salvar($data) {
		foreach ($data[$this->alias] as $valor):
		
			if( $valor['variacao_id'] == "" ) continue;
			
			$valor['produto_id'] = $data['Produto']['id'];
			if (isset($valor['delete']) && $valor['delete'] == true) {
				$this->id = $valor['id'];
				$this->delete($valor);
			} elseif (isset($valor['id']) && $valor['id'] != "") {
				$this->id = $valor['id'];
				$this->save($valor);
			} else {
				$this->save($valor);
				$this->id = null;
			}
		endForeach;
    }
	public function remover_thumb($id){
		//$this->id = $id;
		$variacao['id'] = $id;
		$variacao['thumb'] = 0;
		$variacao['thumb_filename'] = ' ';
		$variacao['thumb_dir'] = null;
		$variacao['thumb_mimetype'] = null;
		$variacao['thumb_filesize'] = null;		
		
		if($this->save($variacao,false))
			return true;
	}
	
}