<?php

class Banner extends AppModel {

    var $name = 'Banner';
    var $useTable = 'banners';
    var $displayField = 'filename';
    var $validate = array(
        'link' => array(
            'rule' => array('url',true),
            'message' => 'Informe um URL válida ex:http://exemplo.com.br',
            'allowEmpty' => true,
        )
    );
    var $actsAs = array('Cached','Containable', 'MeioUpload' => array('filename'));
    var $belongsTo = array(
        'BannerTipo' => array(
            'className' => 'BannerTipo',
            'foreignKey' => 'banner_tipo_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    var $hasAndBelongsToMany = array(
        'Categoria' => array(
            'className' => 'Categoria',
            'joinTable' => 'banners_categorias',
            'foreignKey' => 'banner_id',
            'associationForeignKey' => 'categoria_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'deleteQuery' => '',
            'insertQuery' => ''
        )
    );

}

?>