<?php

class Cliente extends AppModel {

    var $name = 'Cliente';
    var $useTable = 'clientes';
	var $validate = array(
		'nome' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        )
    );
    public $actsAs =  array('MeioUpload' => array(
								'thumb_filename' => array(
										'dir' => 'uploads/clientes', 
										'allowed_mime' => array( 'image/gif', 'image/jpeg', 'image/pjpeg', 'image/png'),
										'allowed_ext' => array('.jpg', '.jpeg', '.png', '.gif'),
										 'fields' => array(
												'filesize' => 'thumb_filesize',
												'mimetype' => 'thumb_mimetype',
												'dir' => 'thumb_dir'
											)
										)
								),'Cached','Containable');

								
}

?>