<?php

class WallpaperTamanho extends AppModel {

    var $name = 'WallpaperTamanho';
    var $useTable = 'wallpaper_tamanhos';
    public $actsAs =  array('Cached','Containable');
	var $validate = array(
		'valor' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
    );
}

?>