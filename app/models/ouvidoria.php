<?php

class Ouvidoria extends AppModel {

  public $actsAs =  array('Cached', 'Containable', 'Validacao',
    'MeioUpload' => array(
      'filename' => array(
        'dir' => 'uploads/ouvidoria',
        'create_directory' => true,
        'allowedMime' => array('application/pdf', 'application/msword', 'application/mspowerpoint', 'application/excel', 'application/rtf', 'application/zip', 'application/octet-stream'),
        'allowedExt' => array('.pdf', '.doc', '.docx', '.dotx', '.xps', '.txt', '.ppt', '.xls', '.xlsx', '.rtf', '.zip', '.odt', '.jpg', '.png', '.jpeg', '.zip', '.rar'),
        'fields' => array(
          'filesize' => 'filesize',
          'mimetype' => 'mimetype',
          'dir' => 'dir'
          )
        )
      )
    );

  var $name = 'Ouvidoria';
  var $useTable = 'ouvidoria';

  var $validate = array(
    'nome' => array(
      'rule' => array('notEmpty'),
      'message' => 'Campo de preenchimento obrigatório.'
      ),
    'telefone' => array(
      'rule' => array('notEmpty'),
      'message' => 'Campo de preenchimento obrigatório.'
      ),
    'celular' => array(
      'rule' => array('notEmpty'),
      'message' => 'Campo de preenchimento obrigatório.'
      ),
    'mensagem' => array(
      'rule' => array('notEmpty'),
      'message' => 'Campo de preenchimento obrigatório.'
      ),
    'email' => array(
      'valido' => array(
        'rule' => array('email'),
        'message' => 'Informe um email válido, ex: example@example.com.br'
        ),
      'obrigatorio' => array(
        'rule' => array('notEmpty'),
        'message' => 'Campo de preenchimento obrigatório.'
        )
      ),
  		'filename' => array(
  				'Empty' => array('check' => false)
  		)
    );

  var $belongsTo = array(
    'SacTipo' => array(
      'className' => 'SacTipo',
      'foreignKey' => 'sac_tipo_id',
      'conditions' => '',
      'fields' => '',
      'order' => ''
      )
    );

  function beforeValidate(){
    if(isset($this->data['Ouvidoria']['nome'])){
      if($this->data['Ouvidoria']['nome']=="Nome*"){
        $this->data['Ouvidoria']['nome'] = null;
      }
    }
    if(isset($this->data['Ouvidoria']['email'])){
      if($this->data['Ouvidoria']['email']=="E-mail*"){
        $this->data['Ouvidoria']['email'] = null;
      }
    }
    if(isset($this->data['Ouvidoria']['mensagem'])){
      if($this->data['Ouvidoria']['mensagem']=="Mensagem*"){
        $this->data['Ouvidoria']['mensagem'] = null;
      }
    }
  }
}
?>