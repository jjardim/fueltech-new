<?php

class Produto extends AppModel {

    var $useTable = 'produtos';
    var $customCountAtributes = false;
    public $actsAs = array('Tree', 'Cached', 'Containable');
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    var $validate = array(
        'status' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigatório.'
            )
        ),
        'nome' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigatório.'
            )
        ),
        'sku' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigatório.'
            ),
            'unique' => array(
                'on'        => 'create',
                'rule' => array('checkUnique', 'sku'),
                'message' => 'Produto já adicionado, com este sku.'
            ),
        ),
        // 'descricao' => array(
        // 'noempty' => array(
        // 'rule' => array('notEmpty'),
        // 'message' => 'Preenchimento obrigatório.'
        // )
        // ),
        'peso' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigatório.'
            )
        ),
        'quantidade' => array(
            'noempty' => array(
                'rule' => array('numeric'),
                'message' => 'Preenchimento obrigatório.'
            )
        ),
        'preco' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigatório.'
            ),
            'validaPreco' => array(
                'rule' => array('validaPreco'),
                'message' => 'O valor deve ser superior a 0,00.'
            )
        ),
        'preco_promocao' => array(
            'validaPrecoPromocao' => array(
                'rule' => array('validaPrecoPromocao'),
                'message' => 'O valor deve ser menor que o preço.',
            )
        ),
        'preco_promocao_novo' => array(
            'valido' => array(
                'rule' => array('validaPrecoPromocaoNovoValido'),
                'message' => 'O valor deve ser menor que o preço de.',
            ),
            'validaPrecoPromocao' => array(
                'rule' => array('validaPrecoPromocaoNovo'),
                'message' => 'Campo de preenchimento obrigatório.',
            )
        ),
        'preco_promocao_inicio' => array(
            'valida' => array(
                'rule' => array('validaPrecoPromocaoInicio'),
                'message' => 'Preenchimento obrigatório.',
            ),
        ),
        'preco_promocao_fim' => array(
            'valida2' => array(
                'rule' => array('validaPrecoPromocaoFimData'),
                'message' => 'A data final deve ser maior que a data inicial.',
            ), 'valida' => array(
                'rule' => array('validaPrecoPromocaoFim'),
                'message' => 'Preenchimento obrigatório.',
            )
        ),
        'fabricante_id' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigatório.'
            )
        ),
        'preco_promocao_pct' => array(
            'noempty' => array(
                'rule' => array('ValidaPct'),
                'message' => 'Preenchimento obrigatório.'
            ), 'noempty' => array(
                'rule' => array('validaPrecoPromocaoPct'),
                'message' => 'Preenchimento obrigatório.'
            )
        )
    );
    var $hasMany = array(
        'ProdutoImagem' => array(
            'className' => 'ProdutoImagem',
            'foreignKey' => 'produto_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'ProdutoCategoria' => array(
            'className' => 'ProdutoCategoria',
            'foreignKey' => 'produto_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'AtributoProduto' => array(
            'className' => 'AtributoProduto',
            'foreignKey' => 'produto_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'VariacaoProduto' => array(
            'className' => 'VariacaoProduto',
            'foreignKey' => 'produto_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'ProdutoPreco' => array(
            'className' => 'ProdutoPreco',
            'foreignKey' => 'produto_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'ProdutoDescricao' => array(
            'className' => 'ProdutoDescricao',
            'foreignKey' => 'produto_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'ProdutoDownload' => array(
            'className' => 'ProdutoDownload',
            'foreignKey' => 'produto_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
		'ProdutoKit' => array(
            'className' => 'ProdutoKit',
            'foreignKey' => 'kit_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );
    var $belongsTo = array(
        'Fabricante' => array(
            'className' => 'Fabricante',
            'foreignKey' => 'fabricante_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    var $hasAndBelongsToMany = array(
        'Categoria' => array(
            'className' => 'Categoria',
            'joinTable' => 'produtos_categorias',
            'foreignKey' => 'produto_id',
            'associationForeignKey' => 'categoria_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'deleteQuery' => '',
            'insertQuery' => ''
        )
    );

    function paginateCount($conditions = null, $recursive = 0, $extra = array()) {
        $parameters = compact('conditions');
        if ($recursive != $this->recursive) {
            $parameters['recursive'] = $recursive;
        }
        //remove os joins
        if ($recursive == -1) {
            $extra = array();
        }
        $count = $this->find('count', array_merge($parameters, $extra));
        return $count;
    }

    // public function beforeFind($queryData) {
    // debug($queryData);;
    // if(empty($queryData['contain']['ProdutoDescricao'])){ 
    // $queryData['joins'][] = array(
    // 'table' => 'produto_descricoes', 
    // 'alias' => 'ProdutoDescricao', 
    // 'type' => 'INNER',
    // 'conditions' => array('ProdutoDescricao.language' => Configure::read('Config.language'))
    // );
    // $queryData['conditions'][] = array('AND' => array('ProdutoDescricao.language' => Configure::read('Config.language'))); 
    // }		
    // debug($queryData);die;
    // return $queryData;
    // }

    public function afterFind($results, $primary = false) {
        if ($this->isCount($results))
            return parent::afterFind($results, $primary);

        App::import("helper", "String");
        $string = new StringHelper();
        App::import("helper", "Calendario");
        $this->Calendario = new CalendarioHelper();

        if (!empty($results)) {
            foreach ($results as $k => &$r) {
                if (isset($r[$this->alias]) && isset($r[$this->alias]['preco_promocao'])) {
                    //strtotime
                    $data_inicial = strtotime($r[$this->alias]['preco_promocao_inicio']);
                    $data_fim = strtotime($r[$this->alias]['preco_promocao_fim']);
                    $data_agora = strtotime(date('Y-m-d H:i:s'));

                    if ($data_agora >= $data_inicial && $data_agora <= $data_fim && (($r[$this->alias]['preco_promocao_novo'] > 0) && ($r[$this->alias]['preco_promocao_novo'] <= $r[$this->alias]['preco']))) {
                        $r[$this->alias]['preco_promocao'] = $r[$this->alias]['preco_promocao_novo'];
                        $r[$this->alias]['preco'] = $r[$this->alias]['preco_promocao_velho'];
                    }
                }
                if (isset($r[$this->alias]) && isset($r[$this->alias]['preco_promocao_inicio'])) {
                    $r[$this->alias]['preco_promocao_inicio'] = $this->Calendario->dataFormatada("d/m/Y", $r[$this->alias]['preco_promocao_inicio']);
                }
                if (isset($r[$this->alias]) && isset($r[$this->alias]['preco_promocao_fim'])) {
                    $r[$this->alias]['preco_promocao_fim'] = $this->Calendario->dataFormatada("d/m/Y", $r[$this->alias]['preco_promocao_fim']);
                }

                if (isset($r[$this->alias]) && isset($r[$this->alias]['relacionados']) && !empty($r[$this->alias]['relacionados'])) {
                    $r[$this->alias]['Relacionados'] = $this->find('list', array('fields' => array('id', 'nome'), 'limit' => 100, 'conditions' => array('Produto.id' => json_decode($r[$this->alias]['relacionados']))));
                    ;
                }
            }
        }

        return parent::afterFind($results, $primary);
    }

    public function beforeSave() {

        App::import("helper", "String");
        $this->String = new StringHelper();
        App::import("helper", "Calendario");
        $this->Calendario = new CalendarioHelper();

        /* decimmal */
        if (isset($this->data[$this->alias]['largura'])) {
            $this->data[$this->alias]['largura'] = $this->String->moedaToBco($this->data[$this->alias]['largura']);
        }
        if (isset($this->data[$this->alias]['altura'])) {
            $this->data[$this->alias]['altura'] = $this->String->moedaToBco($this->data[$this->alias]['altura']);
        }
        if (isset($this->data[$this->alias]['profundidade'])) {
            $this->data[$this->alias]['profundidade'] = $this->String->moedaToBco($this->data[$this->alias]['profundidade']);
        }

        /* decimmal */
        if (!isset($this->data[$this->alias]['parent_id'])) {
            $this->data[$this->alias]['parent_id'] = 0;
        }
        if (isset($this->data[$this->alias]['preco'])) {
            $this->data[$this->alias]['preco'] = $this->String->moedaToBco($this->data[$this->alias]['preco']);
        }
        if (isset($this->data[$this->alias]['custo'])) {
            $this->data[$this->alias]['custo'] = $this->String->moedaToBco($this->data[$this->alias]['custo']);
        }

        if (isset($this->data[$this->alias]['preco_promocao'])) {
            $this->data[$this->alias]['preco_promocao'] = $this->String->moedaToBco($this->data[$this->alias]['preco_promocao']);
        }

        if (isset($this->data[$this->alias]['preco_promocao_novo'])) {
            $this->data[$this->alias]['preco_promocao_novo'] = $this->String->moedaToBco($this->data[$this->alias]['preco_promocao_novo']);
        }
        if (isset($this->data[$this->alias]['preco_promocao_velho'])) {
            $this->data[$this->alias]['preco_promocao_velho'] = $this->String->moedaToBco($this->data[$this->alias]['preco_promocao_velho']);
        }

        if (isset($this->data[$this->alias]['preco_promocao_inicio']) && $this->data[$this->alias]['preco_promocao_inicio'] != "" && $this->String->moedaToBco($this->data[$this->alias]['preco_promocao_novo']) > 0) {
            $this->data[$this->alias]['preco_promocao_inicio'] = $this->Calendario->dataFormatada("Y-m-d 00:00:00", $this->data[$this->alias]['preco_promocao_inicio']);
        } else {
            $this->data[$this->alias]['preco_promocao_inicio'] = null;
            $this->data[$this->alias]['preco_promocao_novo'] = null;
            $this->data[$this->alias]['preco_promocao_velho'] = null;
        }
        if (isset($this->data[$this->alias]['preco_promocao_fim']) && $this->data[$this->alias]['preco_promocao_fim'] != "" && $this->String->moedaToBco($this->data[$this->alias]['preco_promocao_novo']) > 0) {
            $this->data[$this->alias]['preco_promocao_fim'] = $this->Calendario->dataFormatada("Y-m-d 23:59:59", $this->data[$this->alias]['preco_promocao_fim']);
        } else {
            $this->data[$this->alias]['preco_promocao_fim'] = null;
            $this->data[$this->alias]['preco_promocao_novo'] = null;
            $this->data[$this->alias]['preco_promocao_velho'] = null;
        }
        if (isset($this->data[$this->alias]['Relacionados']) && !empty($this->data[$this->alias]['Relacionados'])) {
            $this->data[$this->alias]['relacionados'] = json_encode($this->data[$this->alias]['Relacionados']);
        } else {
            $this->data[$this->alias]['relacionados'] = '';
        }
        return parent::beforeSave();
    }

    function validaPrecoPromocaoInicio($valor) {
        App::import("helper", "String");
        $this->String = new StringHelper();

        if (isset($this->data[$this->alias]['preco_promocao_novo'])) {
            $preco_promocao_novo = $this->String->moedaToBco($this->data[$this->alias]['preco_promocao_novo']);
        } else {
            $preco_promocao_novo = 0;
        }
        if (isset($this->data[$this->alias]['preco_promocao_pct'])) {
            $preco_promocao_pct = $this->String->moedaToBco($this->data[$this->alias]['preco_promocao_pct']);
        } else {
            $preco_promocao_pct = 0;
        }
        if ($preco_promocao_novo > 0 || $preco_promocao_pct > 0) {
            if (Validation::notEmpty($this->data[$this->alias]['preco_promocao_inicio'])) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    function validaPrecoPromocaoFim($valor) {
        App::import("helper", "String");
        $this->String = new StringHelper();

        if (isset($this->data[$this->alias]['preco_promocao_novo'])) {
            $preco_promocao_novo = $this->String->moedaToBco($this->data[$this->alias]['preco_promocao_novo']);
        } else {
            $preco_promocao_novo = 0;
        }
        if (isset($this->data[$this->alias]['preco_promocao_pct'])) {
            $preco_promocao_pct = $this->String->moedaToBco($this->data[$this->alias]['preco_promocao_pct']);
        } else {
            $preco_promocao_pct = 0;
        }
        if ($preco_promocao_novo > 0 || $preco_promocao_pct > 0) {
            if (Validation::notEmpty($this->data[$this->alias]['preco_promocao_fim'])) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    function validaPrecoPromocaoNovo($valor) {
        App::import("helper", "String");
        $this->String = new StringHelper();
        if (Validation::notEmpty($this->data[$this->alias]['preco_promocao_inicio']) && Validation::notEmpty($this->data[$this->alias]['preco_promocao_fim'])) {
            if (Validation::notEmpty($this->data[$this->alias]['preco_promocao_novo'])) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    function validaPrecoPromocaoPct($valor) {
        App::import("helper", "String");
        $this->String = new StringHelper();
        if (Validation::notEmpty($this->data[$this->alias]['preco_promocao_inicio']) && Validation::notEmpty($this->data[$this->alias]['preco_promocao_fim'])) {
            if (Validation::notEmpty($this->data[$this->alias]['preco_promocao_pct'])) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    function validaPrecoPromocaoNovoValido($valor) {
        App::import("helper", "String");
        $this->String = new StringHelper();
        if (Validation::notEmpty($this->data[$this->alias]['preco_promocao_inicio']) && Validation::notEmpty($this->data[$this->alias]['preco_promocao_fim'])) {
            if ($this->String->moedaToBco($this->data[$this->alias]['preco_promocao_novo']) < $this->String->moedaToBco($this->data[$this->alias]['preco'])) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    function validaPrecoPromocaoFimData($valor) {
        App::import("helper", "String");
        $this->String = new StringHelper();
        App::import("helper", "Calendario");
        $this->Calendario = new CalendarioHelper();

        if (isset($this->data[$this->alias]['preco_promocao_novo'])) {
            $preco_promocao_novo = $this->String->moedaToBco($this->data[$this->alias]['preco_promocao_novo']);
        } else {
            $preco_promocao_novo = 0;
        }
        if (isset($this->data[$this->alias]['preco_promocao_pct'])) {
            $preco_promocao_pct = $this->String->moedaToBco($this->data[$this->alias]['preco_promocao_pct']);
        } else {
            $preco_promocao_pct = 0;
        }
        if (($preco_promocao_novo > 0 || $preco_promocao_pct > 0) && $this->data[$this->alias]['preco_promocao_inicio'] != "" && $this->data[$this->alias]['preco_promocao_fim'] != "") {

            list($di, $mi, $yi) = explode('/', current(explode(' ', $this->data[$this->alias]['preco_promocao_inicio'])));
            list($df, $mf, $yf) = explode('/', current(explode(' ', $this->data[$this->alias]['preco_promocao_fim'])));
            $data_inicio = $di . '-' . $mi . '-' . $yi . ' 00:00:00';
            $data_fim = $df . '-' . $mf . '-' . $yf . ' 23:59:59';

            //strtotime
            $data_inicio = strtotime($data_inicio);
            $data_fim = strtotime($data_fim);
            // $data_inicio = $this->Calendario->DataToTimestamp($data_inicio);
            // $data_fim = $this->Calendario->DataToTimestamp($data_fim);

            if ($data_inicio < $data_fim) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    function validaPreco($valor) {
        App::import("helper", "String");
        $string = new StringHelper();

        if ($string->moedaToBco($valor['preco']) <= 0) {
            return false;
        }
        return true;
    }

    function ValidaPct($valor) {

        App::import("helper", "String");
        $string = new StringHelper();

        if ($string->moedaToBco($valor['preco_promocao_pct']) <= 0) {
            return false;
        }
        return true;
    }

    function validaPrecoPromocao() {
        App::import("helper", "String");
        $string = new StringHelper();

        if ($string->moedaToBco($this->data['Produto']['preco_promocao']) >= $string->moedaToBco($this->data['Produto']['preco'])) {
            return false;
        }
        return true;
    }
           
    //atualiza o preco dos produtos a partir um ID
    public function atualiza_preco_by_id($produto_id, $preco) {

        App::import('Model', 'Produto');
        $this->Produto = new Produto();

        if ($produto_id == null) {
            return false;
        }

        $this->Produto->updateAll(array('Produto.preco' => $preco), array('Produto.id' => $produto_id));

    }

    function getProdutosItensPedido($produto_id) {
        //import models
        App::import('Model', 'PedidoItem');
        $this->PedidoItem = new PedidoItem();

        //busco os pedidos que possuem o produto atual
        $pedidos_id = $this->PedidoItem->find('all', array('fields' => array('PedidoItem.pedido_id'), 'conditions' => array('PedidoItem.produto_id' => $produto_id)));
        $pedidos_id = Set::extract('{.n}/PedidoItem/pedido_id', $pedidos_id);

        //seto o numero de pedidos que esse produto se encontra
        $quantidade_pedidos = count($pedidos_id);

        //busco todos os produtos dos respectativos pedidos
        $produtos_pedidos_relacionados = $this->PedidoItem->find('all', array(
            'fields' => array('PedidoItem.produto_id', 'COUNT(PedidoItem.produto_id) as QuantidadeProduto'),
            'conditions' => array('PedidoItem.pedido_id ' => $pedidos_id, 'PedidoItem.produto_id <>' => $produto_id),
            'group' => 'PedidoItem.produto_id',
            'order' => 'QuantidadeProduto DESC',
            'limit' => 8
                )
        );

        //monto o array com o indice com o id do produto, e os valores da calculo da porcentagem
        $retorno = "";
        foreach ($produtos_pedidos_relacionados as $key => $produtos) {
            $retorno[$produtos['PedidoItem']['produto_id']]['produto_id'] = $produtos['PedidoItem']['produto_id'];
            $retorno[$produtos['PedidoItem']['produto_id']]['porcentagem'] = (int) (($produtos[0]['QuantidadeProduto'] / $quantidade_pedidos) * 100);
        }

        //retorno
        return $retorno;
    }

    public function altera_produto_estoque($produto_id) {
	
		$produto = $this->find('first', array('recursive' => -1, 'fields' => array('Produto.id', 'Produto.lft', 'Produto.rght', 'Produto.quantidade_disponivel'), 'conditions' => array('Produto.id' => $produto_id)));
		
		if ($produto['Produto']['quantidade_disponivel'] <= 0) {
			
			//verifico se o produto faz parte de um kit
			//se sim, deixo o mesmo indisponivel
			App::import('Model','ProdutoKit');
			$this->ProdutoKit = new ProdutoKit();
			$produto_kit = $this->ProdutoKit->find('all', array('recursive' => -1, 'fields' => array('ProdutoKit.kit_id'), 'conditions' => array('ProdutoKit.produto_id' => $produto_id)));
			if($produto_kit){
				foreach($produto_kit as $pk){
					$this->updateAll(array('Produto.status' => 2), array('Produto.id' => $pk['ProdutoKit']['kit_id']));
				}
			}
			
            $produtos = $this->find('all', array('recursive' => -1, 'fields' => array('Produto.id', 'Produto.lft', 'Produto.rght', 'Produto.quantidade_disponivel'), 'conditions' => array('Produto.parent_id' => $produto['Produto']['id'])));
			
			if($produtos && count($produtos) > 0){
				foreach ($produtos as $prods) {
					if ($prods['Produto']['quantidade_disponivel'] > 0) {
						//altera o produto atual para filho do filho passado
						$this->updateAll(array('Produto.parent_id' => "'" . $prods['Produto']['id'] . "'", "Produto.lft" => "'" . $prods['Produto']['lft'] . "'", "Produto.rght" => "'" . $prods['Produto']['rght'] . "'"), array('Produto.id' => $produto['Produto']['id']));
						//altera o filho passado para pai de todos
						$this->updateAll(array('Produto.parent_id' => "0", "Produto.lft" => "'" . $produto['Produto']['lft'] . "'", "Produto.rght" => "'" . $produto['Produto']['rght'] . "'"), array('Produto.id' => $prods['Produto']['id']));
						return;
					}
				}
			}else{
				$this->updateAll(array('Produto.status' => 2), array('Produto.id' => $produto_id));
			}
        }
    }
}

?>