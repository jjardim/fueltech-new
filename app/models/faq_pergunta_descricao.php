<?php

class FaqPerguntaDescricao extends AppModel {

    var $name = 'FaqPerguntaDescricao';
    var $useTable = 'faq_pergunta_descricoes';
    public $actsAs =  array('Cached','Containable');
	var $validate = array(
		'pergunta' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
		'resposta' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
    );
	
	var $belongsTo = array(
		'FaqPergunta' => array(
			'className' => 'FaqPergunta',
			'foreignKey' => 'faq_pergunta_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}

?>