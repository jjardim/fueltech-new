<?php

class Usuario extends AppModel {

    var $actsAs = array('Containable','Validacao', 'Acl' => 'requester');
    var $name = 'Usuario';
    var $useTable = 'usuarios';
//The Associations below have been created with all possible keys, those that are not needed can be removed
    var $validate = array(
        'cpf' => array(
            'unico' => array(
                'rule' => array('isUnique'),
                'message' => 'Este cpf já está cadastrado, insira outro cpf!',
            ),
            'valido' => array(
                'rule' => array('cpf', true),
                'message' => 'O campo cpf não contem um CPF válido'
            ),
            'preenchido' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório'
            )
        ),
        'cnpj' => array(
            'unico' => array(
                'rule' => array('isUnique'),
                'message' => 'Este cnpj já está cadastrado, insira outro cnpj!',
                'on' => 'create'
            ),
            'valido' => array(
                'rule' => array('cnpj', true),
                'message' => 'O campo cnpj não contem um CNPJ válido',
            ),
            'preenchido' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório'
            )
        ),
      /*  'rg' => array(
            'preenchido1' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório'
            )
        ),*/
        'celular' => array(
            'preenchido1' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório'
            )
        ),
		'nome' => array(
            'valido' => array(
               'rule'=>array('custom', '/[A-Za-z ] [A-Za-z ]/'),
                'message' => 'O nome deve ser composto'
            ),'preenchido' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório'
            )
        ),
		'nomeTmp' => array(
            'valido' => array(
               'rule'=>array('validaNomeTemporario'),
                'message' => 'O nome deve ser composto'
            ),
			'preenchido' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório'
            )
        ),
        'tipo_pessoa' => array(
            'valid' => array(
                'rule' => array('inList', array('F', 'J')),
                'message' => 'Campo de preenchimento obrigatório.'
            ),
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório.'
            )
        ),
        // 'sexo' => array(
            // 'rule' => array('inList', array('F', 'M')),
            // 'message' => 'Campo de preenchimento obrigatório.'
        // ),
        'email' => array(
            'valid' => array(
                'rule' => array('email'),
                'message' => 'O email informado não é válido, tente o formado teste@teste.com'
            ),
            'notempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório.'
            ), 'unico' => array(
                'rule' => array('isUnique'),
                'message' => 'Este email já está cadastrado, insira outro email!',
                'on' => 'create'
            ),
        ),
        'senha_nova' => array(
            'confirm' => array(
                'rule' => array('validaRepetirSenha'),
                'message' => 'As senhas não conferem'
            ),
            'minimoUpdate' => array(
                'rule' => array('between', 6, 20),
                'message' => 'A senha deve ter de 6 à  20 caracteres',
                'allowEmpty' => true,
                'on' => 'update'
            ),
            'minimo' => array(
                'rule' => array('between', 6, 20),
                'message' => 'A senha deve ter 6 à 20 caracteres',
            //'on' => 'create'
            ),
            'preenchido' => array(
                'rule' => 'notEmpty',
                'message' => 'Campo de preenchimento obrigatório',
                'required' => true,
                'on' => 'create'
            )
        ),
        'senha_nova_fake' => array(
            'preenchido' => array(
                'rule' => 'notEmpty',
                'message' => 'Campo de preenchimento obrigatório',
                'on' => 'create'
            )
        ),
        'status' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
        'grupo_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'Campo de preenchimento obrigatório.'
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Para a validação após esta regra
            //'on' => 'create', // Limitar a validação para as operações 'create' ou 'update'
        )),
    );
    // As associações abaixo foram criadas com todas as chaves possíveis, então é possível remover as que não são necessárias
    var $belongsTo = array(
        'Grupo' => array(
            'className' => 'Grupo',
            'foreignKey' => 'grupo_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    var $hasMany = array(
        'UsuarioEndereco' => array(
            'className' => 'UsuarioEndereco',
            'foreignKey' => 'usuario_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Pedido' => array(
            'className' => 'Pedido',
            'foreignKey' => 'usuario_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );
    function changeDataSource($newSource) {
        parent::setDataSource($newSource);
        parent::__construct();
    }

    //as funções abaixo fazem a adição
    function parentNode() {
        if (!isset($data['Usuario']['grupo_id'])
            )return true;

        if (!$this->id && empty($this->data)) {
            return null;
        }
        $data = $this->data;
        if (empty($this->data)) {
            $data = $this->read();
        }
        if (!$data['Usuario']['grupo_id']) {
            return null;
        } else {
            return array('Grupo' => array('id' => $data['Usuario']['grupo_id']));
        }
    }

    function afterSave($created) {
        if (!isset($this->data['Usuario']['email'])
            )return true;
        $parent = $this->parentNode();
        $parent = $this->node($parent);
        $node = $this->node();
        $aro = $node[0];
        $aro['Aro']['parent_id'] = $parent[0]['Aro']['id'];
        $aro['Aro']['alias'] = $this->data['Usuario']['email'];
        $this->Aro->save($aro);
    }

    /**
     * Verifica se as senhas digitadas conferem
     *
     * @return boolean
     */
    public function validaRepetirSenha() {
        if (isset($this->data[$this->name]['senha_nova']) || isset($this->data[$this->name]['confirm']))
            return (@$this->data[$this->name]['senha_nova'] == @$this->data[$this->name]['confirm']);
        else
            return true;
    }

    public function afterFind($results, $primary = false) {
        if ($this->isCount($results))
            return parent::afterFind($results, $primary);
        App::import("helper", "Calendario");
        $calendario = new CalendarioHelper();
        if (!empty($results)) {
            foreach ($results as $k => &$r) {

                @$r[$this->alias]['data_nascimento'] = $calendario->dataFormatada("d-m-Y", $r[$this->alias]['data_nascimento']);
				 @$r[$this->alias]['nomeTmp'] = $r[$this->alias]['nome'];
            }
        }
        return parent::afterFind($results, $primary);
    }
	public function validaNomeTemporario(){
		if(!preg_match('/[A-Za-z ] [A-Za-z ]/',$this->data[$this->alias]['nomeTmp'])){
			return false;
		}else{
			return true;
		}		
	}
    public function beforeSave() {

        App::import("helper", "Calendario");
        $calendario = new CalendarioHelper();

        if (isset($this->data[$this->alias]['tipo_pessoa']) && $this->data[$this->alias]['tipo_pessoa'] == "F") {
            $this->data[$this->alias]['cpf'] = preg_replace("/[^0-9]/", "", $this->data[$this->alias]['cpf']);
            $this->data[$this->alias]['data_nascimento'] = $calendario->dataFormatada("Y-m-d", $this->data[$this->alias]['data_nascimento']);
        } else {
            if (isset($this->data[$this->alias]['cnpj'])) {
                $this->data[$this->alias]['cnpj'] = preg_replace("/[^0-9]/", "", $this->data[$this->alias]['cnpj']);
            }
        }
        if (isset($this->data[$this->alias]['telefone'])) {
            $this->data[$this->alias]['telefone'] = preg_replace("/[^0-9]/", "", $this->data[$this->alias]['telefone']);
        }
        if (isset($this->data[$this->alias]['celular'])) {
            $this->data[$this->alias]['celular'] = preg_replace("/[^0-9]/", "", $this->data[$this->alias]['celular']);
        }
        if (!empty($this->data[$this->alias]['senha_nova'])) {
            $this->data[$this->alias]['senha'] = Security::hash($this->data[$this->alias]['senha_nova']);
        }
		
		if (isset($this->data[$this->alias]['nomeTmp']) && !empty($this->data[$this->alias]['nomeTmp'])) {
            $this->data[$this->alias]['nome'] = $this->data[$this->alias]['nomeTmp'];
        }

        return parent::beforeSave();
    }

    public function beforeValidate() {
        if (isset($this->data[$this->alias]['tipo_pessoa'])) {
            if (isset($this->data[$this->alias]['tipo_pessoa']) && $this->data[$this->alias]['tipo_pessoa'] == "F") {
                $this->data[$this->alias]['cpf'] = preg_replace("/[^0-9]/", "", $this->data[$this->alias]['cpf']);
            }
            if (isset($this->data[$this->alias]['tipo_pessoa']) && $this->data[$this->alias]['tipo_pessoa'] == "J") {
                $this->data[$this->alias]['cnpj'] = preg_replace("/[^0-9]/", "", $this->data[$this->alias]['cnpj']);
            }
        }
    }    
}

?>