<?php

class Veiculo extends AppModel {

    var $name = 'Veiculo';
    var $useTable = 'veiculos';
    public $actsAs =  array('Cached','Containable');
	var $validate = array(
		'nome' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
        'descricao' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
	);
	
	var $belongsTo = array(
		'Galeria' => array(
            'className' => 'Galeria',
            'foreignKey' => 'galeria_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
	
	function trata_field($field){
		
		$retorno = "";
		
		switch($field){
			case 'veiculo':
			$retorno = "Veículo: ";
			break;
			
			case 'proprietario':
			$retorno = "Proprietário: ";
			break;
			
			case 'preparador':
			$retorno = "Preparador: ";
			break;
			
			case 'cilindradas':
			$retorno = "Cilindrada/Nº de Válvulas: ";
			break;
			
			case 'potencia_estimada':
			$retorno = "Potência estimada ou aferida: ";
			break;
			
			case 'dinamometro':
			$retorno = "Dinamômetro utilizado: ";
			break;
			
			case 'comando_valvulas':
			$retorno = "Comando de válvulas: ";
			break;
			
			case 'injetores':
			$retorno = "Injetores: ";
			break;
			
			case 'combustivel':
			$retorno = "Combustível: ";
			break;
			
			case 'coletor_admissao':
			$retorno = "Coletor de admissão: ";
			break;
			
			case 'coletor_escape':
			$retorno = "Coletor de escape: ";
			break;
			
			case 'turbina':
			$retorno = "Turbina: ";
			break;
			
			case 'pressao_utilizada':
			$retorno = "Pressão utilizada: ";
			break;
			
			case 'bobina':
			$retorno = "Bobina: ";
			break;
			
		}
		return $retorno;
	}
}

?>