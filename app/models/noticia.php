<?php

class Noticia extends AppModel {

    var $name = 'Noticia';
    var $useTable = 'noticias';
    var $admin = false;
    public $actsAs = array('MeioUpload' => array(
	    'thumb_filename' => array(
		'dir' => 'uploads/noticia/thumb',
		'allowed_mime' => array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/png'),
		'allowed_ext' => array('.jpg', '.jpeg', '.png', '.gif'),
		'fields' => array(
		    'filesize' => 'thumb_filesize',
		    'mimetype' => 'thumb_mimetype',
		    'dir' => 'thumb_dir'
		)
	    )
	), 'Cached', 'Containable');
    var $validate = array(
	'titulo' => array(
	    'rule' => array('notEmpty'),
	    'message' => 'Campo de preenchimento obrigatório.'
	),
	'descricao' => array(
	    'rule' => array('notEmpty'),
	    'message' => 'Campo de preenchimento obrigatório.'
	),
	'conteudo' => array(
	    'rule' => array('notEmpty'),
	    'message' => 'Campo de preenchimento obrigatório.'
	),
    );
    var $belongsTo = array(
	'NoticiaTipo' => array(
	    'className' => 'NoticiaTipo',
	    'foreignKey' => 'noticia_tipo_id',
	    'conditions' => '',
	    'fields' => '',
	    'order' => ''
	),
	'Galeria' => array(
	    'className' => 'Galeria',
	    'foreignKey' => 'galeria_id',
	    'conditions' => '',
	    'fields' => '',
	    'order' => ''
	)
    );

    public function remover_thumb($id) {
	$model['id'] = $id;
	$model['thumb_filename'] = ' ';
	$model['thumb_dir'] = null;
	$model['thumb_mimetype'] = null;
	$model['thumb_filesize'] = null;

	if ($this->save($model, false))
	    return true;
    }
    
    public function beforeSave() {
	
        App::import("helper", "String");
        $string = new StringHelper();
	
       
		if (isset($this->data[$this->alias]['created'])) {
			list($dia,$mes,$ano) = explode('/',$this->data[$this->alias]['created']);
			$this->data[$this->alias]['created'] = "$ano-$mes-$dia 23:59:59";
        }	      

        return parent::beforeSave();
    }
	
	 public function afterFind($results, $primary = false) {
        App::import("helper", "String");
        $string = new StringHelper();
        if (!empty($results) && $this->admin==true) {
            foreach ($results as $k => &$r) {

				if (isset($r[$this->alias]) && isset($r[$this->alias]['created'])) {
					list($ano,$mes,$dia) = explode('-',current(explode(' ',$r[$this->alias]['created'])));
					$r[$this->alias]['created'] = "$dia/$mes/$ano";
                }
            }
        }

        return parent::afterFind($results, $primary);
    }

}

?>