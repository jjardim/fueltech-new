<?php

class XmlVariavel extends AppModel {

    var $name = 'XmlVariavel';
    var $useTable = 'xml_variaveis';
    public $actsAs =  array('Cached','Containable');
	var $validate = array(
		'valor' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
    );
	
	public $belongsTo = array(
		'XmlTipo' => array(
			'className' => 'XmlTipo',
			'foreignKey' => 'xml_tipo_id'
		)
	);
}

?>