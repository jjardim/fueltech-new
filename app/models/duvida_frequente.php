<?php

class DuvidaFrequente extends AppModel {

    var $name = 'DuvidaFrequente';
    var $useTable = 'duvidas_frequentes';
    public $actsAs =  array('Cached','Containable');
	
	public $hasMany = array(
		'DuvidaFrequenteDescricao' => array(
            'className' => 'DuvidaFrequenteDescricao',
            'foreignKey' => 'duvida_frequente_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}

?>