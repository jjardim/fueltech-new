<?php

class VariacaoProduto extends AppModel {

    public $actsAs = array('Cached', 'Containable');
    var $name = 'VariacaoProduto';
    var $useTable = 'variacoes_produtos';
    //var $primaryKey = 'variacao_id';
    var $belongsTo = array(
        'Variacao' => array(
            'className' => 'Variacao',
            'foreignKey' => 'variacao_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    public function salvar($data) {
        $this->deleteAll(array('produto_id' => $data['Produto']['id']), false);
        $array_variacoes['Variacoes'] = array();
        
        foreach ($data[$this->alias] as $valor):
            $valor['id'] = '';
            if (isset($valor['delete']) && $valor['delete'] == true) {
                continue;
            } elseif (isset($valor['variacao_id']) && $valor['variacao_id'] != "") {
                if ($rest = $this->find('first', array('recursive' => -1, 'conditions' => array('VariacaoProduto.variacao_id' => $valor['variacao_id'], 'VariacaoProduto.produto_id' => $data['Produto']['id'])))) {
                    //$this->save($valor);
                     continue;
                }else{
                     $valor['agrupador'] = $data['Produto']['agrupador'];
                     //$this->save(array('variacao_id' => $valor['variacao_id'], 'produto_id' => $valor['produto_id'], 'agrupador' => $data['Produto']['agrupador']));
                     $this->save($valor);
                     $array_variacoes['Variacoes'][] = $valor['variacao_id'];
                }
            }
        endForeach;
        return $array_variacoes;
    }

}