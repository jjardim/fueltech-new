<?php

class Sac extends AppModel {

    var $name = 'Sac';
	var $useTable = 'sac';
	public $actsAs =  array('Cached','Containable');
    var $validate = array(
        'nome' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
        'mensagem' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
		'cnpj' => array(
            'preenchido' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório'
            )
        ),
        'email' => array(
            'valido' => array(
                'rule' => array('email'),
                'message' => 'Informe um email válido, ex: example@example.com.br'
            ),
            'obrigatorio' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório.'
            )
        )
    );

    var $belongsTo = array(
        'SacTipo' => array(
            'className' => 'SacTipo',
            'foreignKey' => 'sac_tipo_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Loja' => array(
            'className' => 'Loja',
            'foreignKey' => 'loja_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Vendedor' => array(
            'className' => 'Vendedor',
            'foreignKey' => 'vendedor_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    function beforeValidate(){
		if(isset($this->data['Sac']['nome'])){
            if($this->data['Sac']['nome']=="Nome*"){
                $this->data['Sac']['nome'] = null;
            }
        }
		if(isset($this->data['Sac']['email'])){
            if($this->data['Sac']['email']=="E-mail*"){
                $this->data['Sac']['email'] = null;
            }
        }
        if(isset($this->data['Sac']['mensagem'])){
            if($this->data['Sac']['mensagem']=="Mensagem*"){
                $this->data['Sac']['mensagem'] = null;
            }
        }
		if(isset($this->data['Sac']['cnpj'])){
            if($this->data['Sac']['cnpj']=="CNPJ*"){
                $this->data['Sac']['cnpj'] = null;
            }
        }
    }

}

?>