<?php

class DownloadTipo extends AppModel {

    var $name = 'DownloadTipo';
    var $useTable = 'download_tipos';
    public $actsAs =  array('Cached','Containable');
	var $validate = array(
		'nome' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
    );
	
	var $hasMany = array(
		'Download' => array(
			'className' => 'Download',
			'foreignKey' => 'download_tipo_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
}

?>