<?php
class Cor extends AppModel {
	var $name = 'Cor';
	var $actsAs = array('Containable','MeioUpload' => array('filename'));

	var $displayField = 'nome';
	var $validate = array(
		'nome' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' 	=> 'Campo de preenchimento obrigatório.'
			),
		),
	);
	var $hasMany = array(
		'Produto' => array(
            'className' => 'Produto',
            'foreignKey' => 'cor_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}
?>