<?php

class SacTipo extends AppModel {

    var $name = 'SacTipo';
    var $useTable = 'sac_tipos';
    public $actsAs =  array('Cached','Containable');
	var $validate = array(
        'nome' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        )
    );

    var $hasMany = array(
        'Sac' => array(
            'className' => 'Sac',
            'foreignKey' => 'sac_tipo_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}

?>