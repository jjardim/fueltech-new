<?php

class Download extends AppModel {

    var $name = 'Download';
    var $useTable = 'downloads';
    public $actsAs =  array('MeioUpload' => array(
								'filename' => array(
									'dir' => 'uploads/download/filename', 
									'allowedMime' => array('application/pdf', 'application/msword', 'application/mspowerpoint', 'application/excel', 'application/rtf', 'application/zip','application/octet-stream'),
									'allowedExt' => array('.pdf', '.doc', '.ppt', '.xls', '.rtf', '.zip', '.rar'),
									'maxSize' => 999999999999999999999999,
									'fields' => array(
										'filesize' => 'filesize',
										'mimetype' => 'mimetype',
										'dir' => 'dir'
									)
								),
								'thumb_filename' => array(								
									'dir' => 'uploads/download/thumb', 
									'allowed_mime' => array( 'image/gif', 'image/jpeg', 'image/pjpeg', 'image/png'),
									'allowed_ext' => array('.jpg', '.jpeg', '.png', '.gif'),
									'fields' => array(
										'filesize' => 'thumb_filesize',
										'mimetype' => 'thumb_mimetype',
										'dir' => 'thumb_dir'
									)
								)
							),'Cached','Containable');
	var $validate = array(
		'nome' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
		'thumb_filename' => array(
            // 'Empty' => array('check' => false)	
        ),
    );
	
	var $belongsTo = array(
        'DownloadTipo' => array(
            'className' => 'DownloadTipo',
            'foreignKey' => 'download_tipo_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
	
	public $hasMany = array(
        'ProdutoDownload' => array(
            'className' => 'ProdutoDownload',
            'foreignKey' => 'produto_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );
	
	public function remover_thumb($id){
		//$this->id = $id;
		$data['id'] = $id;
		$data['thumb_filename'] = ' ';
		$data['thumb_dir'] = null;
		$data['thumb_mimetype'] = null;
		$data['thumb_filesize'] = null;		
		
		if($this->save($data,false))
			return true;
	}
}

?>