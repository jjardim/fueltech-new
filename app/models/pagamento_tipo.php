<?php

class PagamentoTipo extends AppModel {

    var $name = 'PagamentoTipo';
    var $useTable = 'pagamento_tipos';
    var $displayField = 'nome';
	public $actsAs =  array('Cached','Containable');
    var $validate = array(
        'nome' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Campo de preenchimento obrigatório.'
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'status' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Campo de preenchimento obrigatório.'
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        
    );
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    var $hasMany = array(
        'PagamentoCondicao' => array(
            'className' => 'PagamentoCondicao',
            'foreignKey' => 'pagamento_tipo_id',
            'dependent' => false,
            'conditions' => 'PagamentoCondicao.status=true',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );
    
   
}

?>