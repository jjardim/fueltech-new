<?php

class Indique extends AppModel {

   public $useTable = false;
   public $actsAs = array('Containable');
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    var $validate = array(
        'seu_nome' => array(
            'noempty1' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório.',
                'required' =>true
            )
        ),
        'seu_email' => array(
             'valid' => array(
                'rule' => array('email'),
                'message' => 'informe um email válido.',
                'required' =>true
            ),
            'noempty2' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório.',
                'required' =>true
            ),
           
        ),
        'amigo_nome' => array(
            'noempty3' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório.',
                'required' =>true
            )
        ),
        'amigo_email' => array(
            'valid' => array(
                'rule' => array('email'),
                'message' => 'informe um email válido.',
                'required' =>true
            ),
            'noempty4' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório.',
                'required' =>true
            ),
            
        ),
    );




}

?>