<?php

class Video extends AppModel {

    var $name = 'Video';
    var $useTable = 'videos';
	 var $admin = false;
    public $actsAs = array('MeioUpload' => array(
	    'thumb_filename' => array(
		'dir' => 'uploads/videos/thumb',
		'allowed_mime' => array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/png'),
		'allowed_ext' => array('.jpg', '.jpeg', '.png', '.gif'),
		'fields' => array(
		    'filesize' => 'thumb_filesize',
		    'mimetype' => 'thumb_mimetype',
		    'dir' => 'thumb_dir'
		)
	    )
	), 'Cached', 'Containable');
    var $validate = array(
	'nome' => array(
	    'rule' => array('notEmpty'),
	    'message' => 'Campo de preenchimento obrigatório.'
	),
	'url' => array(
	    'rule' => array('notEmpty'),
	    'message' => 'Campo de preenchimento obrigatório.'
	),
	'thumb_filename' => array(
	    'Empty' => array('check' => false)
	),
    );
    var $belongsTo = array(
	'VideoTipo' => array(
	    'className' => 'VideoTipo',
	    'foreignKey' => 'video_tipo_id',
	    'conditions' => '',
	    'fields' => '',
	    'order' => ''
	)
    );

    public function remover_thumb($id) {
	//$this->id = $id;
	$data['id'] = $id;
	$data['thumb_filename'] = ' ';
	$data['thumb_dir'] = null;
	$data['thumb_mimetype'] = null;
	$data['thumb_filesize'] = null;

	if ($this->save($data, false))
	    return true;
    }
	
	
	public function beforeSave() {
	
        App::import("helper", "String");
        $string = new StringHelper();
	
       
		if (isset($this->data[$this->alias]['created'])) {
			list($dia,$mes,$ano) = explode('/',$this->data[$this->alias]['created']);
			$this->data[$this->alias]['created'] = "$ano-$mes-$dia 23:59:59";
        }	      

        return parent::beforeSave();
    }
	
	 public function afterFind($results, $primary = false) {
        App::import("helper", "String");
        $string = new StringHelper();
        if (!empty($results)&& $this->admin==true) {
            foreach ($results as $k => &$r) {

				if (isset($r[$this->alias]) && isset($r[$this->alias]['created'])) {
					list($ano,$mes,$dia) = explode('-',current(explode(' ',$r[$this->alias]['created'])));
					$r[$this->alias]['created'] = "$dia/$mes/$ano";
                }
            }
        }

        return parent::afterFind($results, $primary);
    }
	
}

?>