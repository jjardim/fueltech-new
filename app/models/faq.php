<?php

class Faq extends AppModel {

    var $name = 'Faq';
    var $useTable = 'faqs';
    public $actsAs =  array('Cached','Containable');
	var $validate = array(
		'nome' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
    );
	
	public $hasMany = array(
		'FaqPergunta' => array(
            'className' => 'FaqPergunta',
            'foreignKey' => 'faq_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}

?>