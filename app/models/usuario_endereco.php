<?php

class UsuarioEndereco extends AppModel {
    public $actsAs = array('Validacao','Containable');
    var $name = 'UsuarioEndereco';
    var $useTable = 'usuario_enderecos';
    var $displayField = 'nome';
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    var $validate = array(
        'nome' => array(
            'preenchido' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório'
            )
        ),
        'status' => array(
            'preenchido' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório'
            )
        ),
        'rua' => array(
            'preenchido' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório'
            )
        ),
        'numero' => array(
            'preenchido' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório'
            )
        ),
        'cidade' => array(
            'preenchido' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório'
            )
        ),
        'uf' => array(
            'preenchido' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório'
            )
        ),
        'principal' => array(
            'preenchido' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório'
            )
        ),
        'bairro' => array(
            'preenchido' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório'
            )
        ),
        'cep' => array(
            'preenchido' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório'
            )
        )
    );
    var $belongsTo = array(
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'usuario_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );



    public function beforeSave() {

        $this->data[$this->alias]['cep'] = preg_replace("/[^0-9]/", "", $this->data[$this->alias]['cep']);

        return parent::beforeSave();
    }


}

?>