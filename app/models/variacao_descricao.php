<?php
class VariacaoDescricao extends AppModel {
    var $name = 'VariacaoDescricao';
    var $actsAs = array('Containable');
    var $useTable = 'variacao_descricoes';
    var $belongsTo = array(
		'Variacao' => array(
			'className' => 'Variacao',
			'foreignKey' => 'variacao_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
?>