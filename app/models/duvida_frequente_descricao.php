<?php

class DuvidaFrequenteDescricao extends AppModel {

    var $name = 'DuvidaFrequenteDescricao';
    var $useTable = 'duvidas_frequente_descricoes';
    public $actsAs =  array('Cached','Containable');
	var $belongsTo = array(
		'DuvidaFrequente' => array(
			'className' => 'DuvidaFrequente',
			'foreignKey' => 'duvida_frequente_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}

?>