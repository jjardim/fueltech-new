<?php
class Url extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    var $name = 'Url';
/**
 * Behaviors used by the Model
 *
 * @var array
 * @access public
 */
    var $actsAs = array('Containable',
        'Encoder',
        'Cached' => array(
            'prefix' => array(
                'Url_',
                'menu_',
                'croogo_menu_',
            ),
        ),
    );
/**
 * Validation
 *
 * @var array
 * @access public
 */
    var $validate = array(
        'url_antiga' => array(
            'rule' => array('minLength', 1),
            'message' => 'Campo de preenchimento obrigatório.',
        ),
        'url_nova' => array(
            'rule' => array('minLength', 1),
            'message' => 'Campo de preenchimento obrigatório.',
        ),
    );
}
?>