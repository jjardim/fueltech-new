<?php
class VariacaoTipo extends AppModel {
	var $name = 'VariacaoTipo';
	public $actsAs =  array('Containable');

	var $displayField = 'nome';
	var $validate = array(
		'nome' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' 	=> 'Campo de preenchimento obrigatório.'
			),
			'unique' => array(
				'rule' => array('checkUnique','nome'),
				'message' 	=> 'Atributo já adicionado, tente outro nome.'
			),
		),
	);
	
	var $hasMany = array(
		'Variacao' => array(
			'className' => 'VariacaoProduto',
			'foreignKey' => 'variacao_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
?>