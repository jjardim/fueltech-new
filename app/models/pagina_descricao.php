<?php
class PaginaDescricao extends AppModel {

    var $name = 'PaginaDescricao';
    var $actsAs = array('Containable');
    var $useTable = 'pagina_descricoes';
	
    var $belongsTo = array(
		'Pagina' => array(
			'className' => 'Pagina',
			'foreignKey' => 'pagina_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
?>