<?php

class ProdutoVitrine extends AppModel {
    
    public $actsAs =  array('Cached','Containable');
    var $primaryKey = 'vitrine_id';
    var $name = 'ProdutoVitrine';
    var $useTable = 'produtos_vitrines';
    var $displayField = 'nome';
     var $belongsTo = array(
        'Vitrine' => array(
            'className' => 'Vitrine',
            'foreignKey' => 'vitrine_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
     var $hasMany = array(
        'Produto' => array(
            'className' => 'Produto',
            'foreignKey' => 'produto_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
			 'limit'        => '1',
        ),
           'ProdutoImagem' => array(
            'className' => 'ProdutoImagem',
            'foreignKey' => 'produto_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
			 'limit'        => '1',
        )
    );
     

}

?>