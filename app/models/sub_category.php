<?php


class SubCategory extends AppModel {
   
	var $useTable = 'categorias';
    public $hasMany = array(
       
        'TerCategory' => array(
			'className' => 'TerCategory',
            'order' => 'lft',
            'foreignKey' => 'parent_id',
            'dependent' => true
		)
    );

    
	
}

?>