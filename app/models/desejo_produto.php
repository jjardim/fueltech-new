<?php

class DesejoProduto extends AppModel {

    var $name = 'DesejoProduto';
    var $useTable = 'desejos_produtos';
    public $actsAs = array('Containable');
   var $belongsTo = array(
        'Produto' => array(
            'className' => 'Produto',
            'foreignKey' => 'produto_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ));
}

?>