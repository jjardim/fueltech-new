<?php
class Link extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    var $name = 'Link';
/**
 * Behaviors used by the Model
 *
 * @var array
 * @access public
 */
    var $actsAs = array('Containable',
        'Encoder',
        'Tree',
        'Cached' => array(
            'prefix' => array(
                'link_',
                'menu_',
                'croogo_menu_',
            ),
        ),
    );
/**
 * Validation
 *
 * @var array
 * @access public
 */
    var $validate = array(
        'title' => array(
            'rule' => array('minLength', 1),
            'message' => 'Campo de preenchimento obrigatório.',
        ),
        'link' => array(
            'rule' => array('minLength', 1),
            'message' => 'Campo de preenchimento obrigatório.',
        ),
    );
/**
 * Model associations: belongsTo
 *
 * @var array
 * @access public
 */
    var $belongsTo = array(
        'Menu' => array('counterCache' => true)
    );

}
?>