<?php

class ProdutoAtributo extends AppModel {
    
    public $actsAs =  array('Cached','Containable');
    var $primaryKey = 'produto_id';
    var $name = 'ProdutoAtributo';
    var $useTable = 'produtos_atributos';
}