<?php

class Importacao extends AppModel {

    var $name = 'Importacao';
    var $useTable = 'importacoes';
     var $validate = array(
        'file' => array(
            'rule' => array('extension',array('csv')),
            'message' => 'O arquivo deve ter a extensão csv',
            'required' => true,
        ),

    );
}

?>