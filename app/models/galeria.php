<?php

class Galeria extends AppModel {

    var $name = 'Galeria';
    var $useTable = 'galerias';
    public $actsAs =  array('Cached','Containable');
	var $validate = array(
		'marca' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
		'galeria_tipo_id' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        )
    );
	
	 var $belongsTo = array(
        'GaleriaTipo' => array(
            'className' => 'GaleriaTipo',
            'foreignKey' => 'galeria_tipo_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
	
	var $hasMany = array(
		'GaleriaFoto' => array(
			'className' => 'GaleriaFoto',
			'foreignKey' => 'galeria_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
}

?>