<?php

class VideoTipo extends AppModel {

    var $name = 'VideoTipo';
    var $useTable = 'video_tipos';
    public $actsAs =  array('Cached','Containable');
	var $validate = array(
		'nome' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
    );
	
	var $hasMany = array(
        'Video' => array(
            'className' => 'Video',
            'foreignKey' => 'video_tipo_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}

?>