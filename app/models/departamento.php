<?php

/**
 * category.php
 *
 * @author Matti Putkonen,  matti.putkonen@fi3.fi
 * @copyright Copyright (c); 2006-2009, Matti Putkonen, Helsinki, Finland
 * @package BakeSale
 * @version $Id: category.php 451 2007-06-15 08:01:35Z matti $
 */
class Departamento extends AppModel {
	public $displayField = 'nome';
    public $actsAs = array('Tree', 'Containable');
    public $validate = array(
        'nome' => array(
            'notEmpty' => array(
                'required' => true,
                'rule' => 'notEmpty',
                'message' => 'Campo de preenchimento obrigatório',
            ),
        ),
    );

    public function beforeSave() {

        if (empty($this->data[$this->alias]['parent_id'])) {
            $this->data[$this->alias]['parent_id'] = 0;
        }
        if (empty($this->data[$this->alias]['sort'])) {
            $this->data[$this->alias]['sort'] = 0;
        }
        return parent::beforeSave();
    }

    
}

?>