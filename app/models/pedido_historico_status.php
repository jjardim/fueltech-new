<?php

class PedidoHistoricoStatus extends AppModel {

    var $name = 'PedidoHistoricoStatus';
    var $useTable = 'pedido_historico_status';
    var $displayField = 'id';
	var $actsAs = array('Containable');
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    var $belongsTo = array(        
        'Pedido' => array(
            'className' => 'Pedido',
            'foreignKey' => 'pedido_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
		'PedidoStatus' => array(
            'className' => 'PedidoStatus',
            'foreignKey' => 'pedido_status_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}

?>