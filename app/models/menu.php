<?php

class Menu extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    var $name = 'Menu';
/**
 * Behaviors used by the Model
 *
 * @var array
 * @access public
 */
    var $actsAs = array(
		'Containable',
        'Cached' => array(
            'prefix' => array(
                'link_',
                'menu_',
                'croogo_menu_',
            ),
        ),
    );
/**
 * Validation
 *
 * @var array
 * @access public
 */
    var $validate = array(
        'title' => array(
            'rule' => array('minLength', 1),
            'message' => 'Campo de preenchimento obrigatório.',
        ),
        'alias' => array(
            'isUnique' => array(
                'rule' => 'isUnique',
                'message' => 'Este Alias já exite.',
            ),
            'minLength' => array(
                'rule' => array('minLength', 1),
                'message' => 'Campo de preenchimento obrigatório.',
            ),
        ),
    );
/**
 * Model associations: hasMany
 *
 * @var array
 * @access public
 */
    var $hasMany = array(
        'Link' => array(
            'className' => 'Link',
            'foreignKey' => 'menu_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => 'Link.lft ASC',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => '',
        ),
    );
}
?>