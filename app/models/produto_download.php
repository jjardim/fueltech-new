<?php
class ProdutoDownload extends AppModel {

	public $actsAs =  array('Cached','Containable');
    var $name = 'ProdutoDownload';
    var $useTable = 'produtos_downloads';
   // var $primaryKey = 'produto_id';
   
    var $belongsTo = array(	
        'Produto' => array(
            'className' => 'Produto',
            'foreignKey' => 'produto_id',
            'conditions' => '',
            'fields' => '',
        ),		
        'Download' => array(
            'className' => 'Download',
            'foreignKey' => 'download_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );	
	
	public function salvar($data) {
		$this->deleteAll(array('produto_id' => $data['Produto']['id']), false);
		$this->primaryKey = ''; 
		$array_atributos['Downloads'] = array();
		foreach ($data[$this->alias] as $valor):
			if(isset($valor['delete']) && $valor['delete'] == true){
				continue;
			}elseif(isset($valor['download_id']) && $valor['download_id'] != ""){
				if($rest = $this->find('first', array('recursive' => -1, 'conditions' => array('DownloadProduto.download_id'=> $valor['download_id'], 'DownloadProduto.download_id' => $data['Produto']['id'])))){
					continue;
					//$this->save($valor);	
				}else{
					$this->save($valor);
					$array_atributos['Downloads'][] = $valor['download_id'];
				}
			}
        endForeach;
		return $array_atributos;
    }
}
?>