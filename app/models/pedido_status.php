<?php

class PedidoStatus extends AppModel {

    var $name = 'PedidoStatus';
    var $useTable = 'pedido_status';
    public $actsAs =  array('Cached','Containable');
	var $validate = array(
		'nome' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' 	=> 'Campo de preenchimento obrigatório.'
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
		
	);
}

?>