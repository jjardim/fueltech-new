<?php

class Vitrine extends AppModel {
    public $actsAs =  array('Cached','Containable');
    var $name = 'Vitrine';
    var $useTable = 'vitrines';
    var $displayField = 'nome';
    var $validate = array(
        'nome' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Campo de preenchimento obrigatório.'
            ),
        ),
      
        'status' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
    );
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    
    var $hasMany = array(
        'ProdutoVitrine' => array(
            'className' => 'ProdutoVitrine',
            'foreignKey' => 'vitrine_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
      
    );
  
  var $hasAndBelongsToMany = array(
        'Produto' => array(
            'className' => 'Produto',
            'joinTable' => 'produtos_vitrines',
            'foreignKey' => 'vitrine_id',
            'associationForeignKey' => 'produto_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'deleteQuery' => '',
            'insertQuery' => ''
        )
    );
    

	/**
      *  Metodo retorna as vitrines disponiveis contendo:
      *  - os produtos e imagens relativas
      *  - maior parcelamento disponivel
      *  @param int $categoria_id que gostaria de filtrar
      *  @param boolean $capa filtra vitrines que serão exibidas na capa ou não
      *  @param String $capa filtra vitrines pelo tipo
      *  @return Array
      */
    public function buscaVitrines($capa=null,$tipo=null,$categoria_id=null,$false=false){
      	App::import("helper", "Calendario");
        $this->Calendario = new CalendarioHelper();
      
		$conditions['Vitrine.status'] = true;
		if($tipo){
			$conditions['Vitrine.tipo'] = $tipo;
		}
		if(is_bool($capa)){
			$conditions['Vitrine.capa'] = $capa;
		}
		if($categoria_id){
			$conditions['Vitrine.categoria_id'] = $categoria_id;
		}
		
		$joins = 
        array(
            array(
                'table' => 'produtos_vitrines',
                'alias' => 'ProdutoVitrine',
                'type' => 'left',
                'conditions' =>array('ProdutoVitrine.vitrine_id = Vitrine.id')
            ), array(
                'table' => 'produtos',
                'alias' => 'Produto',
                'type' => 'left',
                'conditions' =>array('ProdutoVitrine.produto_id = Produto.id')
            ), array(
                'table' => 'produto_descricoes',
                'alias' => 'ProdutoDescricao',
                'type' => 'left',
                'conditions' =>array('ProdutoDescricao.produto_id = Produto.id')
            ), array(
                'table' => 'produtos_categorias',
                'alias' => 'ProdutoCategoria',
                'type' => 'left',
                'conditions' =>array('ProdutoCategoria.produto_id = ProdutoVitrine.produto_id')
            ), array(
                'table' => 'categorias',
                'alias' => 'Categoria',
                'type' => 'left',
                'conditions' =>array('ProdutoCategoria.categoria_id = Categoria.id')
            )
        );
		
        $vitrines = $this->find('all', array(	
											'order' 	=> 'ProdutoVitrine.ordem ASC',
											'group'		=> 'Produto.id',
											'recursive' => -1,
											'fields'	=> array('Vitrine.id,Vitrine.quantidade_exibida,Produto.id,ProdutoDescricao.nome,Produto.preco,Produto.preco_promocao,Produto.quantidade_disponivel,
																	Produto.status,Produto.preco_promocao_inicio,Produto.preco_promocao_fim,Produto.preco_promocao_novo,Produto.preco_promocao_velho,
																	Produto.marca, Produto.ordem,Categoria.nome,Categoria.seo_url,
																	(SELECT concat("uploads/produto_imagem/filename/",ProdutoImagem.filename) FROM produto_imagens ProdutoImagem WHERE ProdutoImagem.produto_id = Produto.id LIMIT 1) as imagem,
																	(SELECT IF(FreteGratis.label != "",FreteGratis.label,"") FROM frete_gratis FreteGratis WHERE (FreteGratis.categoria_id LIKE concat("%\"",ProdutoCategoria.categoria_id,"\"%") OR FreteGratis.produto_id LIKE concat("%\"",ProdutoCategoria.produto_id,"\"%") ) AND ( FreteGratis.status = true AND IF(Produto.preco_promocao > 0,Produto.preco_promocao,Produto.preco) >= FreteGratis.apartir_de AND FreteGratis.data_inicio <= now() AND FreteGratis.data_fim >= now() ) LIMIT 1) as frete_gratis'),
											'joins'		=> $joins,
											'conditions'=> array( am($conditions,array('Produto.quantidade_disponivel >' => 0, 'Produto.status >'=>0, 'ProdutoDescricao.language' => Configure::read('Config.language'))) )
										)
								);
		$valendo = array();
        $count = 1;
		foreach($vitrines as $chave=>$vitrine):
		      //força a quantidade de itens a serem exibidos na vitrine
            if( $vitrine['Vitrine']['quantidade_exibida'] > 0 && $count > $vitrine['Vitrine']['quantidade_exibida'])continue;
			//strtotime
			$data_inicial  	= strtotime($vitrine['Produto']['preco_promocao_inicio']);
			$data_fim 		= strtotime($vitrine['Produto']['preco_promocao_fim']);
			$data_agora 	= strtotime(date('Y-m-d H:i:s'));
			
			if( $data_agora >= $data_inicial && $data_agora <= $data_fim && (($vitrine['Produto']['preco_promocao_novo']>0) && ($vitrine['Produto']['preco_promocao_novo'] <= $vitrine['Produto']['preco']))){
				$vitrine['Produto']['preco_promocao'] = $vitrine['Produto']['preco_promocao_novo'];
				$vitrine['Produto']['preco'] = $vitrine['Produto']['preco_promocao_velho'];					
			}
			
			$valendo['vitrines']['Produto'][$chave] = $vitrine['Produto'];
			$valendo['vitrines']['Produto'][$chave]['ProdutoDescricao'] = $vitrine['ProdutoDescricao'];
			$valendo['vitrines']['Produto'][$chave]['Categoria'] = $vitrine['Categoria'];
			$valendo['vitrines']['Produto'][$chave][0] = $vitrine[0];
             $count++;
          
		endforeach;
		
		return $valendo;
    }
	
}

?>