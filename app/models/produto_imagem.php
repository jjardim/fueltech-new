<?php
class ProdutoImagem extends AppModel {
    var $name = 'ProdutoImagem';
    var $useTable = 'produto_imagens';
	var $actsAs = array('Containable','MeioUpload' =>
		array('filename'=>array('thumbsizes' => array('normal' =>
		array('width' => 800, 'height' => 600)),'uploadForTempLocal'=>true))
	);
}
?>