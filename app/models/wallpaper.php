<?php

class Wallpaper extends AppModel {

    var $name = 'Wallpaper';
    var $useTable = 'wallpapers';
    public $actsAs =  array('Cached','Containable');
	var $validate = array(
		'nome' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
    );
	
	var $hasMany = array(
        'WallpaperGrade' => array(
            'className' => 'WallpaperGrade',
            'foreignKey' => 'wallpaper_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}

?>