<?php

class Contato extends AppModel {

    var $name = 'Contato';
	public $actsAs =  array('Cached','Containable');
    var $useTable = false;
    var $validate = array(
        'nome' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
        'mensagem' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
        'cidade' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
        'estado' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
        'email' => array(
            'valido' => array(
                'rule' => array('email'),
                'message' => 'Informe um email válido, ex: example@example.com.br'
            ),
            'obrigatorio' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório.'
            )
        )
    );

    function beforeValidate(){
        if(isset($this->data['Contato']['mensagem'])){
            if($this->data['Contato']['mensagem']=="Mensagem"){
                $this->data['Contato']['mensagem'] = null;
            }
        }
    }

}

?>