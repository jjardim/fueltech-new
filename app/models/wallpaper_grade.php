<?php

class WallpaperGrade extends AppModel {

    var $name = 'WallpaperGrade';
    var $useTable = 'wallpaper_grade';
    public $actsAs =  array('MeioUpload' => array(
								'filename' => array(
										'dir' => 'uploads/wallpaper/filename', 
										'allowed_mime' => array( 'image/gif', 'image/jpeg', 'image/pjpeg', 'image/png'),
										'allowed_ext' => array('.jpg', '.jpeg', '.png', '.gif'),
										 'fields' => array(
												'filesize' => 'filesize',
												'mimetype' => 'mimetype',
												'dir' => 'dir'
											)
										)
								),'Cached','Containable');
	var $validate = array(
		'wallpaper_id' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
		'wallpaper_tamanho_id' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        )
    );
	
	var $belongsTo = array(
		'Wallpaper' => array(
			'className' => 'Wallpaper',
			'foreignKey' => 'wallpaper_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'WallpaperTamanho' => array(
			'className' => 'WallpaperTamanho',
			'foreignKey' => 'wallpaper_tamanho_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	public function deletar($id) {
		
		$this->delete(array('id' => $id));
		return true;
    }
}

?>