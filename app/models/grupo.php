<?php

class Grupo extends AppModel {
    var $name = 'Grupo';
    var $actsAs = array('Containable','Acl' => array('requester'));

    function parentNode() {
        return null;
    }

    function afterSave($created) {
        $node = $this->node();
        $aro = $node[0];
        $aro['Aro']['parent_id'] = $parent[0]['Aro']['id'];
        $aro['Aro']['alias'] = $this->data['Grupo']['nome'];
        $this->Aro->save($aro);
    }

    var $validate = array(
        'nome' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Sua mensagem de validação aqui',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Para a validação após esta regra
            //'on' => 'create', // Limitar a validação para as operações 'create' ou 'update'
            ),
        ),
    );
    // As associações abaixo foram criadas com todas as chaves possíveis, então é possível remover as que não são necessárias
    var $hasMany = array(
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'grupo_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}

?>