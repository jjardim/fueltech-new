<?php

class GaleriaFoto extends AppModel {

    var $name = 'GaleriaFoto';
    var $useTable = 'galeria_fotos';
    public $actsAs =  array(
			'MeioUpload' => array(
								'filename' => array(								
										'dir' => 'uploads/galeria/filename', 
										'allowed_mime' => array( 'image/gif', 'image/jpeg', 'image/pjpeg', 'image/png'),
										'allowed_ext' => array('.jpg', '.jpeg', '.png', '.gif'),
										'fields' => array(
												'filesize' => 'filesize',
												'mimetype' => 'mimetype',
												'dir' => 'dir'
											)
										)
								),'Cached','Containable');
	var $validate = array(
		'galera_id' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        )
    );
	var $belongsTo = array(
        'Galeria' => array(
            'className' => 'Galeria',
            'foreignKey' => 'galeria_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}

?>