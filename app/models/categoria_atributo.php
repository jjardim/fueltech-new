<?php

class CategoriaAtributo extends AppModel {
    
    public $actsAs =  array('Cached','Containable');
    var $name = 'CategoriaAtributo';
    var $useTable = 'categorias_atributos';
	
	public function salvar($data, $atributos){
		
		foreach($data['Categoria']['Categoria'] as $key => $categorias){
			foreach($atributos['Atributos'] as $atributo)
			{
				$conditions = array();
				$conditions[] = array('AND' => array('categoria_id' => $categorias));
				$conditions[] = array('AND' => array('atributo_id' => $atributo ));
				$this->primaryKey = 'atributo_id'; 
				$this->deleteAll($conditions);
				
				$new['categoria_id'] = $categorias;
				$new['atributo_id'] = $atributo;
				$this->primaryKey = ''; 
				$this->save($new);
			}
		}
	}
}