<?php

class PedidoItem extends AppModel {

    var $name = 'PedidoItem';
    var $useTable = 'pedido_itens';
    var $displayField = 'id';
	public $actsAs =  array('Cached','Containable');
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    var $belongsTo = array(
        
        'Pedido' => array(
            'className' => 'Pedido',
            'foreignKey' => 'pedido_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}

?>