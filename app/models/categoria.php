<?php


class Categoria extends AppModel {
    public $actsAs = array(
		'MeioUpload' => array(
								'thumb_filename' => array(
								
										'dir' => 'uploads/categoria/thumb', 
										'allowed_mime' => array( 'image/gif', 'image/jpeg', 'image/pjpeg', 'image/png'),
										'allowed_ext' => array('.jpg', '.jpeg', '.png', '.gif'),
										 'fields' => array(
												'filesize' => 'thumb_filesize',
												'mimetype' => 'thumb_mimetype',
												'dir' => 'thumb_dir'
											)
										)
								),'Tree', 'Containable');
    public $validate = array(
        // 'nome' => array(
            // 'notEmpty' => array(
                // 'required' => true,
                // 'rule' => 'notEmpty',
                // 'message' => 'Campo de preenchimento obrigatório',
            // ),
        // ),
		'thumb_filename' => array(
             'Empty' => array('check' => false)	
        ),
    );
	
    public $hasMany = array(
        'ProdutoCategoria' => array(
            'className' => 'ProdutoCategoria',
            'foreignKey' => 'produto_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'SubCategory' => array(
			'className' => 'Categoria',
            'order' => 'lft',
            'foreignKey' => 'parent_id',
            'dependent' => true
		),
		'CategoriaDescricao' => array(
            'className' => 'CategoriaDescricao',
            'foreignKey' => 'categoria_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
	public $belongsTo = array(
							'FatherCategory' => array(
								'className' => 'Categoria',
								'foreignKey' => 'parent_id',
								'dependent' => true
							)
						);
	 var $hasAndBelongsToMany = array(
        'CategoriaAtributo' => array(
            'className' => 'CategoriaAtributo',
            'joinTable' => 'categorias_atributos',
            'foreignKey' => 'categoria_id',
            'associationForeignKey' => 'atributo_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'deleteQuery' => '',
            'insertQuery' => ''
        ),
		'ProdutoCategoria' => array(
            'className' => 'ProdutoCategoria',
            'joinTable' => 'categorias_atributos',
            'foreignKey' => 'categoria_id',
            'associationForeignKey' => 'produto_id',
            'unique' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'deleteQuery' => '',
            'insertQuery' => ''
        )
    );
    
	

    public function beforeSave() {

        if (empty($this->data[$this->alias]['parent_id'])) {
            $this->data[$this->alias]['parent_id'] = 0;
        }
        if (empty($this->data[$this->alias]['sort'])) {
            $this->data[$this->alias]['sort'] = 0;
        }
        return parent::beforeSave();
    }

   
    public function isValidCategory($id) {
        $root_category = Configure::read('Shop.root_category');
        $valid = true;
        if (!empty($root_category)) {
            $valid = false;
            $categoryPath = $this->getpath((int) $id);
            if ($categoryPath[0]['Categoria']['id'] == $root_category) {
                $valid = true;
            }
        }
        return $valid;
    }
	
	public function sort_categorias($array, $field = 'nome', $order = SORT_ASC, $tipo)
	{
		$novo_array = array();
		$tmp_array = array();
		
		App::import('Helper','String');
		$this->String = new StringHelper();

		if (is_array($array) && count($array) > 0) {
		
			if( $tipo == 'subcategorias' ){
				foreach ($array as $k => $v) {
					if (is_array($v)) {
						foreach ($v as $k2 => $v2) {
							if ($k2 == $field) {
								$tmp_array[$k] = $this->String->remove_accent($v2);
							}
						}
					} else {
						if( $k == $field ){
							$tmp_array[$k] = $this->String->remove_accent($v);
						}
					}
				}
			}else if( $tipo == 'categorias' ){
				foreach ($array as $k => $v) {
					$tmp_array[$k] = $v['Categoria'][$field];
				}
			}
	
			switch ($order) {
				case SORT_ASC:
					asort($tmp_array);
				break;
				case SORT_DESC:
					arsort($tmp_array);
				break;
			}

			foreach ($tmp_array as $k => $v) {
				$novo_array[$k] = $array[$k];
			}
		}

		return $novo_array;
	}
	
	public function remover_thumb($id){
		//$this->id = $id;
		$categoria['id'] = $id;
		$categoria['thumb_filename'] = ' ';
		$categoria['thumb_dir'] = null;
		$categoria['thumb_mimetype'] = null;
		$categoria['thumb_filesize'] = null;		
		
		if($this->save($categoria,false))
			return true;
	}
}

?>