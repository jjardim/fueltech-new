<?php

class Parceiro extends AppModel {

    var $name = 'Parceiro';
    var $useTable = 'parceiros';
    public $actsAs =  array('MeioUpload' => array(
								'thumb_filename' => array(								
										'dir' => 'uploads/parceiro/thumb', 
										'allowed_mime' => array( 'image/gif', 'image/jpeg', 'image/pjpeg', 'image/png'),
										'allowed_ext' => array('.jpg', '.jpeg', '.png', '.gif'),
										 'fields' => array(
												'filesize' => 'thumb_filesize',
												'mimetype' => 'thumb_mimetype',
												'dir' => 'thumb_dir'
											)
										)
								),'Cached','Containable');
	var $validate = array(
		'nome' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
        'descricao_resumida' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
		'descricao_completa' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
		'thumb_filename' => array(
             'Empty' => array('check' => false)	
        ),
    );
	
	public function remover_thumb($id){
		$parceiro['id'] = $id;
		$parceiro['thumb_filename'] = ' ';
		$parceiro['thumb_dir'] = null;
		$parceiro['thumb_mimetype'] = null;
		$parceiro['thumb_filesize'] = null;		
		
		if($this->save($parceiro,false))
			return true;
	}
}

?>