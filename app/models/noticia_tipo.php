<?php

class NoticiaTipo extends AppModel {

    var $name = 'NoticiaTipo';
    var $useTable = 'noticia_tipos';
    public $actsAs =  array('Cached','Containable');
	var $validate = array(
		'nome' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        )
    );
	var $hasMany = array(
		'Noticia' => array(
			'className' => 'Noticia',
			'foreignKey' => 'noticia_tipo_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
}

?>