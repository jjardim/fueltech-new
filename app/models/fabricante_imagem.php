<?php
class FabricanteImagem extends AppModel {
    var $name = 'FabricanteImagem';
    var $actsAs = array('Containable','MeioUpload' => array('filename'));
    var $useTable = 'fabricante_imagens';
    var $belongsTo = array(
		'Fabricante' => array(
			'className' => 'Fabricante',
			'foreignKey' => 'fabricante_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
?>