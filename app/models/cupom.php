<?php

class Cupom extends AppModel {

    public $name = 'Cupom';
    public $useTable = 'cupons';
    public $actsAs = array('Containable');
    /**
	 * Variavel que determina se irá validar o cupom para inserção no carrinho de compras. somente utilizado no site.
	 * para utilização de validação no admin setar como false.
	 * @default true
	 */
	public $validaCupomSite = true;
	public $belongsTo = array(
        'CupomTipo' => array(
            'className' => 'CupomTipo',
            'foreignKey' => 'cupom_tipo_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
	

     var $validate = array(
			
         
            'categoria_id' => array(
                            'preenchido' => array(
                                            'rule' 	=> 'validateCategoria',
                                            'message' 	=> 'Seu carrinho não contém produtos da categoria de seu cupom.'
                            )
            ),
          
            'produto_id' => array(
                            'preenchido' => array(
                                            'rule' 	=> 'validateProduto',
                                            'message' 	=> 'Não contém o produto no carrinho para o cupom enviado..'
                            )
            ),
			'nome' => array(
							
                            'preenchido' => array(
											'rule' 	=> array('validaNome'),
                                            'message' => 'Preencha o campo.',
                            )
            ),
			'cupom' => array(
                            'preenchido' => array(
											'rule' 	=> array('notEmpty'),
                                            'message' => 'Preencha o campo.',
                            )
            ),
			'cupom_tipo_id' => array(
                            'preenchido' => array(
											'rule' 	=> array('notEmpty'),
                                            'message' => 'Preencha o campo.',
                            ),
							 'preenchido2' => array(
                                            'rule' 	=> 'validateFrete',
                                            'message' 	=> 'Informe o cep antes de inserir o cupom'
                            )
            ),
			'compra_minima' => array(
                            'preenchido' => array(
											'rule' 	=> array('notEmpty'),
                                            'message' => 'Preencha o campo.',
                            ),
							 'preenchido2' => array(
                                            'rule' 	=> 'validateCompraMinima',
                                            'message' 	=> 'Sua compra não atingiu o valor mínimo.'
                            )
            ),
			'max_usos' => array(
                            'preenchido' => array(
											'rule' 	=> array('notEmpty'),
                                            'message' => 'Preencha o campo.',
                            ),
							 'preenchido2' => array(
                                            'rule' 	=> 'validateNumeroMaxUsos',
                                            'message' 	=> 'Cupom Esgotado.'
                            )
            ),
			'data_fim' => array(
                            'preenchido' => array(
											'rule' 	=> array('notEmpty'),
                                            'message' => 'Preencha o campo.',
                            ),
							 'preenchido2' => array(
                                            'rule' 	=> 'validateDate',
                                            'message' 	=> 'A data de validade deste cupom expirou.'
                            )
            ),
			'vlr_desconto' => array(
									'rule' 	=> 'validaValorDesconto',
                                    'message' => 'Preencha somente um dos campos.',                            
            ),
			'pct_desconto' => array(
									'rule' 	=> 'validaPercDesconto',
									'message' => 'Preencha somente um dos campos.',                            
            )
    );
	
	/**
	* valida se o cupom já expirou
	* @return boolean
	* @uses Utilizado somente para validação na utilização no carrinho de compras
	*/
    public function validateDate($data) {
		if(!$this->validaCupomSite)return true;
        if($data['data_fim'] >= date('Y-m-d 23:59:59')) {
            return true;
        }else {
            return false;
        }
    }
	
	/**
	* valida se o numero max de usos esgotou
	* @return boolean
	* @uses Utilizado somente para validação na utilização no carrinho de compras
	*/
    public function validateNumeroMaxUsos($cupom) {
		if(!$this->validaCupomSite)return true;
		
		App::import("model", 'CupomQueimado');
        $CupomQueimado = new CupomQueimado();
		//conta quantos cupons já foram queimados com o mesmo  código
		if($this->data['Cupom']['usuario_unico']==true){
			$count = $CupomQueimado->find('count',array(
				 'joins' => array(
				array(
					'table' => 'pedidos',
					'alias' => 'Pedido',
					'type' => 'INNER',
					'conditions' => array(
						"Pedido.id = CupomQueimado.pedido_id AND Pedido.usuario_id = '".$this->Session->read('Auth.Usuario.id')."'"
					)
				)
			),
			'conditions'=>array('CupomQueimado.cupom'=> $this->data['Cupom']['cupom'])));
			//se possuir um cupom queimado com este usuário e o cupom for unico por usuário ele não valida
			if($count>0){
				
				$this->Session->setFlash('<strong>O Cupom digitado já foi utilizado por este usuário em outro pedido <br> favor utilize outro Cupom.<br> caso queira utilizar outro cupom </strong>  <a href="../carrinho"><strong style="color:#666">Clique Aqui</strong></a>', 'flash/error');
				return false;
			}
		}else{
			$count = $CupomQueimado->find('count',array('conditions'=>array('cupom'=> $this->data['Cupom']['cupom'])));
		}
	
		if($this->find('count',array('conditions'=>array('max_usos >'=>$count,'cupom'=> $this->data['Cupom']['cupom'])))>0) {
            return true;
        }else {
            return false;
        }
    }
	/**
	* valida se o valor total de itens do carrinho cobre o valor minimo de compra para ativar o cupom
	* @return boolean
	*/
    public function validateCompraMinima($cupom) {
		if(!$this->validaCupomSite)return true;
        
		if($this->find('first',array('conditions'=>array('compra_minima <=' => $this->getValorTotalCarrinho(),'cupom'=> $this->data['Cupom']['cupom'])))) {
            return true;
        }else {
            return false;
        }
    }
	
	/**
	* valida se o frete está setado antes de consultar o cupom
	* @return boolean
	* @uses Utilizado somente para validação na utilização no carrinho de compras
	*/
    public function validateFrete() {
        if(!$this->validaCupomSite)return true;
        App::import("component", "Session");
        $this->Session = new SessionComponent();
        if($this->Session->read("Carrinho.dados.prazo_entrega") > 0) {
            return true;
        }else {
            return false;
        }
    }
	/**
	* valida se algum produto do carrinho faz parte da categoria do cupom
	* @return boolean
	* @uses Utilizado somente para validação na utilização no carrinho de compras
	*/
    public function validateCategoria() {
		if(!$this->validaCupomSite)return true;
        App::import("component", "Session");
        $this->Session = new SessionComponent();
		App::import("model", "ProdutoCategoria");
        $this->ProdutoCategoria = new ProdutoCategoria();
        
        //pega as categorias dos produtos no carrinho
        foreach($this->Session->read("Carrinho.itens") as $itens) {
			$cats = $this->ProdutoCategoria->find('all',array('recursive'=>-1,'conditions'=>array('ProdutoCategoria.produto_id'=>$itens['id'])));
			foreach($cats as $cat){
				$categorias[] = $cat['ProdutoCategoria']['categoria_id'];
			}
        }

	
        if(isset($this->data['Cupom']['categoria_id'])&&$this->data['Cupom']['categoria_id']!="") {
	        $wherecategorias = array('categoria_id'=>$categorias);
        }else {
            $wherecategorias = null;
        }
	
        //verifica se existe cupom para as departamentos dos produtos no carrinho
        if(up($this->data['CupomTipo']['nome'])=='CARRINHO') {
            if($this->find('count',array('conditions'=>array($wherecategorias,'cupom'=> $this->data['Cupom']['cupom'])))>0) {
                return true;
            }else {
                return false;
            }
        }else {
            return true;
        }
    }
	
	/**
	* valida se algum produto do carrinho faz parte do produto de desconto do cupom
	* @return boolean
	* @uses Utilizado somente para validação na utilização no carrinho de compras
	*/
    public function validateProduto() {
		if(!$this->validaCupomSite)return true;
        App::import("component", "Session");
        $this->Session = new SessionComponent();
        foreach($this->Session->read("Carrinho.itens") as $itens) {
		    $id[] = $itens["id"];
        }
        if(up($this->data['CupomTipo']['nome'])=='PRODUTO') {
            if(is_array($id)) {
                if($this->find('count',array('conditions'=>array('produto_id' =>$id,'cupom' => $this->data['Cupom']['cupom'])))) {
                    return true;
                }else {
                    return false;
                }
            }else {
                return false;
            }
        }else {
            return true;
        }
    }
	
	/**
	* Realiza o calculo de do valor total do carrinho
	* @uses Utilizado somente para validação na utilização no carrinho de compras
	*/
     private function getValorTotalCarrinho(){
		if(!$this->validaCupomSite)return true;
		
        App::import("component", "Session");
        $this->Session = new SessionComponent();
        $total = 0;
        Foreach($this->Session->read("Carrinho.itens") as $valor):
			$total += ((($valor['preco_promocao']>0)?$valor['preco_promocao']:$valor['preco'])*$valor['quantidade']);
        endForeach;
        return $total;
    }
	
	
	/**
	* Realiza o calculo de do valor total do carrinho
	* @uses Utilizado somente para validação na utilização no ADMIN
	*/
     public function validaValorDesconto(){
		if($this->validaCupomSite)return true;
		if($this->data['Cupom']['vlr_desconto']!=""&&$this->data['Cupom']['pct_desconto']!="")return false;        
		
		if($this->data['Cupom']['vlr_desconto']!=""){
			if($this->data['Cupom']['vlr_desconto']!=""&&$this->data['Cupom']['pct_desconto']==""){
				return true;
			}else{
				return false;
			}	
		}elseif($this->data['Cupom']['pct_desconto']!=""){
			return true;
		}
		
		return false;
    }
	/**
	* Realiza o calculo de do valor total do carrinho
	* @uses Utilizado somente para validação na utilização no ADMIN
	*/
     public function validaPercDesconto(){
		if($this->validaCupomSite)return true;
		if($this->data['Cupom']['vlr_desconto']!=""&&$this->data['Cupom']['pct_desconto']!="")return false;
        if($this->data['Cupom']['pct_desconto']!=""){
			if($this->data['Cupom']['vlr_desconto']==""&&$this->data['Cupom']['pct_desconto']!=""){
				return true;
			}else{
				return false;
			}
		}elseif($this->data['Cupom']['vlr_desconto']!=""){
			return true;
		}
		return false;
    }	
	/**
	* Valida o preenchimento do nome
	* @uses Utilizado somente para validação na utilização no ADMIN
	*/
     public function validaNome(){
		if($this->validaCupomSite)return true;
		if(Validation::notEmpty($this->data['Cupom']['nome'])){
			return true;
		}else{
			return false;		
		}
    }	
	
	
	 public function afterFind($results, $primary = false) {
        if($this->validaCupomSite)
            return parent::afterFind($results, $primary);
        App::import("helper", "String");
        $string = new StringHelper();
        if (!empty($results)) {
            foreach ($results as $k => &$r) {

                if (isset($r[$this->alias]) && isset($r[$this->alias]['compra_minima'])) {
                    $r[$this->alias]['compra_minima'] = $string->bcoToMoeda($r[$this->alias]['compra_minima']);
                }
				if (isset($r[$this->alias]) && isset($r[$this->alias]['vlr_desconto'])) {
					if($r[$this->alias]['vlr_desconto']>0){
						$r[$this->alias]['vlr_desconto'] = $string->bcoToMoeda($r[$this->alias]['vlr_desconto']);
					}else{
						$r[$this->alias]['vlr_desconto'] = null;
					}
                }
				if (isset($r[$this->alias]) && isset($r[$this->alias]['pct_desconto'])) {
					if($r[$this->alias]['pct_desconto']>0){
						$r[$this->alias]['pct_desconto'] = $string->bcoToMoeda($r[$this->alias]['pct_desconto']);
					}else{
						$r[$this->alias]['pct_desconto'] = null;
					}
                }
				if (isset($r[$this->alias]) && isset($r[$this->alias]['data_fim'])) {
					list($ano,$mes,$dia) = explode('-',current(explode(' ',$r[$this->alias]['data_fim'])));
					$r[$this->alias]['data_fim'] = "$dia/$mes/$ano";
                }
            }
        }

        return parent::afterFind($results, $primary);
    }

	public function beforeSave() {
	
        App::import("helper", "String");
        $string = new StringHelper();
	
        if (isset($this->data[$this->alias]['compra_minima'])) {
            $this->data[$this->alias]['compra_minima'] = $string->moedaToBco($this->data[$this->alias]['compra_minima']);
        }
        if (isset($this->data[$this->alias]['vlr_desconto'])) {
            $this->data[$this->alias]['vlr_desconto'] = $string->moedaToBco($this->data[$this->alias]['vlr_desconto']);
        }
		if (isset($this->data[$this->alias]['pct_desconto'])) {
            $this->data[$this->alias]['pct_desconto'] = $string->moedaToBco($this->data[$this->alias]['pct_desconto']);
        }
		
		if (isset($this->data[$this->alias]['data_fim'])) {
			list($dia,$mes,$ano) = explode('/',$this->data[$this->alias]['data_fim']);
			$this->data[$this->alias]['data_fim'] = "$ano-$mes-$dia 23:59:59";
        }	      

        return parent::beforeSave();
    }
	
	
	
}