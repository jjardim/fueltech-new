<?php

class Xml extends AppModel {

    var $name = 'Xml';
    var $useTable = 'xml';
    public $actsAs =  array('Cached','Containable');
	var $validate = array(
		'nome' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
        'template_cabecalho' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
		'template_centro' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
		'template_rodape' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        )
    );
	
	public $belongsTo = array(
		'XmlTipo' => array(
			'className' => 'XmlTipo',
			'foreignKey' => 'xml_tipo_id'
		)
	);
	
	public function beforeSave() {
		
		if (isset($this->data[$this->alias]['nome'])) {
			$this->data[$this->alias]['url'] = low(Inflector::slug($this->data[$this->alias]['nome'], '-')).'_'.date("d_m_Y_H_i_s");
        }
		
		if (isset($this->data[$this->alias]['produtos_ids']) && !empty($this->data[$this->alias]['produtos_ids'])) {
            $this->data[$this->alias]['produtos_ids'] = json_encode($this->data[$this->alias]['produtos_ids']);
        }else{
			$this->data[$this->alias]['produtos_ids'] = '';
		}
		
        return parent::beforeSave();
    }
	
	// public function afterFind($results, $primary = false) {
		// if ($this->isCount($results))
			// return parent::afterFind($results, $primary);
		
		// App::import("Model","Produto");
		// $this->Produto = new Produto();
		// if (!empty($results)) {
			// foreach ($results as $k => &$r) {
                
				// if (isset($r[$this->alias]) && isset($r[$this->alias]['produtos_ids']) && !empty($r[$this->alias]['produtos_ids'])) {
					// $r[$this->alias]['produtos_ids'] = $this->Produto->find('list',array('fields'=>array('sku','nome'),'limit'=>100,'conditions'=>array('Produto.id'=>json_decode($r[$this->alias]['produtos_ids']))));;
				// }
			// }
		// }

		// return parent::afterFind($results, $primary);
	// }

}

?>