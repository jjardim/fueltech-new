<?php

class Pagina extends AppModel {

    var $name = 'Pagina';
    var $useTable = 'paginas';
    public $actsAs = array('Cached', 'Containable');
    var $hasMany = array(
	'SubPage' => array(
	    'className' => 'Pagina',
	    'order' => 'lft',
	    'foreignKey' => 'parent_id',
	    'dependent' => true
	),
	'PaginaDescricao' => array(
	    'className' => 'PaginaDescricao',
	    'foreignKey' => 'pagina_id',
	    'conditions' => '',
	    'fields' => '',
	    'order' => ''
	)
    );

    public function beforeSave() {
	if (!isset($this->data[$this->alias]['nome']) || empty($this->data[$this->alias]['nome'])) {
	    $this->data[$this->alias]['nome'] = $this->data['PaginaDescricao'][0]['nome'];
	}
	return parent::beforeSave();
    }

}

?>