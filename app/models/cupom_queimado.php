<?php

class CupomQueimado extends AppModel {

    public $name = 'CupomQueimado';
    public $useTable = 'cupom_queimados';
    public $actsAs = array('Containable');
    var $hasMany = array(
        'Cupom' => array(
            'className' => 'Cupom',
            'foreignKey' => 'cupom_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
	);
}