<?php
class AtributoDescricao extends AppModel {
    var $name = 'AtributoDescricao';
    var $actsAs = array('Containable');
    var $useTable = 'atributo_descricoes';
    var $belongsTo = array(
		'Atributo' => array(
			'className' => 'Atributo',
			'foreignKey' => 'atributo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
?>