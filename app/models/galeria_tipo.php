<?php

class GaleriaTipo extends AppModel {

    var $name = 'GaleriaTipo';
    var $useTable = 'galeria_tipos';
    public $actsAs =  array('Cached','Containable');
	var $validate = array(
		'nome' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        )
    );
	
	var $hasMany = array(
		'Galeria' => array(
			'className' => 'Galeria',
			'foreignKey' => 'galeria_tipo_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
}

?>