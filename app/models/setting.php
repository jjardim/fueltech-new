<?php
/**
 * Setting
 *
 * PHP version 5
 *
 * @category Model
 * @version  1.0
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 */
class Setting extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    var $name = 'Setting';
/**
 * Behaviors used by the Model
 *
 * @var array
 * @access public
 */
    var $actsAs = array(
        // 'Ordered' => array(
            // 'field' => 'weight',
            // 'foreign_key' => false,
        // ),
        'Cached' => array(
            'prefix' => array(
                'setting_',
            ),
        ),
    );
/**
 * Validation
 *
 * @var array
 * @access public
 */
    var $validate = array(
        'key' => array(
            'isUnique' => array(
                'rule' => 'isUnique',
                'message' => 'Esta chave já existe, utilize outra',
            ),
            'minLength' => array(
                'rule' => array('minLength', 1),
                'message' => 'Campo de preenchimento obrigatório.',
            ),
        ),
    );
/**
 * afterSave callback
 *
 * @return void
 */
    function afterSave() {
        $this->writeConfiguration();
    }
/**
 * afterDelete callback
 *
 * @return void
 */
    function afterDelete() {
        $this->writeConfiguration();
    }
/**
 * Creates a new record with key/value pair if key does not exist.
 *
 * @param string $key
 * @param string $value
 * @param array $options
 * @return boolean
 */
    function write($key, $value, $options = array()) {
        $_options = array(
            'editable' => 0,
        );
        $options = array_merge($_options, $options);

        $setting = $this->findByKey($key);
        if (isset($setting['Setting']['id'])) {
            $setting['Setting']['id'] = $setting['Setting']['id'];
            $setting['Setting']['value'] = $value;
            $setting['Setting']['editable'] = $options['editable'];
        } else {
            $setting = array();
            $setting['key'] = $key;
            $setting['value'] = $value;
            $setting['editable'] = $options['editable'];
        }

        $this->id = false;
        if ($this->save($setting)) {
            Configure::write($key, $value);
            return true;
        } else {
            return false;
        }
    }
/**
 * Deletes setting record for given key
 *
 * @param string $key
 * @return boolean
 */
    function deleteKey($key) {
        $setting = $this->findByKey($key);
        if (isset($setting['Setting']['id']) &&
            $this->delete($setting['Setting']['id'])) {
            return true;
        }
        return false;
    }
/**
 * All key/value pairs are made accessible from Configure class
 *
 * @return void
 */
    function writeConfiguration() {
	
		if (Cache::read('settings') === false) {
			$settings = $this->find('all', array(
            'fields' => array(
                'Setting.key',
                'Setting.value',
            ),
            'cache' => array(
                'name' => 'setting_write_configuration',
                'config' => 'setting_write_configuration',
            ),
        ));
			Cache::write('settings', $settings);
		}
		else{
			$settings = Cache::read('settings');
		}
	
      
        foreach($settings AS $setting) {
            Configure::write($setting['Setting']['key'], $setting['Setting']['value']);
        }
    }
}
?>