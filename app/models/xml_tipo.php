<?php

class XmlTipo extends AppModel {

    var $name = 'XmlTipo';
    var $useTable = 'xml_tipos';
    public $actsAs =  array('Cached','Containable');
	var $validate = array(
		'nome' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        )
    );
}

?>