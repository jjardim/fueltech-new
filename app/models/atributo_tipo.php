<?php
class AtributoTipo extends AppModel {
	var $name = 'AtributoTipo';
	public $actsAs =  array('Containable');

	var $displayField = 'nome';
	var $validate = array(
		'nome' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' 	=> 'Campo de preenchimento obrigatório.'
			),
			'unique' => array(
				'rule' => array('checkUnique','nome'),
				'message' 	=> 'Atributo já adicionado, tente outro nome.'
			),
		),
	);
	
	var $hasMany = array(
		'Atributo' => array(
			'className' => 'Atributo',
			'foreignKey' => 'atributo_tipo_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
?>