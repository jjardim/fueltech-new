<?php

class CarrinhoAbandonado extends AppModel {

    var $name = 'CarrinhoAbandonado';
    var $useTable = 'carrinho_abandonado';
    public $actsAs =  array('Cached','Containable');
	var $validate = array(
		'produtos' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        )
    );
	var $belongsTo = array(
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'usuario_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
	);
	
	public function salvar($data, $id = null){
		//itens carrinho
		App::import("component", "Session");
		$this->Session = new SessionComponent();
		
		$carrinho_abandonado = array();
		if(isset($data['produtos'])){
			foreach($data['produtos'] as $key => $abandonado)
			{
				$abandonado_temp = unserialize($abandonado);
				$carrinho_abandonado[$key]['produto_id'] =  $abandonado_temp['produto_id'];
				$carrinho_abandonado[$key]['quantidade'] =  $abandonado_temp['quantidade'];
			}		
			$data['produtos'] = json_encode($carrinho_abandonado);
		}
		
		//seto id se tiver
		if( $id != null && $id > 0 ) 
			$this->id = $id;
		
		$carrinho_abandonado_id = array();
		if($this->save($data)){
			//id
			$carrinho_abandonado_id = $this->id;
			
			//add o key pedidos aabandonados
			$this->Session->write("Carrinho.carrinho_abandonado", $carrinho_abandonado_id);
		}
	}
	
	public function remover($id = null){
		$this->delete($id);
	}
}

?>