<?php

class FaqPergunta extends AppModel {

    var $name = 'FaqPergunta';
    var $useTable = 'faq_perguntas';
    public $actsAs =  array('Cached','Containable');
	var $validate = array(
		'faq_id' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
		'pergunta' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
    );
	
	var $belongsTo = array(
		'Faq' => array(
			'className' => 'Faq',
			'foreignKey' => 'faq_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	public $hasMany = array(
		'FaqPerguntaDescricao' => array(
            'className' => 'FaqPerguntaDescricao',
            'foreignKey' => 'faq_pergunta_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}

?>