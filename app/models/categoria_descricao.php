<?php
class CategoriaDescricao extends AppModel {
    var $name = 'CategoriaDescricao';
    var $actsAs = array('Containable');
    var $useTable = 'categoria_descricoes';
    var $belongsTo = array(
		'Categoria' => array(
			'className' => 'Categoria',
			'foreignKey' => 'categoria_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
?>