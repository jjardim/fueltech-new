<?php

class PedidoHistoricoTracking extends AppModel {

    var $name = 'PedidoHistoricoTracking';
    var $useTable = 'pedido_historico_tracking';
    var $displayField = 'id';
	var $actsAs = array('Containable');
     var $validate = array(
        'pedido_id' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.',
            'required' => true,
        ),
        'tracking' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.',
            'required' => true,
        )
    );
	
	//The Associations below have been created with all possible keys, those that are not needed can be removed
    var $belongsTo = array(
        
        'Pedido' => array(
            'className' => 'Pedido',
            'foreignKey' => 'pedido_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}

?>