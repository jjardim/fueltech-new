<?php
class Fabricante extends AppModel {
	var $name = 'Fabricante';
	var $actsAs = array('Cached','Containable');
	var $displayField = 'nome';
	var $validate = array(
						'nome' => array(
									'rule'      => array('notEmpty'),
									'message'   => 'Campo de preenchimento obrigatório.'
								)
					);
    var $hasMany = array(
        'FabricanteImagem' => array(
            'className' => 'FabricanteImagem',
            'foreignKey' => 'fabricante_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
        
}
?>