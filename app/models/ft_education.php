<?php

class FtEducation extends AppModel {

    var $name = 'FtEducation';
    var $useTable = 'ft_educations';

    public $validate = array(
        'nome' => array(
            'rule' => 'notEmpty',
            'message' => 'Campo de preenchimento obrigatório.',
        ),
        'cpf' => array(
            'rule' => 'validar_cpf',
            'message' => 'Informe um CPF válido.',
        ),
        'email' => array(
            'rule' => 'email',
            'message' => 'Informe um e-mail válido.',
        ),
        'endereco' => array(
            'rule' => 'notEmpty',
            'message' => 'Campo de preenchimento obrigatório.',
        ),
        'cidade' => array(
            'rule' => 'notEmpty',
            'message' => 'Campo de preenchimento obrigatório.',
        ),
        'estado' => array(
            'rule' => 'notEmpty',
            'message' => 'Campo de preenchimento obrigatório.',
        ),
        'telefone' => array(
            'rule' => 'notEmpty',
            'message' => 'Campo de preenchimento obrigatório.',
        ),
    );

    public function validar_cpf($check) {
        $check = trim(array_shift($check));

        // sometimes the user submits a masked CNPJ
        if (preg_match('/^\d\d\d.\d\d\d.\d\d\d\-\d\d/', $check)) {
            $check = str_replace(array('-', '.', '/'), '', $check);
        } else if (!ctype_digit($check)) {
            return false;
        }

        if (strlen($check) != 11) {
            return false;
        }

        // repeated values are invalid, but algorithms fails to check it
        for ($i = 0; $i < 10; $i++) {
            if (str_repeat($i, 11) === $check) {
                return false;
            }
        }

        $dv = substr($check, -2);
        for ($pos = 9; $pos <= 10; $pos++) {
            $sum = 0;
            $position = $pos + 1;
            for ($i = 0; $i <= $pos - 1; $i++, $position--) {
                $sum += $check[$i] * $position;
            }
            $div = $sum % 11;
            if ($div < 2) {
                $check[$pos] = 0;
            } else {
                $check[$pos] = 11 - $div;
            }
        }
        $dvRight = $check[9] * 10 + $check[10];

        return ($dvRight == $dv);
    }

    public function validar_cnpj($check) {
        $check = trim(array_shift($check));
        // sometimes the user submits a masked CNPJ
        if (preg_match('/^\d\d.\d\d\d.\d\d\d\/\d\d\d\d\-\d\d/', $check)) {
            $check = str_replace(array('-', '.', '/'), '', $check);
        } elseif (!ctype_digit($check)) {
            return false;
        }

        if (strlen($check) != 14) {
            return false;
        }
        $firstSum = ($check[0] * 5) + ($check[1] * 4) + ($check[2] * 3) + ($check[3] * 2) +
                ($check[4] * 9) + ($check[5] * 8) + ($check[6] * 7) + ($check[7] * 6) +
                ($check[8] * 5) + ($check[9] * 4) + ($check[10] * 3) + ($check[11] * 2);

        $firstVerificationDigit = ($firstSum % 11) < 2 ? 0 : 11 - ($firstSum % 11);

        $secondSum = ($check[0] * 6) + ($check[1] * 5) + ($check[2] * 4) + ($check[3] * 3) +
                ($check[4] * 2) + ($check[5] * 9) + ($check[6] * 8) + ($check[7] * 7) +
                ($check[8] * 6) + ($check[9] * 5) + ($check[10] * 4) + ($check[11] * 3) +
                ($check[12] * 2);

        $secondVerificationDigit = ($secondSum % 11) < 2 ? 0 : 11 - ($secondSum % 11);

        return ($check[12] == $firstVerificationDigit) && ($check[13] == $secondVerificationDigit);
    }
    
    public function validar_cep($check)
    {
        $check = array_shift($check);
        return (bool)preg_match('/^[0-9]{5}-?[0-9]{3}$/', $check);
    }
}

?>