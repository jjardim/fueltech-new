<?php

class AtributoProduto extends AppModel {

    public $actsAs = array('Cached', 'Containable');
    var $name = 'AtributoProduto';
    var $useTable = 'atributos_produtos';
    //var $primaryKey = 'atributo_id';
    var $belongsTo = array(
        'Atributo' => array(
            'className' => 'Atributo',
            'foreignKey' => 'atributo_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    public function salvar($data) {
        $this->deleteAll(array('produto_id' => $data['Produto']['id']), false);
        //$this->primaryKey = '';
        $array_atributos['Atributos'] = array();
        foreach ($data[$this->alias] as $valor):
            $valor['id'] = '';
            if (isset($valor['delete']) && $valor['delete'] == true) {
                continue;
            } elseif (isset($valor['atributo_id']) && $valor['atributo_id'] != "") {
                if ($rest = $this->find('first', array('recursive' => -1, 'conditions' => array('AtributoProduto.atributo_id' => $valor['atributo_id'], 'AtributoProduto.produto_id' => $data['Produto']['id'])))) {
                    //$this->save($valor);
                    continue;
                }else{
                    $this->save($valor);
                    $array_atributos['Atributos'][] = $valor['atributo_id'];
                }
            }
        endForeach;
        return $array_atributos;
    }

}