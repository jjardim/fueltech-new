<?php

class ProdutoCategoria extends AppModel {
    
    public $actsAs =  array('Cached','Containable');
	var $primaryKey = 'categoria_id';
    var $name = 'ProdutoCategoria';
    var $useTable = 'produtos_categorias';
	
	
	 var $belongsTo = array(	
        'Produto' => array(
            'className' => 'Produto',
            'foreignKey' => 'produto_id',
            'conditions' => '',
            'fields' => '',
        ),		
        'Categoria' => array(
            'className' => 'Categoria',
            'foreignKey' => 'categoria_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );	
}