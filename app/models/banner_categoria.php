<?php

class BannerCategoria extends AppModel {
    
    public $actsAs =  array('Containable');
    var $primaryKey = 'banner_id';
    var $name = 'BannerCategoria';
    var $useTable = 'banners_categorias';
    var $displayField = 'nome';     

}

?>