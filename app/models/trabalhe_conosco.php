<?php

class TrabalheConosco extends AppModel {

    public $actsAs =  array('Cached', 'Containable', 'Validacao',
                            'MeioUpload' => array(
                                'filename' => array(
                                'dir' => 'uploads/trabalhe_conosco',
                                'create_directory' => true,
                                      'allowedMime' => array('application/pdf', 'application/msword', 'application/mspowerpoint', 'application/excel', 'application/rtf', 'application/zip, application/x-compressed-zip', 'application/octet-stream', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'),
                                    'allowedExt' => array('.pdf', '.doc', '.docx', '.xps', '.txt', '.ppt', '.xls', '.rtf', '.zip', '.odt'),
                                    'fields' => array(
                                        'filesize' => 'filesize',
                                        'mimetype' => 'mimetype',
                                        'dir' => 'dir'
                                    )
                                )
                            )
        );


    var $name = 'TrabalheConosco';
    var $useTable = 'sac';

    var $validate = array(
        'nome' => array(
            'rule' => array('validaCampos'),
            'message' => 'Campo de preenchimento obrigatório.',
            'required' => true
        ),

        'telefone' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.',
            'required' => true
        ),

        'email' => array(
            'valido' => array(
                'rule' => array('email'),
                'message' => 'Informe um email válido, ex: example@example.com.br',
            ),
            'obrigatorio' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório.',
                'required' => true
            )
        ),

        'cpf' => array(
            'valido' => array(
                'rule' => array('cpf'),
                'message' => 'O campo cpf não contem um CPF válido'
            ),
            'preenchido' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório',
                'required' => true
            )
        ),
    );


    function beforeValidate(){
		if(isset($this->data['Sac']['nome'])){
            if($this->data['Sac']['nome']=="Nome*"){
                $this->data['Sac']['nome'] = null;
            }
        }
		if(isset($this->data['Sac']['email'])){
            if($this->data['Sac']['email']=="E-mail*"){
                $this->data['Sac']['email'] = null;
            }
        }
        if(isset($this->data['Sac']['mensagem'])){
            if($this->data['Sac']['mensagem']=="Telefone*"){
                $this->data['Sac']['mensagem'] = null;
            }
        }
		if(isset($this->data['Sac']['cpf'])){
            if($this->data['Sac']['cpf']=="CNPJ*"){
                $this->data['Sac']['cpf'] = null;
            }
        }
        if (isset($this->data[$this->alias]['telefone'])) {
            $this->data[$this->alias]['telefone'] = preg_replace("/[^0-9]/", "", $this->data[$this->alias]['telefone']);
        }
    }

    function validaCampos($data){
        $attributeValue = null;

        foreach ($data as $key=>$value) {
            $attributeValue = $value;
        }

        return empty($attributeValue) ? false : true;
    }

}

?>