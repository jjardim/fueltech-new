<?php

class Revendedor extends AppModel {

    var $name = 'Revendedor';
    var $useTable = 'revendedores';
    public $actsAs =  array('MeioUpload' => array(
								'thumb_filename' => array(								
										'dir' => 'uploads/revendedor/thumb', 
										'allowed_mime' => array( 'image/gif', 'image/jpeg', 'image/pjpeg', 'image/png'),
										'allowed_ext' => array('.jpg', '.jpeg', '.png', '.gif'),
										 'fields' => array(
												'filesize' => 'thumb_filesize',
												'mimetype' => 'thumb_mimetype',
												'dir' => 'thumb_dir'
											)
										)
								),'Cached','Containable');
	var $validate = array(
		'nome' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
        'descricao_resumida' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
		'descricao_completa' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
		'thumb_filename' => array(
             'Empty' => array('check' => false)	
        ),
    );
	
	public function remover_thumb($id){
		$revendedor['id'] = $id;
		$revendedor['thumb_filename'] = ' ';
		$revendedor['thumb_dir'] = null;
		$revendedor['thumb_mimetype'] = null;
		$revendedor['thumb_filesize'] = null;		
		
		if($this->save($revendedor,false))
			return true;
	}
}

?>