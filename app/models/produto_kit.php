<?php

class ProdutoKit extends AppModel {
    
    public $actsAs =  array('Cached','Containable');
	var $primaryKey = 'produto_id';
    var $name = 'ProdutoKit';
    var $useTable = 'produtos_kits';
	
	var $validate = array(
		'quantidade' => array(
            'noempty' => array(
                'rule' => array('numeric'),
                'message' => 'Preenchimento obrigatório.'
            ),
            'validaQuantidade' => array(
                'rule' => array('validaQuantidade'),
                'message' => 'O valor deve ser superior a 0.'
            ),
			'validaEstoque' => array(
                'rule' => array('validaEstoque'),
                'message' => 'Quantidade indisponível.'
            )
        ),
	);
	
	var $belongsTo = array(	
        'Produto' => array(
            'className' => 'Produto',
            'foreignKey' => 'produto_id',
            'conditions' => '',
            'fields' => '',
        )
	);
	
	function validaQuantidade($valor) {
        if ($valor['quantidade'] <= 0) {
            return false;
        }
        return true;
    }
	
	function validaEstoque($valor) {
		App::import('Model','Produto');
		$this->Produto = new Produto();
		
		$prod_kit_tmp = $this->Produto->find('first', array('recursive' => -1, 'fields' => array('Produto.quantidade_disponivel'), 'conditions' => array('Produto.id' => $this->data['ProdutoKit']['produto_id'])));
		
        if ( $prod_kit_tmp['Produto']['quantidade_disponivel'] <  $valor['quantidade'] ) {
            return false;
        }
        return true;
    }
	
	public function soma_valores(&$data){

		App::import('Model','Produto');
		$this->Produto = new Produto();
		
		App::import('Helper', 'String');
		$this->String = new StringHelper();
	
		$largura 				= 0;
        $altura 				= 0;
        $profundidade	 		= 0;
        $peso                   = 0;
        $peso_ramos             = 0;
        $preco_total            = 0;
        $preco_total_real       = 0;
        $codigo_erp 			= '';
		
		if(isset($data['ProdutoKit']) && is_array($data['ProdutoKit']) && count($data['ProdutoKit']) > 0){
			
			$kit_produtos = array();
			
			$first = true;
			
			foreach ($data['ProdutoKit'] as $k => $i) {

				if (isset($i['delete']) && $i['delete'] == true) {
					continue;
				}else{
					$prd = $this->Produto->find('first', array(
																'recursive' 	=> -1,
																'conditions' 	=> array('Produto.id' => $i['produto_id']),
																'fields'		=> array('Produto.id', 'Produto.nome', 'Produto.sku', 'Produto.codigo_erp', 'Produto.preco_promocao', 'Produto.preco_promocao_inicio', 'Produto.preco_promocao_fim', 'Produto.preco_promocao_novo', 'Produto.peso', 'Produto.peso_ramos', 'Produto.profundidade', 'Produto.largura', 'Produto.altura', '(SELECT concat("uploads/produto_imagem/filename/",ProdutoImagem.filename) FROM produto_imagens as ProdutoImagem WHERE ProdutoImagem.produto_id = Produto.id LIMIT 1) as imagem'),
															)
												);

					if($prd['Produto']['profundidade'] > $profundidade){
						$profundidade	        = $this->String->bcoToMoeda($prd['Produto']['profundidade']);
					}
					if($prd['Produto']['largura'] > $largura){
						$largura	           	= $this->String->bcoToMoeda($prd['Produto']['largura']);
					}
					$altura		                += $this->String->bcoToMoeda($prd['Produto']['altura']*$i['quantidade']);
					$peso                       += $prd['Produto']['peso']*$i['quantidade'];
					$peso_ramos                 += $prd['Produto']['peso_ramos']*$i['quantidade'];

					$preco_total                += $this->String->moedaToBco($i['preco'])*$i['quantidade'];
					$preco_total_real			+= $this->String->moedaToBco($i['preco_real'])*$i['quantidade'];

					if($first){
						$first = false;
						$codigo_erp = $prd['Produto']['codigo_erp'];
					}else{
						$codigo_erp .= ' + '.$prd['Produto']['codigo_erp'];
					}

					$prd['Produto']['preco'] 		= $i['preco'];
					$prd['Produto']['preco_real'] 	= $i['preco_real'];
					$prd['Produto']['quantidade'] 	= $i['quantidade'];
					$kit_produtos[] = $prd;
				}
			}


            if( ($preco_total_real != $preco_total) &&  ($preco_total < $preco_total_real) ){
				$data['Produto']['preco'] 			=  $this->String->moedaToBco($preco_total_real);
				$data['Produto']['preco_promocao'] 	=  $this->String->moedaToBco($preco_total);
			}else{
				$data['Produto']['preco'] 			=  $this->String->moedaToBco($preco_total);
				$data['Produto']['preco_promocao'] 	=  0;
			}

			$data['Produto']['peso'] 			= $peso;
			$data['Produto']['peso_ramos'] 		= $peso_ramos;
			$data['Produto']['altura'] 			= $altura;
			$data['Produto']['largura'] 		= $largura;
			$data['Produto']['profundidade'] 	= $profundidade;
			//$data['Produto']['codigo_erp'] 		= $codigo_erp;
			$data['Produto']['kit_produtos']	= json_encode($kit_produtos);
			
			return $data;
		}
		
	}
	
	public function salvar($data, $produto_id){
		App::import('Helper', 'String');
		$this->String = new StringHelper();
		
		if($produto_id != ""){
			$this->query("DELETE FROM produtos_kits WHERE kit_id = '".(int)$produto_id."'");
	    }
		$this->primaryKey = '';
		if(is_array($data) && count($data) > 0){
			foreach ($data[$this->alias] as $xx => $valor):
				$valor['id'] = '';
				$valor['kit_id'] = $produto_id;
				
				if (isset($valor['delete']) && $valor['delete'] == true) {
					continue;
				} elseif (isset($valor['produto_id']) && $valor['produto_id'] != "") {
					if ($this->find('count', array('recursive' => -1, 'conditions' => array('AND' => array('ProdutoKit.produto_id' => $valor['produto_id'], 'ProdutoKit.kit_id' => $valor['kit_id'])))) > 0) {
						continue;
					} else {
						$this->query("INSERT INTO produtos_kits (kit_id, produto_id, preco, preco_real, quantidade) VALUES ('".(int)$valor['kit_id']."', '".(int)$valor['produto_id']."', '".$this->String->moedaToBco($valor['preco'])."', '".$this->String->moedaToBco($valor['preco_real'])."', '".(int)$valor['quantidade']."')");
					}
				}
			endForeach;
		}
	}
	
	public function remove_selecionados($data){
		if(isset($data['ProdutoKit']) && is_array($data['ProdutoKit']) && count($data['ProdutoKit']) > 0){
			foreach ($data['ProdutoKit'] as $xx => $valor):
				if (isset($valor['delete']) && $valor['delete'] == true) {
					unset($data['ProdutoKit'][$xx]);
				}
			endForeach;
		}
	}
	
	//metodo que valida se todos os produtos do kit possuem estoque
	public function valida_estoque($produto_id){
		if(!$this->_valida_estoque_kit($produto_id)){
			$this->Produto->updateAll(array('Produto.status' => 2), array('Produto.id' => $produto_id));
		}else{
			$this->Produto->updateAll(array('Produto.status' => 1), array('Produto.id' => $produto_id));
		}
	}
	
	//metodo que desativa os KIT que o produto faz parte
	public function desativa_kit_sem_estoque($produto_id = null, $codito_erp = null){
		App::import('Model','Produto');
		$this->Produto = new Produto();
	
		if($produto_id == null && $codito_erp != null){
			$produto = $this->Produto->find('first', array('recursive' => -1, 'fields' => array('Produto.id'), 'conditions' => array('Produto.codigo_erp' => $codito_erp)));
			if($produto){
				$produto_id = $produto['Produto']['id'];
			}
		}
		
		$produtos_kits = $this->find('all', array('conditions' => array('ProdutoKit.produto_id' => $produto_id)));		
		
		if($produtos_kits){
			foreach($produtos_kits as $produto_kit){
				$this->Produto->updateAll(array('Produto.status' => 2), array('Produto.id' => $produto_kit['ProdutoKit']['kit_id']));
			}
		}
	}
	
	//metodo que ativa os KIT que o produto faz parte caso os mesmo possuam estoque suficiente em todos os produtos
	public function ativa_kit_com_estoque($produto_id = null, $codito_erp = null){
		App::import('Model','Produto');
		$this->Produto = new Produto();
		
		if($produto_id == null && $codito_erp != null){
			$produto = $this->Produto->find('first', array('recursive' => -1, 'fields' => array('Produto.id'), 'conditions' => array('Produto.codigo_erp' => $codito_erp)));
			if($produto){
				$produto_id = $produto['Produto']['id'];
			}
		}
		
		if($produto_id == null){
			return false;
		}
		
		$produtos_kits = $this->find('all', array('recursive' => -1, 'conditions' => array('ProdutoKit.produto_id' => $produto_id)));		
		
		if($produtos_kits){
			foreach($produtos_kits as $produto_kit){
				$prd = $this->Produto->find('first', array(
															'recursive' => -1, 
															'fields'	=> array('Produto.id', 'Produto.quantidade_disponivel'),
															'conditions' => array('Produto.id' => $produto_kit['ProdutoKit']['kit_id']) 
														)
											);
				
				if($prd){
					if($this->_valida_estoque_kit($prd['Produto']['id'])){
						if($prd['Produto']['quantidade_disponivel'] > 0){
							$this->Produto->updateAll(array('Produto.status' => 1), array('Produto.id' => $prd['Produto']['id']));
						}
					}
				}
			}
		}
	}
        
        
        //atualiza o preco dos produtos a partir um ID
        public function atualiza_preco_by_id($produto_id, $preco) {
            
            App::import('Model', 'ProdutoKit');
            $this->ProdutoKit = new ProdutoKit();
            
            if ($produto_id == null) {
                return false;
            }
            
            $this->ProdutoKit->updateAll(array('ProdutoKit.preco' => $preco), array('ProdutoKit.produto_id' => $produto_id));
            
        }
	
	//metodo que atualiza a soma de valores do kit
	public function atualiza_soma_valores($produto_id, $data, $update = true){
			
		App::import('Helper', 'String');
		$this->String = new StringHelper();
		
		if($update == true && $data != null){
			//atualiza os valores do produto na tabela de kit
			$this->updateAll(array('ProdutoKit.preco_real' => $this->String->moedaToBco($data['Produto']['preco'])), array('ProdutoKit.produto_id' => $produto_id));
			
			//obtem os kits que o produto em questão faz parte		
			$produtos_kits = $this->find('all',array(
													'recursive' => -1, 
													'fields' => array('ProdutoKit.kit_id'), 
													'conditions' => array('ProdutoKit.produto_id' => $produto_id)
												)
										);
		}else{
			//obtem os kits que o produto em questão faz parte		
			$produtos_kits[0]['ProdutoKit']['kit_id'] = $produto_id;
		}	
		
		//quem kit?
		if($produtos_kits){
		
			//percorre os kits, para validar seus produtos
			foreach($produtos_kits as $produto_kit){

				$largura 				= 0;
				$altura 				= 0;
				$profundidade	 		= 0;
				$peso                   = 0;
				$peso_ramos             = 0;
				$preco_total            = 0;
				$preco_total_real       = 0;
				$codigo_erp				= '';
				
				//obtem todos os produtos do kit em questão
				$produtos_do_kit = $this->find('all', array('recursive' => -1, 'conditions' => array('ProdutoKit.kit_id' => $produto_kit['ProdutoKit']['kit_id'])));
				
				//tem produtos?
				if($produtos_do_kit){
					
					$first = true;
					
					//percorre os produtos e vai somando seus respectivo valores
					foreach($produtos_do_kit as $produto_do_kit){
						
						//obtem dados do produto
						$prd = $this->Produto->find('first', array(
																'recursive' 	=> -1, 
																'conditions' 	=> array('Produto.id' => $produto_do_kit['ProdutoKit']['produto_id']),
																'fields'		=> array('Produto.id', 'Produto.nome', 'Produto.sku', 'Produto.codigo_erp', 'Produto.preco_promocao', 'Produto.preco_promocao_inicio', 'Produto.preco_promocao_fim', 'Produto.preco_promocao_novo', 'Produto.peso', 'Produto.peso_ramos', 'Produto.profundidade', 'Produto.largura', 'Produto.altura', '(SELECT concat("uploads/produto_imagem/filename/",ProdutoImagem.filename) FROM produto_imagens as ProdutoImagem WHERE ProdutoImagem.produto_id = Produto.id LIMIT 1) as imagem'), 
															)
												);
				
						if($prd['Produto']['profundidade'] > $profundidade){
							$profundidade	        = $this->String->bcoToMoeda($prd['Produto']['profundidade']);
						}
						if($prd['Produto']['largura'] > $largura){
							$largura	           	= $this->String->bcoToMoeda($prd['Produto']['largura']);
						}
						$altura		                += $this->String->bcoToMoeda($prd['Produto']['altura']*$produto_do_kit['ProdutoKit']['quantidade']);
						$peso                       += $prd['Produto']['peso']*$produto_do_kit['ProdutoKit']['quantidade'];
						$peso_ramos                 += $prd['Produto']['peso_ramos']*$produto_do_kit['ProdutoKit']['quantidade'];
						
						$preco_total                += $produto_do_kit['ProdutoKit']['preco']*$produto_do_kit['ProdutoKit']['quantidade'];
						$preco_total_real			+= $produto_do_kit['ProdutoKit']['preco_real']*$produto_do_kit['ProdutoKit']['quantidade'];
						
						$prd['Produto']['preco'] 		= $produto_do_kit['ProdutoKit']['preco'];
						$prd['Produto']['preco_real'] 	= $produto_do_kit['ProdutoKit']['preco_real'];
						$prd['Produto']['quantidade'] 	= $produto_do_kit['ProdutoKit']['quantidade'];
						
						if($first){
							$first = false;
							$codigo_erp = $prd['Produto']['codigo_erp'];
						}else{
							$codigo_erp .= ' + '.$prd['Produto']['codigo_erp'];
						}
						
						$kit_produtos[] = $prd;
						
					}
					
				}
				
				//validacao de preco
				if( ($preco_total_real != $preco_total) &&  ($preco_total < $preco_total_real) ){
					$dt['Produto']['preco'] 			=  $preco_total_real;
					$dt['Produto']['preco_promocao'] 	=  $preco_total;
				}else{
					$dt['Produto']['preco'] 			=  $preco_total;
					$dt['Produto']['preco_promocao'] 	=  '0.00';
				}
				
				//salva valores atualizados do kit
				$prdKit = $this->Produto->find('first', array('recursive' => -1, 'conditions' => array('Produto.id' => $produto_kit['ProdutoKit']['kit_id'])));
				
				$dt['Produto']['id']						= $produto_kit['ProdutoKit']['kit_id'];
				$dt['Produto']['peso'] 						= $peso;
				$dt['Produto']['peso_ramos'] 				= $peso_ramos;
				$dt['Produto']['altura'] 					= $altura;
				$dt['Produto']['largura'] 					= $largura;
				$dt['Produto']['profundidade'] 				= $profundidade;
				//$dt['Produto']['codigo_erp'] 				= $codigo_erp;
				$dt['Produto']['kit_produtos']				= json_encode($kit_produtos);
				
				//gamba
				$dt['Produto']['status']					= $prdKit['Produto']['status'];
				$dt['Produto']['kit']						= $prdKit['Produto']['kit'];
				$dt['Produto']['quantidade']				= $prdKit['Produto']['quantidade'];
				$dt['Produto']['quantidade_disponivel']		= $prdKit['Produto']['quantidade_disponivel'];
				$dt['Produto']['quantidade_alocada']		= $prdKit['Produto']['quantidade_alocada'];
				$dt['Produto']['quantidade_acessos']		= $prdKit['Produto']['quantidade_acessos'];
				
				$this->Produto->create();
				$this->Produto->save($dt, array('validate' => false, 'callbacks' => false));
			}
			
		}
		
	}
	
	//metodo que valida se todos os produtos que compoem o KIT em questão possuem estoque
	private function _valida_estoque_kit($codigo_kit){
		App::import('Model','Produto');
		$this->Produto = new Produto();
		
		$produtos_do_kit = $this->find('all',array(
												'recursive' => -1, 
												'fields' => array('ProdutoKit.produto_id'), 
												'conditions' => array('ProdutoKit.kit_id' => $codigo_kit)
											)
									);
		$kit_estoque = true;
		
		if($produtos_do_kit){
			foreach($produtos_do_kit as $produto_do_kit){
				$prd = $this->Produto->find('first', array(
															'recursive' => -1, 
															'fields'	=> array('Produto.id', 'Produto.quantidade_disponivel', 'Produto.status'),
															'conditions' => array('Produto.id' => $produto_do_kit['ProdutoKit']['produto_id']) 
														)
											);
											
				if($prd){
					if($prd['Produto']['quantidade_disponivel'] <= 0 || $prd['Produto']['status'] != 1){
						$kit_estoque = false;
					}
				}
			}
		}else{
			$kit_estoque = false;
		}
		
		return $kit_estoque;
	}
	
}