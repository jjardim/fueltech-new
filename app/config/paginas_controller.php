<?php

class PaginasController extends AppController {

    var $name = 'Paginas';
    var $components = array('Session', 'Email', 'RequestHandler');
    var $helpers = array('Calendario', 'String', 'Image', 'Flash', 'Javascript', 'Estados', 'Marcas');

    //var $uses = array('Sac');

    function pagina_404() {

    }

    function admin_index() {
        $this->Pagina->recursive = 1;
        $this->set('paginas', $this->paginate());
    }

    function view($id = null) {

        if (!empty($this->data) && (isset($this->data['Sac']['sac_tipo_id']) || isset($this->data['TrabalheConosco']['sac_tipo_id'])  || isset($this->data['FtEducation']['sac_tipo_id'])  || isset($this->data['Ouvidoria']['sac_tipo_id'])) ) {

            if(isset($this->data['Sac']['sac_tipo_id']) && $this->data['Sac']['sac_tipo_id'] == "8"){
                App::import('Model', 'Sac');
                $this->Sac = new Sac();
                if (!empty($this->data)) {
                    if ($this->Sac->save($this->data)) {

                    	$this->sendMail($this->data);

                    	/*Se o formulário for do Fale Conosco, faz o envio dos dados para o adManager*/
                        if($this->data['Sac']['sac_tipo_id'] == "8"){
                          require_once('vendors/AdManagerAPI.class.php');

                          $admanager = new AdManagerAPI();
                          $admanager->registraAcesso();

                          $meio_captacao = 'FORM_FALE_CONOSCO';
                          $admanager->registraLead($meio_captacao,$this->data['Sac']['nome'],$this->data['Sac']['email'],$this->data['Sac']['telefone'],$this->data['Sac']['cidade'],$this->data['Sac']['estado'],'Brasil',$this->data['Sac']['mensagem']);
                        }

                        $this->data = array();
                        $this->Session->setFlash('Contato enviado com sucesso.', 'flash/success');
                    } else {
                        $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
                    }
                }
            }

            if(isset($this->data['Ouvidoria']['sac_tipo_id']) && $this->data['Ouvidoria']['sac_tipo_id'] == "13"){
                App::import('Model', 'Ouvidoria');
                $this->Ouvidoria = new Ouvidoria();
		        
                if (!empty($this->data)) {
		        
                    if ($this->Ouvidoria->save($this->data)) {
                        $this->sendMailOuvidoria($this->data, $this->Ouvidoria->getLastInsertId());
                        $this->data = array();
                        $this->Session->setFlash('Contato enviado com sucesso.', 'flash/success');
                    } else {
                        $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
                    }
                }
            }

            if(isset($this->data['TrabalheConosco']['sac_tipo_id']) && $this->data['TrabalheConosco']['sac_tipo_id'] == "11"){

                App::import('Model', 'TrabalheConosco');
                $this->TrabalheConosco = new TrabalheConosco();
                if (!empty($this->data)) {
                    #if($this->TrabalheConosco->validates($this->data)){

                        #die("ops");
                        if ($this->TrabalheConosco->save($this->data)) {
                            if($this->data['TrabalheConosco']['sac_tipo_id'] == "11"){
                                require_once('vendors/AdManagerAPI.class.php');

                                $admanager = new AdManagerAPI();
                                $admanager->registraAcesso();

                                $meio_captacao = 'FORM_TRABALHE_CONOSCO';
                                $admanager->registraLead($meio_captacao,$this->data['TrabalheConosco']['nome'],$this->data['TrabalheConosco']['email'],$this->data['TrabalheConosco']['telefone'],$this->data['TrabalheConosco']['cpf']);
                            }

                            $this->sendMailTrabalheConosco($this->data, $this->TrabalheConosco->getLastInsertId());
                            $this->data = array();
                            $this->Session->setFlash('Contato enviado com sucesso.', 'flash/success');
                        } else {
                            $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
                        }
                    }else{
                        $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
                    }
            }


            #}

        } elseif (!empty($this->data) && isset($this->data['SegundaVia']['documento'])) {
            $resposta = file_get_contents('http://ws.gruposlc.com.br:8080/axis/CFG_WS_GeraBoleto.jws?method=buscaDadosBoleto&identCliente=' . $this->data['SegundaVia']['documento']);
            $resposta = explode(";", $resposta);
            $nomeSacado = explode("|", $resposta[0]);
            $nomeSacado = $nomeSacado[1];

            //seto para a pagina financeiro
            $pagina_element_content['resposta'] = $resposta;
            $pagina_element_content['nomeSacado'] = $nomeSacado;
            $this->set('pagina_element_content', $pagina_element_content);
        } elseif (!empty($this->data) && isset($this->data['Veiculo'])) {

            App::import('Model', 'Veiculo');
            $this->Veiculo = new Veiculo();

            //begin validacao dos campos 'virtuais' [ver com luan]
            if (null == $this->data['Veiculo']['veiculo']) {
                $this->Veiculo->invalidate('veiculo', 'Campo de preenchimento obrigatório');
            }
            if (null == $this->data['Veiculo']['proprietario']) {
                $this->Veiculo->invalidate('proprietario', 'Campo de preenchimento obrigatório');
            }
            if (null == $this->data['Veiculo']['cilindradas']) {
                $this->Veiculo->invalidate('cilindradas', 'Campo de preenchimento obrigatório');
            }
            if (null == $this->data['Veiculo']['potencia_estimada']) {
                $this->Veiculo->invalidate('potencia_estimada', 'Campo de preenchimento obrigatório');
            }
            if (null == $this->data['Veiculo']['comando_valvulas']) {
                $this->Veiculo->invalidate('comando_valvulas', 'Campo de preenchimento obrigatório');
            }
            if (null == $this->data['Veiculo']['injetores']) {
                $this->Veiculo->invalidate('injetores', 'Campo de preenchimento obrigatório');
            }
            if (null == $this->data['Veiculo']['combustivel']) {
                $this->Veiculo->invalidate('combustivel', 'Campo de preenchimento obrigatório');
            }
            if (null == $this->data['Veiculo']['bobina']) {
                $this->Veiculo->invalidate('bobina', 'Campo de preenchimento obrigatório');
            }
            //end validacao dos campos 'virtuais' [ver com luan]
            //begin veiculo nome [veiculo + proprietario]
            if ($this->data['Veiculo']['veiculo'] != "" && $this->data['Veiculo']['proprietario'] != "") {
                $data['Veiculo']['nome'] = $this->data['Veiculo']['veiculo'] . ' - ' . $this->data['Veiculo']['proprietario'];
            }
            //end veiculo nome [veiculo + proprietario]
            //begin veiculo descricao
            $dados = null;
            foreach ($this->data['Veiculo'] as $key => $dt) {
                if ($dt != "") {
                    $dados .= "<p>";
                    $dados .= $this->Veiculo->trata_field($key);
                    $dados .= $dt;
                    $dados .= "</p>";
                }
            }
            if ($dados != null) {
                $data['Veiculo']['descricao'] = $dados;
            }
            //end veiculo descricao

            $data['Veiculo']['status'] = false;

            //salvo veiculos
            if ($this->Veiculo->validates()) {
                if ($this->Veiculo->save($data)) {

                    if (isset($this->data['GaleriaFoto'])) {

                        App::import('Model', 'Galeria');
                        $this->Galeria = new Galeria();

                        $data1['Galeria']['nome'] = $data['Veiculo']['nome'];

                        //salvo a galeria do veiculo
                        if ($this->Galeria->save($data1)) {

                            App::import('Model', 'GaleriaFoto');
                            $this->GaleriaFoto = new GaleriaFoto();

                            $this->Galeria->bindModel(array('hasMany' => array('GaleriaFoto')));

                            // debug($this->data['GaleriaFoto']);
                            // die;
                            //salvo as fotos da galeria
                            foreach ($this->data['GaleriaFoto'] as $k => $fg) {
                                $this->data['GaleriaFoto'][$k]['id'] = '';
                                $this->data['GaleriaFoto'][$k]['ordem'] = 0;
                                $this->data['GaleriaFoto'][$k]['galeria_id'] = $this->Galeria->id;
                                $this->GaleriaFoto->save($this->data['GaleriaFoto'][$k], false);
                            }

                            //vinculo a galeria no veiculo
                            $data2['Veiculo']['id'] = $this->Veiculo->id;
                            $data2['Veiculo']['galeria_id'] = $this->Galeria->id;
                            $this->Veiculo->save($data2, false);
                        }
                    }

                    $this->data = array();
                    $this->Session->setFlash('Veiculo cadastro com sucesso com sucesso. Aguardando aprovação do administrador.', 'flash/success');
                } else {
                    $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
                }
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }

        if (!$id) {
            $this->Session->setFlash('Parâmetro inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }

        //$pagina = $this->Pagina->read(null, $id);
        $this->Pagina->recursive = 0;
        $pagina = $this->Pagina->find("first", array(
            'recusrive' => -1,
            'fields' => array('Pagina.*', 'PaginaDescricao.*'),
            'conditions' => array('Pagina.id' => $id, 'PaginaDescricao.language' => Configure::read('Config.language')),
            'joins' => array(
                array(
                    'table' => 'pagina_descricoes',
                    'alias' => 'PaginaDescricao',
                    'type' => 'LEFT',
                    'conditions' => 'PaginaDescricao.pagina_id = Pagina.id'
                )
            )
                )
        );
        //$content_pagina = $pagina['Pagina']['texto'];
        //mando o tipo da categoria pra view.
        $this->set('pagina_atual_tipo', $pagina['Pagina']['categoria']);
        $this->set('pagina_atual_url', $pagina['Pagina']['url']);
        $this->set('pagina_atual_nome', $pagina['PaginaDescricao']['nome']);

        //start breadcrumb
        $breadcrumb = "";
        $breadcrumb[] = array('url' => 'javascript:void(0);', 'nome' => $pagina['Pagina']['categoria']);
        $breadcrumb[] = array('url' => 'javascript:void(0);', 'nome' => $pagina['PaginaDescricao']['nome']);
        $this->set('breadcrumb', $breadcrumb);
        //end breadcrumb

        if ($pagina['Pagina']['dinamico']) {
            switch ($pagina['Pagina']['element']) {
                case "noticias":
                    App::import('Model', 'Noticia');
                    $this->Noticia = new Noticia();

                    $this->paginate = array(
                        'contain' => array('Galeria' => array('GaleriaFoto'), 'NoticiaTipo'),
                        'recursive' => 1,
                        'limit' => 12,
                        'conditions' => array(
                            'AND' => array(
                                "Noticia.status" => true,
                                "Noticia.language" => Configure::read('Config.language'),
                                "NoticiaTipo.codigo <> 'materias-tecnicas'"
                            )
                        ),
                        'order' => array('Noticia.created DESC')
                    );

                    $this->set('pagina_element_content', $this->paginate('Noticia'));
                    break;

                case "wallpapers":
                    App::import('Model', 'Wallpaper');
                    $this->Wallpaper = new Wallpaper();
                    $this->paginate = array(
                        'contain' => array('WallpaperGrade' => array('WallpaperTamanho')),
                        'limit' => 12,
                        'conditions' => array("Wallpaper.status" => true),
                        'order' => array("Wallpaper.ordem")
                    );
                    $this->set('pagina_element_content', $this->paginate('Wallpaper'));
                    break;

                case "videos":
                    App::import('Model', 'Video');
                    $this->Video = new Video();

                    $video_destaque = $this->Video->find('first', array(
                        'recursive' => -1,
                        'order' => array('Video.created DESC')
                            )
                    );

                    $this->paginate = array(
                        'limit' => 12,
                        'conditions' => array("Video.status" => true, "Video.id <>" => $video_destaque['Video']['id']),
                        'order' => array("Video.created DESC")
                    );

                    $pagina_element_content['destaque'] = $video_destaque;
                    $pagina_element_content['paginate'] = $this->paginate('Video');

                    $this->set('pagina_element_content', $pagina_element_content);
                    break;

                case "revendedores":
                    App::import('Model', 'Revendedor');
                    $this->Revendedor = new Revendedor();

                    $pagina_element_content['estados'] = $this->Revendedor->find("list", array(
                        'fields' => array('Revendedor.localidade_label', 'Revendedor.localidade_label'),
                        'conditions' => array('Revendedor.status' => true, 'Revendedor.language' => Configure::read('Config.language')),
                        'recursive' => -1,
                        'group' => array('Revendedor.localidade_label'),
                        'order' => array('Revendedor.estado DESC')
                            )
                    );

                    $pagina_element_content['locais'] = $this->Revendedor->find("all", array(
                        'fields' => array('Revendedor.localidade_label'),
                        'conditions' => array('Revendedor.status' => true, 'Revendedor.language' => Configure::read('Config.language')),
                        'recursive' => -1,
                        'group' => array('Revendedor.localidade_label'),
                        'order' => array('Revendedor.estado')
                            )
                    );

                    $pagina_element_content['revendedores'] = $this->Revendedor->find("all", array(
                        'conditions' => array('Revendedor.status' => true),
                        'recursive' => -1
                            )
                    );

                    //busca o primeiro idioma externo, para  o uso da url externa
                    App::import('Model', 'Linguagem');
                    $this->Linguagem = new Linguagem();
                    $pagina_element_content['linguagens'] = $this->Linguagem->find('first', array('recursive' => -1, 'conditions' => array('Linguagem.externo' => true)));

                    $this->set('pagina_element_content', $pagina_element_content);
                    $this->set('jquery_map_plugin', true);
                    break;

                case "parceiros":
                    App::import('Model', 'Parceiro');
                    $this->Parceiro = new Parceiro();

                    $pagina_element_content['estados'] = $this->Parceiro->find("list", array(
                        'fields' => array('Parceiro.estado', 'Parceiro.estado'),
                        'conditions' => array('Parceiro.status' => true),
                        'recursive' => -1,
                        'group' => array('Parceiro.estado'),
                        'order' => array('Parceiro.estado')
                            )
                    );

                    $pagina_element_content['locais'] = $this->Parceiro->find("all", array(
                        'fields' => array('Parceiro.localidade_label'),
                        'conditions' => array('Parceiro.status' => true),
                        'recursive' => -1,
                        'group' => array('Parceiro.estado'),
                        'order' => array('Parceiro.estado')
                            )
                    );

                    $pagina_element_content['revendedores'] = $this->Parceiro->find("all", array(
                        'conditions' => array('Parceiro.status' => true),
                        'recursive' => -1
                            )
                    );
                    $this->set('pagina_element_content', $pagina_element_content);
                    $this->set('jquery_map_plugin', true);
                    break;

                case "calculo-de-injetores":
                    $pagina_element_content = array();
                    $this->set('pagina_element_content', $pagina_element_content);
                    break;

                case "envie-a-foto-do-seu-veiculo":
                    $pagina_element_content = array();
                    $this->set('pagina_element_content', $pagina_element_content);
                    break;

                case "veiculos-com-fueltech":
                    App::import('Model', 'Veiculo');
                    $this->Veiculo = new Veiculo();

                    $this->paginate = array(
                        'fields' => array('Veiculo.*, Galeria.*', '(SELECT concat("uploads/galeria/filename/",GaleriaFoto.filename) FROM galeria_fotos as GaleriaFoto WHERE GaleriaFoto.galeria_id = Galeria.id AND GaleriaFoto.destaque = 1 LIMIT 1) as thumb'),
                        'contain' => array('Galeria' => array('GaleriaFoto')),
                        'limit' => 12,
                        'conditions' => array("Veiculo.status" => true),
                        'order' => array("Veiculo.created DESC, Veiculo.id DESC")
                    );
                    $pagina_element_content = $this->paginate('Veiculo');
                    $this->set('pagina_element_content', $pagina_element_content);
                    break;

                case "manuais":
                    App::import('Model', 'Download');
                    $this->Download = new Download();
                    $this->paginate = array(
                        'order' => 'Download.ordem ASC, Download.ordem_versao DESC',
                        'limit' => 12,
                        'conditions' => array(
                            "AND" =>
                            array(
                                "Download.status" => true,
                                "Download.language" => Configure::read('Config.language')
                            ),
                            "OR" => array(
                                array("DownloadTipo.codigo" => "manuais"),
                                array("DownloadTipo.codigo" => "manuais-espanhol"),
                                array("DownloadTipo.codigo" => "manuais-ingles"),
                            )
                        )
                    );
                    $this->set('pagina_element_content', $this->paginate('Download'));
                    break;

                case "manuais-em-portugues":
                    App::import('Model', 'Download');
                    $this->Download = new Download();
                    $this->paginate = array(
                        'order' => 'Download.ordem ASC, Download.ordem_versao DESC',
                        'limit' => 12,
                        'conditions' => array(
                            "AND" =>
                            array(
                                "Download.status" => true,
                                "Download.language" => Configure::read('Config.language')
                            ),
                            "OR" => array(
                                array("DownloadTipo.codigo" => "manuais")
                            )
                        )
                    );
                    $this->set('pagina_element_content', $this->paginate('Download'));
                    break;

                case "manuais-em-espanhol":
                    App::import('Model', 'Download');
                    $this->Download = new Download();
                    $this->paginate = array(
                        'order' => 'Download.ordem ASC, Download.ordem_versao DESC',
                        'limit' => 12,
                        'conditions' => array(
                            "AND" =>
                            array(
                                "Download.status" => true,
                                "Download.language" => Configure::read('Config.language')
                            ),
                            "OR" => array(
                                array("DownloadTipo.codigo" => "manuais-espanhol")
                            )
                        )
                    );
                    $this->set('pagina_element_content', $this->paginate('Download'));
                    break;

                case "manuais-em-ingles":
                    App::import('Model', 'Download');
                    $this->Download = new Download();
                    $this->paginate = array(
                        'order' => 'Download.ordem ASC, Download.ordem_versao DESC',
                        'limit' => 12,
                        'conditions' => array(
                            "AND" =>
                            array(
                                "Download.status" => true,
                                "Download.language" => Configure::read('Config.language')
                            ),
                            "OR" => array(
                                array("DownloadTipo.codigo" => "manuais-ingles")
                            )
                        )
                    );
                    $this->set('pagina_element_content', $this->paginate('Download'));
                    break;

                case "arquivos-tecnicos":
                    App::import('Model', 'Download');
                    $this->Download = new Download();
                    $this->paginate = array(
                        'order' => 'Download.ordem ASC, Download.ordem_versao DESC',
                        'limit' => 12,
                        'conditions' => array(
                            "AND" =>
                            array(
                                "Download.status" => true,
                                "Download.language" => Configure::read('Config.language')
                            ),
                            "OR" => array(
                                array("DownloadTipo.codigo" => "arquivos-tecnicos"),
                                array("DownloadTipo.codigo" => "diagramas-eletricos"),
                                array("DownloadTipo.codigo" => "testes"),
                                array("DownloadTipo.codigo" => "orçamentos"),
                            )
                        )
                    );
                    $this->set('pagina_element_content', $this->paginate('Download'));
                    break;

                case "diagramas-eletricos":
                    App::import('Model', 'Download');
                    $this->Download = new Download();
                    $this->paginate = array(
                        'order' => 'Download.ordem ASC, Download.ordem_versao DESC',
                        'limit' => 12,
                        'conditions' => array(
                            "AND" =>
                            array(
                                "Download.status" => true,
                                "Download.language" => Configure::read('Config.language')
                            ),
                            "OR" => array(
                                array("DownloadTipo.codigo" => "diagramas-eletricos")
                            )
                        )
                    );
                    $this->set('pagina_element_content', $this->paginate('Download'));
                    break;

                case "testes":
                    App::import('Model', 'Download');
                    $this->Download = new Download();
                    $this->paginate = array(
                        'order' => 'Download.ordem ASC, Download.ordem_versao DESC',
                        'limit' => 12,
                        'conditions' => array(
                            "AND" =>
                            array(
                                "Download.status" => true,
                                "Download.language" => Configure::read('Config.language')
                            ),
                            "OR" => array(
                                array("DownloadTipo.codigo" => "testes")
                            )
                        )
                    );
                    $this->set('pagina_element_content', $this->paginate('Download'));
                    break;

                case "softwares":
                    App::import('Model', 'Download');
                    $this->Download = new Download();
                    $this->paginate = array(
                        'order' => 'Download.ordem ASC, Download.ordem_versao DESC',
                        'limit' => 20,
                        'conditions' => array(
                            "Download.status" => true,
                            "Download.language" => Configure::read('Config.language'),
                            "DownloadTipo.codigo" => "software")
                    );
                    $this->set('pagina_element_content', $this->paginate('Download'));
                    break;

                case "versoes-anteriores":                
                    App::import('Model', 'Download');
                    $this->Download = new Download();
                    $this->paginate = array(
                        'order' => 'Download.ordem ASC, Download.ordem_versao DESC',
                        'limit' => 20,
                        'conditions' => array(
                            "Download.status" => true,
                            "Download.language" => Configure::read('Config.language'),
                            "DownloadTipo.codigo" => "versoes-anteriores")
                    );

                    $this->set('pagina_element_content', $this->paginate('Download'));
                    break;


                case "materiais-de-divulgacao":
                    App::import('Model', 'Download');
                    $this->Download = new Download();
                    $this->paginate = array(
                        'limit' => 12,
                        'conditions' => array(
                            "Download.status" => true,
                            "Download.language" => Configure::read('Config.language'),
                            "DownloadTipo.codigo" => "material-de-divulgacao")
                    );
                    $this->set('pagina_element_content', $this->paginate('Download'));
                    break;

                case "duvidas-tecnicas":
                    App::import('Model', 'DuvidaFrequente');
                    $this->DuvidaFrequente = new DuvidaFrequente();
                    $this->paginate = array(
                        'recursive' => -1,
                        'contain' => array('DuvidaFrequenteDescricao'),
                        'fields' => array('DuvidaFrequente.*', 'DuvidaFrequenteDescricao.*'),
                        'limit' => 12,
                        'joins' => array(
                            array(
                                'table' => 'duvidas_frequente_descricoes',
                                'alias' => 'DuvidaFrequenteDescricao',
                                'type' => 'LEFT',
                                'conditions' => 'DuvidaFrequenteDescricao.duvida_frequente_id = DuvidaFrequente.id'
                            )
                        ),
                        'conditions' => array('AND' => array("DuvidaFrequente.status" => true, "DuvidaFrequenteDescricao.language" => Configure::read('Config.language'))),
                    );
                    $this->set('pagina_element_content', $this->paginate('DuvidaFrequente'));
                    break;

                case "materias-tecnicas":
                    App::import('Model', 'Noticia');
                    $this->Noticia = new Noticia();

                    $this->paginate = array(
                    	'order'=>array('Noticia.created'=>'DESC'),
                        'recursive' => 1,
                        'limit' => 12,
                        'conditions' => array(
                            "Noticia.status" => true,
                            "Noticia.language" => Configure::read('Config.language'),
                            "NoticiaTipo.codigo = 'materias-tecnicas'"
                        )
                    );

                    $this->set('pagina_element_content', $this->paginate('Noticia'));
                    break;

                case "faq":
                    App::import('Model', 'Faq');
                    $this->Faq = new Faq();
                    $this->paginate = array(
                        'contain' => array('FaqPergunta' => array('FaqPerguntaDescricao')),
                        'limit' => 12,
                        'joins' => array(
                            array(
                                'table' => 'faq_perguntas',
                                'alias' => 'FaqPergunta',
                                'type' => 'LEFT',
                                'conditions' => 'FaqPergunta.faq_id = Faq.id'
                            ),
                            array(
                                'table' => 'faq_pergunta_descricoes',
                                'alias' => 'FaqPerguntaDescricao',
                                'type' => 'LEFT',
                                'conditions' => 'FaqPerguntaDescricao.faq_pergunta_id = FaqPergunta.id'
                            )
                        ),
                        'group' => array('Faq.id'),
                        'conditions' => array('AND' => array("Faq.status" => true, "FaqPerguntaDescricao.language" => Configure::read('Config.language'))),
                    );
					$this->set('pagina_element_content', $this->paginate('Faq'));
                    break;

                case "form":

                    switch ($pagina['Pagina']['url']) {
                        case 'demonstracao-tecnica':
                            $pagina_element_content['tipo_form'] = "2";
                            $this->set('campo_extra', 'cnpj');
                            break;

                        case 'treinamento':
                            $pagina_element_content['tipo_form'] = "3";
                            $this->set('campo_extra', 'cnpj');
                            break;

                        case 'sac':
                            $pagina_element_content['tipo_form'] = "7";
                            break;

                        case 'grandes-contas':
                            $pagina_element_content['tipo_form'] = "5";
                            break;

                        case 'politica-de-privacidade':
                            $pagina_element_content['tipo_form'] = "9";
                            break;

                        case 'factory-store':
                            $pagina_element_content['tipo_form'] = "6";
                            break;

                        case 'fale-conosco':
                            /* pagina <DOMINIO>/contato */
                        	$pagina_element_content['tipo_form'] = "8";
                            break;

                        case 'ouvidoria':
                            $pagina_element_content['tipo_form'] = "13";
                            break;

                    }

                    $this->set('pagina_element_content', $pagina_element_content);
                    break;
                case "seja-um-revendedor":
                    if ($this->RequestHandler->isPost()) {
                        App::import('Model', 'SejaUmRevendedor');
                        $this->SejaUmRevendedor = new SejaUmRevendedor();
                        $this->SejaUmRevendedor->set($this->data);

                        if ($this->SejaUmRevendedor->validates()) {
                            $this->Session->setFlash('Solicitação enviada com sucesso.', 'flash/success');
                            $this->SejaUmRevendedor->save(); // salva no banco para consulta no admin
                            $this->seja_um_revendedor_email($this->data['SejaUmRevendedor']); // envia e-mail
                            $this->data = array(); // zera o formulário

                        } else {

                            $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');

                        }
                    }
                    break;
                case "trabalhe_conosco":
                    $pagina_element_content['tipo_form'] = "11";
                    $this->set('pagina_element_content', $pagina_element_content);
                    break;

                case 'ouvidoria':
                            $pagina_element_content['tipo_form'] = "13";
                            $this->set('pagina_element_content', $pagina_element_content);
                            break;

                case 'ft-education':
                    $pagina_element_content['tipo_form'] = "12";
                    $this->set('pagina_element_content', $pagina_element_content);

                    if ($this->RequestHandler->isPost()) {

                        App::import('Model', 'FtEducation');
                        $this->FtEducation = new FtEducation();
                        $this->data['FtEducation']['possui_cursos_de'] = implode(',', $this->data['FtEducation']['possui_cursos_de']);
                        $this->FtEducation->set($this->data);

                        if ($this->FtEducation->validates()) {
                            $this->Session->setFlash('Cadastro enviado com sucesso.', 'flash/success');
                            $this->FtEducation->save(); // salva no banco para consulta no admin
                            $this->sendMailFtEducation($this->data['FtEducation']); // envia e-mail
                            $this->data = array(); // zera o formulário

                        } else {

                            $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');

                        }
                    }
                    break;

            }
        }

        //start seo
        $this->set('title_for_layout', $pagina['PaginaDescricao']['nome']);
        $this->set('seo_title', $pagina['PaginaDescricao']['seo_title']);
        $this->set('seo_meta_keywords', $pagina['PaginaDescricao']['seo_meta_keywords']);
        $this->set('seo_meta_description', $pagina['PaginaDescricao']['seo_meta_description']);
        $this->set('seo_institucional', $pagina['PaginaDescricao']['seo_institucional']);
        //end seo
        //begin historico de navegação
        //salvo a visita do cliente, e salvo na sessao
        $historico = $this->Session->read("Historico.navegacao");
        $sessao_atual = array('url' => $pagina['Pagina']['url'], 'nome' => $pagina['PaginaDescricao']['nome']);
        if (isset($historico['Paginas'])) {
            foreach ($historico['Paginas'] as $hp) {
                if ($hp['url'] != $pagina['Pagina']['url']) {
                    $historico_tmp[] = $hp;
                }
            }

            $historico_tmp[] = $sessao_atual;
        } else {
            $historico_tmp = array($sessao_atual);
        }
        $this->Session->write("Historico.navegacao.Paginas", $historico_tmp);
        //end historico de navegação
        $this->set('pagina', $pagina);
        $this->set('breadcrumbs', array(array('nome' => $pagina['PaginaDescricao']['nome'], 'link' => '/' . low(Inflector::slug($pagina['PaginaDescricao']['nome'], '-')))));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Pagina->create();
            if (empty($this->data['Pagina']['url'])) {
                $this->data['Pagina']['url'] = low(Inflector::slug($this->data['PaginaDescricao'][0]['nome'], '-'));
            }

            if ($this->Pagina->saveAll($this->data)) {

                //limpo o cache
                $this->limparCache();
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }

        //set linguas
        App::import('Model', 'Linguagem');
        $this->Linguagem = new Linguagem();
        $idiomas = $this->Linguagem->find('all', array('fields' => array('Linguagem.id', 'Linguagem.codigo', 'Linguagem.nome', 'Linguagem.thumb_filename', 'Linguagem.thumb_dir'), 'conditions' => array('Linguagem.status' => true)));
        $this->set('idiomas', $idiomas);

        //set paginas
        $paginas = $this->Pagina->find('list', array('conditions' => array('Pagina.status' => true)));
        $this->set('paginas', $paginas);
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Parâmetro inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            $this->data['Pagina']['id'] = $id;
            $this->Pagina->id = $id;

            if (empty($this->data['Pagina']['url'])) {
                $this->data['Pagina']['url'] = low(Inflector::slug($this->data['PaginaDescricao'][0]['nome'], '-'));
            }

            if ($this->Pagina->saveAll($this->data)) {

                //limpo o cache
                $this->limparCache();
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Pagina->read(null, $id);
        }

        //set linguas
        App::import('Model', 'Linguagem');
        $this->Linguagem = new Linguagem();
        $idiomas = $this->Linguagem->find('all', array('fields' => array('Linguagem.id', 'Linguagem.codigo', 'Linguagem.nome', 'Linguagem.thumb_filename', 'Linguagem.thumb_dir'), 'conditions' => array('Linguagem.status' => true)));
        $this->set('idiomas', $idiomas);

        //set paginas
        $paginas = $this->Pagina->find('list', array('fields' => array('Pagina.id', 'Pagina.nome'), 'conditions' => array('Pagina.status' => true)));
        $this->set('paginas', $paginas);
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Pagina->delete($id)) {
            //limpo o cache
            $this->limparCache();
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
    }

    function formulario_min($tipo_form, $url_form, $element_id) {
        $this->layout = 'ajax';
        if ($tipo_form == 1)
            $this->set('loja_id', $element_id);
        if ($tipo_form == 4)
            $this->set('vendedor_id', $element_id);
        if ($tipo_form == 10)
            $this->set('quem_somos_tipo', $element_id);

        $this->set('tipo_form', $tipo_form);
        $this->set('url_form', $url_form);
    }

    private function seja_um_revendedor_email($data)
    {
        if (Configure::read('Loja.smtp_host') != 'localhost') {
            $this->Email->smtpOptions = array(
                'port' => (int) Configure::read('Loja.smtp_port'),
                'host' => Configure::read('Loja.smtp_host'),
                'username' => Configure::read('Loja.smtp_user'),
                'password' => Configure::read('Loja.smtp_password')
            );
            $this->Email->delivery = 'smtp';
        }
        $this->Email->lineLength = 120;
        $this->Email->sendAs = 'html';

        $this->Email->to = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_comercial') . ">";
        $this->Email->cc = array(Configure::read('Loja.email_marketing'));
        $this->Email->from = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_sac') . ">";
        $this->Email->replyTo = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_sac') . ">";
        $this->Email->subject = Configure::read('Loja.nome') . " - Contato - Seja um revendedor";

        // Monta o template do e-mail
        $email = str_replace(array(
                '{NOME_DO_RESPONSAVEL}',
                '{CPF}',
                '{RG}',
                '{DATA_DE_NASCIMENTO}',
                '{EMAIL}',
                '{NOME_FANTASIA}',
                '{RAZAO_SOCIAL}',
                '{CNPJ}',
                '{IE}',
                '{ENDERECO}',
                '{BAIRRO}',
                '{CIDADE}',
                '{ESTADO}',
                '{CEP}',
                '{TELEFONE}',
                '{CELULAR}',
                '{FAX}',
                '{SITE}',
            ), array(
                $data['nome_do_responsavel'],
                $data['cpf'],
                $data['rg'],
                $data['data_de_nascimento'],
                $data['email'],
                $data['nome_fantasia'],
                $data['razao_social'],
                $data['cnpj'],
                $data['ie'],
                $data['endereco'],
                $data['bairro'],
                $data['cidade'],
                $data['estado'],
                $data['cep'],
                $data['telefone'],
                $data['celular'],
                $data['fax'],
                $data['site'],
            ),
            Configure::read('LojaTemplate.seja_um_revendedor'));

        $bccs = array();

        // Cópia Reweb
        if (Configure::read('Reweb.nome_bcc')) {
            $assunto = Configure::read('Reweb.nome_bcc');
            $emails = Configure::read('Reweb.email_bcc');
            foreach (explode(';', $emails) as $bcc) {
                $bccs[] = $assunto . "<" . $bcc . ">";
            }
        }

        // BCC
        $this->Email->bcc = $bccs;

        // Dispara o e-mail
        if ($this->Email->send($email)) {
            return true;
        } else {
            return false;
        }
    }

    private function sendMail($dados) {

        Configure::write('debug', 0);
        if (Configure::read('Loja.smtp_host') != 'localhost') {
            $this->Email->smtpOptions = array(
                'port' => (int) Configure::read('Loja.smtp_port'),
                'host' => Configure::read('Loja.smtp_host'),
                'username' => Configure::read('Loja.smtp_user'),
                'password' => Configure::read('Loja.smtp_password')
            );
            $this->Email->delivery = 'smtp';
        }


        $this->Email->lineLength = 120;
        $this->Email->sendAs = 'html';
        $this->Email->from = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_sac') . ">";
        if (isset($dados['Sac']['sac_tipo_id']) && $dados['Sac']['sac_tipo_id'] == 8) {
        	$this->Email->replyTo = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_contato') . ">";
        } else {
        	$this->Email->replyTo = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_sac') . ">";
        }

        //seto o tipo de contato
        App::import('Model', 'SacTipo');
        $this->SacTipo = new SacTipo();
        $sac_tipo = $this->SacTipo->find('first', array('fields' => array('SacTipo.nome', 'SacTipo.email_contato'), 'conditions' => array('SacTipo.id' => $dados['Sac']['sac_tipo_id'])));

        if(!$sac_tipo){
            return false;
        }

        $this->Email->to = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_sac') . ">";
        $this->Email->subject = Configure::read('Loja.nome') . " - Contato - " . $sac_tipo['SacTipo']['nome'];


	echo "<pre>";
	debug($this->Email->to);
	die();
	
        $email = str_replace(
                array(
            '{SAC_TIPO}',
            '{SAC_NOME}',
            '{SAC_EMAIL}',
            '{SAC_ESTADO}',
            '{SAC_CIDADE}',
            '{SAC_TELEFONE}',
            '{SAC_CNPJ}',
            '{SAC_MENSAGEM}',
            '{SAC_DATA}',
            '{SAC_IP}'
                ), array(
            $sac_tipo['SacTipo']['nome'],
            $dados['Sac']['nome'],
            $dados['Sac']['email'],
            (isset($dados['Sac']['estado'])) ? $dados['Sac']['estado'] : "",
            (isset($dados['Sac']['cidade'])) ? $dados['Sac']['cidade'] : "",
            (isset($dados['Sac']['telefone'])) ? $dados['Sac']['telefone'] : "",
            (isset($dados['Sac']['cnpj'])) ? $dados['Sac']['cnpj'] : "",
            $dados['Sac']['mensagem'],
            date("d/m/Y H:i:s"),
            $_SERVER['REMOTE_ADDR']
                ), Configure::read('LojaTemplate.sac')
        );

        $bccs = array();
        //COPIAS REWEB
        if (Configure::read('Reweb.nome_bcc')) {
            $assunto = Configure::read('Reweb.nome_bcc');
            $emails = Configure::read('Reweb.email_bcc');
            foreach (explode(';', $emails) as $bcc) {
                $bccs[] = $assunto . "<" . $bcc . ">";
            }
        }

        //bcc
        $this->Email->bcc = $bccs;
		
        //dispara email
        if ($this->Email->send($email)) {
			//if(@$_SERVER['REMOTE_ADDR']=='187.36.49.172'){
			//	var_dump($this->Email->smtpError);die('Aguarde..');
			//}
			//var_dump($this->Email->smtpError);die('Aguarde..');
            return true;
        } else {
			//if(@$_SERVER['REMOTE_ADDR']=='187.36.49.172'){
			//	var_dump($this->Email->smtpError);die('Aguarde..');
			//}
			//var_dump($this->Email->smtpError);die('Aguarde..');
            return false;
        }
    }

    private function sendMailTrabalheConosco($dados, $TrabalheConoscoId) {

        Configure::write('debug', 0);
        if (Configure::read('Loja.smtp_host') != 'localhost') {
            $this->Email->smtpOptions = array(
                'port' => (int) Configure::read('Loja.smtp_port'),
                'host' => Configure::read('Loja.smtp_host'),
                'username' => Configure::read('Loja.smtp_user'),
                'password' => Configure::read('Loja.smtp_password')
            );
            $this->Email->delivery = 'smtp';
        }

        $this->Email->lineLength    = 120;
        $this->Email->sendAs        = 'html';
        $this->Email->from          = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_rh') . ">";
        //$this->Email->replyTo       = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_rh') . ">";

        $replyTos = array();
        if (Configure::read('Loja.email_rh')) {
        	$assuntoTos = Configure::read('Loja.nome') . " - Contato - " . $sac_tipo['SacTipo']['nome'];
        	$emailsTos = Configure::read('Loja.email_rh');
        	foreach (explode(';', $emailsTos) as $tos) {
        		$replyTos[] = "FuelTech -  trabalhe Conosco" . " <" . $tos . ">";
        	}
        	$str_to = null;
        	foreach ($replyTos as $replys) {
        		$str_to.= $replys .';';
        	}
        	$this->Email->replyTo = $str_to;
        }
		

        App::import('Model', 'SacTipo');
        App::import('Model', 'Sac');
        $this->SacTipo = new SacTipo();
        $this->Sac = new Sac();

        $sac_tipo = $this->SacTipo->find('first', array('fields' => array('SacTipo.nome', 'SacTipo.email_contato'), 'conditions' => array('SacTipo.id' => $dados['TrabalheConosco']['sac_tipo_id'])));

        $path_curriculo = $this->Sac->find('first', array('fields' => array("CONCAT(Sac.dir, '/', Sac.filename) as curriculo"), 'conditions' => array('SacTipo.id' => $dados['TrabalheConosco']['sac_tipo_id'], 'Sac.id' => $TrabalheConoscoId)));

        if(!$sac_tipo){
            return false;
        }

        //$this->Email->to = Configure::read('Loja.smtp_remetente_email') . " <" . $sac_tipo['SacTipo']['email_contato'] . ">";
        //$this->Email->to = Configure::read('Loja.smtp_remetente_email') . " <" . $sac_tipo['SacTipo']['email_contato'] . ">" . ', ' .Configure::read('Loja.smtp_remetente_email') . " <recepcao@fueltech.com.br>";
        $email_tos = array();
        if (isset($sac_tipo['SacTipo']['email_contato'])) {
        	$emailsTo = $sac_tipo['SacTipo']['email_contato'];
        	foreach (explode(';', $emailsTo) as $to) {
        		$email_tos[] = Configure::read('Loja.smtp_remetente_email') . " <" . $to . ">";
        	}
        	$s_to = null;
        	foreach ($email_tos as $email_to) {
      			$s_to.= $email_to;
        	}
        	$this->Email->to = $s_to;
        }

        $this->Email->subject = Configure::read('Loja.nome') . " - Contato - " . $sac_tipo['SacTipo']['nome'];

        if($path_curriculo){
            $path_curriculo = current($path_curriculo);
            if(isset($path_curriculo['curriculo'])){
                $path_curriculo = str_replace("\\", "/", $path_curriculo['curriculo']);
                $this->Email->attachments = array($path_curriculo);
            }
        }

           $email = str_replace(
                array(
            '{SAC_TIPO}',
            '{TRABALHE_CONOSCO_NOME}',
            '{TRABALHE_CONOSCO_EMAIL}',
            '{TRABALHE_CONOSCO_CPF}',
            '{TRABALHE_CONOSCO_TELEFONE}',
            '{TRABALHE_CONOSCO_DATA}',
            '{TRABALHE_CONOSCO_IP}'
                ), array(
            $sac_tipo ,
            $dados['TrabalheConosco']['nome'],
            $dados['TrabalheConosco']['email'],
            $dados['TrabalheConosco']['cpf'],
            $dados['TrabalheConosco']['telefone'],
            date("d/m/Y H:i:s"),
            $_SERVER['REMOTE_ADDR']
                ), Configure::read('LojaTemplate.trabalhe_conosco')
        );


        $bccs = array();
        //COPIAS REWEB
        if (Configure::read('Reweb.nome_bcc')) {
            $assunto = Configure::read('Reweb.nome_bcc');
            $emails = Configure::read('Reweb.email_bcc');
            foreach (explode(';', $emails) as $bcc) {
                $bccs[] = $assunto . "<" . $bcc . ">";
            }
        }

        //bcc
        $this->Email->bcc = $bccs;

        //dispara email
        if ($this->Email->send($email)) {
            return true;
        } else {
        	return false;
        }
    }

    private function limparCache() {
        Cache::write('paginas', false);
        Cache::write('paginas_routes', false);
    }

    private function sendMailFtEducation($data) {

        if (Configure::read('Loja.smtp_host') != 'localhost') {
            $this->Email->smtpOptions = array(
                'port' => (int) Configure::read('Loja.smtp_port'),
                'host' => Configure::read('Loja.smtp_host'),
                'username' => Configure::read('Loja.smtp_user'),
                'password' => Configure::read('Loja.smtp_password')
            );
            $this->Email->delivery = 'smtp';
        }
        $this->Email->lineLength = 120;
        $this->Email->sendAs = 'html';

        $this->Email->to = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_fteducation') . ">";
        //$this->Email->cc = array(Configure::read('Loja.email_marketing'));
        $this->Email->from = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_sac') . ">";
       /// $this->Email->replyTo = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_sac') . ">";
        $this->Email->subject = Configure::read('Loja.nome') . " - Contato - FT Education";

        // Monta o template do e-mail
        $email = str_replace(array(
                '{NOME}',
                '{CPF}',
                '{ENDERECO_RESIDENCIAL}',
                '{TELEFONE}',
                '{ESTADO}',
                '{CIDADE}',
                '{EMPRESA_OFICINA}',
                '{POSSUI_CURSOS_DE}',
                '{EMAIL}',
                '{MENSAGEM}'
            ), array(
                $data['nome'],
                $data['cpf'],
                $data['endereco_residencial'],
                $data['telefone'],
                $data['estado'],
                $data['cidade'],
                $data['empresa_oficina'],
                 $data['possui_cursos_de'],
                $data['email'],
                $data['mensagem']
            ),
            Configure::read('LojaTemplate.ft_educations'));

        $bccs = array();

        // Cópia Reweb
        if (Configure::read('Reweb.nome_bcc')) {
            $assunto = Configure::read('Reweb.nome_bcc');
            $emails = Configure::read('Reweb.email_bcc');
            foreach (explode(';', $emails) as $bcc) {
                $bccs[] = $assunto . "<" . $bcc . ">";
            }
        }

        // BCC
        $this->Email->bcc = $bccs;

        // Dispara o e-mail
        if ($this->Email->send($email)) {
            return true;
        } else {
            return false;
        }
    }

    private function sendMailOuvidoria($dados, $ouvidoriaId) {

        Configure::write('debug', 0);
        if (Configure::read('Loja.smtp_host') != 'localhost') {
            $this->Email->smtpOptions = array(
                'port' => (int) Configure::read('Loja.smtp_port'),
                'host' => Configure::read('Loja.smtp_host'),
                'username' => Configure::read('Loja.smtp_user'),
                'password' => Configure::read('Loja.smtp_password')
            );
            $this->Email->delivery = 'smtp';
        }

        $this->Email->lineLength    = 120;
        $this->Email->sendAs        = 'html';
        $this->Email->replyTo       = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_rh') . ">";
        $this->Email->from          = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_rh') . ">";

        App::import('Model', 'SacTipo');
        App::import('Model', 'Ouvidoria');
        $this->SacTipo = new SacTipo();
        $this->Ouvidoria = new Ouvidoria();

        $sac_tipo = $this->SacTipo->find('first', array('fields' => array('SacTipo.nome', 'SacTipo.email_contato'), 'conditions' => array('SacTipo.id' => $dados['Ouvidoria']['sac_tipo_id'])));

        $path_file = $this->Ouvidoria->find('first', array('fields' => array("CONCAT(Ouvidoria.dir, '/', Ouvidoria.filename) as file"), 'conditions' => array('SacTipo.id' => $dados['Ouvidoria']['sac_tipo_id'], 'Ouvidoria.id' => $ouvidoriaId)));
        
        if(!$sac_tipo){
            return false;
        }

        $this->Email->to = Configure::read('Loja.smtp_remetente_email') . " <" . $sac_tipo['SacTipo']['email_contato'] . ">";
        $this->Email->subject = Configure::read('Loja.nome') . " - " . $sac_tipo['SacTipo']['nome'] . " - " . $dados['Ouvidoria']['assunto'];

        if($path_file){
            $path_file = current($path_file);
            if(isset($path_file['file'])){
                $path_file = str_replace("\\", "/", $path_file['file']);
                $this->Email->attachments = array($path_file);
            }
        }

           $email = str_replace(
                array(
            '{SAC_TIPO}',
            '{OUVIDORIA_NOME}',
            '{OUVIDORIA_EMAIL}',
            '{OUVIDORIA_TELEFONE}',
            '{OUVIDORIA_CELULAR}',
            '{OUVIDORIA_CIDADE}',
            '{OUVIDORIA_ESTADO}',
            '{OUVIDORIA_MENSAGEM}',
            '{OUVIDORIA_IP}'
                ), array(
            $sac_tipo ,
            $dados['Ouvidoria']['nome'],
            $dados['Ouvidoria']['email'],
            $dados['Ouvidoria']['telefone'],
            $dados['Ouvidoria']['celular'],
            $dados['Ouvidoria']['cidade'],
            $dados['Ouvidoria']['estado'],
            $dados['Ouvidoria']['mensagem'],
            date("d/m/Y H:i:s"),
            $_SERVER['REMOTE_ADDR']
                ), Configure::read('LojaTemplate.ouvidoria')
        );


        $bccs = array();
        //COPIAS REWEB
        if (Configure::read('Reweb.nome_bcc')) {
            $assunto = Configure::read('Reweb.nome_bcc');
            $emails = Configure::read('Reweb.email_bcc');
            foreach (explode(';', $emails) as $bcc) {
                $bccs[] = $assunto . "<" . $bcc . ">";
            }
        }

        //bcc
        $this->Email->bcc = $bccs;

        //dispara email
        if ($this->Email->send($email)) {
            return true;
        } else {
            return false;
        }
    }

}

?>