<?php


/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app.config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/views/pages/home.ctp)...
 */
 
Router::connect('/404', array('controller' => 'paginas', 'action' => 'pagina_404'));
Router::connect('/', array('controller' => 'home', 'action' => 'index'));
Router::connect('/:language', array('controller' => 'home', 'action' => 'index'), array('language' => ".{2}"));

Router::connect('/admin', array('admin' => true, 'controller' => 'home', 'action' => 'index'));
Router::connect('/admin/login', array('admin' => true, 'controller' => 'usuarios', 'action' => 'login'));
Router::connect('/login', array('controller' => 'usuarios', 'action' => 'login'));
Router::connect('/admin/logout', array('admin' => true, 'controller' => 'usuarios', 'action' => 'logout'));
Router::connect('/logout', array('controller' => 'usuarios', 'action' => 'logout'));
Router::connect('/busca/index/', array('controller' => 'busca','action' => 'index'));
Router::connect('/busca/index/*', array('controller' => 'busca','action' => 'index'));
Router::connect('/marca/index/', array('controller' => 'busca','action' => 'index'));
Router::connect('/marca/index/*', array('controller' => 'busca','action' => 'index'));
//Router::connect('/busca/index/:slug/:slug/:id/', array('controller' => 'busca','action' => 'index'), array('pass' => array('id'),'id'=>'[0-9]+'));
//Router::connect('/busca/index/:slug/:slug/:id/*', array('controller' => 'busca','action' => 'index'), array('pass' => array('id'),'id'=>'[0-9]+'));
Router::connect('/:slug:div:id', array('controller' => 'categorias','action' => 'index'),array('pass' => array('id'),'div'=>'-cat-','id'=>'[0-9]+'));
Router::connect('/:slug:div:id/*', array('controller' => 'categorias','action' => 'index'),array('pass' => array('id'),'div'=>'-cat-','id'=>'[0-9]+'));
Router::connect('/:slug:div:id', array('controller' => 'categorias','action' => 'index'),array('pass' => array('id'),'div'=>'-depto-','id'=>'[0-9]+'));
Router::connect('/:slug:div:id/*', array('controller' => 'categorias','action' => 'index'),array('pass' => array('id'),'div'=>'-depto-','id'=>'[0-9]+'));
Router::connect('/:slug-cod-:id.html', array('controller' => 'produtos','action' => 'detalhe'),array('pass' => array('id'),'id'=>'[0-9]+'));
//Router::connect('/produtos/*', array('controller' => 'produtos','action' => 'index'),array('pass' => array('id'),'id'=>'[0-9]+'));
Router::connect('/produto/:slug-:id.html', array('controller' => 'produtos','action' => 'detalhe'),array('pass' => array('id'),'id'=>'[0-9]+'));

//Carrinho
Router::connect('/carrinho', array('plugin' => 'carrinho', 'controller' => 'carrinho', 'action' => 'index'));
Router::connect('/carrinho/add/*', array('plugin' => 'carrinho',  'controller' => 'carrinho', 'action' => 'add'));
Router::connect('/carrinho/delete/*', array('plugin' => 'carrinho', 'controller' => 'carrinho', 'action' => 'delete'));
Router::connect('/carrinho/limpar', array('plugin' => 'carrinho', 'controller' => 'carrinho', 'action' => 'limpar'));
Router::connect('/carrinho/forma_pagamento/*', array('plugin' => 'carrinho', 'controller' => 'carrinho', 'action' => 'forma_pagamento'));
Router::connect('/carrinho/entrega/*', array('plugin' => 'carrinho', 'controller' => 'carrinho', 'action' => 'entrega'));
Router::connect('/carrinho/meus_pedidos/*', array('plugin' => 'carrinho', 'controller' => 'carrinho', 'action' => 'meus_pedidos'));
Router::connect('/carrinho/remove_cupom/*', array('plugin' => 'carrinho', 'controller' => 'carrinho', 'action' => 'remove_cupom'));
Router::connect('/carrinho/set_cupom/*', array('plugin' => 'carrinho', 'controller' => 'carrinho', 'action' => 'set_cupom'));
Router::connect('/carrinho/finalizacao/*', array('plugin' => 'carrinho', 'controller' => 'carrinho', 'action' => 'finalizacao'));
Router::connect('/meus_pedidos', array('plugin' => 'carrinho', 'controller' => 'carrinho', 'action' => 'meus_pedidos'));
Router::connect('/meus_pedidos/*', array('plugin' => 'carrinho', 'controller' => 'carrinho', 'action' => 'meus_pedidos'));
Router::connect('/lista_desejos/*', array('controller' => 'desejos','action' => 'index'));
Router::connect('/newsletter/*', array('controller' => 'home','action' => 'newsletter'));
Router::connect('/sitemap.xml', array('controller' => 'sitemaps', 'action' => 'index'));
// Router::connect('/buscape.xml', array('controller' => 'geraxml', 'action' => 'buscape'));
// Router::connect('/ibiubi.xml', array('controller' => 'geraxml', 'action' => 'ibiubi'));
// Router::connect('/shopmania.xml', array('controller' => 'geraxml', 'action' => 'shopmania'));
// Router::connect('/jacotei.xml', array('controller' => 'geraxml', 'action' => 'jacotei'));
//Vitrines
if (Cache::read('vitrines_routes') === false) {
	App::import('Model', 'Vitrine');
	$this->Vitrine = new Vitrine();
	$vitrines = $this->Vitrine->find('all',array('recursive'=>-1,'conditions'  =>  array('Vitrine.status'=>true)));
	Cache::write('vitrines_routes', $vitrines);
}else{
	$vitrines = Cache::read('vitrines_routes');
}

foreach($vitrines as $valor){
	if( $valor['Vitrine']['tipo'] == 'PROMOCOES' ){
		Router::connect("/ofertas", array('controller' => 'vitrines','action' => 'index',$valor['Vitrine']['id']),array());
		Router::connect("/ofertas/*", array('controller' => 'vitrines','action' => 'index',$valor['Vitrine']['id']),array());
	}elseif( $valor['Vitrine']['tipo'] == 'LANCAMENTOS' ){
		Router::connect("/lancamentos", array('controller' => 'vitrines','action' => 'index',$valor['Vitrine']['id']),array());
		Router::connect("/lancamentos/*", array('controller' => 'vitrines','action' => 'index',$valor['Vitrine']['id']),array());
	}elseif( $valor['Vitrine']['tipo'] == 'NORMAL' ){
		Router::connect("/produtos", array('controller' => 'vitrines','action' => 'index',$valor['Vitrine']['id']),array());    
		Router::connect("/produtos/*", array('controller' => 'vitrines','action' => 'index',$valor['Vitrine']['id']),array());
	}else{
		Router::connect("/categoria/".low(Inflector::slug($valor['Vitrine']['nome'],"-")), array('controller' => 'vitrines','action' => 'index',$valor['Vitrine']['id']),array());    
		Router::connect("/categoria/".low(Inflector::slug($valor['Vitrine']['nome'],"-"))."/*", array('controller' => 'vitrines','action' => 'index',$valor['Vitrine']['id']),array());
	}
}
//categorias
if(preg_match('/categorias/',$_GET['url'])){
	Router::connect("/categorias/*", array('controller' => 'categorias', 'action' => 'index'));
}
//noticias
if(preg_match('/noticias/',$_GET['url'])){
	Router::connect("/noticias/:slug-:id.html", array('controller' => 'noticias', 'action' => 'detalhe'),array('pass' => array('id'),'id'=>'[0-9]+'));
}
//materias-tecnicas
if(preg_match('/materias-tecnicas/',$_GET['url'])){
	Router::connect("/materias-tecnicas/:slug-:id.html", array('controller' => 'noticias', 'action' => 'detalhe'),array('pass' => array('id'),'id'=>'[0-9]+'));
}
//videos
if(preg_match('/videos/',$_GET['url'])){
	Router::connect("/videos/:slug-:id.html", array('controller' => 'videos', 'action' => 'detalhe'),array('pass' => array('id'),'id'=>'[0-9]+'));
}
//videos
if(preg_match('/veiculos-com-fueltech/',$_GET['url'])){
	Router::connect("/veiculos-com-fueltech/:slug-:id.html", array('controller' => 'veiculos', 'action' => 'detalhe'),array('pass' => array('id'),'id'=>'[0-9]+'));
}

//Páginas
if (Cache::read('paginas_routes') === false) {
	App::import('Model', 'Pagina');
	$this->Pagina = new Pagina();
	$paginas = $this->Pagina->find('all',array('conditions'  =>  array('Pagina.status'=>true)));
	Cache::write('paginas_routes', $paginas);
}else{
	$paginas = Cache::read('paginas_routes');
}

$id_contato = '';
foreach($paginas as $pagina){
    if($pagina['Pagina']['url']){
        if($pagina['Pagina']['url'] == 'fale-conosco'){
            $id_contato = $pagina['Pagina']['id'];
        }
        Router::connect("/{$pagina['Pagina']['url']}", array('controller' => 'paginas','action' => 'view',$pagina['Pagina']['id']),array());
    }
}

if($id_contato != ''){
    Router::connect("/contato", array('controller' => 'paginas','action' => 'view',$id_contato),array());
}