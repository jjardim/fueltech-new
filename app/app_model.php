<?php

/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       cake
 * @subpackage    cake.app
 */
class AppModel extends Model {

  //  public $customFinds = array('treepath');

    /**
     * Para uso no after find,
     * utilizada para saber se ele ta
     * te passando um result de count ou
     * dados.
     *
     * @author Luan Garcia
     * @param array $results
     * @return boolean Foi feito um count
     */
    public function isCount($results) {
        if (count($results) == 1 && isset($results[0][0]) &&
                (array_key_exists("count", $results[0][0]) || array_key_exists("COUNT", $results[0][0]))) {
            return true;
        } else {
            return false;
        }
    }
	 function checkUnique($data, $fields){
		$this->recursive = -1;
		$found = $this->find(array("{$this->name}.$fields" => $data));
		$same = isset($this->id) && $found[$this->name][$this->primaryKey] == $this->id;
		return !$found || $found && $same;
	}
	 /**
     * Return menu
     */
     public function _menu($type = 'admin',$parent_id = false) {
		if($type=='site') {
			if(is_numeric($parent_id)){
				$conditions['conditions'] = array($this->name . '.status' => true,$this->name  .'.parent_id' => $parent_id);
			}else{
				$conditions['conditions'] = array($this->name . '.status' => true);
			}
			
			$conditions['order']='sort ASC';
		}else{
			if($parent_id){
				$conditions = array($this->name  .'.parent_id' => $parent_id);
			}else{
				$conditions = array();
			}
			$conditions['order']='sort ASC';
		}
		$conditions['recursive']= -1;
		$data = $this->find('threaded', $conditions);

		return $data;
    }


    function beforeSave($options = array()) {
        parent::beforeSave($options);
        $this->data[$this->name]['site_id'] = Configure::read('Loja.id');
       
        return true;
    }

    function beforeFind($queryData) {
        
        // verifica se existe a coluna de site_id
        parent::beforeFind($queryData);
        if (parent::hasField('site_id')) {
            foreach ($queryData as $chave => $valor) {
                if ($chave == 'conditions') {
                    // seta o filtro por site
                    // o login é universal
                    if($this->name!="Usuario"){
                        $queryData[$chave][$this->name.'.site_id'] = Configure::read('Loja.id');
                    }
                }
            }
        }
        return $queryData;
    }

    public function onError() {   
        $caller = debug_backtrace(false);   
        $inf = "";   

        if(isset($caller[2]["file"]) && isset($caller[2]["line"])) {
            $inf = " - File:". $caller[2]["file"] . " - Line:" . $caller[2]["line"];   
        }   

        $src = $this->getDataSource();   
        $this->log($this->name . " error: " . (isset($src->error)? " (" . $src->error . ")" : "") . $inf, 'ERRO_MYSQL_'.date("d_m_y"));
    }        

}
