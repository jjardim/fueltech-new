<?php

class AppController extends Controller {

    var $uses = array('Setting');
    var $cacheAction = array('/home/index' => '1 hour');

    public function __construct(CakeRequest $request = null, CakeResponse $response = null) {

    	if (preg_match('/busca/', $_GET['url'])) {
            $this->components[] = 'Acl';
            $this->components[] = 'Auth';
            $this->components[] = 'Acl.AclFilter';
            $this->components[] = 'Session';
            $this->helpers[] = 'Javascript';
            $this->helpers[] = 'Html';
            $this->helpers[] = 'Form';
            $this->helpers[] = 'Marcas';
            $this->components[] = 'Carrinho.Carrinho';
            $this->components[] = 'Ssl';
            
            $this->helpers[] = 'String';
        } else {
            $this->components[] = 'Importacao';
            $this->components[] = 'Acl';
            $this->components[] = 'Auth';
            $this->components[] = 'Acl.AclFilter';
            $this->components[] = 'Session';
            $this->components[] = 'Carrinho.Carrinho';
            $this->helpers[] = 'Javascript';
            $this->helpers[] = 'Html';
            $this->helpers[] = 'Form';
            $this->helpers[] = 'Session';
            $this->helpers[] = 'String';
            $this->helpers[] = 'Marcas';
            $this->helpers[] = 'Layout';
            $this->components[] = 'Ssl';
        }
        parent::__construct($request, $response);

        if (isset($_GET['term'])) {
            $this->loadModel('Busca');
            $buscas = $this->Busca->find('all', array(
                'recursive' => -1,
                'limit' => 10,
                'order' => array('quantidade_busca' => 'DESC'),
                'fields' => array('Busca.palavra_chave,Busca.quantidade_itens'),
                'conditions' => array("OR" => array("Busca.palavra_chave LIKE '" . addslashes($_GET['term']) . "%'"))
                    )
            );
            $json = array();
            foreach ($buscas as $busca) {
                $json[] = array(
                    'label' => $busca['Busca']['palavra_chave'],
                    'quantidade_itens' => $busca['Busca']['quantidade_itens']
                );
            }
            die(json_encode($json));
        }
    }

    public function carregaMenuSite() {
        //se tiver nas categorias, considera os niveis e parent_id
        $this->loadModel('Categoria');
        $conditions = array();
        if ($this->params['controller'] == 'categorias') {
            $this->Categoria->Behaviors->detach('Tree');
            //no cache
            $this->Categoria->unbindModel(array('hasMany' => array('ProdutoCategoria', 'SubCategory'), 'belongsTo' => array('FatherCategory'), 'hasAndBelongsToMany' => array('CategoriaAtributo', 'ProdutoCategoria')));
            $categoria_route = $this->Categoria->find('first', array('limit' => 1, 'recursive' => -1, 'conditions' => array('Categoria.seo_url' => implode('/', $this->params['pass']))));
            $niveis = count(explode('/', $categoria_route['Categoria']['seo_url']));
            $categoria_id = $categoria_route['Categoria']['id'];
            $parent_id = $categoria_route['Categoria']['parent_id'];
            $this->params['categoria_id'] = $categoria_id;
            $this->params['parent_id'] = $parent_id;
            $this->params['niveis'] = $niveis;

            // if ($niveis == 1) {
            // $id = $categoria_id;
            // $conditions['Categoria.id'] = (int) $id;
            // }        
            // if ($niveis == 2) {
            // $id = $parent_id;
            // $conditions['Categoria.id'] = (int) $id;
            // }            
            if ($niveis == 1) {
                $id = $categoria_id;
                $conditions['Categoria.parent_id'] = (int) $id;
            } elseif ($niveis == 2) {
                $url = explode('/',$categoria_route['Categoria']['seo_url']);
                if($url[0] == 'ftx'){
                    $conditions['Categoria.id'] = (int) $categoria_route['Categoria']['parent_id'];
                }else{
                    $conditions['Categoria.id'] = (int) $categoria_id;
                }
            } elseif ($niveis >= 3) {
                $id = $parent_id;
                $conditions['Categoria.id'] = (int) $id;
               
                if($this->Categoria->find('first', array('limit' => 1, 'recursive' => -1, 'conditions' => array('Categoria.parent_id' => $categoria_id)))){
                    $conditions['Categoria.id'] = (int) $categoria_id;

                    // cria variavel que ira indicar que exite mais um sub nível
                    $subNivel = true;
                    $this->set(compact('subNivel'));
                }
         
            }
          
            $this->set('niveis', $niveis);
        } else {
            //senao, desconsidera ambos
            $categoria_id = '';
            $conditions['Categoria.parent_id'] = 0;
        }

        $menu_topo_temp = null;
        $menu_topo = null;

        //no cache (menu left)
        $categorias = $this->Categoria->find('all', array(
            'contain' => array('SubCategory' => array('conditions' => array('SubCategory.status' => true))),
            'conditions' => array($conditions)
                )
        );
        //debug($categorias);die;
        //start ordenacao das categorias
        $categorias_temp = $categorias;
        foreach ($categorias_temp as $key => $categoria) {
            $categorias_temp[$key]['SubCategory'] = $this->Categoria->sort_categorias($categoria['SubCategory'], 'sort', SORT_ASC, 'subcategorias');
        }
        $categorias = $this->Categoria->sort_categorias($categorias_temp, 'nome', SORT_ASC, 'categorias');
        //end ordenacao das categorias      
        //menu de categorias do topo
        if (Cache::read('menu_topo') === false) {
            if ($menu_topo == null) {

                $menu_topo = $this->Categoria->find('all', array(
                    'fields' => array('Categoria.id', 'Categoria.nome', 'Categoria.seo_url', 'CategoriaDescricao.nome'),
                    'recursive' => -1,
                    'conditions' => array('Categoria.destaque_menu' => 1, 'CategoriaDescricao.language' => Configure::read('Config.language')),
                    'order' => array('Categoria.ordem_menu ASC'),
                    'joins' => array(
                        array(
                            'table' => 'categoria_descricoes',
                            'alias' => 'CategoriaDescricao',
                            'type' => 'LEFT',
                            'conditions' => 'CategoriaDescricao.categoria_id = Categoria.id'
                        )
                    )
                        )
                );
            }
            // cache
            Cache::write('menu_topo', $menu_topo);
        } else {
            $menu_topo = Cache::read('menu_topo');
        }
        $this->set('menu_topo', $menu_topo);
        $this->set('menu', $categorias);
    }

    function importacoes($get) {
        App::import('Component', 'Importacao');
        $this->Importacao = new ImportacaoComponent();

        switch ($get) {
            case 'atualiza_erp_produto':
                $this->Importacao->importa_atualizada_erp_produto();
                break;
            
            case 'revendedor':
                $this->Importacao->importa_revendedores();
                break;

            case 'parceiro':
                $this->Importacao->importa_parceiros();
                break;

            case 'categorias':
                $this->Importacao->importa_categorias();
                break;

            case 'produtos':
                $this->Importacao->importa_produtos();
                break;

            case 'variacoes':
                $this->Importacao->importa_variacoes();
                break;

            case 'noticias':
                $this->Importacao->importa_noticias();
                break;

            case 'galerias':
                $this->Importacao->importa_galerias();
                break;

            case 'veiculos':
                $this->Importacao->importa_veiculos();
                break;

            case 'videos':
                $this->Importacao->importa_videos();
                break;

            case 'downloads':
                $this->Importacao->importa_downloads();
                break;

            case 'atributos':
                $this->Importacao->importa_atributos();
                break;
            case 'estoque':
                $this->Importacao->estoque();
                break;    
            case 'atualiza_preco':
                $this->Importacao->atualiza_preco();
                break;
			case 'teste':
				$this->log('teste','teste_cron');
				break;
        }
    }

    public function beforeFilter() {
        //die('Site em manutenção, aguarde.');
        if (@isset($_GET['importacao'])) {
            $this->importacoes($_GET['importacao']);
            die;
        }
        //debug($this->params);

        if (!preg_match('/busca/', $_GET['url'])) {
            if (in_array($this->action, array('admin_img_add', 'admin_img_edit')) && isset($this->params['named']['sid'])) {
                Configure::write('Security.level', 'medium');
                $this->Session->id($this->params['named']['sid']);
                $this->Session->start();
            }
        }

        if (!isset($this->params['admin'])) {

            if ($this->Session->read('linguagem_default') == "") {
                App::import('Model', 'Linguagem');
                $this->Linguagem = new Linguagem();
                $lingua = $this->Linguagem->find("first", array('recursive' => -1, 'fields' => 'Linguagem.codigo', 'conditions' => array('Linguagem.default' => true)));
                $this->Session->write('linguagem_default', $lingua['Linguagem']['codigo']);
            }

            //seto a linguagem qndo carrega o site
            if (isset($this->params['language'])) {

                App::import('Model', 'Linguagem');
                $this->Linguagem = new Linguagem();

                $linguas = $this->Linguagem->find("all", array('recursive' => -1, 'fields' => 'Linguagem.codigo', 'conditions' => array('Linguagem.status' => true)));
                $linguas = Set::extract('/Linguagem/codigo', $linguas);

                if (substr($this->referer(), 0, strlen($this->referer())) == '/') {
                    $refer = substr($this->referer(), 1, strlen($this->referer()) - 1);
                } else {
                    $refer = $this->referer();
                }

                if (in_array($this->params['language'], $linguas) && $refer != null && !in_array(substr($refer, 1), $linguas)) {
                    Configure::write('Config.language', $this->params['language']);
                    $this->Session->write('LANGUAGE', $this->params['language']);

                    //$this->redirect($refer);
                    if (stripos($refer, $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']) == false) {
                        $this->redirect('/');
                    } else {
                        $this->redirect($refer);
                    }
                } else {
                    $lingua = $this->Linguagem->find("first", array('recursive' => -1, 'fields' => 'Linguagem.codigo', 'conditions' => array('Linguagem.default' => true)));
                    Configure::write('Config.language', $lingua['Linguagem']['codigo']);
                    $this->Session->write('LANGUAGE', $lingua['Linguagem']['codigo']);
                }
            } else {
                if ($this->Session->read('LANGUAGE') != null) {
                    Configure::write('Config.language', $this->Session->read('LANGUAGE'));
                } else {
                    Configure::write('Config.language', $this->Session->read('linguagem_default'));
                }
            }
        }


        //load dos configs
        $this->Setting->writeConfiguration();

        $this->AclFilter->auth();
        $this->set('auth', $this->Auth->user());

        if (isset($this->params['admin']) && $this->params['admin'] == true) {
            $this->layout = 'admin';
            $this->CarregaMenuAdmin();
        } else {

            $passo_carrinho = array('carrinho', 'usuarios', 'usuario_enderecos');

            $this->carregaPaginas();
            $this->carregaMenuSite();
            $this->carregaParcelamento();
            $this->carregaBanners();
            $this->carregaUltimasNoticias();
            $this->carregaIdiomas();
            $country = $this->getIpFrom();

            if (isset($country) && $country != 'Brazil') {
            	Header("HTTP/1.1 301 Moved Permanently");
            	Header("Location: http://www.fueltech.net");
            }

            //quantidade itens carrinho
            $numero_itens_carrinho = 0;
            if (is_array($this->Carrinho->getItens()) && count($this->Carrinho->getItens()) > 0) {
                foreach ($this->Carrinho->getItens() as $quantidade) {
                    $numero_itens_carrinho += $quantidade['quantidade'];
                }
            }

            $this->set('carrinho_itens', $numero_itens_carrinho);
        }
        parent::beforeFilter();
    }

    function beforeRender() {
    	/*if(@$_SERVER['REMOTE_ADDR']!='179.219.197.193'){
    	 header('Location: http://www.fueltech.com.br/aguarde.html');exit;
    	}*/
	   parent::beforeRender();
       if ($_SERVER['SERVER_NAME'] != 'localhost' && $_SERVER['SERVER_NAME'] != '192.168.4.2' && $_SERVER['SERVER_NAME'] != 'externo.reweb.com.br') {
           $this->WWWforce();
       }
    
       if ($_SERVER['SERVER_NAME'] != 'localhost' && $_SERVER['SERVER_NAME'] != 'externo.reweb.com.br') {
           $controller = array('carrinho', 'usuarios');
           if ($this->params['action'] == "finalizacao") {
               $this->Ssl->force();
           } else {
               if (!isset($this->params['admin']) && in_array($this->params['controller'], $controller) && $this->params['action'] != "add_model_voltagem" && $this->params['action'] != "get_frete_em_detalhes") {
                  $this->Ssl->force();
               } else {
                  $this->Ssl->unforce();
               }
           }
       }       

       //redirecionamentos 301 permanentes
        //if($_SERVER['SERVER_NAME'] == 'www.fteducation.com.br'){             
          /// verificar com o cliente este apontamento
          //  $this->redirect('http://www.fueltech.com.br/ft-education',301);
        ///}        
       
       if(preg_match('/ftxturbo.com|ftxracing.com|ftx.com.br/', env('SERVER_NAME'))){
            $this->redirect('http://www.fueltech.com.br'. env('REQUEST_URI'),301);
       }
    
   }
    public function WWWforce() {
        $url = env('SERVER_NAME') . env('REQUEST_URI');

            if (!preg_match('/www/', $url)) {
                $url = 'http://www.' . $url;
                $this->redirect($url, 301);
            }

    }

    /**
     * Carrega o menu do site
     *  @return Array
     */
    public function carregaMenuAdmin() {
        $this->menus_for_layout = array();
        App::import('Model', 'Link');
        $this->Link = new Link();
        App::import('Model', 'Menu');
        $this->Menu = new Menu();
        $menus = $this->Menu->find('list', array('fields' => array('alias')));

        $usuario = $this->Auth->user();
        $this->roleId = $usuario['Usuario']['grupo_id'];
        foreach ($menus AS $menuAlias) {
            $menu = $this->Link->Menu->find('first', array(
                'conditions' => array(
                    'Menu.status' => 1,
                    'Menu.alias' => $menuAlias,
                    'Menu.link_count >' => 0,
                ),
                'cache' => array(
                    'name' => 'menu_' . $menuAlias,
                    'config' => 'menus',
                ),
                'recursive' => '-1',
            ));
            if (isset($menu['Menu']['id'])) {
                $this->menus_for_layout[$menuAlias] = array();
                $this->menus_for_layout[$menuAlias]['Menu'] = $menu['Menu'];
                $findOptions = array(
                    'conditions' => array(
                        'Link.menu_id' => $menu['Menu']['id'],
                        'Link.status' => 1,
                        'AND' => array(
                            array(
                                'OR' => array(
                                    'Link.visibility_grupos' => '',
                                    'Link.visibility_grupos LIKE' => '%"' . $this->roleId . '"%',
                                ),
                            ),
                        ),
                    ),
                    'order' => array(
                        'Link.lft' => 'ASC',
                    ),
                    'recursive' => -1,
                );
                $links = $this->Link->find('threaded', $findOptions);
                $this->menus_for_layout[$menuAlias]['threaded'] = $links;
            }
        }

        $this->set('menus_for_layout', $this->menus_for_layout);
    }

    /**
     * Carrega as categorias do menu
     * @return Array
     */
    public function carregaCategoriasMenu() {
        App::import('Model', 'Categoria');
        $this->Categoria = new Categoria();
        $this->Categoria->unbindModel(array('hasMany' => array('ProdutoCategoria', 'SubCategory'), 'belongsTo' => array('FatherCategory'), 'hasAndBelongsToMany' => array('CategoriaAtributo', 'ProdutoCategoria')));

        $categorias = $this->Categoria->find('all', array('recursive' => -1, 'conditions' => array('Categoria.status' => true, 'Categoria.parent_id' => 0), 'order' => 'Categoria.sort ASC'));
        $this->set('categoria_menu', $categorias);
        $this->set('categorias_busca', $categorias);
    }

    /**
     * Carrega os Idiomas
     *  @return Array
     */
    public function carregaIdiomas() {
        if (Cache::read('menu_idiomas') === false) {
            App::import('Model', 'Linguagem');
            $this->Linguagem = new Linguagem();
            $menu_idiomas = $this->Linguagem->find('all', array('fields' => array('Linguagem.nome', 'Linguagem.codigo', 'Linguagem.thumb_filename', 'Linguagem.thumb_dir', 'Linguagem.externo', 'Linguagem.url'), 'conditions' => array('Linguagem.status' => true)));
            Cache::write('menu_idiomas', $menu_idiomas);
        } else {
            $menu_idiomas = Cache::read('menu_idiomas');
        }
        $this->set('menu_idiomas', $menu_idiomas);
    }

    /**
     * Carrega o menu de exibi√ß√£o de Banners
     *  @return Array
     */
    public function carregaBanners() {
        App::import('Model', 'Banner');
        App::import('Model', 'BannerCategoria');

        $Banner = new Banner();
        $BannerCategoria = new BannerCategoria();
        if (Cache::read('banners') === false) {

            $banners = $Banner->find('all', array('conditions' => array('Banner.status' => true), 'order' => 'coalesce(Banner.ordem,10000) ASC'));
            Cache::write('banners', $banners);
        } else {
            $banners = Cache::read('banners');
        }

        //banners normais
        //banner direita
        $banners_direita = $this->extraiBanners($banners, "banner_direita");
        if (is_array($banners_direita) && count($banners_direita) > 0) {
            $this->set('banners_direita', $banners_direita);
        }

        //banner banners_rodape
        $banners_rodape = $this->extraiBanners($banners, "banner_rodape");
        if (count($banners_rodape) > 1) {
            $this->set('banners_rodape', $banners_rodape[rand(0, count($banners_rodape))]);
        } else {
            $this->set('banners_rodape', $banners_rodape);
        }

        //banner banners_cabecalho
        $banners_cabecalho = $this->extraiBanners($banners, "banner_cabecalho");
        if (count($banners_cabecalho) > 1) {
            $this->set('banners_cabecalho', $banners_cabecalho[rand(0, count($banners_cabecalho))]);
        } else {
            $this->set('banners_cabecalho', $banners_cabecalho);
        }

        //banner esquerda
        $banners_esquerda = $this->extraiBanners($banners, "banner_esquerda");
        if (count($banners_esquerda) > 1) {
            $this->set('banners_esquerda', $banners_esquerda[rand(0, count($banners_esquerda))]);
        } else {
            $this->set('banners_esquerda', $banners_esquerda);
        }

        //debug($banners_esquerda);die;
        if ($this->params['controller'] == 'home') {

            //banners normais
            $this->set('banners_topo', $this->extraiBanners($banners, "banner_topo"));

            //banner vitrine
            $banners_vitrines = $this->extraiBanners($banners, "banner_vitrine");
            if (is_array($banners_vitrines) && count($banners_vitrines) > 0) {
                $this->set('banners_vitrine', $banners_vitrines[rand(0, count($banners_vitrines) - 1)]);
            } else {
                $this->set('banners_vitrine', $banners_vitrines);
            }
        } elseif ($this->params['controller'] == 'categorias') {
            //ids categoria atual
            if (Cache::read('categorias_routes') === false) {

                $this->Categoria->unbindModel(array('hasMany' => array('ProdutoCategoria', 'SubCategory'), 'belongsTo' => array('FatherCategory'), 'hasAndBelongsToMany' => array('CategoriaAtributo', 'ProdutoCategoria')));
                $categoria = $this->Categoria->find('first', array('recursive' => -1, 'conditions' => array('Categoria.seo_url' => implode('/', $this->params['pass']))));
                Cache::write('categorias_routes', $categoria);
            } else {
                $categoria = Cache::read('categorias_routes');
            }
            $this->Categoria->Behaviors->attach('Tree');

            // $banners_ids = $BannerCategoria->find('list', array('recursive' => -1, 'fields' => array('banner_id'), 'conditions' => array('BannerCategoria.categoria_id' =>am($categoria['Categoria']['id'], set::extract('/Categoria/id', $this->Categoria->children($categoria['Categoria']['id']))))));;

            $banners_ids = $BannerCategoria->find('list', array(
                'recursive' => -1,
                'fields' => array('banner_id'),
                'conditions' => array('BannerCategoria.categoria_id' => $this->params['categoria_id'])
                    )
            );

            //banners normais
            $this->set('banners_topo_categoria', $this->extraiBanners($banners, "banner_topo", 0, $banners_ids));
        }
    }

    /**
     * Carrega as perguntas frequentes para exibi√ß√£o no menu
     * @return Array
     */
    public function carregaFaq() {

        if (Cache::read('faq') === false) {
            App::import('Model', 'Faq');
            $this->Faq = new Faq();

            $faq = $this->Faq->find('all', array('contain' => array('FaqPergunta' => array('conditions' => array('FaqPergunta.status' => true))), 'conditions' => array('Faq.status' => true)));
            Cache::write('faq', $faq);
        } else {
            $faq = Cache::read('faq');
        }
        $this->set('faqs_lista', $faq);
    }

    /**
     * Carrega as p√°ginas estaticas
     * @return Array
     */
    public function carregaPaginas() {

        if (Cache::read('paginas') === false) {
            App::import('Model', 'Pagina');
            $this->Pagina = new Pagina();

            $paginas = $this->Pagina->find('all', array(
                'contain' => array('SubPage'),
                'fields' => array('id', 'PaginaDescricao.nome', 'url', 'categoria'),
                'conditions' => array('Pagina.parent_id' => 0, 'Pagina.status' => true, 'Pagina.visivel_menu' => true, 'PaginaDescricao.language' => Configure::read('Config.language')),
                'joins' => array(
                    array(
                        'table' => 'pagina_descricoes',
                        'alias' => 'PaginaDescricao',
                        'type' => 'LEFT',
                        'conditions' => 'PaginaDescricao.pagina_id = Pagina.id'
                    )
                ),
                    )
            );
            Cache::write('paginas', $paginas);
        } else {
            $paginas = Cache::read('paginas');
        }
        $this->set('paginas_lista', $paginas);
        //debug($paginas);
    }

    /**
     * Carrega os fabricantes
     * @return Array
     */
    public function carregaFabricantes() {

        if (Cache::read('fabricantes') === false) {
            App::import('Model', 'Fabricante');
            $this->Fabricante = new Fabricante();

            $fabricantes = $this->Fabricante->find('all', array('contain' => array('FabricanteImagem'), 'conditions' => array('status' => true)));
            Cache::write('fabricantes', $fabricantes);
        } else {
            $fabricantes = Cache::read('fabricantes');
        }
        $this->set('fabricantes_logos', $fabricantes);
    }

    /**
     * Carrega as parcelamento
     * @return Array
     */
    public function carregaParcelamento() {

        if (Cache::read('geral_parcelamento') === false) {
            App::import('Model', 'PagamentoCondicao');
            $this->PagamentoCondicao = new PagamentoCondicao();

            $parcelas = $this->PagamentoCondicao->find('first', array('recursive' => -1, 'conditions' => array('PagamentoCondicao.status' => true)));

            Cache::write('geral_parcelamento', $parcelas);
        } else {
            $parcelas = Cache::read('geral_parcelamento');
        }
        $this->set('parcelamento', $parcelas['PagamentoCondicao']);
    }

    /**
     * Carrega as noticias da home
     * @return Array
     */
    public function carregaUltimasNoticias() {
        if (Cache::read('ultimas_noticias') === false) {
            App::import('Model', 'Noticia');
            $this->Noticia = new Noticia();
            $ultimas_noticias = $this->Noticia->find('all', array(
                'fields' => array('Noticia.id', 'Noticia.titulo', 'Noticia.descricao', 'Noticia.thumb_filename', 'Noticia.thumb_dir', 'Noticia.created'),
                'recursive' => -1,
                'conditions' => array('Noticia.destaque_home' => true, 'Noticia.status' => true, 'Noticia.language' => Configure::read('Config.language')),
                'limit' => 6,
                'order' => array('Noticia.created DESC')
                    )
            );
            Cache::write('ultimas_noticias', $ultimas_noticias);
        } else {
            $ultimas_noticias = Cache::read('ultimas_noticias');
        }
        $this->set('ultimas_noticias', $ultimas_noticias);
    }

    /**
     * Carrega os twits
     * @return Array
     */
    public function carregaTwitter() {
        App::import('Model', 'Twitter');
        $this->Twitter = new Twitter();
        $this->set('twitter', $this->Twitter->find(array('limit' => 3)));
    }

    function arraySortRecursive(&$arr) {
        ksort($arr);
        foreach ($arr as &$a) {
            if (is_array($a) && !empty($a)) {
                $this->arraySortRecursive($a);
            }
        }
    }

    /**
     * Filtra os banners do array de banners atraves dos tipos  e ids
     * @return Array
     */
    private function extraiBanners($array_banners, $tipo_banner, $capa = 1, $banners_ids = null) {
        $return = "";
        foreach ($array_banners as $key => $v) {
            if ($banners_ids == null && $capa == 1) {
                if ($v['BannerTipo']['codigo'] == $tipo_banner && $v['Banner']['capa'] == $capa) {
                    $return[] = $v;
                }
            } elseif (is_array($banners_ids) && count($banners_ids) > 0) {
                foreach ($banners_ids as $id) {
                    if ($v['Banner']['id'] == $id) {
                        $return[] = $v;
                    }
                }
            }
        }
        return $return;
    }

    /**
     * get if is from
     */
    public function getIpFrom() {
    	//http://api.ipinfodb.com/v3/ip-city/?key=b00818573dbad8fd1319af42c0770503e98cbfec6c6532091cb11cd1c8e6a5a3&ip=179.219.198.224&format=json
    	/* http://www.nirsoft.net/countryip/ - testar IP */
    	$ipaddress = $_SERVER['REMOTE_ADDR'];
    	$api_key = 'b00818573dbad8fd1319af42c0770503e98cbfec6c6532091cb11cd1c8e6a5a3';

    	$data = file_get_contents("http://api.ipinfodb.com/v3/ip-city/?key=$api_key&ip=$ipaddress&format=json");
    	$data = json_decode($data, true);

    	if (isset($data['countryName']) && $data['countryName'] != null) {
    		if ($data['countryName'] == '-') {
    			$country = null;
    		} else {
    			$country = $data['countryName'];
    		}
    	} else {
    		$country = null;
    	}
    	return $country; 
    }

    function getQueries() {
    	Configure::write('debug', 2);
    	$db = ConnectionManager::getDataSource('default');
    	$logs = debug($db->getLog());
    	if (is_array($logs)) {
    		return end($logs);
    	} else {
    		return false;
    	}
    	 
    }

}
