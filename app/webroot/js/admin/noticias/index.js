function myCustomFileBrowser(field_name, url, type, win) {
    browserField = field_name;
    browserWin = win;
    window.open(PATH.basename + "/admin/imagens/add", 'browserWindow', 'modal,width=600,height=400,scrollbars=yes');
}
$(function() {
		$.datepicker.regional['pt-BR'] = {
			closeText: 'Fechar',
			prevText: '&#x3c;Anterior',
			nextText: 'Pr&oacute;ximo&#x3e;',
			currentText: 'Hoje',
			monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
			'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
			'Jul','Ago','Set','Out','Nov','Dez'],
			dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sabado'],
			dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
			dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
			weekHeader: 'Sm',
			dateFormat: 'dd/mm/yy',
			firstDay: 0,
			isRTL: false,
			showMonthAfterYear: false,
			yearSuffix: ''
		};
        $.datepicker.setDefaults($.datepicker.regional['pt-BR']);
    $('.datePicker').datepicker();

    tinyMCE.init({
	relative_urls: false,
	convert_urls: false,
	mode: "textareas",
	theme: "advanced",
	editor_selector: "mceEditor",
	file_browser_callback: "myCustomFileBrowser"
    });
});