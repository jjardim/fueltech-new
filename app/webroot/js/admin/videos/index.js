/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function() {
   		$.datepicker.regional['pt-BR'] = {
			closeText: 'Fechar',
			prevText: '&#x3c;Anterior',
			nextText: 'Pr&oacute;ximo&#x3e;',
			currentText: 'Hoje',
			monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
			'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
			'Jul','Ago','Set','Out','Nov','Dez'],
			dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sabado'],
			dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
			dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
			weekHeader: 'Sm',
			dateFormat: 'dd/mm/yy',
			firstDay: 0,
			isRTL: false,
			showMonthAfterYear: false,
			yearSuffix: ''
		};
        $.datepicker.setDefaults($.datepicker.regional['pt-BR']);
    $('.datePicker').datepicker();

    if ($("input[name='data[Video][thumb_default]']:checked").val() == '1') {
	//$("#VideoThumbFilename").attr("disabled", "disabled").parent().hide();
	$("#VideoThumbFilename").parent().hide();
    } else {
	//$("#VideoThumbFilename").attr("disabled", "").parent().show();
	$("#VideoThumbFilename").parent().show();
    }
    $("input[name='data[Video][thumb_default]']").change(function() {
	if ($("input[name='data[Video][thumb_default]']:checked").val() == '1') {
	    //$("#VideoThumbFilename").attr("disabled", "disabled").parent().hide();
	    $("#VideoThumbFilename").parent().hide();
	} else {
	    //$("#VideoThumbFilename").attr("disabled", "").parent().show();
	    $("#VideoThumbFilename").parent().show();
	}
    });

    if ($("input[name='data[Video][thumb_default2]']").is(':checked')) {
	$("#VideoThumbFilename").attr("disabled", "disabled").parent().hide();
    }
    $("input[name='data[Video][thumb_default2]']").change(function() {
	if ($("input[name='data[Video][thumb_default2]']").is(':checked')) {
	    $("#VideoThumbFilename").attr("disabled", "disabled").parent().hide();
	} else {
	    $("#VideoThumbFilename").attr("disabled", "").parent().show();
	}
    });

});