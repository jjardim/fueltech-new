$(function() {

		$.datepicker.regional['pt-BR'] = {
			closeText: 'Fechar',
			prevText: '&#x3c;Anterior',
			nextText: 'Pr&oacute;ximo&#x3e;',
			currentText: 'Hoje',
			monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
			'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
			'Jul','Ago','Set','Out','Nov','Dez'],
			dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sabado'],
			dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
			dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
			weekHeader: 'Sm',
			dateFormat: 'dd/mm/yy',
			firstDay: 0,
			isRTL: false,
			showMonthAfterYear: false,
			yearSuffix: ''
		};
        $.datepicker.setDefaults($.datepicker.regional['pt-BR']);


		$('.mask-numerico').setMask({
			mask:'9',
			type:'repeat'
		});
		$('.mask-moeda').setMask({
			mask : '99,999.999.999.999',
			type : 'reverse'
		});
   
		$('.datePicker').datepicker();
			
		if($("#CupomCupomTipoId").val()=='1'){
			$("#query").val(null).parent().show();
             $("#CupomProdutoId").attr("disabled", "").parent().show();
			$("#CupomCategoriaId").attr("disabled", "disabled").parent().hide();			            
        }else if($("#CupomCupomTipoId").val()=='2'){
            $("#query").val(null).parent().hide();
			$("#CupomCategoriaId").attr("disabled", "").parent().show();
			$("#CupomProdutoId").attr("disabled", "disabled").parent().hide();
            
        }else if($("#CupomCupomTipoId").val()=='3'){
			$("#query").val(null).parent().hide();
            $("#CupomProdutoId").attr("disabled", "disabled").parent().hide();			
			$("#CupomCategoriaId").attr("disabled", "disabled").parent().hide();			
		}

    $("#CupomCupomTipoId").change(function(){
        if($(this).val()=='1'){
			$("#query").val(null).parent().show();
            $("#CupomProdutoId").attr("disabled", "").parent().show();
			$("#CupomCategoriaId").attr("disabled", "disabled").parent().hide();			
           
        }else if($(this).val()=='2'){
			$("#query").val(null).parent().hide();
            $("#CupomCategoriaId").attr("disabled", "").parent().show();
			$("#CupomProdutoId").attr("disabled", "disabled").parent().hide();
           
        }else if($(this).val()=='3'){
			$("#query").val(null).parent().hide();
			$("#CupomProdutoId").attr("disabled", "disabled").parent().hide();			
			$("#CupomCategoriaId").attr("disabled", "disabled").parent().hide();	
		}
    })
	
	$('#buscar-produtos').click(function(){
		$("#buscar-produtos").addClass('loading');
        query = $('#query').val();
        selecteds = null;
        $.post(PATH.basename + "/admin/produtos/ajax_produtos/",{query:query,selecteds:selecteds},function(values){
            if(values){
                options = new Array();
				options += '<option value="">Selecione</option>';
                $.each(values,function(i,v){
					options += '<option value="'+i+'">'+v+'</option>';
                })
                $('#CupomProdutoId').html(options);
				$("#buscar-produtos").removeClass('loading');
            }
        },'json');
        return false;
    });
  
});