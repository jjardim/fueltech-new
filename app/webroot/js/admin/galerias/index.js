$(function() {
   
    $('#add-produto').uploadify({
		'debug'		: true,
        'uploader'  : PATH.basename + '/swf/uploadify.swf',
        'script'    : PATH.basename + '/admin/galerias/img_add/sid:'+PATH.sid,
        'cancelImg' : PATH.basename+'/img/icons/delete.png',
        'auto'      : true,
		'multi'     : true,
		'fileExt'        : '*.jpg;*.gif;*.png',
		'fileDesc'       : 'Image Files (.JPG, .GIF, .PNG)',
        'buttonText':'Enviar Arquivo',
        'sizeLimit' : 104857600,
        'onComplete': function(event, queueID, fileObj, response, data) {
			$('.container-add').html(response);						
        },
        'onError': function (a, b, c, d) {
            if (d.status == 404)
                alert('Erro ao enviar arquivo');
            else if (d.type === "HTTP")
                alert('Erro '+d.type+": "+d.status);
            else if (d.type ==="File Size")
                alert('Arquivo muito grande, suportado somente até 100MB');
            else
                alert('error '+d.type+": "+d.text);
        }
	});   
	$('#edit-produto').uploadify({
        'uploader'  : PATH.basename + '/swf/uploadify.swf',
        'script'    : PATH.basename + '/admin/galerias/img_edit/'+$('#GaleriaId').val()+'/sid:'+PATH.sid,
        'cancelImg' : PATH.basename+'/img/icons/delete.png',
        'auto'      : true,
		'multi'     : true,
		'fileExt'        : '*.jpg;*.gif;*.png',
		'fileDesc'       : 'Image Files (.JPG, .GIF, .PNG)',
        'buttonText':'Enviar Arquivo',
        'sizeLimit' : 104857600,
        'onComplete': function(event, queueID, fileObj, response, data) {
            $('.container-edit').html(response);						
        },
        'onError': function (a, b, c, d) {
            if (d.status == 404)
                alert('Erro ao enviar arquivo');
            else if (d.type === "HTTP")
                alert('Erro '+d.type+": "+d.status);
            else if (d.type ==="File Size")
                alert('Arquivo muito grande, suportado somente até 100MB');
            else
                alert('error '+d.type+": "+d.text);
        }
	});   
   
    $('#add').click(function(){
        options = '';
        $.each($('#buscados option:selected'),function(i,v){
            options += '<option value="'+v.value+'">'+v.text+'</option>';
            $('#buscados option:selected').remove();
        })
        $('#selecionados').append(options);
        $("#selecionados option").attr("selected", "selected");
        return false;
       
    })
	
	$('.rm-img').live("click",function(){
		img = $(this);
        if(confirm('deseja remover a imagem?')){
			img.addClass('loading');
			$.post(PATH.basename + "/admin/galerias/rm_img_tmp/"+img.attr('rel'),function(values){
				if(values){
					img.parent().parent().parent().remove();
				}else{
					alert('Erro ao remover Imagem');
				}
			},'json');
		}
    })
	
	$('.rm-img-old').live("click",function(){
		img = $(this);
        if(confirm('deseja remover a imagem?')){
			img.addClass('loading');
			$.post(PATH.basename + "/admin/galerias/rm_img_old/"+img.attr('rel'),function(values){
				if(values){
					img.parent().parent().parent().remove();
				}else{
					alert('Erro ao remover Imagem');
				}
			},'json');
		}
    })
	
	$('.container-edit .ordem').live('blur',function(){
		parent = $(this).parent().parent().parent();
		$('div a',parent).addClass('loading');		
		
		$.post(PATH.basename + "/admin/galerias/img_ordem_old/"+$(this).attr('rel')+"/"+$(this).val(),function(values){
            if(values){
               $('div a',parent).removeClass('loading');
            }else{
				alert('Ocorreu um erro tente novamente.');
				$('div a',parent).removeClass('loading');
			}
        },'json');
    });
	
	$('.container-edit .destaque').live('click',function(){
		parent = $(this).parent().parent().parent();
		$('div a',parent).addClass('loading');		
		
		if($(this).is(':checked')){
			var valor = 1;
		}else{
			var valor = 0;
		}
		
		$.post(PATH.basename + "/admin/galerias/img_destaque_old/"+$(this).attr('rel')+"/"+valor,function(values){
            if(values){
               $('div a',parent).removeClass('loading');
            }else{
				alert('Ocorreu um erro tente novamente.');
				$('div a',parent).removeClass('loading');
			}
        },'json');
    });
	
	$('.container-add .destaque').live('click',function(){
		parent = $(this).parent().parent().parent();
		$('div a',parent).addClass('loading');
		
		if($(this).is(':checked')){
			var valor = 1;
		}else{
			var valor = 0;
		}
		
		$.post(PATH.basename + "/admin/galerias/img_destaque_tmp/"+$(this).attr('rel')+"/"+valor,function(values){
            if(values){
               $('div a',parent).removeClass('loading');
            }else{
				alert('Ocorreu um erro tente novamente.');
				$('div a',parent).removeClass('loading');
			}
        },'json');
    });
	
    $('#rm').click(function(){
        $('#selecionados option:selected').remove();
        $("#selecionados option").attr("selected", "selected");
        return false;
    })
	$("#selecionados option").attr("selected", "selected");
	
});