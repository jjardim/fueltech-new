/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function(){
    $('.mask-cep').setMask({
        mask:'99.999-999'
    });
   $('.mask-telefone-8-9').keypress(function(){
		  mascara_8_9_dig( this, mtel );  
	});
	 $('.mask-telefone').setMask({
        mask:'(99) 9999-99999'
    });
});

//metodos que altera a mascara do campo telefone, permitindo ao usuario colocar 8 ou 9 digitos
function mascara_8_9_dig(o,f){  
	v_obj=o  
	v_fun=f  
	setTimeout("execmascara()",1)  
}  
function execmascara(){  
	v_obj.value=v_fun(v_obj.value)  
}  
function mtel(v){  
	v=v.replace(/\D/g,"");             //Remove tudo o que n�o � d�gito  
	v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca par�nteses em volta dos dois primeiros d�gitos  
	v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca h�fen entre o quarto e o quinto d�gitos  
	return v;  
}  