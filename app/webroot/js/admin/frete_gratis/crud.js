$(function() {
	 $("#FreteGratisCategoriaId option").attr("selected", "selected");
	 $("#FreteGratisProdutoId option").attr("selected", "selected");
	$('.mask-numerico').setMask({mask:'9',type:'repeat'});
    $('.mask-moeda').setMask({mask : '99,999.999.999.999',type : 'reverse'});
    $('.mask-cep').setMask({mask : '99.999-999'});
	  $('#buscar-produtos').click(function(){
		$("#FreteGratisProdutoId option").attr("selected", "selected");
		$("#buscar-produtos").addClass('loading');
        query = $('#FreteGratisBuscar').val();
        cat = $('#FreteGratisProdutoCategoriaId').val();
        selecteds = $('#FreteGratisProdutoId').val();

        $.post(PATH.basename + "/admin/vitrines/ajax_produtos/",{categoria_id:cat,query:query,selecteds:selecteds},function(values){
            if(values){
                options = new Array();
                $.each(values,function(i,v){
					options += '<option value="'+i+'">'+v+'</option>';
                })
                $('#FreteGratisSelecionar').html(options);
            }
			$("#buscar-produtos").removeClass('loading');
        },'json');
		
        return false;
    });
	$('#buscar-categorias').click(function(){
		$("#FreteGratisCategoriaId option").attr("selected", "selected");
		$("#buscar-categorias").addClass('loading');
        query = $('#FreteGratisBuscar2').val();
        selecteds = $('#FreteGratisCategoriaId').val();

        $.post(PATH.basename + "/admin/categorias/ajax_categorias/",{query:query,selecteds:selecteds},function(values){
            if(values){
                options = new Array();
                $.each(values,function(i,v){
					options += '<option value="'+i+'">'+v+'</option>';
                })
                $('#FreteGratisSelecionar2').html(options);
            }
			$("#buscar-categorias").removeClass('loading');
        },'json');
		
        return false;
    });


    $('#add').click(function(){
	   
        options = '';
        $.each($('#FreteGratisSelecionar option:selected'),function(i,v){
            options += '<option value="'+v.value+'">'+v.text+'</option>';
            $('#FreteGratisSelecionar option:selected').remove();
        })
        $('#FreteGratisProdutoId').append(options);
        $("#FreteGratisProdutoId option").attr("selected", "selected");
		
        return false;
       
    })
    $('#rm').click(function(){
        $('#FreteGratisProdutoId option:selected').remove();
        $("#FreteGratisProdutoId option").attr("selected", "selected");

        return false;
    })
	
    $('#add2').click(function(){
	   
        options = '';
        $.each($('#FreteGratisSelecionar2 option:selected'),function(i,v){
            options += '<option value="'+v.value+'">'+v.text+'</option>';
            $('#FreteGratisSelecionar2 option:selected').remove();
        })
        $('#FreteGratisCategoriaId').append(options);
        $("#FreteGratisCategoriaId option").attr("selected", "selected");
		
        return false;
       
    })
    $('#rm2').click(function(){
        $('#FreteGratisCategoriaId option:selected').remove();
        $("#FreteGratisCategoriaId option").attr("selected", "selected");

        return false;
    })
     $("#FreteGratisCategoriaId option").attr("selected", "selected");
	 	if($("#FreteGratisCodigo").val()=='CATEGORIA'){
			$(".box-produtos").hide();
			$(".box-categorias").show();  
			$("#FreteGratisApartirDe").attr("disabled", "").parent().show();
        }else if($("#FreteGratisCodigo").val()=='PRODUTO'){
			$(".box-produtos").show();
			$(".box-categorias").hide();      
			$("#FreteGratisApartirDe").attr("disabled", "disabled").parent().hide();
        }else if($("#FreteGratisCodigo").val()=='CARRINHO'){
			$(".box-produtos").hide();
			$(".box-categorias").hide();  
			$("#FreteGratisApartirDe").attr("disabled", "").parent().show();
        }
	  $("#FreteGratisCodigo").change(function(){
        if($(this).val()=='CATEGORIA'){
			$(".box-produtos").hide();
			$(".box-categorias").show();
			$("#FreteGratisApartirDe").attr("disabled", "").parent().show();
        }else if($(this).val()=='PRODUTO') {
			$(".box-produtos").show();
			$(".box-categorias").hide();
			$("#FreteGratisApartirDe").attr("disabled", "disabled").parent().hide();
        }else if($(this).val()=='CARRINHO') {
			$(".box-produtos").hide();
			$(".box-categorias").hide();
			$("#FreteGratisApartirDe").attr("disabled", "").parent().show();
        }
    })
	 
	   //IdeaIdeas
 


     $.datepicker.regional['pt-BR'] = {
                closeText: 'Fechar',
                prevText: '&#x3c;Anterior',
                nextText: 'Pr&oacute;ximo&#x3e;',
                currentText: 'Hoje',
                monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
                'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
                'Jul','Ago','Set','Out','Nov','Dez'],
                dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sabado'],
                dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
                dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
                weekHeader: 'Sm',
                dateFormat: 'dd/mm/yy',
                firstDay: 0,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''};
        $.datepicker.setDefaults($.datepicker.regional['pt-BR']);




    $('.datePicker').datepicker();
});