$(function() {
 
    $('#buscar-produtos').click(function(){

        query = $('#VitrineBuscar').val();
        cat = $('#VitrineCategoriaId').val();
        selecteds = $('#ProdutoProduto').val();

        $.post(PATH.basename + "/admin/vitrines/ajax_produtos/",{categoria_id:cat,query:query,selecteds:selecteds},function(values){
            if(values){
                options = new Array();
                $.each(values,function(i,v){
					options += '<option value="'+i+'">'+v+'</option>';
                })
                $('#VitrineSelecionar').html(options);
            }
        },'json');
        return false;
    });


    $('#add').click(function(){
        options = '';
        $.each($('#VitrineSelecionar option:selected'),function(i,v){
            options += '<option value="'+v.value+'" selected=selected>'+v.text+'</option>';
            $('#VitrineSelecionar option:selected').remove();
        })
        $("#ProdutoProduto").append(options).change();
        return false;
       
    })
    $('#rm').click(function(){
        $('#ProdutoProduto option:selected').remove();
        $("#ProdutoProduto option").attr("selected", "selected");
        return false;
    })
     $("#ProdutoProduto option").attr("selected", "selected");

    $(".categoria_produtos_sel").asmSelect({
        sortable: true,
        animate: true,
        addItemTarget: 'top'
    });




    if($("#VitrineTipo").val()!='CATEGORIA'){
        $("#VitrineCategoriaId").attr("disabled", "disabled").parent().hide();
    }else{
        $("#VitrineCategoriaId").attr("disabled", "").parent().show();
    }

    $("#VitrineTipo").change(function(){
        if($(this).val()!='CATEGORIA'){
            $("#VitrineCategoriaId").attr("disabled", "disabled").parent().hide();         
        }else{
            $("#VitrineCategoriaId").attr("disabled", "").parent().show();          
        }
    })
});