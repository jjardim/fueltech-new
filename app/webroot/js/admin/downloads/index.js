/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function() {
    $('.mask-moeda').setMask({mask: '99,999.999.999.999', type: 'reverse'});
    //data[Download][thumb]
    if ($("input[name='data[Download][thumb]']:checked").val() == '1') {
        $("#DownloadThumbFilename").attr("disabled", "").parent().show();
    } else {
        $("#DownloadThumbFilename").attr("disabled", "disabled").parent().hide();
    }
    $("input[name='data[Download][thumb]']").change(function() {
        if ($("input[name='data[Download][thumb]']:checked").val() == '1') {
            $("#DownloadThumbFilename").attr("disabled", "").parent().show();
        } else {
            $("#DownloadThumbFilename").attr("disabled", "disabled").parent().hide();
        }
    });  
    
    if ($("input[name='data[Download][upload]']:checked").val() == '1') {
        $("#DownloadFilename").attr("disabled", "").parent().show();
        $("#DownloadNomeArquivo").attr("disabled", "disabled").parent().hide();
        $("#DownloadTamanhoArquivo").attr("disabled", "disabled").parent().hide();
    } else {
        $("#DownloadFilename").attr("disabled", "disabled").parent().hide();
        $("#DownloadNomeArquivo").attr("disabled", "").parent().show();
        $("#DownloadTamanhoArquivo").attr("disabled", "").parent().show();
    }
    $("input[name='data[Download][upload]']").change(function() {
        if ($("input[name='data[Download][upload]']:checked").val() == '1') {
            $("#DownloadFilename").attr("disabled", "").parent().show();
            $("#DownloadNomeArquivo").attr("disabled", "disabled").parent().hide();
            $("#DownloadTamanhoArquivo").attr("disabled", "disabled").parent().hide();
        } else {
            $("#DownloadFilename").attr("disabled", "disabled").parent().hide();
            $("#DownloadNomeArquivo").attr("disabled", "").parent().show();
            $("#DownloadTamanhoArquivo").attr("disabled", "").parent().show();
        }
    });

});