/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function(){

	//begin container-variacoes
	$('.container-variacoes a.add').click(function(){
        $('.container-variacoes').clone(true).appendTo('.container-variacoes-tmp').each(function(){
            $(this).find('.variacoes').html('');
            $(this).find("select").val('');
        })
        $('.container-variacoes-tmp div').each(function(i,v){
            $(this).removeClass('container-variacoes').addClass('container').addClass('container-content');
        })
        $('.container-variacoes-tmp > div').each(function(i,v){
            $('a.rm',this).each(function(a,b){
                $(this).show();
            })
            $('a.add',this).each(function(a,b){
                $(this).hide();
            })
            ++i
            $('input',this).each(function(a,b){
                str = $(this).attr('name').replace(/[0-9]+/g,i);
                $(this).attr('name',str);
            })
            $('select',this).each(function(a,b){
                str = $(this).attr('name').replace(/[0-9]+/g,i);
                $(this).attr('name',str);
            })
        })
        return false;
    });
    $('.container-variacoes-tmp a.rm').live('click',function(){
        $(this).parent().remove();
        return false;
    })
	//end container-variacoes

});