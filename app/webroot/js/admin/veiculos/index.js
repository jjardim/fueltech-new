$(document).ready(function(){
    $('#edit-produto').uploadify({
	    'uploader'  : PATH.basename + '/swf/uploadify.swf',
	    'script'    : PATH.basename + '/admin/galerias/img_edit/'+$('#GaleriaId').val()+'/sid:'+PATH.sid,
	    'cancelImg' : PATH.basename+'/img/icons/delete.png',
	    'auto'      : true,
		    'multi'     : true,
		    'fileExt'        : '*.jpg;*.gif;*.png',
		    'fileDesc'       : 'Image Files (.JPG, .GIF, .PNG)',
	    'buttonText':'Enviar Arquivo',
	    'sizeLimit' : 104857600,
	    'onComplete': function(event, queueID, fileObj, response, data) {
		$('.container-edit').html(response);						
	    },
	    'onError': function (a, b, c, d) {
		if (d.status == 404)
		    alert('Erro ao enviar arquivo');
		else if (d.type === "HTTP")
		    alert('Erro '+d.type+": "+d.status);
		else if (d.type ==="File Size")
		    alert('Arquivo muito grande, suportado somente até 100MB');
		else
		    alert('error '+d.type+": "+d.text);
	    }
    });  

    $('.rm-img').live("click",function(){
		img = $(this);
        if(confirm('deseja remover a imagem?')){
			img.addClass('loading');
			$.post(PATH.basename + "/admin/veiculos/rm_img_tmp/"+img.attr('rel'),function(values){
				if(values){
					img.parent().parent().parent().remove();
				}else{
					alert('Erro ao remover Imagem');
				}
			},'json');
		}
    })
    $('.rm-img-old').live("click",function(){
		img = $(this);
        if(confirm('deseja remover a imagem?')){
			img.addClass('loading');
			$.post(PATH.basename + "/admin/veiculos/rm_img_old/"+img.attr('rel'),function(values){
				if(values){
					img.parent().parent().parent().remove();
				}else{
					alert('Erro ao remover Imagem');
				}
			},'json');
		}
    }) 
    
   tinyMCE.init({mode : "textareas",theme : "advanced",editor_selector :"mceEditor"});
});