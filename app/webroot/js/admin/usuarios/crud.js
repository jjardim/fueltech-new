$(function() {
 
    $('.mask-cep').setMask({
        mask:'99.999-999'
    });
    $('.mask-telefone').setMask({
        mask:'(99) 9999-9999'
    });
    $('.mask-cpf').setMask({
        mask:'999.999.999-99'
    });
    $('.mask-cnpj').setMask({
        mask:'99.999.999.9999/99'
    });
    $('.mask-data').setMask({
        mask:'99/99/9999'
    });

        if($("#UsuarioTipoPessoa").val()=='J'){
            $("#UsuarioSexo").attr("disabled", "disabled").parent().hide();
            $("#UsuarioDataNascimento").attr("disabled", "disabled").parent().hide();
            $("#UsuarioRg").attr("disabled", "disabled").parent().hide();
            $("#UsuarioCpf").attr("disabled", "disabled").parent().hide();
            $("#UsuarioCnpj").attr("disabled", "").parent().show();
            $("#UsuarioNomeFantasia").attr("disabled", "").parent().show();
            $("#UsuarioRazaoSocial").attr("disabled", "").parent().show();
            $("#UsuarioInscricaoEstadual").attr("disabled", "").parent().show();
        }else{
            $("#UsuarioSexo").attr("disabled", "").parent().show();
            $("#UsuarioDataNascimento").attr("disabled", "").parent().show();
            $("#UsuarioRg").attr("disabled", "").parent().show();
            $("#UsuarioCpf").attr("disabled", "").parent().show();
            $("#UsuarioCnpj").attr("disabled", "disabled").parent().hide();
            $("#UsuarioNomeFantasia").attr("disabled", "disabled").parent().hide();
            $("#UsuarioRazaoSocial").attr("disabled", "disabled").parent().hide();
            $("#UsuarioInscricaoEstadual").attr("disabled", "disabled").parent().hide();
        }

    $("#UsuarioTipoPessoa").change(function(){
        if($(this).val()=='J'){
            $("#UsuarioSexo").attr("disabled", "disabled").parent().hide();
            $("#UsuarioDataNascimento").attr("disabled", "disabled").parent().hide();
            $("#UsuarioRg").attr("disabled", "disabled").parent().hide();
            $("#UsuarioCpf").attr("disabled", "disabled").parent().hide();
            $("#UsuarioCnpj").attr("disabled", "").parent().show();
            $("#UsuarioNomeFantasia").attr("disabled", "").parent().show();
            $("#UsuarioRazaoSocial").attr("disabled", "").parent().show();
            $("#UsuarioInscricaoEstadual").attr("disabled", "").parent().show();
        }else{
            $("#UsuarioSexo").attr("disabled", "").parent().show();
            $("#UsuarioDataNascimento").attr("disabled", "").parent().show();
            $("#UsuarioRg").attr("disabled", "").parent().show();
            $("#UsuarioCpf").attr("disabled", "").parent().show();
            $("#UsuarioCnpj").attr("disabled", "disabled").parent().hide();
            $("#UsuarioNomeFantasia").attr("disabled", "disabled").parent().hide();
            $("#UsuarioRazaoSocial").attr("disabled", "disabled").parent().hide();
            $("#UsuarioInscricaoEstadual").attr("disabled", "disabled").parent().hide();
        }

    })

});