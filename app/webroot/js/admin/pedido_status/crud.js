function myCustomFileBrowser(field_name, url, type, win) {
    browserField = field_name;
    browserWin = win;
    window.open(PATH.basename + "/admin/imagens/add" , 'browserWindow' , 'modal,width=600,height=400,scrollbars=yes');	
}
$(function() {
    tinyMCE.init({
        relative_urls : false,
		convert_urls: false,
        mode : "textareas",
        theme : "advanced",
        editor_selector :"mceEditor",
        file_browser_callback : "myCustomFileBrowser"
    });

    
	
	if($("input[name='data[Pagina][dinamico]']:checked").val() == '0'){
        $("#PaginaElement").attr("disabled", "disabled").parent().hide();
	}else{
        $("#PaginaElement").attr("disabled", "").parent().show();
    }

    $("input[name='data[Pagina][dinamico]']").change(function(){
        if($("input[name='data[Pagina][dinamico]']:checked").val()=='0'){
            $("#PaginaElement").attr("disabled", "disabled").parent().hide();
        }else{
             $("#PaginaElement").attr("disabled", "").parent().show();
        }

    })
 
	jQuery('.colourPicker').colourPicker({ico: PATH.basename + '/img/icons/jquery.colourPicker.gif', title: 'Selecione uma cor'});
})