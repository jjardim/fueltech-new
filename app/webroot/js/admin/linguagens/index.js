$(function() {
	
	if($("input[name='data[Linguagem][externo]']:checked").val() == '0'){
        $("#LinguagemUrl").attr("disabled", "disabled").parent().hide();
	}else{
        $("#LinguagemUrl").attr("disabled", "").parent().show();
    }

    $("input[name='data[Linguagem][externo]']").change(function(){
        if($("input[name='data[Linguagem][externo]']:checked").val()=='0'){
            $("#LinguagemUrl").attr("disabled", "disabled").parent().hide();
        }else{
            $("#LinguagemUrl").attr("disabled", "").parent().show();
        }

    });

});