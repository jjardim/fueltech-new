$(function() {
 
    $('.mask-cep').setMask({
        mask:'99.999-999'
    });

     $('#UsuarioEnderecoCep').change(function(){

        cep = $(this).val();

        $.post(PATH.basename + "/admin/usuario_enderecos/ajax_correios_endereco/" + cep ,function(values){
            if(values){
                $('#UsuarioEnderecoRua').val(values.rua);
                $('#UsuarioEnderecoCidade').val(values.cidade);
                $('#UsuarioEnderecoBairro').val(values.bairro);
                $('#UsuarioEnderecoUf').val(values.estado);
            }
        },'json');
    });
});