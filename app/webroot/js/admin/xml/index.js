/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function(){
	$("#selecionados option").attr("selected", "selected");
	 $('#xml_tipo_id').change(function(){
		$('.loading').show();
		var xml_tipo_id = $(this).val();
        $.post(PATH.basename + "/admin/xml_tipos/ajax_get_dados_xml/",{xml_tipo_id:xml_tipo_id},function(values){
			//alert(values.XmlTipo.template_cabecalho);
            if(values){
                $('#XmlTemplateCabecalho').html(values.XmlTipo.template_cabecalho);
				$('#XmlTemplateCentro').html(values.XmlTipo.template_centro);
				$('#XmlTemplateRodape').html(values.XmlTipo.template_rodape);
            }
			$('.loading').hide();
        },'json');
		$.post(PATH.basename + "/admin/xml_variaveis/ajax_get_variaveis_xml/",{xml_tipo_id:xml_tipo_id},function(variaveis){
			if(variaveis){
				options = new Array();
				$.each(variaveis,function(i,v){
					options += v+', ';
				})
				$('#variaveis_disponiveis').val(options);
			}
		},'json');
		
        return false;
    });
	
	
	$('#buscar-produtos').click(function(){
        query = $('#XmlBuscar').val();
        selecteds = $('#selecionados').val();
        $.post(PATH.basename + "/admin/produtos/ajax_produtos/",{query:query,selecteds:selecteds},function(values){
            if(values){
                options = new Array();
                $.each(values,function(i,v){
					options += '<option value="'+i+'">'+v+'</option>';
                })
                $('#buscados').html(options);
            }
        },'json');
        return false;
    });
	
	 $('#add').click(function(){
        options = '';
        $.each($('#buscados option:selected'),function(i,v){
            options += '<option value="'+v.value+'">'+v.text+'</option>';
            $('#buscados option:selected').remove();
        })
        $('#selecionados').append(options);
        $("#selecionados option").attr("selected", "selected");
        return false;
       
    })
	$('.rm-img').live("click",function(){
		img = $(this);
        if(confirm('deseja remover a imagem?')){
			img.addClass('loading');
			$.post(PATH.basename + "/admin/produtos/rm_img_tmp/"+img.attr('rel'),function(values){
				if(values){
					img.parent().parent().parent().remove();
				}else{
					alert('Erro ao remover Imagem');
				}
			},'json');
		}
    })
});