function myCustomFileBrowser(field_name, url, type, win) {
    browserField = field_name;
    browserWin = win;
    window.open(PATH.basename + "/admin/imagens/add" , 'browserWindow' , 'modal,width=600,height=400,scrollbars=yes');	
}
$(function() {
   
    $('#add-produto').uploadify({
        'uploader'  : PATH.basename + '/swf/uploadify.swf',
        'script'    : PATH.basename + '/admin/produtos/img_add/sid:'+PATH.sid,
        'cancelImg' : PATH.basename+'/img/icons/delete.png',
        'auto'      : true,
		'multi'     : true,
		'fileExt'   : '*.jpg;*.gif;*.png',
		'fileDesc'  : 'Image Files (.JPG, .GIF, .PNG)',
        'buttonText':'Enviar Arquivo',
        'sizeLimit' : 104857600,
        'onComplete': function(event, queueID, fileObj, response, data) {
            $('.container-add').html(response);						
        },
        'onError': function (a, b, c, d) {
			if (d.status == 404)
                alert('Erro ao enviar arquivo');
            else if (d.type === "HTTP")
                alert('Erro '+d.type+": "+d.status);
            else if (d.type ==="File Size")
                alert('Arquivo muito grande, suportado somente até 100MB');
            else
                alert('error '+d.type+": "+d.text);
        }
	});   
	$('#edit-produto').uploadify({
        'uploader'  : PATH.basename + '/swf/uploadify.swf',
        'script'    : PATH.basename + '/admin/produtos/img_edit/'+$('#ProdutoId').val()+'/sid:'+PATH.sid,
        'cancelImg' : PATH.basename+'/img/icons/delete.png',
        'auto'      : true,
		'multi'     : true,
		'fileExt'   : '*.jpg;*.gif;*.png',
		'fileDesc'  : 'Image Files (.JPG, .GIF, .PNG)',
        'buttonText':'Enviar Arquivo',
        'sizeLimit' : 104857600,
        'onComplete': function(event, queueID, fileObj, response, data) {
            $('.container-edit').html(response);						
        },
        'onError': function (a, b, c, d) {
            if (d.status == 404)
                alert('Erro ao enviar arquivo');
            else if (d.type === "HTTP")
                alert('Erro '+d.type+": "+d.status);
            else if (d.type ==="File Size")
                alert('Arquivo muito grande, suportado somente até 100MB');
            else
                alert('error '+d.type+": "+d.text);
        }
	});   
   
    tinyMCE.init({
		relative_urls : false,
		convert_urls: false,
		mode : "textareas",
		theme : "advanced",
		editor_selector :"mceEditor",
		file_browser_callback : "myCustomFileBrowser"
	});
	$('.mask-numerico').setMask({mask:'9',type:'repeat'});
	$('.mask-moeda').setMask({mask : '99,999.999.999.999',type : 'reverse'});
	$('.mask-data').setMask({ mask:'99/99/9999' });
	
	$("#ProdutoCategoriaId").change(function(){
			$("#div-atributos").load(PATH.basename + "/admin/produtos/ajax_atributos/" + $(this).val());
	},'json');
	
	
    $('#buscar-produtos').click(function(){
        query = $('#query').val();
        selecteds = $('#selecionados').val();
        $.post(PATH.basename + "/admin/produtos/ajax_produtos/",{query:query,selecteds:selecteds},function(values){
            if(values){
                options = new Array();
                $.each(values,function(i,v){
					options += '<option value="'+i+'">'+v+'</option>';
                })
                $('#buscados').html(options);
            }
        },'json');
        return false;
    });


    $('#add').click(function(){
        options = '';
        $.each($('#buscados option:selected'),function(i,v){
            options += '<option value="'+v.value+'">'+v.text+'</option>';
            $('#buscados option:selected').remove();
        })
        $('#selecionados').append(options);
        $("#selecionados option").attr("selected", "selected");
        return false;
       
    })
	$('.rm-img').live("click",function(){
		img = $(this);
        if(confirm('deseja remover a imagem?')){
			img.addClass('loading');
			$.post(PATH.basename + "/admin/produtos/rm_img_tmp/"+img.attr('rel'),function(values){
				if(values){
					img.parent().parent().parent().remove();
				}else{
					alert('Erro ao remover Imagem');
				}
			},'json');
		}
    })
	$('.rm-img-old').live("click",function(){
		img = $(this);
        if(confirm('deseja remover a imagem?')){
			img.addClass('loading');
			$.post(PATH.basename + "/admin/produtos/rm_img_old/"+img.attr('rel'),function(values){
				if(values){
					img.parent().parent().parent().remove();
				}else{
					alert('Erro ao remover Imagem');
				}
			},'json');
		}
    })
	
	$('.container-edit .ordem').live('blur',function(){
		parent = $(this).parent().parent().parent();
		$('div a',parent).addClass('loading');		
		
		$.post(PATH.basename + "/admin/produtos/img_ordem_old/"+$(this).attr('rel')+"/"+$(this).val(),function(values){
            if(values){
               $('div a',parent).removeClass('loading');
            }else{
				alert('Ocorreu um erro tente novamente.');
				$('div a',parent).removeClass('loading');
			}
        },'json');
    });
	
	$('.container-add .ordem').live('blur',function(){
		parent = $(this).parent().parent().parent();
		$('div a',parent).addClass('loading');
		
		$.post(PATH.basename + "/admin/produtos/img_ordem_tmp/"+$(this).attr('rel')+"/"+$(this).val(),function(values){
            if(values){
               $('div a',parent).removeClass('loading');
            }else{
				alert('Ocorreu um erro tente novamente.');
				$('div a',parent).removeClass('loading');
			}
        },'json');
    });
	
    $('#rm').click(function(){
        $('#selecionados option:selected').remove();
        $("#selecionados option").attr("selected", "selected");
        return false;
    })
	$("#selecionados option").attr("selected", "selected");
	
	
	
	var idp = $('#ProdutoId').val();
	
	// Abre fecha abas
	$(".clicaaba").live('click',function(){
		var rel = $(this).attr("rel");
		$(".abas").hide();
		$('.abas[rel="' + rel + '"]').show();
		$(".clicaaba").removeClass("active");
		$(this).addClass("active");
		$.cookie("abaproduto" + idp, rel);
	});
	
	$('.abas').each(function(){
		if($('.error-message', this).length>0){
			var rel = $(this).attr("rel");
			$('.clicaaba[rel="'+rel+'"]').addClass('borderred');
		}
	});
	
	if($.cookie("abaproduto" + idp)){
		var qaba = $.cookie("abaproduto" + idp);
		$(".abas").hide();
		$('.abas[rel="' + qaba + '"]').show();
		$(".clicaaba").removeClass("active");
		$('.clicaaba[rel="'+qaba+'"]').addClass("active");
	}
	
	$.datepicker.regional['pt-BR'] = {
                closeText: 'Fechar',
                prevText: '&#x3c;Anterior',
                nextText: 'Pr&oacute;ximo&#x3e;',
                currentText: 'Hoje',
                monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
                'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
                'Jul','Ago','Set','Out','Nov','Dez'],
                dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sabado'],
                dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
                dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
                weekHeader: 'Sm',
                dateFormat: 'dd/mm/yy',
                firstDay: 0,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''};
        $.datepicker.setDefaults($.datepicker.regional['pt-BR']);
	$('.datePicker').datepicker();
	
    $('#buscar-pai').click(function(){
        query = $('#querypai').val();
        $.post(PATH.basename + "/admin/produtos/ajax_pai/",{query:query},function(values){
            if(values){
                options = new Array();
				options += '<option value="">Selecione</option>';
                $.each(values,function(i,v){
					options += '<option value="'+i+'">'+v+'</option>';
                })
                $('#ProdutoParentId').html(options);
            }
        },'json');
        return false;
    });
	
	$('.childrens-produto').click(function(){
        if(confirm('Tem certeza? ao fazer isso irá tornar este produto o produto principal do agrupamento.')){
			return true;
		}else{
			$(".childrens-radio input:radio").removeAttr("checked");
		}
    });
	
	
	//begin container-preco
	$('.container-preco a.add').click(function(){
        $('.container-preco').clone(true).appendTo('.container-preco-tmp').find("input").val('');
        $('.container-preco-tmp div').each(function(i,v){
            $(this).removeClass('container-preco').addClass('container').addClass('container-content');
        })
        $('.container-preco-tmp > div').each(function(i,v){
            $('a.rm',this).each(function(a,b){
                $(this).show();
            })
            $('a.add',this).each(function(a,b){
                $(this).hide();
            })
            ++i
            $('input',this).each(function(a,b){
                str = $(this).attr('name').replace(/[0-9]+/g,i);
                $(this).attr('name',str);
            })
            $('select',this).each(function(a,b){
                str = $(this).attr('name').replace(/[0-9]+/g,i);
                $(this).attr('name',str);
            })
        })
        return false;
    });
    $('.container-preco-tmp a.rm').live('click',function(){
        $(this).parent().remove();
        return false;
    })
	//end container-preco
	
	//begin container-variacoes
	$('.container-variacoes a.add').click(function(){
        $('.container-variacoes').clone(true).appendTo('.container-variacoes-tmp').each(function(){
            $(this).find('.variacoes').html('');
            $(this).find("select").val('');
        })
        $('.container-variacoes-tmp div').each(function(i,v){
            $(this).removeClass('container-variacoes').addClass('container').addClass('container-content');
        })
        $('.container-variacoes-tmp > div').each(function(i,v){
            $('a.rm',this).each(function(a,b){
                $(this).show();
            })
            $('a.add',this).each(function(a,b){
                $(this).hide();
            })
            ++i
            $('input',this).each(function(a,b){
                str = $(this).attr('name').replace(/[0-9]+/g,i);
                $(this).attr('name',str);
            })
            $('select',this).each(function(a,b){
                str = $(this).attr('name').replace(/[0-9]+/g,i);
                $(this).attr('name',str);
            })
        })
        return false;
    });
    $('.container-variacoes-tmp a.rm').live('click',function(){
        $(this).parent().remove();
        return false;
    })
	//end container-variacoes
	
	//begin container-atributos
	$('.container-atributos a.add').click(function(){
        $('.container-atributos').clone(true).appendTo('.container-atributos-tmp').each(function(){
			$(this).find('.atributos').html('');
			$(this).find("select").val('');
		})
        $('.container-atributos-tmp div').each(function(i,v){
            $(this).removeClass('container-atributos').addClass('container').addClass('container-content');
        })
        $('.container-atributos-tmp > div').each(function(i,v){
            $('a.rm',this).each(function(a,b){
                $(this).show();
            })
            $('a.add',this).each(function(a,b){
                $(this).hide();
            })
            ++i
            $('input',this).each(function(a,b){
                str = $(this).attr('name').replace(/[0-9]+/g,i);
                $(this).attr('name',str);
            })
            $('select',this).each(function(a,b){
                str = $(this).attr('name').replace(/[0-9]+/g,i);
                $(this).attr('name',str);
            })
        })
        return false;
    });
    $('.container-atributos-tmp a.rm').live('click',function(){
        $(this).parent().remove();
        return false;
    })
	//end container-atributos
		
		
	//begin kit de produtos
		//begin container-kit
		$('.container-kit a.add').click(function(){
			$('.container-kit').clone(true).appendTo('.container-kit-tmp').each(function(){
				$(this).find('.produtos').html('');
				$(this).find("select").val('');
				$(this).find("input").val('');
			})
			$('.container-kit-tmp div').each(function(i,v){
				$(this).removeClass('container-kit').addClass('container').addClass('container-content');
			})
			$('.container-kit-tmp > div').each(function(i,v){
				$('a.rm',this).each(function(a,b){
					$(this).show();
				})
				$('a.add',this).each(function(a,b){
					$(this).hide();
				})
				$('input.field_quantidade',this).each(function(a,b){
					$(this).val('1');
				})
				++i
				$('input',this).each(function(a,b){
					str = $(this).attr('name').replace(/[0-9]+/g,i);
					$(this).attr('name',str);
				})
				$('select',this).each(function(a,b){
					str = $(this).attr('name').replace(/[0-9]+/g,i);
					$(this).attr('name',str);
				})
			})
			$('.mask-data').setMask({ mask:'99/99/9999' });
			return false;
		});
		$('.container-kit-tmp a.rm').live('click',function(){
			$(this).parent().parent().remove();
			return false;
		})
		//end container-kit
	
		$('.buscar-produtos').live('click', function(){
			$('.loading').show();
			var obj = $(this);
			query = obj.parent().find('.busca_produto').val();
			obj.parent().parent().parent().find('.select_produto').html('<option value="">Carregando...</option>');
			$.post(PATH.basename + "/admin/produtos/ajax_produtos/",{query:query},function(values){
				if(values){
					options = new Array();
					options += '<option value="">Selecione</option>';
					$.each(values,function(i,v){
						options += '<option value="'+i+'">'+v+'</option>';
					})
					obj.parent().parent().parent().find('.select_produto').html(options);
					$('.loading').hide();	
				}
			},'json');
			return false;
		});
		
		$('.select_produto').live('change', function(){
			$('.loading').show();
			var obj = $(this);
			var value = obj.val();
			$.post(PATH.basename + "/admin/produtos/get_preco/",{produto_id:value},function(values){
				if(values){
					obj.parent().parent().parent().find('.field_preco').val(values);
					obj.parent().parent().parent().find('.field_preco_real').find("input[type='hidden']").val(values);
					$('.loading').hide();
				}
			},'json');
			return false;
		});
		
		//load
		show_hide_kit_fields($("input[name='data[Produto][kit]']:checked").val());
		//change
		$("input[name='data[Produto][kit]']").change(function(){
			show_hide_kit_fields($("input[name='data[Produto][kit]']:checked").val());
		});
	//end kit de produtos
	
	//begin container-downloads
	$('.container-downloads a.add').click(function(){
        $('.container-downloads').clone(true).appendTo('.container-downloads-tmp').each(function(){
            $(this).find('.downloads').html('');
            $(this).find("select").val('');
        })
        $('.container-downloads-tmp div').each(function(i,v){
            $(this).removeClass('container-downloads').addClass('container').addClass('container-content');
        })
        $('.container-downloads-tmp > div').each(function(i,v){
            $('a.rm',this).each(function(a,b){
                $(this).show();
            })
            $('a.add',this).each(function(a,b){
                $(this).hide();
            })
            ++i
            $('input',this).each(function(a,b){
                str = $(this).attr('name').replace(/[0-9]+/g,i);
                $(this).attr('name',str);
            })
            $('select',this).each(function(a,b){
                str = $(this).attr('name').replace(/[0-9]+/g,i);
                $(this).attr('name',str);
            })
        })
        return false;
    });
    $('.container-downloads-tmp a.rm').live('click',function(){
        $(this).parent().remove();
        return false;
    })
	//end container-downloads

    //### start atributos tipos
	$( ".atributos_tipos" ).change(function(){		
		if($(this).val()!=""){
			var element = $(this);
			$('.loading').show();			
			 var var_atributo_id = $(this).val();
			 $.post(PATH.basename + "/admin/atributos/get_ajax_atributos/",{atributo_id:var_atributo_id},function(values){
				if(values){
					options = new Array();
					$.each(values,function(i,v){
						options += '<option value="'+i+'">'+v+'</option>';
					});
					$(element).parents('.container').find('.atributos').html(options);
					$('.loading').hide();
				}
			},'json');			
			return false;
		}	
	})	
	//### end atributos tipos

    //### start atributos tipos
    $( ".variacoes_tipos" ).change(function(){      
        if($(this).val()!=""){
            var element = $(this);
            $('.loading').show();           
             var var_variacao_id = $(this).val();
             $.post(PATH.basename + "/admin/variacoes/get_ajax_variacoes/",{variacao_id:var_variacao_id},function(values){
                if(values){
                    options = new Array();
                    $.each(values,function(i,v){
                        options += '<option value="'+i+'">'+v+'</option>';
                    });
                    $(element).parents('.container').find('.variacoes').html(options);
                    $('.loading').hide();
                }
            },'json');          
            return false;
        }   
    })  
    //### end atributos tipos
	
	//### start atributos tipos
    $( ".download_tipos" ).change(function(){      
        if($(this).val()!=""){
            var element = $(this);
            $('.loading').show();           
             var var_download_id = $(this).val();
             $.post(PATH.basename + "/admin/downloads/get_ajax_downloads/",{download_id:var_download_id},function(values){
                if(values){
                    options = new Array();
                    $.each(values,function(i,v){
                        options += '<option value="'+i+'">'+v+'</option>';
                    });
                    $(element).parents('.container').find('.downloads').html(options);
                    $('.loading').hide();
                }
            },'json');          
            return false;
        }   
    })  
    //### end atributos tipos
	
	
	
	$(".categoria_produtos_sel").asmSelect({
	    sortable: true,
        animate: true,
        addItemTarget: 'top'
    });
	
	//## START detail-tabs-nav  ##//
	$("#detail-tabs").tabs();
});

function show_hide_kit_fields( value ){

	if( value =='1'){
		$('.box_produtos_sem_kit input').each(
			function(index){  
				var input = $(this);
				input.attr("disabled", true).parent().hide();
				
			}
		);
		$('.box_produtos_kit').hide();
		$('.box_produtos_kit input').each(
			function(index){  
				var input = $(this);
				input.attr("disabled", false).parent().show();
				
			}
		);
		$('.box_produtos_kit').show();
	}else{
		$('.box_produtos_sem_kit input').each(
			function(index){  
				var input = $(this);
				input.attr("disabled", "").parent().show();
			}
		);
		$('.box_produtos_kit').show();
		$('.box_produtos_kit input').each(
			function(index){  
				var input = $(this);
				input.attr("disabled", true).parent().hide();
				
			}
		);
		$('.box_produtos_kit').hide();
	}
	
}