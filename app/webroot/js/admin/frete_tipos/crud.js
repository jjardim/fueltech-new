$(function() {

	$('.mask-numerico').setMask({mask:'9',type:'repeat'});
    $('.mask-moeda').setMask({mask : '99,999.999.999.999',type : 'reverse'});
    $('.mask-cep').setMask({mask : '99999999'});
  
});