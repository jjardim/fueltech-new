/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function(){
    $('div.product-detail div.bloco1 div.choose-block ul li a').click(function(){
	var variacao_val = $(this).attr('rel');
	 	$('.selecione_tamanho').html('Selecione o Tamanho..');	
        $('.variacoes').hide();
        $('.variacoes-' + variacao_val).show();
        
    })
    $('.icon-zoom-thumb-product').nyroModal();
	$('.add').click(function(){
		$(this).attr('href').replace(/\/([0-9]+)$/, "/"+$('.quantidade').val());	
	});
	
    $('.mask-cep').setMask({
		autoTab: false,
        mask:'99.999-999'
    });
	if( $('#cep').length )
	{	
		if($('#cep').val() == '') { $('#cep').val('CEP'); }
		$('#cep').focus(function(){
			if( $('#cep').val() == 'CEP' ) { $('#cep').val(''); }
		});
		
		$('#cep').blur(function(){
			if($('#cep').val() == '') { $('#cep').val('CEP'); }
		});
	}
    $(".carrossel li a img").live("click",function(){
        src_resize = $(this).attr("src").replace(/52/, "155").replace(/52/, "218");
        src_noresize = $(this).attr("src").replace(/^52$/g, "155").replace(/52/, "218").replace(/false/, "true");
        var title = $(".container-fotos-videos h2").text();
        img = $('<img>').attr({
            'alt': 'Foto',
            'src': src_resize,
            'title': title
        });
        link = $('<a>').attr({
            'class': 'zoom',
            'title': title,
            'href': src_noresize
        });
        link.append(img);
        $(".imgPrincipal").empty().append(link);
        carregaZoom();
        return false;
    });


    $("#votar").live('click',function(){
        var nota = $("#nota").val();
        var produto = $("#nota").attr("rel");
        if(produto!=""&&nota!=""){
            $.post(PATH.basename + "/produtos/ajax_rating/"+produto+"/"+nota ,function(values){
                if(values){
                    alert('Voto contabilizado com sucesso!');
                }else{
                    $('#msg-rating').html('Você precisa estar logado para efetuar a avaliação, <a href="' + PATH.basename + '/login" alt="Efetuar Login" title="Efetuar Login">clique aqui</a> para efetuar o login');
                }
            },'json');
        }
        return false;
    })

    $('div#product_detail div.product_incification div.form_box input.link').live('click',function() {
        var cep = $("div.product_incification div.form_box input.input").val();
        var tipo_entrega = $(".tipo_entrega:checked").val();
        $.post(PATH.basename + "/carrinho/carrinho/set_frete", {cep:cep,tipo_entrega:tipo_entrega}, function(response){
            if(response.endereco&&response.prazo){
                var entrega = "<ul>\n\
                                    <li>"+response.prazo.Servicos.CServico.PrazoEntrega+" dia(s) para entrega</li>\n\
                                    <li>Rua: "+response.endereco.rua+" - "+response.endereco.bairro+"</li>\n\
                                    <li>"+response.endereco.cidade+" - "+response.endereco.estado+"</li>\n\
                                </ul>"
                $('#entrega').html(entrega);
            }else{
                $('#entrega').empty();
            }            
        }, "json");
        return false;
    });

    $(".informacoes").live('click',function(){
        $(".info").hide();
        $("#produto-descricao").show();
        $("#tab_box_nav ul li a").removeClass("active");
        $(this).addClass("active");
    });

    $(".dados-tecnicos").live('click',function(){
        $(".info").hide();
        $("#produto-dados-tecnicos").show();
        $("#tab_box_nav ul li a").removeClass("active");
        $(this).addClass("active");
    });
	
	
	// Abre fecha abas
	$(".clique_aba").live('click',function(){
		var rel = $(this).attr("rel");
		$(".aba_" + rel).toggle();
		$(".clique_aba").removeClass("active");
		$(this).addClass("active");
	});
	
	// Abre fecha abas
	$(".clique_sel").live('click',function(){
		var rel = $(this).attr("rel");
		$(".seleto").hide();
		$(".sel_" + rel).show();
		$(".clique_sel").removeClass("active");
		$(this).addClass("active");
		carregaZoom();
		$('.zoomWindow').delay(400).css('left','390px');
	});	
	
	$('.selectdeproduto').live('change',function(){
        var rel = $(this).val();
		$('.parajax').html('Carregando...');
		$.ajaxSetup({
			cache: false
		});
		$('.parajax').load(PATH.basename + "/produtos/detalhe/" + rel, function(){
			$fgee=1;
		});
		
		return false;
	});
	
	if( $('#add-produto-carrinho-ajax').length ){
		$('#add-produto-carrinho-ajax').attr('href',$('#add-produto-carrinho-ajax').attr('href').replace(/\/([0-9]+)$/, "/"+$('#produto_quantidade').val()));
	}
	$('#produto_quantidade').live('keyup',function(){
        $('#add-produto-carrinho-ajax').attr('href',$('#add-produto-carrinho-ajax').attr('href').replace(/\/([0-9]+)$/, "/"+$(this).val()));			
	});
	
    carregaZoom();
	
	//## START star rating  ##//
	
	//monta as estrelas da lista de classificacoes
	$(".rating-txtb").each(function() {
	    $(this).find(".rating .ratings-star").ratings(5, $(this).find(".rating .ratings-star").attr("rel"), false);
	});
	
	//monta as estrela na media de classificaacao do topo
	$('.rating div.stars').ratings(5,$("div.stars").attr('rel'),false);
	//evento click da media de classificacao, enviando o usuario para o form 
	$("#rating-media").click(function(){
		$('html, body').animate({
			scrollTop: $(".rating-block").offset().top
		}, 500);
	});
	
	//validação de retorno no formulario caso tenha erro no post
	//se o hiddem de nota tiver vazio, crios as estrelas vazias
	if( $('#ProdutoRatingNota').val() == "" )
	{	
		//classficacao form
		$('.rating-form .rating-star div.star-inferior').ratings(5,0,true).bind('ratingchanged', function(event, data) {
			$('#ProdutoRatingNota').val(data.rating);
			setDescricaoRating(data.rating);
		});
	}else
	{
		//senao, seto as estrelas já selecionadas pela usuario
		//classficacao form
		var nota_atual = parseInt($('#ProdutoRatingNota').val());		
		setDescricaoRating(nota_atual);
		
		$('.rating-form .rating-star div.star-inferior').ratings(5,nota_atual,true).bind('ratingchanged', function(event, data) {
			$('#ProdutoRatingNota').val(data.rating);
			setDescricaoRating(data.rating);
		});
	}	
	
	//## END star rating  ##//
	
	//## START detail-tabs-nav  ##//
	$("#detail-tabs").tabs();
	
	//## START ratings-tabs-nav  ##//
	$("#ratings-tabs").tabs();
	
	$(".active").click(function(){
		//alert('alo');
		$( ".tab-nav ul li" ).removeClass('active');
		$( this ).addClass('active');
	});
	//## END detail-tabs-nav  ##//
	
	//## START lista-produtos-relacionados  ##//
	$('#lista-produtos-relacionados').jcarousel({
		scroll: 1,
        wrap: 'last',
        initCallback: mycarousel_initCallbackRelacionados
	});
	//## END lista-produtos-relacionados  ##//
	
	//## START lista-produtos-pedidos-itens  ##//
	$('#lista-produtos-pedidos-itens').jcarousel({
		scroll: 1,
        wrap: 'last',
        initCallback: mycarousel_initCallbackPedidosItens
	});
	//## END lista-produtos-pedidos-itens  ##//
	
	
	//## START add-produto-carrinho-ajax ##//
	//$('.loading').css('visibility','visible');
	// var produto_agrupador = $('#add-produto-carrinho-ajax').attr('rel');
	// $('#detalhe-lista-produtos-carrinho').load(PATH.basename + "/carrinho/carrinho/get_itens_ajax/"+produto_agrupador, function(){
		// $('.loading').css('visibility','hidden');
	// });		
		
	$('#add-produto-carrinho-ajax').live('click',function(){
		var produto_agrupador = $(this).attr('rel');
		var width = 567;
		var height = 307;
		$.nmManual($(this).attr('href'),  {
			_close: function(a) {
				$('.loading').css('visibility','visible');
				$('#detalhe-lista-produtos-carrinho').load(PATH.basename + "/carrinho/carrinho/get_itens_ajax/" + produto_agrupador, function(){
					$('.loading').css('visibility','hidden');
				});
			},
			sizes: {
				initW: width, initH: height,
				minW: width, minH: height,
				w: width, h: height
			}, 
			modal: false,
			galleryLoop: false,
			galleryCounts: false,
			ltr: false
		});
		return false;
	
	})	
		
	
	$('.remove-produto-carrinho-ajax').live('click',function(){
		var produto_id = $(this).attr('rel');
		$.post(PATH.basename + "/carrinho/carrinho/remove_ajax/"+produto_id);
		$(this).parent().fadeOut();
	});	
	//## END add-produto-carrinho-ajax ##//
	
	//## START consultar-frete-em-detalhes  ##//
	if( $('#consultar-frete-em-detalhes').length && $("#cep").val() != "" && $("#cep").val() != "CEP" ){
		$('.loading').css('visibility','visible');
		var cep = $("#cep").val();
		var produto_id = $('#consultar-frete-em-detalhes').attr('rel');
		if(cep){
			$('.fretes-disponiveis-msg').hide();
			$('.fretes-disponiveis-msg').html('');  
			$('.fretes-disponiveis-detalhe').show();
			$('.fretes-disponiveis-detalhe').load(PATH.basename + "/carrinho/carrinho/get_frete_em_detalhes/"+cep+"/"+produto_id, function(){
				$('.loading').css('visibility','hidden');
			});
		}
	}
		
	$('#consultar-frete-em-detalhes').click( function() {
		$('.loading').css('visibility','visible');
		var cep = $("#cep").val();
		var produto_id = $(this).attr('rel');
		if(cep){
			$('.fretes-disponiveis-msg').hide();
			$('.fretes-disponiveis-msg').html('');  
			$('.fretes-disponiveis-detalhe').show();
			$('.fretes-disponiveis-detalhe').load(PATH.basename + "/carrinho/carrinho/get_frete_em_detalhes/"+cep+"/"+produto_id, function(){
				$('.loading').css('visibility','hidden');
			});
		}else{
			$('.fretes-disponiveis-detalhe').hide();
            $('.fretes-disponiveis-detalhe').html('');
			$('.loading').css('visibility','hidden');
			$('.fretes-disponiveis-msg').show();
			$('.fretes-disponiveis-msg').html('Preencha o CEP');
		}
        return false;            
    });
	//## END consultar-frete-em-detalhes  ##//
	
	//scroll for top
	$(".top a").click(function(){
		$('html, body').animate({
			scrollTop: $("#header").offset().top
		}, 500);
	});
	
	//back history
	$(".back a").click(function(){
		window.history.back()
	});
	
	//carousel vitrine normal
	$('.produtosRelacionados').jcarousel({
		scroll: 1,
        wrap: 'last',
        initCallback: produtosRelacionados_initCallback
	});
	
	//carousel produtos imagens
	$('.produtos_imagens_thumb').jcarousel({
		scroll: 1,
        wrap: 'last',
        initCallback: produtos_imagens_thumb_initCallback
	});
	
});

function produtos_imagens_thumb_initCallback(carousel) {
	$('.produtos_imagens_thumb .jcarousel-next').bind('click', function () {        
		carousel.next();        
		return false;
	});

	$('.produtos_imagens_thumb .jcarousel-prev').bind('click', function () {
		carousel.prev();
		return false;
	});
}

function produtosRelacionados_initCallback(carousel) {
	$('.produtosRelacionados .jcarousel-next').bind('click', function () {        
		carousel.next();        
		return false;
	});

	$('.produtosRelacionados .jcarousel-prev').bind('click', function () {
		carousel.prev();
		return false;
	});
}
	
	
function validaFormAviseMeModal() {
  if ($(".form-aviseme #nome").val() == "" || $(".form-aviseme #email").val() == "") {
	if ($(".form-aviseme #nome").val() == ""){
		$("#nome-msg-error").text("Preenchimento obrigatório.").show();
	}	
	if ($(".form-aviseme #email").val() == "") {
		$("#email-msg-error").text("Preenchimento obrigatório.").show();
	}
	return false;
  }
 
}

function fecharModal(){
	//$(".lightbox-product-add").fadeOut();
	//$(".lightbox-product-add").html(" ");
    $.nyroModalRemove();
};
function carregaZoom(){
    var options = {
		preloadImages: false,
        zoomWidth:400,
        zoomHeight:300,
        xOffset:30,
        yOffset:0,
        title:false,
        lens:true,
        showEffect:'show',
        hideEffect:'fadeout',
        fadeoutSpeed:'slow',
        position:"right" //and MORE OPTIONS
    };

    $(".zoom").jqzoom(options);
}

function setDescricaoRating(nota){
	switch(nota){
		case 1:
		$('#rating-nota-escolha').text('Péssimo');
		break;
		
		case 2:
		$('#rating-nota-escolha').text('Ruim');
		break;
		
		case 3:
		$('#rating-nota-escolha').text('Regular');
		break;
		
		case 4:
		$('#rating-nota-escolha').text('Bom');
		break;
		
		case 5:
		$('#rating-nota-escolha').text('Ótimo');
		break;
	}
}

function mycarousel_initCallbackRelacionados(carousel) {
	$('.jcarousel-next-relacionados').bind('click', function () {        
		carousel.next();        
		return false;
	});

	$('.jcarousel-prev-relacionados').bind('click', function () {
		carousel.prev();
		return false;
	});
}

function mycarousel_initCallbackPedidosItens(carousel) {
	$('.jcarousel-next-pedidos-itens').bind('click', function () {        
		carousel.next();        
		return false;
	});

	$('.jcarousel-prev-pedidos-itens').bind('click', function () {
		carousel.prev();
		return false;
	});
}