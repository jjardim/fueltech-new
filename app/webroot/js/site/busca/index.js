$(document).ready(function(){
	
	$('.graycol .select').customSelect();
	
	$('#ordenacao').live('change',function(){
        if($('#ordenacao :selected').val()!=""){
			window.location = $('#ordenacao').attr('rel').replace(/\/sort:(.*)\/(direction:desc|direction:asc)/gi,'') +'/'+ $('#ordenacao :selected').val();
		}else{
			window.location = $('#ordenacao').attr('rel').replace(/\/sort:(.*)\/direction:(.*)/gi,'');
		}
    })
	
	$('.filtros_selecionados').live('click',function(){	
		atributos = [];
		var atributo_clicado = $(this).attr('rel');
		$('.filtros_selecionados').each(function() { 
			if( $(this).attr('rel') != atributo_clicado)
			{
				atributos.push($(this).attr('rel'));
			}
		});
		
		if( atributos.length  != 0 )
		{
			if(JSON.parse(PATH.params.named).atributos!=""||JSON.parse(PATH.params.named).atributos!=undefined){
				
				window.location = $('.filter-current').val() . replace(/\[(.*)]/gi,'['+atributos.join(',')+']'); 
			}else{
			
				window.location = $('.filter-current').val() + '/atributos:['+atributos.join(',')+']';
			}
			
		}else
		{
			window.location = $('.filter-current').val().replace(/\/(atributos:\[(.*)\])/gi,'');
		}		
	})
	
	//evento click do atributo
	$('.checkbox').live('click',function(){
		
		//se o elemento clicaco, estiver checked
		if ($(this).is(':checked')) {
		
			//array de atributos
			atributos = [];
			
			//verifico se j� h� filtros selecionados, e adiciono os mesmo no array
			$('.filtros_selecionados').each(function() { 
				 atributos.push($(this).attr('rel'));			
			});
			
			//pego os atributos que est�o selecionados
			$('.checkbox:checked').each(function() { 
				 atributos.push($(this).val());			
			});
			
			//dou um relaod na pagina, com os atributos na url
			if(JSON.parse(PATH.params.named).atributos!=""&&JSON.parse(PATH.params.named).atributos!=undefined){
				window.location = $('.filter-current').val() . replace(/\[(.*)]/gi,'['+atributos.join(',')+']'); 
			}else{
				window.location = $('.filter-current').val() + '/atributos:['+atributos.join(',')+']';
			}
		}
    })
})