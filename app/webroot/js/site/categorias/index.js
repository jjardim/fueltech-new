$(document).ready(function(){
	
	//load atributos
	if($('#content_menu_filtro').length){
		$('.loading').css('visibility','visible');
		$('#content_menu_filtro').load(PATH.basename + "/atributos/ajax_atributos_categoria/" + $('#content_menu_filtro').attr('rel') +"/", function(){
			$('.loading').css('visibility','hidden');
		});
	}
	
	//custom select
	$('.select-area .select').customSelect();
	$('.graycol .select').customSelect();
	
	//jquery ui slider
	$( "#slider-preco" ).slider({
		range: true,
		min: parseInt($("#slider-valor-min").attr('rel'), 10),
		max: parseInt($("#slider-valor-max").attr('rel'), 10),
		values: [ parseInt($("#slider-valor-min").val(), 10), parseInt($("#slider-valor-max").val(), 10) ],
		slide: function( event, ui ) {
			$("#slider-valor-min").val(ui.values[0]);
			$("#slider-valor-max").val(ui.values[1]);
		},
		change: function(event, ui) { 
			$("#slider-valor-min").val(ui.values[0]);
			$("#slider-valor-max").val(ui.values[1]);
        }
	});
	$("#slider-valor-min").val( $( "#slider-preco" ).slider( "values", 0 ) );
	$("#slider-valor-max").val( $( "#slider-preco" ).slider( "values", 1 ) );
	
	$('.ordenacao').live('change',function(){
		if($(this).val()!=""){
			window.location = $('.ordenacao').attr('rel').replace(/\/sort:(.*)\/(direction:desc|direction:asc)/gi,'') +'/'+ $(this).val();
		}else{
			window.location = $('.ordenacao').attr('rel').replace(/\/sort:(.*)\/direction:(.*)/gi,'');
		}
    })
		
	// $('#ordenacao1, #ordenacao2, #ordenacao3, #ordenacao4, #ordenacao5').live('change',function(){
		// var selecteds = '';
		// $('.select-area select, .graycol .select').each(function() { 
			// if($(this).val() != ''){
				// selecteds += $(this).val() + '/';
			// }
		// });
		// if($(this).val()!=""){
			// window.location = $(this).attr('rel').replace(/\/sort:(.*)\/(direction:desc|direction:asc)/gi,'') +'/'+ selecteds;
		// }else{
			// window.location = $(this).attr('rel').replace(/\/sort:(.*)\/direction:(.*)/gi,'');
		// }
    // })
	
	
	
	$('.filtros_selecionados').live('click',function(){	
		atributos = [];
		var atributo_clicado = $(this).attr('rel');
		$('.filtros_selecionados').each(function() { 
			if( $(this).attr('rel') != atributo_clicado)
			{
				atributos.push($(this).attr('rel'));
			}
		});
		
		if( atributos.length  != 0 )
		{
			if(JSON.parse(PATH.params.named).atributos!=""||JSON.parse(PATH.params.named).atributos!=undefined){
				
				window.location = $('.filter-current').val() . replace(/\[(.*)]/gi,'['+atributos.join(',')+']'); 
			}else{
			
				window.location = $('.filter-current').val() + '/atributos:['+atributos.join(',')+']';
			}
			
		}else
		{
			window.location = $('.filter-current').val().replace(/\/(atributos:\[(.*)\])/gi,'');
		}		
	})
	
	//evento click do atributo
	$('.checkbox').live('click',function(){
		
		//se o elemento clicaco, estiver checked
		if ($(this).is(':checked')) {
		
			//array de atributos
			atributos = [];
			
			//verifico se j� h� filtros selecionados, e adiciono os mesmo no array
			$('.filtros_selecionados').each(function() { 
				 atributos.push($(this).attr('rel'));			
			});
			
			//pego os atributos que est�o selecionados
			$('.checkbox:checked').each(function() { 
				 atributos.push($(this).val());			
			});
			
			//dou um relaod na pagina, com os atributos na url
			if(JSON.parse(PATH.params.named).atributos!=""&&JSON.parse(PATH.params.named).atributos!=undefined){
				window.location = $('.filter-current').val() . replace(/\[(.*)]/gi,'['+atributos.join(',')+']'); 
			}else{
				window.location = $('.filter-current').val() + '/atributos:['+atributos.join(',')+']';
			}
		}
    })
	
	//filtro de marca
	$('#FilterMarca').change(function(){
		$(".loading2").show();
		var marca = $(this).val();
		
		$('#FilterCor').parent().find('.customSelectInner').text('Carregando...');
		$('#FilterCor').html(' ').append($('<option>').text("Carregando..").attr('value', ''));
		$.post(PATH.basename + "/variacoes/ajax_get_cor_fabricante/"+marca, function(value){			
			if(value){
				options = new Array();
				options += '<option value="">Selecione</option>';
				$.each(value,function(i,v){
					options += '<option value="'+i+'">'+v+'</option>';
				});
				$('#FilterCor').parent().find('.customSelectInner').text('Cor');
				$("#FilterCor").html(options);
			}
			$(".loading2").hide();
		},"json");	
	});
	
	//filtro de marca
	$('#FilterCor').change(function(){
		$(".loading2").show();
		var marca = $('#FilterMarca').val();
		var cor = $(this).val();
		
		$('#FilterTamanho').parent().find('.customSelectInner').text('Carregando...');
		$('#FilterTamanho').html(" ").append($('<option>').text("Selecione").attr('value', ''));
		$.post(PATH.basename + "/variacoes/ajax_get_tamanho_cor/"+marca+"/"+cor, function(value){
			if(value){
				options = new Array();
				$.each(value,function(i,v){
					options += '<option value="'+i+'">'+v+'</option>';
				});
				$("#FilterTamanho").html(options);
				$('#FilterTamanho').parent().find('.customSelectInner').text('Tamanho');
			}
			$(".loading2").hide();
		},"json");	
	});
	
	$('.carrousel_categorias').jcarousel({
		scroll: 1,
		wrap: 'last',
        initCallback: carrousel_categorias_initCallback
	});
	
	$('.btn-voltar').click(function(){
		window.history.back();
	});
	
	$('.btn-topo').click(function(){
		$('html, body').animate({
			scrollTop: $("#header").offset().top
		}, 500);
	});
})

function carrousel_categorias_initCallback(carousel) {
	$('.next1').bind('click', function () {        
		carousel.next();        
		return false;
	});

	$('.prev1').bind('click', function () {
		carousel.prev();
		return false;
	});
}