/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$(function() {

    $('.icon-zoom-thumb').nyroModal();

    //formatacao de tela do element 'FORNECEDORES'
    $('.alphabet-nav ul li').first().find('a').addClass('active');
    $('.alphabet-nav ul li').first().find('.alphabet-content').css('display', 'block');

    $('.alphabet-nav ul li').live('click', function() {
        $('.alphabet-nav ul li').find('a').removeClass('active');
        $('.alphabet-nav ul li').find('.alphabet-content').css('display', 'none');
        $(this).find('a').addClass('active');
        $(this).find('.alphabet-content').css('display', 'block');
    })


    //start validacao dos formularios do element 'FORM'
    //campo nome
    // if( $('#SacNome').length )
    // {
    // $('#SacNome').focus(function(){
    // if( $('#SacNome').val() == 'Nome*' ) { $('#SacNome').val(''); }
    // });

    // $('#SacNome').blur(function(){
    // if($('#SacNome').val() == '') { $('#SacNome').val('Nome*'); }
    // });
    // }

    //campo email
    // if( $('#SacEmail').length )
    // {
    // $('#SacEmail').focus(function(){
    // if( $('#SacEmail').val() == 'E-mail*' ) { $('#SacEmail').val(''); }
    // });

    // $('#SacEmail').blur(function(){
    // if($('#SacEmail').val() == '') { $('#SacEmail').val('E-mail*'); }
    // })		
    // }

    //campo telefone
    // if( $('#SacTelefone').length )
    // {
    // $('#SacTelefone').focus(function(){
    // if( $('#SacTelefone').val() == 'Telefone' ) { $('#SacTelefone').val(''); }
    // });

    // $('#SacTelefone').blur(function(){
    // if($('#SacTelefone').val() == '') { $('#SacTelefone').val('Telefone'); }
    // })		
    // }

    //campo cidade
    // if( $('#SacCidade').length )
    // {
    // $('#SacCidade').focus(function(){
    // if( $('#SacCidade').val() == 'Cidade' ) { $('#SacCidade').val(''); }
    // });

    // $('#SacCidade').blur(function(){
    // if($('#SacCidade').val() == '') { $('#SacCidade').val('Cidade'); }
    // })		
    // }

    //campo mensagem
    // if( $('#SacMensagem').length )
    // {
    // $('#SacMensagem').focus(function(){
    // if( $('#SacMensagem').val() == 'Mensagem*' ) { $('#SacMensagem').val(''); }
    // });

    // $('#SacMensagem').blur(function(){
    // if($('#SacMensagem').val() == '') { $('#SacMensagem').val('Mensagem*'); }
    // })		
    // }


    $('.mask-telefone-8-9').keypress(function() {
        mascara_8_9_dig(this, mtel);
    });

    if ($('.mask-telefone').length) {
        $('.mask-telefone').setMask({
            mask: '(99) 9999-99999'
        });
    }

    //end validacao dos formularios do element 'FORM'

    //ajax da tela de assistencia tecnica
    //busca os estados que possuem representantes referente aquela marca
    $('#marcas').change(function() {
        $('#estados').html("");
        $("#resultado_busca").hide();
        $(".loading").show();
        var marca = $(this).val();

        $('#estados').append($('<option>').text("Selecione").attr('value', ''));
        $.post(PATH.basename + "/assistencias/get_estado_por_marca/" + marca, function(response) {
            $.each(response, function(i, value) {
                $('#estados').append($('<option>').text(value).attr('value', value));
                $(".loading").hide();
            });
        }, "json");
    });

    //ajax da tela de assistencia tecnica
    //busca os dados das assistencias daquele estado e daquela marca selecionada
    $('#btn-buscar').click(function() {
        $(".loading").show();
        var marca = $("#marcas").val();
        var estado = $("#estados").val();
        var html = "<div class=\"rowline\">";
        $.post(PATH.basename + "/assistencias/get_assistencia/", {var_marca: marca, var_uf: estado}, function(response) {
            $.each(response, function() {

                var cont = 1;
                $.each(this, function(i, value) {

                    html += "<div class=\"left\"><dl>";
                    html += "<dt><strong>Empresa</strong></dt><dd><strong>" + value.empresa + "</strong></dd>";
                    html += "<dt>Endereço</dt><dd>" + value.endereco + "</dd>";
                    html += "<dt>Fone</dt><dd>" + value.telefone + "</dd>";
                    html += "<dt>Email</dt><dd>" + value.email + "</dd>";
                    html += "<dt>Contato</dt><dd>" + value.contato + "</dd>";
                    html += "</dl></div>";

                    if (cont % 2 == 0)
                    {
                        html += "</div><div class=\"rowline\">";
                    }
                });
                html += "</div>";
                $(".loading").hide();
                $("#assistencias-dados").html(html);
            });

            $("#resultado_busca").css('display', 'block');
            $("#result_marca").text(marca);
            $("#result_estado").text(estado);

        }, "json");

    });

    //funcionalide do evento click no nome da loja no elemento 'ONDE-ESTAMOS'
    $('.btn-entre-contato-loja').live('click', function() {

        $('.form51').fadeOut();
        $('.formulario-content').html(" ");
        $(this).parent().next('.form51').fadeIn();

        var tipo_form = $('#tipo_form').val();
        var url_form = $(this).attr('rel');
        var loja_id = $(this).attr('id');

        $(this).parent().next('.form51').find('.loading').show();

        $(this).parent().next('.form51').find('.formulario-content').load(PATH.basename + "/paginas/formulario_min/" + tipo_form + "/" + url_form + "/" + loja_id, function() {
            $('.loading').hide();
            validaCampos();
        });
    });

    //fecha modal de formulario na tela 'onde-estamos'
    $('.form5 .btn-fechar-modal').live('click', function() {
        $('.form5').fadeOut();
        $('.formulario-content').html(" ");
    });


    //evento click do nome da cidade, que exibe/oculta o thumb do cliente
    $('.green a').live('click', function() {
        $('img.left').fadeOut();
        $(this).parent().parent().find('.left').fadeIn();
    });

    //funcionalide do evento click no nome da loja no elemento 'LOJAS'
    //evento click do nome da cidade, que destaca os dados da loja atual em verde
    $('.btn-nome-loja').live('click', function() {
        $('.content-lojas').removeClass('verde');
        $('img.left').fadeOut();
        $(this).parent().parent().find('.left').fadeIn();
        $(this).parent().parent().addClass('verde');
    });

    //evento click do botao 'buscar' da tela 'vendedores externo'
    $("#busca-vendedores #btn-buscar").click(function() {
        var estado = $('#estado').val();
        var cidade = $('#cidade').val();
        if (cidade == 'Cidade') {
            cidade = "";
        }
        var url_form = $('#url_form').val();
        $('.loading').show();
        $("#content-busca").html(" ");
        $("#content-busca").load(PATH.basename + "/vendedores/busca/" + url_form + "/" + estado + "/" + cidade, function() {
            var html = '<span>UF: ' + estado + '</span> <strong>Cidade: ' + cidade + '</strong>';
            $(".row5").html(html).show();
            $('.loading').hide();
        });
    });

    //evento click do botao 'entrar em contato' da tela 'vendedores externo'
    $('.btn-vendedor-entrar-contato').live('click', function() {
        $('.form5').fadeOut();
        $('.formulario-content').html(" ");
        $(this).next('.form5').fadeIn();

        var tipo_form = $('#tipo_form').val();
        var url_form = $(this).attr('rel');
        var vendedor_id = $(this).attr('id');
        $(this).next('.form5').find('.loading').show();

        $(this).next('.form5').find('.formulario-content').load(PATH.basename + "/paginas/formulario_min/" + tipo_form + "/" + url_form + "/" + vendedor_id, function() {
            $('.loading').hide();
            validaCampos();
        });
    });

    //fecha modal de formulario na tela 'vendedores externo'
    $('.form51 .btn-fechar-modal').live('click', function() {
        $('.form51').fadeOut();
        $('.formulario-content').html(" ");
    });

    //formata o campo cidade, do formulario de vendedores
    if ($('#busca-vendedores #cidade').length)
    {
        $('#busca-vendedores #cidade').focus(function() {
            if ($('#busca-vendedores #cidade').val() == 'Cidade') {
                $('#busca-vendedores #cidade').val('');
            }
        });

        $('#busca-vendedores #cidade').blur(function() {
            if ($('#busca-vendedores #cidade').val() == '') {
                $('#busca-vendedores #cidade').val('Cidade');
            }
        });
    }

    //formata a sanfona da tela de duvidas frequentes
    $(".duvida-frequente li").first().find(".duvida_frequente_resposta").slideDown();
    $(".duvida-frequente li").first().attr('rel', 'ativo').attr('class', 'active');
    $(".duvida-frequente li h3 a").click(function() {
        if ($(this).parent().parent().attr('rel') == 'ativo') {
            $(this).parent().parent().find(".duvida_frequente_resposta").slideUp();
            $(this).parent().parent().attr('rel', 'inativo');
            $(this).parent().parent().removeClass('active');
        } else {
            $(".duvida_frequente_resposta").slideUp();
            $(".duvida-frequente li h3 a").attr('rel', 'inativo');
            $(".duvida-frequente li h3 a").removeClass('ativa');
            $(this).parent().parent().find(".duvida_frequente_resposta").slideDown();
            $(this).parent().parent().attr('rel', 'ativo');
            $(".duvida-frequente li").removeClass('active');
            $(this).parent().parent().addClass('active');
        }
    });

    //begin calculos de injetores
    if ($('#frm-calculo-injetores').length) {
        $('#frm-calculo-injetores').submit(function() {
            return false;
        });

        if ($("#tipo-calculo").val() == "calculo-injetor") {
            $("#block-calculo-potencia").hide();
            $("#result-calculo-potencia").hide();

            $("#block-calculo-injetor").show();
            $("#result-calculo-injetor").show();
        } else {
            $("#block-calculo-potencia").show();
            $("#result-calculo-potencia").show();

            $("#block-calculo-injetor").hide();
            $("#result-calculo-injetor").hide();
        }

        $("#tipo-calculo").change(function() {
            if ($(this).val() != "") {
                if ($(this).val() == "calculo-injetor") {
                    $("#block-calculo-potencia").hide();
                    $("#result-calculo-potencia").hide();

                    $("#block-calculo-injetor").show();
                    $("#result-calculo-injetor").show();
                } else {
                    $("#block-calculo-potencia").show();
                    $("#result-calculo-potencia").show();

                    $("#block-calculo-injetor").hide();
                    $("#result-calculo-injetor").hide();
                }
            }
        });

    }
    $('#btn-calcular-injetores').live('click', function() {
        calc_injetor();
    });

    $('#btn-calcular-potencia').live('click', function() {
        calc_potencia();
    });

    if ($('#calc-potencia-vazao-bicos-injetores').length) {
        $('#calc-potencia-vazao-bicos-injetores').setMask({
            mask: '999999999'
        });
    }
    if ($('#calc-potencia-numero-bico-injetores').length) {
        $('#calc-potencia-numero-bico-injetores').setMask({
            mask: '999999999'
        });
    }
    if ($('#calc-injetor-potencia-motor').length) {
        $('#calc-injetor-potencia-motor').setMask({
            mask: '999999999'
        });
    }
    if ($('#calc-injetor-numero-bico-injetores').length) {
        $('#calc-injetor-numero-bico-injetores').setMask({
            mask: '999999999'
        });
    }
    //end calculos de injetores

    //begin mapa revendedores
    $("#form-revendedores #revendedor-estados").live('change', function() {
        $('#revendedor-cidade').val("");
    });

    $('#revendedor-estados').change(function() {
        $(".loading").show();
        $('#btn-busca-revendedor').hide();
        var estado = $(this).val();
        $('#revendedor-cidade').text('');
        $.post(PATH.basename + "/revendedores/get_cidades/" + estado, function(response) {
            $.each(response, function(i, value) {
                $('#revendedor-cidade').append($('<option>').text(value).attr('value', value));
                $(".loading").hide();
                $('#btn-busca-revendedor').show();
            });
        }, "json");
    });

    $('#btn-busca-revendedor').live('click', function() {
        $('.loading').show();
        var estado = $('#revendedor-estados').val();
        var cidade = $('#revendedor-cidade').val();
        if (cidade != " " && cidade != null) {
            cidade = encodeURI(cidade);
        }
        $('html, body').animate({
            scrollTop: $("#content-revendedores").offset().top - 100
        }, 500);
        $('#content-revendedores').load(PATH.basename + "/revendedores/busca_revendedores/" + estado + "/" + cidade, function() {
            $('.loading').hide();
        });
        return false;
    });
    $("#list-revendedores ul li a").live('click', function() {
        $('.loading').show();
        $('html, body').animate({
            scrollTop: $("#content-revendedores").offset().top - 100
        }, 500);
        $('#content-revendedores').load(PATH.basename + "/revendedores/lista_revendedores/" + $(this).attr('rel'), function() {
            $('.loading').hide();
            $('.loading2').hide();
        });
    });
    $("#content-revendedores .pagination ul li a").live('click', function() {
        $('.loading2').show();
        $("#content-revendedores").load(this.href);
        return false;
    });

//    $("#list-revendedores ul li a").each(function(index) {
//        $(".mapa-revendedores ul li a[rel=" + $(this).attr('rel') + "]").addClass('active').bind('click', function(event) {
//            click_revendedores($(this).attr('rel'));
//        });
//    });

    if(jQuery().mapster) {
        $('#Image-Map-Revendedores').mapster({
                fillColor: 'E18264',
                fillOpacity: 0.4,
                isSelectable: false,
                mapKey: 'rel',
                onClick: function(){
                    click_revendedores($(this).attr('rel'));
                }
        });
        $("#list-revendedores ul li a").each(function(index) {
            $('#_Image-Map-Revendedores area[rel="' + $(this).attr('rel') + '"]').mapster('set', true);
        });
     }
    //end mapa revendedores


    //begin mapa parceiros
    $("#form-parceiros #parceiro-estados").live('change', function() {
        $('#parceiro-cidade').val("");
    });

    $('#parceiro-estados').change(function() {
        $(".loading").show();
        $('#btn-busca-parceiro').hide();
        var estado = $(this).val();
        $('#parceiro-cidade').text('');
        $.post(PATH.basename + "/parceiros/get_cidades/" + estado, function(response) {
            $.each(response, function(i, value) {
                $('#parceiro-cidade').append($('<option>').text(value).attr('value', value));
                $(".loading").hide();
                $('#btn-busca-parceiro').show();
            });
        }, "json");
    });

    $('#btn-busca-parceiro').live('click', function() {
        $('.loading').show();
        var estado = $('#parceiro-estados').val();
        var cidade = $('#parceiro-cidade').val();
        if (cidade != "") {
            cidade = encodeURI(cidade);
        }
        $('html, body').animate({
            scrollTop: $("#content-parceiros").offset().top - 100
        }, 500);
        $('#content-parceiros').load(PATH.basename + "/parceiros/busca_parceiros/" + estado + "/" + cidade, function() {
            $('.loading').hide();
        });
        return false;
    });

    $("#list-parceiros ul li a").live('click', function() {
        $('.loading').show();
        $('html, body').animate({
            scrollTop: $("#content-parceiros").offset().top - 100
        }, 500);
        $('#content-parceiros').load(PATH.basename + "/parceiros/lista_parceiros/" + $(this).attr('rel'), function() {
            $('.loading').hide();
            $('.loading2').hide();
        });
    });
    $("#content-parceiros .pagination ul li a").live('click', function() {
        $('.loading2').show();
        $("#content-parceiros").load(this.href);
        return false;
    });

//    $("#list-parceiros ul li a").each(function(index) {
//        $(".mapa-parceiros ul li a[rel=" + $(this).attr('rel') + "]").addClass('active').bind('click', function(event) {
//            click_parceiros($(this).attr('rel'));
//        });
//    });
    
     if(jQuery().mapster) {
        $('#Image-Map-Parceiros').mapster({
                fillColor: 'E18264',
                fillOpacity: 0.4,
                isSelectable: false,
                mapKey: 'rel',
                onClick: function(){
                    click_parceiros($(this).attr('rel'));
                }
        });
        $("#list-parceiros ul li a").each(function(index) {
            $('#_Image-Map-Parceiros area[rel="' + $(this).attr('rel') + '"]').mapster('set', true);
        });
     }
    //end mapa parceiros

    //begin galeria fotos veiculos
    $(".btn-add-foto").click(function() {
        $('.content-fotos-veiculo .row').clone(true).appendTo('.content-fotos-veiculo-tmp').each(function() {
            $(this).find('.file-original').val('');
            $(this).find('.file-falso').val('');
        })
        $('.content-fotos-veiculo-tmp > div').each(function(i, v) {
            $('input', this).each(function(a, b) {
                str = $(this).attr('name').replace(/[0-9]+/g, i + 1);
                $(this).attr('name', str);
            })
        });
    });
    $(".file-original").live('change', function() {
        $(this).parent().find(".file-falso").val($(this).val());
    });
    $('#form-veiculo').submit(function() {
        if (!validaFormVeiculo()) {
            return false;

        } else
        {
			$('.container-info').slideDown();
			$('.container-info .info-upload').html('Enviando dados, favor aguardar...');
            return true;
        }
    });
    //end galeria fotos veiculos

    // Abre fecha abas
    $(".clique_sel").live('click', function() {
        var rel = $(this).attr("rel");
        $(".seleto").hide();
        $(".sel_" + rel).show();
        $(".clique_sel").removeClass("active");
        $(this).addClass("active");
        carregaZoom();
        $('.zoomWindow').delay(400).css('left', '390px');
    });

});

//btn de fechar da modal com formulario da pagina 'formulario-min'
function clickFechaModal() {
    $('.btn-fecha-form').click(function() {
        $('.content-form').slideUp().html(" ");
    });
}

//formulacao e validacao os campos do formulario da pagina 'formulario-min'
function validaCampos() {
    if ($('#SacNome').length)
    {
        $('#SacNome').focus(function() {
            if ($('#SacNome').val() == 'Nome*') {
                $('#SacNome').val('');
            }
        });

        $('#SacNome').blur(function() {
            if ($('#SacNome').val() == '') {
                $('#SacNome').val('Nome*');
            }
        });
    }

    if ($('#SacEmail').length)
    {
        $('#SacEmail').focus(function() {
            if ($('#SacEmail').val() == 'E-mail*') {
                $('#SacEmail').val('');
            }
        });

        $('#SacEmail').blur(function() {
            if ($('#SacEmail').val() == '') {
                $('#SacEmail').val('E-mail*');
            }
        })
    }
}

//validacao do formulario da pagina 'formulario-min'
function validaFormContato() {
    if ($("#SacNome").val() == "Nome*" || $("#SacEmail").val() == "E-mail*" || !is_email($("#SacEmail").val()) || $("#SacMensagem").val() == "") {

        if ($("#SacNome").val() == "Nome*") {
            $("#nome-msg-error").text("Preenchimento obrigatório.").show();
        } else {
            $("#nome-msg-error").text("").hide();
        }

        if ($("#SacEmail").val() == "E-mail*") {
            $("#email-msg-error").text("Preenchimento obrigatório.").show();
        } else if (!is_email($("#SacEmail").val())) {
            $("#email-msg-error").text("Formato inválido.").show();
        } else {
            $("#email-msg-error").text("").hide();
        }

        if ($("#SacMensagem").val() == "") {
            $("#mensagem-msg-error").text("Preenchimento obrigatório.").show();
        } else {
            $("#mensagem-msg-error").text("").hide();
        }

        return false;
    }
    return true;
}

//metodo que valida o email digitado
function is_email(email)
{
    er = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}/;

    if (er.exec(email))
    {
        return true;
    } else {
        return false;
    }
}

function calc_injetor() {
    var vl_pot_mot = parseInt($("#calc-injetor-potencia-motor").val());
    var vl_nbicos = parseInt($("#calc-injetor-numero-bico-injetores").val());
    var vl_tip_motor = parseFloat($("#calc-injetor-tipo-motor").val());
    var vl_tip_combust = parseFloat($("#calc-injetor-tipo-combustivel").val());
    var vl_cap_utilizada = parseFloat($("#calc-injetor-capacidade-bicos-injetores").val());

    var vl_dimbicos_lbhr = ((vl_pot_mot * vl_tip_motor * vl_tip_combust) / (vl_nbicos * vl_cap_utilizada));
    var vl_dimbicos_ccmin = vl_dimbicos_lbhr * 10.5

    $("#result-cc-min").val(Math.round(vl_dimbicos_ccmin));
    $("#result-lb-hr").val(rnd(vl_dimbicos_lbhr));
    var vl_potencia = ((vl_dimbicos_lbhr * vl_cap_utilizada * vl_nbicos) / (vl_tip_motor * vl_tip_combust));
    $("#result-potencia").val(vl_potencia);
}

function calc_potencia() {
    var v1_vazbicos = parseInt($("#calc-potencia-vazao-bicos-injetores").val());
    var vl_nbicos = parseInt($("#calc-potencia-numero-bico-injetores").val());
    var vl_tip_motor = parseFloat($("#calc-potencia-tipo-motor").val());
    var vl_tip_combust = parseFloat($("#calc-potencia-tipo-combustivel").val());
    var vl_cap_utilizada = parseFloat($("#calc-potencia-capacidade-bicos-injetores").val());

    var vl_potencia = ((v1_vazbicos * vl_nbicos * vl_cap_utilizada) / (vl_tip_motor * vl_tip_combust));

    $("#result-potencia-teorica").val(rnd(vl_potencia));
}

function rnd(num) {
    num = Math.floor((num * 100) + 0.5) / 100;
    return num;
}

function click_revendedores(rel) {
    $('.loading').show();
    $('html, body').animate({
        scrollTop: $("#content-revendedores").offset().top - 100
    }, 500);
    $('#content-revendedores').load(PATH.basename + "/revendedores/lista_revendedores/" + rel, function() {
        $('.loading').hide();
        $('.loading2').hide();
    });
}

function click_parceiros(rel) {
    $('.loading').show();
    $('html, body').animate({
        scrollTop: $("#content-parceiros").offset().top - 100
    }, 500);
    $('#content-parceiros').load(PATH.basename + "/parceiros/lista_parceiros/" + rel, function() {
        $('.loading').hide();
        $('.loading2').hide();
    });
}
//validacao do formulario da pagina 'veiculos-fueltech'
function validaFormVeiculo() {
    if (
            $("#VeiculoVeiculo").val() == ""
            || $("#VeiculoProprietario").val() == ""
            || $("#VeiculoCilindradas").val() == ""
            || $("#VeiculoPotenciaEstimada").val() == ""
            || $("#VeiculoComandoValvulas").val() == ""
            || $("#VeiculoInjetores").val() == ""
            || $("#VeiculoCombustivel").val() == ""
            || $("#VeiculoBobina").val() == ""
            ) {
        if ($("#VeiculoVeiculo").val() == "") {
            $("#veiculo-msg-error").text("Preenchimento obrigatório.").show();
        } else {
            $("#veiculo-msg-error").text("").hide();
        }

        if ($("#VeiculoProprietario").val() == "") {
            $("#proprietario-msg-error").text("Preenchimento obrigatório.").show();
        } else {
            $("#proprietario-msg-error").text("").hide();
        }

        if ($("#VeiculoCilindradas").val() == "") {
            $("#cilindradas-msg-error").text("Preenchimento obrigatório.").show();
        } else {
            $("#cilindradas-msg-error").text("").hide();
        }

        if ($("#VeiculoPotenciaEstimada").val() == "") {
            $("#potencia-estimada-msg-error").text("Preenchimento obrigatório.").show();
        } else {
            $("#potencia-estimada-msg-error").text("").hide();
        }

        if ($("#VeiculoComandoValvulas").val() == "") {
            $("#comando-valvulas-msg-error").text("Preenchimento obrigatório.").show();
        } else {
            $("#comando-valvulas-msg-error").text("").hide();
        }

        if ($("#VeiculoInjetores").val() == "") {
            $("#injetores-msg-error").text("Preenchimento obrigatório.").show();
        } else {
            $("#injetores-msg-error").text("").hide();
        }

        if ($("#VeiculoCombustivel").val() == "") {
            $("#combustivel-msg-error").text("Preenchimento obrigatório.").show();
        } else {
            $("#combustivel-msg-error").text("").hide();
        }

        if ($("#VeiculoBobina").val() == "") {
            $("#bobina-msg-error").text("Preenchimento obrigatório.").show();
        } else {
            $("#bobina-msg-error").text("").hide();
        }

        return false;
    }
    return true;
}