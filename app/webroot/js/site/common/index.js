$(document).ready(function() {

    // $('.header_form, .footer_form').submit(function() {
    // url = PATH.basename + "/busca/index/" + encodeURI($('.BuscaBusca',$(this)).val());
    // console.log(url);
    //return false;
    // window.location = url;
    // return false;
    // });

    //$.noConflict();
	//$('.container-info').slideUp();
	$('#form-veiculo').submit(function(){
		//$('.container-info').slideDown();
		$('.container-info .info-upload').html('Enviando arquivos, favor aguardar...');
	});

    $('.header_form').submit(function() {
        window.location = PATH.basename + "/busca/index/" + encodeURI($('.BuscaBusca', $(this)).val());
        return false;
    });

    $.cookie = function(key, value, options) {

        // key and at least value given, set cookie...
        if (arguments.length > 1 && (!/Object/.test(Object.prototype.toString.call(value)) || value === null || value === undefined)) {
            options = $.extend({}, options);

            if (value === null || value === undefined) {
                options.expires = -1;
            }

            if (typeof options.expires === 'number') {
                var days = options.expires, t = options.expires = new Date();
                t.setDate(t.getDate() + days);
            }

            value = String(value);

            return (document.cookie = [
                encodeURIComponent(key), '=', options.raw ? value : encodeURIComponent(value),
                options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
                options.path ? '; path=' + options.path : '',
                options.domain ? '; domain=' + options.domain : '',
                options.secure ? '; secure' : ''
            ].join(''));
        }

        // key and possibly options given, get cookie...
        options = value || {};
        var decode = options.raw ? function(s) {
            return s;
        } : decodeURIComponent;

        var pairs = document.cookie.split('; ');
        for (var i = 0, pair; pair = pairs[i] && pairs[i].split('='); i++) {
            if (decode(pair[0]) === key)
                return decode(pair[1] || ''); // IE saves cookies with empty string as "c; ", e.g. without "=" as opposed to EOMB, thus pair[1] may be undefined
        }
        return null;
    };
    /**
     * formulário submete os dados para o controller e retorna os campos inválidos via json.
     * Quando os campos estiverem válidos redireciona o usuário com a mensagem de sucesso.
     **/
    $('#newsbox').submit(function() {
        var query = $(this).serialize();
        //posta os dados para a action definida no form
        $.post($(this).attr('action'), query, function(data) {
            if (!data.success) {
                //remove erros anteriores
                $(".error-message").remove();
                var msg = '';
                $.each(data.error, function(i, v) {
                    //adiciona erro para cada campo inválido
                    msg += v + '\r\n';
                })
                alert(msg);

                $('#NewsletterNome').addDefaultText();
                $('#NewsletterEmail').addDefaultText();
            } else {
                $('#NewsletterNome').val('Nome');
                $('#NewsletterEmail').val('E-mail');
                alert('Cadastro realizado com sucesso, em breve entraremos em contato com novidades.');

                return false;
            }
        }, 'json');
        return false;
    })

    $(".useDefault").addDefaultText();

    $('a.menuvermais').click(function() {
        $.cookie('menu-set', $(this).attr('rel'));
        if ($(this).html() == '[ + ]') {
            $(this).html('[ - ]');
        } else {
            $(this).html('[ + ]');
        }
        $(this).next('ul.menuamais').toggle();
    })


    $('a.' + $.cookie('menu-set')).click();


    $('div#tab_box_hp div#tab_nav_hp ul li a').click(function() {
        $('div#tab_box_hp div#tab_nav_hp ul li a').removeClass();
        $(this).addClass('active');
        $('div#tab_box_hp div#tab_content_hp').hide();
        $('div#tab_box_hp div#tab_content_hp.vitrine-' + $(this).attr('rel')).show();
        return false;
    })

    if ($('#BuscaBusca').length) {
        ini = true;
        ini2 = true;

        $("#BuscaBusca").autocomplete({
            //delay: 0,
            source: PATH.basename + "/busca/",
            //source:  PATH.basename + "/produtos.json",
            //minLength: 1,
            messages: {
                noResults: null,
                results: function() {
                }
            },
            select: function(event, ui) {
                if (ui.item.value != "") {
                    window.location.href = PATH.basename + "/busca/index/" + encodeURI(ui.item.value);
                }
            }

        }).data("autocomplete")._renderItem = function(ul, item) {
            var vLabel = item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");

            if (ini) {
                ini = false;
                var inner_html = '<a href="javascript:void(0);"><div class="list_item_container"><div class="label">' + vLabel + '</div><div class="itens">' + item.quantidade_itens + ' itens</div></div></a>';
            } else {
                var inner_html = '<a href="javascript:void(0);"><div class="list_item_container"><div class="label">' + vLabel + '</div><div class="itens">' + item.quantidade_itens + ' itens</div></div></a>';
            }

            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append(inner_html)
                    .appendTo(ul);
        };
    }

    if ($('#BuscaBusca').length)
    {
        $('#BuscaBusca').focus(function() {
            if ($('#BuscaBusca').val() == 'Busque no site') {
                $('#BuscaBusca').val('');
            }
        });

        $('#BuscaBusca').blur(function() {
            if ($('#BuscaBusca').val() == '') {
                $('#BuscaBusca').val('Busque no site');
            }
        })
    }

    if ($('#BuscaFooterBusca').length)
    {
        $('#BuscaFooterBusca').focus(function() {
            if ($('#BuscaFooterBusca').val() == 'Busque no site') {
                $('#BuscaFooterBusca').val('');
            }
        });

        $('#BuscaFooterBusca').blur(function() {
            if ($('#BuscaFooterBusca').val() == '') {
                $('#BuscaFooterBusca').val('Busque no site');
            }
        })
    }

    //## START carrousel da home ##//
    $('.control-nav ul li').first().addClass("active");
    $('.carousel_topo').jcarousel({
        scroll: 1,
        auto: 4,
        wrap: 'last',
        initCallback: mycarousel_initCallback,
        itemVisibleInCallback: {
            onAfterAnimation: function(c, o, i, s) {
                --i;
                $('.control-nav li').removeClass('active');
                $('.control-nav li').eq(i).addClass('active');
            }
        }
    });

    function mycarousel_initCallback(carousel) {
        $('.control-nav li').bind('click', function() {
            carousel.scroll($.jcarousel.intval($(this).text()));
            return false;
        });

        $('.jcarousel-next').bind('click', function() {
            carousel.next();
            return false;
        });

        $('.jcarousel-prev').bind('click', function() {
            carousel.prev();
            return false;
        });
    }

    if ($('.vitrine_marcas').length) {
        $('.vitrine_marcas').jcarousel({
            scroll: 1,
            auto: 3,
            wrap: 'last',
            initCallback: function(jc, state) {
                if (state == 'init') {
                    jc.startAutoOrig = jc.startAuto;
                    jc.startAuto = function() {
                        if (!jc.paused) {
                            jc.startAutoOrig();
                        }
                    }
                    jc.pause = function() {
                        jc.paused = true;
                        jc.stopAuto();
                    };
                    jc.play = function() {
                        jc.paused = false;
                        jc.startAuto();
                    };
                    $('li.jcarousel-item').mouseover(function() {
                        jc.pause();
                    });
                    $('li.jcarousel-item').mouseout(function() {
                        jc.play();
                    });
                }
                jc.play();
            }
        });
    }

    //## END carrousel da home ##//

    //## START menu topo  ##//
    //eu sei que isso por parecer estranho, mas foi o que o layout exigiu
    var offset_nivel_1 = $('#dropdown-list1 .nivel-1').offset();
    $("#dropdown-list1 .nivel-1 li").each(function() {
        var offset_nivel_1_li = $(this).offset();
        var diferenca = parseFloat(offset_nivel_1.top) - parseFloat(offset_nivel_1_li.top);
        $(this).find('.dropdown-list2').css('top', diferenca);
    });

    $('#dropdown-list1 .nivel-1 li').first().addClass('active');
    $('#dropdown-list1 .nivel-1 li .dropdown-list2').first().show();

    $('#dropdown-list3 .nivel-1 li').first().addClass('active');
    $('#dropdown-list3 .nivel-1 li .dropdown-list2').first().show();


    //eu sei que isso por parecer estranho, mas foi o que o layout exigiu, mais titulo
    var offset_nivel_1 = $('#dropdown-list3 .nivel-1').offset();
    $("#dropdown-list3 .nivel-1 li").each(function() {
        var diferenca = "";
        var offset_nivel_1_li = $(this).offset();
        var diferenca = (parseFloat(offset_nivel_1.top) - parseFloat(offset_nivel_1_li.top));
        $(this).find('.dropdown-list2').css('top', diferenca);
    });

    $("#dropdown-list1 .nivel-1 li").hover(function() {
        $("#dropdown-list1 .nivel-1 li").removeClass('active');
        $('.dropdown-list2').hide();
        $(this).find('.dropdown-list2').show();
    });

    $("#dropdown-list3 .nivel-1 li").hover(function() {
        $("#dropdown-list3 .nivel-1 li").removeClass('active');
        $('.dropdown-list2').hide();
        $(this).find('.dropdown-list2').show();
    });
    //## END menu topo  ##//

    $('.btn-ver-todas').live('click', function() {
        if ($(this).attr('rel') == 'mais')
        {
            $(this).parent().find('.inativo').removeClass('inativo').addClass('ocultos');
            $(this).attr('rel', 'menos');
            $(this).text('ocultar');
        }
        else
        {
            $(this).parent().find('.ocultos').addClass('inativo');
            $(this).attr('rel', 'mais');
            $(this).text('ver todas');
        }

    });

    $('.breadcrumb .box ul li').last().find('a').css('color', '#007F43').css('font-weight', 'bold').css('font-size', '12px');

    $('.btn_voltar').click(function() {
        window.history.go(-1);
    });

    $('.btn-topo').click(function() {
        $('html, body').animate({
            scrollTop: $("#header").offset().top
        }, 500);
    });
});

//## START mascara_8_9_dig ##//
//metodos que altera a mascara do campo telefone, permitindo ao usuario colocar 8 ou 9 digitos
function mascara_8_9_dig(o, f) {
    v_obj = o
    v_fun = f
    setTimeout("execmascara()", 1)
}
function execmascara() {
    v_obj.value = v_fun(v_obj.value)
}
function mtel(v) {
    v = v.replace(/\D/g, "");             //Remove tudo o que não é dígito  
    v = v.replace(/^(\d{2})(\d)/g, "($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos  
    v = v.replace(/(\d)(\d{4})$/, "$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos  
    return v;
}
//## END mascara_8_9_dig ##//