/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$(function() {
    $('.lstAbasCategorias a').click(function(){
        var link = $(this);
        $("#home-vitrines").load(PATH.basename + "/vitrines/ajax_vitrines/" + $(this).attr('rel'),function(){
            $('.lstAbasCategorias a').removeClass('abaVermelha').addClass('abaCinza');
            link.removeClass('abaCinza').addClass('abaVermelha');
        });

    })

    $(".titulo-vitrine").click(function(){
        $(".content_produto").hide();
        $(".vitrine" + $(this).attr("rel") ).show(200);
        $(".titulo-vitrine").removeClass("active");
        $(this).addClass("active");
    });

    $(".titulo-vitrine:eq(0)").trigger("click");
	
	$('.mycarousel').jcarousel();

	$('#control-nav ul li').first().addClass("active");
	$('.mycarouseltopo').jcarousel({
		scroll: 1,
		auto: 4,
        wrap: 'last',
        initCallback: mycarousel_initCallback,
		itemVisibleInCallback: {
            onAfterAnimation: function (c, o, i, s) {
                --i;
                $('#control-nav li').removeClass('active');
                $('#control-nav li').eq(i).addClass('active');
            }
        }
	});
	
	function mycarousel_initCallback(carousel) {
		$('#control-nav li a').bind('click', function() {
			carousel.scroll($.jcarousel.intval($(this).text()));
			return false;
		}); 		
		
		$('.jcarousel-next').bind('click', function () {        
			carousel.next();        
			return false;
		});

		$('.jcarousel-prev').bind('click', function () {
			carousel.prev();
			return false;
		});
	}
	
	$('.mycarousel-vitrine').jcarousel({
		scroll: 1,
		auto: 10,
		itemFallbackDimension: 728
	});
	
	// Abre fecha abas
	$(".clicaaba").live('click',function(){
		var rel = $(this).attr("rel");
		$(".abas").hide();
		$('.abas[rel="' + rel + '"]').show();
		$(".clicaaba").removeClass("active");
		$(this).addClass("active");
	});
	
	$(".abas").hide();
	$('.abas[rel="0"]').show();
	
	//carousel vitrine normal
	$('.vitrineNormal').jcarousel({
		scroll: 1,
        wrap: 'last',
        initCallback: vitrineNormal_initCallback
	});
	function vitrineNormal_initCallback(carousel) {
		$('.vitrineNormal .jcarousel-next').bind('click', function () {        
			carousel.next();        
			return false;
		});

		$('.vitrineNormal .jcarousel-prev').bind('click', function () {
			carousel.prev();
			return false;
		});
	}
	
	//carousel vitrine normal
	$('.maisVendidos').jcarousel({
		scroll: 1,
        wrap: 'last',
        initCallback: maisVendidos_initCallback
	});
	function maisVendidos_initCallback(carousel) {
		$('.maisVendidos .jcarousel-next').bind('click', function () {        
			carousel.next();        
			return false;
		});

		$('.maisVendidos .jcarousel-prev').bind('click', function () {
			carousel.prev();
			return false;
		});
	}
	
});