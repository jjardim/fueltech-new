/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$(function() {
    
    $('.mask-cep').setMask({mask:'99.999-999'});
    $('.mask-telefone').setMask({mask:'(99) 9999-99999'});
    $('.mask-cpf').setMask({mask:'999.999.999-99'});
    $('.mask-cnpj').setMask({mask:'99.999.999.9999/99'});
    $('.mask-data').setMask({mask:'99/99/9999'});
	
	$('.mask-telefone-8-9').keypress(function(){
		  mascara_8_9_dig( this, mtel );  
	});

    $('#title-cadastro-pessoa-fisica').hide();
	$('#title-cadastro-pessoa-juridica').hide();
	$("input[name='data[Usuario][tipo_pessoa]']").each(function () {
		if ($(this).attr('checked'))
		{
			if($(this).val() == "F"){
				$('#title-cadastro-pessoa-fisica').show();
				$('#title-cadastro-pessoa-juridica').hide();
			}else if($(this).val() == "J"){
				$('#title-cadastro-pessoa-fisica').hide();
				$('#title-cadastro-pessoa-juridica').show();
			}				
		}		
	});
	

    if($("input[name='data[Usuario][tipo_pessoa]']:checked").val() == 'J'){
		$("#UsuarioNome").attr("disabled", "disabled").parent().hide();
		$("#UsuarioNomeTmp").attr("disabled", "").parent().show();
        $("#UsuarioSexo").attr("disabled", "disabled").parent().hide();
        $("#UsuarioDataNascimento").attr("disabled", "disabled").parent().hide();
        $("#UsuarioRg").attr("disabled", "disabled").parent().hide();
        $("#UsuarioCpf").attr("disabled", "disabled").parent().hide();
        $("#UsuarioCnpj").attr("disabled", "").parent().show();
        $("#UsuarioNomeFantasia").attr("disabled", "").parent().show();
        $("#UsuarioRazaoSocial").attr("disabled", "").parent().show();
        $("#UsuarioInscricaoEstadual").attr("disabled", "").parent().show();
		//$("#UsuarioTipoPessoa").val('J');
    }else{
		$("#UsuarioNomeTmp").attr("disabled", "disabled").parent().hide();
		$("#UsuarioNome").attr("disabled", "").parent().show();
        $("#UsuarioSexo").attr("disabled", "").parent().show();
        $("#UsuarioDataNascimento").attr("disabled", "").parent().show();
        $("#UsuarioRg").attr("disabled", "").parent().show();
        $("#UsuarioCpf").attr("disabled", "").parent().show();
		$("#UsuarioCnpj").attr("disabled", "disabled").parent().hide();
        $("#UsuarioNomeFantasia").attr("disabled", "disabled").parent().hide();
        $("#UsuarioRazaoSocial").attr("disabled", "disabled").parent().hide();
		$("#UsuarioInscricaoEstadual").attr("disabled", "disabled").parent().hide();
		//$("#UsuarioTipoPessoa").val('F');
    }

    $("input[name='data[Usuario][tipo_pessoa]']").change(function(){
        if($("input[name='data[Usuario][tipo_pessoa]']:checked").val()=='J'){
			$("#UsuarioNome").attr("disabled", "disabled").parent().hide();
			$("#UsuarioNomeTmp").attr("disabled", "").parent().show();
            $("#UsuarioSexo").attr("disabled", "disabled").parent().hide();
            $("#UsuarioDataNascimento").attr("disabled", "disabled").parent().hide();
            $("#UsuarioRg").attr("disabled", "disabled").parent().hide();
            $("#UsuarioCpf").attr("disabled", "disabled").parent().hide();
            $("#UsuarioCnpj").attr("disabled", "").parent().show();
            $("#UsuarioNomeFantasia").attr("disabled", "").parent().show();
            $("#UsuarioRazaoSocial").attr("disabled", "").parent().show();
            $("#UsuarioInscricaoEstadual").attr("disabled", "").parent().show();
			$('#title-cadastro-pessoa-fisica').hide();
			$('#title-cadastro-pessoa-juridica').show();
        }else{
			$("#UsuarioNomeTmp").attr("disabled", "disabled").parent().hide();
			$("#UsuarioNome").attr("disabled", "").parent().show();
            $("#UsuarioSexo").attr("disabled", "").parent().show();
            $("#UsuarioDataNascimento").attr("disabled", "").parent().show();
            $("#UsuarioRg").attr("disabled", "").parent().show();
            $("#UsuarioCpf").attr("disabled", "").parent().show();
            $("#UsuarioCnpj").attr("disabled", "disabled").parent().hide();
            $("#UsuarioNomeFantasia").attr("disabled", "disabled").parent().hide();
            $("#UsuarioRazaoSocial").attr("disabled", "disabled").parent().hide();
            $("#UsuarioInscricaoEstadual").attr("disabled", "disabled").parent().hide();
			$('#title-cadastro-pessoa-fisica').show();
			$('#title-cadastro-pessoa-juridica').hide();
        }

    })
    $("#loading").ajaxStart(function(){
            $(this).css("visibility", "visible");
            $(this).css("vertical-align", "middle");
            $(this).css("margin-right", "15px");
        });
        $("#loading").ajaxComplete(function(event,request, settings){
            $(this).css("visibility", "hidden");
             $(this).css("vertical-align", "middle");
            $(this).css("margin-right", "15px");
        });
		
    $('#UsuarioEnderecoCep').change(function(){
        cep = $(this).val();
        $.post(PATH.basename + "/usuario_enderecos/ajax_correios_endereco/" + cep ,function(values){
            if(values){
                $('#UsuarioEnderecoRua').val(values.rua);
                $('#UsuarioEnderecoCidade').val(values.cidade);
                $('#UsuarioEnderecoBairro').val(values.bairro);
                $('#UsuarioEnderecoUf').val(values.estado);
            }
        },'json');
    });
	
	if( $('#UsuarioEnderecoCep').val() != "" )
	{
        cep = $('#UsuarioEnderecoCep').val();
        $.post(PATH.basename + "/usuario_enderecos/ajax_correios_endereco/" + cep ,function(values){
            if(values){
                $('#UsuarioEnderecoRua').val(values.rua);
                $('#UsuarioEnderecoCidade').val(values.cidade);
                $('#UsuarioEnderecoBairro').val(values.bairro);
                $('#UsuarioEnderecoUf').val(values.estado);
            }
        },'json');
    };
	
	
    $(".useDefault").addDefaultText();



    $('a.editar').click(function(){
		$.post(PATH.basename + "/usuarios/ajax_endereco_usuario/" + $(this).attr('rel'),function(values){
            if(values){
				$("input[name='data[UsuarioEndereco][cobranca]']").each(function () {
					if($(this).val() == values.UsuarioEndereco.cobranca){
						$(this).attr('checked','checked');
					}
				});
                $('#UsuarioEnderecoId').val(values.UsuarioEndereco.id);
                $('#UsuarioEnderecoRua').val(values.UsuarioEndereco.rua);
                $('#UsuarioEnderecoCidade').val(values.UsuarioEndereco.cidade);
                $('#UsuarioEnderecoBairro').val(values.UsuarioEndereco.bairro);
                $('#UsuarioEnderecoUf').val(values.UsuarioEndereco.uf);
                $('#UsuarioEnderecoNome').val(values.UsuarioEndereco.nome);
                $('#UsuarioEnderecoNumero').val(values.UsuarioEndereco.numero);
                $('#UsuarioEnderecoComplemento').val(values.UsuarioEndereco.complemento);
                $('#UsuarioEnderecoCep').val(values.UsuarioEndereco.cep).setMask({mask:'99.999-999'});
            }
        },'json');
		
		$("html, body").animate({scrollTop: $('#form-endereco-content').position().top}, 'slow');
        return false;
    })
	
	/*##  begin tela de enderešos de entrega ##*/
	$(".box-dados-endereco").css("display", "none");
	$(".box-dados-endereco").first().show();
	$("ul.list1 li").first().addClass('active');
	$("ul.list1 li a").first().addClass('active');
	
	$("ul.list1 li").click(function(){
		$("ul.list1 li").removeClass('active');
		$("ul.list1 li a").removeClass('active');
		$(".box-dados-endereco").hide();
		$("#endereco-"+$(this).find('a').attr('rel')).show();
		$(this).addClass('active');
		
	});
	
	$("#btn-cadastrar-novo-endereco").click(
		function(){
			$('html, body').animate({
					scrollTop: $(".column5").offset().top
			}, 500);
	});
});