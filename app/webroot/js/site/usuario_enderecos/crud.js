/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$(function() {	    
    $('a.editar').click(function(){
		$.post(PATH.basename + "/usuarios/ajax_endereco_usuario/" + $(this).attr('rel'),function(values){
            if(values){
				$("input[name='data[UsuarioEndereco][cobranca]']").each(function () {
					if($(this).val() == values.UsuarioEndereco.cobranca){
						$(this).attr('checked','checked');
					}
				});
                $('#UsuarioEnderecoId').val(values.UsuarioEndereco.id);
                $('#UsuarioEnderecoRua').val(values.UsuarioEndereco.rua);
                $('#UsuarioEnderecoCidade').val(values.UsuarioEndereco.cidade);
                $('#UsuarioEnderecoBairro').val(values.UsuarioEndereco.bairro);
                $('#UsuarioEnderecoUf').val(values.UsuarioEndereco.uf);
                $('#UsuarioEnderecoNome').val(values.UsuarioEndereco.nome);
                $('#UsuarioEnderecoNumero').val(values.UsuarioEndereco.numero);
                $('#UsuarioEnderecoComplemento').val(values.UsuarioEndereco.complemento);
                $('#UsuarioEnderecoCep').val(values.UsuarioEndereco.cep).setMask({mask:'99.999-999'});
            }
        },'json');
		
		$("html, body").animate({scrollTop: $('#form-endereco-content').position().top}, 'slow');
        return false;
    })
});