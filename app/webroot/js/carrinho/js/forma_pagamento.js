$(document).ready(function() {

    $(".loading").ajaxStart(function(){
        $(this).css("visibility", "visible");
        $(this).css("vertical-align", "middle");
    });
    $(".loading").ajaxComplete(function(event,request, settings){
        $(this).css("visibility", "hidden");
        $(this).css("vertical-align", "middle");
    });
    $('.mask-data').setMask({
        mask:'39/9999'
    });
	$('.mask-numerico').setMask({mask:'9',type:'repeat'});
    $('#PedidoCodigoSegurancaCartao').setMask({
        mask:'999'
    });
    $('#PedidoNumeroCartao').setMask({
        mask:'9999999999999999'
    });
	
	
	

	$('.fretes-disponiveis .tipo-entrega').live( 'click',function() {
		$.post(PATH.basename + "/carrinho/carrinho/set_frete/",{tipo_entrega:$('.tipo-entrega:checked').val()}, function(response){
			if(response.dados){
				$('#frete').html(response.dados.frete);
				$('#total').html(response.dados.total);
				$('#sub_total').html(response.dados.sub_total);
			
				try{
				 if(response.Cupom.status==true){
				 
					$('.cupom-lbl').html(null);
					$('.produto-totalizador,#total').attr('style',null);
					$('.cupom-msg').html(response.Cupom.msg);
					if(response.Cupom.tipo=="FRETE"){
						$('#frete-com-desconto').html('R$ '+response.Carrinho.dados.frete_com_desconto);
						$('#total-com-desconto').html(response.Carrinho.dados.total_com_desconto);
						$('#frete').css('text-decoration','line-through');	
					}else{
						$.each(response.Carrinho.itens,function(i,v){
							$('#produto-total-desconto-'+v.id).html('R$ '+v.preco_com_desconto);
							$('#produto-total-'+v.id).css('text-decoration','line-through');
						})
						$('#total-com-desconto').html(response.Carrinho.dados.total_com_desconto);
					}
					$('#total').css('text-decoration','line-through');
			   }else{
					$('.cupom-msg').html(response.Cupom.msg[0]);
			   }
			   }catch(e){}
				
			}
			
			
		}, "json");		
    });

    $('.tipo_pagamento').click(function(){
        $.post(PATH.basename + "/carrinho/carrinho/ajax_forma_pagamento/"+$(this).val(),{},function(data){
          $(".PedidoParcelas").empty();
            $(".PedidoParcelas").append(data);
        },"json");

        if($(this).attr('rel')=='VISA'||$(this).attr('rel')=='MASTERCARD'||$(this).attr('rel')=='ELO'||$(this).attr('rel')=='DINERS'||$(this).attr('rel')=='DISCOVER'){
            $('.cartao').show();
            $('.boleto').hide();
			$("html, body").animate({scrollTop: $('.cartao').position().top}, 'slow');
        }else if($(this).attr('rel')=='BOLETO'){
            $('.boleto').show();
            $('.cartao').hide();
	   }else{
			$('.boleto').hide();
            $('.cartao').hide();
		}
    })
	if($('.tipo_pagamento:checked').val()){
		if($('.tipo_pagamento:checked').attr('rel')=='VISA'||$('.tipo_pagamento:checked').attr('rel')=='MASTERCARD'||$('.tipo_pagamento:checked').attr('rel')=='ELO'||$('.tipo_pagamento:checked').attr('rel')=='DINERS'||$('.tipo_pagamento:checked').attr('rel')=='DISCOVER'){
            $('.cartao').show();
            $('.boleto').hide();
			$("html, body").animate({scrollTop: $('.cartao').position().top}, 'slow');
        }else if($('.tipo_pagamento:checked').attr('rel')=='BOLETO'){
            $('.boleto').show();
            $('.cartao').hide();
        }else{
			$('.boleto').hide();
            $('.cartao').hide();
		}
    }
})