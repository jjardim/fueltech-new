/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$(function() {

    $('.mask-cep').setMask({
        mask:'99.999-999'
    });
    $('.mask-telefone').setMask({
        mask:'(99) 9999-9999'
    });
    $('.mask-cpf').setMask({
        mask:'999.999.999-99'
    });
    $('.mask-cnpj').setMask({
        mask:'99.999.999.9999/99'
    });
    $('.mask-data').setMask({
        mask:'99/99/9999'
    });


    if($("#UsuarioTipoPessoa").val()=='J'){
        $("#UsuarioSexo").attr("disabled", "disabled").parent().parent().hide();
        $("#UsuarioDataNascimento").attr("disabled", "disabled").parent().hide();
        $("#UsuarioRg").attr("disabled", "disabled").parent().hide();
        $("#UsuarioCpf").attr("disabled", "disabled").parent().hide();
        $("#UsuarioCnpj").attr("disabled", "").parent().show();
        $("#UsuarioNomeFantasia").attr("disabled", "").parent().show();
        $("#UsuarioRazaoSocial").attr("disabled", "").parent().show();
        $("#UsuarioInscricaoEstadual").attr("disabled", "").parent().show();
    }else{
        $("#UsuarioSexo").attr("disabled", "").parent().parent().show();
        $("#UsuarioDataNascimento").attr("disabled", "").parent().show();
        $("#UsuarioRg").attr("disabled", "").parent().show();
        $("#UsuarioCpf").attr("disabled", "").parent().show();
        $("#UsuarioCnpj").attr("disabled", "disabled").parent().hide();
        $("#UsuarioNomeFantasia").attr("disabled", "disabled").parent().hide();
        $("#UsuarioRazaoSocial").attr("disabled", "disabled").parent().hide();
        $("#UsuarioInscricaoEstadual").attr("disabled", "disabled").parent().hide();
    }

    $("#UsuarioTipoPessoa").change(function(){
        if($(this).val()=='J'){
            $("#UsuarioSexo").attr("disabled", "disabled").parent().parent().hide();
            $("#UsuarioDataNascimento").attr("disabled", "disabled").parent().hide();
            $("#UsuarioRg").attr("disabled", "disabled").parent().hide();
            $("#UsuarioCpf").attr("disabled", "disabled").parent().hide();
            $("#UsuarioCnpj").attr("disabled", "").parent().show();
            $("#UsuarioNomeFantasia").attr("disabled", "").parent().show();
            $("#UsuarioRazaoSocial").attr("disabled", "").parent().show();
            $("#UsuarioInscricaoEstadual").attr("disabled", "").parent().show();
        }else{
            $("#UsuarioSexo").attr("disabled", "").parent().parent().show();
            $("#UsuarioDataNascimento").attr("disabled", "").parent().show();
            $("#UsuarioRg").attr("disabled", "").parent().show();
            $("#UsuarioCpf").attr("disabled", "").parent().show();
            $("#UsuarioCnpj").attr("disabled", "disabled").parent().hide();
            $("#UsuarioNomeFantasia").attr("disabled", "disabled").parent().hide();
            $("#UsuarioRazaoSocial").attr("disabled", "disabled").parent().hide();
            $("#UsuarioInscricaoEstadual").attr("disabled", "disabled").parent().hide();
        }

    })
    $("#loading").ajaxStart(function(){
        $(this).css("visibility", "visible");
        $(this).css("vertical-align", "middle");
        $(this).css("margin-right", "15px");
    });
    $("#loading").ajaxComplete(function(event,request, settings){
        $(this).css("visibility", "hidden");
        $(this).css("vertical-align", "middle");
        $(this).css("margin-right", "15px");
    });
    $('#UsuarioEnderecoCep').change(function(){
        cep = $(this).val();
        $.post(PATH.basename + "/usuario_enderecos/ajax_correios_endereco/" + cep ,function(values){
            if(values){
                $('#UsuarioEnderecoRua').val(values.rua);
                $('#UsuarioEnderecoCidade').val(values.cidade);
                $('#UsuarioEnderecoBairro').val(values.bairro);
                $('#UsuarioEnderecoUf').val(values.estado);
            }
        },'json');
    });
    $(".useDefault").addDefaultText();

    $(' a.editar').click(function(){
        //Focus no campo
        $("#UsuarioEnderecoCep").focus();
        $("html, body").animate({
            scrollTop: 640
        }, 'slow');
        $.post(PATH.basename + "/usuarios/ajax_endereco_usuario/" + $(this).attr('rel'),function(values){
            if(values){
                $('#UsuarioEnderecoId').val(values.UsuarioEndereco.id);
                $('#UsuarioEnderecoRua').val(values.UsuarioEndereco.rua);
                $('#UsuarioEnderecoCidade').val(values.UsuarioEndereco.cidade);
                $('#UsuarioEnderecoBairro').val(values.UsuarioEndereco.bairro);
                $('#UsuarioEnderecoUf').val(values.UsuarioEndereco.uf);
                $('#UsuarioEnderecoNome').val(values.UsuarioEndereco.nome);
                $('#UsuarioEnderecoNumero').val(values.UsuarioEndereco.numero);
                $('#UsuarioEnderecoComplemento').val(values.UsuarioEndereco.complemento);
                $('#UsuarioEnderecoCep').val(values.UsuarioEndereco.cep).setMask({
                    mask:'99.999-999'
                });
            }
        },'json');
        return false;
    })
});