<?php

class AppError extends ErrorHandler {

    
    function missingController($params) {
        $this->callErrorPage();
    }

    function missingAction($params) {
        $this->callErrorPage();
    }

    private function callErrorPage() {
        $params['url'] = str_replace('/', '', $_SERVER['REQUEST_URI']);

        if (preg_match('/home\.asp.*/i', $params['url'])) {
            $this->controller->redirect("/", 301);
        }

        $this->controller->loadModel('Url');
        $urls_direct = $this->controller->Url->find('all', array('conditions' => array('status' => true)));

        foreach ($urls_direct as $url):
            $urls[] = array($url['Url']['url_antiga'], $url['Url']['url_nova']);
        endforeach;
        $urls[] = array('home.asp', '');

        foreach ($urls as $url):
            $url[0] = str_replace('/', '', $url[0]);
            if ((isset($url[0]) && $url[0] == $params['url'])) {
                $this->controller->redirect("/{$url[1]}", $urls_direct['Url']['header']);
            }
        endforeach;
        
        //se chegou aqui Ž 404
        header("HTTP/1.0 404 Not Found");


      
    }
     function _outputMessage($template) {
        if($template=='error404'){
            $this->controller->set('seo_title', 'P&aacute;gina n&atilde;o encontrada.');
        }else{
            $this->controller->set('seo_title', 'Ocorreu um erro.');
        }
        $this->controller->beforeFilter();
        $this->controller->render($template, 'default');
        $this->controller->afterFilter();
        echo $this->controller->output;
    }
}

?>
